export const strings = {
    signIn: "Sign in",
    loginLabel: "Login to your account to continue",
    resetPassword: "Reset Password",
    forgetText: "Forgot your login details? ",
    dontHaveText: "Don’t have an account? ",
    signUp: "Sign up",
    newPasswordText: "Create new password",
    newPasslbl: "Your new password must be different from your previous used passwords.",
    resetPasswordSmall: "Reset password",
    confirmPassword: "Confirm Password",
    emailPlaceHolder: "Enter email address",
    verifyStr: "Verify Mobile",
    verifyDescStr: "Please enter the confirmation code sent to your number ",
    confirm: "Confirm",
    stepStr1: "Step 1 of 2",
    basicInformation: "Basic Information",
    firstName: "First Name*",
    lastName: "Last Name*",
    phoneNumber: "Phone number*",
    dobStr: "Date of Birth*",
    address: "Address",
    zipCode: "Zip Code",
    streetAddress: "Street Address",
    createAccount: "Create Account",
    verifyEmail: "Verify Email",
    enterVerifyCode: "Enter Verification code",
    confirmationcCode: "Confirmation code",
    verify: "Verify",
    emailAdress: "Email adress",
    signUpDesc: "Join the world’s first pet social media",
    termsCondtion: "Terms and Conditions",
    homeTab: 'Home',
    exploreTab: 'Explore',
    eventTab: 'Play Date',
    mapTab: 'Map',
    profileTab: 'Profile',
    uploadPhoto: "Upload Photo",
    stepStr2: "Step 2 of 2",
    recommended: "RECOMMENDED",
    addfriend: "Add friend",
    serachPlaceholder: "Search by name, email or phone no.",
    friends: "Friends",
    requestSent: "Request sent",
    categories: "CATEGORIES",
    viewAll: "View all",
    topResult: "Top Result",
    sortResult: "Sort Result",
    distance: "Distance",
    review: "Reviews",
    certificates: "Certificates",
    save: "Save",
    createEvent: "Create Play Date",
    eventName: "Play Date Title",
    startDate: "Start Date Time",
    startTime: "Start Time",
    addTime: "Add end date and time",
    removeTime: "Remove end date and time",
    endDate: "End Date Time",
    endTime: "End Time",
    participants: "Participants",
    selectParticipants: "Select Participants",
    detailTextParticipate: "Choose between public or friends & Groups",
    public: "Public",
    inviteAnyoneStr: "Invite anyone in the community",
    serachPlaceholderBottom: "Search for groups",
    proceed: "Proceed",
    next: "Next",
    skip: "Skip",
    selectLocation: "Select Location",
    addDtail: "Add Detail",
    detail: "Detail",
    addHere: "Add here",
    addLater: "Add Later",
    searchforFriends: "Search for friends",
    callHistory: "Call History",
    resetDescLabel: "Enter the email ID associated with your account and we’ll send an email with instructions to reset your password.",
    instructionsText: "Send Instructions",
    emailChecklbl: "Check your mail",
    emailDesc: "We have sent a password recover instructions to your email.",
    openEmailText: "Open Email App",
    cancelText: "Cancel",
    emailPlace: "Email address",
    password: "Password",
    emailPlaceHolder: "Enter email address",
    verifyStr: "Verify Mobile",
    verifyDescStr: "Please enter the confirmation code sent to your number ",
    confirm: "Confirm",
    stepStr1: "Step 1 of 2",
    basicInformation: "Basic Information",
    name: "Name*",
    firstName: "First Name*",
    lastName: "Last Name*",
    phoneNumber: "Phone number*",
    dobStr: "Date of Birth*",
    address: "Address",
    zipCode: "Zip Code",
    streetAddress: "Street Address",
    createAccount: "Create Account",
    verifyEmail: "Verify Email",
    enterVerifyCode: "Enter Verification code",
    confirmationcCode: "Confirmation code",
    verify: "Verify",
    emailAdress: "Email address",
    signUpDesc: "Join the world’s first pet social media",
    termsCondtion: "Terms and Conditions",
    homeTab: 'Home',
    exploreTab: 'Explore',
    mapTab: 'Map',
    profileTab: 'Profile',
    uploadPhoto: "Upload Photo",
    stepStr2: "Step 2 of 2",
    recommended: "RECOMMENDED",
    addfriend: "Add Friend",
    serachPlaceholder: "Search by name, email or phone no.",
    friends: "Friends",
    requestSent: "Request sent",
    categories: "CATEGORIES",
    viewAll: "View All",
    topResult: "Top Result",
    accept: "Accept",
    decline: "Decline",
    call: "Call",
    message: "Message",
    save: "Save",
    takeAPhoto: "Take a photo",
    uploadFromGallery: "Upload from gallery",
    removePhoto: "Remove photo",
    createGroup: "Create Group",
    groupTitle: "Group Title*",
    addMembers: "Add Members",
    create: "Create",
    shareInvite: "Share Invite",
    about: "About",
    events: "Play Dates",
    decline: "Decline",
    cancelRequest: "Cancel Request",
    oldPassword: "Old Password",
    newPassword: "New Password",
    chnagePassword: "Change Password",
    changeNumber: "Change Number",
    remove: "Remove",
    unblock: "Unblock",

    profileTab: "Profile",
    scheduleTab: "Schedule",
    activityTab: "Activity",
    petMapTab: "Pet Map",
    checkOut: "Check-Out",
    checkOutDescription: "Pet Love Day Care has requested to check out Daisy at 01:30 pm",
    approve: "Approves",
    selectPetType: "Select Pet Type",
    addPet: "Add Pet",
    addBreed: "Add Breed",
    uploadPicture: "Upload Picture",
    enterPetName: "Enter Pet Name",
    txtAdopation: "Add date of adoption with estimated year of birth if date of birth is not available",
    searchPeople: "Search people",
    addCoownerStr: "Are you sure you want to add Billy Green as a co-owner for your pet Daisy?",
    removeCoownerStr: "Are you sure you want to remove Amanda Lewis as a co-owner for your pet Daisy?",
    starting: "Starting..",
    miles: "Miles",
    dashBoard: "Dashboard",
    petList: "Pet List",
    scheduleTitle: "Enter Title",
    startDateAndTime: "Start Date & Time",
    endDateAndTime: "End Date & Time",
    repeat: "Repeat",
    reminder: "Reminder",
    notes: "Notes",
    repeatsOn: "Repeats On",
    ends: "Ends",
    lowBattary: "Low Battery",
    emailRegrex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/,
    passwordRegrex: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    welcome: "Welcome!",
    emptyGroupLabel: "Here are some of the cool things you can do on Tap-Pet. You can find friends, explore business or create groups and post in them.",
    groupDescription: "Group Description",
    mobilePlaceText: "Enter Mobile Number",
    audioErrorMsg: "audio file not supprot.",
    commentPlaceHolder: "Write Comment",
    postStr: "Post",
    eventSearchPlace: "Search for play dates",
    createEventNew: "Create a new play date ",
    createPost: "Create post",
    writeSomething: "Write Something...",
    confirmation: "Confirmation",
    alertMsg: "Are You Sure Selected Images Upload In Server?",
    dateTime: "Date and Time",
    getLocation: "Get Direction",
    eventHost: "Play Date Host",
    location: "Location",
    peopleJoined: "PEOPLE JOINED ",
    addMoreImg: "+ Add more photos",
    interested: "Interested",
    notGoing: "Not going",
    going: "Going",
    pending: "PENDING ",
    rateAndReview: "Rate and review",
    shareYourExpMsg: "Share your experience to help others",
    ratingAndReview: "Ratings and Reviews",
    peopleKey: "People",
    businessesKey: "Businesses",
    frdAndFollowing: "Friends and Following",
    mmmDDYYYYFormat: "MMMM DD, YYYY",
    hhmmaFormat: "hh:MM a",
    recent: "RECENT",
    groupAdmin: "Group Admin",
    noDesc: "No Description added yet",
    inviteMember: "+  Invite Members",
    writeComment: "Write comment",
    petFeed: "Pet Feed",
    likes: "Likes",
    direction: "Direction",
    stopSharing: "Stop Sharing",
    mapType: "satellite",
    addCollar: "Add Collar",
    deviceDetailMsg: "You can add a a collar by simply scanning for available device or by entering the NFC ID that you received with your device.",
    tapOnDevice: "Tap on Device",
    enterManually: "Enter Manually",
    allCowner: "ALL CO-OWNERS",
    addCoowner: "Add Co-owner",
    removeCoowner: "Remove Co-owner",
    coowner: "Co-owners",
    addNote: "Add Note",
    petProfile: "Pet Profile",
    customRecurrence: "Custom Recurrence",
    doneStr: "Done",
    recoverTxt: "You have successfully added your pet. Do you want to pair a collar with this pet?",
    doItLater: "Do it Later",
    percent: "Percent",
    myProfile: "My Profile",
    requestSent: "Request Sent",
    accountSetting: "Account Setting",
    blockedUsers: "Blocked Users",
    deviceLogged: "Devices Logged-In",
    notificationAlerts: "Notification Alerts",
    general:"General",
    petSpecific:"Pet Specific",
    petSpecificationDesc:"Turning this option off will turn notifications for all the calendar play dates. Do you want to proceed?",
    continue:"Continue",
    settings:"Settings",
    saveProcessed:"Save and Proceed",
    //Audio player screen string
    paused:"paused",
    Playback:"Playback",
    playing:"playing",

    //showmessage tilte and type
    error:"error",
    msgTag:"Message",
    success:"success",

    //call Alert msg
    sessionExpiredMsg:"Session expired please try a new call!",

    //comment screnn
    Comments:"Comments",


    //navigation route names
    AuthStack:"AuthStack",
    likeScreen:"likes",
    TabNavigation:"TabNavigation",
    commentsScreen:"comments",
    CreatePostScreen:"createPost",
    messageScreen:"message",
    addEventScreen:"addEvent",
    EventDetailUpcomingScreen:"EventDetailUpcoming",
    viewImagesScreen:"viewImages",
    EventDetailPastScreen:"EventDetailPast",
    serachPeopleScreen:"serachPeople",
    serachMessageScreen:"serachMessage",
    typeMessageScreen:"typeMessage",
    outgoingCallScreen:"outgoingCall",
    incomingCallScreen:"incomingCall",
    callScreen:"callScreen",
    viewPhotosScreen:"viewPhotos",
    historycallsDetailScreen:"historycallsDetail",
    UserProfileScreen:"UserProfile",
    EditProfileScreen:"EditProfile",
    notificationScreen:"notification",
    groupsScreen:"groups",
    createGroupScreen:"createGroup",
    viewGroupScreen:"viewGroup",
    chatMessageScreen:"chatMessage",
    mediaTabScreen:"mediaTab",
    friendsScreen:"friends",
    settingScreen:"setting",
    accountSettingScreen:"accountSetting",
    changePasswordScreen:"changePassword",
    deviceLoginScreen:"deviceLogin",
    blockUserScreen:"blockUser",
    PetProfileTabScreen:"PetProfileTab",
    addPetScreen:"addPet",
    addOwnerScreen:"addOwner",
    addCollarScreen:"addCollar",
    addCollarScanScreen:"addCollarScan",
    viewLocationScreen:"viewLocation",
    viewMapScreen:"viewMap",
    viewActivityScreen:"viewActivity",
    detailPageScreen:"detailPage",
    tapActivityScreen:"tapActivity",
    searchMapScreen:"searchMap",
    geoFenceScreen:"geoFence",
    profileCropScreen:"profileCrop",
    addScheduleScreen:"addSchedule",
    customScheduleScreen:"customSchedule",
    notificationAlertScreen:"notificationAlert",
    petSpecificScreen:"petSpecific",
    profileTabScreen:"profileTab",
    cutomHeaderScreen:"cutomHeaderScreen",
    addMemberScreen:"addMember",
    webViewScreen:"webViewScreen",
    playerScreen:"player",
    webScreen:"webScreen",
    splashScreen:"splash",
    loginScreen:"login",
    resetPasswordScreen:"resetPassword",
    signUpScreen:"signUp",
    verifyEmailScreen:"verifyEmail",
    createaccountScreen:"createaccount",
    uploadPhotoScreen:"uploadPhoto",
    userchatlist:"userchatlist",

    //pet user view
    scrollDown:"SCROLL DOWN",
    callOwner:"Call Owner",
    petFullOtherView:"petfullother",
    PetPageOther:"PetPageOther",
    FullImageView:"fullImageView",
    description:"description"
}