import { Platform } from "react-native";

export const Fonts={
    LobsterTwoRegular:Platform.OS=="ios"?"LobsterTwo":"LobsterTwo-Regular",
    LobsterTwoBold:"LobsterTwo-Bold",
    LobsterTwoBoldItalic:"LobsterTwo-BoldItalic",
    LobsterTwoItalic:"LobsterTwo-Italic",
    DMSansRegular:Platform.OS=="ios"?"DM Sans":"DMSans-Regular",
    DMSansBold:"DMSans-Bold",
    DMSansBoldItalic:"DMSans-BoldItalic"

}