export const Images = {
    emailIcon: require('../assets/Images/apple.png'),
    calenderIcon: require('../assets/Images/apple.png'),
    HomeTab: require('../assets/Images/home.png'),
    dropdown: require('../assets/Images/down_2.png'),
    profileIcon: require('../assets/Images/profileIcon.png'),
    editIcon: require('../assets/Images/editIcon.png'),
    serachIcon: require('../assets/Images/serachIcon.png'),
    backArrow: require("../assets/Images/arrowIcon.png"),
    cameraIcon: require("../assets/Images/camera.png"),
    doggyBedIcon: require("../assets/Images/DogIcon/doggyBedIcon.png"),
    dogIcon: require("../assets/Images/DogIcon/dogIcon.png"),
    dogIcon1: require("../assets/Images/DogIcon/dogIcon1.png"),
    dogIcon2: require("../assets/Images/DogIcon/dogIcon2.png"),
    dogIcon3: require("../assets/Images/DogIcon/dogIcon3.png"),
    starIcon:require("../assets/Images/starIcon.png"),
    placeIcon:require("../assets/Images/placeIcon.png"),
    rightArrow:require("../assets/Images/rightArrow.png"),
    notification:require('../assets/Images/notification.png'),
    backIcon: require('../assets/Images/arrowIcon.png'),
    reportIcon: require('../assets/Images/Report-button.png'),
    likeIcon: require('../assets/Images/like.png'),
    commentIcon: require('../assets/Images/comment.png'),
    filterIcon:require('../assets/Images/filterIcon.png'),
    menuSortIcon:require('../assets/Images/menuSortIcon.png'),
    closeIcon:require("../assets/Images/closeIcon.png"),
    people:require("../assets/Images/People.png"),  
    people1:require("../assets/Images/People1.png"),
    dogIcon5: require("../assets/Images/DogIcon/dogIcon5.png"),
    team:require("../assets/Images/team.png"),
    arrowright:require("../assets/Images/arrowright.png"),
    globe:require("../assets/Images/globe.png"),
    backGround:require("../assets/Images/background.png"),
    tickMark:require("../assets/Images/tickMark.png"),
    share:require("../assets/Images/share.png"),
    menuDot:require("../assets/Images/menuDot.png"),
    mapImage:require("../assets/Images/mapImage.png"),
    starIcon:require("../assets/Images/star.png"),
    deleteIcon:require("../assets/Images/delete.png"),
    heartIcon:require("../assets/Images/heart.png"),
    userIcon:require("../assets/Images/user.png"),
    incomingCall:require("../assets/Images/incomingCall.png"),
    incomingCallReceive:require("../assets/Images/call_incoming_receive.png"),
    outgoingCall:require("../assets/Images/outgoingCall.png"),
    telephone:require("../assets/Images/telephone.png"),
    blocked:require("../assets/Images/blocked.png"),
    history:require("../assets/Images/history.png"),
    addUser:require("../assets/Images/addUser.png"),
    volume:require("../assets/Images/volume.png"),
    mute:require("../assets/Images/mute.png"),
    upChevron:require("../assets/Images/upChevron.png"),
    downChevron:require("../assets/Images/downChevron.png"),
    bell:require("../assets/Images/bell.png"),
    logout:require("../assets/Images/logout.png"),
    document:require("../assets/Images/document.png"),
    mic:require("../assets/Images/mic.png"),

    pet:require("../assets/Images/pet.png"),
    petProfileTab:require("../assets/Images/petProfile.png"),
    calendarTab:require("../assets/Images/calendar.png"),
    locationTab:require("../assets/Images/location.png"),
    setting:require("../assets/Images/settings.png"),
    cat:require("../assets/Images/cat.png"),
    rabbit:require("../assets/Images/rabbit.png"),
    instragram:require("../assets/Images/instagram.png"),
    clock:require("../assets/Images/clock.png"),
    scanner:require("../assets/Images/scanner.png"),
    collar:require("../assets/Images/collar.png"),
    smartphone:require("../assets/Images/smartphone.png"),
    fire:require("../assets/Images/fire.png"),
    flag:require("../assets/Images/flag.png"),
    dashBoard:require("../assets/Images/dashboard.png"),
    likeHeart:require("../assets/Images/likeHeart.png"),
    lowBattery:require("../assets/Images/lowBattery.png"),
    contact:require("../assets/Images/contact.png"),
    viewPhoto:require("../assets/Images/viewPhoto.png"),
    battery:require("../assets/Images/Battery.png"),
    addFriend:require("../assets/Images/addFriend.png"),
    call:require("../assets/Images/Call.png"),
    group:require("../assets/Images/Group.png"),
    addPicture:require("../assets/Images/addPicture.png"),
    Members:require("../assets/Images/Members.png"),
    petIcon:require("../assets/Images/PetIcon.png"),
    fillStar:require("../assets/Images/fillStar.png"),
    checkOut:require("../assets/Images/checkOut.png"),
    petProfileUn:require("../assets/Images/petProfileUn.png"),
    chart:require("../assets/Images/chart.png"),
    chartSelect:require("../assets/Images/chartSelect.png"),
    calendarSelect:require("../assets/Images/calendarSelect.png"),
    locationSelect:require("../assets/Images/locationSelect.png"),
    addEventIcon:require("../assets/Images/addEventIcon.png"),
    helpIcon:require("../assets/Images/Help.png"),
    lockIcon:require("../assets/Images/Lock.png"),
    phoneIcon:require("../assets/Images/Phone.png"),

    Grooming:require("../assets/Images/Grooming.png"),
    GroupIcon:require("../assets/Images/GroupIcon.png"),
    PetSitter:require("../assets/Images/PetSitter.png"),
    PetStore:require("../assets/Images/PetStore.png"),
    PetTrainer:require("../assets/Images/PetTrainer.png"),
    PetWalker:require("../assets/Images/PetWalker.png"),
    Boarding:require("../assets/Images/Boarding.png"),
    marker:require("../assets/Images/marker.png"),
    titleIcon:require("../assets/Images/titleIcon.png"),
    unionIcon:require("../assets/Images/Union.png"),
    bedIcon:require("../assets/Images/DogIcon/bedIcon.png"),
    downloadfile:require("../assets/Images/downloadfile.png"),
    playerIcon:require("../assets/Images/player.png"),
    sendervideocall:require("../assets/Images/sendervideocall.png"),
    unfill:require("../assets/Images/unFill.png"),
    Checkin:require("../assets/Images/Checkin.png"),
    Activity:require("../assets/Images/Activity.png"),
    userIcon1:require("../assets/Images/userIcon.png"),
    plusIcon:require("../assets/Images/Add.png"),
    garbage:require("../assets/Images/garbage.png"),
    arrowDownCircle:require("../assets/Images/arrow_down_circle.png")

}