export const Colors = {
    white : '#FFFFFF',
    black : '#000000',
    screenBackgroundColor:['#0C0024', '#0C0024'],
    btnBackgroundColor:['#DD46F6', '#7235FF'],
    themeColor:'#EA8FFA',
    dialogBackground:'#2C1F47',
    theme : '#2C1F47',
    primaryViolet : '#EA8FFA',
    grey:"#808080",
    violate : '#1D1233',
    backgroundColor : '#0C0024',
    transparent:"#ffffff00",
    opacityColor:'#FFFFFF26',
    opacityColor2:'#FFFFFF33',
    green:'#20BB62',
    red:"#FF3B30",
    opacityColor3: '#FFFFFF90',
    eventBox :'#4F0792',
    progressBackground:"#0C0024",
    viewGroupContain:['rgba(0, 0, 0, 0)','rgba(0, 0, 0, 0.7)'],
    viewCare:['rgba(0, 0, 0, 0)','rgba(0, 0, 0, 0.8)'],
    roundBackground:'#3D3350',
    viewCareTop:['rgba(0, 0, 0, 0.7)','rgba(0, 0, 0, 0)'],
    redColor:'#FF3B30'

 //   viewGroupContain:['#B2000000','#0 0000000']
}