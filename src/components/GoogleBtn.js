import React, { useState, useEffect } from 'react';
import { Keyboard, Text, View, StyleSheet, Dimensions, TouchableOpacity, Platform } from 'react-native';
import { Fonts } from '../Resources/Fonts';
import { GoogleIcon } from '../utils/svg/GoogleIcon';
import {
    GoogleSignin,
} from '@react-native-google-signin/google-signin';
import { comeChatUserTag, comeChat_apiKey, IOSCLIENTID } from '../utils/Constant';
import { apiCall, comeChatLogin, getFirebaseToken, storeChatUserData, storeData } from '../utils/helper';
import { apiUrl } from '../Redux/services/apiUrl';
import { CometChat } from "@cometchat-pro/react-native-chat"
import Toast from 'react-native-toast-message';
import { strings } from '../Resources/Strings';

const { width, height } = Dimensions;
const GoogleBtn = (props) => {
    const [device_token, setDeviceToken] = useState("");

    useEffect(() => {
        _configureGoogleSignIn()
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
    }, [])
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const _configureGoogleSignIn = () => {
        GoogleSignin.configure({
            iosClientId: IOSCLIENTID,
            offlineAccess: false,
            webClientId: "333823282922-6gepu2m2t41mln5p74q0uvol17dp2hkl.apps.googleusercontent.com"
        });
    }
    const signOut = async () => {
        Keyboard.dismiss()
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            signInGoogle()
        } catch (error) {
            signInGoogle()
            console.error(error);
        }
    };

    const signInGoogle = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            callApi(userInfo)
        } catch (error) {
            showMessage(strings.error, strings.msgTag, error.toString())
        }
    };
    const callApi = async (userInfo) => {
        const obj = {
            u_email: userInfo.user.email,
            u_password: '',
            u_user_type: '3',
            u_social_id: userInfo.user.id,
            device_token: device_token,
            device_type: Platform.OS == "ios" ? "ios" : "android",

        }
        await apiCall(obj, props.flag == "login" ? apiUrl.register : apiUrl.register).then(res => {
            if (res.status) {
                
                if (props.flag == "login") {
                    comeChatLogin(res.result.u_id).then(data => {
                        if (data.status == true) {
                            storeData(res.result)
                            storeChatUserData(data.response)
                            props.navigation.replace(strings.TabNavigation)

                        }
                        else {
                            createUserComeChat(res.result, userInfo, "login");
                        }
                    })
                }
                else {
                    createUserComeChat(res.result, userInfo, "register");
                }

              //  showMessage(strings.success, strings.msgTag, res.message)
            }
            else {
                if (res.code == 1000||(res.code == 2000)) {
                    storeData(res.result)
                    props.navigation.navigate(strings.verifyEmailScreen, { isCall: true });
                }
                // else if (res.code == 2000) {
                //     console.log(res.result)
                //     storeData(res.result)
                //     setTimeout(() => {
                //         props.navigation.navigate("createaccount");
                //     }, 400);
                // }
                showMessage(strings.error, strings.msgTag,  res.message)
            }

        })
    }
    const createUserComeChat = (data, userInfo, flag) => {
        var uid = comeChatUserTag + data.u_id;
        var user = new CometChat.User(uid);
        user.setName(userInfo.user?.name);

        CometChat.createUser(user, comeChat_apiKey).then(
            user => {
                storeData(data)
                storeChatUserData(user)
                if (flag != "login")
                    props.navigation.navigate(strings.createaccountScreen, { userInfo: userInfo })
                else
                    props.navigation.replace(strings.TabNavigation)


            }, error => {
                console.log("error", error);
            }
        )
    }
    return (
        <TouchableOpacity onPress={signOut} style={{ marginHorizontal: 10, marginTop: 20 }}  >
            <View style={styles.googleView}>
                <View style={{ marginRight: 10 }}>
                    <GoogleIcon width={20} height={20} />
                </View>
                <Text style={styles.textlabel}>Signin with Google </Text>
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    googleView: {
        borderWidth: 1,
        borderColor: '#fff',
        height: 44,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    callIcon: {
        width: 20,
        height: 20,
        marginRight: 10
    },

    textlabel: {
        color: 'white',
        textAlign: 'center',
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular
    }
})
export default GoogleBtn;