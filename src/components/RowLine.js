import React from 'react';
import { View, Text,StyleSheet } from 'react-native';
import { Fonts } from '../Resources/Fonts';

const RowLine = () => {
    return (
        <View style={styles.mainiew}>
            <View style={styles.line}></View>
            <Text style={styles.txtOR}>OR</Text>
            <View style={styles.line}></View>

        </View>
    )
}
export default RowLine;
const styles=StyleSheet.create({
    txtOR: {
        color: "white",
        flex: 0.1,
        textAlign: "center",
        fontSize: 12,
        lineHeight: 15.62,
        fontWeight: "700",
        fontFamily:Fonts.DMSansRegular
    },
    mainiew:{
        flexDirection: "row", flex: 1, marginHorizontal: 15, marginTop: 20 
    },
    line:{ height: 1, backgroundColor: "#FFFFFF", flex: 0.45, alignSelf: "center" }
})