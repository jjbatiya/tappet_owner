import React from 'react';
import { TouchableOpacity, Text, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
const CommonButton = (props) => {
    return (
        <TouchableOpacity style={props.viewStyle}
            onPress={() => props.onPress()}
        >
            <LinearGradient
                colors={props.colors}
                style={props.btnlinearGradient}
            >
                {props.image && <Image style={props.imageStyle} source={props.image}></Image>}
                <Text style={props.btnText}>{props.text}</Text>
            </LinearGradient>
        </TouchableOpacity>
    )
}
export default CommonButton;