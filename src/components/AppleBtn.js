import React, { useState, useEffect } from 'react';
import { Image, Text, View, StyleSheet, Dimensions, TouchableOpacity, Platform } from 'react-native';
import { Fonts } from '../Resources/Fonts';
import { AppleIcon } from '../utils/svg/AppleIcon';
const { width, height } = Dimensions;
import { appleAuth } from '@invertase/react-native-apple-authentication';
import { apiCall, comeChatLogin, getFirebaseToken, storeChatUserData, storeData } from '../utils/helper';
import Toast from 'react-native-simple-toast';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { apiUrl } from '../Redux/services/apiUrl';
import { comeChatUserTag, comeChat_apiKey } from '../utils/Constant';
import { strings } from '../Resources/Strings';

const AppleBtn = (props) => {
    const [device_token, setDeviceToken] = useState("");
    useEffect(() => {
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
    }, [])
    const clickSignIn = () => {
        if (appleAuth.isSupported) {

            appleAuthRequestResponse = appleAuth.performRequest({
                requestedOperation: appleAuth.Operation.LOGIN,
                requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
            }).then(appleAuthRequestResponse => {
                let { identityToken, email, fullName, user } = appleAuthRequestResponse
                callApi(email, fullName.givenName, user);

            });


            // get current authentication state for user
            // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
            //  const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
            //console.log(credentialState)
            // use credentialState response to ensure the user is authenticated
            //if (credentialState === appleAuth.State.AUTHORIZED) {
            // user is authenticated
            //}
        }
        else {
            Toast.show("AppleAuth is not supported on the device. Currently Apple Authentication works on iOS devices running iOS 13 or later. Use 'AppleAuth.isSupported' to check device is supported before calling any of the module methods.", Toast.SHORT);

        }
    }

    const callApi = async (email, fullName, user) => {
        const userInfo = {
            email: email, fullName: fullName
        }
        const obj = {
            u_email: email,
            u_password: '',
            u_user_type: '2',
            u_social_id: user,
            device_token: device_token,
            device_type: Platform.OS == "ios" ? "ios" : "android",

        }
        await apiCall(obj, props.flag == "login" ? apiUrl.register : apiUrl.register).then(res => {
            if (res.status) {
                if (props.flag == "login") {
                    comeChatLogin(res.result.u_id).then(data => {
                        if (data.status == true) {
                            storeData(res.result)
                            storeChatUserData(data.response)
                            props.navigation.replace(strings.TabNavigation)

                        }
                        else {
                            createUserComeChat(res.result, userInfo, "login");
                        }
                    })
                }
                else {
                    createUserComeChat(res.result, userInfo, "register");
                }

             //   Toast.show(res.message, Toast.LONG);

            }
            else {
                Toast.show(res.message, Toast.LONG);

                if (res.code == 1000 || res.code == 2000) {
                    storeData(res.result)
                    props.navigation.navigate(strings.verifyEmailScreen, { isCall: true });
                }
                // else if (res.code == 2000) {
                //     storeData(res.result)
                //     setTimeout(() => {
                //         props.navigation.navigate("createaccount");
                //     }, 400);
                // }
            }

        })
    }
    const createUserComeChat = (data, userInfo, flag) => {
        var uid = comeChatUserTag + data.u_id;
        var user = new CometChat.User(uid);
        if (userInfo.fullName != null)
            user.setName(userInfo.fullName);
        else
            user.setName(data.u_first_name)

            CometChat.createUser(user, comeChat_apiKey).then(
                user => {
                    storeData(data)
                    storeChatUserData(user)
                    if (flag != "login")
                        props.navigation.navigate(strings.createaccountScreen, { userInfo: userInfo })
                    else
                        props.navigation.replace(strings.TabNavigation)


                }, error => {
                    console.log("error123", error);
                }
            )
    }
    return (
        <TouchableOpacity onPress={() => clickSignIn()} style={{ marginHorizontal: 10, marginTop: 20 }}  >
            <View style={styles.googleView}>
                <View style={{ marginRight: 10 }}>
                    <AppleIcon width={20} height={20} />
                </View>
                <Text style={styles.textlabel}>Signin with Apple </Text>
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    googleView: {
        borderWidth: 1,
        borderColor: '#fff',
        height: 44,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    callIcon: {
        width: 20,
        height: 20,
        marginRight: 10
    },

    textlabel: {
        color: 'white',
        textAlign: 'center',
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular
    }
})
export default AppleBtn;