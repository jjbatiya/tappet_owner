import { StyleSheet,Dimensions } from 'react-native'
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../Resources/Colors';
import { Fonts } from '../Resources/Fonts';
import { ThemeUtils, Color } from "./utils";
const {width,height}=Dimensions.get("window")
const HEADER_IMAGE_HEIGHT = ThemeUtils.relativeHeight(30);
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    /*header style*/
    headerLeftIcon: {
        position: 'absolute',
        left: ThemeUtils.relativeWidth(2),
        flexDirection: "row"
    },
    headerRightIcon: {
        position: 'absolute',
        right: ThemeUtils.relativeWidth(2),
    },
    headerStyle: {
        height: ThemeUtils.APPBAR_HEIGHT,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 200,
    },
    headerTitle: {
        textAlign: 'center',
        justifyContent: 'center',
        color: Color.HEADER_TEXT_COLOR,
        fontSize: ThemeUtils.fontNormal,
        fontFamily: Fonts.DMSansRegular
    },
    /*Top Image Style*/
    headerImageStyle: {
        height: HEADER_IMAGE_HEIGHT,
        width: '100%',
        top: 0,
        alignSelf: 'center',
        position: 'absolute'
    },
    /*profile image style*/
    profileImage: {
        position: 'absolute',
        zIndex: 100,
    },
    /*profile title style*/
    // profileTitle: {

    //     textAlign: 'center',
    //     color: Color.BLACK,
    //     top: ThemeUtils.relativeHeight(35),
    //     left: 0,
    //     right: 0,
    //     fontSize: ThemeUtils.fontXLarge
    // },
    profileTitle: {
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular,
        fontSize: 28, textAlign: "center", alignSelf: "center",
        lineHeight: 35, letterSpacing: 0.2, color: Colors.white,
        position: 'absolute',
        zIndex: 100,
        top: ThemeUtils.relativeHeight(27),
        left: 0,
        right: 0,
    },
    /*song count text style*/
    songCountStyle: {
        position: 'absolute',
        textAlign: 'center',
        fontWeight: '400',
        top: ThemeUtils.relativeHeight(37),
        left: 0,
        right: 0,
        fontSize: ThemeUtils.fontNormal,
    },
    artistCardContainerStyle: {
        backgroundColor: Color.CARD_BG_COLOR,
        elevation: 5,
        shadowRadius: 3,
        shadowOffset: {
            width: 3,
            height: 3
        },
        padding: 15,
        marginVertical: ThemeUtils.relativeWidth(1),
        marginHorizontal: ThemeUtils.relativeWidth(2),
        flexDirection: 'row',
        alignItems: 'center'
    },
    artistImage: {
        height: ThemeUtils.relativeWidth(15),
        width: ThemeUtils.relativeWidth(15),
        borderRadius: ThemeUtils.relativeWidth(7.5)
    },
    songTitleStyle: {
        fontSize: ThemeUtils.fontNormal,
        color: Color.BLACK
    },
    cardTextContainer: {
        flex: 1,
        margin: ThemeUtils.relativeWidth(3)
    },

    //button design
    btnStyle: {
        position: 'absolute',
        zIndex: 100,

        top: ThemeUtils.relativeHeight(35),
        left: 0,
        right: 0,
        fontSize: ThemeUtils.fontXLarge,
        backgroundColor: "pink"
    },
    //left icon 
    touchbleView: { justifyContent: "center", alignItems: "center" },
    backGround: { height: 32, width: 32, backgroundColor: "white", opacity: 0.2, borderRadius: 20 },
    imgBackIcon: { height: 12, width: 12, alignSelf: "center", position: "absolute", tintColor: "white" },
    viewRatingScroll: { flexDirection: "row", alignItems: "center", marginLeft: 12 },
    txtRatingScroll: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        fontSize: 10, lineHeight: 13.02, color: Colors.white
    },
    txtRating: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "500", textAlign: "center", opacity: 0.6,
        fontSize: 14, lineHeight: 18.23, color: Colors.white
    },

    viewRating: {
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular,
        fontSize: 28, textAlign: "center", alignSelf: "center",
        lineHeight: 35, letterSpacing: 0.2, color: Colors.white,
        zIndex: 100,
        flexDirection: "row",
        position: 'absolute',
        alignItems: "center", justifyContent: "center",
        top: ThemeUtils.relativeHeight(34),
        left: 0,
        right: 0,
    },

    //followo button
    followoView: {
        zIndex: 100,
        position: 'absolute',
        top: ThemeUtils.relativeHeight(37),
        width:'100%',
    },
    txtFollow: {
        fontSize: 14, lineHeight: 18.23, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular, color: Colors.white,
        textAlign: "center", alignSelf: "center", marginLeft: responsiveWidth(2)
    },
    imgPetIcon: { height: 15, width: 15, tintColor: Colors.white, alignSelf: "center" },
    viewBtnFollow: { flexDirection: "row", alignItems: "center", justifyContent: "center", flex: 1 },
    //category view
    txtCategory: {
        fontWeight: "500", opacity: 0.6,
        fontFamily: Fonts.DMSansRegular, fontSize: 12, lineHeight: 15.62, color: Colors.white
    },
    txtSelectCategory: {
        fontWeight: "500",
        fontFamily: Fonts.DMSansRegular, fontSize: 12, lineHeight: 15.62, color: Colors.white
    },
    viewLine: {
        flex: 1,
        height: 1,
        backgroundColor: Colors.white,
        marginTop: 10
    },
    viewHorizontalLine: { height: 1, backgroundColor: Colors.white, opacity: 0.15 },




      //overView
      viewOverView: {
        flexDirection: "row",
    },
    imgIcon: {
        height: 17, width: 17,
        tintColor: Colors.white
    },
    txtDetail: {
        fontSize: 14, fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, color: Colors.white, marginLeft: responsiveWidth(4)
    },
    txtLink: {
        fontSize: 14, fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, color: Colors.primaryViolet, marginLeft: responsiveWidth(4)
    },
    //rating
    txtRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 16, lineHeight: 22, textAlign: "center", color: Colors.white,
        marginTop: 5
    },
    txtLabelRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "400",
        fontSize: 14, lineHeight: 22, textAlign: "center", color: Colors.white,
        opacity: 0.6
    },
    ratingView: { alignSelf: "center", alignItems: "center", justifyContent: "center", },
    imgPeople: { height: 48, width: 48, borderRadius: 24 },
    txtRateReviewLabel: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 14, lineHeight: 18.23, textAlign: "center", color: Colors.white,

    },
    imgRight: {
        height: 12, width: 12, marginLeft: 7,
        tintColor: Colors.primaryViolet, alignSelf: "center"
    },
    txtRatingReview: {
        fontFamily: Fonts.DMSansRegular, fontWeight: "400",
        fontSize: 26, lineHeight: 33.54, color: Colors.white,
        marginTop: 10
    },
    txtTotalRating: {
        fontFamily: Fonts.DMSansRegular, fontWeight: "400",
        fontSize: 10, lineHeight: 13.02, color: Colors.white,
        marginTop: 3, opacity: 0.7
    },
    viewLine1: { height: 1, flex: 1, backgroundColor: Colors.white, opacity: 0.1, marginVertical: 15 },
    renderView: {
        backgroundColor: Colors.theme, width: responsiveWidth(55),
        height: 112, borderRadius: 4, marginRight: 10
    },
    txtName:{
        fontFamily: Fonts.DMSansRegular, fontWeight: "700",
        fontSize: 10, lineHeight: 13.02, color: Colors.white,
        marginTop: 3, opacity: 0.7
    },
    txtTime:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:10,lineHeight:13.02,opacity:0.4
    },
    txtItemRate:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:12,lineHeight:15.62,
        margin:10
    },
    //event view
    roundView:{
        position: "absolute", bottom: -7,
        right: -7, height: 20, width: 20,
        borderWidth:1,borderColor:'#0C0024',
        backgroundColor:Colors.white,
        borderRadius:10,alignItems:"center",justifyContent:"center"
    },
    eventIcon:{height:10,width:10,tintColor:Colors.primaryViolet},
    txtEventName:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:14,lineHeight:18.23,letterSpacing:0.1
        
    },
    txtEventTime:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:12,lineHeight:15.62,letterSpacing:0.1,opacity:0.6
    },
    //followers view
    roundViewFollowers: {
        height: 32, width: 32, backgroundColor: Colors.theme, alignItems: "center",
        justifyContent: "center", borderRadius: 20,

    },
    imgView: { height: 32, width: 32, borderRadius: 20 },
    txtMore: {
        fontSize: 9,
        fontWeight: "700",
        lineHeight: 11.72,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    //modal view
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.backgroundColor,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,

    },
    input: {
        borderWidth: 0,
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        width:width-50,
        height:60
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular,
    },
    
})