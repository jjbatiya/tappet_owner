import React, { Component } from 'react';
/*Components*/
import { Animated, View, StatusBar, Text, Image, Platform, StyleSheet, Linking, TouchableOpacity, FlatList, Modal } from 'react-native';
import MaterialAnimatedView from './component/MaterialAnimatedView';
/*utils*/
import styles from './style';
import { ThemeUtils, Color } from './utils';
/*Data*/
import { Images } from '../Resources/Images';

import artistData from './data/SongData';
import ProfileTab from '../screens/App/ExploreScreen/ProfileCrop/ProfileTab';

const ARTIST_NAME = 'Doggy Bed Day Care';
import { Rating } from 'react-native-ratings';
import { Colors } from '../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { EventIcon } from '../utils/svg/EventIcon';
import PercentageBar from '../Components/PercentageBar';
import CommonButton from '../Components/CommonButton';
var FloatingLabel = require('react-native-floating-labels');

const categoryData = [
    {
        id: 1,
        title: "Overview"
    },
    {
        id: 2,
        title: "Ratings"
    },
    {
        id: 3,
        title: "Events"
    },
    {
        id: 4,
        title: "Followers"
    },
    {
        id: 5,
        title: "Photos"
    }
]
const ratingData = [
    {
        id: 1,
        percentage: "70%",
        value: 5
    },
    {
        id: 2,
        percentage: "15%",
        value: 4
    },
    {
        id: 3,
        percentage: "10%",
        value: 3
    },
    {
        id: 4,
        percentage: "13%",
        value: 2
    },
    {
        id: 5,
        percentage: "20%",
        value: 1
    }
]
const ratingReviewData = [
    {
        id: 1,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    },
    {
        id: 2,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    },
    {
        id: 3,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    }
]
const eventData = [
    {
        id: 1,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 2,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 3,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 4,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 5,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 6,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    }
]
const imgPersons = [
    { image: Images.people, id: 1 },
    { image: Images.people1, id: 2 },
    { image: Images.people, id: 3 },
    { image: Images.people1, id: 4 },
    { image: Images.people, id: 5 },
    { image: Images.people1, id: 6 },
    { image: Images.people, id: 7 },
    { image: Images.people1, id: 8 },
    { image: Images.people, id: 9 },
    { image: Images.people1, id: 10 },
    { image: Images.people, id: 11 },
    { image: Images.people1, id: 12 },
    { image: Images.people, id: 13 },
    { image: Images.people1, id: 14 },
    { image: Images.people, id: 15 },
    { image: Images.people1, id: 16 },
    { image: Images.people, id: 17 },
    { image: Images.people1, id: 18 },
]
const photoData = [
    {
        key: "dog1",
        image: Images.dogIcon
    },
    {
        key: "dog2",
        image: Images.dogIcon1
    },
    {
        key: "dog3",
        image: Images.dogIcon2
    },
    {
        key: "dog3",
        image: Images.dogIcon3
    },
    {
        key: "dog4",
        image: Images.dogIcon1
    },
    {
        key: "dog5",
        image: Images.dogIcon2
    },
    {
        key: "dog6",
        image: Images.dogIcon3
    }
]

export default class CutomHeaderScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0),
            selectItem: categoryData[0],
            selectedIndex: 0,
            viewHeights: [0, 0, 0, 0, 0, 0],
            isRatingModal: false,
            ratingValue:0
        };
        this.scrollView = React.createRef();
        this.flatList = React.createRef();


    }

    openLink = (url) => {
        Linking.canOpenURL(url)
            .then((supported) => {
                if (!supported) {
                    console.log('Can\'t handle url: ' + url);
                } else {
                    return Linking.openURL(url);
                }
            })
            .catch((err) => console.error('An error occurred', err));
    };

    renderArtistCard = (index, item) => {
        return (
            <MaterialAnimatedView key={index.toString()} index={index}>
                <TouchableOpacity activeOpacity={0.8} style={styles.artistCardContainerStyle}
                    onPress={() => this.openLink(item.songLink)}>
                    <Image source={{ uri: item.artistImage }} style={styles.artistImage} />
                    <View style={styles.cardTextContainer}>
                        <Text numberOfLines={1} style={styles.songTitleStyle}>{item.songName}</Text>
                        <Text numberOfLines={1}>{item.albumName}</Text>
                    </View>
                </TouchableOpacity>
            </MaterialAnimatedView>
        );
    };

    //For header background color from transparent to header color
    _getHeaderBackgroundColor = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: ['rgba(0,0,0,0.0)', Color.HEADER_COLOR],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //For header image opacity
    _getHeaderImageOpacity = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [1, 0],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image position from left
    _getImageLeftPosition = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 80, 140],
            outputRange: [ThemeUtils.relativeWidth(30), ThemeUtils.relativeWidth(38), ThemeUtils.relativeWidth(10)],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image position from top
    _getImageTopPosition = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [ThemeUtils.relativeHeight(20), Platform.OS === 'ios' ? 8 : 10],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image width
    _getImageWidth = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [ThemeUtils.relativeWidth(40), ThemeUtils.APPBAR_HEIGHT - 20],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image height
    _getImageHeight = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [ThemeUtils.relativeWidth(40), ThemeUtils.APPBAR_HEIGHT - 20],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image border width
    _getImageBorderWidth = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [StyleSheet.hairlineWidth * 3, StyleSheet.hairlineWidth],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist profile image border color
    _getImageBorderColor = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 140],
            outputRange: [Color.CARD_BG_COLOR, 'rgba(0,0,0,0.0)'],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //Song list container position from top
    _getListViewTopPosition = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 250],
            outputRange: [ThemeUtils.relativeWidth(125) - ThemeUtils.APPBAR_HEIGHT - 10, 0],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //header title opacity
    _getHeaderTitleOpacity = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 20, 50],
            outputRange: [0, 0.5, 1],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });
    };

    //artist name opacity
    _getNormalTitleOpacity = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 20, 50],
            outputRange: [1, 0.5, 0],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });

    };
    getBackBtnOpacity = () => {
        const { scrollY } = this.state;

        return scrollY.interpolate({
            inputRange: [0, 20, 50],
            outputRange: [0.2, 0, 0],
            extrapolate: 'clamp',
            useNativeDriver: true,
        });

    };
    categoryView = () => {
        return (
            <View style={{ height: 38 }}>
                <FlatList
                    data={categoryData}
                    horizontal
                    style={{ marginHorizontal: 20, marginTop: 10 }}
                    renderItem={this.renderCategory}
                >
                </FlatList>
                <View style={styles.viewHorizontalLine}></View>
            </View>
        )
    }
    onCategoryClick = (index, item) => {
        this.setState({
            selectItem: item,
            selectedIndex: index
        })
        this.scrollView.scrollTo({ x: 0, y: this.state.viewHeights[index] * index, animated: true })
    }
    renderCategory = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    this.onCategoryClick(index, item)
                }
                style={{ marginRight: 20 }}>
                <Text style={this.state.selectItem?.id == item?.id ? styles.txtSelectCategory : styles.txtCategory}>{item.title}</Text>
                {this.state.selectItem?.id == item?.id &&
                    <View style={styles.viewLine} />
                }
            </TouchableOpacity>
        )
    }
    overView = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", padding: 20 }} onLayout={(event) => this.onLayout(event, 0)}>
                <View style={styles.viewOverView}>
                    <Image style={styles.imgIcon} source={Images.petIcon}></Image>
                    <Text style={styles.txtDetail}>{"Pet Boarding • Day Care • Pet Sitter"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={[styles.imgIcon]} source={Images.telephone}></Image>
                    <Text style={styles.txtDetail}>{"+164 6630 8679 \n+164 6540 1780"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.globe}></Image>
                    <Text style={styles.txtLink}>{"www.doggybed.com"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={[styles.imgIcon, { tintColor: Colors.opacityColor3 }]} source={Images.clock}></Image>
                    <Text style={styles.txtDetail}>{"Open 24 Hours"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.placeIcon}></Image>
                    <View>
                        <Text style={styles.txtDetail}>{"234 Richmonf Tower, Opposite Star Bucks, Park Avenue, New York"}</Text>
                        <Text style={styles.txtLink}>{"View Direction"}</Text>
                    </View>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.document}></Image>
                    <Text style={styles.txtDetail}>{`All the fun stuff happening at B4 Apartment. Guys feel free to post anything and everything. Happy to share all the pet related and maintenance stuff. BTW monthlly outing to be held every last saturday. Cheers 🍺`}</Text>
                </View>
            </View>
        )
    }
    ratingView = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", marginVertical: 10, paddingVertical: 20 }} onLayout={(event) => this.onLayout(event, 1)}>

                <View style={styles.ratingView}>
                    <Image style={styles.imgPeople} source={Images.people}></Image>
                    <Text style={styles.txtRate}>{"Rate and review"}</Text>
                    <Text style={styles.txtLabelRate}>{"Share your experience to help others"}</Text>
                    <Rating
                        type='custom'
                        ratingCount={5}
                        imageSize={25}
                        isDisabled={true}
                        onFinishRating={(value) => this.setState({ isRatingModal: true,ratingValue:value })}
                        startingValue={this.state.ratingValue}
                        style={{ backgroundColor: Colors.opacityColor, marginTop: 5 }}
                        ratingBackgroundColor={Colors.opacityColor3}
                        ratingColor={"#FFB91B"}
                        tintColor={Colors.theme}
                    />
                </View>

            </View>
        )
    }
    ratingReview = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", marginBottom: 10, padding: 20 }} onLayout={(event) => this.onLayout(event, 2)}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.txtRateReviewLabel}>{"Ratings and Reviews"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{"View All"}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flex: 0.2, marginTop: 5 }}>
                        <Text style={styles.txtRatingReview}>{"4.4"}</Text>
                        <Rating
                            type='custom'
                            ratingCount={5}
                            imageSize={8}
                            startingValue={4.5}
                            style={{ backgroundColor: Colors.opacityColor, alignSelf: "flex-start", marginVertical: 5 }}
                            ratingBackgroundColor={Colors.opacityColor3}
                            ratingColor={"#FFB91B"}
                            tintColor={Colors.theme}
                        />
                        <Text style={styles.txtTotalRating}>{"1,24,954"}</Text>
                    </View>
                    <View style={{ flex: 0.8, marginTop: 20, }}>
                        {ratingData.map((item, index) => {
                            return (
                                <View keyExtractor={index} style={{ flexDirection: "row", flex: 1, }}>
                                    <Text style={[styles.txtTotalRating, { flex: 0.05, marginTop: 0 }]}>{item.value}</Text>
                                    <PercentageBar
                                        height={6}
                                        backgroundColor={Colors.opacityColor2}
                                        completedColor={Colors.primaryViolet}
                                        percentage={item.percentage}
                                    />
                                </View>
                            )
                        })}

                    </View>
                </View>
                <View style={styles.viewLine1}></View>
                <FlatList
                    data={ratingReviewData}
                    renderItem={this.renderReview}
                    horizontal
                    initialScrollIndex={2}
                    showsHorizontalScrollIndicator={false}
                >

                </FlatList>

            </View>
        )
    }
    renderReview = ({ item, index }) => {
        return (
            <View style={styles.renderView}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", margin: 10 }}>
                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                        <Image style={{ height: 24, width: 24, borderRadius: 12, alignSelf: "center" }} source={item.image}></Image>
                        <View style={{ marginLeft: 10 }}>
                            <Text style={styles.txtName}>{item.name}</Text>
                            <Rating
                                type='custom'
                                ratingCount={5}
                                imageSize={8}
                                startingValue={3}
                                style={{ backgroundColor: Colors.opacityColor, alignSelf: "flex-start", marginTop: 0 }}
                                ratingBackgroundColor={Colors.opacityColor2}
                                ratingColor={Colors.white}
                                tintColor={Colors.theme}
                            />
                        </View>
                    </View>
                    <Text style={styles.txtTime}>{item.time}</Text>
                </View>
                <Text style={styles.txtItemRate}>{item.review}</Text>
            </View>
        )
    }
    eventView = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", marginBottom: 10, padding: 20 }} onLayout={(event) => this.onLayout(event, 3)}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Events (02)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{"View All"}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                {
                    eventData.map((item, index) => {
                        return (
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <View>
                                    <Image style={{ height: 48, width: 48, borderRadius: 4 }} source={item.image}></Image>
                                    <View style={styles.roundView}>
                                        <EventIcon fillColor={Colors.primaryViolet} width={10} height={10} />

                                        {/* <Image style={styles.eventIcon} source={Images.placeIcon}></Image> */}
                                    </View>
                                </View>
                                <View style={{ marginLeft: 20, alignSelf: "center" }}>
                                    <Text style={styles.txtEventName}>{item.eventName}</Text>
                                    <Text style={styles.txtEventTime}>{item.evnetTime}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
    followersView = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", marginBottom: 10, padding: 20 }} onLayout={(event) => this.onLayout(event, 4)}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Followers (21)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{"View All"}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                <FlatList
                    key={'_'}
                    data={imgPersons}
                    style={{ height: 150, marginTop: 20 }}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity key={index} style={{ margin: 6, flexDirection: "row" }}>
                                {index != imgPersons.length - 1 ? <Image style={styles.imgView} source={item.image}></Image>

                                    : <TouchableOpacity style={styles.roundViewFollowers}>
                                        <Text style={styles.txtMore}>{"+5"}</Text>
                                    </TouchableOpacity>
                                }
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    viewImage = ({ item, index }) => {
        return (
            <View key={index}>
                {index % 2 == 0 ?
                    <View>
                        <TouchableOpacity>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={item.image}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image style={{ width: 105, height: 77 }} source={item.image}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={item.image}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }
    photoView = () => {
        return (
            <View style={{ backgroundColor: "#1D1233", marginBottom: 10, padding: 20 }} onLayout={(event) => this.onLayout(event, 5)}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Photos (24)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{"View All"}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    data={photoData}
                    key={'#'}
                    keyExtractor={item => "#" + item.id}
                    horizontal
                    style={{ marginTop: 20 }}
                    showsHorizontalScrollIndicator={false}
                    renderItem={this.viewImage}
                >

                </FlatList>
            </View>
        )
    }
    renderList = ({ item, index }) => {
        return (
            <View style={{ flex: 1 }}>
                {index == 0 && this.overView()}
                {index == 1 && this.ratingView()}
                {index == 3 && this.eventView()}
                {index == 4 && this.followersView()}
                {index == 5 && this.photoView()}
                {index == 2 && this.ratingReview()}
            </View>
        )
    }


    onLayout = (event, index) => {
        let { width, height } = event.nativeEvent.layout
        let newArray = [...this.state.viewHeights]
        newArray[index] = height
        this.setState({
            viewHeights: newArray
        })
    }
    ratingViewModal = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.isRatingModal}
            >
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <View style={[styles.ratingView, { marginTop: 20 }]}>
                        <Text style={styles.txtRate}>{"Rate and review"}</Text>
                        <Text style={styles.txtLabelRate}>{"Share your experience to help others"}</Text>
                        <Rating
                            type='custom'
                            ratingCount={5}
                            imageSize={25}
                            isDisabled={true}
                            startingValue={this.state.ratingValue}
                            style={{ backgroundColor: Colors.opacityColor, marginTop: 5 }}
                            onFinishRating={(value) => this.setState({ratingValue:value })}
                            ratingBackgroundColor={Colors.opacityColor3}
                            ratingColor={"#FFB91B"}
                            tintColor={Colors.theme}
                        />
                        <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            multiline={true}
                        //value={groupName}
                        //onChangeText={(text) => setGroupValue(text)}
                        //onFocus={() => onBlur("name")}
                        //onBlur={() => onBlur("name")}
                        >{"Write a comments"}</FloatingLabel>
                        <View style={{flexDirection:"row",alignSelf:"flex-end",marginTop:20}}>
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={[Colors.opacityColor2, Colors.opacityColor2]}
                                btnText={styles.btnText}
                                viewStyle={{ marginTop: 10, flex:0.3,marginRight:10 }}
                                onPress={() => this.setState({isRatingModal:false})}
                                text={"Cancel"}
                            />
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={Colors.btnBackgroundColor}
                                btnText={styles.btnText}
                                viewStyle={{ marginVertical: 10, flex:0.4 }}
                                onPress={() => this.setState({isRatingModal:false})}
                                text={"Post"}
                            />
                        </View>
                    </View>
                </View>
            </Modal >

        )
    }
    render() {
        const headerBackgroundColor = this._getHeaderBackgroundColor();

        const headerImageOpacity = this._getHeaderImageOpacity();

        const profileImageLeft = this._getImageLeftPosition();

        const profileImageTop = this._getImageTopPosition();

        const profileImageWidth = this._getImageWidth();

        const profileImageHeight = this._getImageHeight();

        const profileImageBorderWidth = this._getImageBorderWidth();

        const profileImageBorderColor = this._getImageBorderColor();

        const listViewTop = this._getListViewTopPosition();

        const headerTitleOpacity = this._getHeaderTitleOpacity();

        const normalTitleOpacity = this._getNormalTitleOpacity();

        return (
            <View style={styles.container}>
                <StatusBar hidden barStyle={'light-content'} backgroundColor={'#0C0024'} />

                <Animated.Image
                    style={[styles.headerImageStyle, {
                        opacity: headerImageOpacity,

                    }]}
                    source={Images.dogIcon} />

                <Animated.View style={[styles.headerStyle, {
                    backgroundColor: headerBackgroundColor,
                    height: 110
                }]}>

                    <View style={styles.headerLeftIcon}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack(null)}
                            style={styles.touchbleView}>
                            <Animated.View style={[styles.backGround, { opacity: this.getBackBtnOpacity() }]} />
                            <Image style={styles.imgBackIcon} source={Images.backArrow}></Image>
                        </TouchableOpacity>
                        <View style={{ flexDirection: "row", width: '100%' }}>
                            <Animated.Image
                                style={{ height: 38, width: 38, opacity: headerTitleOpacity }}
                                source={Images.bedIcon}
                            />
                            <View>
                                <Animated.Text
                                    style={[styles.headerTitle, {
                                        opacity: headerTitleOpacity,
                                        fontSize: 16, fontWeight: "700", marginLeft: 12
                                    }]}>
                                    {ARTIST_NAME}
                                </Animated.Text>
                                <Animated.View style={[styles.viewRatingScroll, { opacity: headerTitleOpacity, }]}>
                                    <Text style={styles.txtRatingScroll}>{4.4}</Text>
                                    <Rating
                                        type='custom'
                                        ratingCount={5}
                                        imageSize={10}
                                        startingValue={4.5}
                                        readonly={true}
                                        style={{ backgroundColor: Colors.opacityColor, marginLeft: 10 }}
                                        ratingBackgroundColor={Colors.opacityColor3}
                                        ratingColor={"#FFB91B"}
                                        tintColor={Colors.theme}
                                    />
                                </Animated.View>
                            </View>


                        </View>

                    </View>

                    <View style={styles.headerRightIcon}>
                        <TouchableOpacity style={[styles.touchbleView, { marginRight: 0 }]}>
                            <Animated.View style={[styles.backGround, { opacity: this.getBackBtnOpacity() }]} />
                            <Image style={styles.imgBackIcon} source={Images.share}></Image>
                        </TouchableOpacity>
                    </View>
                    <Animated.View style={{ opacity: headerTitleOpacity, top: 35 }}>
                        {this.categoryView()}
                    </Animated.View>
                </Animated.View>

                <Animated.Image
                    style={
                        [styles.profileImage, {
                            borderColor: profileImageBorderColor,
                            borderRadius: (ThemeUtils.APPBAR_HEIGHT - 20) / 2,
                            height: profileImageHeight,
                            width: profileImageWidth,
                            transform: [
                                { translateY: profileImageTop },
                                { translateX: profileImageLeft },
                            ],
                        }]}
                    source={Images.bedIcon}
                />

                <Animated.ScrollView
                    overScrollMode={'never'}
                    style={{ zIndex: 10 }}
                    ref={ref => this.scrollView = ref}
                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([{
                            nativeEvent: { contentOffset: { y: this.state.scrollY } },
                        }], {
                            listener: (event) => {
                            },
                            useNativeDriver: false,
                        })}
                    onContentSizeChange={(event) => console.log(event)}
                >

                    <Animated.Text style={[
                        styles.profileTitle, {
                            opacity: normalTitleOpacity,
                        },
                    ]}>
                        {ARTIST_NAME}
                    </Animated.Text>

                    <Animated.View style={[
                        styles.viewRating, {
                            opacity: normalTitleOpacity,
                        },
                    ]}>
                        <Text style={styles.txtRating}>{4.4}</Text>
                        <Rating
                            type='custom'
                            ratingCount={5}
                            imageSize={20}
                            startingValue={4.5}
                            readonly={true}
                            style={{ backgroundColor: Colors.opacityColor, marginLeft: 10 }}
                            ratingBackgroundColor={Colors.opacityColor3}
                            ratingColor={"#FFB91B"}
                            tintColor={Colors.theme}
                        />
                    </Animated.View>
                    <Animated.View style={[
                        styles.followoView, {
                            opacity: normalTitleOpacity,
                        },
                    ]}>
                        <LinearGradient colors={Colors.btnBackgroundColor} style={{ height: 40, margin: 20, borderRadius: 4 }} >
                            <TouchableOpacity style={styles.viewBtnFollow}>
                                <Image style={styles.imgPetIcon} source={Images.petProfileTab}></Image>
                                <Text style={styles.txtFollow}>{"Follow"}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                        {this.categoryView()}
                    </Animated.View>

                    <Animated.View style={{
                        transform: [{
                            translateY: listViewTop,
                        }],
                    }}>

                        {this.overView()}
                        {this.ratingView()}
                        {this.ratingReview()}
                        {this.eventView()}
                        {this.followersView()}
                        {this.photoView()}
                        {/* <FlatList
                            style={{ zIndex: 10 }}
                            ref={ref => this.flatList = ref}
                           
                            data={["overView", "ratingView", "", "eventView", "followersView", "photoView"]}
                            renderItem={this.renderList}
                            //  onViewableItemsChanged={onViewRef.current}
                            // ListHeaderComponent={headerView}
                            showsVerticalScrollIndicator={false}
                        //onScroll={onScroll}
                        // viewabilityConfig={viewConfigRef.current}
                        >
                        </FlatList> */}
                    </Animated.View>
                    {this.ratingViewModal()}
                </Animated.ScrollView>
            </View>
        );
    }
}
