import React from 'react';
// import redux files 
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

// import reducers
import reducer from '../reducers';

export default function configStore () {
    let store = createStore(reducer , applyMiddleware(thunk))

    return store
}