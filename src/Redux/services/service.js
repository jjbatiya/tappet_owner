import { apiUrl } from "./apiUrl";
import { defaultRestClient } from "../../utils/restClient";

export const registerUserService = (data) => {
    return defaultRestClient.postWithBody(apiUrl.register,data)
};
export const loginUserService = (data) => {
    return defaultRestClient.postWithBody(apiUrl.login,data)
};