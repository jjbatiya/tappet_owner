const { REGISTER_USER, LOGIN_USER } = require("../actions/actionType")

const initalState = {
    people: [],
    data: null
}
const reducer = (state = initalState, action) => {
    switch (action.type) {
        case REGISTER_USER:
            return {
                ...state,
                data: action.payload
            }
        case LOGIN_USER:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state
    }
}
export default reducer;
