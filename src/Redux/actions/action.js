import { loginUserService, registerUserService } from "../services/service";
import { IS_SHOWING, LOGIN_USER, REGISTER_USER } from "./actionType";

export const registerUser = (data) => {
    return (dispatch) => {
        return registerUserService(data).then((response) => {
            dispatch({ type: REGISTER_USER, payload: response })
        }).catch(error => {
            console.log("Error is ==>", error)
        })
    }
};
export const loginUser = (data) => {
    return (dispatch) => {
        return loginUserService(data).then((response) => {
            dispatch({ type: LOGIN_USER, payload: response })
        }).catch(error => {
            console.log("Error is ==>", error)
        })
    }
}
export const changeCallFlag=(isshowing)=> {
    return (dispatch) => {
        dispatch({ type: IS_SHOWING, payload: isshowing })
    }, error => {
        console.log("Error is ==>", error)
    }
}
