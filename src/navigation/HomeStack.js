import React,{useEffect} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useRoute } from '@react-navigation/native';

import HomeScreen from '../screens/App/HomeScreen';
import CommentsScreen from '../screens/App/CommentsScreen';
import LikesScreen from '../screens/App/LikesScreen';
import CreatePostScreen from '../screens/App/CreatePostScreen';

const HomeStack = createStackNavigator();

const HomeStackNavigation = ({ navigation, route }) => {
    useEffect(()=>{
        const routeName =route.name// getFocusedRouteNameFromRoute(route)
        if(routeName == 'comments'){
            navigation.setOptions({tabBarVisible: false})
        } else if(routeName == 'likes'){
            navigation.setOptions({tabBarVisible: false})
        } else if(routeName == 'createPost'){
            navigation.setOptions({tabBarVisible: false})
        } else if(routeName == 'home'){
            navigation.setOptions({tabBarVisible: true})
        }
    })
    return (
        <HomeStack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <HomeStack.Screen name="home" component={HomeScreen} />
            {/* <HomeStack.Screen name="comments" component={CommentsScreen} />
            <HomeStack.Screen name="likes" component={LikesScreen} />
            <HomeStack.Screen name="createPost" component={CreatePostScreen} /> */}
        </HomeStack.Navigator>
    )
}

export default HomeStackNavigation;