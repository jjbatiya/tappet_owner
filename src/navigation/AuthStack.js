import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../screens/Auth/Login/Login';
import ResetPassword from '../screens/Auth/ResetPassword/ResetPassword';
import SignUp from '../screens/Auth/SignUp/SignUp';
import VerifyEmail from '../screens/Auth/SignUp/VerifyEmail/VerifyEmail';
import Account from '../screens/Auth/SignUp/CreateAccount/Account';
import NewPassword from '../screens/Auth/NewPassword/NewPassword';
import UploadPhoto from '../screens/Auth/SignUp/UploadPhoto/UploadPhoto';

import SplashScreen from '../screens/splash/SplashScreen';
import { strings } from '../Resources/Strings';
const AuthStack = createStackNavigator();
const AuthStackNavigation = () => {
    return (
        <AuthStack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <AuthStack.Screen name={strings.splashScreen} component={SplashScreen} />
            <AuthStack.Screen name={strings.loginScreen} component={LoginScreen} />
            <AuthStack.Screen name={strings.resetPasswordScreen} component={ResetPassword} />
            <AuthStack.Screen name={strings.signUpScreen} component={SignUp} />
            <AuthStack.Screen name={strings.verifyEmailScreen} component={VerifyEmail} />
            <AuthStack.Screen name={strings.createaccountScreen} component={Account} />
            <AuthStack.Screen name="newPassword" component={NewPassword} />
            <AuthStack.Screen name={strings.uploadPhotoScreen} component={UploadPhoto} />
        </AuthStack.Navigator>
    )
}

export default AuthStackNavigation;