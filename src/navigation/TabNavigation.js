import React, { useEffect, useState } from 'react';
import { Image, Text, StyleSheet, View, Platform } from 'react-native';
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

/* Screens */
import HomeScreen from '../screens/App/HomeScreen';
import ExploreScreen from '../screens/App/ExploreScreen';
import EventScreen from '../screens/App/EventScreen';
import MapScreen from '../screens/App/MapScreen';
import MyProfile from '../screens/App/ProfileScreen/MyProfile/MyProfile';

import { Colors } from '../Resources/Colors';

import { responsiveHeight } from 'react-native-responsive-dimensions';
import { HomeIcon } from '../utils/svg/HomeIcon'
import { SerachIcon } from '../utils/svg/SerachIcon';
import { EventIcon } from '../utils/svg/EventIcon';
import { EventIconUnselect } from '../utils/svg/EventIconUnselect';
import { PlaceIconUnselect } from '../utils/svg/PlaceIconUnselect';
import { PlaceIcon } from '../utils/svg/PlaceIcon';
import { getUserData, storeGroupId } from '../utils/helper';
const Tab = createMaterialBottomTabNavigator();
var response = {};
const TabNavigation = () => {
    const [u_image, setUserImage] = useState("http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg")
    useEffect(() => {
        getUserData().then(res => {
            if (res.u_image != '')
                setUserImage(res.u_image)
        })
        // console.log("DATA-->", moment(date).format("MM/DD/YYYY"))
    }, [])

    return (

        <Tab.Navigator
         
            shifting={true}
            barStyle={{ backgroundColor: Colors.theme }}

            tabBarOptions={{

                //  activeBackgroundColor: '#0C0024',
                // inactiveBackgroundColor: Colors.theme,

                style: {
                    backgroundColor: '#0C0024',
                    borderTopWidth: 1,
                    height: Platform.OS == "ios" ? responsiveHeight(8.3) : responsiveHeight(8.3),
                    borderTopColor: 'rgba(255,255,255,0.1)',
                    paddingBottom: Platform.OS == "ios" ? 1 : 0
                },
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}

                options={{
                    tabBarLabel: 'Home',
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.homeTab}</Text>
                    // ),
                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>
                            <HomeIcon fillColor={focused ? Colors.primaryViolet : null} storke={focused ? Colors.primaryViolet : Colors.opacityColor3} width={24} height={24} />
                        </View>
                    ),

                }} />
            <Tab.Screen

                name="Explore"
                component={ExploreScreen}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.exploreTab}</Text>
                    // ),
                    tabBarLabel: 'Explore',

                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>
                            <SerachIcon fillColor={focused ? Colors.primaryViolet : null} storke={focused ? Colors.primaryViolet : Colors.opacityColor3} width={24} height={24} />
                        </View>

                    ),
                }} />
            <Tab.Screen
                name="Event"
                component={EventScreen}
                listeners={({ navigation, route }) => ({
                    tabPress: e => {
                        storeGroupId("")
                        navigation.navigate("Event")
                    },
                })}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.eventTab}</Text>
                    // ),
                    tabBarLabel: 'Play dates',
                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>
                            {!focused ?
                                <EventIconUnselect width={24} height={24} /> :
                                <EventIcon fillColor={focused ? Colors.primaryViolet : null} storke={focused ? Colors.primaryViolet : Colors.opacityColor3} width={24} height={24} />
                            }
                        </View>

                    ),

                }} />
            <Tab.Screen
                name="Map"
                component={MapScreen}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.mapTab}</Text>
                    // ),
                    tabBarLabel: 'Map',
                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>

                            {
                                !focused ?
                                    <PlaceIconUnselect width={24} height={24} /> :
                                    <PlaceIcon fillColor={focused ? Colors.primaryViolet : null} storke={focused ? Colors.primaryViolet : Colors.opacityColor3} width={24} height={24} />
                            }
                        </View>
                    ),

                }} />
            <Tab.Screen
                name="MyProfile"
                component={MyProfile}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.profileTab}</Text>
                    // ),
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>
                            <Image source={{ uri: u_image }} style={{ height: 24, width: 24, borderRadius: 12 }} />
                        </View>
                    ),
                }} />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    icon: {
        width: 19,
        height: 20
    },
    tabLable: {
        fontSize: 11,
        paddingBottom: Platform.OS == "android" ? 10 : 10,
        marginTop: Platform.OS == "android" ? 5 : 0,
    },
    tabFocus: {
        tintColor: Colors.primaryViolet
    },
    tabUnFocus: {
        tintColor: Colors.white,
        opacity: 0.4
    },
    tabFocusLabel: {
        color: Colors.primaryViolet
    },
    tabUnFocusLabel: {
        color: Colors.white,
        opacity: 0.4
    },
    viewTabIcon: {
        //    paddingTop: Platform.OS == "ios" ? 0 : 8,
    }
})
export default TabNavigation;
