import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';

import AuthStack from './AuthStack';
import TabNavigation from './TabNavigation';
import SplashScreen from '../screens/splash/SplashScreen';
import Message from '../screens/App/Message';
import SerachMessage from '../screens/App/Message/SerachMessage';
import TypeMessage from '../screens/App/Message/TypeMessage';
import HistoryCallsDetail from '../screens/App/Message/CallHistory/HistoryCallsDetail';
import CallScreen from '../screens/App/Message/TypeMessage/CallScreen';
import ViewPhoto from '../screens/App/Message/TypeMessage/ViewPhoto';
import UserProfile from '../screens/App/ProfileScreen/UserProfile/UserProfile';
import EditProfile from '../screens/App/ProfileScreen/EditProfile/EditProfile';
import Notification from '../screens/App/Notification';
import Groups from '../screens/App/Groups';
import CreateGroup from '../screens/App/Groups/CreateGroup';
import AddMember from '../screens/App/Groups/CreateGroup/AddMember';
import ViewGroup from '../screens/App/Groups/ViewGroup';
import ChatMessage from '../screens/App/Groups/ChatMessage';
import Friends from '../screens/App/Friends';
import { Settings } from 'react-native';
import SettingScreen from '../screens/App/SettingScreen';
import AccountSetting from '../screens/App/SettingScreen/AcountSetting';
import ChangePassword from '../screens/App/SettingScreen/ChangePassword';
import DeviceLoginScreen from '../screens/App/SettingScreen/DeviceLoginScreen';
import BlockUserScreen from '../screens/App/SettingScreen/BlockUserScreen';
import PetProfile from '../screens/App/PetProfile/Profile/PetProfile';
import PetProfileTab from '../screens/App/PetProfile';
import AddPet from '../screens/App/ProfileScreen/AddPet';
import AddOwnerScreen from '../screens/App/PetProfile/Profile/AddOwnerScreen';
import AddCollar from '../screens/App/PetProfile/Profile/AddCollar';
import AddCollarScan from '../screens/App/PetProfile/Profile/AddCollar/AddCollarScan';
import ViewLocation from '../screens/App/MapScreen/ViewLocation';
import ViewMap from '../screens/App/PetProfile/Activity/AllActivity/ViewMap';
import DetailPage from '../screens/App/PetProfile/Activity/StartRun/DetailPage';
import ViewActivity from '../screens/App/PetProfile/Activity/AllActivity/ViewActivity';
import TapActivity from '../screens/App/PetProfile/Activity/AllActivity/TapActivity';
import SearchMap from '../screens/App/PetProfile/PetMap/SearchMap';
import GeoFence from '../screens/App/PetProfile/PetMap/GeoFence';
import ProfileCrop from '../screens/App/ExploreScreen/ProfileCrop';

import AddSchedule from '../screens/App/PetProfile/Schedule/AddSchedule/AddSchedule';
import CustomSchedule from '../screens/App/PetProfile/Schedule/AddSchedule/CustomSchedule';
import NotificationAlert from '../screens/App/SettingScreen/NotificationAlert';
import PetSpecific from '../screens/App/SettingScreen/NotificationAlert/PetSpecific';
import CreatePostScreen from '../screens/App/CreatePostScreen';
import LikesScreen from '../screens/App/LikesScreen';
import CommentsScreen from '../screens/App/CommentsScreen';
import UpcomingEventDetail from '../screens/App/EventScreen/Uplcoming/UpcomingEventDetail';
import ViewImages from '../screens/App/EventScreen/PastEvent/ViewImages';
import PastEventDetail from '../screens/App/EventScreen/PastEvent/PastEventDetail';
import AddEvent from '../screens/App/EventScreen/Uplcoming/AddEvent';
import SerachExplore from '../screens/App/ExploreScreen/SerachExplore';
import ProfileTab from '../screens/App/ExploreScreen/ProfileCrop/ProfileTab';
import CutomHeaderScreen from '../AnimatedProfileCrop/CutomHeaderScreen';
import MediaTab from '../screens/App/Groups/ViewGroup/MediaTab';
import WebViewScreen from '../screens/App/WebViewScreen';
import AudioPlayerScreen from '../screens/App/AudioPlayerScreen';
import OutgoingCallScreen from '../screens/App/Message/TypeMessage/OutgoingCallScreen';
import WebScreen from '../screens/App/SettingScreen/WebScreen';
import { strings } from '../Resources/Strings';
import PetFullPage from '../screens/App/PetViewOther/PetFullPage';
import PetPageOther from '../screens/App/PetViewOther/PetPageOther';
import FullImageView from '../screens/App/PetViewOther/PetPageOther/ProfilePage/FullImageView';
import UserChatList from '../screens/App/Message/UserChatList';
import IncomingCallScreen from '../screens/App/Message/TypeMessage/IncomingCallScreen';
const RootStackNav = createStackNavigator();

const RootStack = () => (
    <NavigationContainer >
        <RootStackNav.Navigator
            screenOptions={{
                headerShown: false, headerBackTitleVisible: false
            }}
        >
            <RootStackNav.Screen name={strings.AuthStack} component={AuthStack} />
            <RootStackNav.Screen name={strings.TabNavigation} component={TabNavigation} />

            <RootStackNav.Screen name={strings.commentsScreen} component={CommentsScreen} />
            <RootStackNav.Screen name={strings.likeScreen} component={LikesScreen} />
            <RootStackNav.Screen name={strings.CreatePostScreen} component={CreatePostScreen} />
            <RootStackNav.Screen name={strings.messageScreen} component={Message}></RootStackNav.Screen>

            <RootStackNav.Screen name={strings.addEventScreen} component={AddEvent} />
            <RootStackNav.Screen name={strings.EventDetailUpcomingScreen} component={UpcomingEventDetail} />
            <RootStackNav.Screen name={strings.viewImagesScreen} component={ViewImages} />
            <RootStackNav.Screen name={strings.EventDetailPastScreen} component={PastEventDetail} />

            <RootStackNav.Screen name={strings.serachPeopleScreen} component={SerachExplore} />

            <RootStackNav.Screen name={strings.serachMessageScreen} component={SerachMessage}></RootStackNav.Screen>
            <RootStackNav.Screen name={strings.typeMessageScreen} component={TypeMessage}></RootStackNav.Screen>
            <RootStackNav.Screen name={strings.outgoingCallScreen} component={OutgoingCallScreen} />
            <RootStackNav.Screen name={strings.incomingCallScreen} component={IncomingCallScreen} />

            <RootStackNav.Screen name={strings.callScreen} component={CallScreen}></RootStackNav.Screen>
            <RootStackNav.Screen name={strings.viewPhotosScreen} component={ViewPhoto}></RootStackNav.Screen>

            <RootStackNav.Screen name={strings.historycallsDetailScreen} component={HistoryCallsDetail}></RootStackNav.Screen>

            <RootStackNav.Screen name={strings.UserProfileScreen} component={UserProfile} />
            <RootStackNav.Screen name={strings.EditProfileScreen} component={EditProfile} />
            <RootStackNav.Screen name={strings.notificationScreen} component={Notification} />
            <RootStackNav.Screen name={strings.groupsScreen} component={Groups} />
            <RootStackNav.Screen name={strings.createGroupScreen} component={CreateGroup} />
            <RootStackNav.Screen name={strings.viewGroupScreen} component={ViewGroup} />
            <RootStackNav.Screen name={strings.chatMessageScreen} component={ChatMessage} />
            <RootStackNav.Screen name={strings.mediaTabScreen} component={MediaTab} />

            <RootStackNav.Screen name={strings.friendsScreen} component={Friends} />
            <RootStackNav.Screen name={strings.settingScreen} component={SettingScreen} />
            <RootStackNav.Screen name={strings.accountSettingScreen} component={AccountSetting} />
            <RootStackNav.Screen name={strings.changePasswordScreen} component={ChangePassword} />
            <RootStackNav.Screen name={strings.deviceLoginScreen} component={DeviceLoginScreen} />
            <RootStackNav.Screen name={strings.blockUserScreen} component={BlockUserScreen} />

            {/* <RootStackNav.Screen name="PetProfile" component={PetProfile}/> */}
            <RootStackNav.Screen name={strings.PetProfileTabScreen} component={PetProfileTab} />
            <RootStackNav.Screen name={strings.addPetScreen} component={AddPet} />
            <RootStackNav.Screen name={strings.addOwnerScreen} component={AddOwnerScreen} />
            <RootStackNav.Screen name={strings.addCollarScreen} component={AddCollar} />
            <RootStackNav.Screen name={strings.addCollarScanScreen} component={AddCollarScan} />
            <RootStackNav.Screen name={strings.viewLocationScreen} component={ViewLocation} />
            <RootStackNav.Screen name={strings.viewMapScreen} component={ViewMap} />
            <RootStackNav.Screen name={strings.viewActivityScreen} component={ViewActivity} />

            <RootStackNav.Screen name={strings.detailPageScreen} component={DetailPage} />
            <RootStackNav.Screen name={strings.tapActivityScreen} component={TapActivity} />
            <RootStackNav.Screen name={strings.searchMapScreen} component={SearchMap} />
            <RootStackNav.Screen name={strings.geoFenceScreen} component={GeoFence} />

            <RootStackNav.Screen name={strings.profileCropScreen} component={ProfileCrop} />
            <RootStackNav.Screen name={strings.addScheduleScreen} component={AddSchedule} />
            <RootStackNav.Screen name={strings.customScheduleScreen} component={CustomSchedule} />
            <RootStackNav.Screen name={strings.notificationAlertScreen} component={NotificationAlert} />
            <RootStackNav.Screen name={strings.petSpecificScreen} component={PetSpecific} />

            <RootStackNav.Screen name={strings.profileTabScreen} component={ProfileTab} />
            <RootStackNav.Screen name={strings.cutomHeaderScreen} component={CutomHeaderScreen} />
            <RootStackNav.Screen name={strings.addMemberScreen} component={AddMember} />
            <RootStackNav.Screen name={strings.webViewScreen} component={WebViewScreen} />
            <RootStackNav.Screen name={strings.playerScreen} component={AudioPlayerScreen} />
            <RootStackNav.Screen name={strings.webScreen} component={WebScreen} />
            <RootStackNav.Screen name={strings.petFullOtherView} component={PetFullPage}/>
            <RootStackNav.Screen name={strings.PetPageOther} component={PetPageOther} 
              options={{ cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS, }}
            />
            <RootStackNav.Screen name={strings.FullImageView} component={FullImageView} />
            <RootStackNav.Screen name={strings.userchatlist} component={UserChatList} />


        </RootStackNav.Navigator>
    </NavigationContainer>
)
export default RootStack;