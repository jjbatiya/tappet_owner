import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import EventScreen from '../screens/App/EventScreen';
import AddEvent from '../screens/App/EventScreen/Uplcoming/AddEvent';
import ViewImages from '../screens/App/EventScreen/PastEvent/ViewImages';
import UpcomingEventDetail from '../screens/App/EventScreen/Uplcoming/UpcomingEventDetail';
import PastEventDetail from '../screens/App/EventScreen/PastEvent/PastEventDetail';
const ExploreStack = createStackNavigator();

const EventStackNavigation = ({ navigation, route }) => {

    useEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route)

        if (routeName == 'addEvent'||routeName=='EventDetailUpcoming') {
            navigation.setOptions({ tabBarVisible: false })
        } else if (routeName == 'EventSceen') {
            navigation.setOptions({ tabBarVisible: true })
        }
        else if(routeName=="viewImages"||routeName=="EventDetailPast")
        {
            navigation.setOptions({ tabBarVisible: false })
        }
    })
    return (
        <ExploreStack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <ExploreStack.Screen name="EventSceen" component={EventScreen} />
            {/* <ExploreStack.Screen name="addEvent" component={AddEvent} />
            <ExploreStack.Screen name="EventDetailUpcoming" component={UpcomingEventDetail} />
            <ExploreStack.Screen name="viewImages" component={ViewImages} />
            <ExploreStack.Screen name="EventDetailPast" component={PastEventDetail} /> */}
        </ExploreStack.Navigator>
    )
}

export default EventStackNavigation;