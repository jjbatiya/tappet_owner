import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import ExploreScreen from '../screens/App/ExploreScreen';
import SerachExplore from '../screens/App/ExploreScreen/SerachExplore';

const ExploreStack = createStackNavigator();

const ExploreStackNavigation = ({ navigation, route }) => {

    useEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route)

        if (routeName == 'serachPeople') {
            navigation.setOptions({ tabBarVisible: false })
        } else if (routeName == 'Explore') {
            navigation.setOptions({ tabBarVisible: true })
        }
    })
    return (
        <ExploreStack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <ExploreStack.Screen name="Explore" component={ExploreScreen} />
            {/* <ExploreStack.Screen name="serachPeople" component={SerachExplore} /> */}
        </ExploreStack.Navigator>
    )
}

export default ExploreStackNavigation;