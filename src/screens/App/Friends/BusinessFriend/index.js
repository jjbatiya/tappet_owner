import React,{useRef} from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './BusinessFriendStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import CallFriends from '../../../../utils/svg/CallFriends';
import ChatMessage from '../../../../utils/svg/ChatMessage';


const BusinessFriend = (props) => {
    let refSheet = useRef();

    const renderItem = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <View>
                        <Image source={item.image} style={styles.imgStyle}></Image>
                        <View style={{
                            position: "absolute", alignItems: "center",
                            justifyContent: "center", height: 20, width: 20,
                            borderWidth: 1, borderColor: '#0C0024', backgroundColor: Colors.white,
                            borderRadius: 10, bottom: 0, right: 0
                        }}>
                            <Image style={{ height: 14, width: 14 }} source={Images.starIcon}></Image>
                        </View>
                    </View>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.name}</Text>
                        <Text style={styles.txtDetail}>{item.detailText}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                    <TouchableOpacity onPress={()=>props.navigation.navigate(strings.messageScreen)}>
                        <Image style={{ width: 16, height: 16, tintColor: Colors.white, marginRight: 10 }} source={Images.contact}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => refSheet.open()}>
                        <Image style={{ width: 14, height: 14, tintColor: Colors.white }} source={Images.menuDot}></Image>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
    const bottomView = () => {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={styles.txtHeading}>Choose Connection Type</Text>
                <Text style={styles.txtLabel}>How would you like to connect?</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: responsiveWidth(10) }}>
                    <View>
                        <TouchableOpacity style={styles.viewRound}>
                            <CallFriends/>
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Call</Text>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.viewRound}>
                            <ChatMessage/>
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Message</Text>

                    </View>
                </View>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>
                    <View>
                        <SimpleSerach
                            onChangeText={(text) => console.log(text)}
                            placeHolder={strings.searchforFriends}
                            isSimpleSerach={true}
                            editable={true}
                            isScreen={true}
                            onSubmitEditing={()=>console.log("dd")}

                        />
                    </View>
                    <FlatList
                        data={[]}
                        style={{marginBottom:100}}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                    >

                    </FlatList>
                </View>
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={290}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {bottomView()}
                </RBSheet>
            </LinearGradient>
        </View>
    )

}
export default BusinessFriend;