import React, { useEffect, useState } from 'react';
import { View, Text, useWindowDimensions, Platform } from 'react-native';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import AllFriend from './AllFriend';
import BusinessFriend from './BusinessFriend';
import PendingFriend from './PendingFriend';

import Toast from 'react-native-simple-toast';
import { apiCallWithToken, getUserData } from '../../../utils/helper';
import MutualFriend from './MutualFriend';
import { strings } from '../../../Resources/Strings';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import NotificationController from '../../../Components/NotificationController';

const Friends = (props) => {
    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes, setRoutes] = useState([]);
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const friendsData = [
        { key: 'AllFriends', title: 'All Friends' },
        { key: 'Business', title: 'Business' },
        { key: 'Pending', title: "Pending" }
    ]
    const mutualData = [
        { key: 'MutualFriend', title: 'Mutual Friend' },
        { key: 'Other', title: 'Other' }
    ]
    const [isUser] = useState(props.route.params.isUser)

    useEffect(() => {

        if (isUser)
            setRoutes(friendsData)
        else
            setRoutes(mutualData)

    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const renderScene = ({ route, index1 }) => {
        switch (route.key) {
            case 'AllFriends':
                return <AllFriend navigation={props.navigation} index={index} />;
            case 'Business':
                return <BusinessFriend navigation={props.navigation} index={index} />;
            case 'Pending':
                return <PendingFriend navigation={props.navigation} index={index} />;
            default:
                return null;
        }
    };
    const renderSceneMutualFriend = ({ route, index1 }) => {
        switch (route.key) {
            case 'MutualFriend':
                return <MutualFriend navigation={props.navigation} friend_id={props.route.params.friend_id} index={index} />;
            case 'Other':
                return <MutualFriend navigation={props.navigation} friend_id={props.route.params.friend_id} index={index} />;
            default:
                return null;
        }
    }

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}

        />
    );
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height: responsiveHeight(12.4) }}
            >
                <AppHeader
                    navigation={props.navigation}
                    title={strings.frdAndFollowing}
                    titleText={style = { marginLeft: -30 }}
                    backButton={Images.backIcon}
                    Tab={true}
                />

            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={isUser ? renderScene : renderSceneMutualFriend}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}
            />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
              {isVisibleCall==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisibleCall}
                        onMessageNotification={()=>{}}

                    />
                }
        </View >
    )
}

export default Friends;