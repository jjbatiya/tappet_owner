import React, { useEffect, useRef, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, Share, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './AllFriendStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import SimpleSerach from '../../../../Components/SimpleSerach';
import MoreMenu from '../../../../Components/MoreMenu';

import { responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import { Fonts } from '../../../../Resources/Fonts';
import ChatMessage from '../../../../utils/svg/ChatMessage';
import { CallIcon } from '../../../../utils/svg/CallIcon';
import CallFriends from '../../../../utils/svg/CallFriends';
import { apiCallGetWithToken, apiCallWithToken, createDynamicLinks, getUserData, imageUploadApi } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import { toastConfig } from '../../../../utils/ToastConfig';


const AllFriend = (props) => {
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(10)
    const [serachText, setSerachText] = useState("")
    const [selectUser, setSelectUser] = useState("")
    const [user_id, setUserID] = useState("")
    const [groupData, setGroupData] = useState([])
    const [eventdata, setEventData] = useState([])
    const [isFetching, setIsFetching] = useState(false)

    const [bottomData, setBottomData] = useState([
        {
            key: "blockUser",
            title: "Block",
            image: Images.blocked
        },
        {
            key: "share",
            title: "Share",
            image: Images.share
        },
        {
            key: "addToGroup",
            title: "Add to a group",
            image: Images.addFriend
        },
        {
            key: "inviteEvent",
            title: "Invite to an event",
            image: Images.serachIcon
        },

        {
            key: "delete",
            title: "Remove friend",
            image: Images.deleteIcon
        },
    ])
    var onEndReached;

    let refSheet = useRef();
    let refSheetDot = useRef();
    let refSheetGroup = useRef();
    let refSheetEvent = useRef();
    useEffect(() => {
        if (props.index == 0) {
            getUserData().then(res => {
                setShowing(true)
                setUserID(res.u_id)
                setToken(res.token)
                getFriendsData(res.token)
                getGroupData(res.token, res.u_id)

            })
        }
    }, [props.index])
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getFriendsData = async (token) => {
        var obj = {
            page: page,
            limit: limit,
            friend_request: 1,
            search: serachText
        }
        await apiCallWithToken(obj, apiUrl.my_friends, token).then(res => {
            setIsFetching(false)
            if (res.status == true) {

                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    if (res.result.length != 0)
                        setFriendData(res.result)
                    else
                        showMessage(strings.error, strings.msgTag, res.message)
                }
                else {
                    setFriendData(friendsData.concat(res.result))
                }

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    setFriendData([])
                }, 1000);
            }
        })
    }
    const filterData = async (array, u_id) => {
        let newArray = [];
        for (var i = 0; i < array.length; i++) {
            var index = array[i].group_last_two_members.findIndex(obj => obj.gm_user_id === u_id);
            if (index > -1) {
                if (array[i].group_last_two_members[index].gm_role == "Admin") {
                    newArray.push(array[i])
                }
            }
        }
        setGroupData(newArray)
    }
    const getGroupData = async (token, u_id) => {
        await apiCallGetWithToken(apiUrl.get_all_groups, token).then(res => {
            setShowing(false)
            setIsFetching(false)

            if (res.status == true) {
                if (res.result.length != 0) {
                    filterData(res.result, u_id)
                    getEventData({ u_id: u_id, token: token });
                }
            }
            else {
                //  showMessage("error", "Message", res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    function filterEventData(data, u_id) {
        return data.event_owner_id == u_id;
    }
    const getEventData = async (data) => {
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", data.token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setEventData(res.result.filter((item) => filterEventData(item, data.u_id)))
            }
            else {
                setShowing(false)

            }
        })
    }
    const clickMenu = async (item, refSheet) => {
        let newArray = [...bottomData]

        if (item.is_blocked_by_me == 0) {
            newArray[0].title = "Block"
        }
        else
            newArray[0].title = "Unblock"
        console.log(newArray)
        setBottomData(newArray)
        await setSelectUser(item)

        setTimeout(() => {
            refSheet.open()
        }, 400);
    }
    const onMessage = () => {
        refSheet.close();
        props.navigation.navigate(strings.typeMessageScreen, { userData: selectUser })
    }
    const renderItem = ({ item, index }) => {
        return (
            <View

                style={styles.renderSubView}>
                <TouchableOpacity onPress={() => props.navigation.replace(strings.UserProfileScreen, { u_id: item.u_id })} style={styles.contentView}>

                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pets"}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                    <TouchableOpacity style={{ paddingLeft: 3 }}
                        onPress={() => clickMenu(item, refSheet)}>
                        <Image style={{ width: 16, height: 16, tintColor: Colors.white, marginRight: 10 }} source={Images.contact}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ paddingHorizontal: 3 }} onPress={() => clickMenu(item, refSheetDot)}>
                        <Image style={{ width: 14, height: 14, tintColor: Colors.white }} source={Images.menuDot}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    const bottomView = () => {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={styles.txtHeading}>Choose Connection Type</Text>
                <Text style={styles.txtLabel}>How would you like to connect?</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: responsiveWidth(10) }}>
                    <View>
                        <TouchableOpacity onPress={() => callClick()} style={styles.viewRound}>
                            <CallFriends />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Call</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => onMessage()} style={styles.viewRound}>
                            <ChatMessage />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Message</Text>
                    </View>
                </View>
            </View>
        )
    }
    const callClick = () => {
        refSheet.close();

        var receiverID = comeChatUserTag + "" + selectUser.u_id;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.USER;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                props.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: selectUser })

                // perform action on success. Like show your calling screen.
            },
            error => {
                console.log("Call initialization failed with exception:", error);
            }
        );
    }
    const onSubmitEditing = () => {
        setShowing(true)
        getFriendsData(token)
    }
    const handleMoreData = () => {
        if (totalPage > page) {
            setPage(page + 1)
            setShowing(true)
            getFriendsData(token)
        }

    }
    const clickItem = (item) => {

        refSheetDot.close()
        if (item.key == "blockUser") {
            BlockUser(item.title);
        }
        else if (item.key == "share") {
            var obj = {
                id: selectUser.u_id,
                title: selectUser.u_first_name + " " + selectUser.u_last_name,
                imageUrl: selectUser.u_image,
                descriptionText: "TapPet User",
                flag: "user"
            }
            setTimeout(() => {
                createDynamicLinks(obj).then(res => {
                    onShare(res)
                    //apiKeyGenerate(res)
                })
            }, 400);
        }
        else if (item.key == "addToGroup") {
            setTimeout(() => {
                if (groupData.length != 0)
                    refSheetGroup.open();
                else
                    showMessage(strings.error, strings.msgTag, "Group data not found!")
            }, 1000);

        }
        else if (item.key == "inviteEvent") {
            setTimeout(() => {
                if (eventdata.length != 0)
                    refSheetEvent.open();
                else
                    showMessage(strings.error, strings.msgTag, "Event data not found!")
            }, 1000);

        }
        else if (item.key == "delete") {
            removeFriend();
        }
    }
    const removeFriend = async () => {
        setShowing(true)
        var obj = {
            friend_id: selectUser.u_id
        }
        await apiCallWithToken(obj, apiUrl.remove_friend, token).then(res => {
            if (res.status == true) {
                //showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getFriendsData(token)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const BlockUser = async (flag) => {
        setShowing(true)
        var obj = {
            user_id: selectUser.u_id,
            state: flag == "Unblock" ? "Unblock" : "Block"
        }
        await apiCallWithToken(obj, apiUrl.block_or_unblock_user, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                showMessage("success", "Message", res.message)
                comeChatBlockUser(flag)

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const comeChatBlockUser = (flag) => {
        var usersList = [comeChatUserTag + selectUser.u_id];
        flag == "Block" ? CometChat.blockUsers(usersList).then(
            list => {
                getFriendsData(token)
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        ) : CometChat.unblockUsers(usersList).then(
            list => {
                getFriendsData(token)
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        );
    }
    const onShare = async (link) => {
        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
        }
    };
    const groupList = () => {
        return (
            <View style={{ marginBottom: 20 }}>
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheetGroup.close()}>

                </TouchableOpacity>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {groupData.map((item, index) => {
                        var memberList = item.group_last_two_members.map(a => a.member.u_first_name).toString();
                        return (
                            <View style={styles.mainGroupView}>
                                <View>
                                    <Image style={styles.imgGroup} source={{ uri: item.group_image }}></Image>

                                </View>
                                <View style={styles.radioViewGroup}>
                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={styles.radioText}>{item.group_name}</Text>
                                        <Text style={styles.txtInvite}>{memberList}</Text>
                                    </View>
                                    < CommonButton
                                        btnlinearGradient={[styles.btnlinearGradient, { paddingHorizontal: 20 }]}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        viewStyle={styles.verifyBtn}
                                        onPress={() => Add(item)}
                                        text={"Add"}
                                    />

                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>

        )
    }
    const Add = async (item) => {
        refSheetGroup.close()
        let data = new FormData();
        setShowing(true)
        data.append("group_id", item.group_id)
        data.append("group_members[]", selectUser.u_id)

        await imageUploadApi(data, apiUrl.add_group_member, token).then(res => {
            if (res.status == true) {
                showMessage("success", "Message", res.message)
                setShowing(false)
                setTimeout(() => {
                    addGroupMembers(item)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const addGroupMembers = (item) => {
        let GUID = comeGroupTag + item.group_id;
        let membersList = [];
        membersList.push(new CometChat.GroupMember(comeChatUserTag + selectUser.u_id, CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT))

        CometChat.addMembersToGroup(GUID, membersList, []).then(
            response => {
                getGroupData({ token: token, u_id: user_id })

                // props.navigation.reset({
                //     index: 0,
                //     routes: [{ name: 'TabNavigation' }],
                // });
            },
            error => {
                console.log("Something went wrong", error);
            }
        );
    }
    const addAsEvent = async (item) => {
        refSheetEvent.close();
        let data = new FormData();
        setShowing(true)
        data.append("event_id", item.event_id)
        data.append("event_members[]", selectUser.u_id);

        await imageUploadApi(data, apiUrl.invite_friend_or_group, token).then(res => {
            if (res.status == true) {
                showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getEventData({ token: token, u_id: user_id })
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const eventListView = () => {
        return (
            <View style={{ marginBottom: 20 }}>
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheetGroup.close()}>

                </TouchableOpacity>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {eventdata.map((item, index) => {
                        var memberList = item.event_members.map(a => a.member.u_first_name).toString();
                        return (
                            <View style={styles.mainGroupView}>
                                <View>
                                    <Image style={styles.imgGroup} source={{ uri: item.event_image }}></Image>

                                </View>
                                <View style={styles.radioViewGroup}>
                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={styles.radioText}>{item.event_name}</Text>
                                        <Text style={styles.txtInvite}>{memberList}</Text>
                                    </View>
                                    < CommonButton
                                        btnlinearGradient={[styles.btnlinearGradient, { paddingHorizontal: 20 }]}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        viewStyle={styles.verifyBtn}
                                        onPress={() => addAsEvent(item)}
                                        text={"Add"}
                                    />

                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
    const onRefresh = async () => {

        await getUserData().then(res => {
            setShowing(true)
            setIsFetching(true)
            getFriendsData(res.token)
            getGroupData(res.token, res.u_id)

        })

    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>
                    <View>
                        <SimpleSerach
                            onChangeText={(text) => setSerachText(text)}
                            placeHolder={strings.searchforFriends}
                            isSimpleSerach={true}
                            editable={true}
                            isScreen={true}
                            onPlaceClick={() => console.log("")}
                            onSubmitEditing={() => onSubmitEditing()}

                        />
                    </View>
                    <FlatList
                        data={friendsData}
                        style={{ marginBottom: 100 }}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                        onEndReachedThreshold={0.1}
                        onMomentumScrollBegin={() => { onEndReached = false; }}
                        onEndReached={() => {
                            if (!onEndReached) {
                                handleMoreData(); // on End reached
                                onEndReached = true;
                            }
                        }}
                        refreshing={isFetching}
                        onRefresh={() => onRefresh()}
                    >
                    </FlatList>
                </View>
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={290}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {bottomView()}
                </RBSheet>
                <RBSheet
                    ref={(ref) => refSheetDot = ref}
                    height={270}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    <MoreMenu
                        data={bottomData}
                        clickItem={clickItem}
                    >

                    </MoreMenu>
                </RBSheet>
                <RBSheet
                    ref={(ref) => refSheetGroup = ref}
                    height={240}
                    openDuration={250}
                    //                        closeOnDragDown={true}

                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {groupList()}
                </RBSheet>
                <RBSheet
                    ref={(ref) => refSheetEvent = ref}
                    height={240}
                    openDuration={250}
                    //                        closeOnDragDown={true}

                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {eventListView()}
                </RBSheet>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )

}
export default AllFriend;