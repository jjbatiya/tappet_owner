import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    //render view
    renderSubView: { height: 48, marginVertical: 15, flexDirection: "row", alignItems: "center", justifyContent: 'space-between' },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 48, width: 48, borderRadius: 24 },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        width:150
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular,
        width:150
    },
    //bottom view
    txtHeading: {
        fontWeight: "700", fontSize: 16, lineHeight: 22,
        color: Colors.white, fontFamily: Fonts.DMSansRegular, 
        alignSelf:"center"
    },
    txtLabel: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        letterSpacing:0.1,opacity:0.6,
        alignSelf:"center",marginTop:10

    },
    viewRound:{
        width: 100, height: 100,
        backgroundColor: Colors.opacityColor2,
        borderRadius: 50, alignItems: "center", 
        justifyContent: "center",
        marginTop:20
    },
    txtCall: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        letterSpacing:0.1,
        alignSelf:"center",
        marginVertical:10

    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        marginRight:10
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        paddingHorizontal: responsiveWidth(5)
    },
    btnView: { alignItems: "center", justifyContent: "center" },

    //group bottom
     //group bottom sheet
     mainGroupView: { flexDirection: "row", marginRight: 20, marginVertical: 5, alignItems: "center" },

     imgGroup: {
         height: 48,
         width: 48,
         borderRadius: 54,
     },
     radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
     radioText: {
         fontSize: 16,
         color: '#fff',
         fontWeight: '400',
         lineHeight: 22,
         fontFamily:Fonts.DMSansRegular,
 
     },
     txtInvite: {
         fontWeight: "400",
         fontSize: 12,
         lineHeight: 15.62,
         color: Colors.white,
         opacity: 0.6,
         fontFamily:Fonts.DMSansRegular,
         width:responsiveWidth(40)
 
 
     },
     drawer: {
         borderWidth: 2,
         width: 50,
         alignSelf: 'center',
         marginTop: 5,
         marginBottom: 15,
         borderColor: '#FFFFFF26'
     },
     verifyBtn: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5),  alignSelf: "center" },
     txtAuto: {
         fontSize: 16,
         backgroundColor: Colors.backgroundColor,
         color: Colors.white,
         fontFamily: Fonts.DMSansRegular,
         flex: 1,
         marginLeft:-7,
         height:70
     
     },
})