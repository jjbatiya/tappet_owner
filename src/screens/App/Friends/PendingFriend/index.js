import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './PendingFriendStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import { apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../utils/ToastConfig';


const PendingFriend = (props) => {
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(30)
    const [serachText, setSerachText] = useState("")
    const [u_id, setUserID] = useState("")
    useEffect(() => {
        if (props.index == 2) {
            getUserData().then(res => {
                setShowing(true)
                setUserID(res.u_id)
                setToken(res.token)
                getFriendsData(res.token)
            })
        }
    }, [props.index])
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getFriendsData = async (token) => {
        var obj = {
            page: page,
            limit: limit,
            friend_request: 2,
            search: serachText,
            u_id: u_id
        }
        await apiCallWithToken(obj, apiUrl.my_friends, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    if (res.result.length != 0)
                        categoryWiseData(res.result)
                  
                }
                else {
                    let result1 = friendsData.map(a => a.data);
                    result1 = result1.concat(res.result)
                    categoryWiseData(result1)
                }
            }
            else {
               // showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    setFriendData([])
                }, 1000);
            }
        })
    }
    const categoryWiseData = (data) => {
        let newArray = [];
        for (var i = 1; i < 3; i++) {
            var obj = {
                title: i == 1 ? "RECEIVED" : "SENT",
                id: i,
                data: data.filter((item) => filterData(item, i))
            }
            newArray.push(obj)
        }
        setFriendData(newArray)
    }
    function filterData(data, i) {
        if (i == 1)
            return data.friend_request_sent_by_me == false;
        else
            return data.friend_request_sent_by_me == true;

    }
    const renderItem = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View
                    style={styles.contentView}>
                    <TouchableOpacity  onPress={() => props.navigation.replace(strings.UserProfileScreen, { u_id: item.u_id })}
>
                        <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    </TouchableOpacity>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 1, alignItems: "center" }}>

                        <TouchableOpacity
                            onPress={() => props.navigation.replace(strings.UserProfileScreen, { u_id: item.u_id })}
                            style={{ marginLeft: responsiveWidth(4), alignSelf: "center" }}>
                            <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                            <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends"}</Text>
                        </TouchableOpacity>
                        {item.friend_request_sent_by_me == true ?
                            <TouchableOpacity onPress={() => actionFriend(item.u_id, "3", true)} style={[styles.declineBtn, { marginRight: responsiveWidth(2) }]}>
                                <Text style={styles.txtCancel}>{strings.cancelRequest}</Text>
                            </TouchableOpacity> : null}
                    </View>

                </View>
                {item.friend_request_sent_by_me == false ?
                    <View style={styles.contentView}>
                        <Image style={styles.imgStyle}></Image>
                        <View style={{ marginLeft: responsiveWidth(4), alignSelf: "center", flexDirection: "row" }}>
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={Colors.btnBackgroundColor}
                                btnText={styles.btnText}
                                viewStyle={styles.btnView}
                                onPress={() => actionFriend(item.u_id, "1", false)}
                                text={strings.accept}
                            />
                            <TouchableOpacity style={styles.declineBtn}
                                onPress={() => actionFriend(item.u_id, "3", false)}
                            >
                                <Text style={styles.declineText}>{strings.decline}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                              onPress={() => actionFriend(item.u_id, "3", false)}
                             style={[styles.declineBtn, { marginHorizontal: 0 }]}>
                                <Image style={styles.closeIcon} resizeMode={"contain"} source={Images.closeIcon}></Image>
                            </TouchableOpacity>
                        </View>
                    </View> : null

                }

            </View>
        )
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 5 }}>
                <Text style={styles.titleText}>
                    {item.title}
                    <Text style={[styles.titleText, { opacity: 0.6 }]}>{"(" + item.data.length + ")"}</Text>
                </Text>
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderItem}
                />
            </View>
        )
    }
    const actionFriend = async (user_id, state, isReceive) => {
        setShowing(true)
        var obj = {
            sender_id: isReceive ? u_id : user_id,
            reciever_id: isReceive ? user_id : u_id,
            state: state
        }
        await apiCallWithToken(obj, apiUrl.friend_request_action, token).then(res => {
            showMessage(res.status ? "success" : "error", "Message", res.message)

            setTimeout(() => {
                setShowing(false)
                if (res.status == true)
                    getFriendsData(token);

            }, 1000);

        })
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>
                    <FlatList
                        data={friendsData}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderView}
                    >
                    </FlatList>

                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )

}
export default PendingFriend;