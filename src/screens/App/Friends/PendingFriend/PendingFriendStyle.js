import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    //render view
    renderSubView: { marginVertical: 10, justifyContent: 'space-between' },
    contentView: { flexDirection: "row" },
    imgStyle: { height: 48, width: 48, borderRadius: 24 },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        width:150
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular,
        width:150
    },
    titleText: {
        fontSize: 14, fontWeight: "700",
        lineHeight: 18.23, color: Colors.white,
        fontFamily: Fonts.DMSansRegular, textTransform: "uppercase"
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        paddingHorizontal: responsiveWidth(10)
    },
    btnView: { alignItems: "center", justifyContent: "center" },
    declineBtn: {
        backgroundColor: Colors.opacityColor2,
        borderWidth: 1, borderColor: Colors.opacityColor2,
        alignItems: "center", justifyContent: "center", borderRadius: 4,
        marginLeft: 7
    },
    declineText: {
        fontSize: 12, fontWeight: "400",
        lineHeight: 15.62, color: Colors.white, letterSpacing: 0.1, paddingHorizontal: responsiveWidth(10)
        ,
        fontFamily: Fonts.DMSansRegular,

    },
    closeIcon: { height: 20, width: 20, tintColor: Colors.white, paddingHorizontal: 15 },
    txtCancel: {
        fontSize: 12, fontWeight: "400",
        lineHeight: 15.62, color: Colors.white, letterSpacing: 0.1,
        paddingHorizontal: responsiveWidth(2), paddingVertical: 5, fontFamily: Fonts.DMSansRegular,

    }

})