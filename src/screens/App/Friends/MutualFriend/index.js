import React, { useEffect, useRef, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from '../AllFriend/AllFriendStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import ChatMessage from '../../../../utils/svg/ChatMessage';
import CallFriends from '../../../../utils/svg/CallFriends';
import { apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../utils/ToastConfig';


const MutualFriend = (props) => {
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(10)
    const [serachText, setSerachText] = useState("")
    var onEndReached;
    const [u_id, setUserId] = useState("")
    let refSheet = useRef();
    useEffect(() => {
        getUserData().then(res => {
            setFriendData([])
            setShowing(true)
            setUserId(res.u_id)
            setToken(res.token)
            getFriendsData(res.token, res.u_id)
        })

    }, [props.index])
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getFriendsData = async (token, u_id) => {
        var obj = {
            page: page,
            limit: limit,
            search: serachText,
            user_id: u_id,
            friend_id: props.friend_id
        }
        await apiCallWithToken(obj, props.index == 0 ? apiUrl.get_mutual_friends : apiUrl.get_other_friends, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    if (res.result.length != 0)
                        setFriendData(res.result)
                    else
                        showMessage(strings.error, strings.msgTag, res.message)
                }
                else {
                    setFriendData(friendsData.concat(res.result))
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    setFriendData([])
                }, 1000);
            }
        })
    }
    const onAddFriend = async (item) => {
        setShowing(true)
        var obj = {
            invited_user_id: item.u_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    getFriendsData(token, u_id);

                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderItem = ({ item, index }) => {
        return (
            <View
                style={styles.renderSubView}>
                <TouchableOpacity   onPress={() => props.navigation.replace(strings.UserProfileScreen, { u_id: item.u_id })} style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pets"}</Text>
                    </View>
                </TouchableOpacity>
                {props.index == 0 ?
                    <View style={{ flexDirection: "row", marginRight: 10 }}>
                        <TouchableOpacity>
                            <Image style={{ width: 16, height: 16, tintColor: Colors.white, marginRight: 10 }} source={Images.contact}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => refSheet.open()}>
                            <Image style={{ width: 14, height: 14, tintColor: Colors.white }} source={Images.menuDot}></Image>
                        </TouchableOpacity>
                    </View> :
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => onAddFriend(item)}
                        text={strings.addfriend}
                    />}
            </View>
        )
    }
    const bottomView = () => {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={styles.txtHeading}>Choose Connection Type</Text>
                <Text style={styles.txtLabel}>How would you like to connect?</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: responsiveWidth(10) }}>
                    <View>
                        <TouchableOpacity style={styles.viewRound}>
                            <CallFriends />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Call</Text>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.viewRound}>
                            <ChatMessage />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Message</Text>
                    </View>
                </View>
            </View>
        )
    }
    const onSubmitEditing = () => {
        setShowing(true)
        getFriendsData(token, u_id)
    }
    const handleMoreData = () => {
        if (totalPage > page) {
            setPage(page + 1)
            setShowing(true)
            getFriendsData(token, u_id)
        }

    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>
                    <View>
                        <SimpleSerach
                            onChangeText={(text) => setSerachText(text)}
                            placeHolder={strings.searchforFriends}
                            isSimpleSerach={true}
                            editable={true}
                            isScreen={true}
                            onSubmitEditing={() => onSubmitEditing()}

                        />
                    </View>
                    {isshowing == false &&
                        <FlatList
                            data={friendsData}
                            style={{ marginBottom: 100 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={renderItem}
                            onEndReachedThreshold={0.1}
                            onMomentumScrollBegin={() => { onEndReached = false; }}
                            onEndReached={() => {
                                if (!onEndReached) {
                                    handleMoreData(); // on End reached
                                    onEndReached = true;
                                }
                            }}
                        >
                        </FlatList>}
                </View>
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={290}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {bottomView()}
                </RBSheet>

            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )

}
export default MutualFriend;