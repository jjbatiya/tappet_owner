import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, Modal, FlatList, ScrollView } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';
import { Images } from '../../../../../Resources/Images';
import { BarChart, ProgressCircle, Grid, XAxis } from 'react-native-svg-charts'

const TapActivity = (props) => {
    const barChart = (title, color) => {
        const fill = 'rgb(255, 255, 255,0.2)'
        const data = [50, 10, 40, 95, 60, 30, 20, 45, 85]

        return (
            <View
                style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ height: 8, width: 8, backgroundColor: color, marginRight: 10 }} />
                    <Text style={[styles.txtTitle, { marginTop: 0, textTransform: "uppercase" }]}>{title}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: 0.3,marginTop:responsiveHeight(9)}} >
                        <Text style={[styles.txtTime, { marginVertical: 0, alignSelf: "flex-start" }]}>{"14% better"}</Text>
                        <Text style={[styles.txtLabel, { alignSelf: "flex-start", paddingTop: 20 }]}>{"German Shepherd"}</Text>
                        <Text style={[styles.txtLabel, { alignSelf: "flex-start" }]}>{"4.4 miles"}</Text>

                    </View>
                    <View style={[styles.horizontalLine, { backgroundColor: color }]}></View>

                    <View style={{ flex: 0.7,height:responsiveHeight(50) }}>
                        <BarChart style={{ height: 200 }} spacingInner={0.5} data={data} svg={{ fill }} contentInset={{ top: 30, bottom: 30 }}>
                        </BarChart>
                        <XAxis
                            data={data}
                            style={{ flex: 1 ,height:100}}
                            formatLabel={(value, index) => index}
                            contentInset={{ left: 10, right: 10 }}
                            svg={{ fontSize: 10, fill: Colors.white, rotation: 45 }}
                        />
                    </View>
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <View >
                        <TouchableOpacity
                            onPress={() => props.navigation.goBack(null)}
                            style={styles.closeStyle}>
                            <Image style={styles.imgClose} source={Images.closeIcon}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems: "center", justifyContent: "center", marginHorizontal: 40 }}>
                        <ProgressCircle style={{ height: 140, width: 140 }} progress={0.7} backgroundColor={'rgb(255,185,27,0.2)'} strokeWidth={6.17} progressColor="#FFB91B" />
                        <ProgressCircle style={{ height: 110, width: 110, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(255,45,101,0.2)'} progress={1} progressColor='#FF2D65' />
                        <ProgressCircle style={{ height: 85, width: 84, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(0,205,249,0.2)'} progress={0.7} progressColor={"#00CDF9"} />
                    </View>
                    {barChart("Other Pets", "#FF2D65")}

                </View>
            </LinearGradient>
        </View>

    )
}
const styles = StyleSheet.create({
    closeStyle: { marginLeft: 20, marginTop: 60 },
    imgClose: { height: 20, width: 20, tintColor: Colors.white },
    txtTitle: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular, marginTop: 60,
        fontSize: 16, lineHeight: 22, color: Colors.white
    },
    txtTime: {
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, fontSize: 14, color: Colors.white, letterSpacing: 0.1, marginVertical: responsiveHeight(0.8)
    },
    txtLabel: { fontSize: 10, fontWeight: "400", lineHeight: 14, color: Colors.white, opacity: 0.6 },
    horizontalLine: { backgroundColor: '#FF2D65', width: '100%', position: "absolute", top: responsiveHeight(11), height: 1 },


})
export default TapActivity;