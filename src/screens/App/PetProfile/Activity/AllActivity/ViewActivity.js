import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, Modal, FlatList, ScrollView, Platform } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';
import { Images } from '../../../../../Resources/Images';
import { BarChart, ProgressCircle, Grid, XAxis } from 'react-native-svg-charts'
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../../../Components/MoreMenu';
import SimpleSerach from '../../../../../Components/SimpleSerach';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { strings } from '../../../../../Resources/Strings';
var FloatingLabel = require('react-native-floating-labels');

const bottomData = [
    {
        key: "shareasMessage",
        title: "Share as Message",
        image: Images.share
    },
    {
        key: "delete",
        title: "Delete this Activity",
        image: Images.deleteIcon
    },
]
const data = [
    {
        groupName: "B4 Apartment, Church S..",
        names: "Sent 2h ago",
        image: Images.backGround,
        id: 1,
        isGroup: true,
        isSend: true
    },
    {
        groupName: "B4 Apartment, Church S..",
        names: "Sent 2h ago",
        image: Images.people,
        id: 2,
        isGroup: false,
        isSend: true

    },
    {
        groupName: "Workplace Dog group",
        names: "Sent 2h ago",
        image: Images.backGround,
        id: 3,
        isGroup: true,
        isSend: true

    },
    {
        groupName: "Workplace Dog group",
        names: "Last Sent 2h ago",
        image: Images.people1,
        id: 4,
        isGroup: false,
        isSend: false
    },

]
const ViewActivity = (props) => {
    let refSheet = useRef();
    let refSheetShare = useRef();
    const pieView = () => {
        return (
            <View style={styles.chartView}        >
                <View style={{ alignItems: "center", justifyContent: "center", marginHorizontal: 40 }}>
                    <ProgressCircle style={{ height: 140, width: 140 }} progress={0.7} backgroundColor={'rgb(255,185,27,0.2)'} strokeWidth={6.17} progressColor="#FFB91B" />
                    <ProgressCircle style={{ height: 110, width: 110, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(255,45,101,0.2)'} progress={1} progressColor='#FF2D65' />
                    <ProgressCircle style={{ height: 85, width: 84, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(0,205,249,0.2)'} progress={0.7} progressColor={"#00CDF9"} />
                </View>
                <View style={{width:160, }} //style={{ alignItems: "center", justifyContent: "center", marginRight: 40 }}
                >
                    <View style={{ flexDirection: "row", }}>
                        <View style={styles.viewBox1}></View>
                        <View style={{flex:1}}>
                            <Text style={styles.txtMiles}>{"4.4 / 6 miles"}</Text>
                            <Text style={styles.txtLabel}>{"Total run"}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                        <View style={styles.viewBox2}></View>
                        <View style={{flex:1}}>
                            <Text style={styles.txtMiles}>{"6.4 / 4.5 miles\n14% better"}</Text>
                            <Text style={styles.txtLabel}>{"Than other pets"}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 10,}}>
                        <View style={styles.viewBox3}></View>
                        <View style={{flex:1}}>

                            <Text style={styles.txtMiles}>{"4.4 / 7 miles\n14% better"}</Text>
                            <Text style={styles.txtLabel}>{"Than friend’s pets"}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    const barChart = (title, color) => {
        const fill = 'rgb(255, 255, 255,0.2)'
        const data = [50, 10, 40, 95, 60, 30, 20, 45, 85]

        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.tapActivityScreen)}
                style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ height: 8, width: 8, backgroundColor: color, marginRight: 10 }} />
                    <Text style={[styles.txtTitle, { marginTop: 0, textTransform: "uppercase" }]}>{title}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>

                    <View style={{ flex: 0.3, alignItems: "center", justifyContent: "center" }} >
                        <Text style={[styles.txtTime, { marginVertical: 0, alignSelf: "flex-start" }]}>{"14% better"}</Text>
                        <Text style={[styles.txtLabel, { alignSelf: "flex-start", paddingTop: 20 }]}>{"German Shepherd"}</Text>
                        <Text style={[styles.txtLabel, { alignSelf: "flex-start" }]}>{"4.4 miles"}</Text>

                    </View>
                    <View style={[styles.horizontalLine, { backgroundColor: color }]}></View>

                    <View style={{ flex: 0.7 }}>
                        <BarChart style={{ height: 200 }} spacingInner={0.5} data={data} svg={{ fill:Colors.opacityColor2 }} contentInset={{ top: 30, bottom: 30 }}>
                        </BarChart>
                      
                            <XAxis
                                data={data}
                                style={{flex:1}}
                                formatLabel={(value, index) =>index}
                                contentInset={{ left: 10, right: 10 }}
                                svg={{ fontSize: 10, fill: Colors.white,rotation:-45 }}
                            />
                    </View>
                </View>
                <View style={{ width: '100%', marginTop: 40, height: 1, backgroundColor: Colors.opacityColor }}></View>
            </TouchableOpacity>
        )
    }
    const shareView = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => refSheetShare.close()} style={styles.viewHandle} />

                <View style={{ marginHorizontal: responsiveWidth(3), flexDirection: "row" }}>
                    <View style={styles.viewProgress}>
                        <ProgressCircle style={{ height: 45, width: 45 }} progress={0.7} backgroundColor={'rgb(255,185,27,0.2)'} strokeWidth={3.17} progressColor="#FFB91B" />
                        <ProgressCircle style={{ height: 30, width: 30, position: "absolute" }} strokeWidth={3.17} backgroundColor={'rgb(255,45,101,0.2)'} progress={1} progressColor='#FF2D65' />
                        <ProgressCircle style={{ height: 20, width: 20, position: "absolute" }} strokeWidth={3.17} backgroundColor={'rgb(0,205,249,0.2)'} progress={0.7} progressColor={"#00CDF9"} />
                    </View>
                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        // onChangeText={(text) => onChangeText(text)}
                        style={styles.focusforminput}
                    >{"Write a message"}</FloatingLabel>

                </View>
                <View style={{ marginTop: responsiveHeight(1) }}>
                    <SimpleSerach
                        onChangeText={(text) => console.log(text)}
                        placeHolder={"Search for friends or groups"}
                        isSimpleSerach={true}
                        editable={true}
                        onSubmitEditing={()=>console.log("dd")}

                    />
                </View>
                <TouchableOpacity style={styles.mainGroupView}>
                    <View>
                        <Image style={styles.imgGroup} source={Images.people}></Image>
                        <View style={styles.viewPlus}>
                            <Text style={{ color: Colors.black, alignSelf: "flex-start", fontSize: 12 }}>+</Text>
                        </View>
                    </View>
                    <Text style={styles.txtSharePost}>{"Share as a Post"}</Text>
                </TouchableOpacity>
                { data.map((item, index) => {
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={item.image}></Image>
                                {item.isGroup &&
                                    <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                        <Image style={styles.peopleImg} source={Images.people1}></Image>
                                        <Image style={styles.peopleImg1} source={Images.people}></Image>

                                    </View>
                                }
                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.groupName}</Text>
                                    <Text style={styles.txtInvite}>{item.names}</Text>
                                </View>
                                {item.isSend ?
                                    <TouchableOpacity
                                        style={styles.btnView}
                                    >
                                        <Text style={styles.txtUndo}>{"Undo"}</Text>
                                    </TouchableOpacity> :
                                    <LinearGradient
                                        colors={Colors.btnBackgroundColor}
                                        style={styles.btnView}>
                                        <Text style={styles.txtUndo}>{"Send"}</Text>

                                    </LinearGradient>
                                }
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
    const middleView = () => {
        return (
            <View style={styles.viewMiddle}>
                <View style={{ alignItems: "center", justifyContent: "center", marginRight: responsiveWidth(6) }}>

                    <Image style={styles.iconFire} source={Images.flag}></Image>
                    <Text style={styles.txtTime}>{"4.4 Miles"}</Text>
                    <Text style={styles.txtLabel1}>{"Total Distance"}</Text>
                </View>
                <View style={{ alignItems: "center", justifyContent: "center", marginRight: responsiveWidth(6) }}>
                    <Image style={styles.iconFire} source={Images.clock}></Image>
                    <Text style={styles.txtTime}>{"01h 03m"}</Text>
                    <Text style={styles.txtLabel1}>{"Total Time"}</Text>
                </View>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <Image style={styles.iconFire} source={Images.fire}></Image>
                    <Text style={styles.txtTime}>{"58 Cal"}</Text>
                    <Text style={styles.txtLabel1}>{"Calories Burnt"}</Text>
                </View>
            </View>
        )
    }
    const clickItem = (item) => {
        console.log(item)
        refSheet.close();
        setTimeout(() => {
            if (item.key == "shareasMessage")
                refSheetShare.open();
        }, 500);
    }
    return (
        <View style={{ flex: 1 }}>
   {Platform.OS=="android"?
            <CommonStatusBar color={Colors.backgroundColor} />:<></>}
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={styles.topView}>
                            <TouchableOpacity
                                onPress={() => props.navigation.goBack(null)}
                                style={styles.closeStyle}>
                                <Image style={styles.imgClose} source={Images.closeIcon}></Image>
                            </TouchableOpacity>
                            <Text style={styles.txtTitle}>{"September 7, 21"}</Text>
                            <TouchableOpacity onPress={() => refSheet.open()}>
                                <Image style={styles.imgDot} source={Images.menuDot}></Image>
                            </TouchableOpacity>

                        </View>
                        {pieView()}
                        {middleView()}
                        {barChart("Other Pets", "#FF2D65")}
                        {barChart("Friend’s Pets", "#00CDF9")}
                        <Text style={[styles.txtLabel1, { margin: 20, alignSelf: "center", textAlign: "center", opacity: 1, color: Colors.opacityColor3 }]}>Data recorded by Tap Pet collar. By using Pet Tap collar, you agree to our
                        <Text style={styles.txtTerms}>{" Terms & Condition"}</Text> of sharing this data</Text>
                    </View>
                </ScrollView>
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={132}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    <MoreMenu
                        data={bottomData}
                        clickItem={(item) => clickItem(item)}
                    >

                    </MoreMenu>
                </RBSheet>
                <RBSheet
                    ref={(ref) => refSheetShare = ref}
                    height={550}
                    openDuration={250}
                    closeOnDragDown={false}
                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 10,
                        }
                    }}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {shareView()}
                    </ScrollView>
                </RBSheet>
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    closeStyle: { marginLeft: 20, marginTop: 60 },
    imgClose: { height: 20, width: 20, tintColor: Colors.white },
    txtTitle: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular, marginTop: 60,
        fontSize: 16, lineHeight: 22, color: Colors.white
    },
    imgDot: {
        height: 18, width: 18, tintColor: Colors.white, marginTop: 60
    },
    topView: { flexDirection: "row", justifyContent: "space-between", width: "97%" },
    //pie chart view

    chartView: {
        flexDirection: "row", justifyContent: "space-between",
        borderRadius: 4, marginTop: responsiveHeight(4)
    },
    viewBox1: { backgroundColor: '#FFB91B', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5,},
    txtMiles: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, color: Colors.white },
    txtLabel: { fontSize: 10, fontWeight: "400", lineHeight: 14, color: Colors.white, opacity: 0.6 },
    viewBox2: { backgroundColor: '#FF2D65', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5, },
    viewBox3: { backgroundColor: '#00CDF9', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5,},
    //middle view
    txtTime: {
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, fontSize: 14, color: Colors.white, letterSpacing: 0.1, marginVertical: responsiveHeight(0.8)
    },
    txtLabel1: {
        fontSize: 12,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 14, color: Colors.white,
        opacity: 0.6
    },
    iconFire: { height: 15, width: 15, tintColor: Colors.white },
    viewMiddle: {
        flexDirection: "row", justifyContent: "space-between",
        alignItems: "center", marginTop: 20, marginHorizontal: 20,
        borderTopWidth: 1, borderColor: Colors.opacityColor,
        paddingVertical: 20,
        paddingHorizontal: 15,
        borderBottomWidth: 1
    },
    horizontalLine: { backgroundColor: '#FF2D65', width: '100%', position: "absolute", top: responsiveHeight(11), height: 1 },

    //share view bottom
    viewHandle: { alignSelf: "center", height: 4, width: 45, backgroundColor: Colors.white, opacity: 0.2, padding: 2 },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.3,


    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        flex: 0.8,
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3
    },
    mainGroupView: { flexDirection: "row", marginHorizontal: 20, marginVertical: 10, alignItems: "center" },

    imgGroup: {
        height: 48,
        width: 48,
        borderRadius: 54,
    },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    peopleImg: { height: 20, width: 20, borderRadius: 15 },
    peopleImg1: { height: 20, width: 20, borderRadius: 15, marginLeft: -8 },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
    },
    btnView: {
        height: 28,
        width: 64,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(1),
        backgroundColor: Colors.opacityColor2
    },
    txtUndo: {
        opacity: 0.6,
        fontSize: 12, lineHeight: 18.23,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        color: Colors.white,
    },
    viewProgress: {
        alignItems: "center", justifyContent: "center"
        , backgroundColor: Colors.progressBackground,
        height: 55, width: 55, alignSelf: "center"
    },
    viewPlus: {
        flexDirection: "row", height: 16, width: 16, borderRadius: 8,
        borderWidth: 1, borderColor: Colors.theme,
        position: "absolute", bottom: 0, right: 0, backgroundColor: Colors.primaryViolet,
        alignItems: "center", justifyContent: "center"
    },
    txtSharePost: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400", color: Colors.primaryViolet, fontSize: 14, lineHeight: 18.23, marginLeft: 20
    },
    txtTerms: {
        fontSize: 13,
        fontFamily: Fonts.DMSansBold,
        lineHeight: 15.62,
        letterSpacing: 0.1, color: Colors.white
    },

})
export default ViewActivity;

