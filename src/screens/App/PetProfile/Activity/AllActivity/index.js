import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import { styles } from './AllActivityStyle';
import MapView, { Polyline } from 'react-native-maps';
import { mapStyle } from '../../../../../Resources/MapStyle';
import { ProgressCircle } from 'react-native-svg-charts'
import Calendar from '../../../../../Components/Calender/CalenderComponent';

const origin = { latitude: 37.3318456, longitude: -122.0296002 };
const destination = { latitude: 37.771707, longitude: -122.4053769 };
const GOOGLE_MAPS_APIKEY = '';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const AllActivity = (props) => {
    console.log(props)
    var date = new Date();
    const [arrayData, setArrayData] = useState([])
    const onSelectDate = (date) => {
        //alert(date)
        //this.setState({ events: filterEvents(date) });
        //setEvents( filterEvents(date) )
    };
    const progressActivity = (item) => {
        return (

            <TouchableOpacity style={styles.chartView}
                onPress={() => props.navigation.navigation.navigate(strings.viewActivityScreen)}

            >
                <View style={{ alignItems: "center", justifyContent: "center", marginHorizontal: 40 }}>
                    <ProgressCircle style={{ height: 140, width: 140 }} progress={0.7} backgroundColor={'rgb(255,185,27,0.2)'} strokeWidth={6.17} progressColor="#FFB91B" />
                    <ProgressCircle style={{ height: 110, width: 110, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(255,45,101,0.2)'} progress={1} progressColor='#FF2D65' />
                    <ProgressCircle style={{ height: 85, width: 84, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(0,205,249,0.2)'} progress={0.7} progressColor={"#00CDF9"} />
                </View>
                <View style={{width:160, marginTop:50}} 
                >
                    <View style={{ flexDirection: "row", }}>
                        <View style={styles.viewBox1}></View>
                        <View >
                            <Text numberOfLines={2} style={styles.txtMiles}>{"4.4 / 6 miles"}</Text>
                            <Text style={styles.txtLabel}>{"Total run"}</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                        <View style={styles.viewBox2}></View>
                        <View >
                            <Text style={styles.txtMiles}>{"6.4 / 4.5 miles\n14% better"}</Text>
                            <Text style={styles.txtLabel}>{"Than other pets"}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                        <View style={styles.viewBox3}></View>
                        <View  >
                            <Text style={styles.txtMiles}>{"4.4 / 7 miles\n14% better"}</Text>
                            <Text style={styles.txtLabel}>{"Than friend’s pets"}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ flex: 1 }}>
                    <MapView
                        customMapStyle={mapStyle}
                        onPress={() => props.navigation.navigation.navigate(strings.viewMapScreen)}
                        style={{
                            flex: 0.6,
                        }}
                        provider={"google"}
                        mapType={strings.mapType}
                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}

                    >

                        <Polyline
                            coordinates={[
                                { latitude: 37.8025259, longitude: -122.4351431 },
                                { latitude: 37.7896386, longitude: -122.421646 },
                                { latitude: 37.7665248, longitude: -122.4161628 },
                                { latitude: 37.7734153, longitude: -122.4577787 },
                                { latitude: 37.7948605, longitude: -122.4596065 },
                                { latitude: 37.8025259, longitude: -122.4351431 },
                            ]}
                            strokeColors={[
                                '#DD46F6',
                                '#7235FF', // no color, creates a "long" gradient between the previous and next coordinate
                            ]}
                            strokeWidth={3}
                        />
                    </MapView>
                    <View style={{ position: "absolute", top: 0, flexDirection: "row" }}>
                        <Calendar showDaysAfterCurrent={30} onSelectDate={onSelectDate} />

                    </View>
                    <View style={{ flex: 0.4 }}>
                        {progressActivity()}
                    </View>
                </View>
            </LinearGradient>
        </View>
    )
}

export default AllActivity;