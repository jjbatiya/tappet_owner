import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, Modal, FlatList, ScrollView } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MapView,{Polyline} from 'react-native-maps';
import { mapStyle } from '../../../../../Resources/MapStyle';
import { Colors } from '../../../../../Resources/Colors';
import { Images } from '../../../../../Resources/Images';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { strings } from '../../../../../Resources/Strings';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const ViewMap = (props) => {
    return (
        <View style={{ flex: 1 }}>
            <CommonStatusBar/>
            <MapView
                customMapStyle={mapStyle}
                style={{
                    flex: 1,
                }}
                provider={"google"}
                mapType={strings.mapType}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta:LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}

            >

                <Polyline
                    coordinates={[
                        { latitude: 37.8025259, longitude: -122.4351431 },
                        { latitude: 37.7896386, longitude: -122.421646 },
                        { latitude: 37.7665248, longitude: -122.4161628 },
                        { latitude: 37.7734153, longitude: -122.4577787 },
                        { latitude: 37.7948605, longitude: -122.4596065 },
                        { latitude: 37.8025259, longitude: -122.4351431 },
                    ]}
                    strokeColors={[
                        '#DD46F6',
                        '#7235FF', // no color, creates a "long" gradient between the previous and next coordinate
                    ]}
                    strokeWidth={3}
                />
            </MapView>
            <TouchableOpacity 
            onPress={()=>props.navigation.goBack(null)}
             style={{position:"absolute",top:0,marginHorizontal:20,marginTop:60}}>
                <Image style={{height:20,width:20,tintColor:Colors.white}} source={Images.closeIcon}></Image>
            </TouchableOpacity>
        </View>
    )
}
export default ViewMap;