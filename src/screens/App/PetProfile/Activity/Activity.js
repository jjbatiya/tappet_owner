import React, { useEffect, useRef, useState } from 'react';
import { View, Text, useWindowDimensions, SafeAreaView, FlatList, Image, ScrollView, Platform } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import styles from './ActivityStyle'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import AllActivity from './AllActivity';
import StartRun from './StartRun';
import CommonStatusBar from '../../../../Components/CommonStatusBar';

const Activity = (props) => {
    const layout = useWindowDimensions();
    const [petData, setPetData] = useState(props.backPressEvent.petData)
    const[u_image]=useState(props.backPressEvent?.userImage)
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'startRun', title: 'Start Run' },
        { key: 'allActivity', title: 'All Activity' },
    ]);
    const renderScene = ({ route,index }) => {
        switch (route.key) {
            case 'startRun':
                return <StartRun navigation={props.navigation}  petData={props.backPressEvent.petData}/>;
            case 'allActivity':
                return <AllActivity navigation={props.navigation}  petData={props.backPressEvent.petData}/>;
            default:
                return null;
        }
    };
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}

        />
    );
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height:Platform.OS=="ios"?responsiveHeight(12.4):responsiveHeight(10) }}
            >
                <CommonStatusBar/>
                <AppHeader
                    navigation={props.navigation.navigation}
                    title={'Activity'}
                    isProfileImage={true}
                    profileImage={{uri:u_image}}
                    titleText={{ marginLeft: -30 }}
                    rightTitlePressed={() => alert("TEST")}
                    // rightIconLeft={Images.menuDot}
                    Tab={true}
                    rightIcon={Images.calendarTab}
                />
            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}
            />
           
        </View >
    )
}
export default Activity;