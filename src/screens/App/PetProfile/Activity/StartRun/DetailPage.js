import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import { Fonts } from '../../../../../Resources/Fonts';
import MapView from 'react-native-maps';
import { mapStyle } from '../../../../../Resources/MapStyle';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { ProgressCircle } from 'react-native-svg-charts'
import LowBattary from '../../../../../Components/LowBattary';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import Toast from 'react-native-toast-message';
import Loader from '../../../../../Components/Loader';
import { apiCallWithToken, getUserData } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import { toastConfig } from '../../../../../utils/ToastConfig';

const DetailPage = (props) => {
    const [second, setSecond] = useState(4)
    const [isActivity, setActivity] = useState(false)
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [pet_id] = useState(props.route.params.pet_id)
    const [pet_activity_id] = useState(props.route.params.pet_activity_id)
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
        })

        if (second > 0) {
            var data = second - 1;
            setTimeout(() => {
                setSecond(data)
            }, 1000);
        }
        // setTimeout(() => {
        //     setActivity(true)
        // }, 6000);
    }, [second])
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const stopRun = async () => {
        let currentDate = new Date();
        let dateTime = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate() + " " + currentDate.getHours() + ":" + currentDate.getMinutes();

        setShowing(true)
        var obj = {
            pet_id: pet_id,
            end_date_time: dateTime,
            pet_activity_id: pet_activity_id
        }
        await apiCallWithToken(obj, apiUrl.stop_run, token).then(res => {
            setShowing(false)
            if (res.status == true) {
               // showMessage("success", "Message", res.message)
                props.navigation.goBack(null)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    return (
        <View style={{ flex: 1 }}>
            {Platform.OS == "android" ?
                <CommonStatusBar color={Colors.backgroundColor} /> : <></>}
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <MapView
                customMapStyle={mapStyle}
                style={{
                    flex: 1,
                }}
                provider={"google"}
                mapType={strings.mapType}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}

            >
                <MapView.Marker
                    coordinate={{ latitude: 37.78825, longitude: -122.4324 }}
                    onPress={() => console.log("hello")}>
                    <View >
                        <Image style={styles.markerIcon} source={Images.dogIcon2}></Image>
                    </View>
                </MapView.Marker>
            </MapView>
            <View style={{ flexDirection: "row", justifyContent: "space-between", top: 0, position: "absolute", width: "90%" }}>
                <TouchableOpacity
                    onPress={() => props.navigation.goBack(null)}
                    style={styles.closeStyle}>
                    <Image style={styles.imgClose} source={Images.closeIcon}></Image>
                </TouchableOpacity>
                <Text style={styles.txtTitle}>Pet Run</Text>
                <Text style={styles.txtTitle}></Text>

            </View>
            <View style={styles.topView}>
                {second == 0 ?
                    <View style={styles.viewStyleProgress}>
                        <ProgressCircle style={styles.progrssView} progress={0.7} progressColor={"#FFB91B"} strokeWidth={7} />
                        <View style={{ position: "absolute", alignItems: "center", justifyContent: "center" }}>
                            <Text style={styles.txtSecond}>{4.5}</Text>
                            <Text style={styles.txtStarting}>{strings.miles}</Text>
                        </View>
                    </View> :
                    <View style={styles.emptyRound} >
                        <Text style={styles.txtSecond}>{second}</Text>
                        <Text style={styles.txtStarting}>{strings.starting}</Text>
                    </View>}
            </View>
            <View style={styles.viewBottom}>
                <View style={{ alignItems: "center", justifyContent: "center", marginLeft: responsiveWidth(6) }}>
                    <Image style={styles.iconFire} source={Images.clock}></Image>
                    <Text style={styles.txtTime}>{"00:00"}</Text>
                    <Text style={styles.txtLabel}>{"Total Time"}</Text>
                </View>
                <TouchableOpacity onPress={() => stopRun()} style={styles.viewRound}>
                    <Text style={styles.txtStop}>{"STOP"}</Text>
                </TouchableOpacity>
                <View style={{ alignItems: "center", justifyContent: "center", marginRight: responsiveWidth(6) }}>
                    <Image style={styles.iconFire} source={Images.fire}></Image>
                    <Text style={styles.txtTime}>{"45"}</Text>
                    <Text style={styles.txtLabel}>{"Calories Burnt"}</Text>
                </View>
            </View>
            <LowBattary
                isVisible={isActivity}
                textHeader={"No Activity"}
                iconTop={Images.lowBattery}
                txtDetail={"You have not recorded any activity for today. Tap on the button to start recording."}
                btnText={"Continue"}
                clickBtn={() => setActivity(false)}
            />
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
const styles = StyleSheet.create({
    closeStyle: { marginLeft: 20, marginTop: 60 },
    imgClose: { height: 20, width: 20, tintColor: Colors.white },
    markerIcon: { height: 40, width: 40, borderRadius: 20 },
    //
    topView: {
        alignItems: "center", justifyContent: "center",
        height: 160, flex: 1, marginTop: responsiveHeight(13), position: "absolute", top: 0, alignSelf: "center"
    },
    emptyRound: {
        height: 160, width: 160,
        borderRadius: 80, borderWidth: 8, borderColor: Colors.opacityColor2,
        alignItems: "center",
        justifyContent: "center"
    },
    txtSecond: {
        fontFamily: Fonts.DMSansBoldItalic,
        fontWeight: "500", color: Colors.white,
        fontSize: 52, lineHeight: 67.7, letterSpacing: 0.2, textAlign: "center",
    },
    txtStarting: {
        fontFamily: Fonts.DMSansBoldItalic, letterSpacing: 0.1,
        fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white
    },
    progrssView: {
        height: 160, width: 160,
        alignItems: "center",
        justifyContent: "center",
    },
    viewStyleProgress: { alignItems: "center", justifyContent: "center", height: 160, width: 160 },
    txtTitle: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular, marginTop: 60,
        fontSize: 16, lineHeight: 22, color: Colors.white
    },
    viewBottom: {
        position: "absolute",
        bottom: 0,
        backgroundColor: Colors.theme,
        height: 118,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    txtTime: {
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, fontSize: 14, color: Colors.white, letterSpacing: 0.1, marginVertical: responsiveHeight(0.8)
    },
    txtLabel: {
        fontSize: 12,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 14, color: Colors.white,
        opacity: 0.6
    },
    iconFire: { height: 15, width: 15, tintColor: Colors.white },
    viewRound: {
        height: 106, width: 106,
        backgroundColor: Colors.red,
        borderRadius: 53,
        bottom: 59,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        marginLeft: 20
    },
    txtStop: {
        fontFamily: Fonts.DMSansBoldItalic,
        letterSpacing: 0.2,
        lineHeight: 22,
        fontSize: 16,
        color: Colors.white

    }
})
export default DetailPage;