import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import { Fonts } from '../../../../../Resources/Fonts';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { apiCallWithToken, getUserData } from '../../../../../utils/helper';
const { width } = Dimensions
import Toast from 'react-native-toast-message';
import Loader from '../../../../../Components/Loader';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import { toastConfig } from '../../../../../utils/ToastConfig';

const StartRun = (props) => {
    const[petData]=useState(props.petData)
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
        })
    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
   const startRun=async()=>{
       let currentDate=new Date();
       let dateTime= currentDate.getFullYear()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getDate()+" "+currentDate.getHours()+":"+currentDate.getMinutes();

    setShowing(true)
    var obj = {
        pet_id: petData.pet_id,
        start_date_time: dateTime
    }
    await apiCallWithToken(obj, apiUrl.start_run, token).then(res => {
        setShowing(false)
        if (res.status == true) {
            //showMessage("success", "Message", res.message)
            setTimeout(() => {
                props.navigation.navigation.navigate(strings.detailPageScreen,{pet_id:petData.pet_id,pet_activity_id:res.result.pet_activity_id})
            }, 500);
        }
        else {
            showMessage(strings.error, strings.msgTag, res.message)
        }
    })
   }
    return (
        <View style={{ flex: 1 }}>
            <CommonStatusBar color={Colors.backgroundColor} />
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <Image style={{ flex: 1, height: "100%", width: "100%" }} source={Images.dogIcon2}></Image>
                <LinearGradient colors={Colors.btnBackgroundColor}
                    style={{ position: "absolute", bottom: 20, height: 114, width: 114, borderRadius: 57, alignSelf: "center" }}>
                    <TouchableOpacity style={{
                        position: "absolute", bottom: 0, height: 114,
                        width: 114, borderRadius: 57, alignItems: "center", justifyContent: "center"
                    }}
                        onPress={() =>startRun()}
                    >
                        <Text style={{
                            fontSize: 16, fontWeight: "700", color: Colors.white,
                            lineHeight: 22, fontFamily: Fonts.DMSansBoldItalic
                        }}>{"START RUN"}</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default StartRun;