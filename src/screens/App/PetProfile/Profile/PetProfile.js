import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList, Image, ScrollView, Alert, Platform, Animated, Easing, Share, Linking } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import { strings } from '../../../../Resources/Strings';
import * as Constant from '../../../../utils/Constant';
import styles from './PetProfileStyle'
import ReadMore from '@fawazahmed/react-native-read-more';
import CommonButton from '../../../../Components/CommonButton';
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../../Components/MoreMenu';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import * as ImagePicker from "react-native-image-picker"
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, apiCallWithToken, apiCallWithTokenAddPet, createDynamicLinks, getUserData, imageUploadApi } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
var FloatingLabel = require('react-native-floating-labels');
import Modal from "react-native-modal";
import CustomAlert from '../../../../Components/CustomAlert';
import { toastConfig } from '../../../../utils/ToastConfig';
import BottomCallMessageView from '../../../../Components/BottomCallMessageView';
import { CometChat } from "@cometchat-pro/react-native-chat"
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import NotificationController from '../../../../Components/NotificationController';

const moment = require('moment')


const PetProfile = (props) => {
    const [petData, setPetData] = useState(props.backPressEvent.petData)
    const [u_image] = useState(props.backPressEvent?.userImage)
    const [breed, setBreed] = useState("")
    let refSheet = useRef();
    const [isVisible, setVisible] = useState(false)
    const [images, setImages] = useState([])
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [photoData, setPhotoData] = useState([])
    const [animatedValue, setAnimation] = useState(new Animated.Value(180))
    const [flag, setFlag] = useState(false)
    const [height, setHeight] = useState(180)
    const [width, setWidth] = useState(180)
    const [u_id, setUserId] = useState("")
    const [ownerData, setOwnerData] = useState([])
    const [note, setNote] = useState("")
    const [isNoteModal, setNoteModal] = useState(false)
    const [isConfirmation, setIsConfirmation] = useState(false)
    const [imagesData, setImageData] = useState(null)
    const [isMore, setMoreMenu] = useState(false)
    const [select_co_Owner, setCowner] = useState("");

    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const [is_friend, setIsFrined] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );

    const [menuData, setMenuData] = useState([
        {
            id: 1,
            title: "Edit profile",
            image: Images.editIcon,
            key: "edit",
            status: "complete"
        },
        {
            id: 2,
            title: "Share profile",
            image: Images.share,
            key: "shareProfile",
            status: "complete"
        },
        {
            id: 3,
            title: "View or add co-owner",
            image: Images.addUser,
            key: "viewOrCoowner",
            status: "complete"
        },
        {
            id: 4,
            title: "Connect with Instagram",
            image: Images.instragram,
            key: "instagram",
            status: "pending"
        },
        {
            id: 5,
            title: "Add pet collar",
            image: Images.petIcon,
            key: "addPetCollor",
            status: "pending"
        },
        {
            id: 6,
            title: "Add notes",
            image: Images.document,
            key: "addNotes",
            status: "pending"
        },
        {
            id: 7,
            title: "Add more images",
            image: Images.addPicture,
            key: "addMore",
            status: "complete"
        },
        {
            id: 8,
            title: "Remove Pet",
            image: Images.deleteIcon,
            key: "delete",
            status: "complete"
        }
    ])
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            getPetProfile(res.token)
            props.navigation.navigation.addListener('focus', () => {
                getPetProfile(res.token)
            })
        })

    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000

        });

    }
    const getPetProfile = async (token) => {
        await apiCallGetWithToken(apiUrl.get_pet_details + "?pet_id=" + petData.pet_id + "&device_type=" + Platform.OS, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setPetData(res.result)
                setNote(res.result?.pet_note)
                //setIsFrined(isFriendCheck(token, res.result.added_by.u_id));
                if (res.result.breed.length != 1) {

                    var i, value = "";
                    for (i = 0; i < res.result.breed.length; i++) {
                        if (i != res.result.breed.length - 1)
                            value += res.result.breed[i].pb_name + " (" + res.result.breed[i].breed_percentage + "%)\n"
                        else
                            value += res.result.breed[i].pb_name + " (" + res.result.breed[i].breed_percentage + "%)"
                    }
                    setBreed(value)
                }
                else
                    setBreed(res.result.breed[0].pb_name + " (" + res.result.breed[0].breed_percentage + "%)")

                setImageDataProper(res.result.images)
                checkRedDot(res.result)
                isFriendCheck(token, res.result.pet_owner_id).then(data => {
                    if (res.result.login_user_type != "co-owner" && (data == true || res.result.login_user_type == "owner"))
                        getCoownerData(token)
                    else {
                        let newArray = []
                        var obj = {
                            u_first_name: res.result.added_by.u_first_name,
                            u_last_name: res.result.added_by.u_last_name,
                            u_image: res.result.added_by.u_image,
                            role: res.result.pet_name + "'s Owner",
                            pet_co_owner_owner_id: res.result.pet_owner_id

                        }
                        newArray.push(obj)
                        setOwnerData(newArray)

                    }
                })


            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    async function isFriendCheck(token, u_id) {
        var is_friend = false;
        await apiCallGetWithToken(apiUrl.user + "/" + u_id + "?device_type=" + Platform.OS, token).then(res => {
            if (res.status == true) {
                if (res.result.friend_request == 1)
                    is_friend = true
                else
                    is_friend = false

            }
            else {
                is_friend = false
            }
        })
        return is_friend
    }
    const checkRedDot = (data) => {
        let newArray = [...menuData]
        if (data.pet_note != null) {
            newArray[5].status = "complete"
        }
        setMenuData(newArray)
    }
    const setImageDataProper = (data) => {
        let newArray = [], i = 0;
        if (data.length == 1) {
            var obj = {}
            obj = {
                image1: data[i].pi_image,
                image2: "",
            }
            newArray.push(obj);
            setPhotoData(newArray)
            return
        }
        // let total = 0;
        // if (data.length % 2 == 0 && data.length != 2)
        //     total = Math.round(data.length / 2) + 1
        // else
        //     total = Math.round(data.length / 2)
        let j = 0;
        while (i < data.length) {
            var obj = {}
            if (j % 2 == 0) {
                obj = {
                    image1: data[i]?.pi_image,
                    image2: data[i + 1]?.pi_image,
                }
                i = i + 2;
                j += 1
            }
            else {
                obj = {
                    image1: data[i]?.pi_image
                }
                i = i + 1;
                j += 1

            }
            newArray.push(obj);

        }

        setPhotoData(newArray)
    }
    const getCoownerData = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_pet_co_owners + "?pet_id=" + petData.pet_id, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                if (res.result.length != 0) {
                    setOwnerData(res.result)
                }
                else {
                    setOwnerData([])
                }
            }

        })
    }
    const coOwnersList = [{
        image: Images.people,
        name: "Leonard Hofstader",
    },
    {
        image: Images.people1,
        name: "Penny Hofstader",
    }]
    const launchImageLibrary = async () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            selectionLimit: 0,
            quality: 0.5,
            maxWidth: width,
            maxHeight: height,
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');

            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                setImageData(response.assets)
                addMoreImageApi(response.assets)

                // setTimeout(() => {
                //     setIsConfirmation(true)

                // }, 500);
            }
        });

    }

    const addMoreImageApi = async (images) => {
        let newArray = [];

        setShowing(true)
        let data = new FormData();
        images.forEach((element, i) => {
            const newFile = {
                uri: element.uri,
                type: 'image/jpg',
                name: element.fileName
            }
            data.append('images[]', newFile)
        });
        //data.append("images[]",newArray);
        data.append("pet_id", petData.pet_id)
        setShowing(true)
        await imageUploadApi(data, apiUrl.add_pet_images, token).then(res => {
            setIsConfirmation(false)

            if (res.status) {
                showMessage("success", strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    props.navigation.navigation.goBack(null);
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }

    const clickImage = (url) => {
        props.navigation.navigation.navigate(strings.webViewScreen,
            {
                url: url,
                title: "", date: ""
            })

    }
    const viewImage = ({ item, index }) => {
        return (
            <View>  
                {index % 2 == 0 ?
                    <View>
                        <TouchableOpacity onPress={() => clickImage(item.image1)}>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={{ uri: item.image1 }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => clickImage(item.image2)}>
                            <Image style={{ width: 105, height: 77 }} source={{ uri: item.image2 }}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity onPress={() => clickImage(item.image1)}>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={{ uri: item.image1 }}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }

    const renderCoOwners = ({ item, index }) => {
        return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
                source={item.image}
                style={{ height: 30, width: 30, marginRight: 10, borderRadius: 30 }} />

            <Text style={styles.description}>
                {item.name}
            </Text>
        </View>
    }
    const clickMenu = (item) => {
        refSheet.close();
        if (item.key == "viewOrCoowner")
            props.navigation.navigation.navigate(strings.addOwnerScreen, { petData: petData })
        else if (item.key == "addPetCollor")
            props.navigation.navigation.navigate(strings.addCollarScreen, { petData: petData })
        else if (item.key == "edit")
            props.navigation.navigation.navigate("addPet", { petData: petData })
        else if (item.key == "addMore") {
            setTimeout(() => {
                launchImageLibrary()
            }, Platform.OS == "ios" ? 1000 : 500);
        }
        else if (item.key == "addNotes") {
            setTimeout(() => {
                setNoteModal(true)

            }, 200);

        }
        else if (item.key == "shareProfile") {
            var obj = {
                id: petData.pet_id,
                title: petData.pet_name,
                imageUrl: petData.pet_image,
                descriptionText: "Pet Profile",
                flag: "petProfile"
            }
            setTimeout(() => {
                createDynamicLinks(obj).then(res => {
                    onShare(res)
                    //apiKeyGenerate(res)
                })
            }, 500);
        }
        else if (item.key == "delete")
            removePet();
        else if (item.key == "instagram") {

            // let { origURL } = "https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U";
            // let encodedURL = encodeURIComponent(origURL);
            // let instagramURL = `instagram://library?AssetPath=${encodedURL}`;
            // Linking.openURL(instagramURL);
        }
    }
    const onShare = async (link) => {

        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType

                } else {

                }

            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    const checkOutModal = () => {
        return (
            <Modal

                isVisible={isVisible}
            >
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={styles.modalIconStyle} source={Images.checkOut}></Image>
                    <Text style={styles.modalTitle}>{strings.checkOut}</Text>
                    <Text style={styles.modalDescription}>{strings.checkOutDescription}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Constant.ColorTheme.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219 }}
                        onPress={() => setVisible(false)}
                        text={strings.approve}
                    />
                    <Text onPress={() => setVisible(false)} style={styles.txtCancel}>{strings.cancelText}</Text>
                </View>
            </Modal >
        )
    }
    const clickDot = (item) => {
        setCowner(item)
        if (isMore == true) {
            setMoreMenu(false)
            setTimeout(() => {
                setMoreMenu(true)
            }, 400);
        }
        else
            setMoreMenu(true)

    }
    const onGroupIcon = (select_co_Owner) => {
        props.navigation.navigation.navigate(strings.typeMessageScreen, {
            userData: {
                u_id: select_co_Owner.pet_co_owner_owner_id,
                u_first_name: select_co_Owner.u_first_name, u_last_name: select_co_Owner.u_last_name
            }
        })

        // props.navigation.navigation.navigate(strings.messageScreen)
    }
    const renderOwner = ({ item, index }) => {
        return (
            <View

                style={[styles.renderView, { marginTop: index != 0 ? 8 : 0 }]}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigation.replace(strings.UserProfileScreen, { u_id: item.pet_co_owner_owner_id })}
                    style={{ flexDirection: "row" }}>
                    <Image style={styles.profileIcon} source={{ uri: item.u_image }}></Image>
                    <View style={{ alignSelf: "center", marginLeft: responsiveWidth(2) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDesignation}>{item?.role != undefined ? item.role : "Co-owner"}</Text>
                    </View>
                </TouchableOpacity>
                {item.is_friend == true &&
                    <View style={styles.iconView}>
                        <TouchableOpacity onPress={() => onGroupIcon(item)}>
                            <Image style={styles.callIcon} source={Images.contact}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => clickDot(item)}>
                            <Image style={[styles.callIcon, {
                                marginLeft: responsiveWidth(2),
                                width: 13, height: 13, alignSelf: "center"
                            }]} resizeMode={"contain"} source={Images.menuDot}></Image>
                        </TouchableOpacity>
                    </View>
                }
            </View >
        )
    }
    const handleAnimation = async () => {
        if (flag == false) {
            Animated.timing(animatedValue, {
                toValue: 382,
                timing: 1500
            }).start(() => {
                setFlag(true)
            });
        }
        else {
            Animated.timing(animatedValue, {
                toValue: 180,
                timing: 1500
            }).start(setFlag(false));
        }
    }

    const animatedStyle = {
        width: animatedValue,
        height: animatedValue,
        borderRadius: flag ? 0 : animatedValue,
        marginTop: flag ? 0 : 10

    }
    const getAgeText = () => {
        var a = moment(new Date());
        var b = moment(new Date(petData.pet_dob));

        var years = a.diff(b, 'year');
        if (years <= 0)
            years = petData.pet_age
        else
            years += " years old"
        return years;
    }
    const modalAddNote = () => {
        return (
            <Modal
                //  animationType="fade"
                //transparent={true}
                isVisible={isNoteModal}
            >
                {/*All views of Modal*/}
                <View style={styles.modalNote}>
                    <Text style={styles.txtRate}>{"Add Note"}</Text>

                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        multiline={true}
                        value={note}
                        //   returnKeyType={ 'done' }
                        onChangeText={(text) => setNote(text)}

                    >{strings.addNote}</FloatingLabel>
                    <View style={{ flexDirection: "row", alignSelf: "flex-end", marginTop: 20 }}>
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={[Colors.opacityColor2, Colors.opacityColor2]}
                            btnText={styles.btnText}
                            viewStyle={{ marginTop: 10, flex: 0.3, marginRight: 10 }}
                            onPress={() => { setNoteModal(false) }}
                            text={strings.cancelText}
                        />
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={Colors.btnBackgroundColor}
                            btnText={styles.btnText}
                            viewStyle={{ marginVertical: 10, flex: 0.4 }}
                            onPress={() => addNote()}
                            text={strings.addNote}
                        />
                    </View>
                </View>
            </Modal >

        )
    }
    const addNote = async () => {
        setNoteModal(false)
        var url = apiUrl.edit_pet

        let data = new FormData();

        data.append("pet_id", petData.pet_id);
        data.append("pet_note", note);
        data.append("pet_type_id", petData.pet_type_id);
        data.append("pet_name", petData.pet_name);
        data.append("pet_gender", petData.pet_gender);
        data.append("pet_dob", petData.pet_dob);

        setShowing(true)
        await apiCallWithTokenAddPet(data, url, token).then(res => {
            if (res.status) {
                setShowing(false)
                getPetProfile(token)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const removePet = async () => {
        setShowing(true)
        var obj = {
            pet_id: petData.pet_id,
        }
        await apiCallWithToken(obj, apiUrl.delete_pet, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                //   showMessage("success", "Message", res.message)
                setTimeout(() => {
                    props.navigation.navigation.goBack(null);

                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const cancelClick = () => {
        setIsConfirmation(false)
    }
    const okClick = () => {
        addMoreImageApi(imagesData)
    }
    const onMessageCowner = () => {
        setMoreMenu(false)
        props.navigation.navigation.navigate(strings.typeMessageScreen, {
            userData: {
                u_id: select_co_Owner.pet_co_owner_owner_id,
                u_first_name: select_co_Owner.u_first_name, u_last_name: select_co_Owner.u_last_name
            }
        })

    }
    const onCallCowner = () => {
        setMoreMenu(false)
        setTimeout(() => {
            var receiverID = Constant.comeChatUserTag + "" + select_co_Owner.pet_co_owner_owner_id;
            var callType = CometChat.CALL_TYPE.AUDIO;
            var receiverType = CometChat.RECEIVER_TYPE.USER;

            var call = new CometChat.Call(receiverID, callType, receiverType);

            CometChat.initiateCall(call).then(
                outGoingCall => {
                    props.navigation.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: select_co_Owner })
                },
                error => {
                    console.log("Call initialization failed with exception:", error);
                }
            );

        }, 500);

    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <AppHeader
                leftIconPress={() => props.navigation.navigation.navigate(props.backPressEvent.screen)}
                //navigation={props.navigation}
                title={strings.petProfile}
                isProfileImage={true}
                profileImage={{ uri: u_image }}
                titleText={{ marginLeft: -30 }}
                rightTitlePressed={() => refSheet.open()}
                rightIcon={petData?.added_by?.u_id == u_id || petData.login_user_type == "owner" || petData.login_user_type == "co-owner" ? Images.menuDot : null}
            />
            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ alignItems: 'center', }}>
                    <>
                        <TouchableOpacity onPress={handleAnimation}  >
                            <Animated.Image
                                source={{ uri: petData.pet_image == "" || petData.pet_image == undefined ? petData.pt_image : petData.pet_image }}
                                resizeMode='cover'
                                style={[styles.petImage, animatedStyle]}
                            />

                        </TouchableOpacity>
                        <Text style={styles.petName}>
                            {petData.pet_name}
                        </Text>
                        <View style={{ width: "90%", marginVertical: 10 }}>
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={Colors.btnBackgroundColor}
                                btnText={styles.btnText}
                                // viewStyle={{ marginTop: 20 }}
                                onPress={() => setVisible(true)
                                }
                                text={strings.checkOut}
                                image={Images.checkOut}
                                imageStyle={styles.addIcon}
                            />
                        </View>
                        {ownerData.length != 0 &&
                            <View style={styles.viewOwner}>
                                <FlatList
                                    data={ownerData}
                                    style={styles.flatListOwner}

                                    renderItem={renderOwner}
                                >
                                </FlatList>
                            </View>
                        }
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Breed
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {breed}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Gender
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {petData.pet_gender}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Birthday
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {moment(petData.pet_dob).format("MM-DD-YYYY").toString()}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Age
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {getAgeText()}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        {/* <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Size
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={[styles.descriptionView, { alignItems: 'center', flexDirection: 'row' }]} >
                               
                                <Text style={styles.description}>
                                    Medium
                                </Text>
                            </View>
                        </View> */}
                        {/* <View style={styles.line} /> */}
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Weight
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={[styles.descriptionView, { alignItems: 'center', flexDirection: 'row' }]} >
                                <Text style={styles.description}>
                                    {petData.pet_size + " pounds"}
                                </Text>

                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Pet Friendly
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={[styles.descriptionView, { alignItems: 'center', flexDirection: 'row' }]} >
                                <Text style={styles.description}>
                                    {petData?.pet_is_friendly}
                                </Text>

                            </View>
                        </View>
                        <View style={styles.line} />
                        {/* <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Co-owners
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <FlatList
                                    data={coOwnersList}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={renderCoOwners}
                                    ItemSeparatorComponent={
                                        () => <View style={{ height: 10, }} />
                                    }
                                >
                                </FlatList>

                            </View>
                        </View> */}
                        {/* <View style={styles.line} /> */}
                        {petData?.images?.length != 0 &&
                            <>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: "90%", marginVertical: 10 }}>
                                    <Text style={styles.title}>
                                        {"PHOTOS "}
                                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + petData?.images?.length + ")"}</Text>

                                    </Text>
                                    <TouchableOpacity onPress={() => props.navigation.navigation.navigate(strings.viewImagesScreen, { images: petData?.images, flag: "pet", owner_id: petData?.added_by?.u_id })} style={{ flexDirection: "row", alignSelf: "center", }}>
                                        <Text style={styles.viewAll}>{"View All"}</Text>
                                        <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                                    </TouchableOpacity>
                                </View>
                                <FlatList
                                    contentContainerStyle={{ paddingLeft: 20, }}
                                    data={photoData}
                                    style={{ alignSelf: 'flex-start', }}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={viewImage}
                                >
                                </FlatList>
                                {petData?.added_by?.u_id == u_id || petData.login_user_type == "owner" || petData.login_user_type == "co-owner" ?
                                    <TouchableOpacity
                                        onPress={() => clickMenu({ key: "addMore" })}
                                        style={{ marginHorizontal: 20, marginVertical: 10, alignSelf: 'flex-start', flexDirection: "row" }}>
                                        <Text style={{
                                            fontSize: 18,
                                            color: Colors.primaryViolet,
                                            alignSelf: "center", textAlign: "center", marginRight: 5,
                                        }}>+</Text>
                                        <Text style={styles.addMorePhots}>{" Add more photos"}</Text>
                                    </TouchableOpacity> :
                                    <View style={{ marginVertical: 10 }} />}
                            </>
                        }
                        {petData.pet_note != undefined &&
                            <View style={{ width: "90%", marginVertical: 10 }}>
                                <Text style={styles.title}>
                                    NOTES
                                </Text>
                                <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                    <View style={{
                                        backgroundColor: Colors.primaryViolet, width: 2,
                                        //   height: readMoreHeight
                                    }}
                                    />
                                    <ReadMore

                                        numberOfLines={6}
                                        seeMoreStyle={{ color: Colors.primaryViolet }}
                                        seeLessStyle={{ color: Colors.primaryViolet }}
                                        style={[styles.title, { marginLeft: 10, fontWeight: "400" }]}>
                                        {
                                            petData.pet_note

                                        }

                                    </ReadMore>

                                </View>
                            </View>
                        }
                    </>
                    {checkOutModal()}
                    {modalAddNote()}
                    <CustomAlert
                        title={"Confirmation"}
                        message={"Are You Sure Selected Images Upload In Server?"}
                        isConfirmation={isConfirmation}
                        cancelClick={cancelClick}
                        okClick={okClick}
                    />
                </ScrollView>

                <RBSheet
                    ref={ref => {
                        refSheet = ref;
                    }}
                    height={372}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 20
                        }
                    }}
                >
                    <MoreMenu
                        clickItem={(item) => clickMenu(item)}
                        data={menuData}
                    >

                    </MoreMenu>
                </RBSheet>
                <BottomCallMessageView
                    isMore={isMore}
                    onMessage={onMessageCowner}
                    onCall={onCallCowner}

                />
            </SafeAreaView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation.navigation}
                    callIsVisible={isVisibleCall}
                />
            }
        </View >
    )


}

export default PetProfile;