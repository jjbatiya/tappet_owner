import React, { useRef, useState, useEffect } from 'react';
import { View, Text, FlatList, Image, Modal, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import CommonButton from '../../../../../Components/CommonButton';
import SimpleSerach from '../../../../../Components/SimpleSerach';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import HeaderView from '../../../../../Components/HeaderView';
import { Fonts } from '../../../../../Resources/Fonts';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../../../utils/helper';
import Toast from 'react-native-toast-message';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Loader from '../../../../../Components/Loader';
import { toastConfig } from '../../../../../utils/ToastConfig';


const AddOwnerScreen = (props) => {
    const [isVisible, setVisible] = useState(false)
    const [co_ownerFlag, setCo_OwnerFlag] = useState(false)
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [petData] = useState(props.route.params.petData)
    const [coOwnerData, setCoownerData] = useState([])
    const [selectUser, setUser] = useState(null)
    const [page, setPage] = useState(0)
    const [limit, setLimit] = useState(10)
    const [serachText, setSerachText] = useState("")
    const [friendsData, setFriendData] = useState([])

    const backClick = () => {
        props.navigation.goBack(null)
    }
    useEffect(async () => {
        await getUserData().then(res => {
            setUserID(res.u_id)
            setToken(res.token)
            getCoownerData(res.token);
            //    getAllData(res.token);
        })
    }, [])
    const getFriendsData = async (token, coOwnerData, serachText) => {
        var obj = {
            // page: 1,
            //  limit: 30,
            friend_request: 1,
            search: serachText
        }
        setSerachText("")
        await apiCallWithToken(obj, apiUrl.my_friends, token).then(res => {
            setShowing(true)
            if (res.status == true) {
                setShowing(false)
                categoryWiseData(res.result, coOwnerData)

            }
            else {
                setTimeout(() => {
                    setShowing(false)
                    setFriendData([])
                }, 1000);
            }
        })
    }
    const getCoownerData = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_pet_co_owners + "?pet_id=" + petData.pet_id, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                if (res.result.length != 0) {
                    coOwnerDataChage(res.result, 0)
                    getFriendsData(token, res.result, "")


                }
                else {
                    //  showMessage(strings.error, strings.msgTag, res.message)
                    setCoownerData([])
                    getFriendsData(token, [], "")

                }


            }
            else {
                //   showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const categoryWiseData = (data, coOwnerData) => {
        let newArray = [];
        for (var i = 0; i < data.length; i++) {
            var isData = false;
            for (var j = 0; j < coOwnerData.length; j++) {
                if (coOwnerData[j].pet_co_owner_owner_id == data[i].u_id)
                    isData = true;
            }
            if (isData==false)
                newArray.push(data[i])

        }

        setFriendData(newArray)
    }
    const coOwnerDataChage = (data, index) => {
        let newArray = [];

        var obj = {
            title: strings.allCowner,
            id: index,
            data: data
        }
        newArray.push(obj)
        setCoownerData(newArray)

    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10 }}>
                {item.title != "" && <Text style={styles.titleText}>
                    {item.title}
                </Text>}
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const clickUser = async (item, flag) => {
        await setUser(item)
        await setCo_OwnerFlag(flag)
        setVisible(true)
    }
    const renderSubView = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_friends + " Friends • " + item.total_pets + " Pets"}</Text>
                    </View>
                </View>
                {item.is_pet_co_owner == false ?
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => clickUser(item, true)}
                        text={strings.addCoowner}
                    /> :
                    <TouchableOpacity
                        onPress={() => clickUser(item, false)}

                        style={{ alignItems: "center", justifyContent: "center" }} >
                        <View style={styles.borderStyle}></View>
                        <Text style={styles.btnText1}>{strings.removeCoowner}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
    const removeAddCoOwner = async () => {
        setVisible(false)

        //setShowing(true)
        var obj = {
            pet_id: petData.pet_id,
            user_id: selectUser?.u_id == undefined ? selectUser.pet_co_owner_owner_id : selectUser.u_id,
            status: co_ownerFlag ? "1" : "2"
        }
        await apiCallWithToken(obj, apiUrl.add_pet_co_owner, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                // showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getCoownerData(token, "")
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const addOwnerModal = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={styles.emailIconStyle} source={Images.addUser}></Image>
                    <Text style={styles.txtlblCheckEmail}>{co_ownerFlag ? strings.addCoowner : strings.removeCoowner}</Text>
                    <Text style={styles.txtlblRecoverStr}>{co_ownerFlag ? strings.addCoownerStr : strings.removeCoownerStr}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient1}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219, height: 40 }}
                        onPress={() => removeAddCoOwner()}
                        text={co_ownerFlag ? strings.addCoowner : strings.removeCoowner}
                    />
                    <Text onPress={() => setVisible(false)} style={styles.txtCancel}>{strings.cancelText}</Text>

                </View>
            </Modal>
        )
    }
    const onSubmitText = (text) => {
        setShowing(true)
        if (text == "")
            getFriendsData(token, coOwnerData[0].data, "")
        else
            getFriendsData(token, coOwnerData[0].data, text)
    }
    const renderFriendsView = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pets"}</Text>
                    </View>
                </View>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={styles.btnView}
                    onPress={() => clickUser(item, true)}
                    text={strings.addCoowner}
                />


            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <CommonStatusBar />
                <SafeAreaView style={{ marginTop: responsiveHeight(4), flex: 1 }}>
                    <HeaderView
                        onPress={backClick}
                        text={strings.coowner}
                    />
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <SimpleSerach
                            onChangeText={(text) => setSerachText(text)}
                            placeHolder={strings.searchPeople}
                            isSimpleSerach={true}
                            isScreen={true}
                            onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onSubmitText(text)}



                        />
                        <FlatList
                            data={coOwnerData}
                            style={{ marginRight: responsiveWidth(4) }}
                            renderItem={renderItem}
                            showsVerticalScrollIndicator={false}

                        />
                        {friendsData.length != 0 &&
                            <FlatList
                                data={friendsData}
                                style={{ marginRight: responsiveWidth(4), marginBottom: 230 }}
                                renderItem={renderFriendsView}
                                showsVerticalScrollIndicator={false}

                            />
                        }
                    </View>
                    {addOwnerModal()}

                </SafeAreaView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    titleText: {
        fontSize: 14, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        textTransform: "uppercase"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnView: { alignItems: "center", justifyContent: "center" },
    renderSubView: { height: 48, marginVertical: 15, flexDirection: "row", alignItems: "center", justifyContent: 'space-between' },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 48, width: 48, borderRadius: 24 },
    borderStyle: {
        height: 28, width: 120, borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2,
        backgroundColor: Colors.white
    },
    btnText1: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        position: "absolute",
        opacity: 0.6,
    },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 96,
    },
    //modal view
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },

    emailIconStyle: { marginVertical: responsiveHeight(2), height: 20, width: 20, tintColor: Colors.white },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    txtlblRecoverStr: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnlinearGradient1: {
        height: 40,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 219,
    },

    txtallCoowner: {
        fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansRegular,
        marginRight: responsiveWidth(4)
    }

})
export default AddOwnerScreen;