import React, { useRef, useState } from 'react';
import { View, Text, FlatList, Image, Modal, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import CommonButton from '../../../../../Components/CommonButton';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import HeaderView from '../../../../../Components/HeaderView';
import { Fonts } from '../../../../../Resources/Fonts';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';

const AddCollar = (props) => {
    const[petData]=useState(props.route.params.petData)
    const backClick = () => {
        props.navigation.goBack(null);
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar/>
                <SafeAreaView style={{marginTop:responsiveHeight(4)}}>
                    <HeaderView
                        onPress={backClick}
                        text={""}
                        isCloseIcon={true}
                    />
                    <View style={{ marginTop: responsiveHeight(2) }}>
                        <Image style={styles.imgCollar} source={Images.collar}></Image>
                        <Text style={styles.txtHeading}>{strings.addCollar}</Text>
                        <Text style={styles.txtDetail}>{strings.deviceDetailMsg}</Text>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate(strings.addCollarScanScreen, { flag: "automatic",petData:petData })}
                            style={styles.borderView}>
                            <Image style={styles.imgScan} source={Images.scanner}></Image>
                            <Text style={styles.txtDeviceLabel}>{strings.tapOnDevice}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.borderView}
                            onPress={() => props.navigation.navigate(strings.addCollarScanScreen, { flag: "manual",petData:petData })}
                        >
                            <Image style={styles.imgScan} source={Images.editIcon}></Image>
                            <Text style={styles.txtDeviceLabel}>{strings.enterManually}</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgCollar: {
        height: 93, width: 93,
        tintColor: Colors.white, alignSelf: "center"
    },
    txtHeading: {
        fontSize: 20, lineHeight: 22, color: Colors.white,
        fontWeight: "700", fontFamily: Fonts.DMSansRegular, alignSelf: "center", marginTop: responsiveHeight(1.6)
    },
    txtDetail: {
        fontSize: 14, lineHeight: 18.23, color: Colors.white, opacity: 0.6, marginHorizontal: responsiveWidth(6),
        fontWeight: "400", fontFamily: Fonts.DMSansRegular, alignSelf: "center",
        marginTop: responsiveHeight(1), textAlign: "center", marginBottom: responsiveHeight(3)
    },
    borderView: {
        borderWidth: 1, borderColor: Colors.opacityColor2,
        height: 44, marginHorizontal: responsiveWidth(6), borderRadius: 4,
        marginTop: responsiveHeight(2), alignItems: "center", justifyContent: "center",
        flexDirection: "row"
    },
    imgScan: { height: 16, width: 16, tintColor: Colors.white },
    txtDeviceLabel: {
        fontWeight: "400", lineHeight: 18.23,
        fontSize: 14, fontFamily: Fonts.DMSansRegular,
        color: Colors.white, marginLeft: responsiveWidth(2)
    }
})

export default AddCollar;