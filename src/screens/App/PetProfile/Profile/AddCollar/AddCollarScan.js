import React, { useRef, useState,useEffect } from 'react';
import { View, Text, FlatList, Image, Modal, TouchableOpacity, StyleSheet, SafeAreaView, ScrollView, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import CommonButton from '../../../../../Components/CommonButton';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import HeaderView from '../../../../../Components/HeaderView';
import { Fonts } from '../../../../../Resources/Fonts';
import RowLine from '../../../../../Components/RowLine';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { apiCallWithToken, getUserData } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
var FloatingLabel = require('react-native-floating-labels');
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../../utils/ToastConfig';

const AddCollarScan = (props) => {
    const [scan, setScan] = useState(3)
    const [isVisible, setVisible] = useState(false)
    const [flag, setFlag] = useState(props.route.params.flag)
    const [petData] = useState(props.route.params.petData)
    const [collar_device_id, setCollerId] = useState(petData.pet_collar?.pet_collar_device_id)
    const [token, setToken] = useState("")
    const[isshowing,setShowing]=useState(false)
    const backClick = () => {
        props.navigation.goBack(null);
    }
    const [isTick, setTickMark] = useState(false)
    useEffect(() => {
        console.log(petData)
        getUserData().then(res => {
            setToken(res.token)
        })
    }, []);
    const addCollarApi = async () => {
          setShowing(true)
          setVisible(false)
          var isCollerId=petData.pet_collar?.pet_collar_device_id==undefined?false:true
          console.log(isCollerId)
        var obj = {
            pet_id: petData.pet_id,
            status: isCollerId?"2":"1",
            collar_device_id: collar_device_id
        }
        await apiCallWithToken(obj, apiUrl.add_collar, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const addCollarModal = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={styles.emailIconStyle} source={Images.collar}></Image>
                    <Text style={styles.txtlblCheckEmail}>{"Collar Added"}</Text>
                    <Text style={styles.txtlblRecoverStr}>{"You have successfully added collar for your pet. You can always remove it by tapping on the collar icon and remove it."}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient1}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219, height: 40 }}
                        onPress={() => addCollarApi()}
                        text={"Continue"}
                    />

                </View>
            </Modal>
        )
    }
    const viewLine = () => {
        return (
            <View>
                <View style={{ height: 5, width: 5, borderRadius: 3, backgroundColor: scan == "1" ? Colors.opacityColor2 : scan == "2" ? "green" : "red", alignSelf: "center" }} />
                <View style={{ height: 100, width: 2, backgroundColor: scan == "1" ? Colors.opacityColor2 : scan == "2" ? "green" : "red", alignSelf: "center" }} />
                <View style={{ height: 5, width: 5, borderRadius: 3, backgroundColor: scan == "1" ? Colors.opacityColor2 : scan == "2" ? "green" : "red", alignSelf: "center" }} />
            </View>
        )
    }
    const addCollarScan = () => {
        return (
            <View style={{ marginTop: responsiveHeight(2) }}>
                <Image style={styles.imgCollar} source={Images.collar}></Image>
                {viewLine()}
                <Image style={{ height: 100, width: 100, tintColor: Colors.white, alignSelf: "center", marginTop: 5 }} source={Images.smartphone}></Image>
                <Text
                    onPress={() => { setScan("2"), setVisible(true) }}
                    style={styles.txtTapNow}>{scan == "3" ? "Failed to Connect" : "Tap Now"}</Text>
                <Text style={styles.txtDetail}>Tap on the pet collar and wait for 3 to 5 second. The collar will pair automatically</Text>
                {scan == "3" && <View>
                    <RowLine />
                    <Text style={styles.txtContact}>{"Contact Customer Care"}</Text>
                </View>}
            </View>
        )
    }
    const onChangeText = (text) => {
        setCollerId(text)
        if (text.length >= 10) {
            setVisible(true)
            setTickMark(true)
        }
    }
    const manuallyView = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(4) }}>
                <View style={{
                    flexDirection: "row", justifyContent: "space-between",
                    flex: 1, alignItems: "center",
                }}>
                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        onChangeText={(text) => onChangeText(text)}
                        value={collar_device_id}
                        style={styles.focusforminput}
                    >{"Please Enter Collar ID"}</FloatingLabel>

                    {isTick &&
                        <View style={{
                            alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(1),
                        }}>
                            <View style={styles.viewCheckBox} />
                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                        </View>
                    }
                </View>
                <View style={{ height: 1, width: '100%', backgroundColor: Colors.primaryViolet, marginLeft: responsiveWidth(2) }}></View>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                <ScrollView>
                    <SafeAreaView style={{ marginTop: responsiveHeight(4) }}>

                        <HeaderView
                            onPress={backClick}
                            text={flag == "automatic" ? "" : "Enter Collar ID"}
                        />
                        {
                            flag == "automatic" ?
                                addCollarScan() :
                                manuallyView()
                        }
                        {addCollarModal()}

                    </SafeAreaView>

                </ScrollView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgCollar: {
        height: 93, width: 93,
        tintColor: Colors.white, alignSelf: "center"
    },
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(25)
    },

    emailIconStyle: { marginVertical: responsiveHeight(2), height: 20, width: 20, tintColor: Colors.white },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    txtlblRecoverStr: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnlinearGradient1: {
        height: 40,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 219,
    },
    //
    txtTapNow: {
        alignSelf: "center", fontSize: 20, fontWeight: "700",
        lineHeight: 22, color: Colors.white, marginTop: responsiveHeight(5),
        fontFamily: Fonts.DMSansRegular
    },
    txtDetail: {
        alignSelf: "center", fontSize: 14, opacity: 0.6, textAlign: 'center', marginHorizontal: responsiveWidth(6),
        fontWeight: "400", lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular,
        marginTop: responsiveHeight(2)
    },
    txtContact: {
        fontWeight: "500", fontSize: 12, fontFamily: Fonts.DMSansRegular,
        lineHeight: 15.62, color: Colors.primaryViolet, alignSelf: "center",
        marginTop: responsiveHeight(4)
    },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,


    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {

        marginTop: responsiveHeight(1),
        flex: 0.8

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
    },
    viewCheckBox: {
        height: 20,
        width: 20,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primaryViolet

    },
    tickImage: { height: 6, width: 8, position: "absolute", alignSelf: "center" },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        position: "absolute",
        opacity: 0.6
    },


})
export default AddCollarScan;