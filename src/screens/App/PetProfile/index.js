import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleSheet, TouchableOpacity, View, Platform } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { createStackNavigator } from '@react-navigation/stack';
import { strings } from '../../../Resources/Strings';
import { Images } from '../../../Resources/Images'
import { Colors } from '../../../Resources/Colors'
import PetProfile from './Profile/PetProfile';
import Activity from './Activity/Activity';
import PetMap from './PetMap/PetMap';
import Schedule from './Schedule/Schedule';
import AddSchedule from './Schedule/AddSchedule/AddSchedule';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { LogoIcon } from '../../../utils/svg/LogoIcon';
import { getUserData } from '../../../utils/helper';
const Tab = createMaterialBottomTabNavigator();

const ScheduleStack = createStackNavigator();

const PetProfileTab = (props) => {
    const [u_id, setUserId] = useState("")
    const [pet_owner_id, setPetOwnerId] = useState("")
    const [isCoowner] = useState(props.route.params?.isCoowner)
    useEffect(() => {
        getUserData().then(res => {
            setUserId(res.u_id)
            setPetOwnerId(props.route.params.petData?.pet_owner_id)
        })
    }, []);
    const test = () => {
        return (
            <PetProfile navigation={props} backPressEvent={props.route.params} />
        )
    }
    const ScheduleTab = () => {
        return (
            <Schedule navigation={props} backPressEvent={props.route.params}></Schedule>
        )
    }
    const ActivityTab = () => {
        return (
            <Activity navigation={props} backPressEvent={props.route.params}></Activity>
        )
    }
    const PetMapView = ({ navigation }) => {

        return (
            <PetMap navigation={props} backPressEvent={props.route.params}></PetMap>

        )
    }
    return (
        <Tab.Navigator
            shifting={true}
            barStyle={{ backgroundColor: Colors.theme }}

            tabBarOptions={{
                activeBackgroundColor: '#0C0024',
                inactiveBackgroundColor: Colors.theme,
                style: {
                    backgroundColor: '#0C0024',
                    borderTopWidth: 1,
                    height: Platform.OS == "ios" ? responsiveHeight(8.3) : responsiveHeight(8.3),
                    borderTopColor: 'rgba(255,255,255,0.1)',
                    paddingBottom: Platform.OS == "ios" ? 1 : 0
                },
            }}>
            <Tab.Screen
                name="Profile"
                component={test}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.profileTab}</Text>
                    // ),
                    tabBarLabel: strings.profileTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.petProfileTab : Images.petProfileUn} style={[styles.icon]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Schedule"
                component={ScheduleTab}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.scheduleTab}</Text>
                    // ),
                    tabBarLabel: strings.scheduleTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.calendarSelect : Images.calendarTab} style={[styles.icon, { tintColor: focused ? Colors.primaryViolet : "grey" }]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Activity"
                component={ActivityTab}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.activityTab}</Text>
                    // ),
                    tabBarLabel: strings.activityTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.chartSelect : Images.chart} style={[styles.icon, { tintColor: focused ? Colors.primaryViolet : "grey" }]} resizeMode={'contain'} />
                    ),
                }} />
            {(u_id == pet_owner_id || isCoowner == true) &&
                <Tab.Screen
                    name="PetMap"
                    component={PetMapView}

                    options={{

                        // tabBarLabel: ({ focused, color, size }) => (
                        //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.petMapTab}</Text>
                        // ),
                        tabBarLabel: strings.petMapTab,
                        tabBarIcon: ({ focused, color, size }) => (
                            <Image source={focused ? Images.locationSelect : Images.placeIcon} style={[styles.icon, { tintColor: focused ? Colors.primaryViolet : "grey" }]} resizeMode={'contain'} />
                        ),
                    }} />
            }
        </Tab.Navigator>
    )
}


const styles = StyleSheet.create({
    icon: {
        width: 19,
        height: 20,
    },
    tabLable: {
        fontSize: 11,
        paddingBottom: Platform.OS == "android" ? 10 : 10,
        marginTop: Platform.OS == "android" ? 5 : 0
    },
    tabFocus: {
        tintColor: Colors.primaryViolet
    },
    tabUnFocus: {
        tintColor: Colors.white,
        opacity: 0.4
    },
    tabFocusLabel: {
        color: Colors.primaryViolet
    },
    tabUnFocusLabel: {
        color: Colors.white,
        opacity: 0.4
    },
    viewTabIcon: {
        // paddingTop: Platform.OS == "ios" ? 0 : 8
    }
})
export default PetProfileTab;