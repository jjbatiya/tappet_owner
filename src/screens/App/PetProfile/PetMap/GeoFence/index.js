import React, { useEffect, useRef, useState } from 'react';
import { View, FlatList, Text, Switch, Image } from 'react-native';
import { Images } from '../../../../../Resources/Images'
import { Colors } from '../../../../../Resources/Colors'
import styles from './GeoFenceStyle'

import { responsiveHeight } from 'react-native-responsive-dimensions';
import LinearGradient from 'react-native-linear-gradient';
import AppHeader from '../../../../../Components/AppHeader/AppHeader';

 
const GeoFence = (props) => {
    const [geoData,setGeoData]=useState([
        {
            id: 1,
            title: "Uncle Sam’s House",
            address: "San Fransisco, USA",
            value: false
        },
        {
            id: 2,
            title: "My Home",
            address: "New York, USA",
            value: true
        },
        {
            id: 3,
            title: "Jane’s Home",
            address: "New York, USA",
            value: true
        }
    ])
    const toggleSwitch = (index) => {
        let newArray=[...geoData]
        newArray[index].value=!newArray[index].value
        setGeoData(newArray)
    }

    const renderView = ({ item, index }) => {
        return (
            <View style={styles.mainRender}>
                <View style={{ flexDirection: "row" }}>
                    <View style={styles.viewHome}>
                        <Image style={styles.imgHome} source={Images.HomeTab}></Image>
                    </View>
                    <View style={{ alignSelf: "center", marginLeft: 20 }}>
                        <Text style={styles.txtTitle}>{item.title}</Text>
                        <Text style={styles.txtAddress}>{item.address}</Text>
                    </View>
                </View>
                <Switch
                    style={{ alignSelf: "center" }}
                    trackColor={{ true: Colors.theme, false: Colors.theme }}
                    thumbColor={item.value ? Colors.primaryViolet : "#f4f3f4"}
                    ios_backgroundColor={item.value ? Colors.primaryViolet : "#767577"}
                     onValueChange={()=>toggleSwitch(index)}
                    value={item.value}
                />
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={'Geo Fence'}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={geoData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >

                </FlatList>
            </LinearGradient>

        </View>
    )
}
export default GeoFence;
