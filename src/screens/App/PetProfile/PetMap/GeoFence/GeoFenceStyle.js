import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../../../../Resources/Colors";
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../../../Resources/Fonts";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1
    },
    //render view
    mainRender:{ flexDirection: "row", justifyContent: "space-between", marginVertical: 10 },
    txtTitle: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 14, lineHeight: 18.23
    },
    txtAddress: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 12, lineHeight: 15.62,
        opacity: 0.6
    },
    viewHome:{
        height: 48, width: 48, backgroundColor: Colors.opacityColor2,
        alignItems: "center", justifyContent: "center", borderRadius: 24
    },
    imgHome:{ height: 16, width: 16, tintColor: Colors.white }
})