import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Slider, Image, ScrollView, TouchableOpacity, Dimensions, BackHandler, PermissionsAndroid, Platform } from 'react-native';
import { Images } from '../../../../../Resources/Images'
import { Colors } from '../../../../../Resources/Colors'
import styles from './SearchMapStyle'

import { responsiveHeight } from 'react-native-responsive-dimensions';
import SerachHeader from '../../../../../Components/SerachHeader/SerachHeader';
import MapView from 'react-native-maps';
import { mapStyle } from '../../../../../Resources/MapStyle';
import RBSheet from "react-native-raw-bottom-sheet";
import CommonButton from '../../../../../Components/CommonButton';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
var FloatingLabel = require('react-native-floating-labels');
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.012;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import AutoSerachComponent from '../../../../../Components/AutoSerachComponent';
import { storeLocationData } from '../../../../../utils/helper';
import { strings } from '../../../../../Resources/Strings';
import Geolocation from '@react-native-community/geolocation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';


const SearchMap = (props) => {
    let mapRef = useRef();
    let refSheet = useRef();
    const [isLocationName, setLocationNameFocus] = useState(false)
    const [serachText, setSerachText] = useState("")
    const [post_latitude, setLatitude] = useState(3.149771)
    const [post_longitude, setLongitude] = useState(101.652778)
    const [radius, setRadius] = useState(152)
    const [currentFlag] = useState(props.route.params.currentFlag)
    const onBlur = () => {
        setLocationNameFocus(!isLocationName)
    }

    useEffect(() => {
        storeLocationData(null)

        getCurrentAddres()

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            onBackPress
        );
        return () => backHandler.remove();

    }, []);

    const getCurrentAddres = () => {
        if (Platform.OS == "android") {

            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
                interval: 10000,
                fastInterval: 5000,
            })
                .then((data) => {
                    if (data == "enabled"||data=="already-enabled")
                        getCurrentAddresData();
                })
                .catch((err) => {
                });
        }
    }
    const getCurrentAddresData = () => {
        try {
            Geolocation.getCurrentPosition(info => {
                var latitude = info.coords.latitude
                var longitude = info.coords.longitude
                setLatitude(latitude)
                setLongitude(info.coords.longitude)


                mapRef?.current.animateToRegion({
                    latitude,
                    longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                })

            }
            );
        } catch (error) {
           
        }
    }
    const onBackPress = () => {
        storeLocationData(null)
        props.navigation.goBack(null)
        return true;
    }
    const onChangeText = (text) => {
    }
    const saveLocation = () => {
        refSheet.close();
        props.navigation.goBack(null)

        // props.navigation.navigate("geoFence")
    }
    const bottomTabView = () => {
        return (
            <View style={{ flex: 1, marginHorizontal: responsiveHeight(1.2) }}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    value={serachText}
                    editable={false}
                    style={isLocationName ? styles.focusforminput : styles.formInput}
                    onFocus={() => onBlur()}
                    onBlur={() => onBlur()}
                >{"Location Name"}</FloatingLabel>
                <Text style={styles.txtGeoFence}>{"Geo Fence"}</Text>
                <Slider
                    step={1}
                    minimumValue={152}
                    maximumValue={1610}
                    value={152}
                    onValueChange={val => setRadius(val)}
                    onSlidingComplete={val => console.log(val)}
                    thumbTintColor={Colors.primaryViolet}
                    minimumTrackTintColor={Colors.primaryViolet}
                />
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.textMile}>500 ft</Text>
                    <Text style={styles.textMile}>1000 ft</Text>
                    <Text style={styles.textMile}>5280 ft</Text>
                </View>


                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={{ marginVertical: 10, width: '100%' }}
                    onPress={() => saveLocation()}
                    text={"Save Location"}
                />
            </View>
        )
    }
    const clickAddress = (data) => {
        storeLocationData(data)
        var latitude = data?.geometry?.location?.lat
        var longitude = data?.geometry?.location?.lng
        setSerachText(data?.formatted_address)
        setLatitude(latitude)
        setLongitude(longitude)
        mapRef.current.animateToRegion({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        })
        setRadius(152)
        if (currentFlag == true)
            props.navigation.goBack(null)

    }
    const onMapMarkClick = () => {
        if (currentFlag == false)
            refSheet.open()
    }
    return (
        <View style={styles.container}>
            <CommonStatusBar />
            <MapView
                customMapStyle={mapStyle}
                style={{
                    flex: 1,
                }}
                ref={mapRef}
                provider={"google"}
                mapType={strings.mapType}
                initialRegion={{
                    latitude: post_latitude,
                    longitude: post_longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}
            >
                <MapView.Marker
                    coordinate={{
                        latitude: post_latitude,
                        longitude: post_longitude,
                    }}
                    onPress={() => onMapMarkClick()}
                >
                    <TouchableOpacity style={styles.roundView}

                    >
                        <View style={styles.markerView}>
                            <Image style={{ height: 15, width: 15, tintColor: Colors.theme }} source={Images.locationTab}></Image>
                        </View>
                    </TouchableOpacity>

                </MapView.Marker>
                <MapView.Circle
                    key={(post_latitude + post_longitude).toString()}
                    center={{ latitude: post_latitude, longitude: post_longitude }}
                    radius={radius}
                    strokeWidth={1}
                    strokeColor={Colors.opacityColor3}
                    fillColor={Colors.opacityColor3}
                //onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                />
            </MapView>
            <View style={{ position: "absolute", top: 50, width: width }} >
                {/* <SerachHeader
                    onBackPress={onBackPress}
                    isPlace={false}
                    onChangeText={onChangeText}
                    placeHolder={"Search for location"}
                /> */}
                <AutoSerachComponent
                    style={styles.txtAuto}
                    onChangeText={(text) => setSerachText(text)}
                    event_location={serachText}
                    onPress={(data) => clickAddress(data)}
                    onBackPress={() => onBackPress()}
                />
            </View>
            {/* {props.route.params.currentFlag &&
                <TouchableOpacity
                    //   onPress={() => setCurrentLocation()}
                    style={styles.touchableOpacityStyle}>
                    <Image source={Images.scanner} style={styles.floatingButtonStyle} />

                </TouchableOpacity>
            } */}
            <RBSheet
                ref={ref => {
                    refSheet = ref;
                }}
                height={332}
                openDuration={250}
                closeOnDragDown={true}
                customStyles={{
                    container: {
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        backgroundColor: Colors.dialogBackground,
                        padding: 10
                    }
                }}
            >
                {bottomTabView()}
            </RBSheet>

        </View>

    )
}
export default SearchMap;