import React from 'react';
import { Dimensions, Platform, StyleSheet } from "react-native";
import { Colors } from "../../../../../Resources/Colors";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../../../Resources/Fonts";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1
    },
    markerView: {
        height: 24, width: 24, alignItems: "center", justifyContent: "center",
        backgroundColor: Colors.primaryViolet, borderRadius: 12
    },
    roundView: {
        height: 44, width: 44, alignItems: "center", justifyContent: "center",
        backgroundColor: Colors.opacityColor2, borderRadius: 22, borderWidth: 1, borderColor: Colors.white
    },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8


    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3
    },
    txtGeoFence: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular, color: Colors.white,
        fontSize: 14, lineHeight: 18.23, letterSpacing: 0.4,
        marginHorizontal: 5, marginTop: 40
    },
    textMile: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1),
        alignItems: "center",
        justifyContent: "center",
        marginTop:20

    },
    //bottom current location
    touchableOpacityStyle: {
        position: 'absolute',
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        right: 10,
        bottom: 20,
        backgroundColor: Colors.white,
        borderRadius: 25
    },
    floatingButtonStyle: {
        width: 15,
        height: 15,
        tintColor: Colors.primaryViolet,
    },
    txtAuto:{
        height: 44,
        paddingVertical:Platform.OS=="ios"?0: 5,
        paddingHorizontal: 10,
        fontSize: 16,
        backgroundColor: Colors.opacityColor3,
        color: Colors.black,
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 22,
        flex: 1,
        paddingTop:Platform.OS=="ios"?10: 0
    }

})