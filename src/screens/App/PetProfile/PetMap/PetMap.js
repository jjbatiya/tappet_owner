import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList, Image, ScrollView, Dimensions } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import styles from './PetMapStyle'
import MapView from 'react-native-maps';
import { mapStyle } from '../../../../Resources/MapStyle';
import { strings } from '../../../../Resources/Strings';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const markers = [{
    title: 'Uncle Sam’s Home',
    coordinates: {
        latitude: 3.148561,
        longitude: 101.652778
    },
    image: Images.HomeTab,
    key: "home"
},
{
    title: 'My Home',
    coordinates: {
        latitude: 3.149771,
        longitude: 101.659433
    },
    image: Images.HomeTab,
    key: "home"

},
{
    title: 'My Home',
    coordinates: {
        latitude: 3.169336,
        longitude: 101.656493
    },
    image: Images.dogIcon,
    key: "dog"
},

]
const PetMap = (props) => {
    let mapRef = useRef()
    const [u_image] = useState(props.backPressEvent?.userImage)

    const roundView = () => {
        return (

            <MapView.Circle
                center={{
                    latitude: 3.148561,
                    longitude: 101.652778
                }
                }

                radius={1500}
                strokeWidth={1}
                strokeColor={Colors.opacityColor2}
                fillColor={Colors.opacityColor}
            >
            </MapView.Circle>
        )
    }
    const innerRoundView = () => {
        return (

            <MapView.Circle
                center={{
                    latitude: 3.148561,
                    longitude: 101.652778
                }
                }

                radius={1200}
                strokeWidth={2}
                strokeColor={Colors.opacityColor}
                fillColor={Colors.opacityColor2}
            >
            </MapView.Circle>
        )
    }
    const currentMarker = () => {
        return (
            <MapView.Marker
                coordinate={{
                    latitude: 3.138906516071251,
                    longitude: 101.65396948224783
                }}>
                <View style={styles.viewMain}>
                    <View style={styles.borderView}></View>
                    <View style={styles.roundViewDot}></View>
                </View>
            </MapView.Marker>
        )
    }
    const onMapPress = (e) => {
        console.log(e)
        const { coordinate } = e.nativeEvent
        console.log(coordinate)
    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation.navigation}
                title={'Pet Map'}
                isProfileImage={true}
                profileImage={{ uri: u_image }}
                titleText={{ marginLeft: -60 }}
                rightTitlePressed={() => alert("TEST")}
                leftIcon={Images.serachIcon}
                rightIcon={Images.setting}
                rightTitleLeftPressed={() => props.navigation.navigation.navigate(strings.searchMapScreen, { currentFlag: false })}
            />
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <MapView
                        customMapStyle={mapStyle}
                        style={{
                            flex: 1,
                        }}
                        ref={mapRef}
                        provider={"google"}
                        mapType={strings.mapType}
                        initialRegion={{
                            latitude: 3.149771,
                            longitude: 101.652778,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}
                        onPress={onMapPress}

                    >
                        {roundView()}
                        {innerRoundView()}
                        {markers.map((marker, i) => (
                            <MapView.Marker
                                key={i}
                                //onPress={( )=> onMarkerClick(marker)}
                                coordinate={marker.coordinates}
                            >
                                {marker.key == "dog" ?
                                    <View >
                                        <Image style={{ height: 40, width: 40, borderRadius: 20 }} source={marker.image}></Image>
                                    </View> :
                                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                                        <View style={styles.markerView}>
                                            <Image style={{ height: 15, width: 15, tintColor: Colors.theme }} source={marker.image}></Image>
                                        </View>
                                        <Text style={styles.txtTitle}>{marker.title}</Text>
                                    </View>
                                }
                            </MapView.Marker>
                        ))}
                        {currentMarker()}
                    </MapView>

                    <TouchableOpacity
                        //   onPress={() => setCurrentLocation()}
                        style={styles.touchableOpacityStyle}>
                        <Image source={Images.scanner} style={styles.floatingButtonStyle} />

                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        </View >
    )
}
export default PetMap;