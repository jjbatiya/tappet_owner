import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../../../Resources/Colors";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../../Resources/Fonts";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        right: 10,
        bottom: 20,
        backgroundColor: Colors.white,
        borderRadius: 25
    },
    floatingButtonStyle: {
        width: 15,
        height: 15,
        tintColor: Colors.primaryViolet,
    },
    roundView: {
        position: "absolute", alignSelf: "center",
        alignItems: "center", justifyContent: "center",
        height: 218, width: 218,
        backgroundColor: Colors.opacityColor,
        marginTop: responsiveHeight(23),
        borderRadius: 109, borderWidth: 1, borderColor: Colors.opacityColor2
    },
    innerRoundView: {
        height: 170, width: 170, borderRadius: 85,
        backgroundColor: Colors.white, opacity: 0.1,
        marginTop: responsiveHeight(23), position: "absolute",
        borderWidth: 1, borderColor: Colors.white
    },
    markerView: {
        height: 24, width: 24, alignItems: "center", justifyContent: "center",
        backgroundColor: Colors.primaryViolet, borderRadius: 12
    },
    viewMain: { height: 60, width: 60, alignItems: "center", justifyContent: "center" },
    roundViewDot: {
        position: "absolute", height: 12, width: 12,
        backgroundColor: Colors.primaryViolet, borderWidth: 2,
        borderColor: Colors.white, borderRadius: 6
    },
    borderView: { height: 60, width: 60, borderRadius: 30, backgroundColor: Colors.primaryViolet, opacity: 0.2 },
    txtTitle:{
        fontWeight:"500",
        fontFamily:Fonts.DMSansRegular,
        fontSize:10,
        color:Colors.primaryViolet,
        lineHeight:13.02,textAlign:"center",alignSelf:"center"
    }

})
