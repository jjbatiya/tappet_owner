import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    KeyboardAvoidingView,
    ScrollView,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions
} from 'react-native';
import HeaderView from '../../../../../Components/HeaderView';
import styles from './AddScheduleStyle';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import { Colors } from '../../../../../Resources/Colors';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import RBSheet from "react-native-raw-bottom-sheet";
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { apiCallWithToken, getUserData, imageUploadApi } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Loader from '../../../../../Components/Loader';
var FloatingLabel = require('react-native-floating-labels');
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../../utils/ToastConfig';

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
];
const { width, height } = Dimensions.get('window');
const AddSchedule = (props) => {
    
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [pet_id] = useState(props.route.params.pet_id)
    const [isTitleFocus, setIsTitleFocus] = useState('')
    const [isStartDateFocus, setIsStartDateFocus] = useState('')
    const [showDate, setShowDate] = useState(false);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [showTime, setShowTime] = useState(false);
    const [startTime, setStartTime] = useState(new Date());
    const [endTime, setEndTime] = useState(new Date());
    const [dateType, setDateType] = useState();
    const [timeType, setTimeType] = useState();
    const [title, setTitle] = useState("")
    const [selectRadio, onSelectRadio] = useState({
        key: "do_not_repeat",
        title: "Do not repeat",
        isChecked: true
    })
    const [selectRepeatTime, setRepeatTime] = useState("5 minutes before")
    const [isRepeatClick, setIsRepeatClick] = useState(true)
    const [pet_schedule_reminder,setPetScheduleReminder]=useState(5)
    const [repeatOptions, setRepeatOptions] = useState([
        {
            key: "do_not_repeat",
            title: "Does not repeat",
            isChecked: true
        },
        {
            key: "every_day",
            title: "Everyday",
            isChecked: false
        },
        {
            key: "every_week",
            title: "Every week",
            isChecked: false
        },
        {
            key: "every_month",
            title: "Every month",
            isChecked: false
        },
        {
            key: "every_year",
            title: "Every year",
            isChecked: false
        },
        {
            key: "custom",
            title: "Custom",
            isChecked: false
        },
    ])
    const [repeatTiming] = useState([5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60])
    const [pet_schedule_note, setScheduleNote] = useState("")
    let refSheet = useRef();

    const backClick = () => {
        props.navigation.goBack()
    }
    useEffect(async () => {

        await getUserData().then(res => {
            setToken(res.token)
        })
    }, [])
    const onBlur = (flag) => {
        if (flag == 'title') {
            setIsTitleFocus(!isTitleFocus)
        } else if (flag == 'startDate') {
            setIsStartDateFocus(!isStartDateFocus)
        }

    }

    const openDateModal = (type) => {
        setShowDate(true);
        setDateType(type)
    }
    const openTimeModal = (type) => {
        setShowTime(true);
        setTimeType(type)
    }

    const onChangeDate = (selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        dateType == 'start' ? setStartDate(currentDate) : setEndDate(currentDate);
    };

    const hideShowDatePicker = () => {
        setShowDate(false);
    };

    const onChangeTime = (selectedTime) => {
        const currentTime = selectedTime || time;
        setShowTime(false);
        timeType == "start" ? setStartTime(currentTime) : setEndTime(currentTime)
    }

    const hideShowTimePicker = () => {
        setShowTime(false)
    }

    const repeatModal = () => {
        return repeatOptions.map(option => (
            <TouchableOpacity
                onPress={() => setRepeatCustomOption(option)}
                style={styles.radioButton}>
                <View style={[styles.radioView, { borderColor: selectRadio.key == option.key ? Colors.themeColor : Colors.white }]} >
                    {selectRadio.key == option.key && <View style={styles.selectedRadioView} />}
                </View>
                <Text style={[styles.input, { marginLeft: 10 }]}>{option.title}</Text>
            </TouchableOpacity>
        ))
    }
    const repeatTimingModal = () => {
        return <ScrollView>
            {repeatTiming.map(option => (
                <TouchableOpacity
                    onPress={() => { setRepeatTime(option + " minutes before"), refSheet.close(),setPetScheduleReminder(option) }}
                    style={styles.radioButton}>
                    <View style={[styles.radioView, { borderColor: selectRepeatTime.includes(option) ? Colors.themeColor : Colors.white }]} >
                        {selectRepeatTime.includes(option) && <View style={styles.selectedRadioView} />}
                    </View>
                    <Text style={[styles.input, { marginLeft: 10 }]}>{option + " minutes before"}</Text>
                </TouchableOpacity>

            ))
            }
        </ScrollView>


    }
    const returnData=(data)=>{
        console.log(data)
    }
    const setRepeatCustomOption = (option) => {

        if (option.key == 'custom') {
            props.navigation.navigate(strings.customScheduleScreen,{returnData:(data)=>returnData(data)})
            onSelectRadio(option),
                refSheet.close()
        } else {
            onSelectRadio(option),
                refSheet.close()
        }

    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const save = () => {
        if(title.trim()=="")
        {
            showMessage(strings.error,strings.msgTag,"Schedule title is required!")
            return;
        }
        addShedule();
    }
    const addShedule = async () => {
        setShowing(true)
        let data = new FormData();
        data.append("pet_id",pet_id)
        data.append("pet_schedule_name",title)
        data.append("pet_schedule_start_date",startDate.getFullYear()+"-"+(startDate.getMonth()+1)+"-"+startDate.getDate())
        data.append("pet_schedule_end_date",endDate.getFullYear()+"-"+(endDate.getMonth()+1)+"-"+endDate.getDate())
        data.append("pet_schedule_start_time",startTime.getHours()+":"+startTime.getMinutes())
        data.append("pet_schedule_end_time",endTime.getHours()+":"+endTime.getMinutes())
        data.append("pet_schedule_repeat_on",selectRadio.title)
        data.append("pet_schedule_reminder",pet_schedule_reminder),
        data.append("pet_schedule_note",pet_schedule_note)
        await imageUploadApi(data, apiUrl.add_pet_schedule, token).then(res => {
            setShowing(false)
            if (res.status == true) {
               // showMessage("success", "Message", res.message)
                setTimeout(() => {
                    props.navigation.goBack();
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const clickRepeatView = async (flag) => {
        await setIsRepeatClick(flag)
        refSheet.open();

    }
    return (
        <KeyboardAvoidingView style={[styles.container, { paddingTop: 40 }]}>
            <CommonStatusBar color={Colors.backgroundColor} />
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <HeaderView
                isPhoto={true}
                isClose={true}
                onPress={backClick}
                onSkip={() => save()}
                textSkip={'Save'}
                headerRightAction={save}
            />
            <ScrollView style={styles.formScroll} showsVerticalScrollIndicator={false}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isTitleFocus ? styles.focusforminput : styles.formInput}
                    onFocus={() => onBlur("title")}
                    onChangeText={(text) => { setTitle(text) }}
                    onBlur={() => onBlur("title")}
                    value={title}
                >
                    {strings.scheduleTitle}
                </FloatingLabel>
                <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                    <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                        {strings.startDateAndTime}
                    </Text>
                    <View style={styles.dateInput}>
                        <TextInput
                            style={styles.input}
                            editable={false}
                            value={monthNames[startDate.getMonth()] + " " + startDate.getDate() + ",  " + startDate.getFullYear()}
                            //  onChangeText={(searchString) => { this.setState({ searchString }) }}
                            underlineColorAndroid="transparent"
                        />
                        <TouchableOpacity onPress={() => openDateModal('start')} >
                            <Image style={styles.calender} source={Images.calendarTab}></Image>
                        </TouchableOpacity>
                        <View style={styles.divider}></View>
                        <TextInput
                            style={styles.input}
                            editable={false}
                            value={startTime.getHours() + ":" + startTime.getMinutes()}
                            //  onChangeText={(searchString) => { this.setState({ searchString }) }}
                            underlineColorAndroid="transparent"
                        />
                        <TouchableOpacity onPress={() => openTimeModal('start')} >
                            <Image style={styles.calender} source={Images.history}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                    <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                        {strings.endDateAndTime}
                    </Text>
                    <View style={styles.dateInput}>
                        <TextInput
                            style={styles.input}
                            editable={false}
                            value={monthNames[endDate.getMonth()] + " " + endDate.getDate() + ", " + endDate.getFullYear()}
                            underlineColorAndroid="transparent"
                        />
                        <TouchableOpacity
                            onPress={() => openDateModal('end')}
                        >
                            <Image style={styles.calender} source={Images.calendarTab}></Image>
                        </TouchableOpacity>
                        <View style={styles.divider}></View>
                        <TextInput
                            style={styles.input}
                            editable={false}
                            value={endTime.getHours() + ":" + endTime.getMinutes()}
                            underlineColorAndroid="transparent"
                        />
                        <TouchableOpacity
                            onPress={() => openTimeModal('end')}
                        >
                            <Image style={styles.calender} source={Images.history}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                    <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                        {strings.repeat}
                    </Text>
                    <View >
                        <TouchableOpacity onPress={() => clickRepeatView(true)} style={styles.dateInput}>
                            <TextInput
                                style={styles.input}
                                editable={false}
                                value={selectRadio.title}
                                underlineColorAndroid="transparent"
                            />
                            <Image style={styles.calender} source={Images.history}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                    <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                        {strings.reminder}
                    </Text>
                    <View >
                        <TouchableOpacity onPress={() => clickRepeatView(false)} style={styles.dateInput}>
                            <TextInput
                                style={styles.input}
                                editable={false}
                                value={selectRepeatTime}
                                underlineColorAndroid="transparent"
                            />
                            <Image style={styles.calender} source={Images.bell}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <FloatingLabel
                    multiline={true}
                    labelStyle={styles.labelInput}
                    inputStyle={styles.inputArea}
                    value={pet_schedule_note}
                    onChangeText={(text) => setScheduleNote(text)}
                    style={isStartDateFocus ? styles.focusforminput : styles.formInput}
                    onFocus={() => onBlur("startDate")}
                    onBlur={() => onBlur("startDate")}
                >
                    {strings.notes}
                </FloatingLabel>

                {/* {showDate && (
                    <DateTimePickerModal
                        isVisible={showDate}
                        mode="date"
                        date={dateType == 'start' ? startDate : endDate}
                        onConfirm={onChangeDate}
                        onCancel={hideShowDatePicker}

                    />
                )} */}
                {showTime && (
                    <DateTimePickerModal
                        isVisible={showTime}
                        mode="time"
                        date={startTime}
                        onConfirm={onChangeTime}
                        onCancel={hideShowTimePicker}
                        is24Hour={true}
                    />
                )}
                <RBSheet
                    ref={ref => {
                        refSheet = ref;
                    }}
                    height={height * 0.5}
                    openDuration={250}
                    closeOnDragDown={true}
                    dragFromTopOnly
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 10,
                            paddingLeft: 20
                        }
                    }}
                >
                    {isRepeatClick ? repeatModal() : repeatTimingModal()}
                </RBSheet>
            </ScrollView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </KeyboardAvoidingView>
    )
}

export default AddSchedule;