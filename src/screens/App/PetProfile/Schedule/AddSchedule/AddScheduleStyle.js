import { StyleSheet  } from "react-native";
import { Colors } from "../../../../../Resources/Colors";
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../../../Resources/Fonts";

export default styles = StyleSheet.create({
    container : {
        flex:1,
        backgroundColor : Colors.backgroundColor
    },
    formScroll : {
        paddingHorizontal : 20
    },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily:Fonts.DMSansRegular,

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.opacityColor3,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    dateClickView: {
        borderBottomColor: Colors.opacityColor3,
        borderBottomWidth: 1,
        paddingBottom: 0, 
        marginVertical: 0, 
        alignItems: 'center', 
        justifyContent: 'space-between',
        flexDirection: "row",
    },
    calender: { 
        resizeMode: "contain", 
        height: 15, 
        width: 15,
        tintColor : Colors.opacityColor3,
    },
    dateInput : {
        borderBottomColor: Colors.opacityColor3,
        borderBottomWidth: 1,
        paddingBottom: 10, 
        marginVertical: 0, 
        alignItems: 'center', 
        justifyContent: 'space-between',
        flexDirection: "row",
    },
    divider : {
        borderLeftWidth:1, 
        borderLeftColor:Colors.opacityColor3,
        height:20
    },
    dialogContainer : {
            justifyContent: "center",
            alignItems: "center",
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: Colors.dialogBackground,
            padding: 10
    },
    radioButton: { 
        flexDirection: 'row', 
        marginVertical: 15, 
        alignItems: 'center' 
    },
    radioContainer : {
        flexDirection: 'row', 
        alignItems: 'center' 
    },
    radioView: { 
        justifyContent: 'center', 
        alignItems: 'center', 
        width: 20, 
        height: 20, 
        borderRadius: 20, 
        borderWidth: 1
    },
    selectedRadioView: { 
        width: 12, 
        height: 12, 
        borderRadius: 12, 
        borderWidth: 1, 
        backgroundColor: Colors.themeColor,
    },
    inputArea : {
        borderWidth: 0,
        fontSize: 16,
        //lineHeight: 22,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular,
        height:100,
    },
    custContainer : {
        paddingTop : 20,
    },
    custTitle : {
        color : Colors.white,
        fontFamily:Fonts.DMSansBold,
        fontSize:14
    },
    section : {
        paddingVertical : 20,
        paddingHorizontal:20
    },
    subSection :{
        marginVertical : 10
    },
    dayList:{
        flexDirection :'row',
    },
    selectDay : {
        color: Colors.white,
        // paddingHorizontal : 15,
        // paddingVertical : 10,
        fontSize: 16,
        fontFamily : Fonts.DMSansRegular
    },
    dayContainer : {
        borderWidth : 1,
        width : 40,
        height : 40,
        borderColor : Colors.opacityColor3,
        borderRadius : 18,
        marginRight : 8,
        justifyContent : 'center',
        alignItems : 'center',
    },
    selectedDay : {
        backgroundColor : Colors.opacityColor2,
        borderColor : Colors.primaryViolet,
    },
    custinput : {
        borderBottomWidth : 1,
        borderBottomColor : Colors.opacityColor3,
        height : 40,
        width : 70,
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,
    },
    dropView: {
        height: 40, 
        flex: 1, 
        marginHorizontal: responsiveWidth(2),
        borderBottomWidth : 1,
        borderBottomColor : Colors.opacityColor3,
        paddingVertical : 10,
        paddingHorizontal : 10
    },
    dropdown: {
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.black,
        fontSize: 14,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,
    },
    txtStyle: {
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,

    },
    arrow: {
        position: 'absolute',
        right: responsiveHeight(0.5),
        height: responsiveHeight(4),
        width: responsiveWidth(3),
        resizeMode: "contain",
        flex: 0.1,
        tintColor: Colors.white
    },
    picker : {
        flexDirection :'row',
        justifyContent : 'space-between',
        alignItems : 'center',
        width : 150,
        borderBottomWidth : 1,
        borderBottomColor : Colors.opacityColor3
    },
    optionText : {
        borderBottomWidth : 1,
        borderBottomColor : Colors.opacityColor3,
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,
    },
})