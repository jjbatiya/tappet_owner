import React, { useState } from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import styles from './AddScheduleStyle'
import HeaderView from '../../../../../Components/HeaderView';
import { strings } from '../../../../../Resources/Strings';
import { Colors } from '../../../../../Resources/Colors';
import { Images } from '../../../../../Resources/Images';

const days = ["S", "M", "T", "W", "T", "F", "S"];
const options = [{
    key: "never",
    title: "Never",
    isChecked: false
},
{
    key: "on",
    title: "On",
    isChecked: false
},
{
    key: "after",
    title: "After",
    isChecked: false
}];

const daysOption = ["Everyday", "Every week", "Every month", "Every year"];
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
const CustomSchedule = (props) => {
    const [selectRadio, setSelectRadio] = useState({
        key: "never",
        title: "Never",
        isChecked: true
    });
    const [endsRepeat, setEndsRepeat] = useState(0);
    const [repeatValue, setRepeatValue] = useState('01');
    const [showDate, setShowDate] = useState(false);
    const [date, setDate] = useState(new Date());
    const [pet_schedule_recurring, setPetScheduleRecurring] = useState("Everyday")
    const[pet_schedule_repeating_weekly_every_weekday,setEveryWeekDay]=useState("")
    const[pet_schedule_repeating_day_of_month,setEveryMonthDay]=useState("")
    const[pet_schedule_repeating_day_of_year,setEveryYearDay]=useState("")
    const[pet_schedule_repeating_ends,setPetRepetingEnds]=useState("Never")
    const backClick = () => {
        props.navigation.goBack()
    }

    const Done = () => {
        props.route.params.returnData({ data: "hello" });

        props.navigation.goBack()
    }

    const onChangeDate = (selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        setDate(currentDate);
    };

    const hideShowDatePicker = () => {
        setShowDate(false);
    };

    const repeats = () => {
        return (
            <View style={styles.section}>
                <Text style={styles.custTitle}>{strings.repeat.toUpperCase()}</Text>

                <View style={[styles.subSection, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                    <TextInput
                        style={styles.custinput}
                        value={repeatValue}
                        onChangeText={(value) => setRepeatValue(value)}
                        underlineColorAndroid="transparent"
                        keyboardType={'number-pad'}
                        maxLength={2}
                    />

                    <ModalDropdown
                        defaultValue={pet_schedule_recurring}
                        options={daysOption}
                        style={styles.dropView}
                        onSelect={(index) => setPetScheduleRecurring(daysOption[index])}
                        dropdownStyle={{
                            width: 300,

                        }}
                        dropdownTextStyle={styles.dropdown}
                        textStyle={styles.txtStyle}
                        dropdownTextHighlightStyle={{
                            color: Colors.theme
                        }}
                        //defaultIndex={this.stateNameTogetIndex(this.state.state)}
                        renderRightComponent={() => {
                            return (
                                <Image source={Images.dropdown} style={styles.arrow} />
                            )
                        }}
                    />

                </View>
            </View>
        )
    }

    const repeatsOn = () => {
        return (
            <View style={styles.section}>
                <Text style={styles.custTitle}>{strings.repeatsOn.toUpperCase()}</Text>

                <View style={[styles.subSection, styles.dayList]}>
                    {
                        days.map((day, index) => (
                            <TouchableOpacity style={[styles.dayContainer, endsRepeat == index ? styles.selectedDay : null]} onPress={() => setEndsRepeat(index)}>
                                <Text style={styles.selectDay}>{day.toUpperCase()}</Text>
                            </TouchableOpacity>
                        ))
                    }
                </View>
            </View>
        )
    }
    const ends = () => {
        return (
            <View style={styles.section}>
                <Text style={styles.custTitle}>{strings.ends.toUpperCase()}</Text>

                <View style={styles.subSection}>
                    {options.map(option => (
                        <View style={styles.radioContainer}>
                            <TouchableOpacity
                                onPress={() => setSelectRadio(option)} style={styles.radioButton}>
                                <View style={[styles.radioView, { borderColor: selectRadio.key == option.key ? Colors.themeColor : Colors.white }]} >
                                    {selectRadio.key == option.key && <View style={styles.selectedRadioView} />}
                                </View>
                                <Text style={[styles.input, { marginLeft: 10 }]}>{option.title}</Text>
                            </TouchableOpacity>
                            <View style={{ marginHorizontal: 10 }}>
                                {
                                    option.key == 'on' ?
                                        <TouchableOpacity onPress={() => setShowDate(true)} style={styles.picker}>
                                            <TextInput
                                                style={styles.input}
                                                editable={false}
                                                value={date.getDate() + " " + monthNames[date.getMonth()] + " " + date.getFullYear()}
                                                underlineColorAndroid="transparent"
                                            />
                                            <Image style={styles.calender} source={Images.dropdown}></Image>
                                        </TouchableOpacity>

                                        : option.key == 'after' ?
                                            <Text style={styles.optionText}>01 Occurence</Text>
                                            : null
                                }
                            </View>
                        </View>
                    )
                    )}
                </View>
                {showDate && (
                    <DateTimePickerModal
                        isVisible={showDate}
                        mode="date"
                        date={date}
                        onConfirm={onChangeDate}
                        onCancel={hideShowDatePicker}

                    />
                )}
            </View>
        )
    }
    return (
        <View style={[styles.container, { paddingTop: 40 }]}>
            <HeaderView
                isPhoto={true}
                onPress={backClick}
                onSkip={() => Done()}
                text={strings.customRecurrence}
                textSkip={strings.doneStr}
                headerRightAction={() => Done()}
            />
            <ScrollView style={styles.custContainer}>
                {repeats()}
                {repeatsOn()}
                {ends()}
            </ScrollView>
        </View>
    )
}

export default CustomSchedule;