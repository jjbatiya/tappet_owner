import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList, Image, ScrollView, Platform } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import styles from './ScheduleStyle'
import Calendar from '../../../../Components/Calender/CalenderComponent';
import Events from '../../../../Components/Events/Events';
import { apiCallGetWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { strings } from '../../../../Resources/Strings';
import { toastConfig } from '../../../../utils/ToastConfig';

const Schedule = (props) => {
    const [petData, setPetData] = useState(props.backPressEvent.petData)
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [u_id, setUserId] = useState("")
    const [page, setPage] = useState(1)
    const [limit, setLimit] = useState("")
    const [pet_id] = useState(props.backPressEvent.petData.pet_id)
    const [scheduleData, setScheduledata] = useState([])
    const[u_image]=useState(props.backPressEvent?.userImage)

    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            getScheduleData(res.token);
        })
    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getScheduleData = async (token) => {
        await apiCallGetWithToken(apiUrl.get_pet_schedules + "?pet_id=" + petData.pet_id + "&page=" + page + "&limit=" + limit, token).then(res => {
            if (res.status == true) {
                if (page == 1)
                {
                    setScheduledata(res.result)
                    setShowing(false)

                }
                else {
                    let newArray = scheduleData.concat(res.result);
                    setScheduledata(newArray)
                    setShowing(false)

                }

            }
            else {
                setScheduledata([])
                showMessage(strings.error, strings.msgTag, res.message)
                setShowing(false)
            }
        })

    }
    const e1 = [
        {
            "albumId": 2,
            "id": 51,
            "title": "Poop Time",
            "time": "01:00",
            "date": "2021-07-22T14:17:54.217Z"
        },
    ]


    const [events, setEvents] = useState(e1)

    const onSelectDate = (date) => {
        //alert(date)
        //this.setState({ events: filterEvents(date) });
        //setEvents( filterEvents(date) )
    };

    // state = {
    //     events: filterEvents(moment()),
    // };

    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <AppHeader
                navigation={props.navigation.navigation}
                title={strings.scheduleTab}
                // leftIconPress={() => props.navigation.navigation.goBack(null)}
                isProfileImage={true}
                profileImage={{uri:u_image}}

                titleText={{ marginLeft: -30 }}
                rightTitlePressed={() => alert("TEST")}
                // rightIconLeft={Images.menuDot}
                rightIcon={Images.calendarTab}
            />
            <SafeAreaView style={{ flex: 1 }}>
                <Calendar showDaysAfterCurrent={30} onSelectDate={onSelectDate} />

                {isshowing==false && <Events data={scheduleData} events={events} navigate={props.navigation.navigation.navigate} pet_id={petData.pet_id} />}
            </SafeAreaView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View >
    )


}

export default Schedule;