import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, Dimensions } from 'react-native';

import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import styles from './GroupsStyle';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { PlusIcon } from '../../../utils/svg/PlusIcon';
import { apiCallGetWithToken, getUserData } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Loader from '../../../Components/Loader';
const { width, height } = Dimensions.get("window")
import Toast from 'react-native-toast-message';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { comeGroupTag } from '../../../utils/Constant';
import { strings } from '../../../Resources/Strings';
import { toastConfig } from '../../../utils/ToastConfig';


const Groups = (props) => {
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(true)
    const [groupData, setGroupData] = useState([])
    const [u_id, setUid] = useState("")

    useEffect(async () => {
        getUserData().then(res => {
            setShowing(true)
            setToken(res.token)
            setUid(res.u_id)
            getGroupData(res);
        })
    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getGroupData = async (res) => {
        await apiCallGetWithToken(apiUrl.get_all_groups, res.token).then(res => {
            if (res.status == true) {
                setShowing(false)
                if (res.result.length == 0)
                    showMessage(strings.error, strings.msgTag, res.message)
                else
                    groupArrayModified(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const groupArrayModified = async (data) => {
        let obj1 = {}
        await CometChat.getUnreadMessageCountForAllGroups().then(res => {
            console.log(res)

            obj1 = res;
        }, error => {
        })

        let newArray = [...data]
        for (var i = 0; i < newArray.length; i++) {

            var guid = comeGroupTag + newArray[i].group_id + "";

            newArray[i].count_unreadmessage = obj1[guid + ""] == undefined ? 0 : obj1[guid + ""]
        }
        newArray.sort((function (obj1, obj2) {
            // Ascending: first id less than the previous
            return obj2.count_unreadmessage - obj1.count_unreadmessage;
        }));
        setGroupData(newArray)

    }
    const clickGroup = (item) => {
        if (item.group_owner_id == u_id) {
            props.navigation.navigate(strings.chatMessageScreen, { groupData: item })
        }
        else
            props.navigation.navigate(strings.chatMessageScreen, { groupData: item })
    }
    const renderItem = ({ item, index }) => {
        return (
            <>
                <TouchableOpacity
                    onPress={() => clickGroup(item)}
                    style={styles.renderStyle} >
                    <Image source={{ uri: item.group_image }} style={styles.imgStyle}></Image>
                    <View style={styles.upperView}>
                        <View style={styles.upperSubView}>
                            <View style={{ flexDirection: "row" }}>
                                {item.group_last_two_members.map((data, parentIndex) => (
                                    parentIndex < 3 ?
                                        parentIndex != 2 && parentIndex < 2 ?
                                            <Image style={[styles.peopleImg, { marginLeft: parentIndex == 0 ? 0 : -8 }]} source={{ uri: data.member?.u_image }}></Image>
                                            :
                                            <View style={styles.moreView}>
                                                <View style={styles.moreBorderView}></View>
                                                <Text style={styles.txtMore}>{"+" + parseInt(item.group_last_two_members.length - 2)}</Text>
                                            </View> : <></>
                                ))}
                            </View>
                        </View>

                    </View>

                    <LinearGradient style={{
                        position: "absolute",
                        height: responsiveHeight(18),
                        width: width, bottom: 0
                    }} colors={Colors.viewGroupContain}>
                    </LinearGradient>
                    <View style={styles.bottomView}>
                        <Text style={styles.txtTitle}>{item.group_name}</Text>
                    </View>

                </TouchableOpacity>
                {item.count_unreadmessage != undefined && item.count_unreadmessage >0 &&
                    <View style={{ height: 10, width: 10, borderRadius: 10, backgroundColor: Colors.redColor, position: "absolute", top: 3, right: -1 }}>
                    </View>
                }
            </>

        )
    }
    return (
        <View style={styles.container}>

            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <AppHeader
                navigation={props.navigation}
                title={'Groups'}
                titleText={style = { marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >

                <View style={styles.contentView}>

                    <FlatList
                        data={groupData}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                    />
                    <LinearGradient
                        colors={Colors.btnBackgroundColor}
                        style={styles.flotingView}
                    >
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate(strings.createGroupScreen)}
                            style={styles.plusView}>
                            <PlusIcon width={24} height={24} />
                        </TouchableOpacity>
                    </LinearGradient>

                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default Groups;