import React from 'react';
import { StyleSheet } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../../Resources/Colors";
import { Fonts } from '../../../../Resources/Fonts';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    txtHeader: {
        fontSize: 14, letterSpacing: 0.4, fontWeight: "700",
        lineHeight: 18.23, fontFamily: Fonts.DMSansRegular,
        color: Colors.white, textTransform: "uppercase"
    },
    viewLineVertical: { width: 2, backgroundColor: Colors.primaryViolet },
    txtDetailData: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white,
        marginHorizontal: responsiveWidth(3),
        fontFamily: Fonts.DMSansRegular
    },
    aboutEmpty: {
        justifyContent: "space-between", alignItems: "center", flexDirection: "row",
        marginVertical: responsiveHeight(1.5)
    },
    txtNoDesc: { fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white, opacity: 0.6 },
    edtImg: { height: 15, width: 15, tintColor: Colors.white },
    aboutView: { flexDirection: "row", marginVertical: responsiveHeight(1.5) },
    viewLine: { height: 1, backgroundColor: Colors.white, opacity: 0.15, marginVertical: responsiveHeight(1) },
    txtViewAll: {
        fontSize: 12, lineHeight: 15.62, fontWeight: "400",
        color: Colors.primaryViolet, fontFamily: Fonts.DMSansRegular
    },
    imgRight: { marginLeft: 7, height: 10, width: 10, tintColor: Colors.primaryViolet },
    imgCamera: { height: 10, width: 10, tintColor: Colors.primaryViolet },
    viewRound: {
        position: "absolute", height: 20, width: 20,
        borderRadius: 10, backgroundColor: Colors.white,
        bottom: -5, right: -5,
        borderWidth: 1,
        borderColor: '#0C0024',
        alignItems: "center",
        justifyContent: "center"
    },
    txtName: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, letterSpacing: 0.1, fontFamily: Fonts.DMSansRegular
    },
    txtDate: {
        fontWeight: "400", fontSize: 12, lineHeight: 18.23,
        color: Colors.white, letterSpacing: 0.1, opacity: 0.6, fontFamily: Fonts.DMSansRegular
    },
    txtAdmin: { color: Colors.white, fontSize: 9, lineHeight: 11.72, fontWeight: "400", letterSpacing: 0.4, opacity: 0.6,paddingHorizontal:responsiveHeight(1),paddingVertical:responsiveHeight(0.5) }

})