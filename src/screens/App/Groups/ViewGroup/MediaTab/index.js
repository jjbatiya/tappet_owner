import React, { useEffect, useState } from 'react';
import { View, Text, useWindowDimensions } from 'react-native';
import AppHeader from '../../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../../Resources/Images';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import Media from './Media';
import Docs from './Docs';

const MediaTab = (props) => {
    const layout = useWindowDimensions();
    const [group_id] = useState(props.route.params.group_id);

    const [index, setIndex] = useState(0);
    const [routes, setRoutes] = useState([
        { key: 'Media', title: 'Media' },
        { key: 'Docs', title: 'Docs' }
    ]);

    const renderScene = ({ route, index1 }) => {
        switch (route.key) {
            case 'Media':
                return <Media index={index} group_id={group_id} navigation={props.navigation} />;
            case 'Docs':
                return <Docs index={index} group_id={group_id} navigation={props.navigation} />;
            default:
                return null;
        }
    };


    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}

        />
    );
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height: responsiveHeight(12.4) }}
            >
                <AppHeader
                    navigation={props.navigation}
                    title={'Tap-Pet App Discussion'}
                    titleText={style = { marginLeft: -30 }}
                    backButton={Images.backIcon}
                    Tab={true}
                />

            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}
            />

        </View >
    )
}

export default MediaTab;