import React from 'react';
import { StyleSheet,Dimensions } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../../../../Resources/Colors";
import { Fonts } from '../../../../../../Resources/Fonts';
const {width,height}=Dimensions.get("window")

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    txtTitle: {
        fontWeight: "700", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, textTransform: 'uppercase',
        fontFamily: Fonts.DMSansRegular

    },
})