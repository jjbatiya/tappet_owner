import React, { useEffect, useState } from "react";
import { FlatList, View, Image, Text, Dimensions, TouchableOpacity, Linking } from 'react-native'
import LinearGradient from "react-native-linear-gradient";
import { Images } from "../../../../../../Resources/Images";
import { Colors } from '../../../../../../Resources/Colors';
import styles from './DocsStyle';
import { responsiveHeight } from "react-native-responsive-dimensions";
import { getGroupChatImages } from "../../../../../../utils/helper";
import { comeGroupTag } from "../../../../../../utils/Constant";
const { width, height } = Dimensions.get("window")
import moment, { now } from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import { strings } from "../../../../../../Resources/Strings";

const Docs = (props) => {
    const [group_id] = useState(props.group_id);
    const [chatDcos, setGroupChatDocs] = useState([])

    useEffect(async () => {
        getGroupImages();
    }, []);
    function get_url_extension(url) {
        return url.split(/[#?]/)[0].split('.').pop().trim() + "";
    }
    function filterData(data, i) {
        var type = get_url_extension(data.data.url);
        return type != "jpg" && type != "jpeg" && type != "png";
    }
    const getGroupImages = () => {
        getGroupChatImages(comeGroupTag + group_id, "file").then(res => {
            if (res.status) {
                setGroupChatDocs(res.result.filter((item) => filterData(item)))
            }
            else
                setGroupChatDocs([])
        })
    }
  
    const renderSubView = ({ item, index }) => {
        console.log(item)
        var newDate = moment(new Date(item.sentAt * 1000)).format('MMM DD YYYY hh:MM a');
        return (
            <TouchableOpacity 
            onPress={()=>Linking.openURL(item.data.url)}
            style={{ flex: 1, flexDirection: "row", marginVertical: 8 }}>
                <Image style={{ height: 50, width: 50, tintColor: Colors.white, alignSelf: "center" }} source={Images.document} />
                <View style={{ alignSelf: "center", marginLeft: 20 }}>
                    <Text style={{ color: "white", fontSize: 16, width: width / 1.3 }}>{item.data.attachments[0].name}</Text>
                    <Text style={{ color: "white", fontSize: 12 }}>{newDate}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ marginTop: responsiveHeight(1) }}>
                <Text style={styles.txtTitle}>{strings.recent}</Text>
                <FlatList
                    data={chatDcos}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                >
                </FlatList>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <FlatList
                    data={[1]}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >

                </FlatList>
            </LinearGradient>
        </View>
    )
}
export default Docs;