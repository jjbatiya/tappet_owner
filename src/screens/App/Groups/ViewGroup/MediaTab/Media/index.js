import React, { useEffect, useState } from "react";
import { FlatList, View, Image, Text, Dimensions, TouchableOpacity, Linking } from 'react-native'
import LinearGradient from "react-native-linear-gradient";
import { Images } from "../../../../../../Resources/Images";
import { Colors } from '../../../../../../Resources/Colors';
import styles from './MediaStyle';
import { responsiveHeight } from "react-native-responsive-dimensions";
import { getGroupChatImages, getUserData } from "../../../../../../utils/helper";
import { comeGroupTag } from "../../../../../../utils/Constant";
const { width, height } = Dimensions.get("window")

import moment, { now } from 'moment';
import { strings } from "../../../../../../Resources/Strings";

const Media = (props) => {
    const [group_id] = useState(props.group_id);
    const [chatImages, setGroupChatImages] = useState([])

    useEffect(async () => {
        getGroupImages();
    }, []);
    function get_url_extension(url) {
        return url.split(/[#?]/)[0].split('.').pop().trim() + "";
    }
    function filterData(data, i) {
        var type = get_url_extension(data.data.url);
        return type == "jpg" || type == "jpeg" || type == "png";
    }
    const getGroupImages = () => {
        getGroupChatImages(comeGroupTag + group_id, "file").then(res => {
            if (res.status) {
                setGroupChatImages(res.result.filter((item) => filterData(item)))
            }
            else
                setGroupChatImages([])
        })
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ marginTop: responsiveHeight(1) }}>
                <Text style={styles.txtTitle}>{"RECENT"}</Text>
                <FlatList
                    data={chatImages}
                    numColumns={3}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                >
                </FlatList>
            </View>
        )
    }
    const renderSubView = ({ item, index }) => {
        var newDate1 = moment(new Date(item.sentAt * 1000)).format('MMMM DD, YYYY');
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                    {
                        images: chatImages,
                        flag: "group_image",
                        index: index
                    })}
                style={{ flex: 0.33, marginBottom: 2 }}>
                <Image style={{ height: 110, width: width / 3 - 3 }} resizeMode={"cover"} source={{ uri: item.data.url }} />
            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <FlatList
                    data={[1]}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >

                </FlatList>
            </LinearGradient>
        </View>
    )
}
export default Media;