import React, { useState, useRef, useEffect } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity, FlatList, Switch, Platform } from 'react-native';
import styles from './ViewGroupStyle';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import EventDetailHeader from '../../../../Components/EventDetailHeader';
import { Images } from '../../../../Resources/Images';
import { strings } from '../../../../Resources/Strings';
import { Fonts } from '../../../../Resources/Fonts';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../../Components/MoreMenu';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, apiCallWithToken, getGroupChatImages, getGroupImages, getUserData, storeGroupId } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';


const menuData = [
    {
        key: "groupEvent",
        title: "Create a group event",
        image: Images.addEventIcon
    },
    {
        key: "inviteMember",
        title: "Invite Members",
        image: Images.addUser
    },
    {
        key: "delete",
        title: "Leave group",
        image: Images.logout
    },
]
const menuDataUser = [

    {
        key: "delete",
        title: "Leave group",
        image: Images.logout
    },
]
const adminMenu = [
    {
        key: "addAdmin",
        title: "Add Admin"
    },
    {
        key: "removedmin",
        title: "Remove Admin"
    },
    {
        key: "removeParticipate",
        title: "Remove Participate"
    }
]
const ViewGroup = (props) => {
    const [about, setAbout] = useState()
    let refSheet = useRef();
    const [bottomFlag, setBotomFlag] = useState(0)
    const [group_id] = useState(props.route.params.group_id);
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [groupData, setGroupData] = useState(null)
    const [isToggle, setToggle] = useState(false)
    const [events, setEvents] = useState(null);
    const backPress = () => {
        props.navigation.goBack(null);
    }
    const [itemUserId, setItemClickUser] = useState("")
    const [gm_role, setGmRole] = useState("User")
    const [chatImages, setGroupChatImages] = useState([])
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const[photoData,setPhotoData]=useState([])
    useEffect(async () => {
        props.navigation.addListener('focus', () => {
            getUserData().then(res => {
                setShowing(true)
                setUserID(res.u_id)
                setToken(res.token)
                getGroupData(res.token, res.u_id);
            })
        });
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getGroupData = async (token, u_id) => {
        await apiCallGetWithToken(apiUrl.get_group_details + "?group_id=" + group_id, token).then(res => {
            if (res.status == true) {
                setGroupData(res.result)
                console.log(res.result)
                setAbout(res.result.group_description)
                setShowing(false)
                checkArrayIsAdmin(u_id, res.result)
                setEvents(res.result.events)
                getGroupImages();
                if (res.result.group_privacy == "Private")
                    setToggle(false)
                else
                    setToggle(true)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    function filterData(data, i) {
        var type = get_url_extension(data.data.url);
        return type == "jpg" || type == "jpeg" || type == "png";
    }
    const getGroupImages = () => {
        getGroupChatImages(comeGroupTag + group_id, "file").then(res => {
            if (res.status) {
                setGroupChatImages(res.result.filter((item) => filterData(item)))
                setImageDataProper(res.result.filter((item) => filterData(item)))
            }
            else
            {
                setGroupChatImages([])
                setPhotoData([])
            }
        })
    
    }
    const setImageDataProper = (data) => {
        let newArray = [], i = 0;
        if (data.length == 1) {
            var obj = {}
            obj = {
                image1: data[i].data.url,
                image2: "",
            }
            newArray.push(obj);
            setPhotoData(newArray)
            return
        }
        // let total = 0;
        // if (data.length % 2 == 0&&data.length!=2)
        //     total = Math.round(data.length / 2) + 1
        // else
        //     total = Math.round(data.length / 2)
        let j = 0;
        while (i < data.length) {
            var obj = {}
            if (j % 2 == 0) {
                obj = {
                    image1: data[i]?.data.url,
                    image2: data[i + 1]?.data.url,
                }
                i = i + 2;
                j += 1
            }
            else {
                obj = {
                    image1: data[i]?.data.url
                }
                i = i + 1;
                j += 1

            }
            newArray.push(obj);

        }
        setPhotoData(newArray)
    }
    const checkArrayIsAdmin = (user_id, groupData) => {
        var index = groupData.group_members.findIndex(obj => obj.gm_user_id === user_id);
        if (index > -1)
            setGmRole(groupData.group_members[index].gm_role)

    }
    const manageGroupAdmin = async (id, gm_role) => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: id,
            gm_role: gm_role
        }
        await apiCallWithToken(obj, apiUrl.manage_group_admin, token).then(res => {
            if (res.status == true) {
              //  showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getGroupData(token, u_id)
                    changeOwnerShip(id, gm_role == "User" ? CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT : CometChat.GROUP_MEMBER_SCOPE.ADMIN)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const manageGroupPrivacy = async () => {

        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            group_privacy: isToggle ? "Private" : "Public"
        }
        setToggle(!isToggle)
        await apiCallWithToken(obj, apiUrl.manage_group_privacy, token).then(res => {
            if (res.status == true) {

                setTimeout(() => {
                    setShowing(false)
                    getGroupData(token, group_id)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const removeGroupMember = async () => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: itemUserId,
        }
        await apiCallWithToken(obj, apiUrl.remove_group_member, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    getGroupData(token, u_id)
                    removeChatGroupMember(itemUserId);
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const removeChatGroupMember = (id) => {
        let GUID = comeGroupTag + group_id;
        var UID = comeChatUserTag + id;

        CometChat.kickGroupMember(GUID, UID).then(
            response => {
                console.log("Group member kicked successfully", response);
            },
            error => {
                console.log("Group member kicking failed with error", error);
            }
        );
    }
    function get_url_extension(url) {
        return url.split(/[#?]/)[0].split('.').pop().trim() + "";
    }
    const viewImage = ({ item, index }) => {
      
        return (
            <View>
                {(index) % 2 == 0 ?
                    <View>
                        <View>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={{ uri: item.image1 }}></Image>
                        </View>
                        <View>
                            <Image style={{ width: 105, height: 77 }} source={{ uri: item?.image2 }}></Image>
                        </View>
                    </View> :
                    <View>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={{ uri: item.image1 }}></Image>
                    </View>}
            </View>
        )
    }
    const viewListParticipate = ({ item, index }) => {
        return (
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: responsiveHeight(1) }}>
                <TouchableOpacity
                    onPress={() => u_id == item.member.u_id ? props.navigation.navigate("MyProfile") : props.navigation.navigate(strings.UserProfileScreen, { u_id: item.member.u_id })}

                    style={{ flexDirection: "row", alignItems: "center" }}>
                    <Image style={{ height: 48, width: 48, borderRadius: 24 }} source={{ uri: item.member.u_image }}></Image>
                    <View style={{ marginVertical: responsiveHeight(0.5), marginLeft: responsiveWidth(4) }}>
                        <Text style={[styles.txtName, { width: 150 }]}>{item.member.u_first_name + " " + item.member.u_last_name}</Text>
                        <Text style={styles.txtDate}>{item.member.has_total_friends_count + " Friends • " + item.member.total_pets + " Pet"}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    {item.gm_role == "Admin" ?
                        <View style={{ borderRadius: 5, borderWidth: 1, borderColor: Colors.opacityColor2 }}>
                            <Text style={styles.txtAdmin}>{strings.groupAdmin}</Text>
                        </View> : <></>}
                    {(gm_role == "Admin") &&
                        <TouchableOpacity onPress={() => u_id == item.member.u_id ? "" : onMoreClick(1, item.member)}>
                            <Image style={{ height: 14, width: 10, tintColor: Colors.white, marginRight: responsiveWidth(1), marginLeft: responsiveWidth(4) }} source={Images.menuDot}></Image>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }
    const onMoreClick = async (flag, item) => {
        if (flag == 0)
            setBotomFlag(0)
        else {
            setItemClickUser(item.u_id)
            setBotomFlag(1)
        }
        refSheet.open()
    }
    const viewToggle = () => {
        return (
            <View>
                <View style={{ flexDirection: "row" }}>
                    <View style={{ alignItems: "center", padding: 7 }}>
                        <Image style={{ height: 15, width: 15, tintColor: Colors.white }} source={Images.lockIcon}></Image>
                    </View>
                    <View>
                        <Text style={{
                            fontSize: 14, lineHeight: 18.23, fontFamily: Fonts.DMSansRegular,
                            color: Colors.white, fontWeight: "400",
                            marginLeft: 5
                        }}>{"Private Group"}</Text>
                        <Text style={{
                            fontSize: 12, lineHeight: 15.62, fontFamily: Fonts.DMSansRegular,
                            color: Colors.white, fontWeight: "400",
                            marginLeft: 5, opacity: 0.6
                        }}>{"Only the admins can change group details."}</Text>
                    </View>

                    <View style={{ flex: 1, alignItems: "center", flexDirection: "row", justifyContent: "flex-end" }}>

                        <Switch
                            thumbColor={isToggle ? Colors.primaryViolet : "#f4f3f4"}
                            ios_backgroundColor={"#767577"}
                            onValueChange={() => manageGroupPrivacy()}
                            value={isToggle}
                        />

                    </View>
                </View>
                <View style={{ height: 1, flex: 1, backgroundColor: Colors.white, opacity: 0.15, marginHorizontal: 10, marginVertical: 20 }} />

            </View>
        )
    }
    const clickItem = (item) => {
        refSheet.close()
        if (item.key == "removedmin")
            manageGroupAdmin(itemUserId, "User")
        else if (item.key == "addAdmin")
            manageGroupAdmin(itemUserId, "Admin")
        else if (item.key == "removeParticipate")
            removeGroupMember();
        else if (item.key == "delete") {
            leaveGroup();
        }
        else if (item.key == "inviteMember") {
            props.navigation.navigate(strings.addMemberScreen, { groupData: groupData })
        }
        else if (item.key == "groupEvent") {
            props.navigation.navigate(strings.addEventScreen, { group_id: groupData.group_id })

        }
    }
    const leaveGroup = async () => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: u_id,
        }
        await apiCallWithToken(obj, apiUrl.leave_group, token).then(res => {
            if (res.status == true) {
                leaveChatGroup();


            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const leaveChatGroup = () => {
        var GUID = comeGroupTag + group_id; // guid of the group to join

        CometChat.leaveGroup(GUID).then(
            hasLeft => {
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            },
            error => {
                console.log("Group leaving failed with exception:", error);
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            }
        );
    }
    const changeOwnerShip = (id, scope) => {
        var GUID = comeGroupTag + group_id;
        var UID = comeChatUserTag + id;
        var scope = scope;

        CometChat.updateGroupMemberScope(GUID, UID, scope).then(
            response => {
                console.log("Group member scopped changed", response);
            }, error => {
                console.log("Group member scopped changed failed", error);
            }
        );
    }
    const onViewAll = async () => {
        await storeGroupId(group_id)
        setTimeout(() => {
            props.navigation.navigate("Event")
        }, 500);
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <LinearGradient
            colors={Colors.screenBackgroundColor}
            style={styles.container}
        >
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            {groupData != null &&
                <ScrollView showsVerticalScrollIndicator={false}>
                    <EventDetailHeader
                        onBackPress={backPress}
                        isMenu={true}
                        isShare={false}
                        uri={groupData.group_image}
                        onMoreClick={() => onMoreClick(0)}
                        title={groupData.group_name}
                    />
                    <View style={{ marginHorizontal: responsiveWidth(4), marginVertical: responsiveHeight(2) }}>
                        <Text style={styles.txtHeader}>{strings.description}</Text>
                        {
                            about == "" ?
                                <View style={styles.aboutEmpty}>
                                    <Text style={styles.txtNoDesc}>{strings.noDesc}</Text>
                                    <Image style={styles.edtImg} source={Images.editIcon} ></Image>
                                </View> :
                                <View style={styles.aboutView}>
                                    <View style={styles.viewLineVertical}></View>
                                    <Text style={styles.txtDetailData}>{about}</Text>
                                </View>
                        }
                        <View style={styles.viewLine}></View>
                        {(events != null && !Array.isArray(events)) &&
                            <>
                                <View style={{ justifyContent: "space-between", flexDirection: "row", marginTop: responsiveHeight(2) }}>
                                    <Text style={styles.txtHeader}>{strings.events}</Text>
                                    <TouchableOpacity onPress={() => onViewAll()} style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Text style={styles.txtViewAll}>{strings.viewAll}</Text>
                                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                                    </TouchableOpacity>
                                </View>

                                <TouchableOpacity
                                    onPress={() => props.navigation.navigate(strings.EventDetailUpcomingScreen, { item: groupData.events })}
                                    style={{ flexDirection: "row", marginVertical: responsiveHeight(1.5) }}>
                                    <View>
                                        <Image style={{ width: 48, height: 48, borderRadius: 4 }} source={{ uri: events.event_image }}></Image>
                                        <View style={styles.viewRound}>
                                            <Image style={styles.imgCamera} source={Images.cameraIcon}></Image>
                                        </View>
                                    </View>
                                    <View style={{ marginVertical: responsiveHeight(0.5), marginLeft: responsiveWidth(4) }}>
                                        <Text style={styles.txtName}>{events.event_name}</Text>
                                        <Text style={styles.txtDate}>{events.event_start_date + " To " + events.event_end_date}</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.viewLine}></View>

                            </>

                        }
                        {chatImages.length != 0 &&

                            <>
                                <View style={{ justifyContent: "space-between", flexDirection: "row", marginTop: responsiveHeight(2) }}>
                                    <Text style={styles.txtHeader}>{"Photos (" + chatImages.length + ")"}</Text>
                                    <TouchableOpacity onPress={() => props.navigation.navigate(strings.mediaTabScreen, { group_id: group_id })}
                                        style={{ flexDirection: "row", alignItems: "center" }}>
                                        <Text style={styles.txtViewAll}>{strings.viewAll}</Text>
                                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                                    </TouchableOpacity>
                                </View>
                                <FlatList
                                    style={{ marginVertical: responsiveHeight(1) }}
                                    data={photoData}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={viewImage}
                                >
                                </FlatList>
                            </>
                        }
                        <View style={{ justifyContent: "space-between", flexDirection: "row", marginTop: responsiveHeight(2) }}>
                            <Text style={styles.txtHeader}>{"Participants (" + groupData.group_members.length + ")"}</Text>
                            {u_id == groupData.added_by.u_id || groupData.group_privacy != "Private" || gm_role == "Admin" ?
                                <TouchableOpacity onPress={() => props.navigation.navigate(strings.addMemberScreen, { groupData: groupData })}
                                    style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                                    <Text style={styles.txtViewAll}>{strings.inviteMember}</Text>
                                </TouchableOpacity> : <></>
                            }
                        </View>
                        <FlatList
                            style={{ marginVertical: responsiveHeight(1) }}
                            data={groupData.group_members}
                            nestedScrollEnabled={false}
                            showsVerticalScrollIndicator={false}
                            renderItem={viewListParticipate}
                        ></FlatList>
                    </View>
                    <RBSheet
                        ref={(ref) => refSheet = ref}
                        height={bottomFlag == 0 ? 243 : 170}
                        openDuration={500}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                padding: 10,
                            }
                        }}
                    >
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {bottomFlag == 0 && gm_role == "Admin" ? viewToggle() : <></>}
                            <MoreMenu
                                clickItem={(item) => clickItem(item)}
                                data={bottomFlag == 0 ? (gm_role == "Admin" || u_id == groupData.added_by.u_id || groupData.group_privacy != "Private") ? menuData : menuDataUser : adminMenu}
                            >
                            </MoreMenu>
                        </ScrollView>
                    </RBSheet>
                </ScrollView>}

            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
             {isVisibleCall==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisibleCall}
                        onMessageNotification={()=>{}}
                    />
                }
        </LinearGradient>

    )
}
export default ViewGroup;