import React, { createRef, useRef, useState, useEffect } from 'react';
import {
    View, Text, FlatList, Image, TextInput, TouchableOpacity, Platform, Share, Modal,
    KeyboardAvoidingView, Linking, Dimensions, TouchableWithoutFeedback, Animated,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import styles from './ChatMessageStyle';
import { Images } from '../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MessageHeader from '../../../../Components/MessageHeader';
import { Fonts } from '../../../../Resources/Fonts';
import RBSheet from "react-native-raw-bottom-sheet";
import { ProgressCircle } from 'react-native-svg-charts'
import MoreMenu from '../../../../Components/MoreMenu';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import {
    apiCallGetWithToken, apiCallWithToken, createCallLink, getUserData, imageUploadApi,
    mediaMessage, requestCameraPermission, requestExternalWritePermission, sendEventMessage, sendNormalMessage,
    createDynamicLinks, storeCallNotification, addressGetFromLatLng, storeChatNotificationFlag
} from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import CommonButton from '../../../../Components/CommonButton';
import CallingBubbleView from '../../../../Components/CallingBubbleView';
var FloatingLabel = require('react-native-floating-labels');
import { CometChat } from "@cometchat-pro/react-native-chat"
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import moment, { now } from 'moment';
import * as ImagePicker from "react-native-image-picker"
import DocumentPicker from 'react-native-document-picker'
import ChatFileBubbleSender from '../../../../Components/ChatFileBubbleSender';
import ChatFileBubbleReceiver from '../../../../Components/ChatFileBubbleReceiver';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import AudioRecord from '../../../../Components/AudioRecord';
import NotificationController from '../../../../Components/NotificationController';
import { strings } from '../../../../Resources/Strings';
import { EventIconUnselect } from '../../../../utils/svg/EventIconUnselect';
import LocationPicker from '../../../../Components/LocationPicker';
import EventMap from '../../../../Components/EventMap';
import { toastConfig } from '../../../../utils/ToastConfig';
import Geolocation from '@react-native-community/geolocation';
import UserListModal from '../../../../Components/UserListModal';
import EmoijModal from '../../../../Components/EmoijModal';
const { width, height } = Dimensions.get('window');
import SwipeView from 'react-native-swipeview';

const bottomMenu = [
    {
        id: 1,
        title: "Camera",
        image: Images.cameraIcon,
        key: "Camera"
    },
    {
        id: 2,
        title: "Photos",
        image: Images.addPicture,
        key: "Photos"

    },
    {
        id: 3,
        title: "Play dates",
        image: Images.addEventIcon,
        key: "Event"

    },
    {
        id: 4,
        title: "Location",
        image: Images.placeIcon,
        key: "Location"

    },
    {
        id: 5,
        title: "Audio",
        image: Images.mic,
        key: "Audio"

    },
    {
        id: 6,
        title: "Documents",
        image: Images.document,
        key: "Documents"

    }
]

const moreData = [
    {
        key: "viewGroupInformation",
        title: "View Group Information",
        image: Images.petIcon
    },
    {
        key: "changeGroupName",
        title: "Change Group Name",
        image: Images.titleIcon
    },
    {
        key: "changeGroupDescription",
        title: "Change Group Description",
        image: Images.document
    },
    {
        key: "inviteMembers",
        title: "Invite Members",
        image: Images.addFriend
    },
    {
        key: "shareGroup",
        title: "Share Group",
        image: Images.share
    },
    {
        key: "delete",
        title: "Leave group",
        image: Images.logout
    },
]
const moreDataUser = [
    {
        key: "viewGroupInformation",
        title: "View Group Information",
        image: Images.petIcon
    },
    {
        key: "shareGroup",
        title: "Share Group",
        image: Images.share
    },
    {
        key: "delete",
        title: "Leave group",
        image: Images.logout
    },
]
const ChatMessage = (props) => {

    let refSheet = useRef()
    let refSheetMenu = useRef()
    let refSheetEvent = useRef()
    let refSheetReaction = useRef()
    const [flatList, setFlatList] = useState(null);

    const [groupData, setGroupData] = useState(props.route.params.groupData)
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [gm_role, setGmRole] = useState("user")
    const [isModal, setModal] = useState(false)
    const [selectKey, setKey] = useState("")
    const [groupName, setGroupName] = useState("")
    const [groupDesc, setGroupDesc] = useState("")
    const [message, setMessage] = useState("")
    const [chatData, setChatData] = useState([])
    const [group_uuid, setguid] = useState(comeGroupTag + props.route.params.groupData.group_id);
    const [userImage, setImageUser] = useState("")
    const [memberList, setMemberList] = useState([])
    const [isVisible, setIsVisible] = useState(false)
    const [eventData, setEventData] = useState([])
    const [selectedEventRadio, setSelectedEventRadio] = useState();
    const [eventSelectItem, setEventItem] = useState(null)
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [isSerachMap, setSerachMap] = useState(false)
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const [currentAddress, setCurrentAddress] = useState("")
    const [u_first_name, setFirstName] = useState("")
    const [tagData, setTagData] = useState(null)
    const [tagClickIndex, setTagClickIndex] = useState(-1)
    const [isVisibleModal, setVisibleModal] = useState(false)
    const [reactionListData, setReactionList] = useState("")
    const [replayData, setReplayData] = useState(null)
    useEffect(async () => {
        await storeCallNotification("")
        storeChatNotificationFlag({ chat_notification_flag: false })

        getUserData().then(res => {
            setShowing(true)
            setImageUser(res.u_image)
            setUserID(res.u_id)
            setToken(res.token)
            setFirstName(res.u_first_name)
            getGroupData(res.token, res.u_id)
            joinGroup();
        })
        getMessage();

        //getCurrentAddres();
    }, []);
    const getCurrentAddres = () => {

        Geolocation.getCurrentPosition(info => {
            addressGetFromLatLng(info.coords.latitude, info.coords.longitude).then(res => {
                setCurrentAddress(res.results[0].formatted_address)
            })
        }
        );

    }
    var listnerID = group_uuid;
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                var sessionID = call.sessionId;
                getMessage()
                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);

            },
            onOutgoingCallAccepted: (call) => {
                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // Outgoing Call Rejected
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)

            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    var listenerID = group_uuid;
    CometChat.addMessageListener(
        listenerID,
        new CometChat.MessageListener({
            onTextMessageReceived: textMessage => {
                getMessage();
                // Handle text message
            },
            onMediaMessageReceived: mediaMessage => {
                getMessage();
                // Handle media message
            },
            onCustomMessageReceived: customMessage => {
                getMessage();

                // Handle custom message
            }
        })
    );


    const getGroupData = async (token, u_id) => {
        await apiCallGetWithToken(apiUrl.get_group_details + "?group_id=" + groupData.group_id, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setGroupData(res.result)
                setGroupName(res.result.group_name)
                setGroupDesc(res.result.group_description)
                checkArrayIsAdmin(u_id, res.result)
                getEventData(token)
                setMemberList(res.result.group_members)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const getMessage = () => {
        var GUID = group_uuid;
        var limit = 70;
        let messagesRequest = new CometChat.MessagesRequestBuilder()
            .setCategories(["message", "custom", "call"])
            .setLimit(limit)
            .setGUID(GUID)
            .build();

        messagesRequest.fetchPrevious().then(
            messages => {
                sortingData(messages)
                markAsRead(messages[messages.length - 1])
            },
            error => {
                console.log("Message fetching failed with error:", error);
            }
        );

    }

    const sortingData = (messages) => {
        var newDateArray = [];
        for (var i = 0; i < messages.length; i++) {
            var newDate = moment.unix(messages[i].sentAt).format(strings.mmmDDYYYYFormat);
            var obj = { ...messages[i] }
            if (i != 0) {
                var previousDate = moment.unix(messages[i - 1].sentAt).format(strings.mmmDDYYYYFormat);
                if (newDate == previousDate) {
                    obj.txtdate = "";
                }
                else
                    obj.txtdate = newDate;

            }
            else
                obj.txtdate = newDate;
            obj.isSelectTag = false

            newDateArray.push(obj)
        }
        setChatData(newDateArray)

    }
    const joinGroup = () => {
        var GUID = group_uuid;
        var password = "";
        var groupType = CometChat.GROUP_TYPE.PUBLIC;

        CometChat.joinGroup(GUID, groupType, password).then(
            group => {
                console.log("Group joined successfully:", group);
            },
            error => {
                console.log("Group joining failed with exception:", error);
            }
        );
    }
    const callClick = () => {
        var receiverID = group_uuid;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.GROUP;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                // perform action on success. Like show your calling screen.
                // props.navigation.navigate("callScreen", { callData: outGoingCall })
                callLinkCreate(outGoingCall)


            },
            error => {
                console.log("Call initialization failed with exception:", error);
            }
        );
    }
    const callLinkCreate = async (call) => {
        var callData = {
            callType: "audio",
            userType: "group",
            id: groupData.group_id,
            title: groupData.group_name,
            descriptionText: "Please click to link call joining",
            flag: "call",
            sessionId: call.sessionId
        }
        props.navigation.navigate(strings.callScreen, { callData: call, flag: "group" })

    }
    const menuClick = async () => {
        refSheetMenu.open()
    }

    const renderCategory = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => bottomCategory(item)} style={{ marginHorizontal: 20, height: 115 }}>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <View style={styles.borderView}></View>
                    <Image source={item.image} style={styles.cameraStyle}></Image>
                </View>
                <Text style={styles.labelText}>{item.title}</Text>
            </TouchableOpacity>
        )
    }
    const bottomCategory = (item) => {
        refSheet.close()

        if (item.key == "Camera") {
            setTimeout(() => {
                launchCamera();
            }, Platform.OS == "ios" ? 600 : 200);

        }
        else if (item.key == "Photos") {
            setTimeout(() => {
                launchImageLibrary();
            }, Platform.OS == "ios" ? 600 : 200);

        }
        else if (item.key == "Documents") {
            setTimeout(() => {
                pickDocument();
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Audio") {
            setTimeout(() => {
                setIsVisible(true)
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Location") {
            setTimeout(() => {
                setSerachMap(true)
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Event") {
            setTimeout(() => {
                refSheetEvent.open();
            }, Platform.OS == "ios" ? 600 : 200);
        }
    }

    const pickDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText, DocumentPicker.types.zip],
            })

            var file = {
                name: res.name,
                type: res.type,
                uri: res.uri
            }
            let array = file.type.split("/")
            setShowing(true)
            mediaMessage(group_uuid, "group", file, "", userImage, replayData).then(res => {
                setReplayData(null)

                if (array[0] == "audio") {
                    setShowing(false)

                    createPost("Audio", file)
                }
                else if (array[0] == "video") {
                    setShowing(false)
                    createPost("Video", file)
                }

                else {
                    setShowing(false)
                    sendNotification("Message from " + groupData.group_name, "Document Share in " + groupData.group_name)
                }
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err
            }
        }
    }
    const launchCamera = async () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.5,
            maxHeight: height,
            maxWidth: width

        };
        if (Platform.OS == "android") {
            let isCameraPermitted = await requestCameraPermission();
            let isStoragePermitted = await requestExternalWritePermission();
            if (isCameraPermitted && isStoragePermitted) {
            }
            else {
                return 0;
            }
        }
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                var file = {
                    name: response.assets[0].fileName,
                    type: response.assets[0].type,
                    uri: response.assets[0].uri
                }
                setShowing(true)
                mediaMessage(group_uuid, "group", file, "", userImage, replayData).then(res => {
                    setReplayData(null)
                    createPost("Photo", file)
                })
            }
        });
    }
    const moreMenu = () => {
        return (
            <View style={{ alignItems: "center", marginTop: 10 }}>
                <FlatList
                    data={bottomMenu}
                    numColumns={3}
                    renderItem={renderCategory}
                >
                </FlatList>
            </View>
        )
    }
    function getChatTime(timestamp) {
        var t = new Date(timestamp * 1000);
        var hours = t.getHours();
        var minutes = t.getMinutes();
        var newformat = t.getHours() >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format 
        hours = hours % 12;

        // To display "0" as "12" 
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return "" + hours + ":" + minutes + " " + newformat

    }

    const onReaction = (objEmoji) => {
        var newArray = []
        {
            Object.entries(objEmoji).map(([key, v]) => {
                // console.log(key,v)
                var obj = {
                    key: key,
                    data: Object.values(v)
                }
                newArray.push(obj)
                //return <Viekw key={key}><Text>{v}</Text></View>
            })
        }
        setReactionList(newArray)
        refSheetReaction.open()
    }
    const simpleMessage = (item, index) => {
        var newDate = getChatTime(item.sentAt);
        //style={{ }}
        var objEmoji = isEmoji(item)

        return (
            item.sender.uid != comeChatUserTag + u_id ?
                <View style={{ flex: 1 }}>
                    <SwipeView
                        swipeDuration={500}
                        //previewOpenValue={-100}
                        // onSwipedLeft={()=>}
                        onSwipedRight={() =>
                            setTimeout(() => {
                                setReplayData(item)
                            }, 500)
                        }
                        renderVisibleContent={() =>

                            <View
                                style={{
                                    alignItems: "flex-start", justifyContent: "flex-start",
                                    backgroundColor: item.isSelectTag == true ? Colors.opacityColor : Colors.backgroundColor
                                }}>

                                <Text style={styles.txtName}>{item.sender.name}</Text>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flexDirection: "row", flex: 0.7 }}>
                                        <Image style={styles.imgProfile} source={{ uri: item.data.metadata?.senderImageUri }}></Image>
                                        <View >
                                            <TouchableOpacity onLongPress={() => setTagData(item)} style={styles.messageView}>
                                                <View>
                                                    {(item.data.metadata?.tagData != undefined && item.data.metadata?.tagData != null) && tagViewItSelf(item.data.metadata.tagData)}

                                                    {item.data.metadata?.isCall == true ? <TouchableOpacity onPress={() => Linking.openURL(item.text)}>
                                                        <Text style={styles.txtMessage}>{item.text}</Text>

                                                    </TouchableOpacity> :
                                                        <Text style={styles.txtMessage}>{item.text}</Text>}
                                                </View>
                                                <Text style={styles.txtTime}>{newDate}</Text>

                                            </TouchableOpacity>
                                            {objEmoji != null ?
                                                <TouchableOpacity
                                                    onPress={() => onReaction(objEmoji)}
                                                    style={styles.likeView}>
                                                    <Text style={styles.imgHerat}>{Object.keys(objEmoji)[0].toString()}</Text>
                                                    {Object.keys(objEmoji).length > 1 &&
                                                        <Text style={styles.txtLike}>{"1+"}</Text>}
                                                </TouchableOpacity>
                                                : <></>}

                                        </View>
                                    </View>
                                    <View style={{ flex: 0.3 }}></View>
                                </View>

                            </View >
                        }
                    />
                </View> :
                <View style={{ flex: 1 }}>
                    <SwipeView
                        swipeDuration={500}

                        // onSwipedLeft={()=>}
                        onSwipedRight={() =>
                            setTimeout(() => {
                                setReplayData(item)
                            }, 500)
                        }
                        renderVisibleContent={() =>
                            <TouchableOpacity onLongPress={() => setTagData(item)} style={{
                                alignItems: "flex-end", justifyContent: "flex-end",
                                backgroundColor: item.isSelectTag == true ? Colors.opacityColor : Colors.backgroundColor

                            }}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 0.3 }}></View>
                                    <View style={{ flexDirection: "row", flex: 0.7, alignItems: "flex-end", justifyContent: "flex-end", marginRight: 12 }}>

                                        <View>

                                            <LinearGradient colors={Colors.btnBackgroundColor} style={styles.messageView}>
                                                {(item.data.metadata?.tagData != undefined && item.data.metadata?.tagData != null) && tagViewItSelf(item.data.metadata.tagData)}

                                                <Text style={styles.txtMessage}>{item.text}</Text>
                                                <Text style={styles.txtTime}>{newDate}</Text>
                                            </LinearGradient>
                                            {objEmoji != null ?
                                                <TouchableOpacity
                                                    onPress={() => onReaction(objEmoji)}
                                                    style={styles.likeView}>
                                                    <Text style={styles.imgHerat}>{Object.keys(objEmoji)[0].toString()}</Text>
                                                    {Object.keys(objEmoji).length > 1 &&
                                                        <Text style={styles.txtLike}>{"1+"}</Text>}
                                                </TouchableOpacity>
                                                : <></>}
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity >}
                    />
                </View>
        )
    }
    function get_url_extension(url) {
        return url?.split(/[#?]/)[0].split('.').pop().trim() + "";
    }

    const imageWithMessage = (item) => {
        var newDate = getChatTime(item.sentAt);
        var isUser = item.sender.uid != comeChatUserTag + u_id
        var fileType = get_url_extension(item.data.url)
        var newDate1 = moment(new Date(item.sentAt * 1000)).format('MMMM DD, YYYY');
        var objEmoji = isEmoji(item)

        return (
            <View style={{ flex: 1 }}>
                <SwipeView
                    // onSwipedLeft={()=>}
                    swipeDuration={500}
                    onSwipedRight={() =>
                        setTimeout(() => {
                            setReplayData(item)
                        }, 500)
                    }

                    renderVisibleContent={() =>
                        <View style={{ flex: 1, flexDirection: "row", backgroundColor: item.isSelectTag == true ? Colors.opacityColor : Colors.backgroundColor }}>
                            {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}

                            {fileType === "jpg" || fileType == "jpeg" || fileType == "png" ?
                                <TouchableOpacity
                                    onLongPress={() => setTagData(item)}
                                    onPress={() => { props.navigation.navigate(strings.webViewScreen, { url: item.data.url, title: item.sender.name, date: newDate1 }) }}
                                    style={{ alignItems: "flex-start", justifyContent: "flex-start", flex: 1, }}
                                >

                                    {isUser && <Text style={styles.txtName}>{item.sender.name}</Text>}

                                    <View style={{ flexDirection: "row", }}>

                                        {isUser && <Image style={styles.imgProfileEvent} source={{ uri: item.data.metadata?.senderImageUri }}></Image>}
                                        <View style={{ flex: 0.79 }}>

                                            <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6, width: responsiveWidth(71.5) }}>
                                                {(item.data.metadata?.tagData != undefined && item.data.metadata?.tagData != null) && tagViewItSelf(item.data.metadata.tagData)}

                                                {item.data.text != undefined && <Text style={styles.txtMessage}>{item.data.text}</Text>}
                                                <Image style={{ height: 171, width: responsiveWidth(66), margin: 10 }} source={{ uri: item.data.url }}></Image>
                                                <Text style={styles.txtTime}>{newDate}</Text>

                                            </View>
                                            {objEmoji != null ?
                                                <TouchableOpacity style={styles.likeView}>
                                                    <Text style={styles.imgHerat}>{Object.keys(objEmoji)[0].toString()}</Text>
                                                    {Object.keys(objEmoji).length > 1 &&
                                                        <Text style={styles.txtLike}>{"1+"}</Text>}
                                                </TouchableOpacity>
                                                : <></>}
                                        </View>
                                        <View style={{ flex: 0.21 }} />
                                    </View>

                                </TouchableOpacity> :
                                isUser == false ?

                                    <ChatFileBubbleSender
                                        navigation={props.navigation}
                                        data={item}
                                        onLongPress={(data) => setTagData(data)}
                                        groupName={groupData.group_name}
                                        tagViewItSelf={() => tagViewItSelf(item.data.metadata.tagData)}
                                    />
                                    :


                                    <ChatFileBubbleReceiver
                                        navigation={props.navigation}
                                        data={item}
                                        onLongPress={(data) => setTagData(data)}
                                        groupName={groupData.group_name}
                                        tagViewItSelf={() => tagViewItSelf(item.data.metadata.tagData)}

                                    />

                            }
                        </View>}
                />
            </View>
        )
    }
    const event = (item) => {
        if (item.data.customData.eventData == null || item.data.customData.eventData == undefined || item.data.customData.eventData == "")
            return
        var day = new Date(item.data.customData.eventData?.event_start_date).getDate();
        var month = new Date(item.data.customData.eventData?.event_start_date).getMonth();
        var isUser = item.sender.uid != comeChatUserTag + u_id
        var newDate = getChatTime(item.sentAt);
        var objEmoji = isEmoji(item)

        return (

            <View style={{ flex: 1 }}>
                <SwipeView
                    swipeDuration={500}

                    // onSwipedLeft={()=>}
                    onSwipedRight={() =>
                        setTimeout(() => {
                            setReplayData(item)
                        }, 500)
                    }
                    renderVisibleContent={() =>
                        <View style={{ alignItems: "flex-start", justifyContent: "flex-start", backgroundColor: item.isSelectTag == true ? Colors.opacityColor : Colors.backgroundColor }}>
                            {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}

                            {isUser && <Text style={styles.txtName}>{item.sender.name}</Text>}
                            <TouchableOpacity style={{ flexDirection: "row" }}
                                onPress={() => props.navigation.navigate(strings.EventDetailUpcomingScreen, { item: item.data.customData.eventData })}
                                onLongPress={() => setTagData(item)}

                            >
                                {isUser ? <Image style={styles.imgProfileEvent} source={{ uri: item.metadata.senderImageUri }}></Image> :
                                    <View style={{ marginLeft: 80 }} />}
                                <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6, width: responsiveWidth(70) }}>
                                    {(item.data.metadata?.tagData != undefined && item.data.metadata?.tagData != null) &&
                                        tagViewItSelf(item.data.metadata.tagData)}
                                    <Text style={styles.txtMessage}>{item.data.customData.eventData?.event_name}</Text>
                                    <View style={styles.renderStyle} >
                                        <Image source={{ uri: item.data.customData.eventData?.event_image }} style={styles.imgStyle}></Image>
                                        <View style={styles.upperView}>
                                            <View style={styles.upperSubView}>
                                                <View style={{ flexDirection: "row" }}>
                                                    {item.data?.customData?.eventData?.event_members?.map((data, parentIndex) => (
                                                        parentIndex < 3 ?
                                                            parentIndex != 2 && parentIndex < 2 ?
                                                                <Image style={[styles.peopleImg, { marginLeft: -8 }]} source={{ uri: data.member.u_image }}></Image> :
                                                                <View style={styles.moreView}>
                                                                    <View style={styles.moreBorderView}></View>
                                                                    <Text style={styles.txtMore}>{"+ " + parseInt(item.data.customData.eventData?.event_members.length - 2)}</Text>
                                                                </View> : <></>
                                                    ))}
                                                </View>
                                                <View style={styles.dateView}>
                                                    <Text style={styles.txtDate}>{day}</Text>
                                                    <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <LinearGradient style={{
                                            position: "absolute",
                                            height: responsiveHeight(18),
                                            width: responsiveWidth(68), bottom: 0,
                                        }} colors={Colors.viewCare}>
                                        </LinearGradient>
                                        <View style={styles.bottomView}>
                                            <Text style={styles.txtTitle}>{item.title}</Text>
                                            <View style={styles.bottomBorderView}></View>
                                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                                <Image style={styles.placeIcon} source={Images.placeIcon}></Image>
                                                <Text style={styles.txtAddress}>{item.data.customData.eventData?.event_location}</Text>
                                            </View>
                                        </View>

                                    </View>
                                    <Text style={styles.txtTime}>{newDate}</Text>

                                </View>
                                <View style={{ flex: 0.21 }}></View>

                            </TouchableOpacity>
                            {objEmoji != null ?
                                <TouchableOpacity
                                    onPress={() => onReaction(objEmoji)}
                                    style={[styles.likeView, { left: responsiveWidth(25) }]}>
                                    <Text style={styles.imgHerat}>{Object.keys(objEmoji)[0].toString()}</Text>
                                    {Object.keys(objEmoji).length > 1 &&
                                        <Text style={styles.txtLike}>{"1+"}</Text>}
                                </TouchableOpacity>
                                : <></>}
                        </View>}
                />
            </View>
        )
    }

    const progressActivity = (item) => {
        return (
            <View style={{ alignItems: "flex-start", justifyContent: "flex-start" }}>
                <Text style={styles.txtName}>{item.name}</Text>
                <View style={{ flexDirection: "row" }}>
                    <Image style={styles.imgProfileEvent} source={item.profileImage}></Image>
                    <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6, flex: 0.79 }}>
                        <Text style={styles.txtMessage}>{item.message}</Text>
                        <View style={styles.chartView}>
                            <View style={{ alignItems: "center", justifyContent: "center", }}>
                                <ProgressCircle style={{ height: 110, width: 110 }} progress={0.7} backgroundColor={'rgb(255,185,27,0.2)'} strokeWidth={6.17} progressColor="#FFB91B" />
                                <ProgressCircle style={{ height: 87, width: 87, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(255,45,101,0.2)'} progress={1} progressColor='#FF2D65' />
                                <ProgressCircle style={{ height: 64, width: 64, position: "absolute" }} strokeWidth={6.17} backgroundColor={'rgb(0,205,249,0.2)'} progress={0.7} progressColor={"#00CDF9"} />
                            </View>
                            <View>
                                <View style={{ flexDirection: "row", }}>
                                    <View style={styles.viewBox1}></View>
                                    <View>
                                        <Text style={styles.txtMiles}>{"4.4 / 6 miles"}</Text>
                                        <Text style={styles.txtLabel}>{"Total run"}</Text>
                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: 5 }}>
                                    <View style={styles.viewBox2}></View>
                                    <View>
                                        <Text style={styles.txtMiles}>{"6.4 / 4.5 miles\n14% better"}</Text>
                                        <Text style={styles.txtLabel}>{"Than other pets"}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: "row", marginTop: 5 }}>
                                    <View style={styles.viewBox3}></View>
                                    <View>
                                        <Text style={styles.txtMiles}>{"4.4 / 7 miles\n14% better"}</Text>
                                        <Text style={styles.txtLabel}>{"Than friend’s pets"}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.txtTime}>{item.time}</Text>

                    </View>
                </View>
            </View>
        )
    }
    const onSelectEmoij = (emoij) => {

        CometChat.callExtension('reactions', 'POST', 'v1/react', {
            msgId: tagData.id,
            emoji: emoij
        }).then(response => {
            // Reaction added successfully
            setTagData(null)
            getMessage()

        }).catch(error => {
            // Some error occured
            setTagData(null)

        });
    }
    const isEmoji = (item) => {
        var objEmoji = null;
        var metadata = item.metadata;
        if (metadata != null) {
            var injectedObject = metadata["@injected"];
            if (injectedObject != null && injectedObject.hasOwnProperty("extensions")) {
                var extensionsObject = injectedObject["extensions"];
                if (
                    extensionsObject != null &&
                    extensionsObject.hasOwnProperty("reactions")
                ) {
                    var reactionsObject = extensionsObject["reactions"];
                    objEmoji = reactionsObject;
                }
            }
        }
        return objEmoji;
    }
    const renderMessage = ({ item, index }) => {
        return (

            <View style={{ marginBottom: 15, flex: 1 }}>
                {item.txtDate != "" && <Text style={styles.txtDateHead}>{item.txtdate}</Text>}

                {
                    item.type == "text" ?
                        simpleMessage(item, index) :
                        item.type == "file" || item.type == "messageWithMap" ?
                            imageWithMessage(item) :
                            item.type == "Event" ?
                                event(item) :
                                item.type == "messageWithActivity" ?
                                    progressActivity(item) :
                                    item.type == "Location" ?
                                        locationView(item) :
                                        item.category == "call" ? bubbleView(item) : null

                }
                <EmoijModal
                    isModalVisible={tagData == item ? true : false}
                    setModalVisible={() => setTagData(null)}
                    onSelectEmoij={(value) => onSelectEmoij(value)}
                />
            </View>
        )
    }
    const bubbleView = (item) => {
        var isUser = item.sender.uid == comeChatUserTag + u_id

        return (
            <CallingBubbleView
                text={isUser ? "You have initiated a call" : item.sender.name + " have initiated a call"}
                style={!isUser ? styles.leftVieW : styles.rightView}
                item={item}
                onJoin={() => onJoin(item)} />
        )
    }
    const onJoin = (item) => {
        CometChat.acceptCall(item.sessionId).then(
            call => {
                props.navigation.navigate(strings.callScreen, { callData: call })
                // start the call using the startCall() method
            },
            error => {
                console.log(error)
                if (error.code == "ERR_CALL_TERMINATED")
                    showMessage(strings.error, "Session Expired", "Call session is expired.")
                else if (error.code == "ERR_CALL_GROUP_ALREADY_JOINED") {
                    showMessage(strings.error, "Session Expired", "Call session is expired.")
                }
                else
                    showMessage(strings.error, "", error.message)
                // handle exception
            }
        );
    }
    const locationView = (item) => {

        var newDate = getChatTime(item.sentAt);

        var isUser = item.sender.uid != comeChatUserTag + u_id

        const obj = {
            name: item.sender.name,
            time: newDate,
            fullDate: item.sentAt,
            imageUri: item.metadata.senderImageUri,
            latitude: item.data.customData.locationData.lat,
            longitude: item.data.customData.locationData.lng,
            isShare: false
        }
        var objEmoji = isEmoji(item)

        return (

            <View style={{ flex: 1 }}>
                <SwipeView
                    swipeDuration={500}
                    onSwipedLeft={() =>
                        setTimeout(() => {
                            setReplayData(item)
                        }, 500)}

                    // onSwipedLeft={()=>}
                    onSwipedRight={() =>
                        setTimeout(() => {
                            setReplayData(item)
                        }, 500)
                    }
                    renderVisibleContent={() =>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("viewLocation", { mapData: obj })}
                            onLongPress={() => setTagData(item)}
                            style={{
                                alignItems: "flex-start", justifyContent: "flex-start",
                                backgroundColor: item.isSelectTag == true ? Colors.opacityColor : Colors.backgroundColor
                            }}>
                            {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}
                            {isUser && <Text style={styles.txtName}>{item.sender.name}</Text>}

                            <View
                                style={{ flexDirection: "row" }}>
                                {isUser ? <Image style={styles.imgProfileEvent} source={{ uri: item.metadata.senderImageUri }}></Image> :
                                    <View style={{ marginLeft: 80 }} />}

                                <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6, width: responsiveWidth(75.5) }}>
                                    {(item.data.metadata?.tagData != undefined && item.data.metadata?.tagData != null) &&
                                        tagViewItSelf(item.data.metadata.tagData)}

                                    <Text style={[styles.txtMessage, { width: responsiveWidth(71.5) }]}>{item.data.customData.locationData.formatted_address}</Text>
                                    <EventMap lat={item.data.customData.locationData.lat} long={item.data.customData.locationData.lng} style={styles.imgMap} />
                                    <Text style={[styles.txtTime]}>{newDate}</Text>
                                </View>
                            </View>
                            {objEmoji != null ?
                                <TouchableOpacity
                                    onPress={() => onReaction(objEmoji)}
                                    style={[styles.likeView, { left: responsiveWidth(25) }]}>
                                    <Text style={styles.imgHerat}>{Object.keys(objEmoji)[0].toString()}</Text>
                                    {Object.keys(objEmoji).length > 1 &&
                                        <Text style={styles.txtLike}>{"1+"}</Text>}
                                </TouchableOpacity>
                                : <></>}
                        </TouchableOpacity>}
                />
            </View>

        )
    }
    const clickItem = (item) => {
        refSheetMenu.close();
        if (item.key == "viewGroupInformation") {
            props.navigation.navigate(strings.viewGroupScreen, { group_id: groupData.group_id })
        }
        else if (item.key == "shareGroup") {
            var obj = {
                id: groupData.group_id,
                title: groupData.group_name,
                imageUrl: groupData.group_image,
                descriptionText: groupData.descriptionText,
                flag: "group"
            }
            setTimeout(() => {
                createDynamicLinks(obj).then(res => {
                    onShare(res)
                })
            }, 500);

        }
        else if (item.key == "delete") {

            leaveGroup();
        }
        else if (item.key == "changeGroupDescription" || item.key == "changeGroupName") {
            setTimeout(() => {
                if (item.key == "changeGroupDescription")
                    setKey("desc")
                else
                    setKey("name")
                setModal(true)
            }, 400);
        }
        else if (item.key == "inviteMembers") {
            props.navigation.navigate('addMember', { groupData: groupData })
        }
    }
    const changeUserScopeChat = (id) => {
        var GUID = comeGroupTag + groupData.group_id;
        var UID = comeChatUserTag + id;
        var scope = CometChat.GROUP_MEMBER_SCOPE.ADMIN;

        CometChat.updateGroupMemberScope(GUID, UID, scope).then(
            response => {
                leaveGroup();
            }, error => {
                console.log("Group member scopped changed failed", error);
            }
        );
    }
    const leaveGroup = async () => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: u_id,
        }
        await apiCallWithToken(obj, apiUrl.leave_group, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    leaveChatGroup();
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const sendNotification = async (title, message) => {
        setShowing(false)
        getMessage();


    }
    const getEventData = async (token) => {
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setEventData(res.result)
            }
            else {
                setShowing(false)

            }
        })
    }
    const leaveChatGroup = () => {
        var GUID = group_uuid; // guid of the group to join

        CometChat.leaveGroup(GUID).then(
            hasLeft => {
                console.log("Group left successfully:", hasLeft);
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            },
            error => {
                console.log("Group leaving failed with exception:", error);
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            }
        );
    }
    const onShare = async (link) => {

        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType

                } else {

                }

            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    const checkArrayIsAdmin = (user_id, groupData) => {
        var index = groupData.group_members.findIndex(obj => obj.gm_user_id === user_id);
        if (index > -1)
            setGmRole(groupData.group_members[index].gm_role)
    }
    const modalNameChange = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={isModal}
            >
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Text style={styles.txtRate}>{selectKey == "name" ? "Change Group Name" : "Change Group Description"}</Text>

                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        multiline={selectKey == "name" ? false : true}
                        value={selectKey == "name" ? groupName : groupDesc}
                        onChangeText={(text) => selectKey == "name" ? setGroupName(text) : setGroupDesc(text)}

                    >{selectKey == "name" ? "Group Name" : "Description"}</FloatingLabel>
                    <View style={{ flexDirection: "row", alignSelf: "flex-end", marginTop: 20 }}>
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={[Colors.opacityColor2, Colors.opacityColor2]}
                            btnText={styles.btnText}
                            viewStyle={{ marginTop: 10, flex: 0.3, marginRight: 10 }}
                            onPress={() => { setModal(false) }}
                            text={"Cancel"}
                        />
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={Colors.btnBackgroundColor}
                            btnText={styles.btnText}
                            viewStyle={{ marginVertical: 10, flex: 0.4 }}
                            onPress={() => changeGroupDetail()}
                            text={"Save"}
                        />
                    </View>
                </View>
            </Modal >

        )
    }
    const changeGroupDetail = async () => {
        setModal(false)
        let data = new FormData();
        data.append("group_name", groupName)
        data.append("group_description", groupDesc)
        data.append("group_id", groupData.group_id)
        setShowing(true)

        await imageUploadApi(data, apiUrl.edit_group, token).then(res => {
            if (res.status) {
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const onClickSend = () => {
        if (message.trim().length != 0)
            sendNormalMessage(message.trim(), group_uuid, "group", userImage, replayData).then(res => {
                setMessage("")
                setReplayData(null)
                getMessage();
            })
    }
    const launchImageLibrary = async () => {
        let options = {
            quality: 0.8,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxHeight: height,
            maxWidth: width
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');

            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                var file = {
                    name: response.assets[0].fileName,
                    type: response.assets[0].type,
                    uri: response.assets[0].uri
                }
                setShowing(true)
                mediaMessage(group_uuid, "group", file, "", userImage, replayData).then(res => {
                    setReplayData(null)

                    createPost("Photo", file)
                })
            }
        });
    }
    const stopRecord = (value) => {
        setIsVisible(false)
        var file = {
            name: value.filename,
            type: "audio/mpeg",
            uri: "file://" + value.path
        }
        setShowing(true)

        mediaMessage(group_uuid, "group", file, "", userImage, replayData).then(res => {
            setReplayData(null)

            createPost("Audio", file)
        })
    }
    const closeRecord = () => {
        setIsVisible(false)
    }
    const addEventClick = () => {
        refSheetEvent.close();
        props.navigation.navigate(strings.addEventScreen)
    }
    const writeMessage = (text) => {
        if (text.slice(-1) == "@") {
            setMessage(text)
            setVisibleModal(true)

        }
        else {
            setVisibleModal(false)
            setMessage(text)
        }
    }
    const renderListHeader = () => {
        return (
            <View >
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheet.close()}>

                </TouchableOpacity>
                <View style={styles.textInputSearch} >
                    <Image source={Images.serachIcon} style={styles.serachIcon} resizeMode={'contain'} />
                    <TextInput
                        placeholder={'Search for play dates'}
                        placeholderTextColor={Colors.white}
                        //multiline={true}
                        //value={serachText}
                        // onChangeText={(text) => setSerachText(text)}
                        //onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onEndEditing(text)}
                        style={styles.searchBox}
                    />
                </View>
                <View>
                    <TouchableOpacity style={styles.createEventContainer}
                        onPress={() => addEventClick()}
                    >
                        <View style={styles.createEventImage}>
                            <EventIconUnselect height={18} width={18} opacity={1} />
                        </View>
                        <View style={[styles.smallIcon, {
                            backgroundColor: Colors.primaryViolet,
                            borderWidth: 1, borderColor: Colors.theme,
                        }]}>
                            <Image style={{ height: 12, width: 12 }} source={Images.plusIcon}></Image>
                        </View>
                        <Text style={styles.createEvent}>Create a new play date </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    const renderItem = ({ item, index }) => {
        return (
            <View>
                <View style={styles.createEventContainer}>
                    <View style={styles.createEventImage}>
                        <Image source={{ uri: item.event_image }} style={styles.eventImage} />
                    </View>
                    <View style={[styles.smallIcon2, {
                        backgroundColor: Colors.roundBackground, borderWidth: 1,
                        borderColor: Colors.theme
                    }]}>
                        <Image source={Images.addEventIcon} style={styles.smallEventIcon} resizeMode={'contain'} />
                    </View>
                    <View style={styles.eventItem}>
                        <View>
                            <Text style={styles.eventTitle}>{item.event_name}</Text>
                            <Text style={styles.eventTime}>{item.event_start_date + " To " + item.event_end_date} </Text>
                        </View>
                        <TouchableOpacity style={[styles.radioBtn, { borderColor: selectedEventRadio == item.event_id ? Colors.primaryViolet : Colors.white }]} onPress={() => selectedEvent(item)}>
                            <View style={selectedEventRadio == item.event_id ? styles.radioBtnSelect : null}></View>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        )
    }
    const selectedEvent = (item) => {
        setEventItem(item)
        setSelectedEventRadio(item.event_id)
    }
    const EventList = () => {
        return (
            <View style={styles.eventListContainer}>
                <FlatList
                    data={eventData}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={renderListHeader}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={renderFooter}
                />
            </View>
        )
    }
    const renderReactionItem = ({ item, index }) => {
        return (
            <View style={{ marginBottom: 5, }}>
                <View style={{ backgroundColor: Colors.progressBackground }}>
                    <Text style={{ fontSize: 20 }}>{item.key}</Text>
                </View>
                {item.data.map((data) => {
                    return (
                        <View style={{ flexDirection: "row", alignItems: "center", marginVertical: 5 }}>
                            <Image source={{ uri: data.avatar }} style={{ height: 30, width: 30, borderRadius: 30 }} />
                            <Text style={[styles.eventTitle, { marginLeft: 20 }]} >{data.name}</Text>
                        </View>
                    )
                })
                }


            </View>
        )
    }
    const renderReactionHeader = () => {
        return (
            <Text style={styles.txtReaction}>Reactions</Text>
        )
    }
    const reactionList = () => {
        return (

            <FlatList
                data={reactionListData}
                style={{ flex: 1 }}
                renderItem={renderReactionItem}
                keyExtractor={item => item.key}
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={renderReactionHeader}
            />
        )
    }
    const renderFooter = () => {
        return (
            <CommonButton
                btnlinearGradient={styles.btnlinearGradient}
                colors={Colors.btnBackgroundColor}
                btnText={styles.btnText}
                viewStyle={styles.verifyBtnGroup}
                onPress={() => onProceed()}
                text={strings.proceed}
            />
        )
    }
    const onProceed = () => {
        refSheetEvent.close()
        setTimeout(() => {
            sendEventMessage(group_uuid, userImage, eventSelectItem, "Event", replayData).then(res => {
                setReplayData(null)
                createPost("Event", eventSelectItem)
            })
        }, 400);
    }
    const saveLocation = (data) => {

        setSerachMap(false)
        if (data == null || data == undefined) {
            showMessage(strings.error, strings.msgTag, "Location Data is required!")
        }
        else {
            var obj = {
                lat: data.geometry.location.lat,
                lng: data.geometry.location.lng,
                formatted_address: data.formatted_address
            }
            sendEventMessage(group_uuid, userImage, obj, "Location", replayData).then(res => {
                setReplayData(null)
                createPost("Location", obj)

            })
        }

    }
    const sendChatNotification = async (title, message) => {

        let result = groupData.group_members.map(a => a.gm_user_id);
        var obj = {
            title: title,
            message: message,
            group_id: groupData.group_id,
            user_ids: result.toString(),

        }

        await apiCallWithToken(obj, apiUrl.send_chat_notifications, token).then(res => {
            setShowing(false)
            getMessage();
        })
    }
    const closeModal = () => {
        setSerachMap(false)
    }
    const markAsRead = (message) => {
        CometChat.markAsRead(message).then(
            () => {
                console.log("mark as read success.");
            }, error => {
                console.log("An error occurred when marking the message as read.", error);
            }
        );
    }
    const createPost = async (type, file) => {
        let data = new FormData();
        data.append("post_name", "");
        if (file != null && (type == "Photo" || type == "document" || type == "Audio" || type == "Video"))
            data.append("images[]", { type: file?.type, uri: file?.uri, name: file?.name });
        data.append("post_type", type)
        data.append("post_group_id", groupData.group_id)
        //  data.append("post_location", currentAddress)

        if (file != null && type == "Location") {
            data.append("post_latitude", file?.lat)
            data.append("post_longitude", file?.lng)
            data.append("post_location", file?.formatted_address)

        }
        if (file != null && type == "Event")
            data.append("post_event_id", file.event_id)


        var url = apiUrl.create_post
        await imageUploadApi(data, url, token).then(res => {
            setShowing(false)
            if (type == "Event")
                sendChatNotification("Message from " + groupData.group_name, type + " Share in " + groupData.group_name)
            else
                sendNotification("Message from " + groupData.group_name, type + " Share in " + groupData.group_name)
        })

    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    const tagViewClick = (item) => {
        if (tagClickIndex == -1) {
            setTagClickIndex(0)
            const index = chatData.findIndex(object => {
                return object.id === item.id;
            });
            let newArray = [...chatData]
            for (var i = 0; i < newArray.length; i++) {
                if (index == i)
                    newArray[i].isSelectTag = true
                else
                    newArray[i].isSelectTag = false
            }
            setChatData(newArray)
            setTimeout(() => {

                try {
                    flatList.scrollToIndex({ index: index })
                } catch (error) {

                }
            }, 500);



            setTimeout(() => {
                let newArray = [...chatData]
                for (var i = 0; i < newArray.length; i++) {
                    newArray[i].isSelectTag = false
                }
                setChatData(newArray)
                setTagClickIndex(-1)

            }, 1000);
        }

    }
    const tagViewItSelf = (tagData) => {
        var fileType = "";
        if (tagData.type == "file")
            fileType = get_url_extension(tagData.data.url)
        return (
            <TouchableOpacity onPress={() => tagViewClick(tagData)}
                style={{ marginHorizontal: 10, backgroundColor: Colors.progressBackground, marginTop: 5, flexDirection: "row", borderRadius: 10 }}>
                <View style={{ width: 0, backgroundColor: Colors.primaryViolet, padding: 4, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} />
                <View style={{ padding: 10, width: '100%' }}>
                    {tagData.type == "text" ?
                        <Text style={{ color: Colors.opacityColor3, }}>{tagData.text}</Text> :
                        (tagData.type == "file" && (fileType === "jpg" || fileType == "jpeg" || fileType == "png")) ?
                            imageTag(tagData, true)
                            :
                            tagData.type == "Location" ?
                                locationTag(tagData, "Location", Images.locationTab, true) :
                                tagData.type == "Event" ?
                                    locationTag(tagData, "Event", "", true) :
                                    locationTag(tagData, tagData.typeFile, Images.document, true)
                    }
                </View>
            </TouchableOpacity>
        )
    }
    const tagView = (tagData) => {
        var fileType = "";
        if (tagData.type == "file")
            fileType = get_url_extension(tagData.data.url)
        return (
            <View style={{ marginHorizontal: 20, backgroundColor: Colors.progressBackground, marginTop: 5, flexDirection: "row", borderRadius: 10 }}>
                <View style={{ width: 0, backgroundColor: Colors.primaryViolet, padding: 4, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} />
                <View style={{ padding: 10, width: '100%' }}>

                    {tagData.type == "text" ?
                        <>
                            <View style={{ justifyContent: "space-between", flexDirection: "row", flex: 1 }}>
                                <Text style={{ color: Colors.white }}>{tagData.sender.name}</Text>
                                <TouchableOpacity onPress={() => setReplayData(null)}>
                                    <Image style={{ height: 15, width: 15, tintColor: Colors.white, marginRight: 10 }} source={Images.closeIcon} />
                                </TouchableOpacity>
                            </View>
                            <Text style={{ color: Colors.opacityColor3, }}>{tagData.text}</Text>
                        </> :
                        (tagData.type == "file" && (fileType === "jpg" || fileType == "jpeg" || fileType == "png")) ?
                            imageTag(tagData, false)
                            :
                            tagData.type == "Location" ?
                                locationTag(tagData, "Location", Images.locationTab, false) :
                                tagData.type == "Event" ?
                                    locationTag(tagData, "Event", "") :
                                    locationTag(tagData, tagData.typeFile, Images.document, false)

                    }
                </View>
            </View>
        )
    }
    const locationTag = (tagData, type, src, flag) => {
        return (
            <View>
                <View style={{ justifyContent: "space-between", flexDirection: "row", flex: 1 }}>
                    <Text style={{ color: Colors.white }}>{tagData.sender.name}</Text>
                    {flag != true &&
                        <TouchableOpacity onPress={() => setReplayData(null)}>
                            <Image style={{ height: 15, width: 15, tintColor: Colors.white, marginRight: 10 }} source={Images.closeIcon} />
                        </TouchableOpacity>}
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    {type == "Event" ?
                        <EventIconUnselect width={15} height={15} /> :
                        <Image style={{ height: 12, width: 12, tintColor: Colors.white }} source={src} />
                    }
                    <Text style={{ color: Colors.opacityColor3, marginLeft: 5 }}>{type}</Text>
                </View>
            </View>

        )
    }
    const imageTag = (item, flag) => {
        return (
            <View style={{ justifyContent: "space-between", flexDirection: "row", alignItems: "center" }}>
                <View>
                    <Text style={{ color: Colors.white }}>{item.sender.name}</Text>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image style={{ height: 12, width: 12, tintColor: Colors.white }} source={Images.addPicture} />
                        <Text style={{ color: Colors.opacityColor3, marginLeft: 5 }}>{"Photo"}</Text>
                    </View>
                </View>
                <View >
                    <Image style={{ height: 40, width: 40, borderBottomRightRadius: 7, borderTopRightRadius: 7, marginRight: 10 }}
                        source={{ uri: item.data.url }} />
                    {flag != true &&
                        <TouchableOpacity
                            style={{
                                position: "absolute", height: 14, width: 14, right: 10,
                                backgroundColor: 'rgba(255,255,255,0.5)', borderRadius: 14,
                                alignItems: "center", justifyContent: "center"
                            }}
                            onPress={() => setReplayData(null)}>

                            <Image
                                style={{ height: 13, width: 13, tintColor: 'rgba(0,0,0,0.5)' }}
                                source={Images.closeIcon} />
                        </TouchableOpacity>}
                </View>
            </View>
        )
    }
    const clickItemUser = (item) => {
        setMessage(message + item.member.u_first_name)

        setVisibleModal(false)
    }
    return (
        <View style={styles.container}>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" && "padding"}
                style={styles.container}
            >

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={{ flex: 1 }}>
                    <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                    <>
                        <LinearGradient
                            colors={Colors.btnBackgroundColor}
                            style={{ height: Platform.OS == "ios" ? responsiveHeight(12.4) : responsiveHeight(10) }}
                        >
                            <MessageHeader
                                navigation={props.navigation}
                                title={groupData.group_name}
                                backButton={Images.backIcon}
                                callClick={callClick}
                                menuClick={menuClick}
                                uri={groupData.group_image}
                            />
                        </LinearGradient>
                        {modalNameChange()}
                        <FlatList
                            ref={ref => setFlatList(ref)}
                            onContentSizeChange={() => flatList.scrollToEnd({ animated: false })}
                            onLayout={() => flatList.scrollToEnd({ animated: false })}
                            data={chatData}
                            showsVerticalScrollIndicator={false}
                            style={{ marginBottom: responsiveHeight(7), marginTop: responsiveHeight(2) }}
                            renderItem={renderMessage}>

                        </FlatList>
                        <View style={[styles.bottomViewFooterTag, { marginBottom: Platform.OS == "ios" ? 10 : 0 }]}>
                            {replayData != null && tagView(replayData)}
                            <View style={styles.bottomViewFooter}>
                                <TouchableOpacity style={{ alignItems: "center", justifyContent: "center", paddingRight: 5, paddingVertical: 5 }} onPress={() => refSheet.open()}>
                                    <Text style={{ color: Colors.white, fontSize: 16, lineHeight: 18.23, marginHorizontal: 5 }}>+</Text>
                                </TouchableOpacity>
                                <TextInput
                                    placeholder={"Write Message"}
                                    placeholderTextColor={Colors.opacityColor}
                                    style={styles.txtInputStyle}
                                    value={message}
                                    multiline
                                    onChangeText={(text) => writeMessage(text)}
                                >
                                </TextInput>
                                <TouchableOpacity style={{ flex: 0.1, alignItems: "center", justifyContent: "center" }} onPress={() => onClickSend()}>
                                    <Text style={styles.txtPost}>{"Send"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>



                        <RBSheet
                            ref={(ref) => refSheet = ref}
                            height={296}
                            openDuration={250}
                            closeOnDragDown={true}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10,
                                }
                            }}
                        >
                            {moreMenu()}

                        </RBSheet>
                        <RBSheet
                            ref={(ref) => refSheetMenu = ref}
                            height={gm_role == "Admin" || groupData.group_privacy != "Private" ? 296 : 200}
                            openDuration={250}
                            closeOnDragDown={true}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10,
                                }
                            }}
                        >
                            <MoreMenu
                                data={gm_role == "Admin" || groupData.group_privacy != "Private" ? moreData : moreDataUser}
                                clickItem={clickItem}>
                            </MoreMenu>
                        </RBSheet>
                        <RBSheet
                            ref={ref => {
                                refSheetEvent = ref;
                            }}

                            openDuration={250}
                            closeOnDragDown={false}
                            customStyles={{
                                container: {
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10
                                }
                            }}
                        >
                            {EventList()}
                        </RBSheet>
                        <RBSheet
                            ref={ref => {
                                refSheetReaction = ref;
                            }}

                            openDuration={250}
                            closeOnDragDown={false}
                            customStyles={{
                                container: {

                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10
                                }
                            }}
                        >
                            {reactionList()}
                        </RBSheet>
                        <AudioRecord isVisible={isVisible} stopRecord={stopRecord}
                            closeRecord={closeRecord}
                        />


                    </>
                </LinearGradient>
                <LocationPicker
                    isVisible={isSerachMap}
                    saveLocation={saveLocation}
                    closeModal={closeModal}
                />
            </KeyboardAvoidingView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisibleCall}
                    onMessageNotification={() => { }}

                />
            }

            <UserListModal
                isVisible={isVisibleModal}
                clickItem={clickItemUser}
                data={memberList}
                onRequestClose={() => setVisibleModal(false)}
                user_id={u_id}
            />
        </View>
    )
}
export default ChatMessage;