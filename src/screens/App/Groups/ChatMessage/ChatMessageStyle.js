import React from 'react';
import { StyleSheet, Dimensions, Platform } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../../Resources/Colors";
import { Fonts } from '../../../../Resources/Fonts';
const { width, height } = Dimensions.get("window")

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    borderView: {
        height: 68, width: 68,
        borderRadius: 40, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white
    },
    cameraStyle: {
        height: 20, width: 20, position: "absolute",
        tintColor: Colors.white, alignSelf: "center", resizeMode: "contain"
    },
    labelText: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white,
        alignSelf: "center", marginVertical: responsiveHeight(1),
        fontFamily: Fonts.DMSansRegular,
    },
    bottomViewFooter: {
        backgroundColor: Colors.theme, flexDirection: "row", width: '100%', alignItems: "center", justifyContent: "center",
        flex:1
    },
    txtInputStyle: {
        flex: 0.78, color: Colors.white, fontFamily: Fonts.DMSansRegular,
     alignSelf:"center"
        
    },
    txtPost: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.primaryViolet, fontFamily: Fonts.DMSansRegular,
        

    },
    //message render simple serach

    txtDateHead: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        letterSpacing: 0.1, color: Colors.white, opacity: 0.6,
        alignSelf: "center", fontFamily: Fonts.DMSansRegular,
    },
    txtName: {
        fontSize: 10, fontWeight: "400",
        lineHeight: 13.02, color: Colors.white, opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
        marginLeft: responsiveWidth(10),
    },
    txtMessage: {
        fontSize: 14, fontWeight: "400",
        lineHeight: 18.23, color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        padding: 10,

    },
    txtTime: {
        fontWeight: "400", fontSize: 8, alignSelf: "flex-end",
        lineHeight: 10.42, opacity: 0.6, color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        padding: 2

    },
    txtLike: {
        fontWeight: "400", fontSize: 8,
        lineHeight: 14, opacity: 0.6, color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        marginRight: 5
    },
    likeView: {
         borderRadius: 10, backgroundColor: '#302645',
        position: "absolute", bottom: -16, left: 10,
        borderWidth: 1.25, borderColor: '#0C0024', flexDirection: 'row',
        alignItems: "center", justifyContent: "center",
    },
    imgHerat: {  padding: 5,fontSize:13 },
    messageView: {
        backgroundColor: Colors.opacityColor, alignItems: "center",
        justifyContent: "center", borderRadius: 6
    },
    imgProfile: { height: 24, width: 24, borderRadius: 12, alignSelf: "center", marginRight: 10 },

    //render Event box
    renderStyle: { height: 180, margin: responsiveHeight(1), borderRadius: 4, },
    imgStyle: { height: 180, width: responsiveWidth(68), borderRadius: 4 },
    upperView: { position: "absolute", margin: 10, width: "95%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    peopleImg: { height: 24, width: 24, borderRadius: 12 },
    peopleImg1: { height: 24, width: 24, borderRadius: 12, marginLeft: -8 },
    moreView: { height: 24, width: 24, marginLeft: -8, alignItems: "center", justifyContent: "center" },
    moreBorderView: { backgroundColor: Colors.black, borderRadius: 15, opacity: 0.6, width: 24, height: 24, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700", fontSize: 8,
        lineHeight: 11.72, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    dateView: { height: 31, width: 30, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2, marginRight: 5 },
    txtDate: {
        fontSize: 12, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 8, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 0, margin: 10 },
    txtTitle: {
        fontWeight: "400", fontSize: 18, lineHeight: 27.5, color: Colors.white,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.4
    },
    bottomBorderView: { height: 1, backgroundColor: Colors.white, opacity: 0.2, marginVertical: 3 },
    placeIcon: { height: 13, width: 12, opacity: 0.7, tintColor: Colors.white },
    txtAddress: {
        fontWeight: "400", fontSize: 10, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 10, fontFamily: Fonts.DMSansRegular
    },
    imgProfileEvent: { height: 24, width: 24, borderRadius: 12, alignSelf: "flex-start", marginRight: 10 },

    //chart view
    viewBox1: { backgroundColor: '#FFB91B', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5 },
    txtMiles: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, color: Colors.white },
    txtLabel: { fontSize: 10, fontWeight: "400", lineHeight: 14, color: Colors.white, opacity: 0.6 },
    viewBox2: { backgroundColor: '#FF2D65', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5 },
    viewBox3: { backgroundColor: '#00CDF9', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5 },
    chartView: {
        flexDirection: "row", justifyContent: "space-between",
        padding: 10, margin: 7, backgroundColor: "#0C0024", borderRadius: 4
    },
    txtViewLocation: {
        fontWeight: "500", fontSize: 14,
        lineHeight: 18.23, color: Colors.primaryViolet, alignSelf: "center",
        marginBottom: 5,
    },
    //modalview
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 210,
        padding: 20,
        borderRadius: 4,
        marginTop: responsiveHeight(30),
        width: '92%'
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,

    },
    input: {
        borderWidth: 0,
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        width: width - 50,
        height: 60
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular,
    },
    txtRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 16, lineHeight: 22, textAlign: "center", color: Colors.white,
        marginTop: 5
    },
    txtLabelRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "400",
        fontSize: 14, lineHeight: 22, textAlign: "center", color: Colors.white,
        opacity: 0.6
    },
    //event view render 
    eventListContainer: {
        paddingHorizontal: 10
    },
    createEventContainer: {
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center'
    },
    createEventImage: {
        backgroundColor: '#FFFFFF26',
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 60,
        marginRight: 16,

    },
    createEvent: {
        color: Colors.primaryViolet,
        fontSize: 14,
        fontWeight: '400'
    },
    smallIcon: {
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        position: 'absolute',
        top: 30,
        left: 32
    },
    smallIcon2: {
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        position: 'absolute',
        top: 30,
        left: 32
    },
    eventImage: {
        width: 48,
        height: 48,
        borderRadius: 48
    },
    smallEventIcon: {
        width: 10,
        height: 10,
        tintColor: Colors.white,
    },
    eventTitle: {
        fontWeight: '400',
        fontSize: 14,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    eventTime: {
        fontWeight: '400',
        fontSize: 12,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular

    },
    eventItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width * 0.7
    },
    radioBtn: {
        borderWidth: 1,
        width: 20,
        height: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioBtnSelect: {
        width: 10,
        height: 10,
        backgroundColor: Colors.primaryViolet,
        borderRadius: 10
    },
    ImagePost: {
        alignItems: 'flex-end',
        paddingRight: 20,
        marginTop: 20,
    },
    //event header
    textInputSearch: {
        width: width * 0.95,
        height: 40,
        backgroundColor: '#FFFFFF26',
        borderRadius: 5,
        paddingHorizontal: 5,
        color: Colors.white,
        opacity: 0.4,
        fontSize: 14,
        flexDirection: 'row',
        alignItems: 'center',
        fontFamily: Fonts.DMSansRegular

    },
    serachIcon: {
        width: 15,
        height: 15,
        tintColor: Colors.white,
        marginHorizontal: 10
    },
    searchBox: {
        marginTop: 5,
        width: width * 0.85,
        height: 40,
        alignItems: 'center',
        color: Colors.white,
        opacity: 0.4,
        fontSize: 12,
        fontFamily: Fonts.DMSansRegular

    },
    drawer: {
        borderWidth: 2,
        width: 50,
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 15,
        borderColor: '#FFFFFF26'
    },

    //event footer 
    verifyBtnGroup: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "100%", alignSelf: "center" },
    //location view
    imgMap: {
        width: responsiveWidth(71.5),
        height: 200,
        borderRadius: 4,
        marginHorizontal:8
    },
    //calling bubble view
    leftVieW:{
        alignSelf:"flex-start",
        padding:20,
        borderRadius:4,
        paddingHorizontal:10,
        paddingVertical:10,
        marginLeft:5

    },
    rightView:{
        alignSelf:"flex-end",
        backgroundColor:"blue",
        paddingHorizontal:10,
        paddingVertical:10,
        borderRadius:4,
        marginRight:10
    },
    //image view chat
    txtName: {
        fontSize: 10, fontWeight: "400",
        lineHeight: 13.02, color: Colors.white, opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
        marginLeft: responsiveWidth(10),
    },

    //tag message view
    bottomViewFooterTag: {
        position: "absolute", bottom: 0,
         backgroundColor: Colors.theme,  width: '100%', 
         flex:1,   paddingVertical:Platform.OS=="ios"?10:0,
     },
     txtReaction:{
        fontSize: 20, fontWeight: "400",
         color: Colors.white, 
        fontFamily: Fonts.DMSansBold,
        marginTop:15,
        marginBottom:10
     }
})