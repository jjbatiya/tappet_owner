import React from 'react';
import { StyleSheet } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../../Resources/Colors";
import { Fonts } from '../../../../Resources/Fonts';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    txtlblStep: {
        alignSelf: "center", fontSize: 12,
        lineHeight: 15.62, fontWeight: "400", color: "white",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6,
        marginRight: 30

    },
    mainView: {
        marginHorizontal: 20, marginTop: 10, marginBottom: 10, width: "100%", flexDirection: "row", height: 1
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center"
    },

    roundView: {
        height: 195, width: 195, borderWidth: 1, borderColor: Colors.opacityColor2,
        alignSelf: "center",
        borderRadius: 114, marginTop: responsiveHeight(2),
        justifyContent: "center",
    },
    imgUploadStyle: {
        height: 33.33, width: 33.33,
        alignSelf: "center", borderRadius: 7
    },
    profileStyle: {
        height: 195, width: 195,
        alignSelf: "center", borderRadius: 7,
        borderRadius: 114
    },
    txtUploadText: {
        color: Colors.white, alignSelf: "center",
        lineHeight: 18.23, fontSize: 14, fontWeight: "400",
        marginTop: responsiveHeight(1), fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    editView: {
        width: 40, height: 40, backgroundColor: Colors.dialogBackground,
        position: 'relative', bottom: 50, left: 150,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: '#0C0024'
        ,
    },
    imgEditStyle: { height: 12, width: 12, tintColor: "white", alignSelf: "center" },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -10,
        fontFamily: Fonts.DMSansRegular

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(4),
        marginTop: responsiveHeight(3),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginHorizontal: responsiveWidth(4),
        marginTop: responsiveHeight(3),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        marginLeft:-9
    },

    //render Title
    titleText: {
        fontSize: 14, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular, textTransform: "uppercase"
    },
    //render sub view
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        width:150
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular,
        width:150
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 96,
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnView: { alignItems: "center", justifyContent: "center" },
    renderSubView: { height: 48, marginVertical: 15, flexDirection: "row", alignItems: "center", justifyContent: 'space-between' },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 48, width: 48,borderRadius:24 },
    borderStyle: { height: 28, width: 96, borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white },
    btnText1: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        position: "absolute",
        opacity: 0.6
    },
})