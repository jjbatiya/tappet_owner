import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, Image, TouchableOpacity, FlatList, Share, ScrollView } from 'react-native';
import styles from './CreateGroupStyle';
import LinearGradient from 'react-native-linear-gradient';
import * as Constant from '../../../../utils/Constant';
import HeaderView from '../../../../Components/HeaderView';
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images';
import {
    launchImageLibrary
} from 'react-native-image-picker';
import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
var FloatingLabel = require('react-native-floating-labels');
import { UploadImageIcon } from '../../../../utils/svg/UploadImageIcon';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { apiCallGetWithToken, apiCallWithToken, getUserData, imageUploadApi } from '../../../../utils/helper';
import Loader from '../../../../Components/Loader';

import Toast from 'react-native-simple-toast';
import { CometChat } from "@cometchat-pro/react-native-chat"

const AddMember = (props) => {
    var onEndReached;
    const [groupData, setGroupData] = useState(props.route?.params?.groupData != undefined ? props.route.params.groupData : null)
    const [imagePath, setImagePath] = useState("")
    const [isGroupName, setGroupName] = useState(false)
    const [imageData, setImageData] = useState(props.route?.params?.groupData != undefined ? { type: "already" } : null)
    const [step, setStep] = useState(1)
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [groupName, setGroupValue] = useState("")
    const [groupDescription, setGroupDescription] = useState("")
    const [isGroupDescription, setIsGroupDescription] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(10)
    const [memberList, setMemberList] = useState([])
    useEffect(async () => {
        getUserData().then(res => {
            setToken(res.token)
            setShowing(true)
            getMemberData(res.token, "")
        })
        if (groupData != null) {
            setGroupDescription(groupData.group_description)
            setGroupValue(groupData.group_name)
            setImagePath(groupData.group_image)
        }
    }, []);

    const setMemberData = async (array) => {
        var ids = await array.reduce((ids, item) => {
            if (item.is_group_member) {
                ids.push(item.u_id);
            }
            return ids;
        }, []);
        setMemberList(ids)
    }
    const getMemberData = async (token, serachText) => {
        setFriendData([])
        await apiCallGetWithToken(apiUrl.explore_people + "?page=" + page + "&limit=" + limit + "&search=" + serachText +
            "&group_id=" + groupData?.group_id, token).then(res => {
                if (res.status == true) {
                    setShowing(false)
                    setTotalPage(res.pagination.lastPage)
                    //                    setMemberData(res.result);
                    if (page == 1) {
                        categoryWiseData(res.result)
                    }
                    else {
                        let result1 = friendsData.map(a => a.data);
                        result1 = result1.concat(res.result)
                        categoryWiseData(result1)
                    }
                }
                else {
                    Toast.show(res.message, Toast.SHORT);
                    setTimeout(() => {
                        setShowing(false)
                        setFriendData([])
                    }, 1000);
                }


            })

    }
    const removeGroupMember = async (id) => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: id,
        }
        await apiCallWithToken(obj, apiUrl.remove_group_member, token).then(res => {
            if (res.status == true) {
                Toast.show(res.message, Toast.SHORT);
                setTimeout(() => {
                    getMemberData(token, "")
                }, 1000);
            }
            else {
                Toast.show(res.message, Toast.SHORT);
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const addGroupMember = async (id) => {
        let data = new FormData();
        setShowing(true)
        data.append("group_id", groupData.group_id)
        data.append("group_members[]", id)

        await imageUploadApi(data, apiUrl.add_group_member, token).then(res => {
            if (res.status == true) {
                Toast.show(res.message, Toast.SHORT);
                setTimeout(() => {
                    getMemberData(token, "")
                    addGroupMembers(id)
                }, 1000);
            }
            else {
                Toast.show(res.message, Toast.SHORT);
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const addGroupMembers = (uid) => {
        let GUID = Constant.comeGroupTag + groupData.group_id;
        let membersList = [];
        membersList.push(new CometChat.GroupMember(Constant.comeChatUserTag + uid, CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT))

        CometChat.addMembersToGroup(GUID, membersList, []).then(
            response => {
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            },
            error => {
                console.log("Something went wrong", error);
            }
        );
    }
    const categoryWiseData = (data) => {
        let newArray = [];
        for (var i = 1; i < 3; i++) {
            var obj = {
                title: i == 1 ? "All friends" : "Others",
                id: i,
                data: data.filter((item) => filterData(item, i))
            }
            newArray.push(obj)
        }
        setFriendData(newArray)
    }
    function filterData(data, i) {
        if (i == 1)
            return data.friend_request == 1;
        else
            return data.friend_request != 1;

    }

    const backClick = () => {
        if (step == 1)
            props.navigation.goBack(null);
        else
            setStep(step - 1)
    }
    const onSave = () => {
        props.navigation.goBack(null)
    }


    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10 }}>
                {item.data.length != 0 && <Text style={styles.titleText}>
                    {item.title}
                </Text>}
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const itemClick = (item, index) => {
        const parentIndex = item.friend_request == 1 ? 0 : 1;
        let newArray = [...friendsData];
        if (newArray[parentIndex].data[index].is_group_member == false) {
            newArray[parentIndex].data[index].is_group_member = true
            addGroupMember(item.u_id)
        }
    }
    const renderSubView = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pet"}</Text>
                    </View>
                </View>
                {item.is_group_member == false ?
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => itemClick(item, index)}
                        //  text={item.isShare == "1" ? strings.shareInvite : strings.addMembers}
                        text={strings.addMembers}
                    /> :
                    <TouchableOpacity onPress={() => itemClick(item, index)} style={{ alignItems: "center", justifyContent: "center" }} >
                        <View style={styles.borderStyle}></View>
                        <Text style={styles.btnText1}>{strings.remove}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
    const onSubmitEditing = async (text) => {
        setShowing(true)
        getMemberData(token, text)
    }
    const renderHeader = () => {
        return (
            <SimpleSerach
                onChangeText={(text) => console.log(text)}
                placeHolder={strings.serachPlaceholderBottom}
                isSimpleSerach={true}
                editable={true}
                isScreen={true}
                onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onSubmitEditing(text)}

            />
        )
    }
    const step2View = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(4) }}>

                <FlatList
                    data={friendsData}
                    style={{ marginBottom: responsiveHeight(10) }}
                    renderItem={renderItem}
                    ListHeaderComponent={renderHeader}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.1}
                    onMomentumScrollBegin={() => { onEndReached = false; }}
                    onEndReached={() => {
                        if (!onEndReached) {
                            handleMoreData(); // on End reached
                            onEndReached = true;
                        }
                    }}

                />
            </View>
        )
    }

    const handleMoreData = () => {
        if (totalPage > page) {
            setPage(page + 1)
            setShowing(true)
            getMemberData(token, "")
        }

    }
    return (

        <View style={{ flex: 1 }} >
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >

                <CommonStatusBar />
                <SafeAreaView style={{ flex: 1, marginTop: responsiveHeight(5) }}>
                    <HeaderView
                        onSkip={onSave}
                        isPhoto={true}
                        onPress={backClick}
                        text={strings.addMembers}
                        textSkip={strings.save} />
                    <View style={styles.mainView}>
                        <LinearGradient
                            colors={Constant.ColorTheme.btnBackgroundColor}
                            style={{ height: 4, flex: 0.91 }}
                        />

                    </View>
                    {step2View()}
                </SafeAreaView>
            </LinearGradient>
        </View>


    )
}
export default AddMember;