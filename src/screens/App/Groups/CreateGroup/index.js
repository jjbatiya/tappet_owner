import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, Image, TouchableOpacity, FlatList, Share, ScrollView, KeyboardAvoidingView, Platform, Dimensions } from 'react-native';
import styles from './CreateGroupStyle';
import LinearGradient from 'react-native-linear-gradient';
import * as Constant from '../../../../utils/Constant';
import HeaderView from '../../../../Components/HeaderView';
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images';

import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
var FloatingLabel = require('react-native-floating-labels');
import { UploadImageIcon } from '../../../../utils/svg/UploadImageIcon';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { apiCallGetWithToken, apiCallWithToken, getUserData, imageUploadApi } from '../../../../utils/helper';
import Loader from '../../../../Components/Loader';

import Toast from 'react-native-toast-message';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { validationMsg } from '../../../../Resources/ValidationMsg';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import ImagePicker from 'react-native-image-crop-picker';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';
const { width ,height} = Dimensions.get('window');

const CreateGroup = (props) => {
    var onEndReached;
    const [groupData, setGroupData] = useState(props.route?.params?.groupData != undefined ? props.route.params.groupData : null)
    const [imagePath, setImagePath] = useState("")
    const [isGroupName, setGroupName] = useState(false)
    const [imageData, setImageData] = useState(props.route?.params?.groupData != undefined ? { type: "already" } : null)
    const [step, setStep] = useState(1)
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [groupName, setGroupValue] = useState("")
    const [groupDescription, setGroupDescription] = useState("")
    const [isGroupDescription, setIsGroupDescription] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(10)
    const [memberList, setMemberList] = useState([])
    const [allData, setAllData] = useState([])
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    useEffect(async () => {
        getUserData().then(res => {
            setToken(res.token)
            setShowing(true)
            getMemberData(res.token, "", page)
        })
        if (groupData != null) {
            setGroupDescription(groupData.group_description)
            setGroupValue(groupData.group_name)
            setImagePath(groupData.group_image)

        }
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const setMemberData = async (array) => {
        var ids = await array.reduce((ids, item) => {
            if (item.is_group_member) {
                ids.push(item.u_id);
            }
            return ids;
        }, []);
        setMemberList(ids)
    }
    const getMemberData = async (token, serachText, page) => {
        await apiCallGetWithToken(apiUrl.explore_people + "?page=" + page + "&limit=" + limit + "&search=" + serachText +
            "&group_id=" + groupData?.group_id, token).then(res => {
                if (res.status == true) {
                    setShowing(false)
                    setTotalPage(res.pagination.lastPage)
                    setMemberData(res.result);
                    if (page == 1) {
                        categoryWiseData(res.result)
                        setAllData(res.result)
                    }
                    else {
                        let result1 = allData.concat(res.result)
                        categoryWiseData(result1)
                        setAllData(result1)

                    }
                }
                else {
                    showMessage(strings.error, strings.msgTag, res.message)
                    setTimeout(() => {
                        setShowing(false)
                        setFriendData([])
                    }, 1000);
                }


            })

    }
    const removeGroupMember = async (id) => {
        setShowing(true)
        var obj = {
            group_id: groupData.group_id,
            user_id: id,
        }
        await apiCallWithToken(obj, apiUrl.remove_group_member, token).then(res => {
            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    getMemberData(token, "", 1)
                    removeChatGroupMember(id, groupData.group_id)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const addGroupMember = async (id) => {
        let data = new FormData();
        setShowing(true)
        data.append("group_id", groupData.group_id)
        data.append("group_members[]", id)

        await imageUploadApi(data, apiUrl.add_group_member, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    getMemberData(token, "", 1)
                    addGroupMembers(Constant.comeGroupTag + groupData.group_id)
                }, 1000);
            }
            else {
                showMessage("error", "Message", res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const categoryWiseData = (data) => {
        let newArray = [];
        for (var i = 1; i < 3; i++) {
            var obj = {
                title: i == 1 ? "All friends" : "Others",
                id: i,
                data: data.filter((item) => filterData(item, i))
            }
            newArray.push(obj)
        }
        setFriendData(newArray)
    }
    function filterData(data, i) {
        if (i == 1)
            return data.friend_request == 1;
        else
            return data.friend_request != 1;

    }
    const onBlur = (flag) => {
        if (flag == "name")
            setGroupName(!isGroupName)
        else
            setIsGroupDescription(!isGroupDescription)
    }
    const backClick = () => {
        if (step == 1)
            props.navigation.goBack(null);
        else
            setStep(step - 1)
    }
    const onSave = () => {
        //     if (imagePath.trim() == "") {
        //         showMessage("error", "Message", "Please Select Group Image!")
        //         return
        //     }
        if (groupName.trim() == "") {
            showMessage("error", "Message", validationMsg.groupNameValidation)
            return
        }
        // else if (groupDescription.trim() == "") {
        //     showMessage("error", "Message", "Group Description is required!")
        //     return
        // }
        else if (memberList.length == 0 && step == 2) {
            showMessage(strings.error, strings.msgTag, validationMsg.groupMemberValidation)
            return
        }
        if (step != 2) {
            setStep(step + 1)

        }
        else {
            imageUpload()
        }
    }
    const chooseFile = () => {
      
        ImagePicker.openPicker({
            compressImageQuality:0.7,
            cropping: true,
            compressImageMaxHeight:height,
            compressImageMaxWidth:width
          }).then(image => {
            try{
                var uri=image?.path;
                var fileName= uri.substr(uri.lastIndexOf("/")+1)

                var obj={
                    type:image?.mime,
                    uri:image?.path,
                    fileName:fileName
                }
                setImageData(obj)
                setImagePath(image?.path)
              }catch(error){

              }
          });
    };
    const imageUpload = async () => {
        let data = new FormData();
        if (imageData != null) {
            if (imageData?.type != "already")
                data.append("group_image", { type: imageData?.type, uri: imageData?.uri, name: imageData?.fileName });
        }
        data.append("group_name", groupName)
        data.append("group_description", groupDescription)
        setShowing(true)
        var url = apiUrl.create_group
        if (groupData != null) {
            data.append("group_id", groupData.group_id)
            url = apiUrl.edit_group
        }
        for (var i = 0; i < memberList.length; i++)
            data.append("group_members[]", memberList[i])
        await imageUploadApi(data, url, token).then(res => {
            if (res.status) {
                setTimeout(() => {
                    createGroup(groupData != null ? "update" : "create", res);
                }, 1000);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const createGroup = (flag, res) => {
        var group_id = "";
        if (groupData != null)
            group_id = groupData.group_id;
        else
            group_id = res.result.group_id;
        var GUID = Constant.comeGroupTag + group_id;
        var groupNameValue = groupData != null ? groupData.group_name : groupName;
        var groupType = CometChat.GROUP_TYPE.PUBLIC;
        var password = "";
        var group = null;
        if (flag == "create") {
            group = new CometChat.Group(GUID, groupNameValue, groupType, password);
            CometChat.createGroup(group).then(
                group => {
                    addGroupMembers(GUID);
                },
                error => {
                    setShowing(false)
                    showMessage(strings.error, strings.msgTag, error)
                }
            );
        }
        else {
            group = new CometChat.Group(GUID, groupNameValue, groupType);
            CometChat.updateGroup(group).then(
                group => {
                    console.log("Groups details updated successfully:", group);
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.TabNavigation }],
                    });
                },
                error => {
                    console.log("Group details update failed with exception:", error);
                }
            );
        }
    }
    const addGroupMembers = (group_uuid) => {
        let GUID = group_uuid;
        let membersList = [];
        for (var i = 0; i < memberList.length; i++) {
            membersList.push(new CometChat.GroupMember(Constant.comeChatUserTag + memberList[i], CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT))
        }
        CometChat.addMembersToGroup(GUID, membersList, []).then(
            response => {
                setShowing(false)
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            },
            error => {
                setShowing(false)
                showMessage(strings.error, strings.msgTag, error)
            }
        );
    }
    const removeChatGroupMember = (id) => {
        let GUID = Constant.comeGroupTag + id;
        var UID = Constant.comeChatUserTag + id;

        CometChat.kickGroupMember(GUID, UID).then(
            response => {
                console.log("Group member kicked successfully", response);
            },
            error => {
                console.log("Group member kicking failed with error", error);
            }
        );
    }
    const step1View = () => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.roundView}>
                    {imagePath == "" ?
                        <TouchableOpacity onPress={() => chooseFile()}>
                            <View style={{ alignSelf: "center" }}>
                                <UploadImageIcon opacity={0.6} />
                            </View>
                            <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                        </TouchableOpacity> :
                        <View style={{ flex: 1 }}>
                            <Image style={styles.profileStyle} source={{ uri: imagePath }}></Image>
                            <TouchableOpacity style={styles.editView} onPress={() => chooseFile()}>
                                <Image
                                    style={styles.imgEditStyle}
                                    source={Images.editIcon}
                                />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isGroupName ? styles.focusforminput : styles.formInput}
                    value={groupName}
                    onChangeText={(text) => setGroupValue(text)}
                    onFocus={() => onBlur("name")}
                    onBlur={() => onBlur("name")}
                >{strings.groupTitle}</FloatingLabel>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isGroupDescription ? styles.focusforminput : styles.formInput}
                    value={groupDescription}
                    onChangeText={(text) => setGroupDescription(text)}
                    onFocus={() => onBlur("desc")}
                    onBlur={() => onBlur("desc")}
                >{strings.groupDescription}</FloatingLabel>
            </ScrollView>
        )
    }
    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10 }}>
                {item.data.length != 0 && <Text style={styles.titleText}>
                    {item.title}
                </Text>}
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const itemClick = (item, index) => {
        const parentIndex = item.friend_request == 1 ? 0 : 1;
        let newArray = [...friendsData];
        newArray[parentIndex].data[index].is_group_member = !item.is_group_member
        setFriendData(newArray)
        var index = memberList.indexOf(item.u_id)
        if (index !== -1) {
            if (groupData != null)
                removeGroupMember(item.u_id)
            memberList.splice(index, 1);
        }
        else {
            if (groupData != null)
                addGroupMember(item.u_id)
            memberList.push(item.u_id)

        }


    }
    const renderSubView = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pet"}</Text>
                    </View>
                </View>
                {item.is_group_member == false ?
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => itemClick(item, index)}
                        text={strings.addMembers}
                    /> :
                    <TouchableOpacity onPress={() => itemClick(item, index)} style={{ alignItems: "center", justifyContent: "center" }} >
                        <View style={styles.borderStyle}></View>
                        <Text style={styles.btnText1}>{strings.remove}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
    const onSubmitEditing = async (text) => {
        await setPage(1)

        setShowing(true)
        getMemberData(token, text, 1)
    }
    const renderHeader = () => {
        return (
            <SimpleSerach
                onChangeText={(text) => console.log(text)}
                placeHolder={strings.serachPlaceholderBottom}
                isSimpleSerach={true}
                editable={true}
                isScreen={true}
                onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onSubmitEditing(text)}

            />
        )
    }
    const step2View = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(4) }}>

                <FlatList
                    data={friendsData}
                    style={{ marginBottom: responsiveHeight(10) }}
                    renderItem={renderItem}
                    ListHeaderComponent={renderHeader}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.1}
                    onMomentumScrollBegin={() => { onEndReached = false; }}
                    onEndReached={() => {
                        if (!onEndReached) {
                            handleMoreData(); // on End reached
                            onEndReached = true;
                        }
                    }}

                />
            </View>
        )
    }

    const handleMoreData = () => {
        if (totalPage > page) {
            setPage(page + 1)
            setShowing(true)
            getMemberData(token, "", (page + 1))
        }

    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (

        <View style={{ flex: 1 }} >
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" && "padding"}
                style={styles.container}
            >
                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.container}
                >
                    <CommonStatusBar />
                    <SafeAreaView style={{ flex: 1, marginTop: responsiveHeight(5) }}>
                        <HeaderView onSkip={onSave} isPhoto={true} onPress={backClick}
                            text={step == "1" ? strings.createGroup : strings.addMembers} textSkip={step == "1" ? strings.next : strings.create} />
                        <Text style={styles.txtlblStep}>{step == "1" ? strings.stepStr1 : strings.stepStr2}</Text>
                        <View style={styles.mainView}>
                            <LinearGradient
                                colors={Constant.ColorTheme.btnBackgroundColor}
                                style={{ height: 4, flex: step == "1" ? 0.3 : 0.91 }}
                            />
                            {step == "1" ? <View style={{ backgroundColor: "white", height: 4, flex: 0.6 }} /> : <></>}

                        </View>
                        {step == "1" ? step1View() : step2View()}
                    </SafeAreaView>
                </LinearGradient>
            </KeyboardAvoidingView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
             {isVisibleCall==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisibleCall}
                        onMessageNotification={()=>{}}

                    />
                }
        </View>
    )
}
export default CreateGroup;