import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, FlatList, Image, ScrollView, TextInput, Dimensions, Linking, Platform } from 'react-native';
import styles from './HomeStyle';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import { Colors } from '../../../Resources/Colors'
import LinearGradient from 'react-native-linear-gradient';
import { EditIcon } from '../../../utils/svg/EditIcon';
import { CommentIcon } from '../../../utils/svg/CommentIcon';
import NatureFun from '../../../utils/svg/NatureFun';
import { strings } from '../../../Resources/Strings';
import { apiCallGetWithToken, apiCallWithToken, comeChatLogin, getCallNotificationData, getCallUserNotificationData, getChatNotificationFlag, getCountUnreadMessage, getDeviceModal, getDeviceUid, getFirebaseToken, getIsNotification, getMessageNotification, getNotificationCount, getUserData, imageUploadApi, storeCallNotification, storeIsNotification, storeMessageNotification, storeserCallNotification } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import Loader from '../../../Components/Loader';
import { PlaceIconUnselect } from '../../../utils/svg/PlaceIconUnselect';
import EventMap from '../../../Components/EventMap';
import moment from 'moment';
const { width, height } = Dimensions.get("window")
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../Components/MoreMenu';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { comeChatUserTag, comeGroupTag } from '../../../utils/Constant';
import DeviceInfo from 'react-native-device-info';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { toastConfig } from '../../../utils/ToastConfig';
import NotificationController from '../../../Components/NotificationController';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import Sound from 'react-native-sound';
import { outgoingCallAlert } from '../../../assets/audio';

const moreData = [

    {
        key: "Edit",
        title: "Edit",
        image: Images.editIcon
    },
    {
        key: "delete",
        title: "Delete post",
        image: Images.deleteIcon
    },
]
const HomeScreen = (props) => {

    const [groupDatasource, setGroupDatasource] = useState([]);
    const [isLowBattery, setLowBattery] = useState(false)
    const [isProgress, setIsProgress] = useState(false)
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [postData, setPostData] = useState([])
    const [limit, setLimit] = useState(10)
    const [u_image, setUImage] = useState("")
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    var onEndReached;
    let refSheet = useRef();
    const [seletedPost, setSelectedPost] = useState()
    const [isVisible, setVisible] = useState(false)
    const [callData, setCallData] = useState(null)
    const [isNotification, setIsNotification] = useState(false)
    const [isMessageNotification, setMessageNotification] = useState(false)
    const [isFetching, setIsFetching] = useState(false)
    const[userData,setUserData]=useState(null)
    var outgoingAlert = new Sound(outgoingCallAlert);

    const checkEnableLocation = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
            interval: 10000,
            fastInterval: 5000,
        })
            .then((data) => {
                console.log(data)
            })
            .catch((err) => {

            });

    }
    useEffect(async () => {

        Platform.OS == "android" && checkEnableLocation()
        await getUserData().then(res => {
            setUserData(res)
            setShowing(true)
            setUserID(res.u_id)
            setToken(res.token)
            setUImage(res.u_image == "" ? "http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg" : res.u_image)
            getGroupData(res)
            comeChatLoginUser(res.u_id, res);
            getNotificationData(res.token)
            getChatNotificationFlag().then(res => {
                res.chat_notification_flag == true ? setMessageNotification(true) : setMessageNotification(false)
            })
            getCountUnreadMessage().then(res => {
                if (res.status == true) {
                    if (Object.keys(res.result.groups).length != 0 || Object.keys(res.result.users).length != 0) {
                        setMessageNotification(true)
                    }
                    else
                        setMessageNotification(false)
                }
            });
            props.navigation.addListener('focus', () => {
                getNotificationData(res.token)
                getGroupData(res)
                getChatNotificationFlag().then(res => {
                    res.chat_notification_flag == true ? setMessageNotification(true) : setMessageNotification(false)
                })
                getCountUnreadMessage().then(res => {
                    if (res.status == true) {
                        if (Object.keys(res.result.groups).length != 0 || Object.keys(res.result.users).length != 0) {
                            setMessageNotification(true)
                        }
                        else
                            setMessageNotification(false)
                    }
                });
            })
        })

        await getCallUserNotificationData().then(res => {
            if (res != undefined && res != "") {

                var obj = {
                    sessionId: res.data.entities.on.entity.sessionid,
                    callInitiator: {
                        name: res.data.entities.by.entity.name,
                        avatar: res.data.entities.by.entity.avatar
                    }
                }
                setCallData(obj)
                storeserCallNotification("")
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: obj, flag: "user", userData: userData })

                    setVisible(true)
                }, 1000);
            }



        })
        await getCallNotificationData().then(res => {
            if (res != undefined && res != "") {
                try {
                    if (Number.isInteger(parseInt(res.group_id))) {
                        storeCallNotification("")
                        props.navigation.navigate(strings.chatMessageScreen, { groupData: res })
                    }
                } catch (error) {

                }
            }
        })
        await getMessageNotification().then(res => {
            if (res != undefined && res != "") {
                try {
                    storeMessageNotification("")
                    var id = res.sender;
                    id = id.replace("comechatuser_", "")
                    props.navigation.navigate(strings.typeMessageScreen, { userData: { u_id: id, u_first_name: "", u_last_name: "" } })
                } catch (error) {

                }
            }

        })
        await getIsNotification().then(res => {
            if (res != undefined && res != "") {
                try {
                    storeIsNotification("")
                    props.navigation.navigate(strings.notificationScreen)

                } catch (error) {

                }
            }
        })

    }, []);
    const getNotificationData = async (token) => {
        var obj = {
            page: 1,
            limit: 20
        }
        await apiCallWithToken(obj, apiUrl.getnotifications, token).then(res => {
            setIsFetching(false)
            if (res.status == true) {
                var total = res.pagination.total;
                getNotificationCount().then(res => {
                    var read = res.notification_count;
                    if (total > read)
                        setIsNotification(true)
                    else {
                        setIsNotification(false)
                    }
                })
            }
        })
    }
    var listnerID = callData != null ? callData.sessionId : "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call
                setCallData(call)
                setTimeout(() => {
                    setVisible(true)
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: userData })
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {
                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                setTimeout(() => {
                    outgoingAlert.stop(()=>{
                        setTimeout(() => {
                            setVisible(false)
                            props.navigation.goBack(null);
                        }, 200);
                    })
                }, 1000);
               

               
                 // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setTimeout(() => {
                    outgoingAlert.stop(()=>{
                        setTimeout(() => {
                            setVisible(false)
                            props.navigation.goBack(null);
                        }, 200);
                    })
                }, 1000);
               

            }
        })
    );
  
    const comeChatLoginUser = async (user_id, data) => {
        await comeChatLogin(user_id).then(res => {
            updateuserdetail(data)
        })
        await getFirebaseToken().then(res => {
            CometChat.registerTokenForPushNotification(res).then(res => {
            });
        })

    }
    const updateuserdetail = async (data) => {
        let uid = comeChatUserTag + data.u_id;
        let avatar = data.u_image;

        var user = new CometChat.User(uid);

        user.setAvatar(avatar);
        CometChat.updateCurrentUserDetails(user).then(
            user => {
            }, error => {
            }
        )
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getAllPost = async (token, page) => {
        await apiCallGetWithToken(apiUrl.get_all_posts + "?page=" + page + "+&limit=" + limit, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    setPostData(res.result)
                }
                else {
                    setPostData(postData.concat(res.result))
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setShowing(false)
            }
        })
    }
    const groupArrayModified = async (data) => {
        let obj1 = {}
        await CometChat.getUnreadMessageCountForAllGroups().then(res => {
            obj1 = res;
        }, error => {
        })
        let newArray = [...data]
        for (var i = 0; i < newArray.length; i++) {

            var guid = comeGroupTag + newArray[i].group_id + "";

            newArray[i].count_unreadmessage = obj1[guid + ""] == undefined ? 0 : obj1[guid + ""]
        }
        newArray.sort((function (obj1, obj2) {
            // Ascending: first id less than the previous
            return obj2.count_unreadmessage - obj1.count_unreadmessage;
        }));
        setGroupDatasource(newArray)

    }
    const getGroupData = async (data) => {
        await apiCallGetWithToken(apiUrl.get_all_groups, data.token).then(res => {
            setIsFetching(false)

            updateDeviceDetail(data.token)
            if (res.status == true) {
                //setShowing(false)
                if (res.result.length != 0) {
                    groupArrayModified(res.result)
                }
                getAllPost(data.token, page);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    //  setShowing(false)
                    getAllPost(data.token, page);

                }, 1000);
            }
        })
    }
    const updateDeviceDetail = async (token) => {
        var device_model_name = getDeviceModal();
        var device_uid = getDeviceUid();
        var app_version = DeviceInfo.getVersion();
        var device_token = "", device_name = "";
        await getFirebaseToken().then(res => {
            device_token = res;
        })
        await DeviceInfo.getDeviceName().then((deviceName) => {
            device_name = deviceName;
        });
        var obj = {
            device_token: device_token,
            device_type: Platform.OS,
            device_uid: device_uid,
            device_name: device_name,
            device_model_name: device_model_name,
            device_os_version: Platform.Version,
            app_version: app_version,
            api_version: "v1"
        }
        await apiCallWithToken(obj, apiUrl.updatedevicetoken, token).then(res => {
        })
    }
    const renderHeader = () => {
        return (
            <FlatList
                data={groupDatasource.slice(0, 5)}
                keyExtractor={item => item.id}
                style={{ marginLeft: 8 }}
                initialNumToRender={5}
                renderItem={renderGroupItem}
                horizontal={true}
            />
        )
    }

    const renderGroupItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.chatMessageScreen, { groupData: item })}
                style={{ marginLeft: 12, marginRight: groupDatasource.length == 1 ? 12 : 0, marginBottom: 16, paddingTop: 20 }}>
                <Image source={{ uri: item.group_image }}
                    style={{ width: 130, height: 160, borderRadius: 4 }} />
                <LinearGradient colors={Colors.viewGroupContain} style={styles.groupItem}>
                    <Text style={styles.groupCommnet}>{item.group_name}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 8, alignSelf: "flex-start", marginBottom: item.group_last_two_members.length > 2 ? 8 : 30 }}>
                        {item.group_last_two_members.map((data, index) => (
                            index != 2 ?
                                <View style={[styles.groupProfile, { left: index != 0 ? 0 : 18 }]}>
                                    <Image source={{ uri: data.member?.u_image }}
                                        style={styles.profileImage}
                                    />
                                </View>
                                :
                                <View
                                    style={[styles.groupProfileContainer, { left: 36 }]}
                                >
                                    <Text style={styles.moreGroupProfile}>{"+" + parseInt(item.group_last_two_members.length - 2)}</Text>
                                </View>
                        ))}

                    </View>
                </LinearGradient>
                {item.count_unreadmessage != undefined && item.count_unreadmessage > 0 &&
                    <View style={{ height: 20, width: 20, borderRadius: 10, backgroundColor: Colors.redColor, position: "absolute", top: 15, right: -4 }}>
                    </View>
                }
            </TouchableOpacity>


        )
    }
    function getDifferenceInDays(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60 * 24);
    }
    function getDifferenceInHours(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60);
    }
    function getDifferenceInMinutes(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60);
    }

    function getDifferenceInSeconds(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / 1000;
    }
    const dateTimeGet = (date1, date2) => {
        const days = Math.round(getDifferenceInDays(date1, date2));
        const hours = Math.round(getDifferenceInHours(date1, date2));
        const minutes = Math.round(getDifferenceInMinutes(date1, date2));
        const second = Math.round(getDifferenceInSeconds(date1, date2));
        if (second < 60)
            return second + " Second ago"
        else if (minutes < 60)
            return minutes + " Minute ago"
        else if (hours < 24)
            return hours + " Hour ago"
        else
            return days + " Days ago"
    }
    const onAddComment = async (text, post_id) => {
        setShowing(true)
        var obj = {
            post_id: post_id,
            post_comment_text: text
        }
        await apiCallWithToken(obj, apiUrl.add_comment, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setTimeout(() => {
                    getAllPost(token, 1)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const onLike = async (post_id, isLike) => {
        setShowing(true)

        let data = new FormData();

        data.append("post_id", post_id)
        data.append("favourite", isLike ? 2 : 1)
        await imageUploadApi(data, apiUrl.like_or_unlike_post, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setTimeout(() => {
                    getAllPost(token, 1)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const moreMenuClick = (item) => {
        setSelectedPost(item)
        refSheet.open();
    }
    const convertUTCToLocalTime = (date) => {
        const milliseconds = Date.UTC(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
        );
        const localTime = new Date(milliseconds);
        return localTime;
    };
    const returnData = (data) => {
        getAllPost(token, 1)

    }
    function getMultiplePostTag(item) {
        let tagArray = []
        if (item.post_event != undefined && item.post_event != null && isEmpty(item.post_event) == false)
            tagArray.push("Event")
        if (item.post_latitude != undefined && item.post_latitude != null && item.post_latitude != "")
            tagArray.push(" Location")
        if (item.post_images.length > 0)
            tagArray.push(" Photo")

        return tagArray.toString()
    }
    const checkOnlyText = (item) => {
        if (item.post_images.length == 0 && item.post_latitude == null && isEmpty(item.post_event) == true && item.post_type == "") {
            return true
        }
        else false
    }
    const renderItem = ({ item, index }) => {
        const now = new Date()
        const date1 = new Date(item.post_created_at.replace(/-/g, "/"));
        var newDate = convertUTCToLocalTime(date1);
        const date2 = new Date();
        const time = dateTimeGet(date2, newDate);
        var isTextOnly = checkOnlyText(item);
        var postType = item.post_type == "Event" ? "Play date" : item.post_type

        // var multiplePostTag = ""
        // if (item.post_type == "") {
        //     getMultiplePostTag(item)
        // }
        var tagVowles = item.post_type == "Event" ? "an" : "a"
        if (item.post_type == "Event" && Object.keys(item?.post_event).length == 0)
            return <></>

        else
            return (
                <View
                    style={styles.itemContainer}>
                    <View
                        style={styles.itemHeader}>
                        <TouchableOpacity onPress={() => props.navigation.navigate(strings.UserProfileScreen, { u_id: item.post_owner_id })}>
                            <Image source={{ uri: item.u_image }} style={styles.itemProfileIcon} />
                        </TouchableOpacity>
                        <View>
                            <Text style={styles.itemPostTitle}>{item.u_first_name + " " + item.u_last_name}
                                {item.post_group != null && Object.keys(item.post_group).length != 0 ?
                                    <Text style={[styles.itemPostTitle, { fontWeight: "400", color: Colors.opacityColor3 }]}>
                                        {item.post_type == "" ? " " + getMultiplePostTag(item) : " posted " + tagVowles + " " + (postType).toString().toLowerCase() + " in " + item?.post_group.group_name + " "}
                                    </Text> :
                                    <Text style={[styles.itemPostTitle, { fontWeight: "400", color: Colors.opacityColor3 }]}>
                                        {isTextOnly ? " posted a Text" : (item.post_type == "" ? " posted " + tagVowles + getMultiplePostTag(item) : " posted " + tagVowles + " " + (postType).toString().toLowerCase())}
                                        {item.post_location != null ? " in " : ""}
                                    </Text>
                                }
                                {item.post_location != null ? item?.post_location : ""}
                            </Text>
                            <Text style={[styles.itemPostTitle, { opacity: 0.4 }]}>{time}</Text>
                        </View>
                        {item.post_owner_id == u_id && (item.post_type != "Audio" && item.post_type != "Video") &&
                            <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={() => moreMenuClick(item)}>
                                <Image source={Images.reportIcon} style={styles.postActionIcon} resizeMode={'contain'} />
                            </TouchableOpacity>}
                    </View>
                    <View style={{ paddingVertical: 5 }}>
                        {item.post_name != "" &&
                            <Text style={styles.postDesc}>{item.title}{item.post_name}</Text>
                        }
                    </View>
                    {item.post_type == "Event" ?
                        postEvent(item) :
                        item.post_type == "Photo" && item.post_images.length == 1 ?
                            postImage(item) :
                            item.post_type == "Location" ?
                                postLocationView(item) :
                                item.post_type == "Multiple Photos" ?
                                    multiImageView(item) :
                                    item.post_type == "Audio" || item.post_type == "Video" ?
                                        audioPostView(item) :
                                        item.post_type == "" || item.post_images.length > 1 ?
                                            multiplePostView(item) :
                                            <></>
                    }

                    <View style={styles.postFooter}>
                        <TouchableOpacity style={styles.footerElement} onPress={() => onLike(item.post_id, item.post_liked_by_me)}>
                            {item.post_liked_by_me == true ?
                                <Image source={Images.likeIcon}
                                    style={styles.likeIcon} resizeMode={'contain'} /> :
                                <Image source={Images.unfill}
                                    style={styles.likeIconUnfill} resizeMode={'contain'} />
                            }

                            <Text style={styles.likesText}>{item.post_likes_count}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.footerElement} onPress={() => props.navigation.navigate(strings.commentsScreen, { post_id: item.post_id, returnData: (data) => returnData(data) })}>
                            <CommentIcon />
                            <Text style={styles.likesText}>{item.post_comments.length}</Text>
                        </TouchableOpacity>
                    </View>
                    {item.post_comments.length != 0 &&
                        <>
                            <View style={styles.commentContainer}>
                                <Image source={{ uri: item.post_comments[item.post_comments.length - 1].user.u_image }} style={styles.commentProfile} />
                                <View style={styles.comments}>
                                    <Text style={styles.commentater}>{item.post_comments[item.post_comments.length - 1].user.u_first_name + " " + item.post_comments[item.post_comments.length - 1].user.u_last_name}</Text>
                                    <Text style={styles.comment}>{item.post_comments[item.post_comments.length - 1].post_comment_text}</Text>
                                    <Text style={styles.txtTime}>{moment.utc(item.post_comments[item.post_comments.length - 1].post_comment_created_at).local().format("MM/DD/YYYY h:mm A")}</Text>
                                </View>
                            </View>
                            <View style={styles.commentContainer}>
                                <TouchableOpacity style={styles.viewAllCommnetBtn} onPress={() => props.navigation.navigate(strings.commentsScreen, { post_id: item.post_id, returnData: (data) => returnData(data) })}>
                                    <Text style={styles.viewComment}>View all comments</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.commentContainer}>
                                <Image source={{ uri: u_image }} style={[styles.commentProfile, { alignSelf: "center" }]} />
                                <TextInput
                                    placeholder={strings.writeComment}
                                    placeholderTextColor={Colors.white}
                                    returnKeyType={'done'}
                                    onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onAddComment(text, item.post_id)}
                                    style={styles.textInput}
                                />
                            </View></>}
                </View>
            )
    }
    function isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }
    const multiplePostView = (item) => {
        return (
            <>
                {item.post_event != undefined && item.post_event != null && isEmpty(item.post_event) == false &&
                    postEvent(item)}
                {item.post_latitude != undefined && item.post_latitude != null && item.post_latitude != "" &&
                    postLocationView(item)}
                {item.post_images.length > 1 &&
                    multiImageView(item)
                }
                {
                    item.post_images.length == 1 &&
                    postImage(item)
                }
            </>
        )
    }
    const audioPostView = (item) => {
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()
        return (
            <TouchableOpacity
                onPress={() => item.post_type == "Audio" ?
                    props.navigation.navigate('player', { title: "Title", filepath: item.post_images[0].post_image_image, dirpath: null }) :
                    props.navigation.navigate(strings.webViewScreen, { url: item.post_images[0].post_image_image, title: item.u_first_name, date: date })
                }
                style={[styles.postImage, { alignItems: "center", justifyContent: "center" }]} >
                <Image source={Images.playerIcon} resizeMode={"contain"} style={[styles.postImage, { tintColor: Colors.white, width: 100, alignSelf: "center" }]} />
            </TouchableOpacity>
        )
    }
    const multiImageView = (item) => {
        const newArray1 = item.post_images;
        const newArray = newArray1.slice(0, (item.post_images.length / 2) + 1);
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()
        return (
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={{ flexDirection: "row" }}>
                    {
                        newArray.map((data, i) => {

                            return (

                                i % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                            {
                                                images: newArray1,
                                                flag: "post_image",
                                                index: i
                                            })
                                        }>
                                        <Image style={{ height: 210, width: 170, borderRadius: 4, marginRight: 2 }} source={{ uri: item.post_images[i].post_image_image }} ></Image>
                                    </TouchableOpacity> :
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                                {
                                                    images: newArray1,
                                                    flag: "post_image",
                                                    index: i
                                                })
                                            }>
                                            <Image style={{ height: 104, width: 170, borderRadius: 4, marginRight: 2 }} source={{ uri: item.post_images[i].post_image_image }} ></Image>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                                {
                                                    images: newArray1,
                                                    flag: "post_image",
                                                    index: i + 1
                                                })
                                            }>
                                            <Image style={{ height: 104, width: 170, borderRadius: 4, marginRight: 2, marginTop: 2 }} source={{ uri: item.post_images[i + 1]?.post_image_image }} ></Image>
                                        </TouchableOpacity>
                                    </View>
                            )
                        })
                    }
                </View>
            </ScrollView>

        )
    }
    const postLocationView = (item) => {

        var newDate = moment.utc(item.post_created_at).local().format(strings.hhmmaFormat);
        const obj = {
            name: item.u_first_name + " " + item.u_last_name,
            time: newDate,
            fullDate: item.post_created_at,
            imageUri: item.u_image,
            latitude: parseFloat(item?.post_latitude),
            longitude: parseFloat(item?.post_longitude),
            isShare: false
        }

        return (
            <TouchableOpacity onPress={() => props.navigation.navigate(strings.viewLocationScreen, { mapData: obj })}>
                <EventMap zoomEnabled={false} lat={item?.post_latitude} long={item?.post_longitude} style={styles.imgMap} />
            </TouchableOpacity>
        )
    }
    const postImage = (item) => {
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()

        return (
            <TouchableOpacity onPress={() =>
                props.navigation.navigate(strings.webViewScreen,
                    {
                        url: item.post_images[0].post_image_image,
                        title: item.u_first_name, date: date
                    })
            }>
                <Image source={{ uri: item?.post_images[0]?.post_image_image }} style={styles.postImage} />
            </TouchableOpacity>
        )
    }
    const postEvent = (item, str) => {
        var day = new Date(item.post_event.event_start_date).getDate();
        var month = new Date(item.post_event.event_start_date).getMonth();
        var isPastFlag = true;
        const date1 = new Date((item.post_event.event_end_date + " " + item.post_event.event_end_time).replace(/-/g, "/"));
        const date2 = new Date();
        var diff = (date1.getTime() - date2.getTime()) / 1000;
        if (diff > 0)
            isPastFlag = false;

        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(isPastFlag ? strings.EventDetailPastScreen : strings.EventDetailUpcomingScreen, { item: item.post_event })}
                style={styles.renderStyle} >
                <Image source={{ uri: item.post_event.event_image }} style={styles.postImage} ></Image>
                <View style={styles.upperView}>
                    <View style={styles.upperSubView}>

                        <View style={styles.dateView}>
                            <Text style={styles.txtDate}>{day}</Text>
                            <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                        </View>
                    </View>
                </View>
                <LinearGradient style={{
                    position: "absolute",
                    height: 180,
                    width: width - 32, bottom: 0,

                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomView}>
                    <Text style={styles.txtTitle}>{item?.post_event?.event_name}</Text>
                    <View style={styles.bottomBorderView}></View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
                        <PlaceIconUnselect height={15} width={15} />
                        <Text style={styles.txtAddress}>{item?.post_event?.event_location}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
    const emptyRender = () => {
        return (
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 50 }}>
                <NatureFun />
                <Text style={styles.txtWelcome}>{strings.welcome}</Text>
                <Text style={styles.txtEmptyLabel}>{strings.emptyGroupLabel}</Text>
            </View>
        )
    }
    const handleMoreData = () => {
        if (totalPage > page) {
            let pageCount = page;
            setPage(page + 1)
            setShowing(true)
            getAllPost(token, (pageCount + 1))
        }
    }
    const clickItem = (item) => {
        refSheet.close();
        if (item.key == "Edit")
            props.navigation.navigate(strings.CreatePostScreen, { postData: seletedPost })
        else {
            deletePost(seletedPost.post_id)
        }

    }
    const deletePost = async (post_id) => {
        setShowing(true)
        var obj = {
            post_id: post_id
        }
        await apiCallWithToken(obj, apiUrl.delete_post, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setTimeout(() => {
                    getAllPost(token, 1)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const callCancel = (data) => {

        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)
                // handle exception
            }
        );
    }
    const onRefresh = async () => {
        await getUserData().then(res => {
            setIsFetching(true)
            setShowing(true)
            getGroupData(res)
            getNotificationData(res.token)
        })
    }
    const onMessageNotification = () => {
        setMessageNotification(true)
    }
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : undefined}
            style={styles.container}
        >
            <View style={styles.container}>

                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <AppHeader
                    navigation={props.navigation}
                    title={strings.petFeed}
                    rightTitleRightPressed={() => props.navigation.navigate(strings.messageScreen)}
                    rightTitleLeftPressed={() => props.navigation.navigate("notification")}
                    rightIconLeft={Images.notification}
                    isNotification={isNotification}
                    isMessageNotification={isMessageNotification}
                    rightIconRight={Images.contact} />

                {isshowing == false && <View style={styles.groupContainer}>
                    <Text style={styles.groupText}>{"GROUPS(" + groupDatasource.length + ")"}</Text>
                    <TouchableOpacity style={[styles.viewContainer, { flex: groupDatasource.length == 0 ? 0.3 : 0.2 }]} onPress={() =>
                        groupDatasource.length != 0 ?
                            props.navigation.navigate(strings.groupsScreen) :
                            props.navigation.navigate(strings.createGroupScreen)

                    }
                    >
                        <Text style={styles.viewText}>{groupDatasource.length != 0 ? "View All" : "Create Group"}</Text>
                        <Image source={Images.backIcon} style={styles.ViewAllIcon} />
                    </TouchableOpacity>
                </View>}

                <FlatList
                    data={postData}
                    renderItem={renderItem}
                    keyExtractor={item => item.post_id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={emptyRender}
                    ListHeaderComponent={renderHeader}
                    onEndReachedThreshold={0.1}
                    onMomentumScrollBegin={() => { onEndReached = false; }}
                    onEndReached={() => {
                        if (!onEndReached) {
                            handleMoreData(); // on End reached
                            onEndReached = true;
                        }
                    }}
                    onRefresh={() => onRefresh()}
                    refreshing={isFetching}
                />
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => props.navigation.navigate(strings.CreatePostScreen)}
                    style={styles.touchableOpacityStyle}>
                    <LinearGradient
                        colors={Colors.btnBackgroundColor}
                        style={{ padding: 20, borderRadius: 30 }}
                    >
                        <EditIcon width={18} height={18} />
                    </LinearGradient>

                </TouchableOpacity>
                {/* <LowBattary
                isVisible={isLowBattery}
                textHeader={strings.lowBattary}
                iconTop={Images.lowBattery}
                txtDetail={"Oops! Looks like your pet collar for Daisy has low battery.Please recharge immediately to keep the functions on."}
                btnText={"Okay"}
                clickBtn={() => setLowBattery(false)}
            />
            <LowBattary
                isVisible={isProgress}
                textHeader={"Daisy on Run"}
                iconTop={Images.people1}
                txtDetail={"Daisy is on run. She has covered 4.5 miles so far. Tap continue to check progress"}
                btnText={"Check progress"}
                clickBtn={() => setIsProgress(false)}
                cancelBtn={() => setIsProgress(false)}
                isPeople={true}
            /> */}
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={140}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 10,
                        }
                    }}
                >
                    <MoreMenu
                        data={moreData}
                        clickItem={clickItem}
                    >

                    </MoreMenu>
                </RBSheet>
                <CometChatIncomingCall
                    callData={callData}
                    callCancel={callCancel}
                    isVisible={isVisible}
                    callAccept={callAccept}
                    navigation={props.navigation}
                    userData={userData}
                />
                {isVisible == false &&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisible}
                        onMessageNotification={() => onMessageNotification()}
                    />
                }
            </View>
        </KeyboardAvoidingView>
    )
}

export default HomeScreen;