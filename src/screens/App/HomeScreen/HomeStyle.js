import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { responsiveHeight, responsiveScreenFontSize, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from "../../../Resources/Colors";
import { Fonts } from "../../../Resources/Fonts";

const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    linearGradient: {
        flex: 1
    },
    groupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 18,
       // backgroundColor: 'rgba(255, 255, 255, 0.07)'
        // backgroundColor : Colors.theme
    },
    groupText: {
        color: Colors.white,
        fontSize: 14,
        fontWeight: '700',
        fontFamily: Fonts.DMSansRegular,
        // lineHeight:18.23,
        letterSpacing: 0.4,
        flex: 0.8
    },
    groupItem: {
        position: 'absolute',
        top: 44,
        left: 0,
        paddingLeft: 8,
        height: 136,

        justifyContent: "flex-end",
        width: 130,
    },
    groupCommnet: {
        fontSize: 14,
        fontWeight: '500',
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23

    },
    groupImage: {
        width: 24,
        height: 24,
        borderRadius: 20,

    },
    viewContainer: {
        flexDirection: 'row',
        flex: 0.3
    },
    viewText: {
        color: Colors.primaryViolet,
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 15.62,
        fontFamily: Fonts.DMSansRegular,
        
    },
    ViewAllIcon: {
        width: 9,
        height: 9,
        tintColor: Colors.primaryViolet,
        transform: [{ rotate: '180deg' }],
        marginLeft: 10,
        alignSelf: "center"
    },
    itemContainer: {
        backgroundColor: '#1D1233',
        marginVertical: 8,
        padding: 16
    },
    itemHeader: {
        flexDirection: 'row',
        width: width - 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemProfileIcon: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    itemPostTitle: {
        color: Colors.white,
        paddingHorizontal: 16,
        width: width * 0.80,
        fontSize: 14,
        fontWeight: '600',
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23
    },
    postActionIcon: {
        width: 5,
        height: 18,
        marginRight: 20
    },
    postDesc: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular

    },
    postImage: {
        width: width - 32,
        height: 180,
        borderRadius: 4
    },
    postFooter: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    footerElement: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 50
    },
    likeIcon: {
        width: 16,
        height: 15,
        tintColor:Colors.red,
      

    },
    likeIconUnfill: {
        width: 16,
        height: 15,
        tintColor:Colors.white,
        
    },
    likesText: {
        fontSize: 14,
        fontWeight: '400',
        color: Colors.white,
        marginLeft: 5,
        fontFamily: Fonts.DMSansRegular

    },
    groupProfile: {
        position: 'absolute',
        //backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileImage: {
        width: 24,
        height: 24,
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
        borderRadius: 12,

    },
    groupProfileContainer: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
    },
    moreGroupProfile: {
        fontSize: 8,
        fontWeight: '500',
        color: Colors.white
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 16,
        bottom: 20,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 15,
        height: 15,
        tintColor: Colors.white,
    },
    commentContainer: {
        flexDirection: 'row',
        marginVertical: 10,
        //justifyContent: 'space-between'
    },
    commentProfile: {
        width: 25,
        height: 25,
        marginRight:8,
        borderRadius:25
    },
    comments: {
        padding: 8,
        backgroundColor: '#FFFFFF26',
        borderRadius: 5,
        width:width/1.2
    },
    commentater: {
        fontSize: 10,
        fontWeight: '700',
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    comment: {
        fontSize: 14,
        lineHeight: 18,
        fontWeight: '400',
        color: Colors.white,
        marginTop: 5,
        fontFamily: Fonts.DMSansRegular,
        width:width/1.2

    },
    txtTime:{
        fontSize: 8,
        lineHeight: 10.42,
        fontWeight: '400',
        color: Colors.white,
        marginTop: 5,
        fontFamily: Fonts.DMSansRegular,
        opacity:0.4,
        flex:1,
        alignSelf:"flex-end"
    },
    textInput: {
        width: width * 0.83,
        height: 40,
        backgroundColor: Colors.theme,
        borderRadius: 5,
        paddingHorizontal: 10,
        color: Colors.white,
        opacity: 0.4,
        fontSize: 12,
        fontFamily: Fonts.DMSansRegular,
    

    },
    viewAllCommnetBtn: {
        marginLeft: 40
    },
    viewComment: {
        color: Colors.primaryViolet,
        fontSize: responsiveScreenFontSize(1.5),
        fontWeight: '500',
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 15.62

    },
    //NatureFun 
    txtWelcome: {
        fontSize: 20,
        lineHeight: 22,
        fontWeight: '700',
        color: Colors.white,
        marginTop: 5,
        fontFamily: Fonts.DMSansRegular
    },
    txtEmptyLabel: {
        fontSize: 14,
        lineHeight: 18.23,
        fontWeight: '400',
        color: Colors.white,
        marginTop: 16,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6,
        textAlign: "center",
        marginHorizontal: responsiveWidth(5)
    },
    //event view
    renderStyle: { height: 180, marginVertical: responsiveHeight(1), borderRadius: 4 },
    imgStyle: { height: 180, width: responsiveWidth(100), borderRadius: 4 },
    txtName: { fontSize: 14, fontWeight: "400", lineHeight: 18.23, alignSelf: "center", color: Colors.white, marginTop: responsiveHeight(1.8) },
    txtDetail: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, alignSelf: "center", color: Colors.white, textAlign: "center", opacity: 0.6 },
    btnView: { width: "100%", height: 40, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(2) },
    upperView: { position: "absolute", margin: 10, width: "95%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    peopleImg: { height: 32, width: 32, borderRadius: 15 },
    peopleImg1: { height: 32, width: 32, borderRadius: 15, marginLeft: -8 },
    moreView: { height: 32, width: 32, marginLeft: -8, alignItems: "center", justifyContent: "center" },
    moreBorderView: { backgroundColor: Colors.black, borderRadius: 15, opacity: 0.6, width: 32, height: 32, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700", fontSize: 9,
        lineHeight: 11.72, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    dateView: { height: 37, width: 36, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2 },
    txtDate: {
        fontSize: 16, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 10, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 0, margin: 10 },
    txtTitle: {
        fontWeight: "400", fontSize: 22, lineHeight: 27.5, color: Colors.white,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.4
    },
    bottomBorderView: { height: 1, backgroundColor: Colors.white, opacity: 0.2, marginVertical: 3 },
    placeIcon: { height: 13, width: 12, opacity: 0.7, tintColor: Colors.white },
    txtAddress: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 10, fontFamily: Fonts.DMSansRegular
    },
    //location view
    imgMap: {
        width: width - 32,
        height: 200,
        borderRadius: 4
    },


})
