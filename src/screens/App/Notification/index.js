import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, Platform } from 'react-native';

import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import styles from './NotificationStyle';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import { apiCallGetWithToken, apiCallWithToken, getUserData, storeNotificatioCount } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Loader from '../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { CometChat } from '@cometchat-pro/react-native-chat';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import moment, { now } from 'moment';
import { strings } from '../../../Resources/Strings';
import { toastConfig } from '../../../utils/ToastConfig';
import NotificationController from '../../../Components/NotificationController';


const Notification = (props) => {
    let refSheet = useRef();
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [totalPage, setTotalPage] = useState(1)
    const [limit, setLimit] = useState(20)
    const [page, setPage] = useState(1)
    const [notificationData, setNotificationData] = useState([])
    const [n_notification_type, setNotificationType] = useState("")
    const [n_id, setNotificationId] = useState("")
    const [u_id, setUserId] = useState("")
    const [placeHolder] = useState("http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg")
    var onEndReached;
    const [event_id, setEventId] = useState('')
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [notification_id, setNotificationIdSet] = useState("")
    useEffect(async () => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            getNotificationData(res.token, page);
        })
    }, []);

    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getNotificationData = async (token, page) => {
        var obj = {
            page: 1,
            limit: limit
        }
        await apiCallWithToken(obj, apiUrl.getnotifications, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    setNotificationData(res.result)
                    storeNotificatioCount({ notification_count: res.pagination.total })
                }
                else {
                    setNotificationData(notificationData.concat(res.result))
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderEmpty = () => {
        return (
            <View style={styles.emptyView}>
                <Image style={styles.imgBell} source={Images.bell}></Image>
                <Text style={styles.txtEmpty}>{"No Notifications"}</Text>
                <Text style={styles.txtDetail}>{"You don’t have any notification yet"}</Text>

            </View>
        )
    }
    const clickItem = (item) => {
        setNotificationType(item.n_notification_type)
        setNotificationId(item.n_sender_id);
        setEventId(JSON.parse(item.n_params).event_id)
        setNotificationIdSet(item.n_id)
        refSheet.open()
    }
    function getDifferenceInDays(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60 * 24);
    }
    function getDifferenceInHours(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60);
    }
    function getDifferenceInMinutes(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60);
    }

    function getDifferenceInSeconds(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / 1000;
    }
    const dateTimeGet = (date1, date2) => {
        const days = Math.round(getDifferenceInDays(date1, date2));
        const hours = Math.round(getDifferenceInHours(date1, date2));
        const minutes = Math.round(getDifferenceInMinutes(date1, date2));
        const second = Math.round(getDifferenceInSeconds(date1, date2));
        if (second < 60)
            return second + " Second ago"
        else if (minutes < 60)
            return minutes + " Minute ago"
        else if (hours < 24)
            return hours + " Hour ago"
        else
            return days + " Days ago"


    }
    const simpleTextGet = (response) => {
        const regex = /<b>(.*?)<\/b>/;
        const corresp = regex.exec(response);
        const firstParagraph = (corresp) ? corresp[0] : "" // <p>text1</p>
        const firstParagraphWithoutHtml = (corresp) ? corresp[1] : "" // text1
        return firstParagraphWithoutHtml;
    }
    const convertUTCToLocalTime = (date) => {
        const milliseconds = Date.UTC(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
        );
        const localTime = new Date(milliseconds);
        return localTime;
    };


    const clickItemNotification = (item) => {
        if (item.n_notification_type == 3) {
            let group_id = JSON.parse(item.n_params)?.group_id
            var obj = { group_id: group_id }
            if (group_id != null && group_id != undefined && group_id != "") {
                props.navigation.navigate(strings.chatMessageScreen, { groupData: obj })
            }

        }
        else if (item.n_notification_type == 4) {
            let event_id = JSON.parse(item.n_params)?.event_id
            var obj = { event_id: event_id }
            if (event_id != null && event_id != undefined && event_id != "") {
                props.navigation.navigate(strings.EventDetailUpcomingScreen, { item: obj })
            }

        }
        else if (item.n_notification_type == 6||item.n_notification_type==5) {
            let post_id = JSON.parse(item.n_params)?.post_id
            if (post_id != null && post_id != undefined && post_id != "") {
                props.navigation.navigate(strings.commentsScreen, { post_id: post_id })
            }
        }
        else {
            let user_id = JSON.parse(item.n_params)?.u_id
            if (user_id != null && user_id != undefined && user_id != "") {
                props.navigation.navigate(strings.UserProfileScreen, { u_id: user_id })
            }
        }
    }
    const renderSubView = ({ item, index }) => {
        const date1 = new Date(item.n_created_at);
        var newDate = convertUTCToLocalTime(date1);
        newDate = moment.utc(item.n_created_at).local();

        const date2 = new Date();
        const time = dateTimeGet(newDate, date2);

        var event_group_name = item.n_notification_type == 3 ? JSON.parse(item.n_params)?.group_name :
            item.n_notification_type == 4 ? JSON.parse(item.n_params)?.event_name : ""
        return (
            <View style={{ flexDirection: "row", marginVertical: responsiveHeight(1), justifyContent: "space-between" }}>
                <View style={{ flex: 0.7 }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <View>
                            <Image style={styles.imgView} source={{ uri: item.n_sender_image == "" ? placeHolder : item.n_sender_image }}></Image>
                            <View style={styles.starView}></View>
                            <Image style={styles.starIcon} source={Images.starIcon} ></Image>
                        </View>
                        <View style={{ marginLeft: 20, marginTop: 10 }}>

                            {(item.n_notification_type != 3 && item.n_notification_type != 4) &&
                                <Text onPress={() => clickItemNotification(item)} style={styles.txtname}>{item.n_notification_type==8||item.n_notification_type==9?"":item.n_sender_name}
                                    <Text style={styles.txtWithText}>{" "+item.n_message.toString().trim()}</Text>
                                </Text>}
                            {(item.n_notification_type == 3 || item.n_notification_type == 4) &&
                             <Text onPress={() => clickItemNotification(item)} style={styles.txtWithText}>{item.n_message}
                                <Text style={styles.txtname}>{" " + event_group_name}</Text>
                            </Text>}

                            <Text style={styles.txtTime}>{time}</Text>

                        </View>
                    </View>
                </View>
                {item.n_notification_type != 3 && item.n_notification_type != 5 && item.n_notification_type != 6 && item.n_notification_type != 8 && item.n_notification_type != 9 &&
                    <LinearGradient
                        colors={item.n_notification_type == 2 || item.n_notification_type == 4 ? Colors.btnBackgroundColor : [Colors.opacityColor2, Colors.opacityColor2]}
                        style={styles.viewRight}
                    >
                        <View style={styles.btnView}>
                            <TouchableOpacity style={{ padding: 3 }} onPress={() => clickAccept(item, "1")}>
                                <Text style={styles.txtBtn}>{item.n_notification_type == 2 ? "Accept" : "Going"}</Text>
                            </TouchableOpacity>
                            {item.n_notification_type == 2 || item.n_notification_type == 4 ?
                                <View style={styles.arrowView}>
                                    <View style={styles.viewLine}></View>
                                    <TouchableOpacity style={{ padding: 2 }} onPress={() => clickItem(item)}>
                                        <Image style={styles.imgDown} source={Images.downChevron}></Image>
                                    </TouchableOpacity>
                                </View> : null}
                        </View>
                    </LinearGradient>
                }
            </View>
        )
    }
    const clickAccept = (item, state) => {

        if (item.n_notification_type == 2) {
            actionFriend(item.n_sender_id, state, item.n_id)
        }
        else if (item.n_notification_type == 4) {
            invitePeople("1", JSON.parse(item.n_params).event_id, item.n_id)
        }

    }
    const removeNotification = async (n_id) => {
        var obj = {
            n_id: n_id != undefined ? n_id : notification_id,
            device_type: Platform.OS == "ios" ? "ios" : "android",
        }
        await apiCallWithToken(obj, apiUrl.removenotification, token).then(res => {
            setTimeout(() => {
                setShowing(false)
                if (res.status == true) {
                    getNotificationData(token, page);
                }
                else
                    showMessage(strings.error, strings.msgTag, res.message)


            }, 1000);

        })
    }
    const actionFriend = async (n_sender_id, state, notification_id) => {
        setShowing(true)
        var obj = {
            sender_id: n_sender_id,
            reciever_id: u_id,
            state: state
        }

        await apiCallWithToken(obj, apiUrl.friend_request_action, token).then(res => {
            setTimeout(() => {
                setShowing(false)
                if (res.status == true) {
                    removeNotification(notification_id)
                }
                else
                    showMessage(strings.error, strings.msgTag, res.message)


            }, 1000);

        })
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ marginVertical: responsiveHeight(2) }}>
                <Text style={styles.txtTitle}>{""}</Text>
                <FlatList
                    data={notificationData}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                    ListEmptyComponent={renderEmpty}
                >
                </FlatList>
            </View>
        )
    }
    const bottomView = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    refSheet.close(),
                        invitePeople("3", "")
                }
                }>
                    <Text style={styles.txtBottom}>{"Interested"}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginVertical: 15 }}
                    onPress={() => { refSheet.close(), invitePeople("4", "") }}
                >
                    <Text style={styles.txtBottom}>{"Not Going"}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { refSheet.close(), invitePeople("1", "") }}>
                    <Text style={styles.txtBottom}>{"Going"}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const invitePeople = async (em_status, eventID, notification_id) => {
        setShowing(true)
        var obj = {
            "event_id": eventID == "" ? event_id : eventID,
            "event_status": em_status,
        }
        await apiCallWithToken(obj, apiUrl.event_invitation_action, token).then(res => {

            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
                removeNotification(notification_id)

            }
            else {
                removeNotification(notification_id)
                showMessage(strings.error, strings.msgTag, res.message)
            }
            setTimeout(() => {
                setShowing(false)
            }, 1000);
        })
    }
    const friendRequest = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    refSheet.close(),
                        actionFriend(n_id, "1")
                }}>
                    <Text style={styles.txtBottom}>{"Accept"}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginVertical: 15 }} onPress={() => {
                    refSheet.close(),
                        actionFriend(n_id, "3")
                }}>
                    <Text style={styles.txtBottom}>{"Decline"}</Text>
                </TouchableOpacity>

            </View>
        )
    }
    const handleMoreData = () => {
        if (totalPage > page) {
            let pageCount = page
            setPage(page + 1)
            setShowing(true)
            getNotificationData(token, (pageCount + 1))
        }
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <AppHeader
                navigation={props.navigation}
                title={'Notifications'}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                {isshowing == false &&
                    <FlatList
                        data={["1"]}
                        style={{ margin: responsiveHeight(2) }}
                        ListEmptyComponent={renderEmpty}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderView}
                        onEndReachedThreshold={0.1}
                        onMomentumScrollBegin={() => { onEndReached = false; }}
                        onEndReached={() => {
                            if (!onEndReached) {
                                handleMoreData(); // on End reached
                                onEndReached = true;
                            }
                        }}
                    >
                    </FlatList>}
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={n_notification_type == 2 ? 100 : 166}
                    openDuration={250}
                    closeOnDragDown={true}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            paddingHorizontal: 25,
                            paddingVertical: 10
                        }
                    }}
                >
                    {n_notification_type == 2 ? friendRequest() : bottomView()}
                </RBSheet>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel}
                isVisible={isVisible} callAccept={callAccept} />}
            {isVisible == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisible}
                    onMessageNotification={()=>{}}

                />
            }
        </View>
    )
}
export default Notification;