import React from 'react';
import { StyleSheet } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../Resources/Colors";
import { Fonts } from '../../../Resources/Fonts';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    //empty view
    emptyView: { flex: 1, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(30) },
    imgBell: { height: 75, width: 75, tintColor: Colors.white },
    txtEmpty: {
        fontSize: 20, fontWeight: "700", lineHeight: 22, alignSelf: "center",
        color: Colors.white, marginTop: responsiveHeight(3),
        fontFamily: Fonts.DMSansRegular
    },
    txtDetail: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23, alignSelf: "center",
        color: Colors.white, marginTop: responsiveHeight(1), opacity: 0.6,
        fontFamily: Fonts.DMSansRegular
    },

    //title view
    txtTitle: {
        fontWeight: "700", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, textTransform: 'uppercase',
        fontFamily: Fonts.DMSansRegular

    },

    //subView
    imgView: { height: 48, width: 48, borderRadius: 24 },
    starView: {
        position: "absolute", height: 20, width: 20,
        backgroundColor: '#3D3350', bottom: 0, borderWidth: 1,
        borderRadius: 10, borderColor: '#0C0024', right: 0, left: 33
    },
    starIcon: {
        position: "absolute", height: 10, width: 10,
        bottom: 5,
        right: 0, left: 38,
        tintColor: Colors.white
    },
    txtContent: {

    },
    txtname: { fontSize: 14, fontWeight: "700", lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular },
    txtWithText: { fontSize: 14, fontWeight: "400", lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular },
    txtTime: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62, color: Colors.white, opacity: 0.4,
        fontFamily: Fonts.DMSansRegular
        // marginLeft: responsiveWidth(19)
    },
    txtBtn: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    btnView: { flexDirection: "row", alignItems: "center", justifyContent: 'center', height: 30 },
    arrowView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    viewLine: {
        height: 16, width: 2, backgroundColor: Colors.white, opacity: 0.15,
        marginVertical: responsiveHeight(1), marginLeft: responsiveWidth(2)
    },
    imgDown: { width: 10, height: 10, tintColor: Colors.white, marginLeft: responsiveWidth(1.5) },
    viewRight: { height: 30, width: 85, borderRadius: 4, marginTop: responsiveHeight(1.7) },

    //sheet view
    txtBottom: {
        fontWeight: "400", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    webviewText:{
        
    }
})