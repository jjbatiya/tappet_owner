import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../../Resources/Colors";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../Resources/Fonts";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    profileIcon: {
        width: 120,
        height: 120,
        borderRadius: 120
    },
    myProfile: { margin: 20, flexDirection: 'row', alignItems: 'center' },
    userInfo: {
        marginHorizontal: 20
    },
    userDetails: { flexDirection: 'row', marginTop: 10, alignItems: 'center' },
    userName: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 25,
        lineHeight: 40,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.2

    },
    userProfile: { margin: 20, flexDirection: 'row', alignItems: 'center' },
    icon: {
        tintColor: Colors.white,
        borderRadius: 20,
        width: 10,
        height: 10,
    },
    inforamtionText: {
        marginLeft: 10,
        color: Colors.white,
        fontWeight: '400',
        fontSize: 14,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6

    },

    editProfileButton: {
        marginVertical: 10,
        borderWidth: 1,
        // opacity: 0.5,
        borderColor: Colors.opacityColor2,
        // borderColor: '#DD46F6',
        height: 45,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    editIcon: {
        tintColor: Colors.white,
        width: 15,
        height: 15,
        marginRight: 10
    },

    addFriendButton: {
        // marginVertical: 10,
        borderWidth: 1,
        borderColor: Colors.white,
        height: 45,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.5,
    },
    addIcon: {
        tintColor: Colors.white,
        width: 20,
        height: 20,
        marginRight: 10
    },

    textlabel: {
        color: Colors.white,
        textAlign: 'center',
        fontWeight: "400",
        fontSize: 14,
        fontFamily: Fonts.DMSansRegular,

    },

    groupContainer: {
        marginHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    groupText: {
        color: Colors.white,
        fontSize: 14,
        fontWeight: '700',
        fontFamily: Fonts.DMSansRegular,

    },
    viewContainer: {
        flexDirection: 'row'
    },
    viewText: {
        color: Colors.primaryViolet,
        fontWeight: '500',
        fontSize: 12,
        fontFamily: Fonts.DMSansRegular,
        marginLeft: 2

    },
    petImage: {
        width: width,
        height: 160,
    },
    petName: {
        color: Colors.white,
        fontSize: 22,
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.2,
        lineHeight: 30

    },
    scientificName: {
        color: Colors.white,
        fontSize: 14,
        opacity:0.7,
        width:"100%"
    },
    petStatus: {
        fontWeight: "bold",
        fontSize: 12,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: 10,
        fontFamily: Fonts.DMSansRegular,

    },
    peopleImg: { height: 32, width: 32, borderRadius: 15 },
    peopleImg1: { height: 32, width: 32, borderRadius: 15, marginLeft: -8 },
    line: { height: 1, backgroundColor: "#FFFFFF", width: width * 0.55, marginVertical: 10, opacity: 0.5, },
    coOwners: {
        height: 30,
        width: 30,
        borderRadius: 30
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 44,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    editProfileText: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 14,
        fontFamily: Fonts.DMSansRegular,

    },
    saveButton: {
        flex: 0.30,
        // marginVertical: 10,
        borderWidth: 0.2,
        backgroundColor: Colors.white,
        height: 35,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    saveText: {
        color: Colors.themeColor,
        fontWeight: '600',
        fontSize: 16,
        fontFamily: Fonts.DMSansRegular,

    },
    profileView: {
        alignSelf: 'center',
    },
    editProfile: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute', right: -5, bottom: 5,
        backgroundColor: "#2C1F47", width: 40, height: 40, borderRadius: 20,
        borderWidth: 1,
        borderColor: 'rgba(12, 0, 36, 1)'
    },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -10,
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        opacity: 0.5,
        fontFamily: Fonts.DMSansRegular,
        marginLeft:-10
    },
    radioButton: { flexDirection: 'row', marginVertical: 10, alignItems: 'center' },
    radioView: { justifyContent: 'center', alignItems: 'center', width: 20, height: 20, borderRadius: 20, borderWidth: 1, },
    selectedRadioView: { width: 12, height: 12, borderRadius: 12, borderWidth: 1, backgroundColor: Colors.themeColor, },
    dropView: {
        height: 22, flex: 1, margin: responsiveHeight(1),
        marginHorizontal: responsiveWidth(2)
    },
    dropdown: {
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.black,
    },
    txtStyle: {
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,

    },
    arrow: {
        position: 'absolute',
        right: responsiveHeight(0.5),
        height: responsiveHeight(4),
        width: responsiveWidth(3),
        resizeMode: "contain",
        flex: 0.1,
        tintColor: Colors.white
    },
    txtCity: {
        opacity: 0.4,
        fontSize: 12, lineHeight: 15.62, fontWeight: "400", color: Colors.white,
        marginTop: responsiveHeight(2), marginHorizontal: responsiveWidth(2),
        fontFamily: Fonts.DMSansRegular,

    },
    dateClickView: {
        borderBottomColor: Colors.white,
        opacity: 0.5,
        borderBottomWidth: 1,
        paddingBottom: 10, marginVertical: 10, alignItems: 'center', justifyContent: 'space-between',
        flexDirection: "row",
    },
    calender: { resizeMode: "contain", height: 25, width: 25 },
    modal: {
        // margin: 50,
        // alignItems: 'center',   
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: '100%',
        width: '100%',
    },
    RBSheetItemView: { flexDirection: 'row', margin: 10, alignItems: 'center' },
    RBSheetIcon: {
        marginHorizontal: 10,
        tintColor: Colors.white,
        width: 20,
        height: 20,
        marginRight: 10
    },
    headerContainer: {
        flexDirection: 'row', justifyContent: 'space-between',
        marginHorizontal: 20, marginVertical: responsiveHeight(3), alignItems: 'center', paddingTop: 40
    },
    txtChangeEmail: {
        color: Colors.primaryViolet,
        fontSize: 12,
        lineHeight: 15.62,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,
        marginRight:8
    },
    tickMarkView:{
        position: "absolute", right: 5, bottom: 15, width: 18, height: 18,
        backgroundColor: Colors.primaryViolet,
        borderRadius: 9,alignItems:"center",justifyContent:"center"
    },
    //
    //group bottom sheet
    mainGroupView: { flexDirection: "row", marginRight: 20, marginVertical: 5, alignItems: "center" },

    imgGroup: {
        height: 48,
        width: 48,
        borderRadius: 54,
    },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily:Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily:Fonts.DMSansRegular,
        width:responsiveWidth(40)


    },
    drawer: {
        borderWidth: 2,
        width: 50,
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 15,
        borderColor: '#FFFFFF26'
    },
    verifyBtn: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5),  alignSelf: "center" },
    txtAuto: {
        fontSize: 16,
        backgroundColor: Colors.backgroundColor,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        flex: 1,
        marginLeft:-7,
        height:70
    
    },

})
