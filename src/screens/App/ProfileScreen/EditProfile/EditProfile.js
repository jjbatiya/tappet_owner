import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, PermissionsAndroid, Image, ScrollView, TextInput, Platform, KeyboardAvoidingView } from 'react-native';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import styles from '../ProfileStyle'
import { strings } from '../../../../Resources/Strings';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ModalDropdown from 'react-native-modal-dropdown';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
var FloatingLabel = require('react-native-floating-labels');
import RBSheet from "react-native-raw-bottom-sheet";
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import { EditIcon } from '../../../../utils/svg/EditIcon';
import { CalenderIcon } from '../../../../utils/svg/CalenderIcon';
import Camera from '../../../../utils/svg/Camera';
import Gallery from '../../../../utils/svg/Gallery';
import { ChevronDown } from '../../../../utils/svg/ChevronDown';
import Toast from 'react-native-toast-message';
import { apiCallGetWithToken, apiCallWithToken, getApiCall, getUserData, storeData, imageUploadApi, addressGet } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import { validationMsg } from '../../../../Resources/ValidationMsg';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import AutoSerachComponent from '../../../../Components/AutoSerachComponent';

import { CometChat } from '@cometchat-pro/react-native-chat';
import ImagePicker from 'react-native-image-crop-picker';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';


const EditProfile = (props) => {

    const [isNameFocus, setIsNameFocus] = useState(false)
    const [isLastFocus, setIsLastFocus] = useState(false)
    const [isEmailFocus, setEmailFocus] = useState(false)
    const [isPhoneNumberFocus, setPhoneNumberFocus] = useState(false)
    const [isZipCodeFocus, setZipCodeFocus] = useState(false)
    const [isAddressFocus, setAddressFocus] = useState(false)
    const [selectRadio, setSelectRadio] = useState("")
    const [showDate, setShowDate] = useState(false);
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [date, setDate] = useState(new Date());
    const [fileUri, setFileUri] = useState("")
    const [u_first_name, setFirstName] = useState("")
    const [u_last_name, setLastName] = useState("")
    const [u_email, setEmail] = useState("")
    const [u_mobile_number, setMobileNo] = useState("")
    const [u_country, setCountry] = useState("Select Country Name")
    const [c_id, setCid] = useState("")
    const [u_state, setUState] = useState("")
    const [u_city, setCity] = useState("Select City Name")
    const [u_zipcode, setZipCode] = useState("")
    const [u_address, setAddress] = useState("")
    const [isshowing, setShowing] = useState(true)
    const [u_name, setName] = useState("")
    const [isUpdateImage, setUpdateImage] = useState(false)
    const [countryData, setCountryData] = useState([])
    const [cityData, setCityData] = useState([])
    const [imageData, setImageData] = useState(null)
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    let refSheet = useRef();
    useEffect(() => {
        getUserData().then(res => {
            setUserID(res.u_id)
            setToken(res.token)
            getProfileData(res);
        })

    }, [])
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })
                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getProfileData = async (res) => {
        await apiCallGetWithToken(apiUrl.user + "/" + res.u_id + "?device_type=" + Platform.OS, res.token).then(res => {
            if (res.status == true) {
                setFirstName(res.result.u_first_name)
                setLastName(res.result.u_last_name)
                setName(res.result.u_first_name + " " + res.result.u_last_name)
                setEmail(res.result.u_email)
                setSelectRadio(res.result.u_gender)
                setDate(res.result.u_dob == "" ? "" : new Date(res.result.u_dob))
                setMobileNo(res.result.u_mobile_number)
                setZipCode(res.result.u_zipcode)
                setAddress(res.result.u_address)
                setCity(res.result.u_city == "" ? "Select City Name" : res.result.u_city)
                setFileUri(res.result.u_image)
                setCid(res.result.u_country)
                setCountry(res.result.u_country)
                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const zipCodeToAddress = async () => {
        await addressGet(u_zipcode).then(res => {
            if (res.results.length != 0) {
                setCountry(res.results[0].address_components[res.results[0].address_components.length - 1].long_name)
                setCity(res.results[0].address_components[1].long_name)
            }
            else
                showMessage(strings.error, strings.msgTag, validationMsg.zipcodeInvalid)
        }
        )
    }
    const onChangeDate = (selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        setDate(currentDate);
    };

    const hideShowDatePicker = () => {
        setShowDate(false);
    };
    const userUpdateDetail = async () => {
        setShowing(true)
        const obj = {
            u_first_name: u_first_name,
            u_last_name: u_last_name,
            u_mobile_number: u_mobile_number,
            u_id: u_id,
            u_gender: selectRadio,
            u_dob: date != "" ? date?.getFullYear() + "-" + parseInt(date?.getMonth() + 1) + "-" + date?.getDate() : "",
            u_zipcode: u_zipcode,
            u_city: u_city,
            u_address: u_address,
            //u_country: c_id
            u_country: u_country
        }
        await apiCallWithToken(obj, apiUrl.updateuserdetail, token).then(res => {
            if (res.status == true) {
                var obj = { ...res.result, token: token }
                storeData(obj);
                setTimeout(() => {
                    setShowing(false)
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.TabNavigation }],
                    });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const onBlur = (flag) => {
        if (flag == "name")
            setIsNameFocus(!isNameFocus)
        else if (flag == "email")
            setEmailFocus(!isEmailFocus)
        else if (flag == "phoneNumber")
            setPhoneNumberFocus(!isPhoneNumberFocus)
        else if (flag == "zipCode")
            setZipCodeFocus(!isZipCodeFocus)
        else if (flag == "address")
            setAddressFocus(!isAddressFocus)
        else if (flag == "last")
            setIsLastFocus(!isLastFocus)
    }

    const onSelectRadio = (value) => {
        setSelectRadio(value)
    }
    const clickImage = () => {
        refSheet.close();
        setTimeout(() => {

            launchImageLibrary()
        }, 400);

    }
    const clickCamera = () => {
        refSheet.close();
        setTimeout(() => {

            launchCamera()
        }, 400);
    }
    const verifyModal = () => {
        return (
            <View style={styles.modal}>
                <TouchableOpacity
                    onPress={() => clickCamera()}
                    style={styles.RBSheetItemView}>
                    <View style={styles.RBSheetIcon}>
                        <Camera />
                    </View>
                    <Text style={styles.editProfileText}>{strings.takeAPhoto}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => clickImage()}
                    style={styles.RBSheetItemView}>
                    <View style={styles.RBSheetIcon}>
                        <Gallery />
                    </View>
                    <Text style={styles.editProfileText}>{strings.uploadFromGallery}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => removePhoto()}
                    style={styles.RBSheetItemView}>
                    <Image source={Images.deleteIcon} style={[styles.RBSheetIcon, { tintColor: "red" }]} />
                    <Text style={[styles.editProfileText, { color: "red" }]}>{strings.removePhoto}</Text>
                </TouchableOpacity>

            </View>
        )
    }
    const removePhoto = () => {
        refSheet.close();
        setTimeout(() => {
            setImageData(null)
            setFileUri("")
            setUpdateImage(false)
        }, 500);

    }
    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };
    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };

    const launchCamera = async () => {

        if (Platform.OS == "android") {
            let isCameraPermitted = await requestCameraPermission();
            let isStoragePermitted = await requestExternalWritePermission();
            if (isCameraPermitted && isStoragePermitted) {
            }
            else {
                return 0;
            }
        }
        ImagePicker.openCamera({
            compressImageQuality: 0.7,
            width:300,
            height:400,
            cropping: true,
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                setImageData(obj)
                setFileUri(image?.path)
                setUpdateImage(true)
            } catch (error) {

            }
        });

    }

    const launchImageLibrary = async () => {

        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                setImageData(obj)
                setFileUri(image?.path)
                setUpdateImage(true)
            } catch (error) {

            }
        });
    }
    const onSave = async () => {
        if (u_first_name.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.firstname)
        else if (u_last_name.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.lastname)
        else if (u_mobile_number.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.phoneNo)
        else if (selectRadio == undefined || selectRadio == '')
            showMessage(strings.error, strings.msgTag, validationMsg.gender)
        else if (u_zipcode.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.zipCode)
        else if (c_id + "".trim() == '' && u_country == '')
            showMessage(strings.error, strings.msgTag, validationMsg.country)
        else if (u_city.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.city)
        else if (u_address.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.address)
        else {
            if (isUpdateImage) {
                await imageUpload()
            }
            else
                userUpdateDetail();

        }

    }
    const imageUpload = async () => {
        let data = new FormData();
        data.append("u_image", { type: imageData.type, uri: imageData.uri, name: imageData.fileName });
        data.append("u_id", u_id)
        setShowing(true)
        await imageUploadApi(data, apiUrl.update_profile_picture, token).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    userUpdateDetail();
                }, 2000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }


    const renderProfile = () => {

        if (fileUri) {
            return <View style={styles.profileView}>
                <Image source={{ uri: fileUri }} style={styles.profileIcon} />
                <TouchableOpacity
                    onPress={() => refSheet.open()}
                    style={styles.editProfile}>
                    <View style={[styles.editIcon, { marginRight: 0 }]}>
                        <EditIcon width={15} height={15} />
                    </View>
                </TouchableOpacity>
            </View>
        } else {
            return <View style={styles.profileView}>
                <Image source={{ uri: "http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg" }} style={styles.profileIcon} />
                <TouchableOpacity
                    onPress={() => refSheet.open()}
                    style={styles.editProfile}>
                    <View style={[styles.editIcon, { marginRight: 0 }]}>
                        <EditIcon width={15} height={15} />
                    </View>
                </TouchableOpacity>
            </View>
        }
    }
    const getCountryName = (cid, array) => {
        return array.find(data => data.c_id == cid);

    }
    const onSelectCountry = async (index) => {
        await setCid(countryData[index].c_id)
        await setCountry(countryData[index].c_name)
        getCity(countryData[index].c_id);
    }
    const getCity = async (cid) => {
        setShowing(true)
        await getApiCall(apiUrl.get_city_by_country_id + "/" + cid + "?type=2").then(res => {
            setTimeout(() => {
                setShowing(false)
            }, 1000);
            if (res.status == true) {
                setCityData(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })


    }
    const onSelectCity = (index) => {
        setCity(cityData[index].city_name)
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const clickAddress = (data) => {
        setAddress(data.formatted_address)
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" && "padding"}
            style={styles.container}
        >
            <View style={styles.container}>
                <CommonStatusBar color={Colors.backgroundColor} />
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => props.navigation.navigate("MyProfile")} >
                        <Image source={Images.closeIcon} style={[styles.editIcon, { width: 20, height: 20 }]} />
                    </TouchableOpacity>
                    <Text style={styles.editProfileText}>Edit Profile</Text>
                    <TouchableOpacity
                        style={styles.saveButton}
                        onPress={onSave}
                    >
                        <Text style={styles.saveText}>{strings.save}</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} style={{ marginHorizontal: 20, marginBottom: 10 }}>
                    <>
                        {renderProfile()}

                        <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={isNameFocus ? styles.focusforminput : styles.formInput}
                            onFocus={() => onBlur("name")}
                            onBlur={() => onBlur("name")}
                            onChangeText={(text) => setFirstName(text)}
                            value={u_first_name}
                        >
                            {strings.name}
                        </FloatingLabel>

                        <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={isLastFocus ? styles.focusforminput : styles.formInput}
                            onFocus={() => onBlur("last")}
                            value={u_last_name}
                            onBlur={() => onBlur("last")}
                            onChangeText={(value) => setLastName(value)}
                        >{strings.lastName}</FloatingLabel>
                        <View>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={isEmailFocus ? styles.focusforminput : styles.formInput}
                                onFocus={() => onBlur("email")}
                                onBlur={() => onBlur("email")}
                                onChangeText={(text) => setEmail(text)}
                                value={u_email}

                            >
                                {strings.emailAdress}
                            </FloatingLabel>
                            {/* <TouchableOpacity style={{ position: "absolute", right: 0, bottom: 15, }}>
                                <Text style={styles.txtChangeEmail}>Change Email</Text>
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ margin: 5, marginTop: 30 }}>
                            <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                                Gender*
                            </Text>

                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => onSelectRadio("Male")}
                                    style={styles.radioButton}>
                                    <View style={[styles.radioView, { borderColor: selectRadio == "Male" ? Colors.themeColor : Colors.white }]} >
                                        {selectRadio == "Male" && <View style={styles.selectedRadioView} />}
                                    </View>
                                    <Text style={[styles.input, { marginLeft: 10 }]}>Male</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => onSelectRadio("Female")}
                                    style={[styles.radioButton, { marginLeft: responsiveWidth(12) }]}>
                                    <View style={[styles.radioView, { borderColor: selectRadio == "Female" ? Colors.themeColor : Colors.white }]} >
                                        {selectRadio == "Female" && <View style={styles.selectedRadioView} />}
                                    </View>
                                    <Text style={[styles.input, { marginLeft: 10 }]}>Female</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ flexDirection: 'row' }}>

                                <TouchableOpacity
                                    onPress={() => onSelectRadio("Other")}
                                    style={styles.radioButton}>
                                    <View style={[styles.radioView, { borderColor: selectRadio == "Other" ? Colors.themeColor : Colors.white }]} >
                                        {selectRadio == "Other" && <View style={styles.selectedRadioView} />}
                                    </View>
                                    <Text style={[styles.input, { marginLeft: 10 }]}>Other</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => onSelectRadio("Prefer not to say")}
                                    style={[styles.radioButton, { marginLeft: responsiveWidth(10) }]}>
                                    <View style={[styles.radioView, { borderColor: selectRadio == "Prefer not to say" ? Colors.themeColor : Colors.white }]} >
                                        {selectRadio == "Prefer not to say" && <View style={styles.selectedRadioView} />}
                                    </View>
                                    <Text style={[styles.input, { marginLeft: 10 }]}>Prefer not to say</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                            <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                                Date of Birth
                            </Text>
                            <TouchableOpacity onPress={() => setShowDate(true)} style={styles.dateClickView}>
                                <TextInput
                                    style={[styles.input, { marginLeft: -5 }]}
                                    placeholder="MM/DD/YYYY"
                                    editable={false}
                                    value={date != "" ? parseInt(date?.getMonth() + 1) + "/" + date?.getDate() + "/" + date?.getFullYear() : ""}
                                    //  onChangeText={(searchString) => { this.setState({ searchString }) }}
                                    underlineColorAndroid="transparent"
                                />
                                <CalenderIcon height={20} width={20} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={isPhoneNumberFocus ? styles.focusforminput : styles.formInput}
                                onFocus={() => onBlur("phoneNumber")}
                                onBlur={() => onBlur("phoneNumber")}
                                keyboardType={'number-pad'}
                                onChangeText={(text) => setMobileNo(text)}
                                value={u_mobile_number}
                                editable={false} selectTextOnFocus={false}
                            >
                                {strings.phoneNumber}
                            </FloatingLabel>
                            <View style={styles.tickMarkView}>
                                <Image style={{ height: 8, width: 8 }} source={Images.tickMark} />
                            </View>
                        </View>
                        <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            keyboardType={'number-pad'}
                            style={isZipCodeFocus ? styles.focusforminput : styles.formInput}
                            onFocus={() => onBlur("zipCode")}
                            onBlur={() => onBlur("zipCode")}
                            onChangeText={(text) => setZipCode(text)}
                            // onSubmitEditing={() => zipCodeToAddress()}
                            onEndEditing={() => zipCodeToAddress()}

                            returnKeyType={'done'}
                            value={u_zipcode}

                        >
                            {strings.zipCode}
                        </FloatingLabel>
                        <Text style={styles.txtCity}>Country</Text>
                        <ModalDropdown
                            defaultValue={u_country}
                            disabled={true}
                            //   options={countryData.map(a => a.c_name)}
                            style={styles.dropView}
                            onSelect={(index) => onSelectCountry(index)}
                            dropdownStyle={{
                                width: 300
                            }}
                            dropdownTextStyle={styles.dropdown}
                            textStyle={styles.txtStyle}
                            dropdownTextHighlightStyle={{
                                color: Colors.theme
                            }}

                            renderRightComponent={() => {
                                return (
                                    <View style={styles.arrow}>
                                        <ChevronDown width={responsiveWidth(3)} height={responsiveHeight(4)} />
                                    </View>
                                )
                            }}
                        />
                        <View style={{ marginHorizontal: 5, height: 1, backgroundColor: Colors.white }}></View>

                        {u_zipcode.length != 0 && <>
                            <Text style={styles.txtCity}>City</Text>
                            <ModalDropdown
                                defaultValue={u_city}
                                disabled={true}

                                //   options={cityData.map(a => a.city_name)}
                                style={styles.dropView}
                                onSelect={(index) => onSelectCity(index)}
                                dropdownStyle={{
                                    width: 300
                                }}
                                dropdownTextStyle={styles.dropdown}
                                textStyle={styles.txtStyle}
                                dropdownTextHighlightStyle={{
                                    color: Colors.theme
                                }}
                                //defaultIndex={this.stateNameTogetIndex(this.state.state)}
                                renderRightComponent={() => {
                                    return (
                                        <View style={styles.arrow}>
                                            <ChevronDown width={responsiveWidth(3)} height={responsiveHeight(4)} />
                                        </View>
                                    )
                                }}
                            />
                            <View style={{ marginHorizontal: 5, height: 1, backgroundColor: Colors.white }}></View>
                        </>}
                        <View style={{ marginHorizontal: 5, marginTop: 20,flex:1 }}>
                            <Text style={[styles.labelInput, { marginLeft: 0 }]}>
                                {strings.address}
                            </Text>

                            <AutoSerachComponent
                                style={styles.txtAuto}
                                onChangeText={(text) => setAddress(text)}
                                event_location={u_address}
                                onPress={(data) => clickAddress(data)}
                                isRightIcon={false}
                                placeholderTextColor={Colors.opacityColor3}
                            />

                        </View>
                        <View style={{ marginHorizontal: 5, height: 1, backgroundColor: Colors.white }}></View>

                        {/* <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={isAddressFocus ? styles.focusforminput : styles.formInput}
                            onFocus={() => onBlur("address")}
                            value={u_address}
                            onChangeText={(text) => setAddress(text)}

                            onBlur={() => onBlur("address")}
                        >
                            {strings.address}
                        </FloatingLabel> */}
                    </>
                    <RBSheet
                        ref={ref => {
                            refSheet = ref;
                        }}
                        height={172}
                        openDuration={250}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {
                                justifyContent: "center",
                                alignItems: "center",
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                padding: 10
                            }
                        }}
                    >
                        {verifyModal()}
                    </RBSheet>
                    {showDate && (
                        <DateTimePickerModal
                            isVisible={showDate}
                            mode="date"
                            date={date}
                            onConfirm={onChangeDate}
                            onCancel={hideShowDatePicker}
                        />
                    )}
                </ScrollView>
            </View>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
             {isVisibleCall==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisibleCall}
                        onMessageNotification={()=>{}}

                    />
                }
        </KeyboardAvoidingView>
    )
}

export default EditProfile;