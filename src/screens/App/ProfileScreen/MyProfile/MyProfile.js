import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, Dimensions, FlatList, Image, ScrollView, Platform } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import LinearGradient from 'react-native-linear-gradient';
import styles from '../ProfileStyle'
import { EditIcon } from '../../../../utils/svg/EditIcon';
import { PlusIcon } from '../../../../utils/svg/PlusIcon';
import Friends from '../../../../utils/svg/Friends';
import Business from '../../../../utils/svg/Business';
import { apiCallGetWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
const { width } = Dimensions.get("window")
import Toast from 'react-native-toast-message';
import moment from 'moment';
import { strings } from '../../../../Resources/Strings';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';

const MyProfile = (props) => {
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [u_image, setUserImage] = useState("http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg")
    const [u_name, setUserName] = useState("")
    const [petList, setPetList] = useState([])
    const [userData, setUserData] = useState("");
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const [pet_co_owned, setCo_Owned_PetList] = useState([])
    useEffect(() => {

        props.navigation.addListener('focus', () => {
            getUserData().then(res => {
                setShowing(true)
                setUserID(res.u_id)
                setToken(res.token)
                getProfileData(res);
            })
        });
    }, [])
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getProfileData = async (res) => {
        await apiCallGetWithToken(apiUrl.user + "/" + res.u_id + "?device_type=" + Platform.OS, res.token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setUserData(res.result)
                setUserName(res.result.u_first_name + " " + res.result.u_last_name)
                setPetList(res.result.pets)
                setCo_Owned_PetList(res.result.pet_co_owned)
                if (res.result.u_image != "") {
                    setUserImage(res.result.u_image)
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderMyPetList = ({ item, index }) => {
        var petImage = item.pet_image != "" && item.pet_image != undefined ? item.pet_image : item.pt_image
        let breed = item.breed[0]?.pb_name;
        var a = moment(new Date());
        var b = moment(new Date(item.pet_dob));

        var years = a.diff(b, 'year');
        if (years <= 0)
            years = item.pet_age
        else
            years += " Year"
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.PetProfileTabScreen, { screen: "MyProfile", petData: item, userImage: u_image })}
                style={{}}>

                <View>
                    <Image source={{ uri: petImage }} style={styles.petImage} />
                    <View style={{ position: 'absolute' }}>
                        <View style={{
                            position: 'absolute',
                            height: 20,
                            backgroundColor: Colors.white,
                            opacity: 0.2,
                            justifyContent: 'center', flexDirection: 'row', alignItems: 'center',
                            width: 120, margin: 10,
                        }} />
                        <>
                            {index == 0 &&
                                <View style={{ flexDirection: "row", justifyContent: "space-between", width: '78%' }}>
                                    <View style={{
                                        height: 20, flexDirection: 'row', margin: 10,
                                        alignItems: 'center', justifyContent: 'center',
                                        backgroundColor: Colors.opacityColor2,
                                    }}>
                                        < View style={{
                                            backgroundColor: 'green', width: 10, height: 10,
                                            borderRadius: 10, alignSelf: "center", marginLeft: 10,

                                        }} />
                                        <Text style={styles.petStatus}>
                                            Checked In
                                        </Text>
                                    </View>
                                    <Image style={{ height: 20, width: 20, alignSelf: "center" }} source={Images.battery}></Image>
                                </View>

                            }
                            {index == 1 &&
                                <View style={{ flexDirection: "row", justifyContent: "space-between", width: '78%' }}>

                                    <View style={{ height: 20, flexDirection: 'row', margin: 10, justifyContent: 'center', alignItems: 'center', paddingLeft: 10 }}>

                                        <Text style={styles.petStatus}>
                                            Out for walk
                                        </Text>
                                    </View>
                                    <Image style={{ height: 20, width: 20, alignSelf: "center" }} source={Images.battery}></Image>

                                </View>
                            }
                        </>
                    </View>
                    <LinearGradient style={{ width: width, height: 160, position: "absolute" }} colors={Colors.viewGroupContain} />
                    <View style={{
                        margin: 10, bottom: 5, position: 'absolute',
                        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'
                    }}>
                        <View style={{ flex: 0.85 }}>
                            <Text style={styles.petName}>
                                {item.pet_name}
                            </Text>
                            <View style={styles.line} />
                            <Text style={styles.scientificName}>
                                {breed + "  •  " + years}
                            </Text>
                        </View>

                    </View>
                </View>

            </TouchableOpacity>
        )
    }

    const renderCoOwnPetList = ({ item, index }) => {
        var petImage = item.pet_image != "" && item.pet_image != undefined ? item.pet_image : item.pt_image
        let breed = item?.breed[0]?.pb_name;
        var a = moment(new Date());
        var b = moment(new Date(item.pet_dob));

        var years = a.diff(b, 'year');
        if (years <= 0)
            years = item.pet_age
        else
            years += " Year"
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.PetProfileTabScreen, { screen: "MyProfile", petData: item, userImage: u_image,isCoowner:true })}

            >
                <Image source={{ uri: petImage }} style={styles.petImage} />
                <LinearGradient style={{ width: width, height: 160, position: "absolute" }} colors={Colors.viewGroupContain} />

                <View style={{ margin: 10, bottom: 5, position: 'absolute' }}>
                    <Text style={styles.petName}>
                        {item.pet_name}
                    </Text>
                    <View style={styles.line} />
                    <Text style={styles.scientificName}>
                        {breed + "  •  " + years}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    const renderMyPets = () => {
        return (
            <>
                <View style={styles.groupContainer}>
                    <Text style={styles.groupText}>{"MY PETS (" + petList.length + ")"}</Text>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate(strings.addPetScreen)}
                        style={styles.viewContainer}>
                        <View style={{ alignSelf: "center" }}>
                            <PlusIcon width={14} height={14} color={Colors.primaryViolet} />
                        </View>
                        <Text style={styles.viewText}>Add Pet</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    contentContainerStyle={{ paddingBottom: 20 }}
                    style={{ marginVertical: 10 }}
                    data={petList}
                    // keyExtractor={item => item.id}
                    renderItem={renderMyPetList}
                />
            </>
        )
    }

    const renderCoOwnPets = () => {
        return (
            <>
                <View style={styles.groupContainer}>
                    <Text style={styles.groupText}>{"CO-OWNED (" + pet_co_owned.length + ")"} </Text>
                </View>
                <FlatList
                    contentContainerStyle={{ paddingBottom: 20 }}
                    style={{ marginVertical: 10 }}
                    data={pet_co_owned}
                    // keyExtractor={item => item.id}
                    renderItem={renderCoOwnPetList}
                />
            </>
        )
    }

    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <AppHeader
                navigation={props.navigation}
                title={strings.myProfile}
                rightTitleRightPressed={() => props.navigation.navigate("setting")}
                rightIconRight={Images.setting} />
            <ScrollView showsVerticalScrollIndicator={false}
            >
                <>
                    <View style={styles.myProfile}>
                        <Image source={{ uri: u_image }} style={styles.profileIcon} />
                        <View style={styles.userInfo}>
                            <Text style={styles.userName}>
                                {u_name}
                            </Text>
                            <TouchableOpacity
                                onPress={() => props.navigation.navigate("friends", { isUser: true })}
                                style={styles.userDetails}>
                                <Friends />
                                <Text style={styles.inforamtionText}>
                                    {userData?.has_total_friends_count + " Friends"}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => props.navigation.navigate("friends", { isUser: true })}
                                style={styles.userDetails}>
                                <Business />
                                <Text style={styles.inforamtionText}>
                                    {userData?.has_total_business_count + " Business"}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={{ marginHorizontal: 20, marginBottom: 15 }} onPress={() => props.navigation.navigate(strings.EditProfileScreen)}>
                        <View style={styles.editProfileButton}>
                            <View style={styles.editIcon}>
                                <EditIcon width={15} height={15} />
                            </View>
                            <Text style={styles.textlabel}>{"Edit Profile "}</Text>
                        </View>
                    </TouchableOpacity>

                    {petList.length != 0 && renderMyPets()}
                    {pet_co_owned.length != 0 && renderCoOwnPets()}
                </>
            </ScrollView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    onMessageNotification={() => { }}
                    callIsVisible={isVisibleCall}
                />
            }
        </View>
    )
}

export default MyProfile;