import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, Dimensions, FlatList, Image, ScrollView, Platform, Share } from 'react-native';
import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import LinearGradient from 'react-native-linear-gradient';
import styles from '../ProfileStyle'
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import Friends from '../../../../utils/svg/Friends';
import Business from '../../../../utils/svg/Business';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, apiCallWithToken, createDynamicLinks, getUserData, imageUploadApi } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
const { width } = Dimensions.get("window")
import Toast from 'react-native-toast-message';
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../../Components/MoreMenu';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';


const UserProfile = (props) => {
    const [bottomData, setBottomData] = useState([
        {
            key: "blockUser",
            title: "Block",
            image: Images.blocked
        },
        {
            key: "share",
            title: "Share",
            image: Images.share
        },
        {
            key: "addToGroup",
            title: "Add to a group",
            image: Images.addFriend
        },
        {
            key: "inviteEvent",
            title: "Invite to an play date",
            image: Images.serachIcon
        },

        {
            key: "delete",
            title: "Remove friend",
            image: Images.deleteIcon
        },
    ])
    const [isshowing, setShowing] = useState(true)

    const [isFriend, setIsFriend] = useState(false)
    const [isRequested, setIsRequested] = useState(false)
    const [u_name, setUserName] = useState("")
    const [petList, setPetList] = useState([])
    const [token, setToken] = useState("")
    const [u_image, setUserImage] = useState("http://tappet.reviewprototypes.com/public/assets/images/no-image-placeholder.jpg")
    const [u_id] = useState(props.route.params.u_id != undefined ? props.route.params.u_id : "1")
    const [userData, setUserData] = useState(null);
    const [userId, setUserID] = useState("")
    const [groupData, setGroupData] = useState([])
    const [blockByMe, setBlockByMe] = useState(false)
    const [eventdata, setEventData] = useState([])
    let refSheet = useRef();
    let refSheetGroup = useRef();
    let refSheetEvent = useRef();
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    useEffect(() => {
        getUserData().then(res => {
            setUserID(res.u_id)
            setToken(res.token)
            getProfileData(res);
            getEventData(res);
        })
        // console.log("DATA-->", moment(date).format("MM/DD/YYYY"))
    }, [])
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const callClick = () => {
        var receiverID = comeChatUserTag + "" + userData.u_id;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.USER;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                //props.navigation.navigate("callScreen", { callData: outGoingCall })
                props.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: userData })

                // perform action on success. Like show your calling screen.
            },
            error => {
                console.log("Call initialization failed with exception:", error);
                if(error?.code=="ERR_BLOCKED_SENDER")
                {
                    showMessage(strings.error, strings.msgTag,userData.u_first_name + " block you so can't send message")

                }
            }
        );
    }
    const getProfileData = async (data) => {
        await apiCallGetWithToken(apiUrl.user + "/" + u_id + "?device_type=" + Platform.OS, data.token).then(res => {
            if (res.status == true) {
                setBlockByMe(res.result.userblockbyme)
                setUserData(res.result)
                setUserName(res.result.u_first_name + " " + res.result.u_last_name)
                setPetList(res.result.pets)
                if (res.result.u_image != "") {
                    setUserImage(res.result.u_image)
                }
                getGroupData(data.token, data.u_id)
                editMenuData(res.result.userblockbyme)
                //  Toast.show(res.message, Toast.SHORT);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const editMenuData = (isBlock) => {
        let newArray = [...bottomData]
        if (isBlock) {
            newArray[0].title = "Unblock"
        }
        else {
            newArray[0].title = "Block"
        }
        setBottomData(newArray)
    }
    const filterData = async (array, u_id) => {
        let newArray = [];
        for (var i = 0; i < array.length; i++) {
            var index = array[i].group_last_two_members.findIndex(obj => obj.gm_user_id === u_id);
            if (index > -1) {
                if (array[i].group_last_two_members[index].gm_role == "Admin") {
                    newArray.push(array[i])
                }
            }
        }
        setGroupData(newArray)
    }
    const getGroupData = async (token, u_id) => {
        await apiCallGetWithToken(apiUrl.get_all_groups, token).then(res => {
            setShowing(false)

            if (res.status == true) {
                if (res.result.length != 0) {
                    filterData(res.result, u_id)
                }
            }
            else {
                //  showMessage("error", "Message", res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    function filterEventData(data, u_id) {
        return data.event_owner_id == u_id;
    }
    const getEventData = async (data) => {
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", data.token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setEventData(res.result.filter((item) => filterEventData(item, data.u_id)))
                //console.log(res.result[0].event_members)
            }
            else {
                setShowing(false)

            }
        })
    }
    const renderPetList = ({ item, index }) => {
        var petImage = item.pet_image != "" && item.pet_image != undefined ? item.pet_image : item.pt_image
        let breed = item.breed.map(a => a.pb_name);
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate(strings.PetProfileTabScreen, { screen: "MyProfile", petData: item, userImage: u_image })} >
                <Image source={{ uri: petImage }} style={styles.petImage} />
                <LinearGradient style={{ width: width, height: 160, position: "absolute" }}
                    colors={Colors.viewGroupContain} />

                <View style={{ margin: 10, bottom: 5, position: 'absolute' }}>
                    <Text style={styles.petName}>
                        {item.pet_name}
                    </Text>
                    <View style={styles.line} />
                    <Text style={styles.scientificName}>
                        {breed.toString() + "  •  " + item.pet_age}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    const addFriend = () => {
        onAddFriend();
    }
    const onAddFriend = async () => {
        setShowing(true)
        var obj = {
            invited_user_id: userData.u_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {
                //  showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getProfileData({ token: token, u_id: userId });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const actionFriend = async (user_id, state, isReceive) => {
        setShowing(true)
        var obj = {
            sender_id: isReceive ? userId : user_id,
            reciever_id: isReceive ? user_id : userId,
            state: state
        }
        await apiCallWithToken(obj, apiUrl.friend_request_action, token).then(res => {
            setTimeout(() => {
                setShowing(false)
                if (res.status == true) {
                    //  showMessage("success", "Message", res.message)
                    getProfileData({ token: token, u_id: userId });
                }
                else
                    showMessage(strings.error, strings.msgTag, res.message)
            }, 1000);

        })
    }
    const acceptRequest = () => {
        setIsRequested(false)
        setIsFriend(true)
    }

    const declineRequest = () => {
        setIsRequested(true)
        setIsFriend(false)
    }
    const onMessageClick = () => {
        props.navigation.navigate(strings.typeMessageScreen, { userData: userData })
    }
    const clickItem = (item) => {
        refSheet.close()
        if (item.key == "blockUser") {
            BlockUser(item.title);
        }
        else if (item.key == "share") {
            var obj = {
                id: userData.u_id,
                title: userData.u_first_name + " " + userData.u_last_name,
                imageUrl: userData.u_image,
                descriptionText: "TapPet User",
                flag: "user"
            }
            setTimeout(() => {
                createDynamicLinks(obj).then(res => {
                    onShare(res)
                    //apiKeyGenerate(res)
                })
            }, 500);
        }
        else if (item.key == "addToGroup") {
            setTimeout(() => {
                if (groupData.length != 0)
                    refSheetGroup.open();
                else
                    showMessage(strings.error, strings.msgTag, "Group data not found!")
            }, 1000);

        }
        else if (item.key == "inviteEvent") {
            setTimeout(() => {
                if (eventdata.length != 0)
                    refSheetEvent.open();
                else
                    showMessage(strings.error, strings.msgTag, "Play date data not found!")
            }, 1000);

        }
        else if (item.key == "delete") {
            removeFriend();
        }
    }
    const removeFriend = async () => {
        setShowing(true)
        var obj = {
            friend_id: userData.u_id
        }
        await apiCallWithToken(obj, apiUrl.remove_friend, token).then(res => {
            if (res.status == true) {
                //showMessage("success", "Message", res.message)
                setTimeout(() => {
                    var obj = { u_id: userId, token: token }
                    getProfileData(obj)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const onShare = async (link) => {

        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType

                } else {

                }

            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    const BlockUser = async (flag) => {
        setShowing(true)
        var obj = {
            user_id: userData.u_id,
            state: flag == "Unblock" ? "Unblock" : "Block"
        }
        await apiCallWithToken(obj, apiUrl.block_or_unblock_user, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                 showMessage("success", "Message", res.message)
                comeChatBlockUser(flag)

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const comeChatBlockUser = (flag) => {
        var usersList = [comeChatUserTag+userData.u_id];
        flag == "Block" ? CometChat.blockUsers(usersList).then(
            list => {
                getProfileData({ token: token, u_id: userId })
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        ) : CometChat.unblockUsers(usersList).then(
            list => {
                getProfileData({ token: token, u_id: userId })
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        );
    }
    const blockMsg = () => {
        showMessage(strings.error, strings.msgTag, "The request you made is not possible Because You have blocked the person so could not ")
    }
    const Add = async (item) => {
        refSheetGroup.close()
        let data = new FormData();
        setShowing(true)
        data.append("group_id", item.group_id)
        data.append("group_members[]", userData.u_id)

        await imageUploadApi(data, apiUrl.add_group_member, token).then(res => {
            if (res.status == true) {
                // showMessage("success", "Message", res.message)
                setShowing(false)
                setTimeout(() => {
                    addGroupMembers(item)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const addGroupMembers = (item) => {
        let GUID = comeGroupTag + item.group_id;
        let membersList = [];
        membersList.push(new CometChat.GroupMember(comeChatUserTag + userData.u_id, CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT))

        CometChat.addMembersToGroup(GUID, membersList, []).then(

            response => {
                // props.navigation.reset({
                //     index: 0,
                //     routes: [{ name: 'TabNavigation' }],
                // });
            },
            error => {
                console.log("Something went wrong", error);
            }
        );
    }
    const addAsEvent = async (item) => {
        refSheetEvent.close();
        let data = new FormData();
        setShowing(true)
        data.append("event_id", item.event_id)
        data.append("event_members[]", userData.u_id);

        await imageUploadApi(data, apiUrl.invite_friend_or_group, token).then(res => {
            if (res.status == true) {
                // showMessage("success", "Message", res.message)
                setTimeout(() => {
                    getProfileData({ u_id: userId, token: token })
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const eventListView = () => {
        return (
            <View style={{ marginBottom: 20 }}>
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheetGroup.close()}>

                </TouchableOpacity>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {eventdata.map((item, index) => {
                        var memberList = item.event_members.map(a => a.member.u_first_name).toString();
                        return (
                            <View style={styles.mainGroupView}>
                                <View>
                                    <Image style={styles.imgGroup} source={{ uri: item.event_image }}></Image>
                                    {/* <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                        {item.group_last_two_members.map((item, index) => {
                                            return (
                                                <Image style={[styles.peopleImg, { marginLeft: index != 0 ? -8 : 0 }]} source={{ uri: item.u_image }}></Image>
                                            )
                                        })}
                                    </View> */}
                                </View>
                                <View style={styles.radioViewGroup}>
                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={styles.radioText}>{item.event_name}</Text>
                                        <Text style={styles.txtInvite}>{memberList}</Text>
                                    </View>
                                    < CommonButton
                                        btnlinearGradient={[styles.btnlinearGradient, { paddingHorizontal: 20 }]}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        viewStyle={styles.verifyBtn}
                                        onPress={() => addAsEvent(item)}
                                        text={"Add"}
                                    />
                                    {/* <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClick(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity> */}
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
    const groupList = () => {
        return (
            <View style={{ marginBottom: 20 }}>
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheetGroup.close()}>

                </TouchableOpacity>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {groupData.map((item, index) => {
                        var memberList = item.group_last_two_members.map(a => a.member.u_first_name).toString();
                        return (
                            <View style={styles.mainGroupView}>
                                <View>
                                    <Image style={styles.imgGroup} source={{ uri: item.group_image }}></Image>
                                    {/* <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                        {item.group_last_two_members.map((item, index) => {
                                            return (
                                                <Image style={[styles.peopleImg, { marginLeft: index != 0 ? -8 : 0 }]} source={{ uri: item.u_image }}></Image>
                                            )
                                        })}
                                    </View> */}
                                </View>
                                <View style={styles.radioViewGroup}>
                                    <View style={{ marginLeft: 20, marginTop: 10 }}>
                                        <Text style={styles.radioText}>{item.group_name}</Text>
                                        <Text style={styles.txtInvite}>{memberList}</Text>
                                    </View>
                                    < CommonButton
                                        btnlinearGradient={[styles.btnlinearGradient, { paddingHorizontal: 20 }]}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        viewStyle={styles.verifyBtn}
                                        onPress={() => Add(item)}
                                        text={"Add"}
                                    />
                                    {/* <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClick(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity> */}
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </View>

        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
          
                <AppHeader
                    navigation={props.navigation}
                    title={'User Profile'}
                    backButton={Images.backIcon}
                    titleText={{ marginLeft: -30 }}
                    // rightIconLeft={Images.notification}
                    rightIcon={Images.menuDot}
                    rightTitlePressed={() => refSheet.open()}
                /> 
            {userData != null &&

                <ScrollView>
                    <>
                        <View style={styles.userProfile}>
                            <Image source={{ uri: u_image }} style={styles.profileIcon} />
                            <View style={styles.userInfo}>
                                <Text style={styles.userName}>
                                    {u_name}
                                </Text>
                                <TouchableOpacity
                                    onPress={() => props.navigation.navigate(strings.friendsScreen, { isUser: false, friend_id: userData.u_id })}
                                    style={styles.userDetails}>
                                    <Friends />
                                    <Text style={styles.inforamtionText}>
                                        {userData?.has_total_mutual_friends_count + " Mutual Friends"}
                                    </Text>
                                </TouchableOpacity>
                                <View style={styles.userDetails}>
                                    <Business />
                                    <Text style={styles.inforamtionText}>
                                        {userData?.has_total_business_count + " Business"}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        {userData.friend_request == 2 && userData.friend_request_sent_by_me == false ?
                            < View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', margin: 20, }}>
                                <View style={{ flex: 0.43 }}>
                                    <CommonButton
                                        btnlinearGradient={styles.btnlinearGradient}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        // viewStyle={{ marginTop: 20 }}
                                        onPress={() => actionFriend(userData.u_id, "1", false)}
                                        text={strings.accept}
                                    />
                                </View>
                                <TouchableOpacity style={{ flex: 0.43 }}
                                    onPress={() => actionFriend(userData.u_id, "3", false)}
                                >
                                    <View style={styles.addFriendButton}>
                                        <Text style={styles.textlabel}>{strings.decline}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flex: 0.10 }}>
                                    <View style={styles.addFriendButton}>
                                        <Image source={Images.downChevron} style={styles.icon} />
                                    </View>
                                </TouchableOpacity>
                            </View> : <></>

                        }
                        {userData.friend_request == 1 ?
                            <View style={{ flexDirection: 'row', flex: 1, marginTop: 15, marginHorizontal: 20, justifyContent: 'space-between' }}>
                                <View style={{ flex: 0.49 }}>
                                    <CommonButton
                                        btnlinearGradient={styles.btnlinearGradient}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        // viewStyle={{ marginTop: 20 }}
                                        onPress={() => blockByMe ? blockMsg() : callClick()}
                                        text={strings.call}
                                        image={Images.call}
                                        imageStyle={styles.addIcon}
                                    />
                                </View>
                                <View style={{ flex: 0.49 }}>
                                    <CommonButton
                                        btnlinearGradient={styles.btnlinearGradient}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        // viewStyle={{ marginTop: 20 }}
                                        onPress={() => blockByMe ? blockMsg() : onMessageClick()}
                                        text={strings.message}
                                        image={Images.group}
                                        imageStyle={styles.addIcon}
                                    />
                                </View>
                            </View>
                            : userData.friend_request == 0 ?
                                <CommonButton
                                    btnlinearGradient={[styles.btnlinearGradient, { marginHorizontal: 20, }]}
                                    colors={Colors.btnBackgroundColor}
                                    btnText={styles.btnText}
                                    viewStyle={{ marginTop: 20 }}
                                    onPress={addFriend}
                                    text={strings.addfriend}
                                    image={Images.addFriend}
                                    imageStyle={styles.addIcon}
                                /> :
                                <CommonButton
                                    btnlinearGradient={[styles.btnlinearGradient, { marginHorizontal: 20, }]}
                                    colors={[Colors.opacityColor, Colors.opacityColor]}
                                    btnText={styles.btnText}
                                    viewStyle={{ marginTop: 20 }}
                                    onPress={() => { }}
                                    text={strings.requestSent}
                                    imageStyle={styles.addIcon}
                                />
                        }

                        {petList.length != 0 && <View style={[styles.groupContainer, { marginTop: 30 }]}>
                            <Text style={styles.groupText}>{"ALL PETS"}</Text>
                        </View>}
                        <FlatList
                            contentContainerStyle={{ paddingBottom: 20 }}
                            style={{ marginVertical: 20 }}
                            data={petList}
                            showsVerticalScrollIndicator={false}
                            // keyExtractor={item => item.id}
                            renderItem={renderPetList}
                        />
                    </>
                    <RBSheet
                        ref={(ref) => refSheet = ref}
                        height={userData.friend_request!=1?150:270}
                        openDuration={250}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {

                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                paddingHorizontal: 25,
                                paddingVertical: 10
                            }
                        }}
                    >
                        <MoreMenu
                            data={userData.friend_request!=1?[bottomData[0],bottomData[1]] :bottomData}
                            clickItem={clickItem}
                        >

                        </MoreMenu>
                    </RBSheet>
                    <RBSheet
                        ref={(ref) => refSheetGroup = ref}
                        height={240}
                        openDuration={250}
                        //                        closeOnDragDown={true}

                        customStyles={{
                            container: {

                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                paddingHorizontal: 25,
                                paddingVertical: 10
                            }
                        }}
                    >
                        {groupList()}
                    </RBSheet>
                    <RBSheet
                        ref={(ref) => refSheetEvent = ref}
                        height={240}
                        openDuration={250}
                        //                        closeOnDragDown={true}

                        customStyles={{
                            container: {

                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                paddingHorizontal: 25,
                                paddingVertical: 10
                            }
                        }}
                    >
                        {eventListView()}
                    </RBSheet>
                </ScrollView>}
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisibleCall}
                    onMessageNotification={() => { }}

                />
            }
        </View >
    )
}


export default UserProfile;