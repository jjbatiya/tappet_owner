import React, { useState, useRef, useEffect } from 'react';
import {
    View, SafeAreaView, Text, KeyboardAvoidingView, Platform, ScrollView,
    Modal, Image, TextInput, TouchableOpacity, FlatList, Slider, PermissionsAndroid
} from 'react-native';
import { styles } from './AddPetStyle';
import LinearGradient from 'react-native-linear-gradient';
import HeaderView from '../../../../Components/HeaderView';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../Components/CommonButton';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { strings } from '../../../../Resources/Strings';
import { validationMsg } from '../../../../Resources/ValidationMsg';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images'
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { PieChart } from 'react-native-svg-charts'
import { Fonts } from '../../../../Resources/Fonts';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import { LogoIcon } from '../../../../utils/svg/LogoIcon';
import { CalenderIcon } from '../../../../utils/svg/CalenderIcon';
import { UploadImageIcon } from '../../../../utils/svg/UploadImageIcon';
import { getApiCall, getUserData, apiCallWithTokenAddPet } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';

const moment = require('moment')
import { CometChat } from '@cometchat-pro/react-native-chat';
import ImagePickerOption from '../../../../Components/ImagePickerOption';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';


const AddPet = (props) => {

    const [petObj] = useState(props.route.params?.petData)
    const [step, setStep] = useState(1)
    const [seletedItem, setItem] = useState(null)
    const [isPetName, setIsPetName] = useState(false)
    const [date, setDate] = useState(petObj == undefined ? new Date() : new Date(petObj.pet_dob));
    const [todayDate] = useState(new Date());
    const [petData, setPetData] = useState([])
    const [showDate, setShowDate] = useState(false);
    const [age, setAge] = useState(petObj == undefined ? "" : petObj.pet_age)
    const [imagePath, setImagePath] = useState(petObj == undefined ? "" : (petObj.pet_image == "" || petObj.pet_image == undefined) ? petObj.pt_image : petObj.pet_image)
    const [isVisible, setVisible] = useState(false)
    const [breedData, setBreedData] = useState([])
    const [pet_breed_ids, setBreedIds] = useState("")
    const [pet_breed_percentage, setBreedPer] = useState("")
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [u_id, setUid] = useState("")
    const [petName, setPetName] = useState(petObj == undefined ? "" : petObj.pet_name)
    const [selectRadio, onSelectRadio] = useState(petObj == undefined ? "" : petObj.pet_gender)
    const [friendlyRadio, setFriendly] = useState(petObj == undefined ? "" : petObj.pet_is_friendly)
    const [isRBShowing, setRBShowing] = useState()
    const [arrayData, setArrayData] = useState([
        {
            id: 1,
            placeHolder: "Add Breed 1",
            labelText: "Primary Breed",
            labelColor: "pink",
            percentage: 0,
            data: [],
            pb_name: "",
            pb_id: ""
        }
    ])
    const [percentageData, setPercentageData] = useState(petObj == undefined ? [] : petObj.pet_breed_percentage.split(','))
    const [petWeight, setPetWeight] = useState(petObj == undefined ? 0 : petObj.pet_size == "" ? 0 : parseInt(petObj.pet_size))
    const [imageData, setImageData] = useState(petObj != undefined ? { type: "already" } : null)
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState()
    useEffect(() => {
        let newArray = [];
        for (var i = 0; i < arrayData.length; i++) {
            newArray.push(arrayData[i].percentage)
        }

        getUserData().then(res => {
            setToken(res.token)
            setUid(res.u_id)
        })
        setPercentageData(newArray)
        getPetTypes()
        getPetBreeds();
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getPetTypes = async () => {
        await getApiCall(apiUrl.get_pet_types).then(res => {
            if (res.status) {
                setPetData(res.result)
                if (petObj != undefined) {
                    var obj = setObject(res.result);
                    setItem(obj[0])
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    function setObject(data) {
        return data.filter(item => {
            return item.pt_id === petObj.pt_id
        })
    };
    const getPetBreeds = async () => {
        await getApiCall(apiUrl.get_pet_breeds).then(res => {
            setShowing(false)
            if (res.status) {
                setBreedData(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const onChangeDate = async (selectedDate) => {
        setAge("")
        const currentDate = selectedDate || date;
        setShowDate(false);
        setDate(currentDate);

        var a = moment(todayDate);
        var b = moment(currentDate);

        var years = a.diff(b, 'year');
        b.add(years, 'years');

        var months = a.diff(b, 'months');
        b.add(months, 'months');

        var days = a.diff(b, 'days');
        var str = "";
        if (years > 0)
            str = years > 1 ? years + ' Years ' : years + ' Year '
        if (months > 0)
            str += months > 1 ? months + ' Months ' : months + ' Month '
        if (days > 0)
            str += days > 1 ? days + ' Days' : days + ' Day'
        setAge(str.trim())
    };

    function monthDiff(d2, d1) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }
    const hideShowDatePicker = () => {
        setShowDate(false);
    };
    const onNext = () => {
        if (step != 4) {
            if (seletedItem == null && step == 1) {
                showMessage(strings.error, strings.msgTag, validationMsg.petType)
            }
            else if (petName.toString().trim() == "" && step == 2) {
                showMessage(strings.error, strings.msgTag, validationMsg.petName)
            }
            else if (selectRadio.trim() == "" && step == 2) {
                showMessage(strings.error, strings.msgTag, validationMsg.petGender)
            }
            else if (age == "" && step == 2)
                showMessage(strings.error, strings.msgTag, validationMsg.petAge)
            else if (step == 3) {
                var flag = true;
                for (var i = 0; i < arrayData.length; i++) {
                    if (arrayData[i].pb_id) { }
                    else {
                        flag = false;
                        break;
                    }
                }
                let result = arrayData.map(a => a.pb_id);

                setBreedIds(result.toString())
                let result1 = arrayData.map(a => a.percentage);
                setBreedPer(result1.toString())
                var sum = 0;
                result1.forEach(function (obj) {
                    sum += parseInt(obj);
                });
                if (sum != 100) {
                    showMessage("error", "Message", validationMsg.petBreedPercentage)
                }

                else if (result.toString().trim() == "" || flag == false) {
                    showMessage(strings.error, strings.msgTag, validationMsg.petBreed)
                }
                else {
                    setStep(step + 1)
                }
            }
            else {
                if (petObj != undefined) {
                    let newArray = [];
                    var myArray = petObj.pet_breed_percentage.split(',');
                    for (var i = 0; i < petObj?.breed?.length; i++) {
                        var ColorCode = "";
                        if (i == 0)
                            ColorCode = "pink";
                        else if (i == 1)
                            ColorCode = "yellow";
                        else if (i == 2)
                            ColorCode = "cyan";
                        else
                            ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';

                        var obj = {
                            id: parseInt(i + 1),
                            placeHolder: "Add Breed " + parseInt(i + 1),
                            labelText: "Primary Breed",
                            labelColor: ColorCode,
                            percentage: myArray[i],
                            data: [],
                            pb_name: petObj.breed[i].pb_name,
                            pb_id: petObj.breed[i].pb_id
                        }
                        newArray.push(obj)
                    }
                    setArrayData(newArray)
                    setPercentageData(petObj.pet_breed_percentage.split(','))
                }
                setStep(step + 1)
            }
        }
        else {
            if (imageData == null)
                showMessage(strings.error, strings.msgTag, validationMsg.petImage)
            else {
                addPetApi();
            }
            // setVisible(true)
        }
    }
    const addPetApi = async () => {
        var url = apiUrl.add_pet

        let data = new FormData();
        if (imageData.type != "already")
            data.append("pet_image", { type: imageData.type, uri: imageData.uri, name: imageData.fileName });
        data.append("pet_name", petName);
        data.append("pet_gender", selectRadio);
        data.append("pet_dob", date.getFullYear() + "-" + parseInt(date.getMonth() + 1) + "-" + date.getDate());
        data.append("pet_age", age);
        data.append("pet_type_id", seletedItem.pt_id);
        data.append("pet_breed_ids", pet_breed_ids);
        data.append("pet_breed_percentage", pet_breed_percentage);
        data.append("pet_size", petWeight);
        data.append("pet_is_friendly", friendlyRadio);
        data.append("pet_name", petName);
        if (petObj != undefined) {
            data.append("pet_id", petObj.pet_id);
            url = apiUrl.edit_pet

        }
        setShowing(true)
        await apiCallWithTokenAddPet(data, url, token).then(res => {
            if (res.status) {
                setTimeout(() => {
                    setShowing(false)
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.TabNavigation }],
                    });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const backClick = () => {
        if (petObj == undefined) {
            setPercentageData([])

            setArrayData([{
                id: 1,
                placeHolder: "Add Breed 1",
                labelText: "Primary Breed",
                labelColor: "#FFB91B",
                percentage: '',
                data: [],
                pb_name: ''
            }])
        }

        if (step == 1)
            props.navigation.goBack(null);
        else {
            setStep(step - 1)
        }
    }
    const onBlur = () => {
        setIsPetName(!isPetName)
    }
    const renderView = ({ item, index }) => {
        return (
            <TouchableOpacity style={{
                marginHorizontal: responsiveHeight(2.2), flex: 1,
                borderWidth: seletedItem == item ? 1 : 0,
                borderColor: seletedItem == item ? Colors.primaryViolet : Colors.white,
                borderRadius: 4, marginVertical: responsiveHeight(1),
            }}
                onPress={() => setItem(item)}
            >


                <Image style={{ width: '100%', borderRadius: 4, height: 170, flex: 1, }}
                    source={{ uri: item.pt_image }} ></Image>
                <LinearGradient style={{
                    width: '100%', height: 52,
                    position: "absolute", bottom: 0,
                }}
                    colors={Colors.viewGroupContain} />

                <Text style={styles.txtType}>{item.pt_name}</Text>
                {item == seletedItem &&
                    <TouchableOpacity
                        style={styles.radioCircle}
                    >
                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                            <View style={styles.viewCheckBox} />
                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                        </View>

                    </TouchableOpacity>
                }
            </TouchableOpacity>
        )
    }
    const firstStep = () => {
        return (
            <FlatList
                data={petData}
                style={{ marginTop: 10 }}
                showsVerticalScrollIndicator={false}
                renderItem={renderView}
            >
            </FlatList>
        )
    }

    String.prototype.replaceAt = function (index, replacement) {
        return this.substr(0, index) + replacement + this.substr(index + replacement.length);
    }
    const secondStep = () => {
        return (
            <View style={{ margin: 17 }}>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    value={petName}
                    onChangeText={(text) => setPetName(text)}
                    style={isPetName ? styles.focusforminput : styles.formInput}
                    onFocus={() => onBlur()}
                    onBlur={() => onBlur()}
                >{strings.enterPetName}</FloatingLabel>
                <Text style={styles.txtGender}>Gender*</Text>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <TouchableOpacity
                        onPress={() => onSelectRadio("Male")}
                        style={styles.radioButton}>
                        <View style={[styles.radioView, { borderColor: selectRadio == "Male" ? Colors.themeColor : Colors.white }]} >
                            {selectRadio == "Male" && <View style={styles.selectedRadioView} />}
                        </View>
                        <Text style={[styles.radioText, { marginLeft: 10, marginRight: 40 }]}>Male</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => onSelectRadio("Female")}
                        style={styles.radioButton}>
                        <View style={[styles.radioView, { borderColor: selectRadio == "Female" ? Colors.themeColor : Colors.white }]} >
                            {selectRadio == "Female" && <View style={styles.selectedRadioView} />}
                        </View>
                        <Text style={[styles.radioText, { marginLeft: 10 }]}>Female</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.txtGender}>Date of Birth*</Text>
                <TouchableOpacity onPress={() => setShowDate(true)} style={styles.dateClickView}>
                    <TextInput
                        style={styles.input}
                        placeholder="MM/DD/YYYY"
                        editable={false}
                        value={parseInt(date.getMonth() + 1) + "/" + parseInt(date.getDate()) + "/" + date.getFullYear()}
                        underlineColorAndroid="transparent"
                    />
                    <View style={{ alignSelf: "center" }}>
                        <CalenderIcon width={18} height={18} />
                    </View>
                </TouchableOpacity>
                <View style={styles.horizontalLine}></View>
                <Text style={styles.txtAdopation}>{strings.txtAdopation}</Text>
                <Text style={styles.txtGender}>Age*</Text>
                <TextInput
                    style={[styles.input, { marginVertical: 5, marginLeft: -3 }]}
                    placeholderTextColor={Colors.white}
                    value={age}
                    editable={false}
                />
                <View style={styles.horizontalLine}></View>
                <Text style={styles.txtGender}>{"Weight (" + petWeight + " pound)"}</Text>
                <Slider
                    step={1}
                    minimumValue={0}
                    maximumValue={350}
                    value={petWeight}
                    onValueChange={val => setPetWeight(val)}
                    onSlidingComplete={val => console.log(val)}
                    thumbTintColor={Colors.primaryViolet}
                    minimumTrackTintColor={Colors.primaryViolet}
                />
                <Text style={styles.txtGender}>Friendly with other pets*</Text>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <TouchableOpacity
                        onPress={() => setFriendly("Yes")}
                        style={styles.radioButton}>
                        <View style={[styles.radioView, { borderColor: friendlyRadio == "Yes" ? Colors.themeColor : Colors.white }]} >
                            {friendlyRadio == "Yes" && <View style={styles.selectedRadioView} />}
                        </View>
                        <Text style={[styles.radioText, { marginLeft: 10, marginRight: 40 }]}>Yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setFriendly("No")}
                        style={styles.radioButton}>
                        <View style={[styles.radioView, { borderColor: friendlyRadio == "No" ? Colors.themeColor : Colors.white }]} >
                            {friendlyRadio == "No" && <View style={styles.selectedRadioView} />}
                        </View>
                        <Text style={[styles.radioText, { marginLeft: 10 }]}>No</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    const chooseFile = () => {
        // refSheet.open();
        setRBShowing(!isRBShowing)

    };
    const emailModal = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={styles.emailIconStyle} source={Images.emailIcon}></Image>
                    <Text style={styles.txtlblCheckEmail}>{strings.addCollar + "?"}</Text>
                    <Text style={styles.txtlblRecoverStr}>{strings.recoverTxt}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219 }}
                        onPress={() => setVisible(false)}
                        text={"Yes, " + strings.addCollar}
                    />
                    <Text onPress={() => setVisible(false)} style={styles.txtCancel}>{strings.doItLater}</Text>

                </View>
            </Modal>
        )
    }
    const addMore = async () => {

        const randomColor = () => ('#' + ((Math.random() * 0xffffff) << 0).toString(16) + '000000').slice(0, 7)
        let duplicateArray = [...arrayData];
        const obj = {
            id: arrayData.length,
            placeHolder: "Add Breed " + (arrayData.length + 1),
            labelText: "Breed " + (arrayData.length + 1),
            labelColor: arrayData.length == 1 ? "yellow" : arrayData.length == 2 ? "cyan" : randomColor(),
            percentage: 0,
            data: [],
            name: ""
        }
        duplicateArray.push(obj)
        await setArrayData(duplicateArray)
        await setPercentage();
    }
    const setPercentage = () => {
        let newArray = [];
        for (var i = 0; i < arrayData.length; i++) {
            newArray.push(arrayData[i].percentage)
        }
        setPercentageData(newArray)
    }
    const setPercentageFunc = (array) => {
        let newArray = [];
        for (var i = 0; i < array.length; i++) {
            newArray.push(array[i].percentage)
        }
        setPercentageData(newArray)
    }
    const thirdStep = () => {
        const pieData = percentageData
            .filter((value) => parseInt(value) > 0)
            .map((value, index) => ({
                value,
                svg: {
                    fill: arrayData[index]?.labelColor,
                    onPress: () => console.log('press', index),
                },
                key: `pie-${index}`,
            }))

        return (
            <View>
                <View style={styles.viewThirdStep}>
                    {percentageData.length != 0 && percentageData[0] > 0 ?
                        <PieChart padAngle={0} innerRadius={'90%'} style={{ height: 165, width: 165 }} data={pieData} /> :
                        <View style={styles.emptyRound} />
                    }
                    <View style={{ position: "absolute" }}>
                        <LogoIcon width={48} height={48} opacity={0.3} />
                    </View>

                </View>
                <View style={{ marginTop: responsiveHeight(5), marginHorizontal: responsiveWidth(4) }}>
                    {arrayData.map((data, index) => {
                        return (
                            viewBreed(data, index)
                        )
                    })
                    }
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ color: Colors.primaryViolet, fontSize: 18, marginRight: 5 }}>+</Text>
                        <Text
                            onPress={() => addMore()}
                            style={styles.txtaddMore}> Add more breed</Text>
                    </View>
                </View>

            </View>
        )
    }
    const removeItem = async (index) => {
        console.log("index", index)
        let duplicateArray = [...arrayData];
        duplicateArray.splice(index, 1)
        await setPercentageFunc(duplicateArray)
        await setArrayData(duplicateArray)
    }
    const changePercentage = (text, index) => {
        text = text.replace("%", "")
        let newArray = [...arrayData]
        newArray[index].percentage = text

        setArrayData(newArray)
        setPercentage();

    }
    const onBreedNameChnage = (text, index) => {
        let newArray = [...arrayData];
        if (text.trim() != '') {

            const newData = breedData.filter(item => {
                const itemData = item.pb_name.toUpperCase()
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1
            });
            newArray[index].pb_name = text
            newArray[index].data = newData;
            setArrayData(newArray)
        }
        else {
            newArray[index].pb_name = text
            newArray[index].data = [];
            setArrayData(newArray)

        }

    }
    const downChevron = (index) => {
        let newArray = [...arrayData];
        newArray[index].name = ''
        if (newArray[index].data.length == 0) {
            newArray[index].data = breedData;
            setArrayData(newArray)
        }
        else {
            newArray[index].data = [];
            setArrayData(newArray)
        }
    }
    const clickItem = (item, index) => {
        let newArray = [...arrayData];
        newArray[index].pb_name = item.pb_name
        newArray[index].pb_id = item.pb_id
        newArray[index].data = []
        setArrayData(newArray)
    }
    const viewBreed = (data, index) => {
        return (
            <View style={{ marginBottom: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.labelText, { color: data.labelColor, }]}>{data.labelText}</Text>
                    {index != 0 &&
                        <TouchableOpacity onPress={() => removeItem(index)}>
                            <Image style={{ height: 20, width: 20, tintColor: "white" }} source={Images.closeIcon}></Image>
                        </TouchableOpacity>
                    }
                </View>
                <View style={{ flex: 1, flexDirection: "row", marginTop: responsiveHeight(1) }}>
                    <TextInput
                        style={styles.inputBox}
                        onChangeText={(text) => onBreedNameChnage(text, index)}
                        placeholder={data.placeHolder}
                        value={data.pb_name}
                        placeholderTextColor={Colors.opacityColor2}
                    />

                    <TouchableOpacity style={{ flex: 0.1, alignSelf: "center", }} onPress={() => downChevron(index)}>
                        <Image style={{ height: 10, width: 10, tintColor: Colors.white }} source={Images.downChevron}></Image>
                    </TouchableOpacity>
                    <View style={styles.viewVertical}></View>
                    <View style={{ flexDirection: "row", flex: 0.3 }}>
                        <TextInput
                            style={styles.inputPercentage}
                            placeholder={strings.percent}
                            keyboardType={"numeric"}
                            maxLength={3}
                            value={data.percentage}
                            onChangeText={(text) => changePercentage(text, index)}
                            placeholderTextColor={Colors.opacityColor2}

                        />
                        <Text style={styles.percenTage}>%</Text>
                    </View>
                </View>
                <View style={[styles.horizontalLine, { marginTop: 5 }]}></View>
                <FlatList
                    data={data.data}
                    style={{ backgroundColor: Colors.theme, marginTop: 6, height: 35 * (data.data.length) }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => clickItem(item, index)}
                        >
                            <Text style={{
                                fontSize: 14, lineHeight: 18.23, fontWeight: "400",
                                color: Colors.white, fontFamily: Fonts.DMSansRegular,
                                paddingHorizontal: responsiveWidth(3),
                                paddingVertical: responsiveHeight(1)
                            }}>{item.pb_name}</Text>
                        </TouchableOpacity>
                    )}
                //renderItem={renderBreed}
                >
                </FlatList>
            </View>
        )
    }
    const renderBreed = ({ item, index }) => {
        return (
            <TouchableOpacity>
                <Text style={{
                    fontSize: 14, lineHeight: 18.23, fontWeight: "400",
                    color: Colors.white, fontFamily: Fonts.DMSansRegular,
                    paddingHorizontal: responsiveWidth(3),
                    paddingVertical: responsiveHeight(1)
                }}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    const fourthStep = () => {
        return (
            <View style={styles.roundView}>
                {imagePath == "" ?
                    <TouchableOpacity onPress={() => chooseFile()}>
                        <View style={{ alignSelf: "center" }}>
                            <UploadImageIcon />
                        </View>
                        <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                    </TouchableOpacity> :
                    <View style={{ flex: 1 }}>
                        <Image style={styles.profileStyle} source={{ uri: imagePath }}></Image>
                        <TouchableOpacity style={styles.editView} onPress={() => chooseFile()}>
                            <Image
                                style={styles.imgEditStyle}
                                source={Images.editIcon}
                            />

                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }

    const clickItemMenu = (obj) => {
        setImageData(obj)
        setImagePath(obj?.uri)
    }
    return (
        <View style={{ flex: 1 }}>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : null}>
                    <SafeAreaView style={{ marginTop: 30 }} >
                        <HeaderView onPress={backClick} onSkip={onNext} textSkip={step != 4 ? strings.next : strings.save} isPhoto={true}
                            text={step == 1 ? strings.selectPetType : step == 2 ? strings.addPet :
                                step == 3 ? strings.addBreed : strings.uploadPicture}
                            isClose={step == 1 ? true : false}
                        />
                        <Text style={styles.txtlblStep}>{"Step " + step + " of 4"}</Text>
                        <View style={styles.mainView}>
                            <LinearGradient
                                colors={Colors.btnBackgroundColor}
                                style={step == 1 ? { height: 4, flex: 0.25 } : step == 2 ? { height: 4, flex: 0.50 } : step == 3 ? { height: 4, flex: 0.75 } : { height: 4, flex: 0.92 }}
                            />
                            <View style={step == 1 ? styles.step1 : step == 2 ? styles.step2 : step == 3 ? styles.step3 : styles.step4} />
                        </View>
                        <View>

                        </View>
                        <ScrollView keyboardShouldPersistTaps='handled' style={{ marginBottom: 170 }} showsVerticalScrollIndicator={false}>
                            {step == 1 ? firstStep() : step == 2 ? secondStep() : step == 3 ? thirdStep() : fourthStep()}
                            {showDate && (
                                <DateTimePickerModal
                                    isVisible={showDate}
                                    mode="date"
                                    date={date}
                                    maximumDate={new Date()}
                                    onConfirm={onChangeDate}
                                    onCancel={hideShowDatePicker}
                                />
                            )}
                            {emailModal()}
                        </ScrollView>
                    </SafeAreaView>
                </KeyboardAvoidingView>
            </LinearGradient>

            <ImagePickerOption
                isRBShowing={isRBShowing}
                clickItemMenu={clickItemMenu}
            />
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisibleCall}
                    onMessageNotification={()=>{}}

                />
            }
        </View>
    )
}
export default AddPet;
