import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    step1: { backgroundColor: Colors.white, height: 4, flex: 0.65 },
    step2: { backgroundColor: Colors.white, height: 4, flex: 0.42 },
    step3: { backgroundColor: Colors.white, height: 4, flex: 0.17 },
    step4: {},
    mainView: {
        marginHorizontal: responsiveWidth(5), marginVertical: responsiveHeight(1),
        flex: 1, width: "100%", flexDirection: "row"
    },
    txtlblStep: {
        alignSelf: "center", fontSize: 12, lineHeight: 15.62,
        fontWeight: "400", color: Colors.white, marginRight: responsiveWidth(8),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    //render view step 1
    txtType: {
        position: "absolute", bottom: 7, textAlign: "center", color: Colors.white,
        fontSize: 22, fontWeight: "400", lineHeight: 27.5, letterSpacing: 0.4, alignSelf: "center",
        fontFamily: Fonts.LobsterTwoRegular
    },
    radioCircle: {
        height: 18,
        width: 18,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        margin: responsiveHeight(0.8),
        position: "absolute",
        top: 0,
        right: 0
    },
    viewCheckBox: {
        height: 18,
        width: 18,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primaryViolet

    },
    tickImage: { height: 6, width: 8, position: "absolute", alignSelf: "center" },
    //second step
    labelInput: {
        fontSize: 12,
        color: Colors.white,
        opacity: 0.3,
        marginLeft: -8,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginTop: responsiveHeight(1),

    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontWeight: "400",
        color: "white",
        fontSize: 16,
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3,
        marginLeft:-3
        //height:40
    },
    radioButton: { flexDirection: 'row', marginVertical: 10, alignItems: 'center' },
    radioView: { justifyContent: 'center', alignItems: 'center', width: 20, height: 20, borderRadius: 20, borderWidth: 1, },
    selectedRadioView: { width: 12, height: 12, borderRadius: 12, borderWidth: 1, backgroundColor: Colors.themeColor, },
    radioText: { fontWeight: "400", fontSize: 16, lineHeight: 22, color: Colors.white, fontFamily: Fonts.DMSansRegular },
    txtGender: {
        fontSize: 12, lineHeight: 15.62, color: Colors.white,
        marginTop: responsiveHeight(2), fontFamily: Fonts.DMSansRegular, opacity: 0.6
    },
    dateClickView: { flexDirection: "row", justifyContent: "space-between", paddingVertical: responsiveHeight(1) },
    calender: { resizeMode: "contain", height: 20, width: 20, tintColor: Colors.white },
    horizontalLine: { height: 1, backgroundColor: Colors.white },
    txtAdopation: {
        fontSize: 10, lineHeight: 14, color: Colors.white,
        marginTop: responsiveHeight(1), fontFamily: Fonts.DMSansRegular, opacity: 0.4
    },
    //fourth step
    roundView: {
        height: 195, width: 195, borderWidth: 1, borderColor: Colors.opacityColor2,
        alignSelf: "center",
        borderRadius: 114, marginTop: responsiveHeight(2),
        justifyContent: "center",
    },
    imgUploadStyle: {
        height: 33.33, width: 33.33,
        alignSelf: "center", borderRadius: 7
    },
    profileStyle: {
        height: 195, width: 195,
        alignSelf: "center", borderRadius: 7,
        borderRadius: 114
    },
    txtUploadText: {
        color: Colors.white, alignSelf: "center",
        lineHeight: 18.23, fontSize: 14, fontWeight: "400",
        marginTop: responsiveHeight(1), fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    editView: {
        width: 40, height: 40, backgroundColor: Colors.dialogBackground,
        position: 'relative', bottom: 50, left: 150,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: '#0C0024'
        ,
    },
    imgEditStyle: { height: 12, width: 12, tintColor: "white", alignSelf: "center" },
    //modal view
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },

    emailIconStyle: { marginVertical: responsiveHeight(1) },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    txtlblRecoverStr: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },

    //third step Style
    viewThirdStep: { alignItems: "center", justifyContent: "center", height: 160, flex: 1, marginTop: 20 },
    emptyRound: { height: 160, width: 160, opacity: 0.2, borderRadius: 80, borderWidth: 8, borderColor: Colors.white },
    logoApp: { height: 40, width: 40, position: "absolute" },
    txtaddMore: {
        fontWeight: "500", fontSize: 12, lineHeight: 15.62, color: Colors.primaryViolet,
        fontFamily: Fonts.DMSansRegular, marginVertical: responsiveHeight(0.5)
    },
    //viewBreed
    labelText: {
        fontSize: 12, lineHeight: 15.62, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular
    },
    inputBox: { flex: 0.6, color: Colors.white, fontFamily: Fonts.DMSansRegular },
    viewVertical: { opacity: 0.2, backgroundColor: Colors.white, width: 1, height: 20, alignSelf: "center" },
    inputPercentage: {
        flex: 0.7, color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        textAlign:"center"
    },
    percenTage: {
        flex: 0.3, color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        alignSelf:"center"
    }

})