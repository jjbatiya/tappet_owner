import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './PastEventStyle';
import { Images } from '../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, getGroupId, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
const { width } = Dimensions.get("window")
import Toast from 'react-native-toast-message';
import { strings } from '../../../../Resources/Strings';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';

const PastEvent = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [eventData, setEventData] = useState([])
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [u_id, setUserId] = useState()

    useEffect(() => {
        if (props.index == 1) {
            getUserData().then(res => {
                setUserId(res.u_id)
                setEventData([])
                setShowing(true)
                getGroupId().then(group_id => {
                    if (group_id == "" || group_id == undefined)
                        getEventData(res)
                    else
                        getGroupWiseData(group_id, res)

                })
            })
        }

    }, [props.index]);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getEventData = async (res) => {
        console.log(res.token)
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=past", res.token).then(res => {
            if (res.status == true) {
                console.log(res.result)
                if (res.result.length != 0)
                    setEventData(res.result)
                else
                    showMessage(strings.error, strings.msgTag, res.message)

                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const getGroupWiseData = async (group_id, res) => {
        await apiCallGetWithToken(apiUrl.get_all_events_by_group_id + "?event_type=past&group_id=" + group_id, res.token).then(res => {
            if (res.status == true) {
                if (res.result.length != 0)
                    setEventData(res.result)
                else
                    showMessage(strings.error, strings.msgTag, res.message)

                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderItem = ({ item, index }) => {
        var day = new Date(item.event_start_date).getDate();
        var month = new Date(item.event_start_date).getMonth();
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate(strings.EventDetailPastScreen, { case: u_id == item.added_by.u_id ? "case3" : "case1", item: item })} style={styles.renderStyle}>
                <Image source={{ uri: item.event_image }} style={styles.imgStyleMain}></Image>
                <View style={styles.upperView}>
                    <View style={styles.dateView}>
                        <View style={{ flexDirection: "row" }}>
                        </View>
                        <View style={styles.dateViewTxt}>
                            <Text style={styles.txtDate}>{day}</Text>
                            <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                        </View>
                    </View>
                </View>

                <LinearGradient style={{
                    position: "absolute",
                    height: responsiveHeight(19),
                    width: width, bottom: 0,
                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomView}>
                    <Text style={styles.txtTitle}>{item.event_name}</Text>
                    {item.images.length != 0 &&

                        <View>
                            <View style={styles.bottomInnerView}></View>
                            <View style={styles.imageView}>
                                {
                                    item.images.map((data, index) => {
                                        return (
                                            index < 2 ?
                                                <TouchableOpacity onPress={() => props.navigation.navigate(strings.viewImagesScreen, { images: item.images, flag: "event" })}>
                                                    <Image style={styles.imgStyle} source={{ uri: data.event_image_image }}></Image>
                                                </TouchableOpacity> : <></>

                                        )
                                    })
                                }
                                {item.images.length > 2 &&
                                    <TouchableOpacity style={styles.moreView}>
                                        <Image style={styles.imgStyleAbs} source={Images.dogIcon5}></Image>
                                        <Text style={styles.txtMore}>{"+" + parseInt(item.images.length - 2)}</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                    }
                </View>


            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={styles.contentView}>
                    <FlatList
                        data={eventData}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                    />



                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {/* {Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={false}
                />
            } */}
        </View>
    )
}
export default PastEvent