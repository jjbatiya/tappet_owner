import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 5,
        // marginHorizontal: responsiveWidth(4),
        alignItems: "center",
        justifyContent: "center",
        width: 96
    },
    recommendedText: {
        fontWeight: "700",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    contentView: {
        flex: 1,
        marginHorizontal: responsiveWidth(4),
        marginVertical: responsiveHeight(2)
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    renderStyle: { height: 202, marginVertical: responsiveHeight(1), borderRadius: 4 },
    imgStyle: { height: 202, width: responsiveWidth(100) },
    txtName: { fontSize: 14, fontWeight: "400", lineHeight: 18.23, alignSelf: "center", color: Colors.white, marginTop: responsiveHeight(1.8) },
    txtDetail: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, alignSelf: "center", color: Colors.white, textAlign: "center", opacity: 0.6 },
    btnView: { width: "100%", height: 40, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(2) },

    //flatlist contentView
    upperView: { position: "absolute", margin: 10, width: "95%" },
    dateView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    dateViewTxt: {
        height: 37, width: 36, backgroundColor: Colors.white,
        alignSelf: "flex-end", borderRadius: 2, fontFamily: Fonts.DMSansRegular
    },
    txtDate: { fontSize: 16, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular },
    txtMonth: { fontSize: 10, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular },
    bottomView: { position: "absolute", bottom: 0, width: responsiveWidth(100) },
    txtTitle: {
        fontWeight: "400", fontSize: 22,
        lineHeight: 27.5, color: Colors.white, margin: 10, fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing:0.4
    },
    bottomInnerView: { height: 56, backgroundColor: Colors.black, opacity: 0.4, width: responsiveWidth(100) },
    imageView: { position: "absolute", height: 56, flexDirection: "row", margin: 10 },
    imgStyle: { width: 32, height: 32, borderRadius: 2, marginRight: 6 },
    moreView: { height: 32, width: 32, alignItems: "center", justifyContent: "center" },
    imgStyleAbs: { backgroundColor: Colors.black, opacity: 0.6, width: 32, height: 32, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700",
        fontSize: 12, lineHeight: 16, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    imgStyleMain: { height: 202, width: responsiveWidth(100),borderRadius:4 },


})