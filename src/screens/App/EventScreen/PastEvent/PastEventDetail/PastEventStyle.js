import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    //person view
    renderSubView: { height: 48, marginVertical: 20, flexDirection: "row", alignItems: "center", justifyContent: 'space-between', marginHorizontal: responsiveWidth(5) },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 40, width: 40, resizeMode: "contain", borderRadius: 25 },
    borderStyle: { height: 28, width: 96, borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white },
    btnText1: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        position: "absolute",
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular
    },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400",
        lineHeight: 15.62, color: Colors.white, opacity: 0.6, fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 96,
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    btnView: { alignItems: "center", justifyContent: "center" },

    //view Line
    viewLine: {
        height: 1,
        backgroundColor: Colors.white,
        marginHorizontal: 20,
        marginVertical: 5,
        opacity: 0.15
    },
    //dateView
    txtDateLbl: {
        fontWeight: "700", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, textTransform: "uppercase",
        marginVertical: responsiveHeight(1.5), fontFamily: Fonts.DMSansRegular
    },
    txtDate: {
        fontWeight: "400", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },

    //location view
    txtgetLocation: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.primaryViolet, marginVertical: 5, fontFamily: Fonts.DMSansRegular
    },
    imgMap: { height: 120, width: "100%", borderRadius: 4, marginVertical: responsiveHeight(1) },

    //detailView
    txtDetailData: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, opacity: 0.6,
        marginHorizontal: responsiveWidth(3),
        fontFamily: Fonts.DMSansRegular

    },
    viewLineVertical: { width: 2, backgroundColor: Colors.primaryViolet },

    //peole going
    roundView: {
        height: 32, width: 32, backgroundColor: Colors.theme, alignItems: "center",
        justifyContent: "center", borderRadius: 20,

    },
    imgView: { height: 32, width: 32, borderRadius: 20 },
    txtMore: {
        fontSize: 9,
        fontWeight: "700",
        lineHeight: 11.72,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    //pending view
    starView: {
        position: "absolute", height: 16, width: 16,
        backgroundColor: '#3D3350', bottom: 0, borderWidth: 1,
        borderRadius: 10, borderColor: '#0C0024', right: 0, left: 20
    },
    starIcon: {
        position: "absolute", height: 8, width: 8,
        bottom: 4, right: 0,
        tintColor: Colors.white
    },
    //photo view
    txtNoPicture: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, opacity: 0.6,
        fontFamily: Fonts.DMSansRegular
    },
    mainViewStep3: {
        height: 154, borderColor: Colors.white,
        alignSelf: "center",
        borderWidth: 1,
        marginTop: responsiveHeight(2),
        justifyContent: "center",
        width: "100%",
        opacity: 0.2
    },
    imgUploadStyle: {
        height: 33.33, width: 33.33,
        alignSelf: "center", borderRadius: 7
    },
    txtUploadText: {
        color: Colors.white, alignSelf: "center",
        lineHeight: 18.23, fontSize: 14, fontWeight: "400", marginTop: responsiveHeight(1),
        fontFamily: Fonts.DMSansRegular, opacity: 0.6
    },
    profileStyle: {
        height: 154,
        alignSelf: "center",
        width: '100%'
    },
    editView: {
        width: 40, height: 40, backgroundColor: Colors.dialogBackground,
        position: 'absolute', bottom: -20, right: 10,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: '#0C0024'
        ,
    },
    imgEditStyle: { height: 12, width: 12, tintColor: "white", alignSelf: "center" },

    subView: { position: "absolute", width: '100%', height: 154, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(2) },
    rightArrow: { height: 10, width: 10, tintColor: Colors.primaryViolet, alignSelf: "center", marginLeft: 7 },
    //border View
    borderViewStyle: { height: 28, borderRadius: 4, width: 100, borderColor: Colors.white, borderWidth: 1, opacity: 0.2 },
    selectBorderView: { height: 28, borderRadius: 4, width: 100, backgroundColor: Colors.white, opacity: 0.2 },
    txtBottom: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, position: "absolute", fontFamily: Fonts.DMSansRegular
    },
    viewTouchable: { alignItems: "center", justifyContent: "center" },
    bottomView: {
        height: 80, backgroundColor: Colors.theme,
        justifyContent: "space-between",
        flexDirection: "row",
        paddingHorizontal: responsiveWidth(5),
    },

    txtInvite: {
        fontWeight: "500", color: Colors.primaryViolet, fontSize: 12,
        lineHeight: 15.62, fontFamily: Fonts.DMSansRegular
    }
})