import React, { useEffect, useState } from 'react';
import { ScrollView, View, Image, Text, FlatList, TouchableOpacity, Platform, Alert, Linking, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { strings } from '../../../../../Resources/Strings';
import { Colors } from '../../../../../Resources/Colors';
import { Images } from '../../../../../Resources/Images'
import CommonButton from '../../../../../Components/CommonButton';
import { styles } from './PastEventStyle';
import EventDetailHeader from '../../../../../Components/EventDetailHeader';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

import {
    launchCamera,
    launchImageLibrary
} from 'react-native-image-picker';
import { Fonts } from '../../../../../Resources/Fonts';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { UploadImageIcon } from '../../../../../utils/svg/UploadImageIcon';
import EventMap from '../../../../../Components/EventMap';
import { apiCallGetWithToken, getUserData, imageUploadApi } from '../../../../../utils/helper';
import Toast from 'react-native-toast-message';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Loader from '../../../../../Components/Loader';
import CometChatIncomingCall from '../../../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import CustomAlert from '../../../../../Components/CustomAlert';
import { toastConfig } from '../../../../../utils/ToastConfig';
import NotificationController from '../../../../../Components/NotificationController';
import moment from 'moment';

const { width ,height} = Dimensions.get('window');

const PastEventDetail = (props) => {
    const [imagePath, setImagePath] = useState("")
    const [selectedIndex, setIndex] = useState(0)
    const [caseValue, setCase] = useState(props.route.params.case)
    const [itemData, setItemData] = useState(props.route.params.item)
    const [u_id, setUserId] = useState()
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(true)
    const [peopleGoingData, setPeopleGoingData] = useState([])
    const [peopleNotGoingData, setpeopleNotGoingData] = useState([])
    const [pendingData, setPendingData] = useState([])
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [userData, setUserData] = useState("")
    const [imageData, setImageData] = useState([])
    //const [isConfirmation, setIsConfirmation] = useState(false)
    const [moreImages, setMoreImageData] = useState(null)

    const onAddFriend = async () => {
        setShowing(true)
        var obj = {
            invited_user_id: itemData.event_owner_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    var obj = { u_id: u_id, token: token }
                    getEventDetails(obj)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    useEffect(() => {
        getUserData().then(res => {
            setShowing(true)
            setUserData(res)
            setToken(res.token)
            setUserId(res.u_id)
            getEventDetails(res)

        })
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    function filterData(data, condtion) {
        return data.em_status == condtion;
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getEventDetails = async (data) => {
        await apiCallGetWithToken(apiUrl.get_event_details + "?event_id=" + itemData.event_id, data.token).then(res => {
            if (res.status == true) {
                setItemData(res.result)
                setShowing(false)
                setEmstatus(data.u_id, res.result.event_members)
                setPeopleGoingData(res.result.event_members.filter((item) => filterData(item, 1)))
                setpeopleNotGoingData(res.result.event_members.filter((item) => filterData(item, 4)))
                setPendingData(res.result.event_members.filter((item) => filterData(item, 2)))
                setImageDataProper(res.result.images);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const setImageDataProper = (data) => {
        let newArray = [], i = 0;
        if (data.length == 1) {
            var obj = {}
            obj = {
                image1: data[i].event_image_image,
                image2: "",
            }
            newArray.push(obj);
            setImageData(newArray)

            return
        }
        // let total = 0;
        // if (data.length % 2 == 0 && data.length != 2)
        //     total = Math.round(data.length / 2) + 2
        // else
        //     total = Math.round(data.length / 2)+1
        let j = 0;
        while (i < data.length) {
            var obj = {}
            if (j % 2 == 0) {
                obj = {
                    image1: data[i]?.event_image_image,
                    image2: data[i + 1]?.event_image_image,
                }
                i = i + 2;
                j += 1
            }
            else {
                obj = {
                    image1: data[i]?.event_image_image
                }
                i = i + 1;
                j += 1

            }
            newArray.push(obj);

        }
        setImageData(newArray)
    }
    const setEmstatus = (user_id, event_members) => {
        var index = event_members.findIndex(obj => obj.em_user_id === user_id);
        if (index > -1) {
            var em_status = event_members[index].em_status + "";
            setIndex(defaultIndex(em_status))
        }
    }
    const defaultIndex = (em_status) => {
        if (em_status == "1")
            return 3;
        else if (em_status == "2")
            return 0;
        else if (em_status == "3")
            return 1;
        else if (em_status == "4")
            return 2
    }

    const addMoreImageApi = async (images) => {

        setShowing(true)
        let data = new FormData();
        images.forEach((element, i) => {
            const newFile = {
                uri: element.uri,
                type: 'image/jpg',
                name: element.fileName
            }
            data.append('images[]', newFile)
        });
        data.append("event_id", itemData.event_id)
        setShowing(true)
        await imageUploadApi(data, apiUrl.add_event_images, token).then(res => {
            // setIsConfirmation(false)

            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    getEventDetails(userData)
                    // props.navigation.navigation.goBack(null);
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)

                }, 1000);
            }
        })

    }
    const personView = () => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: itemData.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{itemData.u_first_name + " " + itemData.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{strings.eventHost}</Text>
                    </View>
                </View>
                {(itemData.friend_request != 1 && itemData.friend_request == 0) &&

                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={onAddFriend}
                        text={strings.addfriend}
                    />}
            </View>
        )
    }
    const viewLine = () => {
        return (
            <View style={styles.viewLine} />
        )
    }
    const dateTimeView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={[styles.txtDateLbl, { color: "red" }]}>{strings.dateTime}</Text>
                <Text style={styles.txtDate}>{itemData.event_start_date}</Text>
                <Text style={styles.txtDate}>{ moment(itemData.event_start_time, "hh:mm").format('LT') }</Text>
            </View>
        )
    }
    const locationView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{strings.location}</Text>
                <Text style={styles.txtDate}>{itemData.event_location}</Text>
                <TouchableOpacity onPress={() => openMap()}>

                    <Text style={styles.txtgetLocation}>{strings.getLocation}</Text>
                </TouchableOpacity>
                <EventMap lat={itemData.event_latitude} long={itemData.event_longitude} style={styles.imgMap} />

            </View>
        )
    }
    const detailView = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{strings.detail}</Text>
                <View style={{ flexDirection: "row" }}>
                    <View style={styles.viewLineVertical}></View>
                    <Text style={styles.txtDetailData}>{itemData.event_description}</Text>
                </View>
            </View>
        )
    }
    const peopleGoing = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row", alignItems: "center" }}>
                    <Text style={styles.txtDateLbl}>{strings.peopleJoined}
                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + peopleGoingData.length + ")"}</Text>
                    </Text>
                </View>
                <FlatList
                    data={peopleGoingData}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                {true ? <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>

                                    : <TouchableOpacity style={styles.roundView}>
                                        <Text style={styles.txtMore}>{"+5"}</Text>
                                    </TouchableOpacity>
                                }
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }

    const chooseFile = () => {
        let options = {
            mediaType: "photo",
            quality: 0.5,
            selectionLimit: 0,
            maxHeight:height,
            maxWidth:width

        };
        launchImageLibrary(options, (response) => {

            if (response.didCancel) {
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                return;
            } else if (response.errorCode == 'permission') {
                return;
            } else if (response.errorCode == 'others') {
                return;
            }
            else {
                setMoreImageData(response.assets)
                setTimeout(() => {
                    setImagePath(response.assets[0].uri)
                    addMoreImageApi(response.assets)
                    //  setIsConfirmation(true)
                }, 500);
            }

        });
    }
    const photoView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
                    <Text style={styles.txtDateLbl}>{"PHOTOS "}
                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + itemData?.images.length + ")"}</Text>
                    </Text>
                    {itemData?.images.length != 0 &&
                        <TouchableOpacity onPress={() => props.navigation.navigate(strings.viewImagesScreen, { images: itemData?.images, flag: "event" })} style={{ flexDirection: "row", alignSelf: "center" }}>
                            <Text style={styles.txtInvite}>{"View All"}</Text>
                            <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                        </TouchableOpacity>
                    }
                </View>
                {itemData?.images.length == 0 && (u_id == itemData?.event_owner_id || selectedIndex == 1) ?
                    <View>
                        <View style={styles.mainViewStep3}></View>
                        {imagePath == "" ?
                            <TouchableOpacity style={styles.subView} onPress={() => chooseFile()}>
                                <UploadImageIcon />
                                <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                            </TouchableOpacity>
                            :
                            <View style={styles.subView}>
                                <Image style={styles.profileStyle} source={{ uri: imagePath }}></Image>
                                <TouchableOpacity style={styles.editView} onPress={() => chooseFile()}>
                                    <Image
                                        style={styles.imgEditStyle}
                                        source={Images.editIcon}
                                    />
                                </TouchableOpacity>
                            </View>}
                    </View>
                    : itemData?.images.length == 0 && (u_id != itemData?.event_owner_id || selectedIndex != 1) ?
                        <Text>There are no pictures uplaoded yet</Text> :
                        <View>
                            <FlatList
                                data={imageData}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                renderItem={viewImage}
                            >

                            </FlatList>
                            {(u_id == itemData.event_owner_id || selectedIndex == 1) &&
                                <TouchableOpacity onPress={() => chooseFile()}>
                                    <Text style={{
                                        fontWeight: "400", fontSize: 12,
                                        lineHeight: 15.62, color: Colors.primaryViolet, marginVertical: 10, fontFamily: Fonts.DMSansRegular
                                    }}>{strings.addMoreImg}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                }
            </View >
        )
    }
    const viewImage = ({ item, index }) => {
        let date = moment(itemData?.event_created_at).format("MMMM DD, YYYY").toString()

        return (
            <View>
                {index % 2 == 0 ?
                    <View>
                        <TouchableOpacity onPress={() =>
                            props.navigation.navigate(strings.webViewScreen,
                                {
                                    url: item.image1,
                                    title: itemData?.event_name, date: date
                                })
                        }>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={{ uri: item.image1 }} resizeMode={"stretch"}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() =>
                            props.navigation.navigate(strings.webViewScreen,
                                {
                                    url: item.image2,
                                    title: itemData?.event_name, date: date
                                })
                        }>
                            <Image style={{ width: 105, height: 77 }} source={{ uri: item?.image2 }} resizeMode={"stretch"}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity onPress={() =>
                        props.navigation.navigate(strings.webViewScreen,
                            {
                                url: item.image1,
                                title: itemData?.event_name, date: date
                            })
                    }>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={{ uri: item.image1 }} resizeMode={"stretch"}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }
    const backPress = () => {
        props.navigation.goBack(null);
    }
    const bottomTabView = () => {
        return (
            <View style={styles.bottomView}>
                <TouchableOpacity
                    //onPress={() => setIndex(1)}
                    style={styles.viewTouchable}>
                    <View style={selectedIndex == 1 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{strings.interested}</Text>
                </TouchableOpacity>
                <TouchableOpacity //onPress={() => setIndex(2)} 
                    style={styles.viewTouchable}>
                    <View style={selectedIndex == 2 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{strings.notGoing}</Text>
                </TouchableOpacity>
                <TouchableOpacity //onPress={() => setIndex(3)}
                    style={styles.viewTouchable}>
                    <View style={selectedIndex == 3 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{strings.going}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const pendingView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{strings.pending}
                    <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + pendingData.length + ")"}</Text>
                </Text>
                <FlatList
                    data={pendingData}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>
                                <View style={styles.starView}></View>
                                <Image style={styles.starIcon} source={Images.starIcon} ></Image>

                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const notGoingView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{strings.notGoing + " "}
                    <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + peopleNotGoingData.length + ")"}</Text></Text>
                <FlatList
                    data={peopleNotGoingData}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const openMap = () => {
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${itemData.event_latitude},${itemData.event_longitude}`;
        const label = itemData.event_name;
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });

        Linking.openURL(url);
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)
                // handle exception
            }
        );
    }
    const cancelClick = () => {
        //   setIsConfirmation(false)
    }
    const okClick = () => {
        // addMoreImageApi(moreImages)
    }
    return (
        <LinearGradient
            colors={Colors.screenBackgroundColor}
            style={styles.container}
        >
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            {Platform.OS == "android" ?
                <CommonStatusBar color={Colors.backgroundColor} /> : <></>}

            <ScrollView showsVerticalScrollIndicator={false}>
                {isshowing == false &&
                    <View>
                        <EventDetailHeader
                            onBackPress={backPress}
                            image={Images.dogIcon}
                            isMenu={false}
                            isShare={false}
                            uri={itemData.event_image}
                            onMoreClick={() => alert("hhhe")}
                            title={itemData.event_name}
                        >
                        </EventDetailHeader>
                        {itemData.event_owner_id != u_id && personView()}
                        {itemData.event_owner_id != u_id && viewLine()}
                        {dateTimeView()}
                        {viewLine()}
                        {itemData?.event_latitude != undefined && itemData?.event_latitude != "null" && itemData?.event_latitude != null  && locationView()}
                        {itemData.event_description != "" && itemData.event_description != null ? detailView() : <></>}
                        {peopleGoing()}
                        {(pendingData.length != 0 && itemData.event_owner_id == u_id) && pendingView()}
                        {(peopleNotGoingData.length != 0 && itemData.event_owner_id == u_id) && notGoingView()}
                        {photoView()}
                        {/* <CustomAlert
                            title={strings.confirmation}
                            message={strings.alertMsg}
                            isConfirmation={isConfirmation}
                            cancelClick={cancelClick}
                            okClick={okClick}
                        /> */}
                    </View>}
            </ScrollView>
            {bottomTabView()}
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisible} callAccept={callAccept} />
            }
            {isVisible == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisible}
                    onMessageNotification={()=>{}}

                />
            }

        </LinearGradient>
    )
}
export default PastEventDetail;