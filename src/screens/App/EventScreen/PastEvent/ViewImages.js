import React, { useEffect, useRef, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, StyleSheet, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import HeaderView from '../../../../Components/HeaderView';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images';
import Carousel from 'react-native-snap-carousel';
import { scrollInterpolator, animatedStyles } from '../../../../Components/Animation';
import { apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.8);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);
import Toast from 'react-native-simple-toast';

const ViewImages = (props) => {
    const [seleteIndex, setIndex] = useState(0)
    const [imageData, setImageData] = useState([])
    const [imgArray] = useState(props.route.params.images)
    const [flag] = useState(props.route.params.flag)
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [user_id, setUserId] = useState("")
    const [owner_id, setOwnerId] = useState("")
    useEffect(() => {
        if (flag == "event")
            setEventImages();
        else if (flag == "pet") {
            setPetImages();
            setOwnerId(props.route.params.owner_id)
        }
        else if (flag == "post_image")
            setPostImages()
        else if(flag=="group_image")
            setGroupImages()
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
        })

    }, []);
    const setGroupImages=()=>{
        var newArray = [];
        for (var i = 0; i < imgArray.length; i++) {
            var obj = {
                image: imgArray[i].data.url,
                key: imgArray[i].id,
                pi_pet_id: imgArray[i].id
            }
            newArray.push(obj)
        }
        setImageData(newArray);
        setTimeout(() => {
            setIndex(props.route.params.index)
        }, 400);
    }
    const setPostImages = () => {
        var newArray = [];
        for (var i = 0; i < imgArray.length; i++) {
            var obj = {
                image: imgArray[i]?.post_image_image,
                key: imgArray[i]?.post_image_id
            }
            newArray.push(obj)
        }
        setImageData(newArray);
        setTimeout(() => {
            setIndex(props.route.params.index)
        }, 400);
      
    }
    const setEventImages = () => {
        var newArray = [];
        for (var i = 0; i < imgArray.length; i++) {
            var obj = {
                image: imgArray[i].event_image_image,
                key: imgArray[i].event_image_id
            }
            newArray.push(obj)
        }
        setImageData(newArray);
        
    }
    const setPetImages = () => {
        var newArray = [];
        for (var i = 0; i < imgArray.length; i++) {
            var obj = {
                image: imgArray[i].pi_image,
                key: imgArray[i].pi_id,
                pi_pet_id: imgArray[i].pi_pet_id
            }
            newArray.push(obj)
        }
        setImageData(newArray);
    }
    const backClick = () => {
        props.navigation.goBack(null);
    }
    const removePetImage = async (data, index) => {

        setShowing(true)
        setTimeout(() => {
            console.log(data)
        }, 3000);
        var obj = {
            pet_id: data.pi_pet_id,
            pi_id: data.key
        }
        await apiCallWithToken(obj, apiUrl.delete_pet_image, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                //Toast.show(res.message, Toast.LONG);
                let newArray = [...imageData]
                newArray.splice(index, 1);
                setImageData(newArray)
                if (imageData.length != 1) {
                    imageData.length - 1 == index && setIndex(index - 1)
                }
                else
                    props.navigation.goBack(null);

            }
            else {
                Toast.show(res.message, Toast.LONG);

            }
        })
    }
    const renderImage = ({ item, index }) => {
        return (
            <View style={styles.itemContainer}>

                <Image style={{ height: responsiveHeight(60), width: 295, alignSelf: "center", borderRadius: 4 }} resizeMode={"contain"} source={{ uri: item.image }}></Image>
                {flag == "pet" && owner_id == user_id && <TouchableOpacity onPress={() => removePetImage(item, index)} style={{ position: "absolute", top: 18, right: 0 }} >
                    <Image style={{ height: 20, width: 20, tintColor: Colors.red, padding: 10 }}
                        resizeMode={"contain"} source={Images.garbage}></Image>
                </TouchableOpacity>}
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <SafeAreaView style={{ marginTop: 30 }} >
                    <View style={styles.contentView}>
                        <HeaderView isCloseIcon={true} onPress={backClick} />

                        <Text style={styles.txtCount}>{imageData.length == 1 ? "1" : (seleteIndex + 1)}
                            <Text style={[styles.txtCount, { color: 'grey' }]}>{"/" + imageData.length}</Text>
                        </Text>
                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                            <Carousel
                                //  ref={(c) => this.carousel = c}
                                data={imageData}
                                renderItem={renderImage}
                                sliderWidth={SLIDER_WIDTH}
                                itemWidth={ITEM_WIDTH}
                                containerCustomStyle={styles.carouselContainer}
                                inactiveSlideShift={0}
                                firstItem={seleteIndex}
                                onSnapToItem={(index) => setIndex(index)}
                                scrollInterpolator={scrollInterpolator}
                                slideInterpolatedStyle={animatedStyles}
                                useScrollView={true}
                            />
                        </View>
                        <ScrollView horizontal>
                            <View style={styles.imageView}>
                                {
                                    imageData.map((data, index) => {
                                        return (
                                            <TouchableOpacity onPress={() => setIndex(index)}>
                                                <Image style={index == seleteIndex ? styles.selectedImgStyle : styles.imgStyle} source={{ uri: data.image }}></Image>
                                                {index == seleteIndex &&
                                                    <View style={{
                                                        height: 4, width: 4, backgroundColor: Colors.primaryViolet,
                                                        alignSelf: "center", marginRight: 10, borderRadius: 2, marginTop: 2
                                                    }}></View>}
                                            </TouchableOpacity>
                                        )
                                    })
                                }

                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentView: {
        //   alignItems: "center",
        // justifyContent: "center"

    },
    carouselContainer: {
        marginTop: 10
    },
    itemContainer: {
        width: 295,
        height: responsiveHeight(65),
        alignItems: 'center',
        justifyContent: 'center',
        //  backgroundColor: 'dodgerblue',

    },
    imgStyle: { width: 52, height: 52, borderRadius: 4, marginRight: 10 },
    selectedImgStyle: { width: 52, height: 52, borderWidth: 1, borderColor: Colors.primaryViolet, marginRight: 10, borderRadius: 4 },
    imageView: { height: 70, flexDirection: "row", marginTop: 20 },
    txtCount: { fontSize: 14, lineHeight: 18.23, fontWeight: "400", color: Colors.white, marginLeft: 20, marginTop: 29 }


})
export default ViewImages;