import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 5,
        // marginHorizontal: responsiveWidth(4),
        alignItems: "center",
        justifyContent: "center",
        width: 96
    },
    recommendedText: {
        fontWeight: "700",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    contentView: {
        flex: 1,
        marginHorizontal: responsiveWidth(4),
        marginVertical: responsiveHeight(2)
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    renderStyle: { height: 180, marginVertical: responsiveHeight(1), borderRadius: 4 },
    imgStyle: { height: 180, width: responsiveWidth(100),borderRadius:4 },
    txtName: { fontSize: 14, fontWeight: "400", lineHeight: 18.23, alignSelf: "center", color: Colors.white, marginTop: responsiveHeight(1.8) },
    txtDetail: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, alignSelf: "center", color: Colors.white, textAlign: "center", opacity: 0.6 },
    btnView: { width: "100%", height: 40, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(2) },


    //floting view
    flotingView: {
        position: 'absolute',
        width: 54,
        height: 54,
        right: 0,
        bottom: 0,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    plusView: {
        height: 54, width: 54, alignItems: 'center',
        justifyContent: 'center',
    },

    //flatlist contentView
    upperView: { position: "absolute", margin: 10, width: "95%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    peopleImg: { height: 32, width: 32, borderRadius: 15 },
    peopleImg1: { height: 32, width: 32, borderRadius: 15, marginLeft: -8 },
    moreView: { height: 32, width: 32, marginLeft: -8, alignItems: "center", justifyContent: "center" },
    moreBorderView: { backgroundColor: Colors.black, borderRadius: 15, opacity: 0.6, width: 32, height: 32, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700", fontSize: 9,
        lineHeight: 11.72, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    dateView: { height: 37, width: 36, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2 },
    txtDate: {
        fontSize: 16, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 10, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 0, margin: 10 },
    txtTitle: {
        fontWeight: "400", fontSize: 22, lineHeight: 27.5, color: Colors.white,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.4
    },
    bottomBorderView: { height: 1, backgroundColor: Colors.white, opacity: 0.2, marginVertical: 3 },
    placeIcon: { height: 13, width: 12, opacity: 0.7, tintColor: Colors.white },
    txtAddress: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 10, fontFamily: Fonts.DMSansRegular
    }
})