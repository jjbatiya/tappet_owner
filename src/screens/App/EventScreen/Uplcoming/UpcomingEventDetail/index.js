import React, { useEffect, useRef, useState } from 'react';
import { ScrollView, View, Image, Text, FlatList, TouchableOpacity, Platform, Alert, Linking, Share, Dimensions, KeyboardAvoidingView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { strings } from '../../../../../Resources/Strings';
import { Colors } from '../../../../../Resources/Colors';
import { Images } from '../../../../../Resources/Images'
import CommonButton from '../../../../../Components/CommonButton';
import { styles } from './UpcomingDetailStyle';
import EventDetailHeader from '../../../../../Components/EventDetailHeader';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MoreMenu from '../../../../../Components/MoreMenu';
import {
    launchCamera,
    launchImageLibrary
} from 'react-native-image-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { UploadImageIcon } from '../../../../../utils/svg/UploadImageIcon';
import EventMap from '../../../../../Components/EventMap';
import Loader from '../../../../../Components/Loader';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import { apiCallGetWithToken, apiCallWithToken, createDynamicLinks, getUserData, imageUploadApi } from '../../../../../utils/helper';
import Toast from 'react-native-toast-message';
import { Fonts } from '../../../../../Resources/Fonts';
import SimpleSerach from '../../../../../Components/SimpleSerach';
import CometChatIncomingCall from '../../../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import CustomAlert from '../../../../../Components/CustomAlert';
import { toastConfig } from '../../../../../utils/ToastConfig';
import NotificationController from '../../../../../Components/NotificationController';

import moment from 'moment';
import CommentModal from '../../../../../Components/CommentModal';
const { width, height } = Dimensions.get('window');

const caseData3 = [
    {
        key: "edit",
        title: "Edit event",
        image: Images.editIcon
    },
    {
        key: "Invite friends",
        title: "Invite groups & friends",
        image: Images.Members
    },

    {
        key: "addpictures",
        title: "Add pictures",
        image: Images.addPicture
    },
    {
        key: "delete",
        title: "Delete event",
        image: Images.deleteIcon
    },
]
const caseData2 = [

    {
        key: "unfollowEvent",
        title: "UnFollow event",
        image: Images.Members
    },

    {
        key: "addpictures",
        title: "Add pictures",
        image: Images.addPicture
    },
    {
        key: "delete",
        title: "Delete event",
        image: Images.deleteIcon
    },
]
const UpcomingEventDetail = (props) => {
    const [CommentsData, setCommentData] = useState([])

    const [imagePath, setImagePath] = useState("")
    const [selectedIndex, setIndex] = useState(0)
    const [caseValue, setCase] = useState(props.route.params.case)
    const [itemData, setItemData] = useState(props.route.params.item)
    let refSheet = useRef();
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [u_id, setUserId] = useState("")
    const [em_status, setEmStatusValue] = useState("")
    const [peopleGoingData, setPeopleGoingData] = useState([])
    const [peopleNotGoingData, setpeopleNotGoingData] = useState([])
    const [pendingData, setPendingData] = useState([])
    const [groupsData, setGroupData] = useState([])
    const [friendsData, setFriendData] = useState([])
    let groupRefSheet = useRef();
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [userData, setUserData] = useState("")
    const [imageData, setImageData] = useState([])
    // const [isConfirmation, setIsConfirmation] = useState(false)
    const [moreImages, setMoreImageData] = useState(null)
    const [event_post_id, setEventPostId] = useState("")
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [isModal, setIsModal] = useState(false)
    const onAddFriend = async () => {
        setShowing(true)
        var obj = {
            invited_user_id: itemData.event_owner_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    var obj = { u_id: u_id, token: token }
                    getEventDetails(obj)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    useEffect(() => {
        getUserData().then(res => {
            setUserData(res)
            setShowing(true)
            setToken(res.token)
            setUserId(res.u_id)
            getEventDetails(res)
        })
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    function filterData(data, condtion) {
        return data.em_status == condtion;
    }
    const getEventDetails = async (data) => {
        await apiCallGetWithToken(apiUrl.get_event_details + "?event_id=" + itemData.event_id, data.token).then(res => {
            if (res.status == true) {
                setItemData(res.result)
                setShowing(false)
                setEmstatus(data.u_id, res.result.event_members)
                setPeopleGoingData(res.result.event_members.filter((item) => filterData(item, 1)))
                setpeopleNotGoingData(res.result.event_members.filter((item) => filterData(item, 4)))
                setPendingData(res.result.event_members.filter((item) => filterData(item, 2)))
                getGroupData(data.token)
                setImageDataProper(res.result.images);
                getCommeentData(res.result.event_has_post, data.token);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const getCommeentData = async (event_has_post, token) => {

        if (event_has_post != null && Object.keys(event_has_post).length != 0 && event_has_post != undefined) {
            setEventPostId(event_has_post.post_id)
            await apiCallGetWithToken(apiUrl.get_all_post_comments + "?post_id=" + event_has_post.post_id, token).then(res => {

                if (res.status == true) {
                    if (res.result.comment.length != 0) {
                        setCommentData(res.result.comment)
                    }
                }

            })
        }
    }
    const setImageDataProper = (data) => {
        let newArray = [], i = 0;
        if (data.length == 1) {
            var obj = {}
            obj = {
                image1: data[i].event_image_image,
                image2: "",
            }
            newArray.push(obj);
            setImageData(newArray)

            return
        }
        // let total = 0;
        // if (data.length % 2 == 0&&data.length!=2)
        //     total = Math.round(data.length / 2) + 2
        // else
        //     total = Math.round(data.length / 2)+1
        let j = 0;
        while (i < data.length) {
            var obj = {}
            if (j % 2 == 0) {
                obj = {
                    image1: data[i]?.event_image_image,
                    image2: data[i + 1]?.event_image_image,
                }
                i = i + 2;
                j += 1
            }
            else {
                obj = {
                    image1: data[i]?.event_image_image
                }
                i = i + 1;
                j += 1

            }
            newArray.push(obj);

        }
        setImageData(newArray)
    }
    const memberManage = (data, groupData) => {
        let newArray = [...data]
        for (var i = 0; i < itemData?.event_members?.length; i++) {
            var index = getIndex(itemData.event_members[i].member.u_id, data)
            if (index > -1)
                newArray[index].isChecked = true
        }
        let newArray1 = [...groupData]
        for (var i = 0; i < itemData?.event_groups?.length; i++) {
            var index = getIndexGroup(itemData.event_groups[i].eg_group_id, groupData)
            if (index > -1)
                newArray1[index].isChecked = true
        }
        setFriendData(newArray)
        setGroupData(newArray1)
    }
    function getIndex(u_id, data) {
        return data.findIndex(obj => obj.u_id === u_id);
    }
    function getIndexGroup(eg_group_id, data) {
        return data.findIndex(obj => obj.group_id === eg_group_id);
    }
    const getGroupData = async (token) => {
        await apiCallGetWithToken(apiUrl.get_all_groups_and_friends, token).then(res => {

            if (res.status == true) {
                setGroupData(res.result.groups)
                memberManage(res.result.friends, res.result.groups)
                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const setEmstatus = (user_id, event_members) => {
        var index = event_members.findIndex(obj => obj.em_user_id === user_id);
        if (index > -1) {
            var em_status = event_members[index].em_status + "";
            setEmStatusValue(em_status)
            setIndex(defaultIndex(em_status))
        }
        else {
            setEmStatusValue("4")
            setIndex(2)

        }


    }
    const defaultIndex = (em_status) => {
        if (em_status == "1")
            return 3;
        else if (em_status == "2")
            return 0;
        else if (em_status == "3")
            return 1;
        else if (em_status == "4")
            return 2
    }
    const personView = () => {
        return (
            <View style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: itemData.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{itemData.u_first_name + " " + itemData.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{"Event Host"}</Text>
                    </View>
                </View>
                {(itemData.friend_request != 1 && itemData.friend_request == 0) &&
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={onAddFriend}
                        text={strings.addfriend}
                    />
                }
            </View>
        )
    }
    const viewLine = () => {
        return (
            <View style={styles.viewLine} />
        )
    }
    const dateTimeView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{"Date and Time"}</Text>
                <Text style={styles.txtDate}>{itemData.event_start_date}</Text>
                <Text style={styles.txtDate}>{ moment(itemData.event_start_time, "hh:mm").format('LT')  }</Text>
            </View>
        )
    }
    const locationView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{"Location"}</Text>
                <Text style={styles.txtDate}>{itemData.event_location}</Text>
                <TouchableOpacity onPress={() => openMap()}>
                    <Text style={styles.txtgetLocation}>{"Get Direction"}</Text>
                </TouchableOpacity>
                <EventMap lat={itemData?.event_latitude} long={itemData?.event_longitude} style={styles.imgMap} />
            </View>
        )
    }
    const detailView = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{"DETAIL"}</Text>
                <View style={{ flexDirection: "row" }}>
                    <View style={styles.viewLineVertical}></View>
                    <Text style={styles.txtDetailData}>{itemData.event_description}</Text>
                </View>
            </View>
        )
    }
    const peopleGoing = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row", alignItems: "center" }}>
                    <Text style={styles.txtDateLbl}>{"PEOPLE GOING "}
                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + peopleGoingData.length + ")"}</Text>

                    </Text>
                    {itemData.event_owner_id == u_id ?
                        <Text onPress={() => groupRefSheet.open()} style={styles.txtInvite}>{"+ Invite People"}</Text>
                        : null}
                </View>
                <FlatList
                    data={peopleGoingData}

                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                {true ? <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>

                                    : <TouchableOpacity style={styles.roundView}>
                                        <Text style={styles.txtMore}>{"+5"}</Text>
                                    </TouchableOpacity>
                                }
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const commentView = () => {
        return (
            <View style={{ marginBottom: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
                    <Text style={styles.txtDateLbl}>{"Comments "}
                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + itemData.images.length + ")"}</Text>
                    </Text>

                    <TouchableOpacity onPress={() => props.navigation.navigate(strings.commentsScreen, { post_id: event_post_id })}
                        style={{ flexDirection: "row", alignSelf: "center" }}>
                        <Text style={styles.txtInvite}>{"View All"}</Text>
                        <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                    </TouchableOpacity>

                </View>
                <FlatList
                    data={CommentsData}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >
                </FlatList>
                <TouchableOpacity onPress={() => setIsModal(true)}>
                    <Text style={{
                        fontWeight: "400", fontSize: 12,
                        lineHeight: 15.62, color: Colors.primaryViolet, marginVertical: 10, fontFamily: Fonts.DMSansRegular
                    }}>{"+ Add Comments"}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const convertUTCToLocalTime = (date) => {
        const milliseconds = Date.UTC(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
        );
        const localTime = new Date(milliseconds);
        return localTime;
    };
    const renderView = ({ item, index }) => {
        const date1 = new Date(item.post_comment_created_at.replace(/-/g, "/"));
        const date = convertUTCToLocalTime(date1);

        return (
            <View style={styles.mainView}>
                <Image style={styles.imgStyleComment} source={{ uri: item.u_image }}></Image>
                <View>
                    <View style={styles.commentView}>
                        <Text style={styles.txtNameComment}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtComment}>{item.post_comment_text}</Text>
                        <Text style={styles.txtTime}>{date.getDate() + " " + monthArray[date.getMonth()] + " " + moment.utc(item.post_comment_created_at).local().format("h:mm A")}</Text>
                    </View>
                </View>
            </View>
        )
    }
    const pendingView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{"PENDING "}
                    <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + pendingData.length + ")"}</Text>
                </Text>
                <FlatList
                    data={pendingData}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>
                                <View style={styles.starView}></View>
                                <Image style={styles.starIcon} source={Images.starIcon} ></Image>

                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const openMap = () => {
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${itemData.event_latitude},${itemData.event_longitude}`;
        const label = itemData.event_name;
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });

        Linking.openURL(url);
    }
    const notGoingView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <Text style={styles.txtDateLbl}>{"Not going "}
                    <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + peopleNotGoingData.length + ")"}</Text></Text>
                <FlatList
                    data={peopleNotGoingData}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    keyExtractor={item => item.em_user_id}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={{ margin: 6, flexDirection: "row" }}>
                                <Image style={styles.imgView} source={{ uri: item.member.u_image }}></Image>
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const chooseFile = () => {
        let options = {
            mediaType: "photo",
            quality: 0.5,
            selectionLimit: 0,
            maxHeight: height,
            maxWidth: width
        };
        launchImageLibrary(options, (response) => {

            if (response.didCancel) {
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                return;
            } else if (response.errorCode == 'permission') {
                return;
            } else if (response.errorCode == 'others') {
                return;
            }
            else {
                //confirnDialog(response.assets)
                setMoreImageData(response.assets)
                setTimeout(() => {
                    setImagePath(response.assets[0].uri)
                    addMoreImageApi(response.assets)

                    // setIsConfirmation(true)
                }, 500);
            }

        });
    }
    const photoView = () => {
        return (
            <View style={{ marginVertical: responsiveHeight(1.2), marginHorizontal: responsiveWidth(5) }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
                    <Text style={styles.txtDateLbl}>{"PHOTOS "}
                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + itemData.images.length + ")"}</Text>
                    </Text>
                    {itemData.images.length != 0 &&
                        <TouchableOpacity onPress={() => props.navigation.navigate(strings.viewImagesScreen, { images: itemData?.images, flag: "event" })} style={{ flexDirection: "row", alignSelf: "center" }}>
                            <Text style={styles.txtInvite}>{"View All"}</Text>
                            <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                        </TouchableOpacity>
                    }
                </View>
                {itemData.images.length == 0 && (u_id == itemData.event_owner_id || selectedIndex == 1) ?
                    <View>
                        <View style={styles.mainViewStep3}></View>
                        {imagePath == "" ?
                            <TouchableOpacity style={styles.subView} onPress={() => chooseFile()}>
                                <UploadImageIcon />
                                <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                            </TouchableOpacity>
                            :
                            <View style={styles.subView}>
                                <Image style={styles.profileStyle} source={{ uri: imagePath }}></Image>
                                <TouchableOpacity style={styles.editView} onPress={() => chooseFile()}>
                                    <Image
                                        style={styles.imgEditStyle}
                                        source={Images.editIcon}
                                    />
                                </TouchableOpacity>
                            </View>}
                    </View>
                    : itemData.images.length == 0 && (u_id != itemData.event_owner_id || selectedIndex != 1) ?
                        <Text style={{ color: Colors.white, opacity: 0.6 }}>There are no pictures uplaoded yet</Text> :
                        <View>
                            <FlatList
                                data={imageData}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                renderItem={viewImage}
                            >

                            </FlatList>
                            {(u_id == itemData.event_owner_id || selectedIndex == 1) &&

                                <TouchableOpacity onPress={() => chooseFile()}>
                                    <Text style={{
                                        fontWeight: "400", fontSize: 12,
                                        lineHeight: 15.62, color: Colors.primaryViolet, marginVertical: 10, fontFamily: Fonts.DMSansRegular
                                    }}>{"+ Add more photos"}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                }
            </View >
        )
    }
    const viewImage = ({ item, index }) => {
        let date = moment(itemData?.event_created_at).format("MMMM DD, YYYY").toString()

        return (
            <View>
                {(index) % 2 == 0 ?
                    <View>
                        <TouchableOpacity onPress={() =>
                            props.navigation.navigate(strings.webViewScreen,
                                {
                                    url: item.image1,
                                    title: itemData?.event_name, date: date
                                })
                        }>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} resizeMode={"stretch"} source={{ uri: item.image1 }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() =>
                            props.navigation.navigate(strings.webViewScreen,
                                {
                                    url: item?.image2,
                                    title: itemData?.event_name, date: date
                                })
                        }>
                            <Image style={{ width: 105, height: 77 }} resizeMode={"stretch"} source={{ uri: item?.image2 }}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity onPress={() =>
                        props.navigation.navigate(strings.webViewScreen,
                            {
                                url: item?.image1,
                                title: itemData?.event_name, date: date
                            })
                    }>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} resizeMode={"stretch"} source={{ uri: item.image1 }}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }

    const backPress = () => {
        props.navigation.goBack(null);
    }
    const bottomClick = (index, em_status_data) => {
        //if (em_status == "2")
        {
            setIndex(index)
            invitePeople(em_status_data)
        }
        // else if (em_status == "") {
        //     showMessage(strings.error, strings.msgTag, "you are not invite this event")
        // }
    }
    const bottomTabView = () => {
        return (
            <View style={styles.bottomView}>
                <TouchableOpacity onPress={() => bottomClick(1, "3")} style={styles.viewTouchable}>
                    <View style={selectedIndex == 1 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{"Interested"}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => bottomClick(2, "4")} style={styles.viewTouchable}>
                    <View style={selectedIndex == 2 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{"Not going"}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => bottomClick(3, "1")} style={styles.viewTouchable}>
                    <View style={selectedIndex == 3 ? styles.selectBorderView : styles.borderViewStyle}></View>
                    <Text style={styles.txtBottom}>{"Going"}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const onclickMore = () => {

        refSheet.open()
    }
    const clickItem = (item) => {
        refSheet.close();
        if (item.key == "edit") {
            setTimeout(() => {
                props.navigation.navigate(strings.addEventScreen, { eventData: itemData })
            }, 1000);

        }
        else if (item.key == "addpictures") {
            setTimeout(() => {
                chooseFile()
            }, Platform.OS == "ios" ? 1000 : 500);
        }
        else if (item.key == "delete") {
            deleteEvent();
        }
        else if (item.key == "Invite friends") {
            setTimeout(() => {
                groupRefSheet.open();
            }, 400);
        }
        else if (item.key == "unfollowEvent")
            invitePeople("5")
    }
    const invitePeople = async (em_status) => {
        setShowing(true)
        var obj = {
            "event_id": itemData.event_id,
            "event_status": em_status,
        }
        await apiCallWithToken(obj, apiUrl.event_invitation_action, token).then(res => {
            if (res.status == true) {
                setEmStatusValue(em_status)
                getEventDetails(userData)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
            setTimeout(() => {
                setShowing(false)
            }, 1000);
        })
    }

    const deleteEvent = async () => {
        setShowing(true)
        var obj = {
            "event_id": itemData.event_id
        }

        await apiCallWithToken(obj, apiUrl.delete_event, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.TabNavigation }],
                    });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const addMoreImageApi = async (images) => {

        setShowing(true)
        let data = new FormData();
        images.forEach((element, i) => {
            const newFile = {
                uri: element.uri,
                type: 'image/jpg',
                name: element.fileName
            }
            data.append('images[]', newFile)
        });
        data.append("event_id", itemData.event_id)
        setShowing(true)
        await imageUploadApi(data, apiUrl.add_event_images, token).then(res => {
            //setIsConfirmation(false)
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    getEventDetails(userData)

                    // props.navigation.navigation.goBack(null);
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const onShareClick = () => {
        var obj = {
            id: itemData.event_id,
            title: itemData.event_name,
            imageUrl: itemData.event_image,
            descriptionText: itemData.event_description != null ? itemData.event_description : "",
            flag: "eventUpcoming"
        }
        setTimeout(() => {
            createDynamicLinks(obj).then(res => {
                onShare(res)
            })
        }, 500);

    }
    const onShare = async (link) => {
        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    const groupModal = () => {
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={styles.modal}>

                <TouchableOpacity activeOpacity={1}>
                    <SimpleSerach
                        onChangeText={(text) => console.log(text)}
                        placeHolder={strings.serachPlaceholderBottom}
                        isSimpleSerach={true}
                        editable={true}
                        onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onSubmitEditing(text)}

                    />
                    {groupViewBottom()}
                    {friendViewBottom()}
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradientBottom}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.verifyBtnGroup}
                        onPress={onProcesd}
                        text={strings.proceed}
                    />
                    {/* <Text onPress={() =>refSheet.close()} style={styles.txtCancel}>{strings.cancelText}</Text> */}
                </TouchableOpacity>
            </ScrollView>
        )
    }
    const onSubmitEditing = (text) => {
        if (text.length != 0) {
            setFriendData(friendsData.filter((item) => filterDataFreind(item, text)))
            setGroupData(groupsData.filter((item) => filterDataGroup(item, text)))
        }
        else {
            getGroupData(token)
        }
    }
    function filterDataGroup(data, text) {
        return data.group_name.includes(text)
    }
    function filterDataFreind(data, text) {
        return data.u_first_name.includes(text) || data.u_last_name.includes(text)
    }
    const onProcesd = () => {
        groupRefSheet.close();
        addMember();
    }
    const onCheckBoxClickFriends = (index) => {
        let newArray = [...friendsData]
        newArray[index].isChecked = !newArray[index].isChecked
        setFriendData(newArray)
    }
    const onCheckBoxClick = (index) => {
        let newArray = [...groupsData]
        newArray[index].isChecked = !newArray[index].isChecked
        setGroupData(newArray)
    }
    const addMember = async () => {
        let data = new FormData();
        setShowing(true)
        data.append("event_id", itemData.event_id)
        for (var i = 0; i < groupsData.length; i++) {
            if (groupsData[i].isChecked != undefined && groupsData[i]?.isChecked)
                data.append("event_groups[]", groupsData[i].group_id);
        }
        for (var i = 0; i < friendsData.length; i++) {
            if (friendsData[i].isChecked != undefined && friendsData[i]?.isChecked)
                data.append("event_members[]", friendsData[i].u_id);
        }
        await imageUploadApi(data, apiUrl.invite_friend_or_group, token).then(res => {
            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    getEventDetails({ u_id: u_id, token: token })
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const groupViewBottom = () => {
        return (
            <View>
                {groupsData.map((item, index) => {
                    var memberList = item.get_last_two_member.map(a => a.u_first_name).toString();
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={{ uri: item.group_image }}></Image>
                                <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                    {item.get_last_two_member.map((item, index) => {
                                        return (
                                            <Image style={[styles.peopleImg, { marginLeft: index != 0 ? -8 : 0 }]} source={{ uri: item.u_image }}></Image>
                                        )
                                    })}
                                </View>
                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.group_name}</Text>
                                    <Text style={styles.txtInviteBottom}>{memberList}</Text>
                                </View>
                                <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClick(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
    const friendViewBottom = () => {
        return (
            <View>
                {friendsData.map((item, index) => {
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={{ uri: item.u_image }}></Image>

                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.u_first_name + " " + item.u_last_name}</Text>
                                    <Text style={styles.txtInviteBottom}>{item.total_mutual_friends + " Mutual Friends " + item.total_pets + " Pets"}</Text>
                                </View>
                                <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClickFriends(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
            </View>
        )

    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)
                // handle exception
            }
        );
    }
    const cancelClick = () => {
        // setIsConfirmation(false)
    }
    const okClick = () => {
        addMoreImageApi(moreImages)
    }
    const onAddComment = async (comment) => {
        setIsModal(false)
        setShowing(true)
        var obj = {
            post_id: event_post_id,
            post_comment_text: comment
        }
        await apiCallWithToken(obj, apiUrl.add_comment, token).then(res => {
            setShowing(false)

            if (res.status == true) {
                getCommeentData({ post_id: event_post_id }, token)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    return (
        <LinearGradient
            colors={Colors.screenBackgroundColor}
            style={styles.container}
        >

                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                {Platform.OS == "android" ?
                    <CommonStatusBar color={Colors.backgroundColor} /> : <></>}
                {itemData.images != undefined &&
                    <ScrollView style={{ marginBottom: 100 }} showsVerticalScrollIndicator={false}  >

                        <View>
                            <EventDetailHeader
                                onBackPress={backPress}
                                isShare={true}
                                onShareClick={onShareClick}
                                image={Images.dogIcon}
                                uri={itemData.event_image}
                                onMoreClick={onclickMore}
                                isMenu={itemData.event_owner_id == u_id || selectedIndex == 1 ? true : false}
                                title={itemData.event_name}
                            >
                            </EventDetailHeader>
                            {itemData.event_owner_id != u_id && personView()}
                            {itemData.event_owner_id != u_id && viewLine()}
                            {dateTimeView()}
                            {viewLine()}
                            {itemData?.event_latitude != undefined && itemData?.event_latitude != "null" && itemData?.event_latitude != null && locationView()}
                            {itemData.event_description != "" && itemData.event_description != null ? detailView() : <></>}

                            {peopleGoingData.length != 0 && peopleGoing()}
                            {(pendingData.length != 0 && itemData.event_owner_id == u_id) && pendingView()}
                            {(peopleNotGoingData.length != 0 && itemData.event_owner_id == u_id) && notGoingView()}
                            {photoView()}
                            {itemData.event_has_post != null && commentView()}
                            {/* <CustomAlert
                            title={strings.confirmation}
                            message={strings.alertMsg}
                            isConfirmation={isConfirmation}
                            cancelClick={cancelClick}
                            okClick={okClick}
                        /> */}
                        </View>
                        <RBSheet
                            ref={(ref) => refSheet = ref}
                            height={caseValue == "case2" ? 170 : 212}
                            openDuration={250}
                            closeOnDragDown={true}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10,
                                }
                            }}
                        >
                            <MoreMenu
                                data={itemData.event_owner_id != u_id && selectedIndex == 1 ? caseData2 : caseData3}
                                clickItem={clickItem}
                            >

                            </MoreMenu>
                        </RBSheet>
                        <RBSheet
                            ref={ref => {
                                groupRefSheet = ref;
                            }}
                            height={438}
                            openDuration={250}
                            closeOnDragDown={true}
                            customStyles={{
                                container: {
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10,
                                }
                            }}
                        >
                            {groupModal()}
                        </RBSheet>

                    </ScrollView>}
                {itemData.images != undefined && bottomTabView()}
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
                {Platform.OS == "ios" &&
                    <CometChatIncomingCall callData={callData} callCancel={callCancel}
                        isVisible={isVisible} callAccept={callAccept} />
                }
                {isVisible == false && Platform.OS == "ios" &&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisible}
                        onMessageNotification={() => { }}

                    />
                }
                <CommentModal
                    isModal={isModal}
                    onClose={() => setIsModal(false)}
                    onAddComment={(text) => onAddComment(text)}
                />
        </LinearGradient>
    )
}
export default UpcomingEventDetail;