import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    labelInput: {
        fontSize: 20,
        lineHeight: 26,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginTop: responsiveHeight(1),
       

    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginTop: responsiveHeight(1),
       
    },
    input: {
        borderWidth: 0,
        fontWeight: "400",
        color: "white",
        fontSize: 16,
        lineHeight: 26,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3,
        marginLeft:-8,

    },
    inputDate: {
        borderWidth: 0,
        fontWeight: "400",
        color: "white",
        fontSize: 16,
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,
        opacity:0.3

    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center"

    },
    arrow: {
        position: 'absolute',
        right: responsiveHeight(0.5),
        height: responsiveHeight(4),
        width: responsiveWidth(3),
        resizeMode: "contain",
        flex: 0.1,
        tintColor: Colors.white
    },
    txtforget: {

        color: Colors.white,
        textAlign: "center",
        marginTop: 20,
        fontSize: 12,
        lineHeight: 15.62,
        fontFamily: Fonts.DMSansRegular,


    },
    txtReset: {
        fontWeight: "500",
        color: Colors.white,
        fontSize: 12,
        lineHeight: 15.62,
        textAlign: "center",
        fontFamily: Fonts.DMSansRegular,


    },
    txtlblStep: {
        alignSelf: "center", fontSize: 12, lineHeight: 15.62,
        fontWeight: "400", color: Colors.white, marginRight: responsiveWidth(8),
        fontFamily: Fonts.DMSansRegular,
        opacity:0.6
    },
    mainView: {
        marginHorizontal: responsiveWidth(5), marginVertical: responsiveHeight(1),
        flex: 1, width: "100%", flexDirection: "row"
    },
    modal: {
        //alignItems: 'center',   
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: '100%',
        width: '100%',


    },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16, lineHeight: 22,
        color: Colors.white, fontWeight: "700", marginTop: responsiveHeight(2.5), fontFamily: Fonts.DMSansRegular,
    },
    txtlblRecoverStr: {
        textAlign: "center", alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        width: 295, marginVertical: responsiveHeight(1.5), fontFamily: Fonts.DMSansRegular,
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, fontWeight: "400", fontFamily: Fonts.DMSansRegular,
    },
    verifyBtn: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "90%", alignSelf: "center" },
    contentView: {
        marginHorizontal: responsiveWidth(5), marginTop: responsiveHeight(1.1),
        marginBottom: responsiveHeight(20),
        height: responsiveHeight(80)
    },
    dobText: {
        fontSize: 12, lineHeight: 15.62, fontWeight: "400",
        color: Colors.white, marginTop: responsiveHeight(2), 
        fontFamily: Fonts.DMSansRegular,
        opacity:0.6
    },
    dateClickView: { flexDirection: "row", justifyContent: "space-between", paddingVertical: responsiveHeight(1) },
    calender: { resizeMode: "contain", height: 18, width: 20 },
    horizontalLine: { height: 1, backgroundColor: Colors.white },

    //add time
    addTimeText: {
        fontWeight: "500",
        color: Colors.primaryViolet,
        fontSize: 12,
        lineHeight: 15.62,
        fontFamily: Fonts.DMSansRegular

    },
    txtPlus:{
        color: Colors.primaryViolet,
        fontSize: 18,
        lineHeight: 15.62,
   
        marginTop:3
        
    },
    //participate
    basicStyle: { fontWeight: "700", fontSize: 14, lineHeight: 18.23, color: Colors.white, textTransform: "uppercase", marginTop: responsiveHeight(2) },
    person: {
        height: 20,
        width: 20,
        tintColor: Colors.white
    },
    txtSelectParti: {
        fontSize: 14,
        fontWeight: "400",
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular,

    },
    txtDetailParticipate: {
        fontSize: 12,
        fontWeight: "400",
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6
    },
    imgRight: { width: 10, height: 10, tintColor: Colors.white, opacity: 0.6 },

    //participate bottom sheet view
    radioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(1)
    },
    selectradioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: '#EA8FFA',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(1)

    },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily:Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily:Fonts.DMSansRegular,


    },
    selectedRb: {
        width: 10,
        height: 10,
        borderRadius: 50,
        backgroundColor: '#EA8FFA',
    },
    mainParticipate: { flexDirection: "row", alignItems: "center", margin: 20 },
    imgItem: { height: 20, width: 20, tintColor: Colors.white },
    radioView: { justifyContent: "space-between", flexDirection: "row", width: '100%' },


    //group bottom sheet
    mainGroupView: { flexDirection: "row", marginHorizontal: 20, marginVertical: 10, alignItems: "center" },

    imgGroup: {
        height: 48,
        width: 48,
        borderRadius: 54,
    },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    peopleImg: { height: 20, width: 20, borderRadius: 15 },
    peopleImg1: { height: 20, width: 20, borderRadius: 15, marginLeft: -8 },
    verifyBtnGroup: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "95%", alignSelf: "center" },
    viewCheckBox: {
        height: 20,
        width: 20,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primaryViolet

    },
    tickImage: { height: 6, width: 8, position: "absolute", alignSelf: "center" },


    //step line handle
    step1: { backgroundColor: Colors.white, height: 4, flex: 0.67 },
    step2: { backgroundColor: Colors.white, height: 4, flex: 0.33 },
    step3: {},

    //step 3 view style
    mainViewStep3: {
        height: 154, borderColor: Colors.white, alignSelf: "center",
        borderWidth: 1,
        marginTop: responsiveHeight(2),
        justifyContent: "center",
        width: "100%",
        opacity: 0.2
    },
    imgUploadStyle: {
        height: 33.33, width: 33.33,
        alignSelf: "center", borderRadius: 7
    },
    txtUploadText: { color: Colors.white, alignSelf: "center",
     lineHeight: 18.23, fontSize: 14, fontWeight: "400", marginTop: responsiveHeight(1),
     fontFamily:Fonts.DMSansRegular,
     opacity:0.6
    },
    profileStyle: {
        height: 154,
        alignSelf: "center",
        width: '100%'
    },
    editView: {
        width: 40, height: 40, backgroundColor: Colors.dialogBackground,
        position: 'absolute', bottom: -20, right: 10,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: '#0C0024'
        ,
    },
    imgEditStyle: { height: 12, width: 12, tintColor: "white", alignSelf: "center" },
    subView: {
        height: 154, alignSelf: "center",
        marginTop: responsiveHeight(2),
        justifyContent: "center",
        width: "100%",
        position: "absolute"
    },
    labelInputThirdStep: {
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontWeight: "400",
        fontFamily:Fonts.DMSansRegular,

    },
    inputThirdStep: {
        borderWidth: 0,
        fontWeight: "400",
        color: "white",
        fontSize: 16,
        lineHeight: 22,
        fontFamily:Fonts.DMSansRegular,
        textAlign:"left",
        opacity:0.3,
        marginLeft:-8

    },
    bottomText: {
        position: "absolute",
        bottom: 0,
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.primaryViolet,
        alignSelf: "center",
        fontFamily:Fonts.DMSansRegular,
    },
    txtAuto:{
        height: 60,
        paddingVertical: 5,
        paddingHorizontal: 10,
        fontSize: 16,
        backgroundColor: Colors.backgroundColor,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 22,
        flex: 1
    }


})