import React, { useState, useRef, useEffect } from 'react';
import {
    View, SafeAreaView, Text, KeyboardAvoidingView, Platform, ScrollView, Modal, Image,
    TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard, Dimensions
} from 'react-native';
import { styles } from './addEventStyle';
import LinearGradient from 'react-native-linear-gradient';
import HeaderView from '../../../../../Components/HeaderView';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../../Components/CommonButton';
import RadioButton from '../../../../../Components/RadioButton';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { strings } from '../../../../../Resources/Strings';
import { Colors } from '../../../../../Resources/Colors';
import { Images } from '../../../../../Resources/Images'
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import SimpleSerach from '../../../../../Components/SimpleSerach';

import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { CalenderIcon } from '../../../../../utils/svg/CalenderIcon';
import Friends from '../../../../../utils/svg/Friends';
import { UploadImageIcon } from '../../../../../utils/svg/UploadImageIcon';
import { EditIcon } from '../../../../../utils/svg/EditIcon';
import Toast from 'react-native-toast-message';
import AutoSerachComponent from '../../../../../Components/AutoSerachComponent';
import Loader from '../../../../../Components/Loader';
import { addressGetFromLatLng, apiCallGetWithToken, apiCallWithTokenAddPet, getUserData, imageUploadApi, sendEventMessage } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
const moment = require('moment')
import { CometChat } from '@cometchat-pro/react-native-chat';
import CometChatIncomingCall from '../../../../../Components/Calls/CometChatIncomingCall';
import ImagePicker from 'react-native-image-crop-picker';
import { comeGroupTag } from '../../../../../utils/Constant';
import { toastConfig } from '../../../../../utils/ToastConfig';
import NotificationController from '../../../../../Components/NotificationController';
import Geolocation from '@react-native-community/geolocation';
const { width, height } = Dimensions.get('window');

const radioData = [
    {
        key: "Public",
        title: "Public",
        image: Images.globe,
        detail: "Invite anyone in the community"
    },
    {
        key: "Friends & Groups",
        title: "Friends & Groups",
        image: Images.Members,
        detail: "Invite specific groups and people"
    }
]

const addEvent = (props) => {

    const [eventData] = useState(props.route.params?.eventData)
    const [isFirstFocus, setFirstFocus] = useState(false)
    const [eventName, setEventName] = useState(eventData != undefined ? eventData.event_name : "")
    const [date, setDate] = useState(eventData != undefined ? new Date(eventData.event_start_date) : "");
    const [endDate, setEndDate] = useState(eventData != undefined ? eventData.event_end_time != null ? new Date(eventData.event_end_date) : "" : "");

    const [showDate, setShowDate] = useState(false);

    const [flagDate, setFlagDate] = useState("1")
    let refSheet = useRef();
    let groupRefSheet = useRef();
    const [radioValue, setRadioValue] = useState(eventData != undefined ? eventData.event_participants : "Public")
    const [step, setStep] = useState(1)
    const [imagePath, setImagePath] = useState(eventData != undefined ? eventData.event_image : "")
    const [isDetailFocus, setDetailFocus] = useState(false)
    const [eventAddress, setEventAddress] = useState(eventData != undefined ? {} : null)
    const [token, setToken] = useState("")
    const [group_id] = useState(props.route.params?.group_id)
    const [event_location, setEeventLocation] = useState(eventData != undefined ? eventData.event_location : "")
    const [event_latitude, setLatitude] = useState(eventData != undefined ? eventData.event_latitude : "")
    const [event_longitude, setLongitude] = useState(eventData != undefined ? eventData.event_longitude : "")
    const [isshowing, setShowing] = useState(false)
    const [imageData, setImageData] = useState(eventData != undefined ? { type: "already" } : null)
    const [eventDesc, setDescription] = useState(eventData != undefined ? eventData.event_description : "")
    const [groupsData, setGroupData] = useState([])
    const [friendsData, setFriendData] = useState([])
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [userImage, setImageUser] = useState("")
    const [currentAddress, setCurrentAddress] = useState("")
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            getGroupData(res.token)
            setImageUser(res.u_image)
            // getCurrentAddres();
        })
    }, []);
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getCurrentAddres = () => {

        Geolocation.getCurrentPosition(info => {
            addressGetFromLatLng(info.coords.latitude, info.coords.longitude).then(res => {
                setCurrentAddress(res.results[0].formatted_address)
            })
        }
        );

    }
    const getGroupData = async (token) => {
        await apiCallGetWithToken(apiUrl.get_all_groups_and_friends, token).then(res => {
            if (res.status == true) {
                setGroupData(res.result.groups)
                if (eventData == undefined)
                    setFriendData(res.result.friends)
                else
                    memberManage(res.result.friends,res.result.groups)
                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const memberManage = (data,groupData) => {
        let newArray = [...data]
        for (var i = 0; i < eventData.event_members.length; i++) {
            var index = getIndex(eventData.event_members[i].member.u_id, data)
            if (index > -1)
                newArray[index].isChecked = true
        }
        let newArray1 = [...groupData]
        for (var i = 0; i < eventData.event_groups.length; i++) {
            var index = getIndexGroup(eventData.event_groups[i].eg_group_id, groupData)
            if (index > -1)
                newArray1[index].isChecked = true
        }
        setFriendData(newArray)
        setGroupData(newArray1)
    }
    function getIndex(u_id, data) {
        return data.findIndex(obj => obj.u_id === u_id);
    }
    function getIndexGroup(eg_group_id, data) {
        return data.findIndex(obj => obj.group_id === eg_group_id);
    }
    const onBlur = (flag) => {
        if (flag == "name")
            setFirstFocus(!isFirstFocus)
        else
            setDetailFocus(!isDetailFocus)
    }
    const backClick = () => {
        if (step == 1)
            props.navigation.goBack(null);
        else {
            setStep(step - 1)
        }
    }

    const onChangeDate = (selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        if (flagDate == "1") {
            setDate(currentDate)
            setEndDate("")
        }
        else {
            var duration = moment.duration(moment(currentDate).diff(moment(date)));
            if (duration.asMinutes() > 0)
                setEndDate(currentDate)
            else {
                setEndDate("")
                showMessage(strings.error, "Message", "Must be end datetime larger than start datetime")

            }
        }
    };

    const hideShowDatePicker = () => {
        setShowDate(false);
    };


    const onSave = () => {
        refSheet.close()

        setTimeout(() => {
            if (radioValue != "Public")
                groupRefSheet.open();
        }, 1000);
    }
    const onCheckBoxClick = (index) => {
        let newArray = [...groupsData]
        newArray[index].isChecked = !newArray[index].isChecked
        setGroupData(newArray)
    }
    const onProcesd = () => {
        groupRefSheet.close();
        if (eventData != undefined) {
            addMember()
        }
    }
    const addMember = async () => {
        let data = new FormData();
        setShowing(true)
        data.append("event_id", eventData.event_id)
        for (var i = 0; i < groupsData.length; i++) {
            if (groupsData[i].isChecked != undefined && groupsData[i]?.isChecked)
                data.append("event_groups[]", groupsData[i].group_id);
        }
        for (var i = 0; i < friendsData.length; i++) {
            if (friendsData[i].isChecked != undefined && friendsData[i]?.isChecked)
                data.append("event_members[]", friendsData[i].u_id);
        }
        await imageUploadApi(data, apiUrl.invite_friend_or_group, token).then(res => {
            if (res.status == true) {
                showMessage("success", "Message", res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const onCheckBoxClickFriends = (index) => {
        let newArray = [...friendsData]
        newArray[index].isChecked = !newArray[index].isChecked
        setFriendData(newArray)
    }
    const onSubmitEditing = (text) => {
        if (text.length != 0) {
            setFriendData(friendsData.filter((item) => filterData(item, text)))
            setGroupData(groupsData.filter((item) => filterDataGroup(item, text)))
        }
        else {
            getGroupData(token)
        }
    }
    function filterDataGroup(data, text) {
        return data.group_name.includes(text)
    }
    function filterData(data, text) {
        return data.u_first_name.includes(text) || data.u_last_name.includes(text)
    }
    const groupModal = () => {
        return (
            <ScrollView keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} style={styles.modal}>

                <TouchableOpacity activeOpacity={1}>
                    <SimpleSerach
                        onChangeText={(text) => console.log(text)}
                        placeHolder={strings.serachPlaceholderBottom}
                        isSimpleSerach={true}
                        editable={true}
                        onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onSubmitEditing(text)}

                    />
                    {groupViewBottom()}
                    {friendViewBottom()}
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.verifyBtnGroup}
                        onPress={onProcesd}
                        text={strings.proceed}
                    />
                </TouchableOpacity>
            </ScrollView>
        )
    }
    const groupViewBottom = () => {
        return (
            <View>
                {groupsData.map((item, index) => {
                    var memberList = item.get_last_two_member.map(a => a.u_first_name).toString();
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={{ uri: item.group_image }}></Image>
                                <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                    {item.get_last_two_member.map((item, index) => {
                                        return (
                                            <Image style={[styles.peopleImg, { marginLeft: index != 0 ? -8 : 0 }]} source={{ uri: item.u_image }}></Image>
                                        )
                                    })}
                                </View>
                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.group_name}</Text>
                                    <Text style={styles.txtInvite}>{memberList}</Text>
                                </View>
                                <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClick(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
    const friendViewBottom = () => {
        return (
            <View>
                {friendsData.map((item, index) => {
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={{ uri: item.u_image }}></Image>

                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.u_first_name + " " + item.u_last_name}</Text>
                                    <Text style={styles.txtInvite}>{item.total_mutual_friends + " Mutual Friends " + item.total_pets + " Pets"}</Text>
                                </View>
                                <TouchableOpacity
                                    style={styles.radioCircle}
                                    onPress={() => onCheckBoxClickFriends(index)}
                                >
                                    {item.isChecked &&
                                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                                            <View style={styles.viewCheckBox} />
                                            <Image style={styles.tickImage} source={Images.tickMark}></Image>
                                        </View>
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
            </View>
        )

    }
    const participaView = () => {
        return (

            <View style={styles.modal}>
                {radioData.map((item) => {
                    return (
                        <View style={styles.mainParticipate}>
                            <Image style={styles.imgItem} source={item.image}></Image>
                            <View style={styles.radioView}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.title}</Text>
                                    <Text style={styles.txtInvite}>{item.detail}</Text>
                                </View>
                                <TouchableOpacity
                                    style={item.key == radioValue ? styles.selectradioCircle : styles.radioCircle}
                                    onPress={() => setRadioValue(item.key)}
                                >
                                    {radioValue === item.key && <View style={styles.selectedRb} />}
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
                < CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={styles.verifyBtn}
                    onPress={onSave}
                    text={strings.save}
                />

            </View>
        )
    }
    const onNext = () => {
        if (eventName.trim() == "") {
            showMessage(strings.error, strings.msgTag, "EventName is required!")
        }
        else if (date == "") {
            showMessage(strings.error, strings.msgTag, "Event Start Date is required!")
        }
        else if (endDate == "") {
            showMessage(strings.error, strings.msgTag, "Event End Date is required!")
        }
        // else if (eventAddress == null && step == 2) {
        //     showMessage("error", "Message", "EventAddress is required!")
        // }
        // else if (imageData == null && step == 3) {
        //     showMessage("error", "Message", "Please select event image!")
        // }
        // else if (eventDesc.trim() == "" && step == 3) {
        //     Toast.show("Event Description required!", Toast.SHORT)
        // }
        else {
            if (step != 3)
                setStep(step + 1)
            else {
                createEvent();
            }
        }
    }
    const clickEndDate = () => {
        if (date != "") {
            setShowDate(true)
            setFlagDate("2")
        }
        else
            showMessage(strings.error, strings.msgTag, "Please Select Start datetime!")

    }
    const firstStep = () => {
        return (
            <View style={styles.contentView}>

                <FloatingLabel
                    labelStyle={[styles.labelInput, { marginTop: 20 }]}
                    inputStyle={styles.input}
                    style={isFirstFocus ? styles.focusforminput : styles.formInput}
                    onFocus={() => onBlur("name")}
                    onBlur={() => onBlur("name")}
                    value={eventName}
                    autoCapitalize='none'
                    returnKeyType={'done'}
                    onChangeText={(text) => setEventName(text)}
                >{strings.eventName}</FloatingLabel>

                <FloatingLabel
                    labelStyle={styles.labelInputThirdStep}
                    inputStyle={styles.inputThirdStep}
                    style={isDetailFocus ? [styles.focusforminput] : styles.formInput}
                    onFocus={() => onBlur("detail")}
                    onBlur={() => onBlur("detail")}
                    onChangeText={(text) => setDescription(text)}
                    value={eventDesc}
                    multiline
                >{"About The Play date"}</FloatingLabel>
                <Text style={styles.dobText}>{strings.startDate}</Text>
                <TouchableOpacity onPress={() => (setShowDate(true), setFlagDate("1"))} style={styles.dateClickView}>
                    <TextInput
                        style={styles.inputDate}
                        placeholder="MM/DD/YYYY"
                        editable={false}
                        placeholderTextColor={Colors.white}
                        value={date != "" ? moment(date).format("MM/DD/YYYY h:mm A") : ""}
                        underlineColorAndroid="transparent"
                    />
                    <CalenderIcon />
                </TouchableOpacity>
                <View style={styles.horizontalLine}></View>


                <View style={styles.horizontalLine}></View>
                <View>
                    <Text style={styles.dobText}>{strings.endDate}</Text>
                    <TouchableOpacity onPress={() => clickEndDate()} style={styles.dateClickView}>
                        <TextInput
                            style={styles.inputDate}
                            placeholder="MM/DD/YYYY"
                            editable={false}
                            placeholderTextColor={Colors.white}
                            value={endDate != "" ? moment(endDate).format("MM/DD/YYYY h:mm A") : ""}
                            underlineColorAndroid="transparent"
                        />
                        <CalenderIcon />
                    </TouchableOpacity>

                    <View style={styles.horizontalLine}></View>
                </View>



                <Text style={styles.basicStyle}>
                    {strings.participants}
                </Text>
                <TouchableOpacity
                    onPress={() => refSheet.open()}
                    style={{ marginTop: responsiveHeight(2), justifyContent: "space-between", flexDirection: "row" }}>
                    <Friends opacity={1} />
                    <View>
                        <Text style={styles.txtSelectParti}>{strings.selectParticipants}</Text>
                        <Text style={styles.txtDetailParticipate}>{strings.detailTextParticipate}</Text>
                    </View>
                    <Image style={styles.imgRight} source={Images.arrowright} />
                </TouchableOpacity>
            </View>
        )
    }
    const chooseFile = () => {

        ImagePicker.openPicker({
            compressImageQuality: 0.7,
            cropping: true,
            compressImageMaxWidth: width,
            compressImageMaxHeight: height
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                setImageData(obj)
                setImagePath(image?.path)
            } catch (error) {

            }
        });
    }
    const secondStep = () => {
        return (
            <View style={styles.contentView}>
                <Text style={styles.dobText}>{"Select Location"}</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <AutoSerachComponent
                        style={styles.txtAuto}
                        placeholderTextColor={Colors.opacityColor3}
                        onChangeText={(text) => setEeventLocation(text)}
                        event_location={event_location}
                        onPress={(data) => clickAddress(data)}
                    />
                </View>
                <View style={styles.horizontalLine}></View>
            </View>
        )
    }
    const clickAddress = (data) => {
        setEventAddress(data)
        setEeventLocation(data?.formatted_address)
        setLatitude(data?.geometry?.location?.lat)
        setLongitude(data?.geometry?.location?.lng)

    }
    const thirdStep = () => {
        return (
            <View style={styles.contentView}>
                <View>
                    <View style={styles.mainViewStep3}></View>
                    {imagePath == "" ?
                        <TouchableOpacity style={[styles.subView]} onPress={() => chooseFile()}>
                            <View style={{ alignSelf: "center" }}>
                                <UploadImageIcon />
                            </View>
                            <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                        </TouchableOpacity> :
                        <View style={styles.subView}>
                            <Image style={styles.profileStyle} source={{ uri: imagePath }}></Image>
                            <TouchableOpacity style={styles.editView} onPress={() => chooseFile()}>
                                <View style={{ alignSelf: "center" }}>
                                    <EditIcon width={15} height={15} />
                                </View>

                            </TouchableOpacity>
                        </View>
                    }
                </View>

            </View>
        )
    }

    const createEvent = async () => {
        var url = apiUrl.create_event

        let data = new FormData();
        if (imageData?.type != "already" && imageData != null)
            data.append("event_image", { type: imageData.type, uri: imageData.uri, name: imageData.fileName });
        data.append("event_name", eventName);
        data.append("event_description", eventDesc);
        data.append("event_start_date", date.getFullYear() + "-" + parseInt(date.getMonth() + 1) + "-" + date.getDate());
        data.append("event_start_time", moment(date).format('H:MM'));
        if (endDate != "")
            data.append("event_end_date", endDate.getFullYear() + "-" + parseInt(endDate.getMonth() + 1) + "-" + endDate.getDate());
        if (endDate != "")
            data.append("event_end_time", moment(endDate).format('H:MM'));
        data.append("event_location", event_location);
        data.append("event_latitude", event_latitude);
        data.append("event_longitude", event_longitude);
        data.append("event_participants", radioValue);
        if (group_id != undefined) {
            data.append("event_groups[]", group_id);
            data.append("event_participants", "Friends & Groups");
        }
        if (eventData != undefined) {
            data.append("event_id", eventData.event_id);
            url = apiUrl.edit_event
        }
        else {
            for (var i = 0; i < groupsData.length; i++) {
                if (groupsData[i].isChecked != undefined && groupsData[i]?.isChecked)
                    data.append("event_groups[]", groupsData[i].group_id);
            }
            for (var i = 0; i < friendsData.length; i++) {
                if (friendsData[i].isChecked != undefined && friendsData[i]?.isChecked)
                    data.append("event_members[]", friendsData[i].u_id);
            }
        }
        setShowing(true)
        await apiCallWithTokenAddPet(data, url, token).then(res => {
            if (res.status) {
                if (eventData == undefined)
                    createPost(res.result)
                else {
                    setTimeout(() => {
                        setShowing(false)
                        props.navigation.reset({
                            index: 0,
                            routes: [{ name: strings.TabNavigation }],
                        });
                    }, 1000);
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const createPost = async (type) => {
        let data = new FormData();
        data.append("post_name", eventName);

        data.append("post_type", "Event")

        data.append("post_event_id", type.event_id)

        //  data.append("post_location", currentAddress)
        var url = apiUrl.create_post
        await imageUploadApi(data, url, token).then(res => {
            for (var i = 0; i < groupsData.length; i++) {
                if (groupsData[i].isChecked != undefined && groupsData[i]?.isChecked) {
                    sendEventMessage(comeGroupTag + groupsData[i].group_id, userImage, type, "Event").then(res => {
                        sendChatNotification("Message from " + groupsData[i].group_name,
                            type + " Share in " + groupsData[i].group_name, groupsData[i].group_id)
                    })
                }
            }
            setTimeout(() => {
                setShowing(false)
                props.navigation.reset({
                    index: 0,
                    routes: [{ name: strings.TabNavigation }],
                });
            }, 3000);
        })

    }
    const sendChatNotification = async (title, message, group_id) => {
        var obj = {
            title: title,
            message: message,
            group_id: group_id,
        }
        await apiCallWithToken(obj, apiUrl.send_chat_notifications, token).then(res => {
            setShowing(false)
        })
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)
                // handle exception
            }
        );
    }
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

            <View style={{ flex: 1 }}>

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.container}
                >
                    {Platform.OS == "android" ?
                        <CommonStatusBar color={Colors.backgroundColor} /> : <></>}
                    <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                    <KeyboardAvoidingView
                        behavior={Platform.OS === "ios" ? "padding" : "height"}>
                        <SafeAreaView style={{ marginTop: 30 }} >
                            <HeaderView onPress={backClick} onSkip={onNext} textSkip={strings.next} isPhoto={true}
                                text={step == 1 ? strings.createEvent : step == 2 ? strings.selectLocation : strings.addDtail}
                                isClose={step == 1 ? true : false}
                            />
                            <Text style={styles.txtlblStep}>{"Step " + step + " of 3"}</Text>
                            <View style={styles.mainView}>
                                <LinearGradient
                                    colors={Colors.btnBackgroundColor}
                                    style={step == 1 ? { height: 4, flex: 0.33 } : step == 2 ? { height: 4, flex: 0.67 } : { height: 4, flex: 0.92 }}
                                />
                                <View style={step == 1 ? styles.step1 : step == 2 ? styles.step2 : styles.step3} />
                            </View>
                            <View style={{ marginBottom: 20 }} >

                                {step == 1 ? firstStep() :
                                    step == 2 ? secondStep() :
                                        thirdStep()}

                                {showDate && (
                                    <DateTimePickerModal
                                        isVisible={showDate}
                                        mode="datetime"
                                        //  date={flagDate == "1" ? date : endDate}
                                        onConfirm={onChangeDate}
                                        minuteInterval={100}
                                        minimumDate={flagDate == "1" ? new Date() : new Date(date)}
                                        onCancel={hideShowDatePicker}
                                    />
                                )}


                            </View>
                            <RBSheet
                                ref={ref => {
                                    refSheet = ref;
                                }}
                                height={270}
                                openDuration={250}
                                closeOnDragDown={true}
                                customStyles={{
                                    container: {
                                        justifyContent: "center",
                                        alignItems: "center",
                                        borderTopRightRadius: 20,
                                        borderTopLeftRadius: 20,
                                        backgroundColor: Colors.dialogBackground,
                                        padding: 10,
                                    }
                                }}
                            >
                                {participaView()}

                            </RBSheet>
                            <RBSheet
                                ref={ref => {
                                    groupRefSheet = ref;
                                }}
                                height={438}
                                openDuration={250}
                                closeOnDragDown={true}
                                customStyles={{
                                    container: {
                                        justifyContent: "center",
                                        alignItems: "center",
                                        borderTopRightRadius: 20,
                                        borderTopLeftRadius: 20,
                                        backgroundColor: Colors.dialogBackground,
                                        padding: 10,
                                    }
                                }}
                            >
                                {groupModal()}
                            </RBSheet>
                        </SafeAreaView>

                    </KeyboardAvoidingView>
                </LinearGradient>
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
                {Platform.OS == "ios" &&
                    <CometChatIncomingCall callData={callData} callCancel={callCancel}
                        isVisible={isVisible} callAccept={callAccept} />
                }
                {isVisible == false && Platform.OS == "ios" &&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisible}
                        onMessageNotification={() => { }}

                    />
                }
            </View>
        </TouchableWithoutFeedback>
    )
}
export default addEvent;