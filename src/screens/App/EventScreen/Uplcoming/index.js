import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './UpcomingStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
const { width } = Dimensions.get("window")
import { PlusIcon } from '../../../../utils/svg/PlusIcon';
import { PlaceIconUnselect } from '../../../../utils/svg/PlaceIconUnselect';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, getGroupId, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';

import Toast from 'react-native-toast-message';
import { strings } from '../../../../Resources/Strings';
import { toastConfig } from '../../../../utils/ToastConfig';
import NotificationController from '../../../../Components/NotificationController';

const Upcoming = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [eventData, setEventData] = useState([])
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [u_id, setUserId] = useState()

    useEffect(() => {
        props.navigation.addListener('focus', () => {
            if (props.index == 0) {

                getUserData().then(res => {
                    setShowing(true)
                    setEventData([])
                    setUserId(res.u_id)
                    getGroupId().then(group_id => {
                        if (group_id == "" || group_id == undefined)
                            getEventData(res)
                        else
                            getGroupWiseData(group_id, res)

                    })
                })
            }

        })
    }, [props.index, props.navigation]);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getEventData = async (res) => {
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", res.token).then(res => {
            setShowing(false)

            if (res.status == true) {
                if (res.result.length != 0)
                    setEventData(res.result)

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const getGroupWiseData = async (group_id, res) => {
        await apiCallGetWithToken(apiUrl.get_all_events_by_group_id + "?event_type=upcoming&group_id=" + group_id, res.token).then(res => {
            if (res.status == true) {
                if (res.result.length != 0)
                    setEventData(res.result)
                else
                    showMessage(strings.error, strings.msgTag, res.message)
                setShowing(false)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderItem = ({ item, index }) => {
        var day = new Date(item.event_start_date).getDate();
        var month = new Date(item.event_start_date).getMonth();
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.EventDetailUpcomingScreen, { case: u_id == item.added_by.u_id ? "case3" : "case1", item: item })}
                style={styles.renderStyle} >
                <Image source={{ uri: item.event_image }} style={styles.imgStyle}></Image>
                <View style={styles.upperView}>
                    <View style={styles.upperSubView}>
                        <View style={{ flexDirection: "row" }}>
                            {item.event_members.map((data, parentIndex) => (
                                parentIndex < 3 ?
                                    parentIndex != 2 && parentIndex < 2 ?
                                        <Image style={[styles.peopleImg, { marginLeft: -8 }]} source={{ uri: data.member.u_image }}></Image> :
                                        <View style={styles.moreView}>
                                            <View style={styles.moreBorderView}></View>
                                            <Text style={styles.txtMore}>{"+ " + parseInt(item.event_members.length - 2)}</Text>
                                        </View> : <></>
                            ))}
                        </View>
                        <View style={styles.dateView}>
                            <Text style={styles.txtDate}>{day}</Text>
                            <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                        </View>
                    </View>
                </View>
                <LinearGradient style={{
                    position: "absolute",
                    height: responsiveHeight(18),
                    width: width, bottom: 0,
                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomView}>
                    <Text style={styles.txtTitle}>{item.event_name}</Text>
                    <View style={styles.bottomBorderView}></View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
                        <PlaceIconUnselect height={15} width={15} />
                        <Text style={styles.txtAddress}>{item.event_location}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >

                <View style={styles.contentView}>
                    <FlatList
                        data={eventData}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                    />

                    <LinearGradient
                        colors={Colors.btnBackgroundColor}
                        style={styles.flotingView}
                    >
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('addEvent')}
                            style={styles.plusView}>
                            <PlusIcon />
                        </TouchableOpacity>
                    </LinearGradient>

                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {/* {Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={false}
                />
            } */}
        </View>
    )
}
export default Upcoming