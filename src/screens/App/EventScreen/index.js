import React, { useEffect, useState } from 'react';
import { View, Text, useWindowDimensions, Platform } from 'react-native';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import Upcoming from './Uplcoming';
import PastEvent from './PastEvent';
import { Fonts } from '../../../Resources/Fonts';
import NotificationController from '../../../Components/NotificationController';

const EventScreen = (props) => {
    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes, setRoute] = React.useState([
        { key: 'Upcoming', title: 'Upcoming' },
        { key: 'PastEvents', title: 'Past Play Dates' },
    ]);
    // useEffect(() => {
    //     props.navigation.addListener('focus', () => {
    //         setIndex(index)
    //     });
    // },[index]);
    const renderScene = ({ route, index1 }) => {
        switch (route.key) {
            case 'Upcoming':
                return <Upcoming navigation={props.navigation} index={index} />;
            case 'PastEvents':
                return <PastEvent navigation={props.navigation} flag={true} index={index} />
            default:
                return null;
        }
    };
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color, fontSize: 12, fontWeight: "500", fontFamily: Fonts.DMSansRegular, lineHeight: 15.62 }}>
                    {route.title}
                </Text>
            )}
        />
    );
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height: Platform.OS == "ios" ? responsiveHeight(12.4) : responsiveHeight(8.5) }}
            >
                <AppHeader
                    navigation={props.navigation}
                    Tab={true}
                    title={'Play dates'} />

            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}
            />
           
           {/* {Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={false}
                />
            } */}
        </View >
    )
}

export default EventScreen;