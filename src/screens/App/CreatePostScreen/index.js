import React, { useState, useRef, useEffect, createRef } from 'react';
import {
    TouchableOpacity, View, Image, Text, TextInput, Platform,
    PermissionsAndroid, FlatList,
    Dimensions,
    ScrollView,
} from 'react-native';
import { strings } from '../../../Resources/Strings';
import HeaderView from '../../../Components/HeaderView';
import styles from './CreatePostStyles';
import { Images } from '../../../Resources/Images';
import { Colors } from '../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import {
    launchCamera,
    launchImageLibrary
} from 'react-native-image-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../Components/CommonStatusBar';
import { EventIconUnselect } from '../../../utils/svg/EventIconUnselect';
import { apiCallGetWithToken, apiCallWithToken, getLocationData, getUserData, imageUploadApi, storeLocationData, addressGetFromLatLng } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
const { width, height } = Dimensions.get('window');
import Toast from 'react-native-toast-message';
import Loader from '../../../Components/Loader';
import CommonButton from '../../../Components/CommonButton';
import EventMap from '../../../Components/EventMap';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { toastConfig } from '../../../utils/ToastConfig';
import NotificationController from '../../../Components/NotificationController';
import Geolocation from '@react-native-community/geolocation';

const CreatePostScreen = (props) => {
    const [imagePath, setImagePath] = useState(null);
    const [selectedEventRadio, setSelectedEventRadio] = useState();
    const [eventData, setEventData] = useState([])
    let refSheet = useRef();
    let sheetRef = useRef();
    const [isshowing, setShowing] = useState(false)
    const [serachText, setSerachText] = useState("")
    const [userImage, setUserImage] = useState("")
    const [postEvent, setPostEvent] = useState(null)
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [locationData, setLocationData] = useState(null)
    const [description, setDescription] = useState("")
    const [imageData, setImageData] = useState(null)
    const [token, setToken] = useState("")
    const [imageList, setImageList] = useState([])
    const [u_address, setAddress] = useState("")
    const [postData] = useState(props.route.params?.postData != undefined ? props.route.params.postData : null)
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [currentAddress, setCurrentAddress] = useState("")
    useEffect(() => {
        storeLocationData(null)
        getUserData().then(res => {
            setUserImage(res?.u_image)
            setToken(res.token)
            setAddress(res.u_address)
            getEventData(res)
            if (postData != null) {
                setPostEditData()
            }
            //  setPostEditData()
        })

        //getCurrentAddres();
        props.navigation.addListener('focus', () => {
            setLocationData(null)
            getLocationData().then(res => {
                if (res != null) {
                    setLocationData(res)
                    console.log(res)
                }
                else {
                    if (postData != null) {
                        setPostEditData()
                    }
                }
            })
        })

    }, []);

    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                //  console.log("Incoming call coming:", call);
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
                setVisible(false)
                props.navigation.goBack(null)

            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
                props.navigation.goBack(null)

            }
        })
    );
    const getFilename = (url) => {
        var filename = url.substring(url.lastIndexOf('/') + 1);
        return filename;
    }
    const setPostEditData = () => {
        setDescription(postData.post_name)
        // if (postData.post_type == "Photo") {
        //     setImagePath(postData.post_image)
        //     var obj = {
        //         uri: postData.post_image,
        //         type: "image/jpeg",
        //         fileName: getFilename(postData.post_image)
        //     }
        //     setImageData(obj)
        // }
        if (postData.post_latitude != null && postData.post_latitude != "") {
            var obj = {
                geometry: {
                    location: {
                        lat: postData.post_latitude,
                        lng: postData.post_longitude
                    }
                },
                formatted_address: postData.post_location

            }
            setLocationData(obj)
        }
        if (postData.post_event != undefined && postData.post_event != null && isEmpty(postData.post_event) == false) {
            setPostEvent(postData.post_event)
        }
        if (postData.post_images.length != 0) {
            let newArray = [...imageList]
            for (var i = 0; i < postData.post_images.length; i++) {
                var obj = {
                    uri: postData.post_images[i].post_image_image,
                    type: "image/jpeg",
                    fileName: getFilename(postData.post_images[i].post_image_image),
                    post_image_id: postData.post_images[i].post_image_id
                }
                newArray.push(obj)
            }
            setImageList(newArray)
        }
    }
    function isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }
    const deletePostMedia = async (post_image_id) => {
        setShowing(true)
        var obj = {
            post_id: postData.post_id,
            post_image_id: post_image_id
        }
        await apiCallWithToken(obj, apiUrl.delete_post_media, token).then(res => {
            setShowing(false)
        })
    }
    const getEventData = async (res) => {
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", res.token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setEventData(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)

                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const backClick = () => {
        props.navigation.goBack(null)
    }
    const getType = () => {
        var str = ""
        if (imagePath != null)
            str += "1,";
        if (locationData != null)
            str += "2,"
        if (postEvent != null)
            str += "3,"
        if (imageList.length != 0)
            str += "4";
        str = str.slice(0, -1)
        return str;
    }
    const getCurrentAddres = () => {

        Geolocation.getCurrentPosition(info => {
            addressGetFromLatLng(info.coords.latitude, info.coords.longitude).then(res => {
                setCurrentAddress(res.results[0].formatted_address)
            })
        }
        );

    }
    const createPost = async () => {
        // if (locationData == null && postEvent == null && imagePath == null && imageList.length == 0) {
        //     showMessage(strings.error, strings.msgTag, "Please Select Any Post Type!")
        //     return;
        // }
        let type = getType();
        let data = new FormData();
        data.append("post_name", description);
        //if (imagePath != null)
        //  data.append("post_image", { type: imageData?.type, uri: imageData?.uri, name: imageData?.fileName });
        data.append("post_type",type==""?"7":type)


        if (locationData != null) {
            data.append("post_location", locationData?.name)
            data.append("post_latitude", locationData?.geometry?.location?.lat)
            data.append("post_longitude", locationData?.geometry?.location?.lng)
        }
        else {
            //data.append("post_location", currentAddress)
        }
        if (postEvent != null && postEvent != undefined)
            data.append("post_event_id", postEvent.event_id)



        setShowing(true)

        var url = apiUrl.create_post
        if (postData != null) {
            data.append("post_id", postData.post_id)
            url = apiUrl.edit_post
            if (postData.group != null && postData.group != undefined)
                data.append("post_group_id", postData.group.group_id)
        }

        for (var i = 0; i < imageList.length; i++)
            data.append("images[]", { type: imageList[i]?.type, uri: imageList[i]?.uri, name: imageList[i]?.fileName })

        await imageUploadApi(data, url, token).then(res => {
            if (res.status) {
                setTimeout(() => {
                    // showMessage("success", "Message", res.message)
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.TabNavigation }],
                    });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)

                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }

    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };

    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                // If WRITE_EXTERNAL_STORAGE Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };

    const chooseFile = () => {
        let options = {
            mediaType: "photo",
            quality: 0.5,
            selectionLimit: 0,
            maxWidth: width,
            maxHeight: height,
            //      allowsEditing: false,

        };
        launchImageLibrary(options, (response) => {

            if (response.didCancel) {
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                return;
            } else if (response.errorCode == 'permission') {
                return;
            } else if (response.errorCode == 'others') {
                return;
            }


            // setPostEvent(null)
            //setLocationData(null)
            // if (response.assets.length <= 1) {
            //     setImagePath(response.assets[0].uri)
            //     setImageData(response.assets[0])
            //     setImageList([])
            // }
            //else 
            {
                setImageList(response.assets.concat(imageList))
                setImageData(null)
                setImagePath(null)

            }

        });
    };

    const captureImage = async (type) => {
        let options = {
            mediaType: type,
            quality: 0.5,
            saveToPhotos: true,
            maxWidth: width,
            maxHeight: height,
            // maxWidth: 500, maxHeight: 500, 
            // allowsEditing: false,
        };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            launchCamera(options, (response) => {
                if (response.didCancel) {
                    //alert('User cancelled camera picker');
                    return;
                } else if (response.errorCode == 'camera_unavailable') {
                    alert('Camera not available on device');
                    return;
                } else if (response.errorCode == 'permission') {
                    alert('Permission not satisfied');
                    return;
                } else if (response.errorCode == 'others') {
                    alert(response.errorMessage);
                    return;
                }
                setImageList(response.assets.concat(imageList))
                setImageData(null)
                setImagePath(null)

                //setPostEvent(null)
                //setLocationData(null)

                // setImagePath(response.assets[0].uri)
                // setImageData(response.assets[0])
                // setImageList([])


            });
        }
    };

    const createEvent = () => {
        refSheet.open()
        //sheetRef.current.snapTo(0)
    }

    const selectedEvent = (item) => {
        // setImageList([])
        // setImagePath(null)
        setPostEvent(item)
        //  setLocationData(null)
        setSelectedEventRadio(item.event_id)
    }
    const renderFooter = () => {
        return (
            <CommonButton
                btnlinearGradient={styles.btnlinearGradient}
                colors={Colors.btnBackgroundColor}
                btnText={styles.btnText}
                viewStyle={styles.verifyBtnGroup}
                onPress={() => refSheet.close()}
                text={strings.proceed}
            />
        )
    }
    const clickLocationView = () => {
        // setPostEvent(null)
        //   setLocationData(null)
        //  setImageList([])
        //  setImagePath(null)
        props.navigation.navigate(strings.searchMapScreen, { currentFlag: true })
    }
    const EventList = () => {
        return (
            <View style={styles.eventListContainer}>
                <FlatList
                    data={eventData}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={renderListHeader}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={renderFooter}
                />
            </View>
        )
    }
    const locationView = () => {
        return (
            <View >
                <View>
                    <EventMap lat={locationData?.geometry?.location?.lat} long={locationData?.geometry?.location?.lng} style={styles.imgMap} />
                </View>
                <TouchableOpacity style={{ top: 15, position: "absolute", right: 10 }} onPress={() => setLocationData(null)}>
                    <Image source={Images.closeIcon} style={[styles.uploadImageClose]} />
                </TouchableOpacity>
            </View>
        )
    }
    const renderItem = ({ item, index }) => {
        return (
            <View>
                <View style={styles.createEventContainer}>
                    <View style={styles.createEventImage}>
                        <Image source={{ uri: item.event_image }} style={styles.eventImage} />
                    </View>
                    <View style={[styles.smallIcon2, {
                        backgroundColor: Colors.roundBackground, borderWidth: 1,
                        borderColor: Colors.theme
                    }]}>
                        <Image source={Images.addEventIcon} style={styles.smallEventIcon} resizeMode={'contain'} />
                    </View>
                    <View style={styles.eventItem}>
                        <View>
                            <Text style={styles.eventTitle}>{item.event_name}</Text>
                            <Text style={styles.eventTime}>{item.event_start_date + " To " + item.event_end_date} </Text>
                        </View>
                        <TouchableOpacity style={[styles.radioBtn, { borderColor: selectedEventRadio == item.event_id ? Colors.primaryViolet : Colors.white }]} onPress={() => selectedEvent(item)}>
                            <View style={selectedEventRadio == item.event_id ? styles.radioBtnSelect : null}></View>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        )
    }
    const onEndEditing = async (serachText) => {
        if (serachText.trim() != "") {
            const newData = await eventData.filter(item => {
                const itemData = item.event_name.toUpperCase()
                const textData = serachText.toUpperCase();
                return itemData.indexOf(textData) > -1
            });
            setEventData(newData)
            setSerachText(serachText)
        }
        else {
            getUserData().then(res => {
                getEventData(res)
            })
        }
    }
    const onAddEvent = () => {
        refSheet.close()
        setTimeout(() => {
            props.navigation.navigate(strings.addEventScreen)
        }, 500);
    }
    const renderListHeader = () => {
        return (
            <View style={styles.bottomsheetHeader}>
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheet.close()}>

                </TouchableOpacity>
                <View style={styles.textInputSearch} >
                    <Image source={Images.serachIcon} style={styles.serachIcon} resizeMode={'contain'} />
                    <TextInput
                        placeholder={strings.eventSearchPlace}
                        placeholderTextColor={Colors.white}
                        //multiline={true}
                        //value={serachText}
                        // onChangeText={(text) => setSerachText(text)}
                        onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onEndEditing(text)}
                        style={styles.searchBox}
                    />
                </View>
                <View>
                    <TouchableOpacity style={styles.createEventContainer}
                        onPress={() => onAddEvent()}
                    >
                        <View style={styles.createEventImage}>
                            <EventIconUnselect height={18} width={18} opacity={1} />
                        </View>
                        <View style={[styles.smallIcon, {
                            backgroundColor: Colors.primaryViolet,
                            borderWidth: 1, borderColor: Colors.theme, alignItems: "center", justifyContent: "center"
                        }]}>
                            {/* <Text style={{ alignSelf: "center" }}>+</Text> */}
                            <Image style={{ height: 12, width: 12 }} source={Images.plusIcon}></Image>

                        </View>
                        <Text style={styles.createEvent}>{strings.createEventNew}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    const eventView = () => {
        var day = new Date(postEvent.event_start_date).getDate();
        var month = new Date(postEvent.event_start_date).getMonth();
        return (
            <View >

                <Image source={{ uri: postEvent?.event_image }} style={styles.petImage} />
                <View style={styles.upperView}>

                    <View style={styles.upperSubView}>
                        <View style={styles.dateView}>
                            <Text style={styles.txtDate}>{day}</Text>
                            <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                        </View>
                        <TouchableOpacity style={styles.uploadImageCloseContainer} onPress={() => setPostEvent()}>
                            <Image source={Images.closeIcon} style={[styles.uploadImageClose]} />
                        </TouchableOpacity>
                    </View>

                </View>
                <LinearGradient style={{ width: width - 50, height: 110, position: "absolute", bottom: 0 }}
                    colors={Colors.viewGroupContain} />

                <View style={{ margin: 10, bottom: 5, position: 'absolute' }}>
                    <Text style={styles.petName}>
                        {postEvent?.event_name}
                    </Text>
                    <View style={styles.line} />
                    <Text style={styles.scientificName}>
                        {postEvent?.event_location}
                    </Text>
                </View>
            </View>
        )
    }
    const removeImage = (index, item) => {
        let newArray = [...imageList]
        if (index > -1) {
            newArray.splice(index, 1);
        }
        setImageList(newArray)
        if (item.post_image_id != undefined) {
            deletePostMedia(item.post_image_id)
        }

    }
    const renderMultipleImage = ({ item, index }) => {
        return (
            <View style={{ marginRight: 20 }}>
                <Image source={{ uri: item.uri }} style={styles.uploadedMultiImage} resizeMode={'cover'} />
                <TouchableOpacity style={styles.uploadImageCloseContainer} onPress={() => removeImage(index, item)}>
                    <Image source={Images.closeIcon} style={styles.uploadImageClose} />
                </TouchableOpacity>
            </View>
        )

    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)
                // handle exception
            }
        );
    }
    return (
        // <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={true}>
        <View style={[styles.container, { paddingTop: 40 }]}>
            <CommonStatusBar color={Colors.backgroundColor} />
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <HeaderView
                isPhoto={true}
                onPress={backClick}
                text={strings.createPost}
                textSkip={strings.postStr}
                onSkip={createPost}
            />
            <ScrollView
                keyboardShouldPersistTaps='handled'
                showsVerticalScrollIndicator={false}
                style={{ marginBottom: responsiveHeight(20) }}
            >
                <View style={styles.postContainer}>
                    <Image source={{ uri: userImage }} style={styles.profileIcon} />
                    <TextInput
                        style={styles.textInput}
                        placeholderTextColor={Colors.white}
                        placeholder={strings.writeSomething}
                        onChangeText={(text) => setDescription(text)}
                        value={description}
                        multiline={true} />
                </View>
                <View style={styles.ImagePost}>
                    {imagePath &&
                        <View>
                            <Image source={{ uri: imagePath }} style={styles.uploadedImage} resizeMode={'cover'} />
                            <TouchableOpacity style={styles.uploadImageCloseContainer} onPress={() => setImagePath(null)}>
                                <Image source={Images.closeIcon} style={styles.uploadImageClose} />
                            </TouchableOpacity>
                        </View>
                    }
                    {postEvent != null ? eventView() : <></>}
                    {locationData != null ? locationView() : <></>}
                    {imageList.length != 0 &&
                        <FlatList
                            data={imageList}
                            horizontal
                            renderItem={renderMultipleImage}
                            style={{ height: 172, width: width * 0.7 }}
                        >

                        </FlatList>
                    }
                </View>
                <RBSheet
                    ref={ref => {
                        refSheet = ref;
                    }}
                    height={height * 0.7}
                    openDuration={250}
                    closeOnDragDown={false}
                    customStyles={{
                        container: {
                            justifyContent: "center",
                            alignItems: "center",
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 10
                        }
                    }}
                >
                    {EventList()}
                </RBSheet>
            </ScrollView>

            <View style={styles.footer}>
                {(postData == null || postData?.post_type == "Photo") &&
                    <View style={styles.footerbtn}>
                        <TouchableOpacity style={styles.cameraContent} onPress={() => captureImage('photo')}>
                            <Image source={Images.cameraIcon} style={styles.Icons} resizeMode={'contain'} />
                        </TouchableOpacity>
                        <Text style={styles.footerText}>Camera</Text>
                    </View>
                }
                {(postData == null || postData?.post_images.length != 0) &&
                    <View style={styles.footerbtn}>
                        <TouchableOpacity style={styles.cameraContent} onPress={() => chooseFile()}>
                            <Image source={Images.addPicture} style={styles.Icons} resizeMode={'contain'} />
                        </TouchableOpacity>
                        <Text style={styles.footerText}>Photo</Text>
                    </View>}
                {(postData == null || isEmpty(postData.post_event) == false) &&
                    <View style={styles.footerbtn}>
                        <TouchableOpacity style={styles.cameraContent} onPress={() =>

                            createEvent()}>
                            <EventIconUnselect opacity={1} />
                            {/* <Image source={Images.cameraIcon} style={styles.Icons} resizeMode={'contain'} /> */}
                        </TouchableOpacity>
                        <Text style={styles.footerText}>Play Date</Text>
                    </View>
                }
                {(postData == null || postData?.post_latitude != null) &&

                    <View style={styles.footerbtn}>
                        <TouchableOpacity onPress={() => clickLocationView()} style={styles.cameraContent}>
                            <Image source={Images.placeIcon} style={styles.Icons} resizeMode={'contain'} />
                        </TouchableOpacity>
                        <Text style={styles.footerText}>Location</Text>
                    </View>
                }
            </View>

            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel} isVisible={isVisible} callAccept={callAccept} />}
            {isVisible == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisible}
                    onMessageNotification={()=>{}}

                />
            }
        </View>
    )
}

export default CreatePostScreen;