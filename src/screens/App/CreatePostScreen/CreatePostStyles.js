import { StyleSheet, Dimensions } from "react-native";
import { responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../Resources/Colors";
import { Fonts } from "../../../Resources/Fonts";

const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    footer: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: responsiveHeight(4),
    },
    footerbtn: {
        width: width / 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraContent: {
        backgroundColor: Colors.opacityColor2,
        width: 68,
        height: 68,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 60,
        marginHorizontal: 15
    },
    Icons: {
        width: 18,
        height: 18,
        tintColor: Colors.white,
    },
    footerText: {
        color: Colors.white,
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginTop: 10,
        fontFamily: Fonts.DMSansRegular

    },
    profileIcon: {
        width: 40,
        height: 40,
        borderRadius: 40,
        marginRight: 10
    },
    postContainer: {
        marginTop: 30,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textInput: {
        width: width - 100,
        color: Colors.white,
        opacity: 0.6,
        fontSize: 16,
        fontWeight: '400',
        fontFamily: Fonts.DMSansRegular

    },
    textInputSearch: {
        width: width * 0.95,
        height: 40,
        backgroundColor: '#FFFFFF26',
        borderRadius: 5,
        paddingHorizontal: 5,
        color: Colors.white,
        opacity: 0.4,
        fontSize: 14,
        flexDirection: 'row',
        alignItems: 'center',
        fontFamily: Fonts.DMSansRegular

    },
    serachIcon: {
        width: 15,
        height: 15,
        tintColor: Colors.white,
        marginHorizontal: 10
    },
    searchBox: {
        marginTop: 5,
        width: width * 0.85,
        height: 40,
        alignItems: 'center',
        color: Colors.white,
        opacity: 0.4,
        fontSize: 12,
        fontFamily: Fonts.DMSansRegular

    },
    drawer: {
        borderWidth: 2,
        width: 50,
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 15,
        borderColor: '#FFFFFF26'
    },
    eventListContainer: {
        paddingHorizontal: 10
    },
    createEventContainer: {
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center'
    },
    createEventImage: {
        backgroundColor: '#FFFFFF26',
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 60,
        marginRight: 16,

    },
    createEvent: {
        color: Colors.primaryViolet,
        fontSize: 14,
        fontWeight: '400'
    },
    smallIcon: {
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        position: 'absolute',
        top: 30,
        left: 32
    },
    smallIcon2: {
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        position: 'absolute',
        top: 30,
        left: 32
    },
    eventImage: {
        width: 48,
        height: 48,
        borderRadius: 48
    },
    smallEventIcon: {
        width: 10,
        height: 10,
        tintColor: Colors.white,
    },
    eventTitle: {
        fontWeight: '400',
        fontSize: 14,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    eventTime: {
        fontWeight: '400',
        fontSize: 12,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular

    },
    eventItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width * 0.7
    },
    radioBtn: {
        borderWidth: 1,
        width: 20,
        height: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioBtnSelect: {
        width: 10,
        height: 10,
        backgroundColor: Colors.primaryViolet,
        borderRadius: 10
    },
    ImagePost: {
        alignItems: 'flex-end',
        paddingRight: 20,
        marginTop: 20,
    },
    uploadedImage: {
        width: width * 0.75,
        height: 172,
        borderRadius: 4
    },
    uploadImageCloseContainer: {
        position: 'absolute',
        top: 10,
        right: 10,

    },
    uploadImageClose: {
        width: 20,
        height: 20,
        tintColor: Colors.white,
        backgroundColor: 'black',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    //event view
    petImage: {
        width: width * 0.75,
        height: 160,
        borderRadius: 4,

    },
    petName: {
        color: Colors.white,
        fontSize: 22,
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.2,
        lineHeight: 30,

    },
    line: { height: 1, backgroundColor: "#FFFFFF", width: width * 0.55, marginVertical: 10, opacity: 0.5, },
    scientificName: {
        color: Colors.white,
        fontSize: 14,
        opacity: 0.7,
        width: "90%"
    },
    upperView: { position: "absolute", width: "77%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row" },
    dateView: { height: 37, width: 36, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2, margin: 10 },
    txtDate: {
        fontSize: 16, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 10, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },

    //common button style
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20

    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
    },
    verifyBtnGroup: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "100%", alignSelf: "center" },

    //map view 
    imgMap: { height: 160, width: width *0.75, borderRadius: 4, marginVertical: responsiveHeight(1) },

    //multiple image view
    uploadedMultiImage: {
        width: 150,
        height: 150,
        borderRadius: 4
    },


})