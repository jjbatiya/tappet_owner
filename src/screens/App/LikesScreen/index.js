import React, { useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, Platform } from 'react-native';

import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import styles from './LikesStyle';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { strings } from '../../../Resources/Strings';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import NotificationController from '../../../Components/NotificationController';

const LikesScreen = (props) => {
    const [likeData] = useState(props.route.params.likeData)
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const renderView = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate(strings.messageScreen)} style={styles.contentView}>
                <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>

                <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>

            </TouchableOpacity>
        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={strings.likes}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={likeData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >

                </FlatList>
            </LinearGradient>
            {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel} 
            isVisible={isVisible} callAccept={callAccept} />}
             {isVisible==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisible}
                        onMessageNotification={()=>{}}

                    />
                }
        </View>
    )
}

export default LikesScreen;