import { StyleSheet } from "react-native";
import { responsiveWidth,responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../Resources/Colors";
import { Fonts } from "../../../Resources/Fonts";

export default styles = StyleSheet.create({
    container : {
        flex: 1,
        
    },
    contentView:{flexDirection:"row",marginVertical:responsiveHeight(1)},
    imgStyle:{
        height:40,
        width:40,
        borderRadius:25
    },
    txtName:{
        color:Colors.white,
        fontSize:14,
        fontWeight:"400",
        lineHeight:18.23,
        alignSelf:"center",
        marginLeft:responsiveWidth(3),
        fontFamily:Fonts.DMSansRegular
    }
})
