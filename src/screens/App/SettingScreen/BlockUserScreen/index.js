import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import styles from '../SettingStyle';
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { comeChatUserTag } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { toastConfig } from '../../../../utils/ToastConfig';


const BlockUserScreen = (props) => {
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [blockUserData, setBlockUserData] = useState([])
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            getBlocksUser(res.token)
        })
    }, []);
    const getBlocksUser = async (token) => {

        await apiCallGetWithToken(apiUrl.get_blocked_users, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                if (res.result.length == 0) {
                    showMessage(strings.error, strings.msgTag, res.message)
                    setBlockUserData([])
                }
                else
                    setBlockUserData(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setBlockUserData([])

            }

        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const unBlockUser = async (item) => {
        console.log(item)
        setShowing(true)
        var obj = {
            user_id: item.u_id,
            state: "Unblock"
        }
        await apiCallWithToken(obj, apiUrl.block_or_unblock_user, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                comeChatUnBlockUser(obj.user_id);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const comeChatUnBlockUser = (user_id) => {
        var usersList = [comeChatUserTag + user_id];

        CometChat.unblockUsers(usersList).then(
            list => {
                setTimeout(() => {
                    getBlocksUser(token)
                }, 1000);
            }, error => {
                console.log("unblocking user fails with error", error);
            }
        );
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ flexDirection: "row", marginVertical: 10 }}>
                <Image style={styles.viewUnknown} source={{ uri: item.u_image }}></Image>
                <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 1 }}>
                    <View style={{ marginLeft: 20, marginTop: 5, alignSelf: "center" }}>
                        <Text style={styles.radioText}>{item.user_name}</Text>
                        <Text style={styles.txtInvite}>{item.total_mutual_friends + " Mutual Friends •" + item.total_pets + " Pets"}</Text>
                    </View>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradientRemove}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnTextRemove}
                        viewStyle={styles.verifyBtnRemove}
                        onPress={() => unBlockUser(item)}
                        text={strings.unblock}
                    />
                </View>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <AppHeader
                navigation={props.navigation}
                title={strings.blockedUsers}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={blockUserData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >
                </FlatList>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}

export default BlockUserScreen;