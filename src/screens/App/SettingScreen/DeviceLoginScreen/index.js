import React, { useEffect ,useState} from 'react';
import { View, Text, FlatList, Image } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import styles from '../SettingStyle';
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../utils/ToastConfig';

const DeviceLoginScreen = (props) => {
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [deviceData,setDeviceData]=useState([])
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            getUserDevices(res.token)
        })
    }, []);
    const getUserDevices = async (token) => {

        await apiCallGetWithToken(apiUrl.get_user_devices, token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setDeviceData(res.result)
            }
            else
                showMessage(strings.error, strings.msgTag, res.message)

        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const removeDevice=async(item)=>{
        setShowing(true)
        var obj = {
            device_id: item.udt_id,
        }
        await apiCallWithToken(obj, apiUrl.remove_device, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setTimeout(() => {
                    getUserDevices(token)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const renderView = ({ item, index }) => {
        return (
            <View style={{ flexDirection: "row", marginVertical: 10 }}>
                <View style={styles.viewUnknown} >
                    <Image style={styles.imgUnknown} source={Images.phoneIcon}></Image>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 1 }}>
                    <View style={{ marginLeft: 20, marginTop: 5, alignSelf: "center" }}>
                        <Text style={styles.radioText}>{item.udt_device_name}</Text>
                        <Text style={styles.txtInvite}>{item.udt_device_model_name}</Text>
                    </View>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradientRemove}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnTextRemove}
                        viewStyle={styles.verifyBtnRemove}
                        onPress={() =>removeDevice(item)}
                        text={strings.remove}
                    />
                </View>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <AppHeader
                navigation={props.navigation}
                title={strings.deviceLogged}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={deviceData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >
                </FlatList>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}

export default DeviceLoginScreen;