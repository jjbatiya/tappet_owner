import { StyleSheet } from "react-native";
import { responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import { Colors } from "../../../Resources/Colors";
import { Fonts } from "../../../Resources/Fonts";

export default styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    //render View Setting
    txtTitle: {
        fontFamily: Fonts.DMSansRegular, fontSize: 14,
        fontWeight: "400", lineHeight: 18.23, color: Colors.white,
        marginLeft: 15
    },
    imgStyle: { height: 15, width: 15, tintColor: Colors.white },
    viewLine: { height: 1, marginLeft: 15, backgroundColor: Colors.white, opacity: 0.15, flex: 1 },
    viewLeft: { flexDirection: "row", alignItems: "center", marginVertical: 15 },

    //change password input box
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3,
        marginLeft:-8
    },

    //modal view
    modal: {
        //alignItems: 'center',   
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: '100%',
        width: '100%',


    },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16,
        lineHeight: 22, color: Colors.white, fontWeight: "700",
        marginTop: responsiveHeight(2.5), fontFamily: Fonts.DMSansRegular
    },
    txtlblRecoverStr: {
        textAlign: "center", alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        width: 295, marginVertical: responsiveHeight(1.5), opacity: 0.6
    },
    txtCancel: { marginTop: responsiveHeight(1), alignSelf: "center", fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400" },
    verifyBtn: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "90%", alignSelf: "center" },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },

    //device login
    viewUnknown: {
        height: 48, width: 48,
        backgroundColor: Colors.opacityColor2,
        borderRadius: 24,
        alignItems: "center",
        justifyContent: "center"

    },
    imgUnknown: {
        height: 22,
        width: 20,
        position: "absolute",
        tintColor: Colors.white,
    },
    radioText: {
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular

    },
    verifyBtnRemove: { alignSelf: "center", },
    btnlinearGradientRemove: {
        height: 28,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },
    btnTextRemove: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        paddingHorizontal:10
    },

})