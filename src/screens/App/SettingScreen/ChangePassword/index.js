import React, { useState, useRef, useEffect } from 'react';
import { View, Text, TouchableOpacity, Modal, Image, Platform } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import styles from '../SettingStyle';
import { strings } from '../../../../Resources/Strings';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../Components/CommonButton';
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiCall, apiCallWithToken, getFirebaseToken, getUserData, storeData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { validationMsg } from '../../../../Resources/ValidationMsg';
import { toastConfig } from '../../../../utils/ToastConfig';

const ChangePassword = (props) => {
    const [isPasswordFocus, setPasswordFocus] = useState(false)
    const [isConfiremPasswordFocus, setConfirmPasswordFocus] = useState(false)
    const [isOldPassword, setOldPassword] = useState(false)
    const [isMobile, setMobileFocus] = useState(false)
    let refSheet = useRef();
    const [flag, setFlag] = useState(props.route.params.flag)
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [currentPassword, setCurrentPassword] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const [u_id, setUserId] = useState("")
    const [u_mobile_number, setMobileNumber] = useState("")
    const [u_otp, setOtp] = useState("")
    const [device_token, setDeviceToken] = useState("")

    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            setMobileNumber(res.u_mobile_number)
        })
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
    }, []);
    const onBlur = (flag) => {
        if (flag == "password")
            setPasswordFocus(!isPasswordFocus)
        else if (flag == "oldPassword")
            setOldPassword(!isOldPassword)
        else if (flag == "mobile")
            setMobileFocus(!isMobile)
        else
            setConfirmPasswordFocus(!isConfiremPasswordFocus)

    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const chnagePasswordView = () => {
        return (
            <View>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isOldPassword ? styles.focusforminput : styles.formInput}
                    password
                    value={currentPassword}
                    onFocus={() => onBlur("oldPassword")}
                    onChangeText={(text) => setCurrentPassword(text)}
                    onBlur={() => onBlur("oldPassword")}
                >{strings.oldPassword}</FloatingLabel>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isPasswordFocus ? styles.focusforminput : styles.formInput}
                    password
                    value={newPassword}
                    onChangeText={(text) => setNewPassword(text)}
                    onFocus={() => onBlur("password")}
                    onBlur={() => onBlur("password")}
                >{strings.newPassword}</FloatingLabel>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={isConfiremPasswordFocus ? styles.focusforminput : styles.formInput}
                    password
                    value={confirmPassword}
                    onChangeText={(text) => setConfirmPassword(text)}
                    onFocus={() => onBlur("confirmPassword")}
                    onBlur={() => onBlur("confirmPassword")}
                >{strings.confirmPassword}</FloatingLabel>
            </View>
        )
    }
    const changeNumberView = () => {
        return (
            <FloatingLabel
                labelStyle={styles.labelInput}
                inputStyle={styles.input}
                style={isMobile ? styles.focusforminput : styles.formInput}
                keyboardType={"number-pad"}
                value={u_mobile_number}
                onChangeText={(text) => setMobileNumber(text)}
                onFocus={() => onBlur("mobile")}
                onBlur={() => onBlur("mobile")}
            >{strings.mobilePlaceText}</FloatingLabel>
        )
    }

    const clickSave = () => {
        if (flag == "Change Password") {
            if (currentPassword.trim() == "") {
                showMessage(strings.error, strings.msgTag, validationMsg.currentPassword)
            }
            else if (newPassword.trim() == "")
                showMessage(strings.error, strings.msgTag, validationMsg.newPassword)
            else if (strings.passwordRegrex.test(newPassword) === false) {
                showMessage(strings.error, strings.msgTag, validationMsg.passwordRegrex)
            } else if (newPassword != confirmPassword) {
                showMessage(strings.error, strings.msgTag, validationMsg.changePasswordConfirmMsg)
            }
            else {
                changePassword();
            }
        }
        else {
            changeMobileNumber(refSheet);
        }

    }
    const changePassword = async () => {
        setShowing(true)
        var obj = {
            current_password: currentPassword,
            new_password: newPassword
        }
        await apiCallWithToken(obj, apiUrl.changePassword, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const changeMobileNumber = async (refSheet) => {
        setShowing(true)
        var obj = {
            u_id: u_id,
            u_mobile_number: u_mobile_number
        }
        await apiCallWithToken(obj, apiUrl.updateuserdetail, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                   showMessage(strings.success, strings.msgTag, "Your OTP is "+res.result.u_otp)

                setOtp(res.result.u_otp)
                
                setTimeout(() => {
                    refSheet.open();

                }, 1000);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const verifyMobile = async () => {
        refSheet.close();
        setShowing(true)
        const obj = {
            u_mobile_number: u_mobile_number,
            u_otp: u_otp,
            device_token: device_token,
            device_type: Platform.OS
        }
        await apiCall(obj, apiUrl.verify_otp).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                setShowing(false)
                var obj = { ...res.result, token: token }
                storeData(obj);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const verifyModal = () => {
        return (

            <View style={styles.modal}>
                <Text style={styles.txtlblCheckEmail}>{strings.verifyStr}</Text>
                <Text style={styles.txtlblRecoverStr}>{strings.verifyDescStr}{u_mobile_number}</Text>
                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    keyboardType={"number-pad"}
                    value={u_otp}
                    // onChangeText={(text) => setOtp(text)}
                    style={true ? [styles.focusforminput, { marginHorizontal: 20 }] : styles.formInput}
                // onFocus={() => onBlur("code")}
                // onBlur={() => onBlur("code")}
                >Confirmation code</FloatingLabel>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={styles.verifyBtn}
                    onPress={() => verifyMobile()}
                    text={strings.confirm}
                />

            </View>
        )
    }
    return (
        <View style={styles.container}>

            <View style={styles.container}>

                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
                <AppHeader
                    navigation={props.navigation}
                    title={flag == "Change Password" ? strings.chnagePassword : strings.changeNumber}
                    titleText={{ marginLeft: 10 }}
                    textSkip={"Save"}
                    isSkip={true}
                    onSkip={() => clickSave()}
                    backButton={Images.backIcon} />
                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={{ flex: 1 }}
                >
                    <View style={{ margin: 20 }}>
                        {
                            flag == "Change Password" ?
                                chnagePasswordView() :
                                changeNumberView()
                        }
                    </View>
                    <RBSheet
                        ref={ref => {
                            refSheet = ref;
                        }}
                        height={292}
                        openDuration={250}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {
                                justifyContent: "center",
                                alignItems: "center",
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                padding: 10
                            }
                        }}
                    >
                        {verifyModal()}
                    </RBSheet>
                </LinearGradient>
            </View>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}

export default ChangePassword;