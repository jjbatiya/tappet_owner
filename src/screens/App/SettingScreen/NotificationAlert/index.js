import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet, Switch, ScrollView } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { strings } from '../../../../Resources/Strings';
import { Fonts } from '../../../../Resources/Fonts';
import { apiCallWithToken, getUserData, storeData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../utils/ToastConfig';

const NotificationAlert = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [userData, setData] = useState("")
    useEffect(async () => {
        getUserData().then(res => {
            setUserID(res.u_id)
            setToken(res.token)
            setArray(res);
            setData(res)
        })
    }, []);

    const [alertData, setAlertData] = useState([
        {
            id: 1,
            title: "Group Message",
            detail: "All messages",
            image: Images.contact,
            isOn: true
        },
        {
            id: 2,
            title: "Comments",
            detail: "Comments on a post",
            image: Images.commentIcon,
            isOn: true
        },
        {
            id: 3,
            title: "Likes",
            detail: "Likes on a post",
            image: Images.likeHeart,
            isOn: true
        },
        {
            id: 4,
            title: "Friend Request",
            detail: "New Friend Request or accepted.",
            image: Images.addFriend,
            isOn: true
        },
        {
            id: 5,
            title: "Play dates",
            detail: "New Play dates, Invites, Media Post etc",
            image: Images.addFriend,
            isOn: true
        }
    ])
    const petData = [
        {
            id: 1,
            petName: "Daisy",
            content: "7 Notifications Enabled",
            image: Images.dogIcon1
        },
        {
            id: 2,
            petName: "Melody",
            content: "All Notifications Enabled",
            image: Images.dogIcon2
        },
        {
            id: 3,
            petName: "Jolene",
            content: "All Notifications Enabled",
            image: Images.dogIcon3
        }
    ]
    const toggleSwicth = (index) => {
        let newArray = [...alertData]
        newArray[index].isOn = !newArray[index].isOn
        setAlertData(newArray)
        notificationSettingChange(newArray)
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const notificationSettingChange = async (newArray) => {
        setShowing(true)
        var obj = {
            u_id: u_id,
            u_group_message_notification: newArray[0].isOn == true ? 1 : 2,
            u_post_comment_notification: newArray[1].isOn == true ? 1 : 2,
            u_post_like_notification: newArray[2].isOn == true ? 1 : 2,
            u_friend_request_notification: newArray[3].isOn == true ? 1 : 2,
            u_event_notification: newArray[4].isOn == true ? 1 : 2
        }
        await apiCallWithToken(obj, apiUrl.update_notification_settings, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    let userValue = { ...userData }
                    userValue.u_group_message_notification = newArray[0].isOn == true ? 1 : 2,
                        userValue.u_post_comment_notification = newArray[1].isOn == true ? 1 : 2,
                        userValue.u_post_like_notification = newArray[2].isOn == true ? 1 : 2,
                        userValue.u_friend_request_notification = newArray[3].isOn == true ? 1 : 2,
                        userValue.u_event_notification = newArray[4].isOn == true ? 1 : 2
                    storeData(userValue)
                    setShowing(false)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const setArray = (res) => {
        let newArray = [...alertData]
        newArray[0].isOn = res.u_group_message_notification == 1 ? true : false;
        newArray[1].isOn = res.u_post_comment_notification == 1 ? true : false;
        newArray[2].isOn = res.u_post_like_notification == 1 ? true : false;
        newArray[3].isOn = res.u_friend_request_notification == 1 ? true : false;
        newArray[4].isOn = res.u_event_notification == 1 ? true : false;
        setAlertData(newArray)
    }
    const renderGeneral = () => {
        return (
            <View>
                {alertData.map((item, index) => {
                    return (
                        <View style={styles.mainRender}>
                            <View style={{ flexDirection: "row" }}>
                                {index == 0 || index == 3 || index == 4 ?
                                    <Image style={[styles.imgHome,{opacity:0.6}]} source={item.image}></Image> :
                                    <Image style={styles.imgHome} source={item.image}></Image>}
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.txtTitle}>{item.title}</Text>
                                    <Text style={styles.txtAddress}>{item.detail}</Text>
                                </View>
                            </View>
                            <Switch
                                style={{ alignSelf: "center" }}
                                trackColor={{ true: Colors.theme, false: Colors.theme }}
                                thumbColor={item.isOn ? Colors.primaryViolet : "#f4f3f4"}
                                ios_backgroundColor={item.isOn ? Colors.primaryViolet : "#767577"}
                                onValueChange={() => toggleSwicth(index)}
                                value={item.isOn}
                            />
                        </View>
                    )
                })
                }
            </View>
        )
    }
    const renderPetSpecific = () => {
        return (
            <View>
                {petData.map((item, index) => {
                    return (
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate(strings.petSpecificScreen, { title: item.petName })}
                            style={styles.mainRender}>
                            <View style={{ flexDirection: "row" }}>

                                <Image style={styles.imgPeople} source={item.image}></Image>
                                <View style={{ marginLeft: 20, alignSelf: "center" }}>
                                    <Text style={styles.txtTitle}>{item.petName}</Text>
                                    <Text style={styles.txtAddress}>{item.content}</Text>
                                </View>
                            </View>
                            <Text
                                style={styles.txtArrow}>{">"}</Text>

                        </TouchableOpacity>
                    )
                })
                }
            </View>
        )

    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={strings.notificationAlerts}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <ScrollView>
                    <View style={{ flex: 1, margin: 20 }}>
                        <Text style={styles.txtHeading}>{strings.general}</Text>
                        {renderGeneral()}
                        <Text style={[styles.txtHeading, { marginTop: responsiveHeight(3), marginBottom: responsiveHeight(1) }]}>{strings.petSpecific}</Text>
                        {renderPetSpecific()}

                    </View>
                </ScrollView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    txtHeading: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "700", letterSpacing: 0.4, textTransform: "uppercase",
        color: Colors.white, fontSize: 14, lineHeight: 18.23, marginBottom: 15
    },
    //render View
    mainRender: { flexDirection: "row", justifyContent: "space-between", marginVertical: 10 },
    txtTitle: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 14, lineHeight: 18.23
    },
    txtAddress: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 12, lineHeight: 15.62,
        opacity: 0.6
    },

    imgHome: { height: 16, width: 16, tintColor: Colors.white, marginTop: 5 },
    //render pet specific
    imgPeople: {
        height: 48, width: 48,
        borderRadius: 24
    },
    txtArrow: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 16, lineHeight: 15.62,
        opacity: 0.6, alignSelf: "center", textAlign: "center",
        // paddingVertical: 20
    }
})
export default NotificationAlert;