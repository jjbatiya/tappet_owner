import React, { useState } from 'react';
import { View, Text, Modal, FlatList, Image, StyleSheet, Switch, ScrollView } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { strings } from '../../../../Resources/Strings';
import { Fonts } from '../../../../Resources/Fonts';
import CommonButton from '../../../../Components/CommonButton';

const PetSpecific = (props) => {
    const [alertData, setAlertData] = useState([
        {
            id: 1,
            title: "Calendar Reminder",
            detail: "Lunch time, poop time etc",
            image: Images.clock,
            isOn: false
        },
        {
            id: 2,
            title: "Check-in",
            detail: "Checked-in at any pet org.",
            image: Images.Checkin,
            isOn: true
        },
        {
            id: 3,
            title: "Check-Out",
            detail: "Checked-out of any pet org.",
            image: Images.checkOut,
            isOn: true
        },
        {
            id: 4,
            title: "Pet on Run",
            detail: "Activity and Health.",
            image: Images.Activity,
            isOn: true
        },
    ])
    const [isVisible, setIsVisible] = useState(false)
    const [selectIndex, setIndex] = useState(0)
    const toggleSwicth = (index) => {
        setIndex(index)
        if (alertData[index].isOn)
            setIsVisible(true)
        else {
            let newArray = [...alertData]
            newArray[index].isOn = !newArray[index].isOn
            setAlertData(newArray)
        }
    }
    const changeData = () => {
        let newArray = [...alertData]
        newArray[selectIndex].isOn = !newArray[selectIndex].isOn
        setAlertData(newArray)
        setIsVisible(false)
    }
    const modalView = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={isVisible}
            >
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={styles.modalIconStyle} source={Images.notification}></Image>
                    <Text style={styles.modalTitle}>{strings.reminder}</Text>
                    <Text style={styles.modalDescription}>{strings.petSpecificationDesc}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219 }}
                        onPress={() => changeData()}
                        text={strings.continue}
                    />
                    <Text onPress={() => setIsVisible(false)} style={styles.txtCancel}>{strings.cancelText}</Text>
                </View>
            </Modal >
        )
    }

    const renderGeneral = () => {
        return (
            <View>
                {alertData.map((item, index) => {
                    return (
                        <View style={styles.mainRender}>
                            <View style={{ flexDirection: "row" }}>
                                {index % 2 == 0 ?
                                    <Image style={[styles.imgHome,{opacity:0.6}]} source={item.image}></Image> :
                                    <Image style={styles.imgHome} source={item.image}></Image>}
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.txtTitle}>{item.title}</Text>
                                    <Text style={styles.txtAddress}>{item.detail}</Text>
                                </View>
                            </View>
                            <Switch
                                style={{ alignSelf: "center" }}
                                trackColor={{ true: Colors.theme, false: Colors.theme }}
                                thumbColor={item.isOn ? Colors.primaryViolet : "#f4f3f4"}
                                ios_backgroundColor={item.isOn ? Colors.primaryViolet : "#767577"}
                                onValueChange={() => toggleSwicth(index)}
                                value={item.isOn}
                            />
                        </View>
                    )
                })
                }
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={props.route.params.title}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <ScrollView>
                    <View style={{ flex: 1, margin: 20 }}>
                        {renderGeneral()}
                        {modalView()}
                    </View>
                </ScrollView>
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    txtHeading: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "700", letterSpacing: 0.4, textTransform: "uppercase",
        color: Colors.white, fontSize: 14, lineHeight: 18.23, marginBottom: 15
    },
    //render View
    mainRender: { flexDirection: "row", justifyContent: "space-between", marginVertical: 10 },
    txtTitle: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 14, lineHeight: 18.23
    },
    txtAddress: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        color: Colors.white, fontSize: 12, lineHeight: 15.62,
        opacity: 0.6
    },

    imgHome: { height: 16, width: 16, tintColor: Colors.white, marginTop: 5 },

    //modal view
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },
    modalIconStyle: {
        tintColor: Colors.white,
        width: 25,
        height: 25,
        marginVertical: responsiveHeight(2)
    },
    modalTitle: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    modalDescription: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 44,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
})
export default PetSpecific;