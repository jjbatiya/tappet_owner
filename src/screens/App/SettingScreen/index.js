import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image } from 'react-native';

import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import styles from './SettingStyle';
import { apiCallWithToken, getDeviceUid, getUserData, storeChatUserData, storeData } from '../../../utils/helper';
import { CometChat } from "@cometchat-pro/react-native-chat"
import messaging from '@react-native-firebase/messaging';
import Toast from 'react-native-toast-message';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Loader from '../../../Components/Loader';
import { strings } from '../../../Resources/Strings';
import { toastConfig } from '../../../utils/ToastConfig';

const settingData = [
    {
        id: 1,
        title: "Account Setting",
        image: Images.userIcon1
    },
    {
        id: 2,
        title: "Notifications Alerts",
        image: Images.bell
    },
    {
        id: 3,
        title: "Help Centre",
        image: Images.helpIcon
    },
    {
        id: 4,
        title: "Terms & Conditions",
        image: Images.document
    },
    {
        id: 5,
        title: "About App",
        image: Images.petIcon
    },
    {
        id: 6,
        title: "Logout",
        image: Images.logout
    }
]
const SettingScreen = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [device_uid] = useState(getDeviceUid());

    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
        })
    }, []);
    const clickItem = async (item) => {
        if (item.title == "Account Setting")
            props.navigation.navigate(strings.accountSettingScreen)
        else if (item.title == "Notifications Alerts")
            props.navigation.navigate(strings.notificationAlertScreen)
        else if (item.title == "Logout") {
            setShowing(true)

            await storeData("");
            await storeChatUserData("")
            await comeChatLogout();
            await messaging().deleteToken();
            removeDevice();
        }
        else {
            props.navigation.navigate("webScreen", { title: item.title })
        }
    }
    const removeDevice = async () => {
        var obj = {
            device_uid: device_uid,

        }
        await apiCallWithToken(obj, apiUrl.logout, token).then(res => {
            setShowing(false)


            props.navigation.reset({
                index: 0,
                routes: [{ name: strings.AuthStack }],
            });
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 1000
        });
    }
    const comeChatLogout = () => {
        CometChat.logout().then(
            () => {
                console.log("Logout completed successfully");
            }, error => {
                console.log("Logout failed with exception:", { error });
            }
        );
    }
    const renderView = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => clickItem(item)}
                    style={styles.viewLeft}>
                    <Image style={[styles.imgStyle, { tintColor: item.title == "Logout" ? "red" : Colors.white }]} source={item.image}></Image>
                    <Text style={[styles.txtTitle, { color: item.title == "Logout" ? "red" : Colors.white }]}>{item.title}</Text>
                </TouchableOpacity>
                {settingData.length - 1 != index ?
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image style={styles.imgStyle}></Image>
                        <View style={styles.viewLine}></View>
                    </View> : null}
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <AppHeader
                navigation={props.navigation}
                title={strings.settings}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={settingData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >
                </FlatList>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}

export default SettingScreen;