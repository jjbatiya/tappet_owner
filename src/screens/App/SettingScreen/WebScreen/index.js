import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet, Switch, ScrollView } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { getApiCall, getUserData } from '../../../../utils/helper';
import Loader from '../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { strings } from '../../../../Resources/Strings';
import { toastConfig } from '../../../../utils/ToastConfig';

const WebScreen = (props) => {
    const [data, setData] = useState(null)
    const [isshowing, setShowing] = useState(false)
    const [title]=useState(props.route.params.title)
    const [token, setToken] = useState("")
    useEffect(() => {
        console.log(props.route.params.title)
        getUserData().then(res => {
            setToken(res.token)

            getData(res.token,title)
        })
    }, []);
    const getData = async (data,title) => {
        var apiurl="";
        if(title=="Terms & Conditions")
            apiurl=apiUrl.terms_and_conditions
        else if(title=="About App")
            apiurl=apiUrl.aboutus
        else if(title=="Help Centre")
            apiurl=apiUrl.faqs
        setShowing(true)
        await getApiCall(apiurl).then(res => {
            if (res.status == true) {
                setShowing(false)
            }
            else {
                showMessage(strings.error,strings.msgTag,res.message)
                setShowing(false)

            }
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <AppHeader
                navigation={props.navigation}
                title={props.route.params.title}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backArrow} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
export default WebScreen;