import React from 'react';
import { View, Text, TouchableOpacity, FlatList, Image } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import styles from '../SettingStyle';
import { strings } from '../../../../Resources/Strings';
const settingData = [
    {
        id: 1,
        title: "Change Password",
        image: Images.lockIcon
    },
    {
        id: 2,
        title: "Change Phone Number",
        image: Images.phoneIcon
    },
    {
        id: 3,
        title: "Devices logged in",
        image: Images.globe
    },
    {
        id: 4,
        title: "Blocked Users",
        image: Images.blocked
    },

]
const AccountSetting = (props) => {
    const clickItem = (item) => {
        if (item.title == "Change Password" || item.title == "Change Phone Number")
            props.navigation.navigate(strings.changePasswordScreen, { flag: item.title })
        else if (item.title == "Devices logged in")
            props.navigation.navigate(strings.deviceLoginScreen)
        else
            props.navigation.navigate(strings.blockUserScreen)
    }
    const renderView = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => clickItem(item)}
                    style={styles.viewLeft}>
                    <Image style={[styles.imgStyle]} source={item.image}></Image>
                    <Text style={styles.txtTitle}>{item.title}</Text>
                </TouchableOpacity>
                {settingData.length - 1 != index ?
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image style={styles.imgStyle}></Image>
                        <View style={styles.viewLine}></View>
                    </View> : null}
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={strings.accountSetting}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <FlatList
                    data={settingData}
                    style={{ margin: 20 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderView}
                >
                </FlatList>
            </LinearGradient>

        </View>
    )
}

export default AccountSetting;