import { StyleSheet,Dimensions } from "react-native";
import { responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import { Colors } from "../../../Resources/Colors";
import { Fonts } from "../../../Resources/Fonts";
const { width } = Dimensions.get('window');

export default styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    contentView: {
        margin: responsiveHeight(2),
    },

    //heart view
    heartView: { flexDirection: "row", margin: responsiveHeight(2.2) },
    heartIcon: { height: 15, width: 15, alignSelf: "center" },
    txtlblComment: {
        fontSize: 12, fontWeight: "400",
        lineHeight: 15.62, color: Colors.white, opacity: 0.6, alignSelf: "center",
        marginLeft: responsiveWidth(2), fontFamily: Fonts.DMSansRegular
    },

    //view Line
    lineView: { height: 1, backgroundColor: Colors.white, opacity: 0.15 },

    //commentRenderView css
    imgStyle: { height: 24, width: 24, borderRadius: 12 },
    mainView: { flexDirection: "row", marginVertical: 10, marginHorizontal: responsiveWidth(2) },
    commentView: {
        backgroundColor: Colors.opacityColor, borderRadius: 6,
        paddingTop: responsiveHeight(1), marginRight: responsiveWidth(28), marginLeft: responsiveWidth(2)
    },
    txtName: {
        color: Colors.white, fontWeight: "700", fontSize: 10,
        lineHeight: 13.02, paddingHorizontal: 15, fontFamily: Fonts.DMSansRegular
    },
    txtComment: {
        color: Colors.white, fontWeight: "400", fontSize: 14,
        lineHeight: 18.23, paddingHorizontal: 15, fontFamily: Fonts.DMSansRegular
    },
    txtTime: {
        fontSize: 8, fontWeight: "400", lineHeight: 10.42, color: Colors.white,
        opacity: 0.4, alignSelf: "flex-end", margin: 5, fontFamily: Fonts.DMSansRegular
    },

    //bottom /view
    bottomView: {
        height: 50, position: "absolute", bottom: 0,
        backgroundColor: Colors.theme, flexDirection: "row", width: '100%', alignItems: "center", justifyContent: "center"
    },
    txtInputStyle: { flex: 0.8, height: 40, color: Colors.white },
    txtPost: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.primaryViolet, flex: 0.1, fontFamily: Fonts.DMSansRegular
    },

    //post header view
    itemContainer: {
        backgroundColor: '#1D1233',
        marginVertical: 8,
        padding: 16
    },
    itemHeader: {
        flexDirection: 'row',
        width: width - 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemProfileIcon: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    itemPostTitle: {
        color: Colors.white,
        paddingHorizontal: 16,
        width: width * 0.80,
        fontSize: 14,
        fontWeight: '600',
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23
    },
    postActionIcon: {
        width: 5,
        height: 18,
        marginRight: 20
    },
    postDesc: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular

    },
    postImage: {
        width: width - 32,
        height: 180,
        borderRadius: 4
    },
    postFooter: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    footerElement: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 50
    },
    likeIcon: {
        width: 16,
        height: 15,
        tintColor:Colors.red,
      

    },
    likeIconUnfill: {
        width: 16,
        height: 15,
        tintColor:Colors.white,
        
    },
    likesText: {
        fontSize: 14,
        fontWeight: '400',
        color: Colors.white,
        marginLeft: 5,
        fontFamily: Fonts.DMSansRegular

    },
    groupProfile: {
        position: 'absolute',
        //backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileImage: {
        width: 24,
        height: 24,
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
        borderRadius: 12,

    },

    //event view
    renderStyle: { height: 180, marginVertical: responsiveHeight(1), borderRadius: 4 },
    imgStyleEvent: { height: 180, width: responsiveWidth(100), borderRadius: 4 },
    txtNameEvent: { fontSize: 14, fontWeight: "400", lineHeight: 18.23, alignSelf: "center", color: Colors.white, marginTop: responsiveHeight(1.8) },
    txtDetail: { fontSize: 12, fontWeight: "400", lineHeight: 15.62, alignSelf: "center", color: Colors.white, textAlign: "center", opacity: 0.6 },
    btnView: { width: "100%", height: 40, alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(2) },
    upperView: { position: "absolute", margin: 10, width: "95%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    peopleImg: { height: 32, width: 32, borderRadius: 15 },
    peopleImg1: { height: 32, width: 32, borderRadius: 15, marginLeft: -8 },
    moreView: { height: 32, width: 32, marginLeft: -8, alignItems: "center", justifyContent: "center" },
    moreBorderView: { backgroundColor: Colors.black, borderRadius: 15, opacity: 0.6, width: 32, height: 32, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700", fontSize: 9,
        lineHeight: 11.72, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    dateView: { height: 37, width: 36, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2 },
    txtDate: {
        fontSize: 16, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 10, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    bottomViewEvent: { position: "absolute", bottom: 0, margin: 10 },
    txtTitle: {
        fontWeight: "400", fontSize: 22, lineHeight: 27.5, color: Colors.white,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.4
    },
    bottomBorderView: { height: 1, backgroundColor: Colors.white, opacity: 0.2, marginVertical: 3 },
    placeIcon: { height: 13, width: 12, opacity: 0.7, tintColor: Colors.white },
    txtAddress: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 10, fontFamily: Fonts.DMSansRegular
    },
    //location view
    imgMap: {
        width: width - 32,
        height: 200,
        borderRadius: 4
    },

})
