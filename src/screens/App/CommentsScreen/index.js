import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, TextInput, Platform, KeyboardAvoidingView, ScrollView, Dimensions } from 'react-native';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'
import styles from './CommentsStyle';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Loader from '../../../Components/Loader';
import Toast from 'react-native-toast-message';
import moment from 'moment';
import { strings } from '../../../Resources/Strings';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';

import { CometChat } from '@cometchat-pro/react-native-chat';
import { toastConfig } from '../../../utils/ToastConfig'
import NotificationController from '../../../Components/NotificationController';
import { PlaceIconUnselect } from '../../../utils/svg/PlaceIconUnselect';
import EventMap from '../../../Components/EventMap';
const { width } = Dimensions.get('window');


const CommentsScreen = (props) => {
    const [CommentsData, setCommentData] = useState([])
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [post_id] = useState(props.route.params.post_id)
    const [commentText, setCommentText] = useState("")
    const [likeData, setLikeData] = useState([])
    const [likeUser, setLikeUserData] = useState("")
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    const [postData, setPostData] = useState(null)
    useEffect(async () => {
        await getUserData().then(res => {
            setShowing(true)
            setUserID(res.u_id)
            setToken(res.token)
            getPostDetail(res.token)
        })
    }, []);
    const getPostDetail = async (token) => {

        await apiCallGetWithToken(apiUrl.get_post_details + "?post_id=" + post_id, token).then(res => {
            if (res.status == true) {
                setPostData(res.result)
                getCommentData(token);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                getCommentData(token);

                setShowing(false)

            }
        })
    }
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {


                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })
                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

            },
            onOutgoingCallRejected: (call) => {
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    function getDifferenceInDays(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60 * 24);
    }
    function getDifferenceInHours(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60 * 60);
    }
    function getDifferenceInMinutes(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / (1000 * 60);
    }

    function getDifferenceInSeconds(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return diffInMs / 1000;
    }
    const dateTimeGet = (date1, date2) => {
        const days = Math.round(getDifferenceInDays(date1, date2));
        const hours = Math.round(getDifferenceInHours(date1, date2));
        const minutes = Math.round(getDifferenceInMinutes(date1, date2));
        const second = Math.round(getDifferenceInSeconds(date1, date2));
        if (second < 60)
            return second + " Second ago"
        else if (minutes < 60)
            return minutes + " Minute ago"
        else if (hours < 24)
            return hours + " Hour ago"
        else
            return days + " Days ago"
    }
    function isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }
    function getMultiplePostTag(item) {
        let tagArray = []
        if (item.post_event != undefined && item.post_event != null && isEmpty(item.post_event) == false)
            tagArray.push("Event")
        if (item.post_latitude != undefined && item.post_latitude != null && item.post_latitude != "")
            tagArray.push(" Location")
        if (item.post_images.length > 0)
            tagArray.push(" Photo")

        return tagArray.toString()
    }
    const checkOnlyText = (item) => {
        if (item.post_images.length == 0 && item.post_latitude == null && isEmpty(item.post_event) == true && item.post_type == "") {
            return true
        }
        else false
    }
    const renderHeader = () => {
        const now = new Date()
        const date1 = new Date(postData.post_created_at.replace(/-/g, "/"));
        var newDate = convertUTCToLocalTime(date1);
        const date2 = new Date();
        const time = dateTimeGet(date2, newDate);
        var isTextOnly = checkOnlyText(postData);

        // var multiplePostTag = ""
        // if (item.post_type == "") {
        //     getMultiplePostTag(item)
        // }
        if (postData.post_type == "Event" && Object.keys(postData?.post_event).length == 0)
            return <></>

        else
            return (
                <View
                    style={styles.itemContainer}>
                    <View
                        style={styles.itemHeader}>
                        <TouchableOpacity onPress={() => props.navigation.navigate(strings.UserProfileScreen, { u_id: postData.post_owner_id })}>
                            <Image source={{ uri: postData.added_by.u_image }} style={styles.itemProfileIcon} />
                        </TouchableOpacity>
                        <View>
                            <Text style={styles.itemPostTitle}>{postData.added_by.u_first_name + " " + postData.added_by.u_last_name}
                                {postData.group != null ?
                                    <Text style={[styles.itemPostTitle, { fontWeight: "400", color: Colors.opacityColor3 }]}>
                                        {postData.post_type == "" ? " " + getMultiplePostTag(postData) : " posted an " + (postData.post_type).toString().toLowerCase() + " in " + postData?.group.group_name + " "}
                                    </Text> :
                                    <Text style={[styles.itemPostTitle, { fontWeight: "400", color: Colors.opacityColor3 }]}>
                                        {isTextOnly ? " posted a Text" : (postData.post_type == "" ? " posted an " + getMultiplePostTag(postData) : " posted an " + (postData.post_type).toString().toLowerCase())}
                                        {postData.post_location != null ? " in " : ""}
                                    </Text>
                                }
                                {postData.post_location != null ? postData?.post_location : ""}
                            </Text>
                            <Text style={[styles.itemPostTitle, { opacity: 0.4 }]}>{time}</Text>
                        </View>

                    </View>
                    <View style={{ paddingVertical: 20 }}>
                        {postData.post_name != "" &&
                            <Text style={styles.postDesc}>{postData.title}{postData.post_name}</Text>
                        }
                    </View>
                    {postData.post_type == "Event" ?
                        postEvent(postData) :
                        postData.post_type == "Photo" && postData.post_images.length == 1 ?
                            postImage(postData) :
                            postData.post_type == "Location" ?
                                postLocationView(postData) :
                                postData.post_type == "Multiple Photos" ?
                                    multiImageView(postData) :
                                    postData.post_type == "Audio" || postData.post_type == "Video" ?
                                        audioPostView(postData) :
                                        postData.post_type == "" || postData.post_images.length > 1 ?
                                            multiplePostView(postData) :
                                            <></>
                    }


                </View>
            )
    }
    const postEvent = (item, str) => {
        var day = new Date(item.post_event.event_start_date).getDate();
        var month = new Date(item.post_event.event_start_date).getMonth();
        var isPastFlag = true;
        const date1 = new Date((item.post_event.event_end_date + " " + item.post_event.event_end_time).replace(/-/g, "/"));
        const date2 = new Date();
        var diff = (date1.getTime() - date2.getTime()) / 1000;
        if (diff > 0)
            isPastFlag = false;

        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(isPastFlag ? strings.EventDetailPastScreen : strings.EventDetailUpcomingScreen, { item: item.post_event })}
                style={styles.renderStyle} >
                <Image source={{ uri: item.post_event.event_image }} style={styles.postImage} ></Image>
                <View style={styles.upperView}>
                    <View style={styles.upperSubView}>

                        <View style={styles.dateView}>
                            <Text style={styles.txtDate}>{day}</Text>
                            <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                        </View>
                    </View>
                </View>
                <LinearGradient style={{
                    position: "absolute",
                    height: 180,
                    width: width - 32, bottom: 0,

                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomViewEvent}>
                    <Text style={styles.txtTitle}>{item?.post_event?.event_name}</Text>
                    <View style={styles.bottomBorderView}></View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
                        <PlaceIconUnselect height={15} width={15} />
                        <Text style={styles.txtAddress}>{item?.post_event?.event_location}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
    const postImage = (item) => {
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()

        return (
            <TouchableOpacity onPress={() =>
                props.navigation.navigate(strings.webViewScreen,
                    {
                        url: item.post_images[0].post_image_image,
                        title: item.u_first_name, date: date
                    })
            }>
                <Image source={{ uri: item?.post_images[0]?.post_image_image }} style={styles.postImage} />
            </TouchableOpacity>
        )
    }
    const postLocationView = (item) => {

        var newDate = moment.utc(item.post_created_at).local().format(strings.hhmmaFormat);
        const obj = {
            name: item.added_by.u_first_name + " " + item.added_by.u_last_name,
            time: newDate,
            fullDate: item.post_created_at,
            imageUri: item.added_by.u_image,
            latitude: parseFloat(item?.post_latitude),
            longitude: parseFloat(item?.post_longitude),
            isShare: false
        }
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate(strings.viewLocationScreen, { mapData: obj })}>
                <EventMap zoomEnabled={false} lat={item?.post_latitude} long={item?.post_longitude} style={styles.imgMap} />
            </TouchableOpacity>
        )
    }
    const getCommentData = async (token) => {
        await apiCallGetWithToken(apiUrl.get_all_post_comments + "?post_id=" + post_id, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                if (res.result.comment.length != 0) {
                    setCommentData(res.result.comment)
                }

                if (res.result.likes.length != 0) {
                    setLikeData(res.result.likes)
                    let result1 = res.result.likes.map(a => a.u_first_name + " " + a.u_last_name);
                    setLikeUserData(result1.toString())
                }


            }
            else {
                //showMessage(strings.error, strings.msgTag, res.message)

                setShowing(false)

            }
        })
    }
    const addComment = async () => {
        setShowing(true)
        var obj = {
            post_id: post_id,
            post_comment_text: commentText
        }
        await apiCallWithToken(obj, apiUrl.add_comment, token).then(res => {
            setShowing(false)

            if (res.status == true) {
                setTimeout(() => {
                    setCommentText("")
                    getCommentData(token)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const convertUTCToLocalTime = (date) => {
        const milliseconds = Date.UTC(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
        );
        const localTime = new Date(milliseconds);
        return localTime;
    };
    const renderView = ({ item, index }) => {
        const date1 = new Date(item.post_comment_created_at.replace(/-/g, "/"));
        const date = convertUTCToLocalTime(date1);

        return (
            <View style={styles.mainView}>
                <Image style={styles.imgStyle} source={{ uri: item.u_image }}></Image>
                <View>
                    <View style={styles.commentView}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtComment}>{item.post_comment_text}</Text>
                        <Text style={styles.txtTime}>{date.getDate() + " " + monthArray[date.getMonth()] + " " + moment.utc(item.post_comment_created_at).local().format("h:mm A")}</Text>
                    </View>
                </View>
            </View>
        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)
                showMessage(strings.error, strings.msgTag, strings.sessionExpiredMsg)

                // handle exception
            }
        );
    }
    const onBackPress = () => {
        try {
            props.route.params.returnData({ isBack: true });
            props.navigation.goBack()
        } catch (error) {
            props.navigation.goBack()
        }

    }
    const multiImageView = (item) => {
        const newArray1 = item.post_images;
        const newArray = newArray1.slice(0, (item.post_images.length / 2) + 1);
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()
        return (
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={{ flexDirection: "row" }}>
                    {
                        newArray.map((data, i) => {
                            return (
                                i % 2 == 0 ?
                                    <TouchableOpacity
                                        onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                            {
                                                images: newArray1,
                                                flag: "post_image",
                                                index: i
                                            })
                                        }>
                                        <Image style={{ height: 210, width: 170, borderRadius: 4, marginRight: 2 }} source={{ uri: item.post_images[i].post_image_image }} ></Image>
                                    </TouchableOpacity> :
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                                {
                                                    images: newArray1,
                                                    flag: "post_image",
                                                    index: i
                                                })
                                            }>
                                            <Image style={{ height: 104, width: 170, borderRadius: 4, marginRight: 2 }}
                                                source={{ uri: item.post_images[i].post_image_image }} ></Image>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => props.navigation.navigate(strings.viewImagesScreen,
                                                {
                                                    images: newArray1,
                                                    flag: "post_image",
                                                    index: i + 1
                                                })
                                            }>
                                            <Image style={{ height: 104, width: 170, borderRadius: 4, marginRight: 2, marginTop: 2 }}
                                                source={{ uri: item.post_images[i + 1]?.post_image_image }} ></Image>
                                        </TouchableOpacity>
                                    </View>
                            )
                        })
                    }
                </View>
            </ScrollView>

        )
    }
    const audioPostView = (item) => {
        let date = moment(item.post_created_at).format("MMMM DD, YYYY").toString()
        return (
            <TouchableOpacity
                onPress={() => item.post_type == "Audio" ?
                    props.navigation.navigate('player', { title: "Title", filepath: item.post_images[0].post_image_image, dirpath: null }) :
                    props.navigation.navigate(strings.webViewScreen, { url: item.post_images[0].post_image_image, title: item.added_by.u_first_name, date: date })
                }
                style={[styles.postImage, { alignItems: "center", justifyContent: "center" }]} >
                <Image source={Images.playerIcon} resizeMode={"contain"} style={[styles.postImage, { tintColor: Colors.white, width: 100, alignSelf: "center" }]} />
            </TouchableOpacity>
        )
    }
    const multiplePostView = (item) => {
        return (
            <>
                {item.post_event != undefined && item.post_event != null && isEmpty(item.post_event) == false &&
                    postEvent(item)}
                {item.post_latitude != undefined && item.post_latitude != null && item.post_latitude != "" &&
                    postLocationView(item)}
                {item.post_images.length > 1 &&
                    multiImageView(item)
                }
                {
                    item.post_images.length == 1 &&
                    postImage(item)
                }
            </>
        )
    }
    return (
        <>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : undefined}
                style={styles.container}
            >
                <View style={styles.container}>
                    <AppHeader
                        navigation={props.navigation}
                        title={strings.Comments}
                        titleText={{ marginLeft: -30 }}
                        leftIconPress={() => onBackPress()}
                        backButton={Images.backIcon} />
                    <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                    <LinearGradient
                        colors={Colors.screenBackgroundColor}
                        style={{ flex: 1 }}
                    >

                        <View style={styles.container}>
                            {likeData.length != 0 &&
                                <TouchableOpacity
                                    onPress={() => props.navigation.navigate(strings.likeScreen, { likeData: likeData })}
                                >
                                    <View style={styles.heartView}>
                                        <Image style={styles.heartIcon} source={Images.heartIcon}></Image>
                                        <Text style={styles.txtlblComment}>{likeUser}</Text>
                                    </View>
                                    <View style={styles.lineView}></View>
                                </TouchableOpacity>}
                            
                                <FlatList
                                    data={CommentsData}
                                    showsVerticalScrollIndicator={false}
                                    style={{ marginBottom: 50 }}
                                    ListHeaderComponent={postData != null && renderHeader}
                                    renderItem={renderView}
                                >
                                </FlatList>


                            <View style={[styles.bottomView, { marginBottom: Platform.OS == "ios" ? 10 : 0 }]}>
                                <TextInput
                                    placeholder={strings.commentPlaceHolder}
                                    placeholderTextColor={Colors.opacityColor}
                                    style={styles.txtInputStyle}
                                    value={commentText}
                                    onChangeText={(text) => setCommentText(text)}
                                >
                                </TextInput>
                                <Text onPress={() => addComment()} style={styles.txtPost}>{strings.postStr}</Text>
                            </View>

                        </View>

                    </LinearGradient>

                    <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                        ref={(ref) => Toast.setRef(ref)} />

                    {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel} isVisible={isVisible} callAccept={callAccept} />}
                    {isVisible == false && Platform.OS == "ios" &&
                        <NotificationController
                            navigation={props.navigation}
                            callIsVisible={isVisible}
                            onMessageNotification={() => { }}
                        />
                    }
                </View>

            </KeyboardAvoidingView>
        </>
    )
}

export default CommentsScreen;