import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, Modal, FlatList, ScrollView } from 'react-native';

import AppHeader from '../../../../Components/AppHeader/AppHeader';
import { Images } from '../../../../Resources/Images'

import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MapView from 'react-native-maps';
import { mapStyle } from '../../../../Resources/MapStyle';
import Geolocation from '@react-native-community/geolocation';
import { Fonts } from '../../../../Resources/Fonts';
import { strings } from '../../../../Resources/Strings';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const ViewLocation = (props) => {
    const [latitude, setLatitude] = useState(props.route.params.mapData.latitude)
    const [longitude, setLongitude] = useState(props.route.params.mapData.longitude)
    const [name]=useState(props.route.params.mapData.name)
    const[time]=useState(props.route.params.mapData.time)
    const[imageUri]=useState(props.route.params.mapData.imageUri)
    let mapRef = useRef()
    const [isVisible, setVisible] = useState(false)
    const[isShare,setShare]=useState(props.route.params.mapData.isShare)
    console.log(latitude)
    useEffect(() => {
        // Geolocation.getCurrentPosition(info => {
        //     setLatitude(info.coords.latitude)
        //     setLongitude(info.coords.longitude)
        //     //    changeRegion(latitude, longitude)
        // }
        // );
        
    }, []);
    const setCurrentLocation = () => {
        Geolocation.getCurrentPosition(info => {
            setLatitude(info.coords.latitude)
            setLongitude(info.coords.longitude)
            changeRegion(info.coords.latitude, info.coords.longitude)

        }
        );

    }
    const changeRegion = (lat, log) => {
        let r = {
            latitude: lat,
            longitude: log,
            latitudeDelta:LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        };
        mapRef.current.animateToRegion(r, 2000);
    }
    const modalView = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={{ height: 240, width: 240 }} source={Images.dogIcon1}></Image>
                    <Text style={styles.txtName}>{"Daisy"}</Text>
                    <View style={{ flexDirection: "row", width: 240, marginTop: 5 }}>
                        <LinearGradient
                            colors={Colors.btnBackgroundColor}
                            style={{ height: 40, flex: 1 }}
                        >
                            <TouchableOpacity onPress={() => setVisible(false)} style={styles.viewDirection}>
                                <Image style={styles.imgDirection} source={Images.placeIcon}></Image>
                                <Text style={styles.txtDirection}>{strings.direction}</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <AppHeader
                navigation={props.navigation}
                title={name+" Shared Location"}
                titleText={{ marginLeft: -30 }}
                backButton={Images.backIcon} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1 }}>
                    <MapView
                        customMapStyle={mapStyle}
                        ref={mapRef}
                        style={{
                            flex: 1,
                        }}
                        provider={"google"}
                        mapType={strings.mapType}
                        initialRegion={{
                            latitude: latitude,
                            longitude: longitude,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}

                    >
                        <MapView.Marker
                        //    onPress={() => setVisible(true)}
                            coordinate={{
                                latitude: latitude,
                                longitude: longitude
                            }}>
                            <View style={styles.viewMain}>
                                <View style={styles.borderView}></View>
                                <View style={styles.roundView}></View>
                            </View>
                        </MapView.Marker>
                    </MapView>
                    <TouchableOpacity
                        onPress={() => setCurrentLocation()}
                        style={styles.touchableOpacityStyle}>
                        <Image source={Images.scanner} style={styles.floatingButtonStyle} />

                    </TouchableOpacity>
                    <View style={styles.bottomView}>
                        <Image style={styles.imgProfile} source={{uri:imageUri}}></Image>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 1 }}>
                            <View style={{ marginLeft: 20, alignSelf: "center" }}>
                                <Text style={styles.radioText}>{name}</Text>
                                <Text style={styles.txtInvite}>{"Sent at "+time}</Text>
                            </View>
                            {isShare&&
                            <Text style={styles.txtStop}>{strings.stopSharing}</Text>}
                        </View>
                    </View>
                </View>
                {modalView()}
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        right: 10,
        bottom: 100,
        backgroundColor: Colors.white,
        borderRadius: 25
    },
    floatingButtonStyle: {
        width: 15,
        height: 15,
        tintColor: Colors.primaryViolet,
    },
    bottomView: {
        backgroundColor: Colors.theme, height: 80,
        position: "absolute", bottom: 0, width: "100%", flexDirection: "row"
    },
    imgProfile: { height: 48, width: 48, borderRadius: 24, alignSelf: "center", marginLeft: 30 },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
    },
    //modal
    modal: {
        borderRadius: 4,
        marginTop: responsiveHeight(25),
        marginLeft: 20
    },
    txtDirection: {
        fontSize: 14,
        fontWeight: "400",
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular,
        color: Colors.white,
        marginLeft: 10
    },
    imgDirection: { height: 15, width: 15, tintColor: Colors.white },
    viewDirection: {
        flexDirection: "row", height: 40, alignItems: "center",
        justifyContent: "center"
    },
    txtName: {
        fontWeight: "400", fontSize: 18, lineHeight: 22,
        color: Colors.white, position: "absolute", top: 0, fontFamily: Fonts.LobsterTwoRegular
    },
    //current location view
    viewMain: { height: 60, width: 60, alignItems: "center", justifyContent: "center" },
    roundView: {
        position: "absolute", height: 12, width: 12,
        backgroundColor: Colors.primaryViolet, borderWidth: 2,
        borderColor: Colors.white, borderRadius: 6
    },
    borderView: { height: 60, width: 60, borderRadius: 30, backgroundColor: Colors.primaryViolet, opacity: 0.2 },
    txtStop: {
        alignSelf: "center", fontSize: 12, fontWeight: "500",
        lineHeight: 15.62, color: Colors.red, fontFamily: Fonts.DMSansRegular,
        marginRight:20
    }
})
export default ViewLocation;