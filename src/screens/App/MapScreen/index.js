import React, { useRef, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, Modal, FlatList, ScrollView, Platform } from 'react-native';

import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images'

import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MapView from 'react-native-maps';
import { mapStyle } from '../../../Resources/MapStyle';
import Geolocation from '@react-native-community/geolocation';
import { Fonts } from '../../../Resources/Fonts';
import RBSheet from "react-native-raw-bottom-sheet";
import SimpleSerach from '../../../Components/SimpleSerach';
import { strings } from '../../../Resources/Strings';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
var FloatingLabel = require('react-native-floating-labels');
import { CometChat } from '@cometchat-pro/react-native-chat';
import NotificationController from '../../../Components/NotificationController';


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const markers = [{
    name: 'Daisy',
    coordinates: {
        latitude: 3.148561,
        longitude: 101.652778
    },
    image: Images.dogIcon1
},
{
    name: 'Daisy',
    coordinates: {
        latitude: 37.78825,
        longitude: -122.4324,
    },
    image: Images.dogIcon
},

]
const durationData = [
    {
        id: 1,
        duration: "30 Min"
    },
    {
        id: 2,
        duration: "01 Hr"
    },
    {
        id: 3,
        duration: "02 Hr"
    },
    {
        id: 4,
        duration: "8 Hr"
    }
]
const data = [
    {
        groupName: "B4 Apartment, Church S..",
        names: "Sent 2h ago",
        image: Images.backGround,
        id: 1,
        isGroup: true,
        isSend: true
    },
    {
        groupName: "B4 Apartment, Church S..",
        names: "Sent 2h ago",
        image: Images.people,
        id: 2,
        isGroup: false,
        isSend: true

    },
    {
        groupName: "Workplace Dog group",
        names: "Sent 2h ago",
        image: Images.backGround,
        id: 3,
        isGroup: true,
        isSend: true

    },
    {
        groupName: "Workplace Dog group",
        names: "Last Sent 2h ago",
        image: Images.people1,
        id: 4,
        isGroup: false,
        isSend: false
    },

]
const MapScreen = (props) => {
    const [isVisible, setVisible] = useState(false)
    const [item, setItem] = useState("")
    const [latitude, setLatitude] = useState()
    const [longitude, setLongitude] = useState()
    const [markerArray, setMarkerArray] = useState(markers)
    const [selectedIndex, setSelectedIndex] = useState(-1)
    let mapRef = useRef()
    let refSheet = useRef();
    const changeRegion = (lat, log) => {
        let r = {
            latitude: latitude,
            longitude: longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
        };
        mapRef.current.animateToRegion(r, 2000);
    }
    // useEffect(() => {
    //     Geolocation.getCurrentPosition(info => {
    //         setLatitude(info.coords.latitude)
    //         setLongitude(info.coords.longitude)
    //         //    changeRegion(latitude, longitude)
    //     }
    //     );
    // }, []);
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const modalView = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <Image style={{ height: 240, width: 240, borderRadius: 4 }} source={item.image}></Image>
                    <Text style={styles.txtName}>{item.name}</Text>
                    <View style={{ flexDirection: "row", width: 240, marginTop: 5 }}>
                        <LinearGradient
                            colors={Colors.btnBackgroundColor}
                            style={{ height: 40, flex: 0.5, borderRadius: 4 }}
                        >
                            <TouchableOpacity onPress={() => setVisible(false)} style={styles.viewDirection}>
                                <Image style={styles.imgDirection} source={Images.placeIcon}></Image>
                                <Text style={styles.txtDirection}>{"Direction"}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                        <LinearGradient
                            colors={Colors.btnBackgroundColor}
                            style={{ height: 40, flex: 0.5, marginLeft: 5, borderRadius: 4 }}
                        >
                            <TouchableOpacity
                                onPress={() => { setVisible(false), refSheet.open() }}
                                style={styles.viewDirection}>
                                <Image style={styles.imgDirection} source={Images.share}></Image>
                                <Text style={styles.txtDirection}>{"Share"}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                </View>
            </Modal>
        )
    }
    const onMarkerClick = (item) => {
        setItem(item)
        setVisible(true)
    }
    const setCurrentLocation = () => {
        Geolocation.getCurrentPosition(info => {
            console.log(info)
            setLatitude(info.coords.latitude)
            setLongitude(info.coords.longitude)
            changeRegion(latitude, longitude)

        }
        );

    }
    const renderDuration = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => setSelectedIndex(index)} style={selectedIndex == index ? styles.selectViewDuration : styles.viewDuration}>
                <Text style={selectedIndex == index ? styles.txtSelectHour : styles.txtHour}>{item.duration}</Text>
            </TouchableOpacity>
        )
    }
    const shareView = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => refSheet.close()} style={styles.viewHandle} />
                <Text style={styles.txtHeading}>{"Share Daisy’s Location"}</Text>
                <Text style={styles.txtDuration}>{"Select duration"}</Text>
                <FlatList
                    data={durationData}
                    horizontal
                    style={{ alignSelf: "center", marginTop: 20 }}
                    showsHorizontalScrollIndicator={false}
                    renderItem={renderDuration}
                >

                </FlatList>
                <View style={{ marginHorizontal: responsiveWidth(2) }}>
                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        // onChangeText={(text) => onChangeText(text)}
                        style={styles.focusforminput}
                    >{"Write a message"}</FloatingLabel>
                    <View style={{ height: 1, width: '95%', backgroundColor: Colors.primaryViolet, marginLeft: responsiveWidth(2) }}></View>

                </View>
                <View style={{ marginTop: responsiveHeight(1) }}>
                    <SimpleSerach
                        onChangeText={(text) => console.log(text)}
                        placeHolder={"Search for friends or groups"}
                        isSimpleSerach={true}
                        editable={true}
                        onSubmitEditing={() => console.log("dd")}

                    />
                </View>
                {data.map((item, index) => {
                    return (
                        <View style={styles.mainGroupView}>
                            <View>
                                <Image style={styles.imgGroup} source={item.image}></Image>
                                {item.isGroup &&
                                    <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                                        <Image style={styles.peopleImg} source={Images.people1}></Image>
                                        <Image style={styles.peopleImg1} source={Images.people}></Image>

                                    </View>
                                }
                            </View>
                            <View style={styles.radioViewGroup}>
                                <View style={{ marginLeft: 20 }}>
                                    <Text style={styles.radioText}>{item.groupName}</Text>
                                    <Text style={styles.txtInvite}>{item.names}</Text>
                                </View>
                                {item.isSend ?
                                    <TouchableOpacity
                                        style={styles.btnView}
                                    >
                                        <Text style={styles.txtUndo}>{"Undo"}</Text>
                                    </TouchableOpacity> :
                                    <LinearGradient
                                        colors={Colors.btnBackgroundColor}
                                        style={styles.btnView}
                                    >
                                        <Text style={styles.txtUndo}>{"Send"}</Text>

                                    </LinearGradient>
                                }
                            </View>
                        </View>
                    )
                })}
            </View>
        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisibleCall(false)

            },
            error => {
                setVisibleCall(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <View style={{ flex: 1 }}>
            <AppHeader
                navigation={props.navigation}
                title={'Map'}
                titleText={{ marginLeft: -30 }}
                rightTitleRightPressed={() => props.navigation.navigate(strings.settingScreen)}
                rightIconRight={Images.setting} />
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1 }}>
                    <MapView
                        customMapStyle={mapStyle}
                        style={{
                            flex: 1,
                        }}
                        mapType={strings.mapType}
                        ref={mapRef}
                        provider={"google"}
                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}

                    >
                        {markers.map((marker, i) => (
                            <MapView.Marker
                                key={i}
                                onPress={() => onMarkerClick(marker)} coordinate={marker.coordinates}>
                                <View onPress={() => alert("geee")}>
                                    <Image style={{ height: 40, width: 40, borderRadius: 20 }} source={marker.image}></Image>
                                </View>
                            </MapView.Marker>
                        ))}
                    </MapView>
                    <TouchableOpacity
                        onPress={() => setCurrentLocation()}
                        style={styles.touchableOpacityStyle}>
                        <Image source={Images.scanner} style={styles.floatingButtonStyle} />

                    </TouchableOpacity>
                </View>
                {modalView()}
                <RBSheet
                    ref={(ref) => refSheet = ref}
                    height={550}
                    openDuration={250}
                    closeOnDragDown={false}
                    customStyles={{
                        container: {

                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor: Colors.dialogBackground,
                            padding: 10,
                        }
                    }}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {shareView()}
                    </ScrollView>
                </RBSheet>
            </LinearGradient>
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />
            }
            {isVisibleCall == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisibleCall}
                    onMessageNotification={()=>{}}

                />
            }
        </View>
    )
}
const styles = StyleSheet.create({
    touchableOpacityStyle: {
        position: 'absolute',
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        right: 10,
        bottom: 20,
        backgroundColor: Colors.white,
        borderRadius: 25
    },
    floatingButtonStyle: {
        width: 15,
        height: 15,
        tintColor: Colors.primaryViolet,
    },
    modal: {
        borderRadius: 4,
        marginTop: responsiveHeight(25),
        marginLeft: 20
    },
    txtDirection: {
        fontSize: 14,
        fontWeight: "400",
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular,
        color: Colors.white,
        marginLeft: 10
    },
    imgDirection: { height: 15, width: 15, tintColor: Colors.white },
    viewDirection: {
        flexDirection: "row", height: 40, alignItems: "center",
        justifyContent: "center"
    },
    txtName: {
        fontWeight: "400", fontSize: 18, lineHeight: 22,
        color: Colors.white, position: "absolute", top: 0, fontFamily: Fonts.LobsterTwoRegular
    },

    //bottom sheet view
    txtHeading: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: "700",
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        marginTop: responsiveHeight(1)

    },
    txtDuration: {
        opacity: 0.6,
        fontSize: 14, lineHeight: 18.23,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        color: Colors.white,
        marginTop: responsiveHeight(0.5)
    },
    viewDuration: {
        backgroundColor: Colors.opacityColor2,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        width: 75,
        marginHorizontal: responsiveWidth(2),
        borderRadius: 4.4


    },
    selectViewDuration: {
        backgroundColor: Colors.opacityColor2,
        height: 40,
        borderWidth: 1,
        borderColor: Colors.primaryViolet,
        alignItems: "center",
        justifyContent: "center",
        width: 75,
        marginHorizontal: responsiveWidth(2),
        borderRadius: 4.4

    },
    txtHour: {
        opacity: 0.6,
        fontSize: 14, lineHeight: 18.23,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        color: Colors.white,
    },
    txtSelectHour: {
        fontSize: 14, lineHeight: 18.23,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        color: Colors.white,
    },
    labelInput: {
        color: '#673AB7',
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.3,


    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {

        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3
    },
    mainGroupView: { flexDirection: "row", marginHorizontal: 20, marginVertical: 10, alignItems: "center" },

    imgGroup: {
        height: 48,
        width: 48,
        borderRadius: 54,
    },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    peopleImg: { height: 20, width: 20, borderRadius: 15 },
    peopleImg1: { height: 20, width: 20, borderRadius: 15, marginLeft: -8 },
    verifyBtnGroup: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "95%", alignSelf: "center" },
    viewCheckBox: {
        height: 20,
        width: 20,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primaryViolet

    },
    tickImage: { height: 6, width: 8, position: "absolute", alignSelf: "center" },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
    },
    btnView: {
        height: 28,
        width: 64,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(1),
        backgroundColor: Colors.opacityColor2
    },
    txtUndo: {
        opacity: 0.6,
        fontSize: 12, lineHeight: 18.23,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center",
        color: Colors.white,
    },
    viewHandle: { alignSelf: "center", height: 4, width: 45, backgroundColor: Colors.white, opacity: 0.2, padding: 2 }
})
export default MapScreen;