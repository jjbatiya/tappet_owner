import React, { useState, useEffect } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
import { Images } from '../../../../Resources/Images';

const Header = (props) => {
    const{pet_name,onGroupIcon,onCloseClick}=props
    return (
        <View style={styles.mainHeaderStyle}>
            <TouchableOpacity onPress={() => onCloseClick()} style={{ padding: 10}}>
                <Image style={styles.closeIcon} source={Images.closeIcon} />
            </TouchableOpacity>
            <Text style={styles.txtPetName}>{pet_name}</Text>
            <TouchableOpacity onPress={() =>onGroupIcon()} style={{ padding: 10 }}>
                <Image style={styles.closeIcon} source={Images.contact} />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    mainHeaderStyle: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 20,
        justifyContent: 'space-between'
    },
    closeIcon: {
        height: 28,
        width: 28,
        tintColor: Colors.white
    },
    txtPetName: {
        alignSelf: "center",
        fontFamily: Fonts.LobsterTwoRegular,
        fontSize: 30,
        color: Colors.white,
        textAlign:"center"
    }
})
export default Header;