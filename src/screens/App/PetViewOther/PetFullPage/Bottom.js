import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import CommonButton from '../../../../Components/CommonButton';
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
import { Images } from '../../../../Resources/Images';
import { strings } from '../../../../Resources/Strings';

const Bottom = (props) => {
    return (
        <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 0.50 }}>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    onPress={() => props.onCallClick()}
                    text={strings.callOwner}
                    image={Images.call}
                    imageStyle={styles.addIcon}
                />
            </View>
            <View style={{ flex: 0.50, marginLeft: 1 }}>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    onPress={() => props.oNmessage()}
                    text={strings.message}
                    image={Images.group}
                    imageStyle={styles.addIcon}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    btnlinearGradient: {
        flexDirection: 'row',
        height: 60,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    addIcon: {
        tintColor: Colors.white,
        width: 20,
        height: 20,
        marginRight: 10
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
})
export default Bottom;