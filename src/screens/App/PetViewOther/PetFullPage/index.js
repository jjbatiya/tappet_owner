import React, { useState, useEffect } from 'react';
import { Text, StyleSheet, View, ImageBackground, ScrollView, Dimensions, Image, Platform, TouchableOpacity } from 'react-native';
import { Images } from '../../../../Resources/Images';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { Colors } from '../../../../Resources/Colors';
import Header from '../Header';
import Bottom from './Bottom';
import { Fonts } from '../../../../Resources/Fonts';
import { strings } from '../../../../Resources/Strings';
import { apiCallGetWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { toastConfig } from '../../../../utils/ToastConfig';
const { width, height } = Dimensions.get("window")
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { comeChatUserTag } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"

var obj = {
    pet_id: 3
}
const PetFullPage = (props) => {
    const [petData, setData] = useState(obj)
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(true)
    const [owner, setOwner] = useState(null)
    const [owner_friend_request, setFriendRequest] = useState("")
    const[flag,setFlag]=useState("")
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            getPetProfile(res.token)

        })

    }, []);
    const onScroll = (event) => {
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? 'down' : 'up';
        this.offset = currentOffset;
        console.log(direction);
        if (direction == "up"&&flag=="")
            props.navigation.navigate(strings.PetPageOther, { petData: petData, owner_friend_request: owner_friend_request })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000

        });

    }
    const getPetProfile = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_pet_details + "?pet_id=" + petData.pet_id + "&device_type=" + Platform.OS, token).then(res => {
            setShowing(false)
            //   console.log(res)
            if (res.status == true) {
                setData(res.result)
                setOwner(res.result.added_by)
                getCoownerData(token, res.result.added_by.u_id)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const getCoownerData = async (token, u_id) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.user + "/" + u_id + "?device_type=" + Platform.OS, token).then(res => {
            setShowing(false)
            if (res.status == true)
                setFriendRequest(res.result.friend_request)

        })
    }
    const oNmessage = () => {
        props.navigation.navigate(strings.typeMessageScreen, { userData: { u_id: owner.u_id, u_first_name: owner.u_first_name, u_last_name: owner.u_last_name } })
    }
    const onGroupIcon = () => {
        setFlag("groupClick")
        props.navigation.navigate(strings.messageScreen)
    }
    const onCallClick = () => {
        var receiverID = comeChatUserTag + "" + owner.u_id;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.USER;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                props.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: owner })
            },
            error => {
                console.log("Call initialization failed with exception:", error);
            }
        );
    }
    const onCloseClick = () => {
        props.navigation.goBack(null)
    }
    return (
        <ScrollView
            onScroll={onScroll}
            style={{ flex: 1, backgroundColor: Colors.backgroundColor }} showsVerticalScrollIndicator={false}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <View style={styles.container}>
                <ImageBackground
                    style={styles.backgroundImage}
                    resizeMode={"contain"}
                    source={{ uri: petData?.pet_image }} />

                <LinearGradient
                    style={styles.coverStyle}
                    colors={Colors.viewCareTop} />

                <View style={styles.headerView}>
                    <Header
                        pet_name={petData?.pet_name}
                        onGroupIcon={onGroupIcon}
                        onCloseClick={onCloseClick}
                    />
                </View>
                {owner_friend_request == "1" &&
                    <View style={styles.bottomView}>
                        <Bottom
                            oNmessage={oNmessage}
                            onCallClick={onCallClick}
                        />
                    </View>}

                <TouchableOpacity
                    onPress={() => props.navigation.navigate(strings.PetPageOther, { petData: petData, owner_friend_request: owner_friend_request })}
                    style={styles.scrollView}>
                    <Image source={Images.arrowDownCircle} style={styles.scrollDownImage} />
                    <Text style={styles.scrollDownTxt}>{strings.scrollDown}</Text>
                </TouchableOpacity>
            </View>
            <View style={{ height: 5 }} />
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: height,

    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        // backgroundColor:Colors.backgroundColor
    },
    scrollDownTxt: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 15.62,
        fontSize: 12,
        letterSpacing: 0.3,
        color: 'rgba(255, 255, 255, 0.7)',
        marginTop: 10
    },
    scrollDownImage: { height: 40, width: 40 },
    headerView: {
        position: "absolute",
        top: 0,
        flex: 1,
        width: "100%"
    },
    coverStyle: {
        position: "absolute",
        height: '100%',
        width: '100%', bottom: 0,
    },
    bottomView: {
        position: "absolute",
        bottom: 0,
        flex: 1,
        width: "100%"
    },
    scrollView: {
        position: "absolute",
        bottom: 100,
        flex: 1,
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    }

})
export default PetFullPage;