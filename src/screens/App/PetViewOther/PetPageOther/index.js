import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleSheet, TouchableOpacity, View, Platform } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { createStackNavigator } from '@react-navigation/stack';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import { responsiveHeight } from 'react-native-responsive-dimensions';
import ProfilePage from './ProfilePage';
import PetShedulePage from './PetShedulePage';
import ActivityPage from './ActivityPage';

const Tab = createMaterialBottomTabNavigator();

const ScheduleStack = createStackNavigator();

const PetPageOther = (props) => {
    const test = () => {
        return (
            <ProfilePage navigation={props} backPressEvent={props.route.params} />

        )
    }
    const ScheduleTab=()=>{
        return(
            <PetShedulePage navigation={props} backPressEvent={props.route.params} />
        )
    }
    const ActivityTab=()=>{
        return(
            <ActivityPage navigation={props} backPressEvent={props.route.params}></ActivityPage>
        )
    }
   
    return (
        <Tab.Navigator
            shifting={true}
            barStyle={{ backgroundColor: Colors.theme }}
            tabBarOptions={{
                activeBackgroundColor: '#0C0024',
                inactiveBackgroundColor: Colors.theme,
                style: {
                    backgroundColor: '#0C0024',
                    borderTopWidth: 1,
                    height: Platform.OS == "ios" ? responsiveHeight(8.3) : responsiveHeight(8.3),
                    borderTopColor: 'rgba(255,255,255,0.1)',
                    paddingBottom: Platform.OS == "ios" ? 1 : 0
                },
            }}>
            <Tab.Screen
                name="Profile"
                component={test}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.profileTab}</Text>
                    // ),
                    tabBarLabel:strings.profileTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.petProfileTab : Images.petProfileUn} style={[styles.icon]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Schedule"
                component={ScheduleTab}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.scheduleTab}</Text>
                    // ),
                    tabBarLabel:strings.scheduleTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.calendarSelect : Images.calendarTab} style={[styles.icon,{tintColor:focused?Colors.primaryViolet:"grey"}]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Activity"
                component={ActivityTab}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.activityTab}</Text>
                    // ),
                    tabBarLabel:strings.activityTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={focused ? Images.chartSelect : Images.chart} style={[styles.icon,{tintColor:focused?Colors.primaryViolet:"grey"}]} resizeMode={'contain'} />
                    ),
                }} />
          
        </Tab.Navigator>
    )
}


const styles = StyleSheet.create({
    icon: {
        width: 19,
        height: 20,
    },
    tabLable: {
        fontSize: 11,
        paddingBottom: Platform.OS == "android" ? 10 : 10,
        marginTop: Platform.OS == "android" ? 5 : 0
    },
    tabFocus: {
        tintColor: Colors.primaryViolet
    },
    tabUnFocus: {
        tintColor: Colors.white,
        opacity: 0.4
    },
    tabFocusLabel: {
        color: Colors.primaryViolet
    },
    tabUnFocusLabel: {
        color: Colors.white,
        opacity: 0.4
    },
    viewTabIcon: {
       // paddingTop: Platform.OS == "ios" ? 0 : 8
    }
})
export default PetPageOther;