import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../../../../Resources/Colors";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

})
