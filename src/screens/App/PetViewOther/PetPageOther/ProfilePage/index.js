import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList, Image, ScrollView, Alert, Platform, Animated, Easing, Share, Linking } from 'react-native';
import { Images } from '../../../../../Resources/Images'
import { Colors } from '../../../../../Resources/Colors'
import { strings } from '../../../../../Resources/Strings';
import * as Constant from '../../../../../utils/Constant';
import styles from './styles'
import ReadMore from '@fawazahmed/react-native-read-more';
import CommonButton from '../../../../../Components/CommonButton';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Loader from '../../../../../Components/Loader';
import { apiCallGetWithToken, getUserData } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../../utils/ToastConfig';
import Header from '../../Header';

import { CometChat } from "@cometchat-pro/react-native-chat"
import BottomCallMessageView from '../../../../../Components/BottomCallMessageView';

const moment = require('moment')


const ProfilePage = (props) => {
    const [petData, setPetData] = useState(props.backPressEvent.petData)
    const [u_image] = useState(props.backPressEvent?.userImage)
    const [breed, setBreed] = useState("")
    let refSheet = useRef();
    const [isVisible, setVisible] = useState(false)
    const [images, setImages] = useState([])
    const [isshowing, setShowing] = useState(true)
    const [token, setToken] = useState("")
    const [photoData, setPhotoData] = useState([])
    const [animatedValue, setAnimation] = useState(new Animated.Value(180))
    const [flag, setFlag] = useState(false)
    const [owner_friend_request] = useState(props.backPressEvent.owner_friend_request)
    const [u_id, setUserId] = useState("")
    const [ownerData, setOwnerData] = useState([])
    const [note, setNote] = useState("")
    const [isMore, setMoreMenu] = useState(false)
    const [select_co_Owner, setCowner] = useState("");
    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            getPetProfile(res.token)
        })

    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000

        });

    }
    const getPetProfile = async (token) => {
        await apiCallGetWithToken(apiUrl.get_pet_details + "?pet_id=" + petData.pet_id + "&device_type=" + Platform.OS, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setPetData(res.result)
                setNote(res.result?.pet_note)
                if (res.result.breed.length != 1) {

                    var i, value = "";
                    for (i = 0; i < res.result.breed.length; i++) {
                        if (i != res.result.breed.length - 1)
                            value += res.result.breed[i].pb_name + " (" + res.result.breed[i].breed_percentage + "%)\n"
                        else
                            value += res.result.breed[i].pb_name + " (" + res.result.breed[i].breed_percentage + "%)"
                    }
                    setBreed(value)
                }
                else
                    setBreed(res.result.breed[0].pb_name + " (" + res.result.breed[0].breed_percentage + "%)")

                setImageDataProper(res.result.images)

                if (res.result.login_user_type != "co-owner")
                    getCoownerData(token)
                else {
                    let newArray = []
                    var obj = {
                        u_first_name: res.result.added_by.u_first_name,
                        u_last_name: res.result.added_by.u_last_name,
                        u_image: res.result.added_by.u_image,
                        role: res.result.pet_name + "'s Owner"
                    }
                    newArray.push(obj)
                    setOwnerData(newArray)
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }

    const setImageDataProper = (data) => {
        let newArray = [], i = 0;
        if (data.length == 1) {
            var obj = {}
            obj = {
                image1: data[i].pi_image,
                image2: "",
            }
            newArray.push(obj);
            setPhotoData(newArray)
            return
        }
        // let total = 0;
        // if (data.length % 2 == 0 && data.length != 2)
        //     total = Math.round(data.length / 2) + 1
        // else
        //     total = Math.round(data.length / 2)
        let j = 0;
        while (i < data.length) {
            var obj = {}
            if (j % 2 == 0) {
                obj = {
                    image1: data[i]?.pi_image,
                    image2: data[i + 1]?.pi_image,
                }
                i = i + 2;
                j += 1
            }
            else {
                obj = {
                    image1: data[i]?.pi_image
                }
                i = i + 1;
                j += 1

            }
            newArray.push(obj);

        }
        setPhotoData(newArray)
    }
    const getCoownerData = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_pet_co_owners + "?pet_id=" + petData.pet_id, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                if (res.result.length != 0) {
                    setOwnerData(res.result)
                }
                else {
                    setOwnerData([])
                }
            }

        })
    }



    const clickImage = (uri) => {
        props.navigation.navigation.navigate(strings.FullImageView, { imageUri: uri, petData: petData })
    }
    const viewImage = ({ item, index }) => {
        return (
            <View>
                {index % 2 == 0 ?
                    <View>
                        <TouchableOpacity onPress={() => clickImage(item.image1)}>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={{ uri: item.image1 }}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => clickImage(item.image2)}>
                            <Image style={{ width: 105, height: 77 }} source={{ uri: item.image2 }}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity onPress={() => clickImage(item.image1)}>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={{ uri: item.image1 }}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }


    const clickDot = (item) => {
        setCowner(item)
        if (isMore == true) {
            setMoreMenu(false)
            setTimeout(() => {
                setMoreMenu(true)
            }, 400);
        }
        else
            setMoreMenu(true)

    }
    const renderOwner = ({ item, index }) => {
        return (
            <View

                style={[styles.renderView, { marginTop: index != 0 ? 8 : 0 }]}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigation.replace(strings.UserProfileScreen, { u_id: item.pet_co_owner_owner_id })}
                    style={{ flexDirection: "row" }}>
                    <Image style={styles.profileIcon} source={{ uri: item.u_image }}></Image>
                    <View style={{ alignSelf: "center", marginLeft: responsiveWidth(2) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDesignation}>{item?.role != undefined ? item.role : "Co-owner"}</Text>
                    </View>
                </TouchableOpacity>
                {item.is_friend==true&&
                <View style={styles.iconView}>
                    <TouchableOpacity onPress={() => onGroupIcon()}>
                        <Image style={styles.callIcon} source={Images.contact}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => clickDot(item)}>
                        <Image style={[styles.callIcon, {
                            marginLeft: responsiveWidth(2),
                            width: 13, height: 13, alignSelf: "center"
                        }]} resizeMode={"contain"} source={Images.menuDot}></Image>
                    </TouchableOpacity>
                </View>}
            </View >
        )
    }
    const handleAnimation = async () => {
        if (flag == false) {
            Animated.timing(animatedValue, {
                toValue: 382,
                timing: 1500
            }).start(() => {
                setFlag(true)
            });
        }
        else {
            Animated.timing(animatedValue, {
                toValue: 180,
                timing: 1500
            }).start(setFlag(false));
        }
    }

    const animatedStyle = {
        width: animatedValue,
        height: animatedValue,
        borderRadius: flag ? 0 : animatedValue,
        marginTop: flag ? 0 : 10


    }
    const onGroupIcon = () => {
        props.navigation.navigation.navigate(strings.messageScreen)
    }
    const onCloseClick = () => {
        props.navigation.navigation.goBack(null)
    }
    const getAgeText = () => {
        var a = moment(new Date());
        var b = moment(new Date(petData.pet_dob));

        var years = a.diff(b, 'year');
        if (years <= 0)
            years = petData.pet_age
        else
            years += " years old"
        return years;
    }
    const oNmessage = () => {
        props.navigation.navigation.navigate(strings.typeMessageScreen, { userData: { u_id: petData.added_by.u_id, u_first_name: petData.added_by.u_first_name, u_last_name: petData.added_by.u_last_name } })
    }
    const onCallClick = () => {
        var receiverID = Constant.comeChatUserTag + "" + petData.added_by.u_id;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.USER;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                props.navigation.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: petData.added_by })
            },
            error => {
                console.log("Call initialization failed with exception:", error);
            }
        );
    }
    const onMessageCowner = () => {
        setMoreMenu(false)
        props.navigation.navigation.navigate(strings.typeMessageScreen, {
            userData: {
                u_id: select_co_Owner.pet_co_owner_owner_id,
                u_first_name: select_co_Owner.u_first_name, u_last_name: select_co_Owner.u_last_name
            }
        })

    }
    const onCallCowner = () => {
        setMoreMenu(false)
        setTimeout(() => {
            var receiverID = Constant.comeChatUserTag + "" + select_co_Owner.pet_co_owner_owner_id;
            var callType = CometChat.CALL_TYPE.AUDIO;
            var receiverType = CometChat.RECEIVER_TYPE.USER;

            var call = new CometChat.Call(receiverID, callType, receiverType);

            CometChat.initiateCall(call).then(
                outGoingCall => {
                    props.navigation.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: select_co_Owner })
                },
                error => {
                    console.log("Call initialization failed with exception:", error);
                }
            );

        }, 500);

    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>


            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ alignItems: 'center', }}>
                    <View style={styles.headerView}>
                        <Header
                            pet_name={petData?.pet_name}
                            onGroupIcon={onGroupIcon}
                            onCloseClick={onCloseClick}
                        />
                    </View>
                    <>
                        <TouchableOpacity style={{ marginTop: 70 }} onPress={handleAnimation}  >
                            <Animated.Image
                                source={{ uri: petData.pet_image == "" || petData.pet_image == undefined ? petData.pt_image : petData.pet_image }}
                                resizeMode='cover'
                                style={[styles.petImage, animatedStyle]}
                            />

                        </TouchableOpacity>
                        {owner_friend_request == "1" &&
                            <View style={{ flexDirection: "row", margin: 20 }}>
                                <View style={{ flex: 0.50 }}>
                                    <CommonButton
                                        btnlinearGradient={styles.btnlinearGradient}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        onPress={() => onCallClick()}
                                        text={strings.callOwner}
                                        image={Images.call}
                                        imageStyle={styles.addIcon}
                                    />
                                </View>
                                <View style={{ flex: 0.50, marginLeft: 10 }}>
                                    <CommonButton
                                        btnlinearGradient={styles.btnlinearGradient}
                                        colors={Colors.btnBackgroundColor}
                                        btnText={styles.btnText}
                                        onPress={() => oNmessage()}
                                        text={strings.message}
                                        image={Images.group}
                                        imageStyle={styles.addIcon}
                                    />
                                </View>
                            </View>
                        }
                        {ownerData.length != 0 &&
                            <View style={styles.viewOwner}>
                                <FlatList
                                    data={ownerData}
                                    style={styles.flatListOwner}

                                    renderItem={renderOwner}
                                >
                                </FlatList>
                            </View>
                        }
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Breed
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {breed}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Gender
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {petData.pet_gender}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Birthday
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {moment(petData.pet_dob).format("MM-DD-YYYY").toString()}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Age
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={styles.descriptionView}>
                                <Text style={styles.description}>
                                    {getAgeText()}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.line} />

                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Weight
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={[styles.descriptionView, { alignItems: 'center', flexDirection: 'row' }]} >
                                <Text style={styles.description}>
                                    {petData.pet_size + " pounds"}
                                </Text>

                            </View>
                        </View>
                        <View style={styles.line} />
                        <View style={styles.profileContainer}>
                            <View style={styles.lableView}>
                                <Text style={styles.lable}>
                                    Pet Friendly
                                </Text>
                            </View>
                            <View style={{ flex: 0.1 }}>
                                <Text style={styles.lable}>
                                    :
                                </Text>
                            </View>
                            <View style={[styles.descriptionView, { alignItems: 'center', flexDirection: 'row' }]} >
                                <Text style={styles.description}>
                                    {petData?.pet_is_friendly}
                                </Text>

                            </View>
                        </View>
                        <View style={styles.line} />

                        {petData?.images?.length != 0 &&
                            <>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: "90%", marginVertical: 10 }}>
                                    <Text style={styles.title}>
                                        {"PHOTOS "}
                                        <Text style={[styles.txtDateLbl, { color: "grey" }]}>{"(" + petData?.images?.length + ")"}</Text>

                                    </Text>
                                    <TouchableOpacity onPress={() => props.navigation.navigation.navigate(strings.viewImagesScreen, { images: petData?.images, flag: "pet", owner_id: petData?.added_by?.u_id })} style={{ flexDirection: "row", alignSelf: "center", }}>
                                        <Text style={styles.viewAll}>{"View All"}</Text>
                                        <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                                    </TouchableOpacity>
                                </View>
                                <FlatList
                                    contentContainerStyle={{ paddingLeft: 20, }}
                                    data={photoData}
                                    style={{ alignSelf: 'flex-start', }}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={viewImage}
                                >
                                </FlatList>

                                <TouchableOpacity

                                    style={{ marginHorizontal: 20, marginVertical: 10, alignSelf: 'flex-start', flexDirection: "row" }}>
                                    <Image source={Images.instragram} style={{
                                        tintColor: Colors.primaryViolet,
                                        alignSelf: "center", marginRight: 5, height: 14, width: 14
                                    }} />
                                    <Text style={styles.addMorePhots}>{" View on Instagram"}</Text>
                                </TouchableOpacity>

                            </>
                        }
                        {petData.pet_note != undefined &&
                            <View style={{ width: "90%", marginVertical: 10 }}>
                                <Text style={styles.title}>
                                    NOTES
                                </Text>
                                <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                    <View style={{
                                        backgroundColor: Colors.primaryViolet, width: 2,
                                        //   height: readMoreHeight
                                    }}
                                    />
                                    <ReadMore

                                        numberOfLines={6}
                                        seeMoreStyle={{ color: Colors.primaryViolet }}
                                        seeLessStyle={{ color: Colors.primaryViolet }}
                                        style={[styles.title, { marginLeft: 10, fontWeight: "400" }]}>
                                        {
                                            petData.pet_note

                                        }

                                    </ReadMore>

                                </View>
                            </View>
                        }
                    </>


                </ScrollView>


                <BottomCallMessageView
                    isMore={isMore}
                    onMessage={onMessageCowner}
                    onCall={onCallCowner}

                />
            </SafeAreaView>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View >
    )


}

export default ProfilePage;