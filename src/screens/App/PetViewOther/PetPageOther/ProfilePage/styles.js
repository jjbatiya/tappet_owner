import React from 'react';
import { Dimensions, StyleSheet } from "react-native";
import { Colors } from "../../../../../Resources/Colors";

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from "../../../../../Resources/Fonts";
const { width } = Dimensions.get('window');
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    petImage: { height: 180, width:180,borderRadius:90,marginTop:10 },
    petName: {
        marginVertical: 5,
        color: Colors.white,
        fontWeight: '400',
        fontSize: 30,
        fontFamily: Fonts.LobsterTwoRegular,
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 44,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    addIcon: {
        tintColor: Colors.white,
        width: 15,
        height: 15,
        marginRight: 10
    },
    petSizeView: {
        height: 30, width: 30,
        backgroundColor: Colors.white,
        padding: 5,
        borderRadius: 30,
        marginRight: 10,
        opacity: 0.2,
    },
    petSize: { margin: 5, justifyContent: 'center', height: 20, width: 20, tintColor: Colors.white, position: 'absolute' },
    petStatus: {
        fontWeight: "500",
        fontSize: 12,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: 10,
        fontFamily: Fonts.DMSansRegular,

    },
    profileContainer: { marginTop: 10, marginHorizontal: 20, flexDirection: 'row', justifyContent: 'space-between' },
    lableView: { flex: 0.3 },
    lable: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 16,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.7,
    },
    descriptionView: { flex: 0.7 },
    description: {
        color: Colors.white,
        fontWeight: '400',
        fontSize: 14,
        fontFamily: Fonts.DMSansRegular,

    },
    line: {
        height: 1, backgroundColor: "#FFFFFF", width: width * 0.9,
        marginVertical: 10, opacity: 0.1,
    },
    title: {
        color: Colors.white,
        fontWeight: '700',
        fontSize: 14,
        fontFamily: Fonts.DMSansRegular,
    },
    viewAll: {
        fontWeight: "500", color: Colors.primaryViolet, fontSize: 12,
        fontFamily: Fonts.DMSansRegular
    },
    rightArrow: { height: 10, width: 10, tintColor: Colors.primaryViolet, alignSelf: "center", marginLeft: 7 },
    addMorePhots: {
        fontWeight: "400", fontSize: 12,
        color: Colors.primaryViolet, fontFamily: Fonts.DMSansRegular,
        alignSelf:"center",textAlign:"center"
    },

    icon: {
        width: 19,
        height: 20
    },
    tabLable: {
        marginTop: 5,
        fontSize: 11,
    },
    tabFocus: {
        tintColor: Colors.primaryViolet
    },
    tabUnFocus: {
        tintColor: Colors.white,
        opacity: 0.4
    },
    tabFocusLabel: {
        color: Colors.primaryViolet
    },
    tabUnFocusLabel: {
        color: Colors.white,
        opacity: 0.4
    },
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },
    modalIconStyle: {
        tintColor: Colors.white,
        width: 25,
        height: 25,
        marginVertical: responsiveHeight(2)
    },
    modalTitle: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    modalDescription: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    //owner render view
    txtName: {
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
    },
    txtDesignation: {
        fontSize: 12, lineHeight: 15.62, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular, opacity: 0.6
    },
    callIcon: { height: 16, width: 16, tintColor: Colors.white },
    renderView: {
        flexDirection: "row", justifyContent: "space-between",
        marginHorizontal: responsiveWidth(4), 
    },
    viewOwner: { marginHorizontal: 20, marginVertical: responsiveHeight(2), flex: 1, borderRadius: 4 },
    flatListOwner:{
        backgroundColor: Colors.theme, width: responsiveWidth(90),paddingVertical:responsiveHeight(1.8),borderRadius:4
    },
    iconView:{ flexDirection: "row", alignSelf: "center", marginRight: responsiveWidth(2) },
    profileIcon:{ height: 48, width: 48, borderRadius: 24 },
    //note modal
    txtRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 16, lineHeight: 22, textAlign: "center", color: Colors.white,
        marginTop: 5
    },
    modalNote: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 210,
        padding: 20,
        borderRadius: 4,
        marginTop: responsiveHeight(30),
        width:width-40
    },
   
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,

    },
    input: {
        borderWidth: 0,
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        width: width - 50,
        height: 60
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular,
    },
    txtDateLbl: {
        fontWeight: "700", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, textTransform: "uppercase",
        marginVertical: responsiveHeight(1.5), fontFamily: Fonts.DMSansRegular
    },

    //header view
    headerView: {
        position: "absolute",
        top: 0,
        flex: 1,
        width: "100%"
    },
})
