import React, { useEffect, useRef, useState } from 'react';
import { Image, SafeAreaView, StyleSheet, View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Colors } from '../../../../../../Resources/Colors';
import { Fonts } from '../../../../../../Resources/Fonts';
import { Images } from '../../../../../../Resources/Images';
import { strings } from '../../../../../../Resources/Strings';
import Header from '../../../Header';


const FullImageView = (props) => {
    const [imageUri] = useState(props.route.params.imageUri)
    const onGroupIcon = () => {
        props.navigation.navigate(strings.messageScreen)
    }
    const onCloseClick = () => {
        props.navigation.goBack(null)
    }
    return (

        <SafeAreaView
            style={styles.container}>
            <Header
                pet_name={""}
                onGroupIcon={onGroupIcon}
                onCloseClick={onCloseClick}
            />
            <Image
                source={{ uri: imageUri }}
                style={{ height: '70%', marginTop: 10 }}
                resizeMode={"contain"}
            />
            <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                <Text style={styles.txtPetName}>Stacy Green</Text>
                <Text style={styles.txtPetStatus}>{"Sunny day with my bad boy! ❤️❤"}</Text>
                <Text style={styles.txtPetsDate}>Aug 24, 2020</Text>
                <View style={styles.viewLine} />
                <View style={styles.likeView}>
                    <View style={{ flexDirection: "row" }}>
                        <Image
                            style={styles.likeImg}
                            source={Images.likeIcon}
                            resizeMode={"contain"} />
                        <Text style={styles.txPetLike}>256 likes</Text>
                    </View>
                    <TouchableOpacity
                        style={{ alignSelf: 'flex-start', flexDirection: "row" }}>
                        <Image source={Images.instragram} style={styles.imgInstra} />
                        <Text style={styles.addMorePhots}>{" View on Instagram"}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    txtPetName: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "700",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    txtPetStatus: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: 'rgba(255,255,255,0.8)',
        marginTop: 4
    },
    txtPetsDate: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        fontSize: 10,
        lineHeight: 13.02,
        color: 'rgba(255,255,255,0.6)',
        marginTop: 6
    },
    viewLine: {
        height: 1,
        backgroundColor: 'rgba(255,255,255,0.15)',
        marginVertical: 10
    },
    likeView: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    txPetLike: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
    },
    addMorePhots: {
        fontWeight: "400", fontSize: 12,
        color: Colors.primaryViolet, fontFamily: Fonts.DMSansRegular,
        alignSelf: "center", textAlign: "center"
    },
    imgInstra: {
        tintColor: Colors.primaryViolet,
        alignSelf: "center", marginRight: 5, height: 14, width: 14
    },
    likeImg:{ height: 12, width: 12, marginTop: 2, marginRight: 10 }

})
export default FullImageView;