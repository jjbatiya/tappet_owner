import { Platform, StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"#0C0024"
    },
    linearGradient: {
        flex: 1,
        marginVertical:Platform.OS=="ios"?45:0
    },
    //chart view
    viewBox1:{ backgroundColor: '#FFB91B', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5 },
    txtMiles:{ fontSize: 12, fontWeight: "400", lineHeight: 15.62, color: Colors.white },
    txtLabel:{ fontSize: 10, fontWeight: "400", lineHeight: 14, color: Colors.white, opacity: 0.6 },
    viewBox2:{ backgroundColor: '#FF2D65', height: 8, width: 8, borderRadius: 2, marginTop: 4, marginRight: 5},
    viewBox3:{ backgroundColor: '#00CDF9', height: 8, width: 9, borderRadius: 2, marginTop: 4, marginRight: 5 },
    chartView:{
        flexDirection: "row", justifyContent: "space-between",
       backgroundColor: "#0C0024", borderRadius: 4,
       flex:1
    },
    txtViewLocation:{
        fontWeight: "500", fontSize: 14,
        lineHeight: 18.23, color: Colors.primaryViolet, alignSelf: "center",
        marginBottom:5,
    }
})