import React, { useRef, useState, useEffect } from 'react';
import { View, Dimensions, StatusBar, Image, Text, StyleSheet, TouchableOpacity, SafeAreaView, Platform } from 'react-native';
import { WebView } from 'react-native-webview';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { Colors } from '../../../Resources/Colors';
import { Fonts } from '../../../Resources/Fonts';
import { Images } from '../../../Resources/Images';
const { width, height } = Dimensions.get("window")
import { CometChat } from '@cometchat-pro/react-native-chat';
import { strings } from '../../../Resources/Strings';

const WebViewScreen = (props) => {
    const [url] = useState(props.route.params.url)
    const [title] = useState(props.route.params?.title)
    const [sendDate] = useState(props.route.params?.date)
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                //  console.log("Incoming call coming:", call);
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <SafeAreaView style={{ height: height, flex: 1, backgroundColor: Colors.black }}>
            <StatusBar hidden barStyle={'light-content'} backgroundColor={'#0C0024'} />

            <View style={styles.headerStyle}>
                <TouchableOpacity onPress={() => props.navigation.goBack(null)} style={{ flex: 0.3 }}>
                    <Image style={styles.backImgStyle} source={Images.backArrow} />
                </TouchableOpacity>
                <View style={{ flex: 0.7 }} >
                    <Text style={styles.txtTitle}>{title}</Text>
                    <Text style={{ color: Colors.white }}>{sendDate}</Text>

                </View>
            </View>
            <WebView source={{ uri: url }}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
            />
            {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel}
                isVisible={isVisible} callAccept={callAccept} />}
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    headerStyle: {
        flexDirection: "row", flex: 0.1,
        alignItems: "center", paddingHorizontal: 15,

    },
    backImgStyle: { height: 20, width: 20, tintColor: Colors.white },
    txtTitle: {
        color: Colors.white,
        fontSize: 16,
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "600",
        lineHeight: 20
    }
})
export default WebViewScreen;