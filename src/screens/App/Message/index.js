import React, { useState } from 'react';
import { View, Text, useWindowDimensions, Platform, StyleSheet, TouchableOpacity, Image } from 'react-native';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import AllMessage from './AllMessage';
import CallHistory from './CallHistory';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { strings } from '../../../Resources/Strings';
import NotificationController from '../../../Components/NotificationController';
import ChatMessage from '../../../utils/svg/ChatMessage';


const Message = (props) => {
    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'AllMessages', title: 'All Messages' },
        { key: 'CallHistory', title: 'Call History' },
    ]);
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const renderScene = ({ route, index }) => {
        switch (route.key) {
            case 'AllMessages':
                return <AllMessage navigation={props.navigation} />;
            case 'CallHistory':
                return <CallHistory navigation={props.navigation} />;
            default:
                return null;
        }
    };
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}

        />
    );
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height: Platform.OS == "ios" ? responsiveHeight(12.4) : responsiveHeight(10) }}
            >
                <AppHeader
                    navigation={props.navigation}
                    title={'Message'}
                    titleText={style = { marginLeft: -30 }}
                    backButton={Images.backIcon}
                    Tab={true}
                />

            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}
            />
            {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel}
                isVisible={isVisible} callAccept={callAccept} />}
            {isVisible == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisible}
                    onMessageNotification={() => { }}

                />
            }
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={styles.flotingView}
            >
                <TouchableOpacity
                    onPress={() => { props.navigation.navigate(strings.userchatlist) }}
                    style={styles.plusView}>
                    <ChatMessage width={24} height={24} />

                </TouchableOpacity>
            </LinearGradient>
        </View >
    )
}
const styles = StyleSheet.create({
    //floting view
    flotingView: {
        position: 'absolute',
        width: 54,
        height: 54,
        right: 15,
        bottom: 30,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    plusView: {
        height: 54, width: 54, alignItems: 'center',
        justifyContent: 'center',
    },
})
export default Message;