import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 5,
       // marginHorizontal: responsiveWidth(4),
        alignItems: "center",
        justifyContent: "center",
        width:96
    },

    //render View
    mainRender:{
        flexDirection:"row",
        marginVertical:responsiveHeight(1),
        alignItems:"center",
        
    },
   
    imgStyle:{
        height:48,
        width:48,
        borderRadius:24
    },
    peopleImg:{ height: 15, width: 15, borderRadius: 10 },
    radioText: {
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        lineHeight:18.23  ,
        fontFamily:Fonts.DMSansRegular

    },
    txtInvite:{
        fontWeight:"400",
        fontSize:12,
        lineHeight:15.62,
        color:Colors.white,
        opacity:0.6,
        marginLeft:10,
        fontFamily:Fonts.DMSansRegular


    },
    callIncoming:{
        height: 13, width: 13,alignSelf:"center",
        tintColor:Colors.green 
    },
    calloutGoing:{
        height: 13, width: 13,alignSelf:"center",
        tintColor:Colors.red 
    }
})