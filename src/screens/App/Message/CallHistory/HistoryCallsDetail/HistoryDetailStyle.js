import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },

    //header view
    headerView: { marginHorizontal: responsiveWidth(3), marginVertical: responsiveHeight(1), flexDirection: "row", alignItems: "center", justifyContent: "space-between" },
    backImage: { width: 15, height: 15, tintColor: Colors.white },
    txtTitle: {
        fontSize: 16, fontWeight: "700", lineHeight: 22, color: Colors.white,
        textAlign: "center", fontFamily: Fonts.DMSansRegular
    },

    //profileView
    viewProfile: { flexDirection: "row", marginHorizontal: responsiveWidth(4), marginTop: responsiveHeight(3) },
    imgProfile: { height: 48, width: 48, borderRadius: 24 },
    txtName: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, alignSelf: "center", marginLeft: responsiveWidth(4), fontFamily: Fonts.DMSansRegular
    },

    //viewLine
    viewLine: { height: 1, backgroundColor: Colors.white, opacity: 0.15, margin: responsiveHeight(2), },

    //renderView
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: 10,
        fontFamily:Fonts.DMSansRegular

    },
    callIncoming: {
        height: 13, width: 13, alignSelf: "center",
        tintColor: Colors.green
    },
    calloutGoing: {
        height: 13, width: 13, alignSelf: "center",
        tintColor: Colors.red
    },
    renderView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: responsiveHeight(0.5) }

})