import React, { useRef, useState, useEffect } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, TextInput, SafeAreaView, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { strings } from '../../../../../Resources/Strings';
import { styles } from './HistoryDetailStyle';
import { Images } from '../../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { apiCallGetWithToken, getUserData } from '../../../../../utils/helper';
import { comeChatUserTag } from '../../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import moment, { now } from 'moment';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Loader from '../../../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { toastConfig } from '../../../../../utils/ToastConfig';


const HistoryCallsDetail = (props) => {
    const [userData] = useState(props.route.params.userData)
    const [token, setToken] = useState("")
    const [u_id, setUserId] = useState("")
    const [receiverID, setReceiverID] = useState(comeChatUserTag + props.route.params.userData.u_id)
    const [callHistory, setCallHistory] = useState(null)
    const [isshowing, setShowing] = useState(false)

    useEffect(() => {
        getUserData().then(async res => {
            setToken(res.token)
            setUserId(res.u_id)
            await setReceiverID(comeChatUserTag + props.route.params.userData.u_id)
            getCallHistoryUser(res.token)
        })
    }, []);
    const getCallHistoryUser = async (token) => {
        setShowing(true)

        await apiCallGetWithToken(apiUrl.get_call_history_by_user_id + "?other_user_id=" + userData.u_id, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setCallHistory(res.result)
                console.log(res.result)
            }
            else {
                showMessage(strings.error,strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const getCallHistory = () => {
        var UID = receiverID;
        var limit = 30;
        let messagesRequest = new CometChat.MessagesRequestBuilder()
            .setCategories(["call"])
            .setLimit(limit)
            .setUID(UID)
            .build();

        messagesRequest.fetchPrevious().then(
            messages => {
                setCallHistory(messages)
            },
            error => {
                console.log("Message fetching failed with error:", error);
            }
        );

    }

    const renderView = ({ item, index }) => {
        var newDate = ""
      //  if (item.call_duration == 300)
            newDate = moment.utc(item.call_datetime).local().format('MMMM DD hh:mm a');
        //else
         // newDate = item.call_datetime //moment.unix(item.sentAt).format('MMMM DD, YYYY');
        var callByMe = item.call_from_user_id != userData.u_id ? true : false ///item.callInitiator.uid != receiverID ? true : false
        var status = item.call_history_status//item.status;

        return (
            <View style={styles.renderView}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    {
                        callByMe ?
                            <Image style={styles.callIncoming} source={Images.outgoingCall}></Image> :
                            <Image style={status == "2" ? styles.calloutGoing : styles.callIncoming} source={status == "2" ? Images.incomingCall : Images.incomingCallReceive}></Image>
                    }
                    {
                        callByMe ?
                            <Text style={styles.txtInvite}>{newDate}</Text> :
                            <Text style={status == "2" ? [styles.txtInvite, { color: Colors.red }] : styles.txtInvite}>{newDate}</Text>

                    }
                </View>
                <Text style={styles.txtInvite}>{status == "2" ? "-" : item.call_duration + " sec."}</Text>

            </View>
        )
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <CommonStatusBar />
                <SafeAreaView style={{ flex: 1, marginTop: Platform.OS == "android" ? responsiveHeight(6) : 0 }}>
                    <View style={styles.headerView}>
                        <TouchableOpacity onPress={() => props.navigation.goBack(null)} >
                            <Image style={styles.backImage} source={Images.backArrow}></Image>
                        </TouchableOpacity>
                        <Text style={styles.txtTitle}>{strings.callHistory}</Text>
                        <Text></Text>
                    </View>
                    <View style={styles.viewProfile}>
                        <Image style={styles.imgProfile} source={{ uri: props.route.params.userData.u_image }}></Image>
                        <Text style={styles.txtName}>{props.route.params.userData.user_name}</Text>
                    </View>
                    <View style={styles.viewLine}></View>
                    <FlatList
                        style={{ marginHorizontal: responsiveHeight(2), marginVertical: responsiveHeight(0.5) }}
                        data={callHistory?.history}
                        renderItem={renderView}
                    >

                    </FlatList>
                </SafeAreaView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default HistoryCallsDetail;