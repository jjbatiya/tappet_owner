import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './CallHistoryStyle';
import { Images } from '../../../../Resources/Images';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import moment, { now } from 'moment';
import Toast from 'react-native-toast-message';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { apiCallGetWithToken, getUserData, imageUploadApi } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import Loader from '../../../../Components/Loader';
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import { strings } from '../../../../Resources/Strings';
import { toastConfig } from '../../../../utils/ToastConfig';


const CallHistory = (props) => {
    const [userConversion, setUserConversion] = useState([])
    const [userList, setUserList] = useState([])
    const [groupList, setGroupList] = useState([])
    const [isshowing, setShowing] = useState(false)
    const [lastMessageUserList, setLastMessageUserList] = useState([])
    const [u_id, setUserID] = useState("")
    const [token, setToken] = useState("")
    useEffect(async () => {
        getUserData().then(res => {
            setUserID(res.u_id)
            setToken(res.token)
            getCallHistoryUser(res.token)
        })
        //await getRetrivedData("user");
    }, [])
    const getCallHistoryUser = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_call_history, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                //console.log("addd",res.result)
                setUserList(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const getRetrivedData = (type) => {
        let limit = 30;
        let conversationRequest = new CometChat.ConversationsRequestBuilder()
            .setLimit(limit)
            .setConversationType(type)
            .build();

        conversationRequest.fetchNext().then(
            conversationList => {
                setUserConversion(conversationList)
                getUserAndGroupData(conversationList, [])
            }, error => {
                console.log("Conversations list fetching failed with error:", error);
            }
        );
    }
    const getUserAndGroupData = (userData, groupData) => {

        let userIds = userData.map(a => a.conversationWith.uid);
        for (var i = 0; i < userIds.length; i++) {
            userIds[i] = userIds[i].replace(comeChatUserTag, "")
        }
        let groupIds = groupData.map(a => a.conversationWith.guid);
        for (var i = 0; i < groupIds.length; i++) {
            groupIds[i] = groupIds[i].replace(comeGroupTag, "")
        }
        getUserAndGroupDetail(userIds, groupIds)

    }
    const getUserAndGroupDetail = async (userList, groupList) => {
        let token = "";
        await getUserData().then(res => {
            token = res.token;
        })
        let data = new FormData();
        for (var i = 0; i < userList.length; i++)
            data.append("userIds[]", userList[i]);
        for (var i = 0; i < groupList.length; i++)
            data.append("groupIds[]", groupList[i]);
        await imageUploadApi(data, apiUrl.get_user_and_group_details, token).then(res => {
            if (res.status) {
                setGroupList(res.result.groupList)
                setShowing(false)
                getLastCall(res.result.userList)

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setShowing(false)
            }

        })
    }
    const getLastCall = (list) => {
        for (var i = 0; i < list.length; i++) {
            getCallHistory(comeChatUserTag + list[i].u_id, "user", i, list[i])
        }
    }
    const getCallHistory = async (id, type, index, data) => {
        var UID = id;
        var limit = 1;

        let messagesRequest = await new CometChat.MessagesRequestBuilder()
            .setCategories(["call"])
            .setLimit(limit)
            .setUID(UID)
            .build();

        await messagesRequest.fetchPrevious().then(
            messages => {
                let newArray = [...userList]
                var obj = { ...data }
                obj.lastMessage = messages.length != 0 ? messages[0] : null
                newArray[index] = obj
                setUserList(newArray)

            },
            error => {
                console.log("Message fetching failed with error:", error);
            }
        );

    }

    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
  
    const renderView = ({ item, index }) => {
        var  newDate = moment.utc(item.call_datetime).local().format('MMMM DD hh:mm a');

      // var newDate = convertUTCToLocalTime(new Date(item.call_datetime))
     //  newDate = item.call_datetime//moment(newDate).format('MMMM DD, hh:mm A');

        var callByMe = item.call_from_user_id == u_id ? true : false //item?.lastMessage?.callInitiator?.uid != comeChatUserTag+u_id ? false : true
        var status = item.call_history_status //item?.lastMessage?.status;
        var secTag = item.call_duration == "" || item.call_duration == '0' ? "" : " • " + item.call_duration + " sec"
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.historycallsDetailScreen, { userData: item })}
                style={styles.mainRender}>

                <Image style={styles.imgStyle} source={{ uri: item.u_image }}></Image>

                <View style={{ marginLeft: 20, marginTop: 5 }}>
                    {/* <Text style={styles.radioText}>{item.u_first_name + " " + item.u_last_name}</Text> */}
                    <Text style={styles.radioText}>{item.user_name}</Text>
                    <View style={{ flexDirection: "row", marginTop: 5 }}>

                        {
                            callByMe ?
                                <Image style={styles.callIncoming} source={Images.outgoingCall}></Image> :
                                // <Image style={status == "initiated" || status == "unanswered" ? styles.calloutGoing : styles.callIncoming} source={status == "initiated" ? Images.incomingCall : Images.incomingCallReceive}></Image>
                                <Image style={status == "2" ? styles.calloutGoing : styles.callIncoming} source={status == "2" ? Images.incomingCall : Images.incomingCallReceive}></Image>
                        }
                        <Text style={styles.txtInvite}>{newDate + secTag}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>

                    <FlatList
                        data={userList}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderView}
                    >

                    </FlatList>
                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default CallHistory;