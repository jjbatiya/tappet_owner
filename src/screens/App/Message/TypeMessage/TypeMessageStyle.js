import { StyleSheet,Dimensions } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { moderateScale } from 'react-native-size-matters';
import { Fonts } from '../../../../Resources/Fonts';
const { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    item: {
        marginVertical: moderateScale(9, 5),
        flexDirection: 'row'
    },
    itemIn: {
        marginLeft: 10
    },

    balloon: {
        maxWidth: moderateScale(250, 2),
        paddingHorizontal: moderateScale(10, 2),
        paddingTop: moderateScale(5, 2),
        paddingBottom: moderateScale(7, 2),
        borderRadius: 20,
    },
    arrowContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1,
        flex: 1
    },
    arrowLeftContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },


    arrowLeft: {
        left: moderateScale(-6, 0.5),
    },
    txtMessage: {
        paddingTop: 5, color: 'white', fontSize: 14,
        fontWeight: "400", lineHeight: 18.23, paddingHorizontal: responsiveWidth(3), fontFamily: Fonts.DMSansRegular
    },
    txtTime: {
        fontSize: 8, paddingTop: 5, paddingHorizontal: 2, fontWeight: "400",
        lineHeight: 10.42, alignSelf: "flex-end", color: Colors.white, opacity: 0.6,
        fontFamily: Fonts.DMSansRegular

    },
    viewSend: { alignSelf: "flex-end", borderRadius: 5, marginTop: responsiveHeight(2), marginRight: 10, maxWidth: '60%' },
    txtDate: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62, color: Colors.white,
        opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    callView: { flexDirection: "row", alignItems: "center", justifyContent: "center", marginVertical: responsiveHeight(3) },
    imgCall: { marginRight: responsiveWidth(2) },

    //bottom view
    bottomViewPlus: {
        height: 50, position: "absolute", bottom: 0,
        backgroundColor: Colors.theme, flexDirection: "row", width: '100%', alignItems: "center", justifyContent: "center"
    },
    txtInputStyle: {
        flex: 0.8, height: 40, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    txtPost: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.primaryViolet, fontFamily: Fonts.DMSansRegular
    },
    txtPostView: {
        flex: 0.1
    },

    //header view
    headerView: {
        marginHorizontal: responsiveWidth(3), marginVertical: responsiveHeight(1), flexDirection: "row",
        alignItems: "center", justifyContent: "space-between",
        marginTop: responsiveHeight(6),
    },
    backImage: { width: 15, height: 15, tintColor: Colors.white },
    borderStyle: {
        backgroundColor: Colors.opacityColor2,
        height: 36,
        width: responsiveWidth(65),
        borderRadius: 4,
        marginLeft: responsiveWidth(3),

    },
    txtSerach: {
        width: responsiveWidth(65), alignSelf: "center",
        height: 36, paddingHorizontal: 5, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    imgUp: { height: 15, width: 15, tintColor: Colors.white },
    imgDown: { height: 15, width: 15, tintColor: Colors.white, marginLeft: 20 },

    //tag modal 
    titleText: {
        fontSize: 12, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular, textTransform: "uppercase"
    },
    mainGroupView: { flexDirection: "row", marginHorizontal: 0, marginVertical: 10, alignItems: "center" },

    imgGroup: {
        height: 32,
        width: 32,
        borderRadius: 16,
    },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    peopleImg: { height: 16, width: 16, borderRadius: 8, borderWidth: 1, borderColor: Colors.theme },
    radioText: {
        fontSize: 12,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center"
    },
    tagView: {
        position: "absolute", bottom: 60, width: 242, marginLeft: 30,
        backgroundColor: Colors.theme, borderWidth: 1, borderColor: Colors.opacityColor
    },
    //calling bubble view
    leftVieW: {
        alignSelf: "flex-start",
        padding: 20,
        borderRadius: 4,
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginLeft: 5

    },
    rightView: {
        alignSelf: "flex-end",
        backgroundColor: "blue",
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 4,
        marginRight: 10
    },

    //bottomView
    borderView: {
        height: 68, width: 68,
        borderRadius: 40, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white
    },
    cameraStyle: {
        height: 20, width: 20, position: "absolute",
        tintColor: Colors.white, alignSelf: "center", resizeMode: "contain"
    },
    labelText: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white,
        alignSelf: "center", marginVertical: responsiveHeight(1),
        fontFamily: Fonts.DMSansRegular,
    },
    //location view
    imgMap: {
        width: responsiveWidth(71.5),
        height: 200,
        borderRadius: 4,
        marginHorizontal: 8
    },
    //event view render 
    eventListContainer: {
        paddingHorizontal: 10
    },
    createEventContainer: {
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center'
    },
    createEventImage: {
        backgroundColor: '#FFFFFF26',
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 60,
        marginRight: 16,

    },
    createEvent: {
        color: Colors.primaryViolet,
        fontSize: 14,
        fontWeight: '400'
    },
    smallIcon: {
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        position: 'absolute',
        top: 30,
        left: 32
    },
    smallIcon2: {
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        position: 'absolute',
        top: 30,
        left: 32
    },
    eventImage: {
        width: 48,
        height: 48,
        borderRadius: 48
    },
    smallEventIcon: {
        width: 10,
        height: 10,
        tintColor: Colors.white,
    },
    eventTitle: {
        fontWeight: '400',
        fontSize: 14,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },
    eventTime: {
        fontWeight: '400',
        fontSize: 12,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular

    },
    eventItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width * 0.7
    },
    radioBtn: {
        borderWidth: 1,
        width: 20,
        height: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioBtnSelect: {
        width: 10,
        height: 10,
        backgroundColor: Colors.primaryViolet,
        borderRadius: 10
    },
    ImagePost: {
        alignItems: 'flex-end',
        paddingRight: 20,
        marginTop: 20,
    },
    //event header
    textInputSearch: {
        width: width * 0.95,
        height: 40,
        backgroundColor: '#FFFFFF26',
        borderRadius: 5,
        paddingHorizontal: 5,
        color: Colors.white,
        opacity: 0.4,
        fontSize: 14,
        flexDirection: 'row',
        alignItems: 'center',
        fontFamily: Fonts.DMSansRegular

    },
    serachIcon: {
        width: 15,
        height: 15,
        tintColor: Colors.white,
        marginHorizontal: 10
    },
    searchBox: {
        marginTop: 5,
        width: width * 0.85,
        height: 40,
        alignItems: 'center',
        color: Colors.white,
        opacity: 0.4,
        fontSize: 12,
        fontFamily: Fonts.DMSansRegular

    },
    drawer: {
        borderWidth: 2,
        width: 50,
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 15,
        borderColor: '#FFFFFF26'
    },

    //event footer 
    verifyBtnGroup: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "100%", alignSelf: "center" },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },

    //render Event box
    renderStyle: { height: 180, margin: responsiveHeight(1), borderRadius: 4, },
    imgStyle: { height: 180, width: responsiveWidth(72), borderRadius: 4 },
    upperView: { position: "absolute", margin: 10, width: "95%" },
    upperSubView: { alignItems: "center", justifyContent: "space-between", flexDirection: "row", },
    peopleImg: { height: 24, width: 24, borderRadius: 12 },
    peopleImg1: { height: 24, width: 24, borderRadius: 12, marginLeft: -8 },
    moreView: { height: 24, width: 24, marginLeft: -8, alignItems: "center", justifyContent: "center" },
    moreBorderView: { backgroundColor: Colors.black, borderRadius: 15, opacity: 0.6, width: 24, height: 24, position: "absolute" },
    txtMore: {
        color: Colors.white, fontWeight: "700", fontSize: 8,
        lineHeight: 11.72, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    dateView: { height: 31, width: 30, backgroundColor: Colors.white, alignSelf: "flex-end", borderRadius: 2, marginRight: 5 },
    txtDateCal: {
        fontSize: 12, fontWeight: "700", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    txtMonth: {
        fontSize: 8, fontWeight: "400", textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 0, margin: 10 },
    txtTitle: {
        fontWeight: "400", fontSize: 18, lineHeight: 27.5, color: Colors.white,
        fontFamily: Fonts.LobsterTwoRegular,
        letterSpacing: 0.4
    },
    bottomBorderView: { height: 1, backgroundColor: Colors.white, opacity: 0.2, marginVertical: 3 },
    placeIcon: { height: 13, width: 12, opacity: 0.7, tintColor: Colors.white },
    txtAddress: {
        fontWeight: "400", fontSize: 10, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 10, fontFamily: Fonts.DMSansRegular
    },
    imgProfileEvent: { height: 24, width: 24, borderRadius: 12, alignSelf: "flex-start", marginRight: 10 },

})