import React, { createRef, useRef, useState, useEffect } from 'react';
import { View, Text, FlatList, Image, TextInput, TouchableOpacity, Modal, Platform, KeyboardAvoidingView, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './TypeMessageStyle';
import { Images } from '../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MessageHeader from '../../../../Components/MessageHeader';
import Svg, { Path } from 'react-native-svg';
import { moderateScale } from 'react-native-size-matters';
import RBSheet from "react-native-raw-bottom-sheet";
import MoreMenu from '../../../../Components/MoreMenu';
import { Fonts } from '../../../../Resources/Fonts';
import { CallOutgoing } from '../../../../utils/svg/CallOutgoing';
import { PlusIcon } from '../../../../utils/svg/PlusIcon';
import { apiCallGetWithToken, apiCallWithToken, getUserData, mediaMessage, requestCameraPermission, requestExternalWritePermission, sendEventMessage, sendNormalMessage, sendUserEventMessage } from '../../../../utils/helper';
import { comeChatUserTag } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import moment, { now } from 'moment';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import CallingBubbleView from '../../../../Components/CallingBubbleView';
import * as ImagePicker from "react-native-image-picker"
import ChatFileBubbleSender from '../../../../Components/ChatFileBubbleSender';
import ChatFileBubbleReceiver from '../../../../Components/ChatFileBubbleReceiver';
import DocumentPicker from 'react-native-document-picker'
import AudioRecord from '../../../../Components/AudioRecord';
import LocationPicker from '../../../../Components/LocationPicker';
import EventMap from '../../../../Components/EventMap';
import { EventIconUnselect } from '../../../../utils/svg/EventIconUnselect';
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import CometChatActionMessageBubble from '../../../../Components/CometChatActionMessageBubble';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import { toastConfig } from '../../../../utils/ToastConfig';
const { width, height } = Dimensions.get('window');

const bottomMenu = [
    {
        id: 1,
        title: "Camera",
        image: Images.cameraIcon,
        key: "Camera"
    },
    {
        id: 2,
        title: "Photos",
        image: Images.addPicture,
        key: "Photos"

    },
    {
        id: 3,
        title: "Event",
        image: Images.addEventIcon,
        key: "Event"

    },
    {
        id: 4,
        title: "Location",
        image: Images.placeIcon,
        key: "Location"

    },
    {
        id: 5,
        title: "Audio",
        image: Images.mic,
        key: "Audio"

    },
    {
        id: 6,
        title: "Documents",
        image: Images.document,
        key: "Documents"

    }
]

const tagData = [
    {
        id: 1,
        title: "members",
        data: [
            {
                id: 1,
                image: Images.people,
                name: "David Rodriguez"
            },
            {
                id: 2,
                image: Images.people1,
                name: "Amanda Lewis"
            },
            {
                id: 3,
                image: Images.people,
                name: "Stacy Green"
            },
            {
                id: 4,
                image: Images.people1,
                name: "Stacy Green"
            }
        ]
    },
    {
        id: 2,
        title: "Pets",
        data: [
            {
                id: 1,
                name: "Cuco",
                ownwerName: "Green Mile",
                image: Images.dogIcon
            },
            {
                id: 2,
                name: "Taz",
                ownwerName: "Joseph Jose",
                image: Images.dogIcon1
            },
            {
                id: 3,
                name: "Robert",
                ownwerName: "Stacy Free",
                image: Images.dogIcon2
            }
        ]
    }
]

const TypeMessage = (props) => {
    const [bottomData, setBottomData] = useState([
        {
            key: "viewProfile",
            title: "View Profile",
            image: Images.userIcon
        },
        {
            key: "viewPhotos",
            title: "View Photos",
            image: Images.viewPhoto
        },
        {
            key: "searchinChat",
            title: "Search in Chat",
            image: Images.serachIcon
        },
        {
            key: "callHistory",
            title: "Call History",
            image: Images.history
        },
        {
            key: "blockUser",
            title: "Block User",
            image: Images.blocked
        },
        {
            key: "delete",
            title: "Delete Chat",
            image: Images.deleteIcon
        },
    ])
    let refSheet = useRef();
    let refSheetBottom = useRef();
    let flatList = useRef(null);
    const [serachFlag, setSerachFlag] = useState(false)
    const [serachTxt, setSerachText] = useState("")
    const [serachTxtEvent, setSerachTextEvent] = useState("")
    const [indexArray, setIndexArray] = useState([])
    const [initialIndex, setInitailIndex] = useState(-1)
    const [isVisible, setVisible] = useState(false)
    const [receiverID] = useState(comeChatUserTag + "" + props.route.params.userData.u_id)
    const [message, setMessage] = useState("")
    const [token, setToken] = useState("")
    const [u_id, setUserId] = useState("")
    const [userImage, setUserImage] = useState("")
    const [messageData, setMessageData] = useState([])
    const [isshowing, setShowing] = useState(false)
    const [userData, setUserData] = useState(props.route.params.userData)
    const [comeChatUser, setComeChatUser] = useState("")
    const [isVisibleRecord, setIsVisibleRecord] = useState(false)
    const [isSerachMap, setSerachMap] = useState(false)
    let refSheetEvent = useRef()
    const [eventSelectItem, setEventItem] = useState(null)
    const [eventData, setEventData] = useState([])
    const [eventDataBackUp, setEventDataBackUp] = useState([])
    const [selectedEventRadio, setSelectedEventRadio] = useState();
    const [monthArray] = useState(["Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"])
    const [callData, setCallData] = useState(null)
    const [isVisibleCall, setVisibleCall] = useState(false)
    const callClick = () => {

        var receiverID = comeChatUserTag + "" + props.route.params.userData.u_id;
        var callType = CometChat.CALL_TYPE.AUDIO;
        var receiverType = CometChat.RECEIVER_TYPE.USER;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
            outGoingCall => {
                props.navigation.navigate(strings.outgoingCallScreen, { callData: outGoingCall, flag: "user", userData: userData })
                //props.navigation.navigate("callScreen", { callData: outGoingCall,flag:"user" })

                // perform action on success. Like show your calling screen.
            },
            error => {
                console.log("Call initialization failed with exception:", error);
                if(error?.code=="ERR_BLOCKED_SENDER")
                {
                    showMessage(strings.error, strings.msgTag,comeChatUser.name + " block you so can't send message")

                }
            }
        );
    }
    const menuClick = () => {
        refSheet.open();
    }
    useEffect(async () => {
        if (initialIndex > -1)
            _goToNextPage()
        else {
            getMessage();
            getComeChatUserDetail()
        }

        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            setUserImage(res.u_image)
            getEventData(res.token)
        })
    }, [initialIndex]);
    var listenerID = receiverID;
    CometChat.addMessageListener(
        listenerID,
        new CometChat.MessageListener({
            onTextMessageReceived: textMessage => {
                getMessage();
                // Handle text message
            },
            onMediaMessageReceived: mediaMessage => {
                getMessage();
                // Handle media message
            },
            onCustomMessageReceived: customMessage => {
                getMessage();

                // Handle custom message
            }
        })
    );
    const getProfileData = async (data) => {
        await apiCallGetWithToken(apiUrl.user + "/" + userData.u_id + "?device_type=" + Platform.OS, data.token).then(res => {
            setShowing(false)
            if (res.status == true) {
                setUserData(res.result)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                //  console.log("Incoming call coming:", call);
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisibleCall(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisibleCall(false)
            }
        })
    );
    const getEventData = async (token) => {
        setShowing(true)
        await apiCallGetWithToken(apiUrl.get_all_events + "?event_type=upcoming", token).then(res => {

            if (res.status == true) {
                setEventData(res.result)
                setEventDataBackUp(res.result)
            }
            else {
                setShowing(false)
            }
        })
        await getProfileData({ token: token })

    }
    const callFunction = (index) => {
        indexArray.push(index)
        indexArray.length == 1 ? setInitailIndex(0) : null
    }
    const getMessage = () => {
        var UID = receiverID;
        var limit = 30;
        let messagesRequest = new CometChat.MessagesRequestBuilder()
            .setCategories(["message", "custom", "call"])
            .setLimit(limit)
            .setUID(UID)
            .build();

        messagesRequest.fetchPrevious().then(
            messages => {
                sortingData(messages)
                markAsRead(messages[messages.length - 1])
            },
            error => {
                console.log("Message fetching failed with error:", error);
            }
        );

    }
    const markAsRead = (message) => {
        CometChat.markAsRead(message).then(
            () => {
                console.log("mark as read success.");
            }, error => {
                console.log("An error occurred when marking the message as read.", error);
            }
        );
    }
    const sortingData = (messages) => {

        var newDateArray = [];
        for (var i = 0; i < messages.length; i++) {
            var newDate = moment.unix(messages[i].sentAt).format('MMMM DD, YYYY');
            var obj = { ...messages[i] }
            if (i != 0) {
                var previousDate = moment.unix(messages[i - 1].sentAt).format('MMMM DD, YYYY');
                if (newDate == previousDate) {
                    obj.txtdate = "";
                }
                else
                    obj.txtdate = newDate;

            }
            else
                obj.txtdate = newDate;

            newDateArray.push(obj)
        }
        setMessageData(newDateArray)

    }
    const getHighlightedText = (text, index) => {
        const parts = text.split(' ');
        {
            parts.map(part => part.toLowerCase() === serachTxt.toLowerCase() ?
                callFunction(index) : null)
        }
        return (
            <Text style={styles.txtMessage}>
                {parts.map(part => part.toLowerCase() === serachTxt.toLowerCase()
                    ?
                    <Text style={{
                        backgroundColor: Colors.white, color: Colors.red, fontFamily: Fonts.DMSansRegular
                    }}>{part + " "}</Text>
                    : part + " ")}
            </Text>)
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const BlockUser = async (flag) => {
        setShowing(true)
        var obj = {
            user_id: userData.u_id,
            state: flag == "Unblock" ? "Unblock" : "Block"
        }
        await apiCallWithToken(obj, apiUrl.block_or_unblock_user, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                showMessage(strings.success, strings.msgTag, res.message)
                comeChatBlockUser(flag);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    const comeChatBlockUser = (flag) => {
        var usersList = [receiverID];
        flag == "Block" ? CometChat.blockUsers(usersList).then(
            list => {
                getComeChatUserDetail()
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        ) : CometChat.unblockUsers(usersList).then(
            list => {
                getComeChatUserDetail()
            }, error => {
                console.log("Blocking user fails with error", error);
            }
        );

    }
    const renderSubView = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => setVisible(false)}
                style={styles.mainGroupView}>
                <View>
                    <Image style={styles.imgGroup} source={item.image}></Image>
                    <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                        <Image style={styles.peopleImg} source={Images.people1}></Image>
                    </View>
                </View>
                <View style={styles.radioViewGroup}>
                    <View style={{ marginLeft: 20, flexDirection: "row" }}>
                        <Text style={styles.radioText}>{item.name}</Text>
                        {item.ownwerName &&
                            <Text style={styles.txtInvite}>{" - " + item.ownwerName}</Text>
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10, marginHorizontal: 10 }}>
                <Text style={styles.titleText}>
                    {item.title}
                </Text>
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const tagModal = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                <View style={styles.tagView}>
                    <FlatList
                        data={tagData}
                        renderItem={renderItem}
                        showsVerticalScrollIndicator={false}

                    />
                </View>
            </Modal>
        )
    }
    function getChatTime(timestamp) {

        var t = new Date(timestamp * 1000);
        var hours = t.getHours();
        var minutes = t.getMinutes();
        var newformat = t.getHours() >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format 
        hours = hours % 12;

        // To display "0" as "12" 
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return "" + hours + ":" + minutes + " " + newformat

    }
    const simpleMessage = (item, index) => {
        var newDate = getChatTime(item.sentAt)//moment(new Date(item.sentAt * 1000)).format('hh:MM a');

        return (item.sender.uid == receiverID && item.category == "message" ?
            <View style={[styles.item, styles.itemIn]}>
                <View style={[styles.balloon, { backgroundColor: 'grey' }]}>
                    {/* <Text style={styles.txtMessage}>{item.message}</Text> */}
                    {getHighlightedText(item.text, index)}
                    <Text style={styles.txtTime}>{newDate}</Text>
                    <View
                        style={[
                            styles.arrowContainer,
                            styles.arrowLeftContainer,
                        ]}
                    >
                        <Svg style={styles.arrowLeft} width={moderateScale(15.5, 0.6)} height={moderateScale(17.5, 0.6)} viewBox="32.484 17.5 15.515 17.5" enable-background="new 32.485 17.5 15.515 17.5">
                            <Path
                                d="M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z"
                                fill={"grey"}
                                x="0"
                                y="0"
                            />
                        </Svg>
                    </View>
                </View>
            </View> :
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={styles.viewSend}
            >
                <View style={{ paddingVertical: 5 }}>
                    {getHighlightedText(item.text, index)}
                    <Text style={styles.txtTime}>{newDate}</Text>
                </View>
            </LinearGradient>
            // <View style={styles.callView}>
            //     <View style={styles.imgCall}>
            //         <CallOutgoing />
            //     </View>
            //     <Text style={styles.txtDate}>{"Call connected at " + newDate}</Text>
            // </View>

        )
    }
    const renderView = ({ item, index }) => {

        return (
            <>
                {item.txtDate != "" && <Text style={styles.txtDate}>{item.txtdate}</Text>}
                {item.type == "text" ?
                    simpleMessage(item, index) :
                    item.category == "call" ? bubbleView(item) :
                        item.type == "file" ?
                            imageWithMessage(item, index) :
                            item.type == "Location" ?
                                locationView(item) :
                                item.type == "Event" ?
                                    event(item) :
                                    <></>
                }

            </>

        )
    }
    function get_url_extension(url) {
        return url.split(/[#?]/)[0].split('.').pop().trim() + "";
    }

    const imageWithMessage = (item, index) => {
        //var newDate = moment(new Date(item.sentAt * 1000)).format('hh:MM a');
        var newDate = getChatTime(item.sentAt)
        var isUser = item.sender.uid != comeChatUserTag + u_id
        var fileType = get_url_extension(item.data.url)
        var newDate1 = moment(new Date(item.sentAt * 1000)).format('MMMM DD, YYYY');

        return (
            <View style={{ flex: 1, flexDirection: "row" }}>
                {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}
                {fileType === "jpg" || fileType == "jpeg" || fileType == "png" ?
                    <TouchableOpacity
                        onPress={() => { props.navigation.navigate(strings.webViewScreen, { url: item.data.url, title: !isUser ? "Send By Me" : item.sender.name, date: newDate1 }) }}
                        style={{ alignItems: "flex-start", justifyContent: "flex-start", flex: 1 }}
                    >
                        {/* {isUser && <Text style={styles.txtName}>{item.sender.name}</Text>} */}
                        <View style={{ flexDirection: "row" }}>
                            {/* {isUser && <Image style={styles.imgProfileEvent} source={{ uri: item.data.metadata?.senderImageUri }}></Image>} */}
                            <View style={{ flex: 0.79 }}>
                                <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6, width: responsiveWidth(75) }}>
                                    <Text style={styles.txtMessage}>{item.data.text}</Text>
                                    <Image style={{ height: 171, width: responsiveWidth(69.5), margin: 10 }} source={{ uri: item.data.url }}></Image>
                                    {/* <Text style={styles.txtViewLocation}>{"View Live Location"}</Text> */}
                                    <Text style={styles.txtTime}>{newDate}</Text>

                                </View>
                                {/* {item.likes > 0 ?
                                    <View style={styles.likeView}>
                                        <Image style={styles.imgHerat} source={Images.heartIcon}></Image>
                                        <Text style={styles.txtLike}>{item.likes}</Text>
                                    </ View>
                                    : <></>} */}
                            </View>
                            <View style={{ flex: 0.21 }} />
                        </View>
                    </TouchableOpacity> :
                    isUser == false ?
                        <ChatFileBubbleSender
                            navigation={props.navigation}
                            data={item}
                            groupName={"Send By Me"}
                            onLongPress={() => { }}

                        /> :
                        <ChatFileBubbleReceiver
                            navigation={props.navigation}
                            data={item}
                            onLongPress={() => { }}

                            groupName={userData.u_first_name + " " + userData.u_last_name}

                        />
                }
            </View>
        )
    }
    const event = (item) => {
        var day = new Date(item.data.customData.eventData.event_start_date).getDate();
        var month = new Date(item.data.customData.eventData.event_start_date).getMonth();
        var isUser = item.sender.uid != comeChatUserTag + u_id
        // var newDate = moment(new Date(item.sentAt * 1000)).format('hh:MM a');
        var newDate = getChatTime(item.sentAt)
        return (
            <View style={{ alignItems: "flex-start", justifyContent: "flex-start" }}>
                {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}

                {/* {isUser && <Text style={styles.txtName}>{item.sender.name}</Text>} */}
                <TouchableOpacity style={{ flexDirection: "row" }}
                    onPress={() => props.navigation.navigate(strings.EventDetailPastScreen, { item: item.data.customData.eventData })}
                >
                    {isUser ? <></> :
                        <View style={{ marginLeft: 80 }} />}
                    <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6 }}>
                        <Text style={styles.txtMessage}>{item.data.customData.eventData.event_name}</Text>
                        <View style={styles.renderStyle} >
                            <Image source={{ uri: item.data.customData.eventData.event_image }} style={styles.imgStyle}></Image>
                            <View style={styles.upperView}>
                                <View style={styles.upperSubView}>
                                    <View style={{ flexDirection: "row" }}>
                                        {item.data.customData.eventData.event_members.map((data, parentIndex) => (
                                            parentIndex < 3 ?
                                                parentIndex != 2 && parentIndex < 2 ?
                                                    <Image style={[styles.peopleImg, { marginLeft: -8 }]} source={{ uri: data.member.u_image }}></Image> :
                                                    <View style={styles.moreView}>
                                                        <View style={styles.moreBorderView}></View>
                                                        <Text style={styles.txtMore}>{"+ " + parseInt(item.data.customData.eventData.event_members.length - 2)}</Text>
                                                    </View> : <></>
                                        ))}
                                    </View>
                                    <View style={styles.dateView}>
                                        <Text style={styles.txtDateCal}>{day}</Text>
                                        <Text style={styles.txtMonth}>{monthArray[month]}</Text>
                                    </View>
                                </View>
                            </View>
                            <LinearGradient style={{
                                position: "absolute",
                                height: responsiveHeight(18),
                                width: responsiveWidth(68), bottom: 0,
                            }} colors={Colors.viewCare}>
                            </LinearGradient>
                            <View style={styles.bottomView}>
                                <Text style={styles.txtTitle}>{item.title}</Text>
                                <View style={styles.bottomBorderView}></View>
                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                    <Image style={styles.placeIcon} source={Images.placeIcon}></Image>
                                    <Text style={styles.txtAddress}>{item.data.customData.eventData.event_location}</Text>
                                </View>
                            </View>

                        </View>
                        <Text style={styles.txtTime}>{newDate}</Text>

                    </View>
                    <View style={{ flex: 0.21 }}></View>
                </TouchableOpacity>
            </View>
        )
    }
    const onJoin = (item) => {
        CometChat.acceptCall(item.sessionId).then(
            call => {
                props.navigation.navigate(strings.callScreen, { callData: call })
                // start the call using the startCall() method
            },
            error => {
                console.log(error)
                // else if(CometChat.CALL_ERROR.CA)
                if (error.code == "ERR_CALL_TERMINATED")
                    showMessage(strings.error, "Session Expired", "Call session is expired.")
                else if (error.code == "ERR_CALL_GROUP_ALREADY_JOINED") {
                    showMessage(strings.error, "Session Expired", "Call session is expired.")
                }
                else
                    showMessage(strings.error, "", error.message)
                // handle exception
            }
        );
    }
    const bubbleView = (item) => {
        var isUser = item.sender.uid == comeChatUserTag + u_id
        var loggedInUser = {
            uid: comeChatUserTag + u_id

        }
        return (
            // <CallingBubbleView
            //     text={isUser ? "You have initiated a call" : item.sender.name + " have initiated a call"}
            //     style={!isUser ? styles.leftVieW : styles.rightView}
            //     item={item}
            //     onJoin={() => onJoin(item)} />
            <CometChatActionMessageBubble
                message={item}
                loggedInUser={loggedInUser}
            />
        )
    }
    const _goToNextPage = () => {
        flatList?.scrollToIndex({ animated: true, index: indexArray[initialIndex] });
    };
    const getItemLayout = (data, index) => (
        { length: 50, offset: 50 * index, index }
    )
    const clickItem = (item) => {
        refSheet.close()

        if (item.key == "viewPhotos") {
            props.navigation.navigate(strings.viewPhotosScreen, { userData: props.route.params.userData })
        }
        else if (item.key == "searchinChat") {
            setSerachFlag(true)
        }
        else if (item.key == "callHistory") {

            props.navigation.navigate(strings.historycallsDetailScreen, { userData: props.route.params.userData })
        }
        else if (item.key == "viewProfile") {
            props.navigation.replace(strings.UserProfileScreen, { u_id: props.route.params.userData.u_id })
        }
        else if (item.key == "blockUser") {
            BlockUser(item.title)
        }
        else if (item.key == "delete")
            deleteChat()

    }
    const deleteChat = () => {
        let UID = receiverID;
        let type = "user";
        CometChat.deleteConversation(UID, type).then(
            deletedConversation => {
                getMessage();
            }, error => {
                console.log('error while deleting a conversation', error);
            }
        );
    }
    const onChangeText = (text) => {
        setIndexArray([])
        setSerachText(text)
    }
    const upClick = () => {
        if (initialIndex != 0)
            setInitailIndex(initialIndex - 1)
    }
    const downClick = () => {
        if (initialIndex != indexArray.length - 1)
            setInitailIndex(initialIndex + 1)
    }
    const writeMessage = (text) => {
        // if (text.includes('@'))
        //     setVisible(true)
        // else
        {
            setVisible(false)
            setMessage(text)
        }
    }
    const getComeChatUserDetail = () => {
        let UID = receiverID;
        CometChat.getUser(UID).then(
            user => {
                setComeChatUser(user)
                editMenuData(user)
            }, error => {
                console.log("User details fetching failed with error:", error);
            }
        );
    }
    const editMenuData = (user) => {
        let newArray = [...bottomData]
        if (user?.blockedByMe) {
            newArray[4].title = "Unblock"
        }
        else {
            newArray[4].title = "Block"
        }
        setBottomData(newArray)
    }
    const sendMessage = () => {
       
        if (comeChatUser?.hasBlockedMe) {
            showMessage(strings.error, "Message", comeChatUser.name + " block you so can't send message")
            return;
        }
        if (comeChatUser?.blockedByMe) {
            showMessage(strings.error, "Message", "You blocked " + comeChatUser.name + " so can't send message")
            return;
        }
        if (message.trim().length != 0) {
            try {
                sendNormalMessage(message.trim(), receiverID, "user", userImage).then(res => {
                    setMessage("")
                    getMessage();
                })
            }

            catch (error) {

            }
        }
    }
    const moreMenu = () => {
        return (
            <View style={{ alignItems: "center", marginTop: 10 }}>
                <FlatList
                    data={bottomMenu}
                    numColumns={3}
                    renderItem={renderCategory}
                >
                </FlatList>
            </View>
        )
    }
    const renderCategory = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => bottomCategory(item)} style={{ marginHorizontal: 20, height: 115 }}>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <View style={styles.borderView}></View>
                    <Image source={item.image} style={styles.cameraStyle}></Image>
                </View>
                <Text style={styles.labelText}>{item.title}</Text>
            </TouchableOpacity>
        )
    }
    const bottomCategory = (item) => {
        refSheetBottom.close()
        if (item.key == "Camera") {
            setTimeout(() => {
                launchCamera();
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Photos") {
            setTimeout(() => {
                launchImageLibrary();
            }, Platform.OS == "ios" ? 600 : 200);

        }
        else if (item.key == "Documents") {
            setTimeout(() => {
                pickDocument();
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Audio") {
            setTimeout(() => {
                setIsVisibleRecord(true)
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Location") {
            setTimeout(() => {
                setSerachMap(true)
            }, Platform.OS == "ios" ? 600 : 200);
        }
        else if (item.key == "Event") {
            setTimeout(() => {
                refSheetEvent.open();
            }, Platform.OS == "ios" ? 600 : 200);
        }
    }
    const launchCamera = async () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.5,
            maxWidth: width,
            maxHeight: height,
        };
        if (Platform.OS == "android") {
            let isCameraPermitted = await requestCameraPermission();
            let isStoragePermitted = await requestExternalWritePermission();
            if (isCameraPermitted && isStoragePermitted) {
            }
            else {
                return 0;
            }
        }
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                var file = {
                    name: response.assets[0].fileName,
                    type: response.assets[0].type,
                    uri: response.assets[0].uri
                }
                mediaMessage(receiverID, "user", file, "", userImage).then(res => {
                    getMessage();
                })
            }
        });
    }
    const launchImageLibrary = async () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            quality: 0.8,
            maxWidth: width,
            maxHeight: height,
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');

            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                var file = {
                    name: response.assets[0].fileName,
                    type: response.assets[0].type,
                    uri: response.assets[0].uri
                }
                mediaMessage(receiverID, "user", file, "", userImage).then(res => {
                    getMessage()
                })
            }
        });
    }
    const pickDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.plainText, DocumentPicker.types.zip],
            })

            var file = {
                name: res.name,
                type: res.type,
                uri: res.uri
            }
            let array = file.type.split("/")

            mediaMessage(receiverID, "user", file, "", userImage).then(res => {
                if (array[0] == "audio") {
                }
                else if (array[0] == "video") {
                }
                else {
                }
                getMessage()
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err
            }
        }
    }
    const closeRecord = () => {
        setIsVisibleRecord(false)
    }
    const stopRecord = (value) => {
        setIsVisibleRecord(false)
        var file = {
            name: value.filename,
            type: "audio/mpeg",
            uri: "file://" + value.path
        }
        mediaMessage(receiverID, "user", file, "", userImage).then(res => {
            getMessage();
        })
    }
    const closeModal = () => {
        setSerachMap(false)
    }
    const saveLocation = (data) => {

        setSerachMap(false)
        if (data == null || data == undefined) {
            showMessage(strings.error, strings.msgTag, "Location Data is required!")
        }
        else {
            var obj = {
                lat: data.geometry.location.lat,
                lng: data.geometry.location.lng,
                formatted_address: data.formatted_address
            }
            sendUserEventMessage(receiverID, userImage, obj, "Location").then(res => {
                getMessage()

            })
        }

    }
    const locationView = (item) => {
        var newDate = getChatTime(item.sentAt)
        var isUser = item.sender.uid != comeChatUserTag + u_id
        const obj = {
            name: item.sender.name,
            time: newDate,
            fullDate: item.sentAt,
            imageUri: item.metadata.senderImageUri,
            latitude: item.data.customData.locationData.lat,
            longitude: item.data.customData.locationData.lng,
            isShare: false
        }
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate("viewLocation", { mapData: obj })}//Linking.openURL(url)}
                style={{ alignItems: "flex-start", justifyContent: "flex-start" }}>
                {!isUser && <View style={{ flex: 0, marginRight: 80 }}></View>}
                <View
                    style={{ flexDirection: "row" }}>
                    {isUser ? <></> :
                        <View style={{ marginLeft: 80 }} />}
                    <View style={{ backgroundColor: Colors.opacityColor, borderRadius: 6 }}>
                        <Text style={[styles.txtMessage, { width: responsiveWidth(71.5) }]}>{item.data.customData.locationData.formatted_address}</Text>
                        <EventMap lat={item.data.customData.locationData.lat} long={item.data.customData.locationData.lng} style={styles.imgMap} />
                        <Text style={[styles.txtTime]}>{newDate}</Text>
                    </View>
                </View>
            </TouchableOpacity>

        )
    }
    const onProceed = () => {
        refSheetEvent.close()
        setTimeout(() => {
            sendUserEventMessage(receiverID, userImage, eventSelectItem, "Event").then(res => {
                getMessage()

            })
        }, 400);
    }
    const EventList = () => {
        return (
            <View style={styles.eventListContainer}>
                <FlatList
                    data={eventData}
                    renderItem={renderEventItem}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={renderListHeader}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={renderFooter}
                />
            </View>
        )
    }
    const selectedEvent = (item) => {
        setEventItem(item)
        setSelectedEventRadio(item.event_id)
    }
    const renderEventItem = ({ item, index }) => {
        return (
            <View>
                <View style={styles.createEventContainer}>
                    <View style={styles.createEventImage}>
                        <Image source={{ uri: item.event_image }} style={styles.eventImage} />
                    </View>
                    <View style={[styles.smallIcon2, {
                        backgroundColor: Colors.roundBackground, borderWidth: 1,
                        borderColor: Colors.theme
                    }]}>
                        <Image source={Images.addEventIcon} style={styles.smallEventIcon} resizeMode={'contain'} />
                    </View>
                    <View style={styles.eventItem}>
                        <View>
                            <Text style={styles.eventTitle}>{item.event_name}</Text>
                            <Text style={styles.eventTime}>{item.event_start_date + " To " + item.event_end_date} </Text>
                        </View>
                        <TouchableOpacity style={[styles.radioBtn, { borderColor: selectedEventRadio == item.event_id ? Colors.primaryViolet : Colors.white }]} onPress={() => selectedEvent(item)}>
                            <View style={selectedEventRadio == item.event_id ? styles.radioBtnSelect : null}></View>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        )
    }
    const addEventClick = () => {
        refSheetEvent.close();
        props.navigation.navigate(strings.addEventScreen)
    }
    const onChangeTextEvent = (text) => {
        //  setSerachTextEvent(text)
    }
    const onEndEditing = (text) => {
        if (text.trim().length == 0) {
            setEventData(eventDataBackUp)
        }
        else {
            var result = eventData.filter(obj => {
                return (obj.event_name).toLowerCase().includes(text.toLowerCase())
            })
            setEventData(result)


        }
    }
    const renderListHeader = () => {
        return (
            <View >
                <TouchableOpacity
                    style={styles.drawer}
                    onPress={() => refSheetEvent.close()}>

                </TouchableOpacity>
                <View style={styles.textInputSearch} >
                    <Image source={Images.serachIcon} style={styles.serachIcon} resizeMode={'contain'} />
                    <TextInput
                        placeholder={'Search for events'}
                        placeholderTextColor={Colors.white}
                        //multiline={true}
                        //value={serachTxtEvent}
                        onChangeText={(text) => onChangeTextEvent(text)}
                        onSubmitEditing={({ nativeEvent: { text, eventCount, target } }) => onEndEditing(text)}
                        style={styles.searchBox}
                    />
                </View>
                <View>
                    <TouchableOpacity style={styles.createEventContainer}
                        onPress={() => addEventClick()}
                    >
                        <View style={styles.createEventImage}>
                            <EventIconUnselect height={18} width={18} opacity={1} />
                        </View>
                        <View style={[styles.smallIcon, {
                            backgroundColor: Colors.primaryViolet,
                            borderWidth: 1, borderColor: Colors.theme, alignItems: "center", justifyContent: "center"
                        }]}>
                            <Text style={{ alignSelf: "center" }}>+</Text>
                        </View>
                        <Text style={styles.createEvent}>Create a new event </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    const renderFooter = () => {
        return (
            <CommonButton
                btnlinearGradient={styles.btnlinearGradient}
                colors={Colors.btnBackgroundColor}
                btnText={styles.btnText}
                viewStyle={styles.verifyBtnGroup}
                onPress={() => onProceed()}
                text={strings.proceed}
            />
        )
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisibleCall(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisibleCall(false)

                // handle exception
            }
        );
    }
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" && "padding"}
            style={styles.container}
        >

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={{ flex: 1 }}>
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <LinearGradient
                    colors={serachFlag ? [Colors.transparent, Colors.transparent] : Colors.btnBackgroundColor}
                    style={{ height: Platform.OS == "ios" ? responsiveHeight(12.4) : responsiveHeight(10) }}
                >
                    {serachFlag ?
                        <View style={styles.headerView}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity onPress={() => props.navigation.goBack(null)} >
                                    <Image style={styles.backImage} source={Images.backArrow}></Image>
                                </TouchableOpacity>
                                <View style={styles.borderStyle}>
                                    <TextInput
                                        style={styles.txtSerach}
                                        onChangeText={(text) => onChangeText(text)}
                                    >
                                    </TextInput>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", marginRight: responsiveWidth(4) }}>
                                <TouchableOpacity onPress={() => upClick()}>
                                    <Image style={styles.imgUp} source={Images.upChevron}></Image>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => downClick()}>
                                    <Image style={styles.imgDown} source={Images.downChevron}></Image>
                                </TouchableOpacity>
                            </View>


                        </View> :
                        <MessageHeader
                            navigation={props.navigation}
                            title={userData?.u_first_name + " " + userData?.u_last_name}
                            backButton={Images.backIcon}
                            callClick={callClick}
                            menuClick={menuClick}
                            uri={userData?.u_image}
                        />
                    }

                </LinearGradient>
                <LinearGradient
                    colors={serachFlag ? [Colors.transparent, Colors.transparent] : Colors.screenBackgroundColor}
                    style={styles.linearGradient}
                >
                    <View style={{ marginVertical: responsiveHeight(2), flex: 1 }}>
                        <FlatList
                            ref={(ref) => { flatList = ref; }}
                            data={messageData}
                            //getItemLayout={getItemLayout}
                            style={{ marginBottom: responsiveHeight(6) }}
                            renderItem={renderView}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={item => item}
                            onContentSizeChange={() => flatList.scrollToEnd({ animated: true })}
                            onLayout={() => flatList.scrollToEnd({ animated: true })}
                        //initialScrollIndex={messageData.length - 1}

                        >
                        </FlatList>


                    </View>
                    {/* {tagModal()} */}
                    <View style={[styles.bottomViewPlus, { marginBottom: Platform.OS == "ios" ? 10 : 0 }]}>
                        <TouchableOpacity style={{ marginHorizontal: 8 }} onPress={() => refSheetBottom.open()}>
                            <PlusIcon />
                        </TouchableOpacity>
                        <TextInput
                            placeholder={"Write Comment"}
                            placeholderTextColor={Colors.opacityColor}
                            style={styles.txtInputStyle}
                            value={message}
                            onChangeText={(text) => writeMessage(text)}
                        >
                        </TextInput>
                        <TouchableOpacity onPress={() => sendMessage()} style={styles.txtPostView} >
                            <Text style={styles.txtPost}>{"Post"}</Text>
                        </TouchableOpacity>
                    </View>
                    <RBSheet
                        ref={(ref) => refSheet = ref}
                        height={292}
                        openDuration={250}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {

                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                paddingHorizontal: 25,
                                paddingVertical: 10
                            }
                        }}
                    >
                        <MoreMenu
                            data={bottomData}
                            clickItem={clickItem}
                        >

                        </MoreMenu>
                    </RBSheet>

                    <RBSheet
                        ref={(ref) => refSheetBottom = ref}
                        height={296}
                        openDuration={250}
                        closeOnDragDown={true}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                padding: 10,
                            }
                        }}
                    >
                        {moreMenu()}

                    </RBSheet>
                    <RBSheet
                        ref={ref => {
                            refSheetEvent = ref;
                        }}

                        openDuration={250}
                        closeOnDragDown={false}
                        customStyles={{
                            container: {
                                justifyContent: "center",
                                alignItems: "center",
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                                backgroundColor: Colors.dialogBackground,
                                padding: 10
                            }
                        }}
                    >
                        {EventList()}
                    </RBSheet>
                    <AudioRecord isVisible={isVisibleRecord} stopRecord={stopRecord}
                        closeRecord={closeRecord}
                    />
                    <LocationPicker
                        isVisible={isSerachMap}
                        saveLocation={saveLocation}
                        closeModal={closeModal}
                    />
                </LinearGradient>
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
                {Platform.OS == "ios" && <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisibleCall} callAccept={callAccept} />}
            </LinearGradient>
        </KeyboardAvoidingView>
    )
}
export default TypeMessage;