import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity, Dimensions, PermissionsAndroid } from 'react-native';
import { Colors } from '../../../../../Resources/Colors';
import { styles } from './CallScrennStyle';
import { Images } from '../../../../../Resources/Images';
import { CallDropIcon } from '../../../../../utils/svg/CallDropIcon';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { apiCallWithToken, getUserData } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
const { width, height } = Dimensions.get("window")
import { comeChatUserTag } from '../../../../../utils/Constant';
import moment from 'moment';

const CallScreen = (props) => {
    const [miceFlag, setMiceFlag] = useState(false)
    const [speakerFlag, setspeakerFlag] = useState(false)
    const [callData] = useState(props.route.params.callData)
    const [callFlag,setCallFlag] = useState(props.route.params.flag)
    const [user_id, setUserId] = useState("")
    const [token, setToken] = useState("")
    let startTime = null;
    let status = "2"
   const getPermissions = async () => {
        if (Platform.OS === 'android') {
          let granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          ]);
          if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            granted = await PermissionsAndroid.requestMultiple([
              PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
            ]);
          }
        }
      };
    
    useEffect(() => {

        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            setCallFlag(props.route.params?.callData?.receiverType)

            getPermissions();
        })

    }, []);

    var sessionId = callData.sessionId;
    var callType = CometChat.CALL_TYPE.AUDIO;
    let callListener = new CometChat.OngoingCallListener({
        onUserJoined: user => {
            if (startTime == null) {
                status = "1"
                startTime = new Date()
            }

            console.log("userJoined", user)

        },
        onUserLeft: user => {

        },
        onUserListUpdated: userList => {
            // console.log("user list:", userList);
        },
        onAudioModesUpdated: (audioModes) => {
            //  console.log("audio modes:", audioModes);
        },
        onCallEnded: call => {

            getUserData().then(res => {
                if (callData.callInitiator.uid == (comeChatUserTag + res.u_id))
                    callFlag == "user" ? storeCallData(call, res.token) : props.navigation.goBack(null)
                else {
                    props.navigation.goBack(null)
                }
            })

        },
        onError: error => {

            console.log('Call Error: ', error);
        },
    });
    var callSettings = new CometChat.CallSettingsBuilder()
        .setSessionID(callData.sessionId)
        .enableDefaultLayout(false)
        .setDefaultAudioMode(CometChat.AUDIO_MODE.SPEAKER)
        .setIsAudioOnlyCall(true)
        .setCallEventListener(callListener)
        

        .build();
    // var callSettings = new CometChat.CallSettingsBuilder()
    //     .setSessionID(sessionId)
    //     .setCallEventListener(callListener)
    //     .build();
    const onMiceClick = () => {
        let callController = CometChat.CallController.getInstance();

        callController.muteAudio(!miceFlag);
        setMiceFlag(!miceFlag)

    }
    const callClose = () => {
        CometChat.endCall(callData.sessionId).then(
            call => {
                // var id = call.callInitiator.uid;
                // getUserData().then(res => {
                //     if (id == (comeChatUserTag + res.u_id))
                //         storeCallData(call, res.token);
                //     else {
                //         props.navigation.goBack(null)
                //     }
                // })

            }, error => {
                console.log('error123', error);
            }
        );
    }

    const storeCallData = async (call, token) => {

        var newDate = moment.utc(new Date()).format('Y-MM-DD hh:mm:s');

        var totalSeconds = 0;
        if (startTime != null) {
            var msdiff = new Date().getTime() - startTime?.getTime()
            totalSeconds = Math.floor(msdiff / 1000);
        }
        var obj = {
            call_from_user_id: (callData.callInitiator.uid).replace(comeChatUserTag, ""),
            call_to_user_id: (callData.callReceiver.uid).replace(comeChatUserTag, ""),
            call_duration: totalSeconds,
            call_datetime: newDate,
            call_status: status
        }
        await apiCallWithToken(obj, apiUrl.store_call_history, token).then(res => {
            if (res.status == true) {
                props.navigation.goBack(null)
            }

        })
    }
    const onSpeakerClick = () => {
        let callController = CometChat.CallController.getInstance();
        let earPieceMode = speakerFlag ? CometChat.AUDIO_MODE.EARPIECE : CometChat.AUDIO_MODE.HEADPHONES;
        callController.setAudioMode(earPieceMode);
        setspeakerFlag(!speakerFlag)
    }
    return (
        <View style={{ height: height, width: width }}>
            {callSettings &&
                <CometChat.CallingComponent

                    callsettings={callSettings}
                />
            }
            <View style={styles.bottomView}>
                <TouchableOpacity onPress={() => onMiceClick()}
                    style={miceFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                    <Image style={miceFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} source={Images.mute}></Image>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => callClose()} style={styles.callDiscView}>
                    <CallDropIcon />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => onSpeakerClick()}
                    style={speakerFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                    <Image style={speakerFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} source={Images.volume}></Image>
                </TouchableOpacity>
            </View>
        </View>
    );

}
export default CallScreen;


