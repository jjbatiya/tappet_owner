import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    //header view
    headerView: { marginHorizontal: responsiveWidth(3), marginVertical: responsiveHeight(1), flexDirection: "row", alignItems: "center", justifyContent: "space-between" },
    backImage: { width: 15, height: 15, tintColor: Colors.white },
    addUser: { tintColor: Colors.white, height: 15, width: 15 },
    //profileView
    mainView: { alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(4) },
    imgProfile: { height: 195, width: 195, borderRadius: 100 },
    txtName: {
        fontSize: 20, fontWeight: "500", lineHeight: 26.04,
        color: Colors.white, marginTop: responsiveHeight(1.5), fontFamily: Fonts.DMSansRegular
    },
    txtTime: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, marginTop: responsiveHeight(0.3), fontFamily: Fonts.DMSansRegular
    },

    //bottom view
    bottomView: {
        position: "absolute", bottom: 0,
        flexDirection: "row", alignItems: "center",
        justifyContent: "space-between",
        marginBottom: responsiveHeight(1),
        marginHorizontal: responsiveWidth(10),
        width: responsiveWidth(80)
    },
    miceView: {
        height: 48,
        width: 48,
        backgroundColor: Colors.opacityColor2,
        borderRadius: 24,
        alignItems: "center", justifyContent: "center"
    },
    imgMice: { height: 15, width: 15, tintColor: Colors.white },
    callDiscView: {
        height: 68,
        width: 68,
        backgroundColor: Colors.red,
        borderRadius: 34,
        alignItems: "center", justifyContent: "center",
    },
    callAccept: {
        height: 68,
        width: 68,
        backgroundColor: Colors.green,
        borderRadius: 34,
        alignItems: "center", justifyContent: "center",
    },
    imgCall: { height: 20, width: 20, tintColor: Colors.white }


})