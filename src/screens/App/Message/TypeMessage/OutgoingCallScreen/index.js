
import React, {  } from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView, Platform, Dimensions, Alert, BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { styles } from '../CallScreen/CallScrennStyle';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { CallDropIcon } from '../../../../../utils/svg/CallDropIcon';
import { CometChat } from "@cometchat-pro/react-native-chat"
const { width, height } = Dimensions.get("window")
import Sound from 'react-native-sound';
import { outgoingCallAlert } from '../../../../../assets/audio';
import { strings } from '../../../../../Resources/Strings';

class OutgoingCallScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            callSettings: null,
            errorScreen: false,
            errorMessage: null,
            callInProgress: null,
            outgoingCallScreen: false,
            miceFlag: false,
            speakerFlag: false,
            textCalling: "",
            callFlag: false
        };

        this.callScreenManager = null;

        this.outgoingAlert = new Sound(outgoingCallAlert);


    }

    componentDidMount() {
     
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );
        this.setState({
            textCalling: "Calling..."
        })
        setTimeout(() => {
            try {
                this.playOutgoingAlert();

            } catch (error) {
                console.log(error)
            }
        }, 1000);
        var listnerID = this.props.route.params.callData.sessionId;
        CometChat.addCallListener(
            listnerID,
            new CometChat.CallListener({
                onIncomingCallReceived: (call) => {
                    console.log("Incoming call:", call);
                    // Handle incoming call
                },
                onOutgoingCallAccepted: (call) => {
                    this.outgoingAlert.stop(() => {
                        setTimeout(() => {
                            this.props.navigation.pop()
                            this.props.navigation.navigate(strings.callScreen, { callData: this.props.route.params.callData, flag: this.props.route.params.flag })

                        }, 2000);
                    });
                    // Outgoing Call Accepted
                },
                onOutgoingCallRejected: (call) => {
                    console.log("Outgoing call rejected:", call);
                    // Outgoing Call Rejected
                    
                   // this.goBack()
                   this.outgoingAlert.stop();
                   this.props.navigation.goBack(null);
                   
                },
                onIncomingCallCancelled: (call) => {
                    console.log("Incoming call calcelled:", call);
                }
            })
        );
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }
    backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to go back?", [
            {
                text: "Cancel",
                onPress: () => null,
                style: "cancel"
            },
            { text: "YES", onPress: () => this.goBack() }
        ]);
        return true;
    };
    playOutgoingAlert = () => {
        try {
            this.outgoingAlert.setCurrentTime(0);
            this.outgoingAlert.setNumberOfLoops(-1);
            this.outgoingAlert.play((success) => {
                if (success) {
                    console.log('successfully finished playing');
                } else {
                    console.log('playback failed due to audio decoding errors');
                }
            })

        } catch (error) {
            console.log(error)

        }
    };
    goBack() {
        var status = CometChat.CALL_STATUS.CANCELLED;

        CometChat.rejectCall(this.props.route.params.callData.sessionId,status).then(
            call => {
                this.outgoingAlert.stop();
                this.props.navigation.goBack(null);
            }, error => {
                console.log('error123', error);
               
            }
        );
      
    }
    render() {
        return (

            <View style={styles.container}>

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.linearGradient}
                >
                    <SafeAreaView style={{ flex: 1, marginTop: Platform.OS == "android" ? responsiveHeight(6) : 0 }}>
                        {/* <View style={styles.headerView}>
                            <TouchableOpacity onPress={() => this.goBack()} >
                                <Image style={styles.backImage} source={Images.backArrow}></Image>
                            </TouchableOpacity>
                            <Text></Text>
                            <AddUser />
                        </View> */}
                        <View style={styles.mainView}>
                            <Image style={styles.imgProfile} source={{ uri: this.props.route.params.userData.u_image }}></Image>
                            <Text style={styles.txtName}>{this.props.route.params.userData.u_first_name + " " + this.props.route.params.userData.u_last_name}</Text>
                            <Text style={styles.txtTime}>{this.state.textCalling}</Text>
                        </View>
                    </SafeAreaView>
                    <View style={[styles.bottomView, { justifyContent: "center" }]}>
                        {/* <TouchableOpacity onPress={() => this.setState({
                            miceFlag: !this.state.miceFlag
                        })}
                            style={this.state.miceFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                            <Image style={this.state.miceFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} //source={Images.mute}
                            ></Image>
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.goBack()} style={styles.callDiscView}>
                            <CallDropIcon />
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.setState({
                            speakerFlag: !this.state.speakerFlag
                        })}
                            style={this.state.speakerFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                            <Image style={this.state.speakerFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} source={Images.volume}></Image>
                        </TouchableOpacity> */}
                    </View>
                </LinearGradient>
            </View>

        )
    }
}
export default OutgoingCallScreen;