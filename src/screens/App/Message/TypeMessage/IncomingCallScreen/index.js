
import React, { } from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView, Platform, Dimensions, Alert, BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { styles } from '../CallScreen/CallScrennStyle';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { CallDropIcon } from '../../../../../utils/svg/CallDropIcon';
import { CometChat } from "@cometchat-pro/react-native-chat"
const { width, height } = Dimensions.get("window")
import Sound from 'react-native-sound';
import { outgoingCallAlert } from '../../../../../assets/audio';
import { strings } from '../../../../../Resources/Strings';
import CallCancel from '../../../../../utils/svg/CallCancel';
var outgoingAlert = new Sound(outgoingCallAlert);

class IncomingCallScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            callSettings: null,
            errorScreen: false,
            errorMessage: null,
            callInProgress: null,
            outgoingCallScreen: false,
            miceFlag: false,
            speakerFlag: false,
            textCalling: "",
            callFlag: false
        };

        this.callScreenManager = null;



    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );
        this.setState({
            textCalling: "Incoming call..."
        })
        setTimeout(() => {
            try {
                this.playOutgoingAlert();

            } catch (error) {
                console.log(error)
            }
        }, 1000);
        var listnerID = this.props.route.params.callData.sessionId;
        CometChat.addCallListener(
            listnerID,
            new CometChat.CallListener({
                onIncomingCallReceived: (call) => {

                },
                onOutgoingCallAccepted: (call) => {

                    // Outgoing Call Accepted
                },
                onOutgoingCallRejected: (call) => {

                    try {
                        setTimeout(() => {
                            outgoingAlert.stop(() => {
                                setTimeout(() => {
                                    this.props.navigation.goBack();
                                }, 200);
                            })
                        }, 1000);
                      
                    } catch (error) {
                    }

                },
                onIncomingCallCancelled: (call) => {
                    try {
                        setTimeout(() => {
                            outgoingAlert.stop(() => {
                                setTimeout(() => {
                                    this.props.navigation.goBack();
                                }, 200);
                            })
                        }, 1000);
                      
                    } catch (error) {
                    }

                },
                
                        })
        );

    }
    componentWillUnmount() {
        this.backHandler.remove();
    }
    backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to go back?", [
            {
                text: "Cancel",
                onPress: () => null,
                style: "cancel"
            },
            { text: "YES", onPress: () => this.goBack() }
        ]);
        return true;
    };
    playOutgoingAlert = () => {
        try {
            outgoingAlert.setCurrentTime(0);
            outgoingAlert.setNumberOfLoops(-1);
            outgoingAlert.play((success) => {
                if (success) {
                    console.log('successfully finished playing');
                } else {
                    console.log('playback failed due to audio decoding errors');
                }
            })

        } catch (error) {
            console.log(error)

        }
    };
    callAccept() {
        CometChat.acceptCall(this.props.route.params.callData.sessionId).then(
            call => {
                // start the call using the startCall() method
                outgoingAlert.stop(() => {
                    setTimeout(() => {
                        this.props.navigation.pop()
                        this.props.navigation.navigate(strings.callScreen, { callData: call })

                    }, 1000);
                });

            },
            error => {
                outgoingAlert.stop();
                this.props.navigation.goBack(null);
                // handle exception
            }
        );
    }
    goBack() {
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(this.props.route.params.callData.sessionId, status).then(
            call => {
                outgoingAlert.stop();
                this.props.navigation.goBack(null);
            }, error => {
                outgoingAlert.stop();
                this.props.navigation.goBack(null);
                console.log('error123', error);

            }
        );

    }
    render() {
        return (

            <View style={styles.container}>

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.linearGradient}
                >
                    <SafeAreaView style={{ flex: 1, marginTop: Platform.OS == "android" ? responsiveHeight(6) : 0 }}>
                        {/* <View style={styles.headerView}>
                            <TouchableOpacity onPress={() => this.goBack()} >
                                <Image style={styles.backImage} source={Images.backArrow}></Image>
                            </TouchableOpacity>
                            <Text></Text>
                            <AddUser />
                        </View> */}
                        <View style={styles.mainView}>
                            <Image style={styles.imgProfile} source={{ uri: this.props.route.params.callData?.callInitiator?.avatar }}></Image>
                            <Text style={styles.txtName}>{this.props.route.params.callData?.callInitiator?.name}</Text>
                            <Text style={styles.txtTime}>{this.state.textCalling}</Text>
                        </View>
                    </SafeAreaView>
                    <View style={[styles.bottomView, { justifyContent: "space-between" }]}>
                        {/* <TouchableOpacity onPress={() => this.setState({
                            miceFlag: !this.state.miceFlag
                        })}
                            style={this.state.miceFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                            <Image style={this.state.miceFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} //source={Images.mute}
                            ></Image>
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.callAccept()} style={styles.callAccept}>
                            <CallCancel />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.goBack()} style={styles.callDiscView}>
                            <CallDropIcon />
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.setState({
                            speakerFlag: !this.state.speakerFlag
                        })}
                            style={this.state.speakerFlag ? [styles.miceView, { backgroundColor: Colors.white }] : styles.miceView}>
                            <Image style={this.state.speakerFlag ? [styles.imgMice, { tintColor: Colors.black }] : styles.imgMice} source={Images.volume}></Image>
                        </TouchableOpacity> */}
                    </View>
                </LinearGradient>
            </View>

        )
    }
}
export default IncomingCallScreen;