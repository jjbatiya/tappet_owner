import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, TextInput, SafeAreaView, Platform, StyleSheet, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Images } from '../../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../../../../../Resources/Colors';
import { getUserData } from '../../../../../utils/helper';
import { comeChatUserTag } from '../../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import moment, { now } from 'moment';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
import { strings } from '../../../../../Resources/Strings';


const ViewPhoto = (props) => {
    const [token, setToken] = useState("")
    const [u_id, setUserId] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [imageList, setImageList] = useState(false)
    const [receiverID] = useState(comeChatUserTag + props.route.params.userData.u_id)

    useEffect(() => {
        getUserData().then(res => {
            setToken(res.token)
            setUserId(res.u_id)
            getMessage();

        })
    }, []);
    function get_url_extension(url) {
        return url.split(/[#?]/)[0].split('.').pop().trim() + "";
    }
    function filterData(data, i) {
        var type = get_url_extension(data.data.url);
        return type == "jpg" || type == "jpeg" || type == "png";
    }
    const getMessage = () => {
        var UID = receiverID;
        var limit = 100;
        let messagesRequest = new CometChat.MessagesRequestBuilder()
            .setCategories(["message"])
            .setType("file")
            .setLimit(limit)
            .setUID(UID)
            .build();

        messagesRequest.fetchPrevious().then(
            messages => {
                setImageList(messages.filter((item) => filterData(item)))
            },
            error => {
                console.log("Message fetching failed with error:", error);
            }
        );

    }
    const renderView = ({ item, index }) => {

        var isUser = item.sender.uid != comeChatUserTag + u_id
        var newDate1 = moment(new Date(item.sentAt * 1000)).format('MMMM d, YYYY');

        return (
            <TouchableOpacity
                onPress={() => { props.navigation.navigate(strings.webViewScreen, { url: item.data.url, title: !isUser ? "Send By Me" : item.sender.name, date: newDate1 }) }}

                style={{ flexDirection: "row", width: "33%", marginHorizontal: 1, marginVertical: 1 }}>
                <Image style={{ height: 122, width: "100%" }} source={{ uri: item.data.url }}></Image>
            </TouchableOpacity>
        )
    }
    return (
        <View style={styles.container}>

            {Platform.OS == "android" ?
                <CommonStatusBar color={Colors.backgroundColor} /> : <></>}
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <SafeAreaView style={{ flex: 1, marginTop: Platform.OS == "android" ? responsiveHeight(6) : 0 }}>
                    <View style={styles.headerView}>
                        <TouchableOpacity onPress={() => props.navigation.goBack(null)} >
                            <Image style={styles.backImage} source={Images.backArrow}></Image>
                        </TouchableOpacity>
                        <Text style={styles.txtTitle}>{"Photos"}</Text>
                        <Text></Text>
                    </View>
                    <FlatList
                        data={imageList}
                        numColumns={3}
                        style={{ marginVertical: responsiveHeight(2) }}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderView}
                    >

                    </FlatList>
                </SafeAreaView>
            </LinearGradient>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },

    //header view
    headerView: {
        marginHorizontal: responsiveWidth(3), marginVertical: responsiveHeight(1), flexDirection: "row",
        alignItems: "center", justifyContent: "space-between",
    },
    backImage: { width: 15, height: 15, tintColor: Colors.white },
    txtTitle: { fontSize: 16, fontWeight: "700", lineHeight: 22, color: Colors.white, textAlign: "center" },

})
export default ViewPhoto;