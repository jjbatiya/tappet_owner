import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, Image, TouchableOpacity, FlatList, Share, ScrollView, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Constant from '../../../../utils/Constant';
import HeaderView from '../../../../Components/HeaderView';
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images';

import SimpleSerach from '../../../../Components/SimpleSerach';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { apiCallWithToken, getUserData } from '../../../../utils/helper';
import Loader from '../../../../Components/Loader';

import Toast from 'react-native-simple-toast';
import { Fonts } from '../../../../Resources/Fonts';

const UserChatList = (props) => {
    var onEndReached;
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [friendsData, setFriendData] = useState([])
    const [limit, setLimit] = useState(10)
    const [serachText, setSerachText] = useState("")

    useEffect(async () => {
        getUserData().then(res => {
            setToken(res.token)
            setShowing(true)

            getFriendsData(res.token)
        })

    }, []);


    const getFriendsData = async (token) => {
        var obj = {
            page: page,
            limit: limit,
            friend_request: 1,
            search: serachText
        }
        await apiCallWithToken(obj, apiUrl.my_friends, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1) {
                    if (res.result.length != 0)
                        setFriendData(res.result)
                    else
                        Toast.show(res.message, Toast.LONG);
                }
                else {
                    setFriendData(friendsData.concat(res.result))
                }
            }
            else {
                Toast.show(res.message, Toast.LONG);
                setTimeout(() => {
                    setShowing(false)
                    setFriendData([])
                }, 1000);
            }
        })
    }


    const backClick = () => {
        props.navigation.goBack(null);
    }
    const renderSubView = ({ item, index }) => {
        return (
            <TouchableOpacity
            onPress={()=>props.navigation.replace(strings.typeMessageScreen, { userData: item })}
             style={styles.renderSubView}>
                <View style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pet"}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
    const onSubmitEditing = async () => {
        setShowing(true)
        getFriendsData(token)
    }

    const step2View = () => {
        return (
            <View style={{ marginHorizontal: responsiveWidth(4) }}>
                <SimpleSerach
                    onChangeText={(text) => setSerachText(text)}
                    placeHolder={strings.searchforFriends}
                    isSimpleSerach={true}
                    editable={true}
                    isScreen={true}
                    onPlaceClick={() => console.log("")}
                    onSubmitEditing={() => onSubmitEditing()}

                />
                <FlatList
                    data={friendsData}
                    style={{ marginBottom: responsiveHeight(10) }}
                    renderItem={renderSubView}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.1}
                    onMomentumScrollBegin={() => { onEndReached = false; }}
                    onEndReached={() => {
                        if (!onEndReached) {
                            handleMoreData(); // on End reached
                            onEndReached = true;
                        }
                    }}

                />
            </View>
        )
    }

    const handleMoreData = () => {

        if (totalPage > page) {
            setPage(page + 1)
            setShowing(true)
            getMemberData(token, "")
        }

    }
    return (

        <View style={{ flex: 1 }} >
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >

                <CommonStatusBar />
                <SafeAreaView style={{ flex: 1, marginTop: responsiveHeight(5) }}>
                    <HeaderView
                        onPress={backClick}
                        text={"Select Contact"}
                    />

                    {step2View()}
                </SafeAreaView>
            </LinearGradient>
        </View>


    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    titleText: {
        fontSize: 14, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular, textTransform: "uppercase"
    },
    renderSubView: { height: 48, marginVertical: 15, flexDirection: "row", alignItems: "center", justifyContent: 'space-between' },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 48, width: 48, borderRadius: 24 },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        width: 150
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular,
        width: 150
    },
})
export default UserChatList;