import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './AllMessageStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import SimpleSerach from '../../../../Components/SimpleSerach';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { getUserData, imageUploadApi, storeChatNotificationFlag } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import moment, { now } from 'moment';
import { toastConfig } from '../../../../utils/ToastConfig';


const AllMessage = (props) => {
    const [userConversion, setUserConversion] = useState([])
    const [groupConversion, setGroupConversion] = useState([])
    const [isshowing, setShowing] = useState(true)
    const [userList, setUserList] = useState([])
    const [groupList, setGroupList] = useState([])
    useEffect(async () => {
        storeChatNotificationFlag({ chat_notification_flag: false })
        await getRetrivedData("user");
        props.navigation.addListener('focus', () => {
            setShowing(true)
            getRetrivedData("user");
        })


    }, [])
    const getRetrivedData = (type) => {
        let limit = 30;
        let conversationRequest = new CometChat.ConversationsRequestBuilder()
            .setLimit(limit)
            .setConversationType(type)
            .build();

        conversationRequest.fetchNext().then(
            conversationList => {
                setUserConversion(conversationList)
                getRetrivedDataGroup(conversationList)

            }, error => {
                console.log("Conversations list fetching failed with error:", error);
            }
        );
    }

    const getRetrivedDataGroup = (userConversion) => {
        let limit = 30;
        let conversationRequest = new CometChat.ConversationsRequestBuilder()
            .setLimit(limit)
            .setConversationType("group")
            .build();
        conversationRequest.fetchNext().then(
            conversationList => {
                setGroupConversion(conversationList)
                getUserAndGroupData(userConversion, conversationList);

            }, error => {
                console.log("Conversations list fetching failed with error:", error);
            }
        );

    }


    const getUserAndGroupData = (userData, groupData) => {

        let userIds = userData.map(a => a.conversationWith.uid);
        for (var i = 0; i < userIds.length; i++) {
            userIds[i] = userIds[i].replace(comeChatUserTag, "")
        }
        let groupIds = groupData.map(a => a.conversationWith.guid);
        for (var i = 0; i < groupIds.length; i++) {
            groupIds[i] = groupIds[i].replace(comeGroupTag, "")
        }
        getUserAndGroupDetail(userIds, groupIds)

    }
    const getUserAndGroupDetail = async (userList, groupList) => {
        let token = "";
        await getUserData().then(res => {
            token = res.token;
        })
        let data = new FormData();
        for (var i = 0; i < userList.length; i++)
            data.append("userIds[]", userList[i]);
        for (var i = 0; i < groupList.length; i++)
            data.append("groupIds[]", groupList[i]);
        await imageUploadApi(data, apiUrl.get_user_and_group_details, token).then(res => {
            
            if (res.status) {
                setUserList(res.result.userList)
                setGroupList(res.result.groupList)
                setShowing(false)

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setShowing(false)
            }

        })
    }
    const renderViewUser = ({ item, index }) => {
        var newDate = moment(new Date(userConversion[index]?.lastMessage?.sentAt * 1000)).format('DD-MM-YYYY hh:MM a');

        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.typeMessageScreen, { userData: item })}
                style={[styles.mainRender, { justifyContent: "space-between" }]}>
                <View style={{ flexDirection: "row" }}>
                    {item?.name == "Unknown" ?
                        <View style={{ alignItems: "center", justifyContent: "center" }}>
                            <View style={styles.viewUnknown} />
                            <Image style={styles.imgUnknown} source={Images.userIcon}></Image>
                        </View> :
                        <View>
                            <Image style={styles.imgStyle} source={{ uri: item.u_image }}></Image>
                        </View>
                    }
                    <View style={{ marginLeft: 20, marginTop: 5 }}>
                        <Text style={styles.radioText}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtInvite}>{"Sent At " + newDate}</Text>
                    </View>
                </View>
                {
                    userConversion[index]?.unreadMessageCount != 0 ?
                        <View style={styles.viewRound}>
                            <View style={styles.pendingView}>
                                <Text style={styles.txtPending}>{userConversion[index]?.unreadMessageCount}</Text>
                            </View>
                        </View> : <></>
                }
            </TouchableOpacity>
        )
    }
    const renderViewGroup = ({ item, index }) => {
        var newDate = moment(new Date(groupConversion[index]?.lastMessage?.sentAt * 1000)).format('DD-MM-YYYY hh:MM a');
        if(item.group_members.length==0)
            return
        
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.chatMessageScreen, { groupData: item })}
                style={styles.mainRender}>

                <View>
                    <Image style={styles.imgStyle} source={{ uri: item.group_image }}></Image>
                    <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0, left: item?.group_members?.length == 1 ? 30 : 20 }}>
                        {
                            item?.group_members?.map((data, index) => {
                                return (
                                    index < 2 &&
                                    <Image style={styles.peopleImg} source={{ uri: data.member.u_image }}></Image>
                                )
                            })
                        }
                    </View>
                </View>

                <View style={{ marginLeft: 20, marginTop: 5 }}>
                    <Text style={styles.radioText}>{item.group_name}</Text>
                    <Text style={styles.txtInvite}>{"Sent At " + newDate}</Text>
                </View>
                {
                    groupConversion[index]?.unreadMessageCount != 0 ?
                        <View style={[styles.viewRound,{right:0,position:"absolute"}]}>
                            <View />
                            <View style={styles.pendingView}>
                                <View />
                                <Text style={styles.txtPending}>{groupConversion[index].unreadMessageCount}</Text>
                            </View>
                        </View> : <></>
                }
            </TouchableOpacity>
        )
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ marginLeft: 16, marginTop: 5 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate(strings.serachMessageScreen, {
                        userList: userList, groupList: groupList,
                        userConversion: userConversion,
                        groupConversion: groupConversion
                    })}>
                        <SimpleSerach
                            onChangeText={(text) => console.log(text)}
                            placeHolder={strings.searchforFriends}
                            isSimpleSerach={true}
                            editable={false}
                            isScreen={true}
                            onPlaceClick={() => console.log("hello")}
                            onSubmitEditing={() => console.log("dd")}

                        />
                    </TouchableOpacity>
                    <ScrollView style={{ marginBottom: 100 }} showsVerticalScrollIndicator={false}>
                        {userList.length > 0 &&
                            <FlatList
                                data={userList}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderViewUser}
                                scrollEnabled={false}
                            >
                            </FlatList>
                        }
                        {groupList.length > 0 &&
                            <FlatList
                                data={groupList}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderViewGroup}
                                scrollEnabled={false}
                            >
                            </FlatList>
                        }
                    </ScrollView>
                </View>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default AllMessage;