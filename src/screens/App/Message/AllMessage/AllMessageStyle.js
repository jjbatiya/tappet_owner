import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 5,
       // marginHorizontal: responsiveWidth(4),
        alignItems: "center",
        justifyContent: "center",
        width:96
    },

    //all message
    mainRender:{
        flexDirection:"row",
        marginVertical:responsiveHeight(1),
        alignItems:"center",
        
    },
    viewUnknown:{
        height:48,width:48,
        backgroundColor:Colors.white,
        opacity:0.2,
        borderRadius:24

    },
    imgUnknown:{
        height:12.35,
        width:12.35,
        position:"absolute",
        tintColor:Colors.white,
    },
    imgStyle:{
        height:48,
        width:48,
        borderRadius:24
    },
    peopleImg:{ height: 15, width: 15, borderRadius: 10 },
    radioText: {
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        lineHeight:18.23  ,
        fontFamily:Fonts.DMSansRegular

    },
    txtInvite:{
        fontWeight:"400",
        fontSize:12,
        lineHeight:15.62,
        color:Colors.white,
        opacity:0.6,
        fontFamily:Fonts.DMSansRegular

    },
    pendingView:{
        height:22,
        width:23,
        borderRadius:12,
        backgroundColor:Colors.primaryViolet,
        alignSelf:"flex-end",
        alignItems:"center",
        justifyContent:"center"
        
    },
    txtPending:{
        fontSize:12,
        fontWeight:"700",
        lineHeight:15.62,
        color:Colors.white,
        fontFamily:Fonts.DMSansRegular

    },
    viewRound:{flexDirection:"row",marginRight:10}

})