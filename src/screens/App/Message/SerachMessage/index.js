import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, Dimensions, StatusBar, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './SerachStyleMsg';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SerachHeader from '../../../../Components/SerachHeader/SerachHeader';
import { comeChatUserTag, comeGroupTag } from '../../../../utils/Constant';
import moment, { now } from 'moment';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { getUserData } from '../../../../utils/helper';



const SerachMessage = (props) => {
    const [serachTxt, setSerachTxt] = useState("")
    const [userList] = useState(props.route.params.userList)
    const [groupList] = useState(props.route.params.groupList)
    const [userConversion] = useState(props.route.params.userConversion)
    const [groupConversion] = useState(props.route.params.groupConversion)
    const [serachListUser, setUserListSerach] = useState(props.route.params.userList)
    const [serachListGroup, setGroupListSerach] = useState(props.route.params.groupList)
    const [serachListMessageUser, setMessageUserListSerach] = useState([])
    const [serachListMessageGroup, setMessageGroupListSerach] = useState([])
    const onBackPress = () => {
        props.navigation.goBack(null)
    }
    useEffect(async () => {
        getUserData().then(res => {
            // getAllMessages(comeChatUserTag + res.u_id);

        })
    }, [])
    const getAllMessages = async (id) => {
        let limit = 30;
        let parentMessageId = 1;
        let searchKeyword = "Hello";

        let messagesRequest = new CometChat.MessagesRequestBuilder()
            .setLimit(limit)
            .setSearchKeyword(searchKeyword)
            .build();

        messagesRequest.fetchPrevious().then(
            messages => {

                console.log("Messages for thread fetched successfully", messages);
            }, error => {
                console.log("Message fetching failed with error:", error);
            }
        );
    }
    const onChangeText = (text) => {
        setSerachTxt(text)
        if (text.trim().length == 0) {
            setUserListSerach(userList)
            setGroupListSerach(groupList)
        }
        else {
            var result = userList.filter(obj => {
                return (obj.u_first_name + " " + obj.u_last_name).toLowerCase().includes(text.toLowerCase())
            })
            setUserListSerach(result)

            var result = groupList.filter(obj => {
                return (obj.group_name).toLowerCase().includes(text.toLowerCase())
            })
            setGroupListSerach(result)


        }
    }
    const getDateTimeUser = (id) => {
        var result = userConversion.filter(obj => {
            return obj.conversationWith.uid == comeChatUserTag + id
        })
        return result[0].lastMessage.sentAt
    }
    const getDateTimeGroup = (id) => {
        var result = groupConversion.filter(obj => {
            return obj.conversationWith.guid == comeGroupTag + id
        })
        return result[0].lastMessage.sentAt
    }
    const renderViewUser = ({ item, index }) => {
        var txtDate = getDateTimeUser(item.u_id);
        var newDate = moment(new Date(txtDate * 1000)).format('DD-MM-YYYY hh:MM a');
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.typeMessageScreen, { userData: item })}
                style={styles.mainRender}>
                <Image style={styles.imgStyle} source={{ uri: item.u_image }}></Image>

                <View style={{ marginLeft: 20, marginTop: 5 }}>
                    <Text style={styles.radioText}>{item.u_first_name + " " + item.u_last_name}</Text>
                    <Text style={styles.txtInvite}>{"Sent At " + newDate}</Text>
                </View>

            </TouchableOpacity>
        )
    }
    const renderViewGroup = ({ item, index }) => {
        var txtDate = getDateTimeGroup(item.group_id);
        var newDate = moment(new Date(txtDate * 1000)).format('DD-MM-YYYY hh:MM a');
        if (item.group_members.length == 0)
            return
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.chatMessageScreen, { groupData: item })}

                style={styles.mainRender}>
                <Image style={styles.imgStyle} source={{ uri: item.group_image }}></Image>

                <View style={{ marginLeft: 20, marginTop: 5 }}>
                    <Text style={styles.radioText}>{item.group_name}</Text>
                    <Text style={styles.txtInvite}>{"Sent At " + newDate}</Text>
                </View>

            </TouchableOpacity>
        )
    }
    const getHighlightedText = (text) => {

        const parts = text.split(' ');
        return (
            <Text style={styles.txtInvite}>
                {parts.map(part => part.toLowerCase() === serachTxt.toLowerCase()
                    ? <Text style={{ backgroundColor: Colors.white, color: Colors.red }}>{part + " "}</Text>
                    : part + " ")}
            </Text>)
    }
    const renderMessages = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10, }}>
                <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
                    <Text style={styles.radioText}>{item.name}</Text>
                    <Text style={styles.txtDate}>{item.date}</Text>
                </View>
                {getHighlightedText(item.message)}


            </View>
        )
    }
    return (
        <View style={styles.container}>
            <StatusBar hidden barStyle={'light-content'} backgroundColor={'#0C0024'} />

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ flex: 1 }}>
                    <SerachHeader
                        onBackPress={onBackPress}
                        isPlace={false}
                        onChangeText={onChangeText}
                        placeHolder={""}
                    />
                    <View style={styles.viewLine} />
                    <ScrollView style={styles.mainView}>
                        {serachListUser.length != 0 &&
                            <Text style={styles.txtallFriends}>{"All friends"}</Text>
                        }
                        {userList.length > 0 &&
                            <FlatList
                                data={serachListUser}
                                style={{ marginVertical: 10 }}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderViewUser}
                                scrollEnabled={false}
                            >
                            </FlatList>
                        }
                        {groupList.length > 0 &&
                            <FlatList
                                data={serachListGroup}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderViewGroup}
                                scrollEnabled={false}
                            >
                            </FlatList>
                        }

                        {/* <Text style={styles.txtallFriends}>{"Messages"}</Text>
                        <FlatList
                            data={messages}
                            style={{ marginVertical: 10 }}
                            showsVerticalScrollIndicator={false}
                            renderItem={renderMessages}
                        /> */}
                    </ScrollView>
                </View>
            </LinearGradient>
        </View>
    )
}
export default SerachMessage;
