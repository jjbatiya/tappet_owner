import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    txtallFriends:{
        fontWeight:"700",
        fontSize:14,
        lineHeight:18.23,
        color:Colors.white,
        textTransform:"uppercase",
        fontFamily:Fonts.DMSansRegular

    },
    viewLine:{
        height:1,
        backgroundColor:Colors.white,
        opacity:0.2
    },
    mainView:{
        margin:responsiveHeight(2)
    },
     //all message
     mainRender:{
        flexDirection:"row",
        marginVertical:responsiveHeight(1),
        alignItems:"center",
        
    },
    imgStyle:{
        height:48,
        width:48,
        borderRadius:24
    },
    radioText: {
        fontSize: 14,
        color: '#fff',
        fontWeight: '400',
        lineHeight:18.23  ,
        fontFamily:Fonts.DMSansRegular

    },
    txtInvite:{
        fontWeight:"400",
        fontSize:12,
        lineHeight:15.62,
        color:Colors.white,
        opacity:0.6,
        fontFamily:Fonts.DMSansRegular
    },

    //render messages
    txtDate:{
        fontSize:10,
        fontWeight:"400",
        color:Colors.white,
        lineHeight:13.02,
        opacity:0.4,
        fontFamily:Fonts.DMSansRegular

    }
   
})