import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    txtName: {
        fontSize: 14, fontWeight: "400", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    txtDetail: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
        color: Colors.white, opacity: 0.6, alignSelf: "center", fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        width: 96,
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnView: { alignItems: "center", justifyContent: "center" },
    renderSubView: { height: 48, marginVertical: 15, flexDirection: "row", alignItems: "center", justifyContent: 'space-between' },
    contentView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    imgStyle: { height: 48, width: 48, borderRadius: 24 },
    borderStyle: { height: 28, width: 96, borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white },
    btnText1: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        position: "absolute",
        opacity: 0.6
    },
    titleText: {
        fontSize: 14, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        textTransform:"uppercase"
    },
    borderRq: { height: 28, width: 96, borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white },

})