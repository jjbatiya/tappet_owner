import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native';
import { Colors } from '../../../../../Resources/Colors';
import { styles } from './PeopleComStyle';
import { strings } from '../../../../../Resources/Strings';
import CommonButton from '../../../../../Components/CommonButton';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../../../utils/helper';
import { apiUrl } from '../../../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import Loader from '../../../../../Components/Loader';
import { toastConfig } from '../../../../../utils/ToastConfig';

const PeopleComponent = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [exploreData, setExploreData] = useState([])
    const [limit, setLimit] = useState(10)
    const [allData, setAllData] = useState([])
    useEffect(async () => {

        if (props.index == 0 || props.index == undefined) {
           
            
            getUserData().then(res => {
                setShowing(true)
                setToken(res.token)
                setExploreData([])
                getExplorePeople(res.token, 1);
            })
        }
    }, [props.onSubmit]);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    function filterData(data, i) {
        if (i == 1)
            return data.friend_request == 1 ;
        else
            return data.friend_request != 1 ;

    }
    const getExplorePeople = async (token, page) => {
        
        await apiCallGetWithToken(apiUrl.explore_people + "?page=" + page + "+&limit=" + limit + "&search=" +props.serachText, token).then(res => {
            if (res.status == true) {
                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                if (page == 1 || page == "1") {
                    if (res.result.length != 0) {
                        setAllData(res.result)
                        categoryWiseData(res.result)
                    }
                    else {
                        showMessage(strings.error, strings.msgTag, res.message)
                    }
                }
                else {
                    //  let result1 = exploreData.map(a => a.data);
                    setExploreData([])
                    let result1 = allData.concat(res.result)
                    categoryWiseData(result1)
                    setAllData(allData)
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const categoryWiseData = (data) => {
        let newArray = [];
        for (var i = 1; i < 3; i++) {
            var obj = {
                title: i == 1 ? "All Friends" : "Others",
                id: i,
                data: data.filter((item) => filterData(item, i))
            }
            newArray.push(obj)
        }
        setExploreData(newArray)
    }
    const handleMoreData = async () => {

        if (totalPage > page) {
            await setPage(page + 1)
            setShowing(true)
            await getExplorePeople(token, (page + 1))
        }
    }
    var onEndReached;

    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10 }}>
                <Text style={styles.titleText}>
                    {item.title}
                </Text>
                <FlatList
                    data={item.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const onAddFriend = async (item) => {
        setShowing(true)
        var obj = {
            invited_user_id: item.u_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {

                getExplorePeople(token, 1);

            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderSubView = ({ item, index }) => {
        return (
            <View style={styles.renderSubView}>
                <TouchableOpacity
                    onPress={() => props.navigation.replace(strings.UserProfileScreen, { u_id: item.u_id })}

                    style={styles.contentView}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <View style={{ marginLeft: responsiveWidth(4) }}>
                        <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                        <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends • " + item.total_pets + " Pets"}</Text>
                    </View>
                </TouchableOpacity>
                {item.friend_request == 0 ?
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Colors.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => onAddFriend(item)}
                        text={strings.addfriend}
                    /> :
                    <TouchableOpacity style={{ alignItems: "center", justifyContent: "center" }} >
                        <View style={styles.borderRq}></View>
                        <Text style={styles.btnText1}>{item.friend_request == 2 ? strings.requestSent : strings.friends}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
    return (
        <View style={{
            flex: 1,

        }}>

            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
            <FlatList
                style={{
                    marginHorizontal: responsiveWidth(4),
                    marginVertical: responsiveHeight(2)
                }}
                data={exploreData}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}
                onEndReachedThreshold={0.1}
                onMomentumScrollBegin={() => { onEndReached = false; }}
                onEndReached={() => {
                    if (!onEndReached) {
                        handleMoreData(); // on End reached
                        onEndReached = true;
                    }
                }}

            />

            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default PeopleComponent;