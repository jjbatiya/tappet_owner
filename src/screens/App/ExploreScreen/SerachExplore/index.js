import React, { useState } from 'react';
import { View, Text, FlatList, Image, useWindowDimensions, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './SerachPeopleStyle';
import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images';
import CommonButton from '../../../../Components/CommonButton';
import SerachHeader from '../../../../Components/SerachHeader/SerachHeader';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import PeopleComponent from './PeopleComponent';
import BusinessesSerach from './BusinessesSerach';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import NotificationController from '../../../../Components/NotificationController';

const SerachPeople = (props) => {
    
    const onBackPress = () => {
        props.navigation.goBack(null)
    }
    const [serachText,setSerachText]=useState("")
    const onChangeText = (text) => {
        setSerachText(text)
    }
    const[onSubmit,setOnSubmit]=useState(false)
    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(props.route.params.tabIndex);
    const [routes] = React.useState([
        { key: strings.peopleKey, title: strings.peopleKey },
        { key: strings.businessesKey, title: strings.businessesKey },
    ]);
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                  console.log("Incoming call coming:1234", call);
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const renderScene = ({ route,index1 }) => {
        switch (route.key) {
            case strings.peopleKey:
                return <PeopleComponent navigation={props.navigation} serachText={serachText} onSubmit={onSubmit} index={index}/>;
            case strings.businessesKey:
                return<BusinessesSerach  navigation={props.navigation} serachText={serachText} index={index} />
            default:
                return null;
        }
    };
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#ffffff00', paddingVertical: -50 }}
        />
    );
    const onPlaceClick=()=>{
        alert("place click")
    }
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={{ flex: 1 }}>
                    <SerachHeader
                        onBackPress={onBackPress}
                        isPlace={index==1?true:false}
                        onChangeText={onChangeText}
                        placeHolder={strings.serachPlaceholder}
                        onPlaceClick={onPlaceClick}
                        onSubmitText={()=>setOnSubmit(!onSubmit)}

                    />
                    <TabView
                        navigationState={{ index, routes }}
                        renderScene={renderScene}
                        onIndexChange={setIndex}
                        initialLayout={{ width: layout.width }}
                        renderTabBar={renderTabBar}
                    />
                </View>
            </LinearGradient>
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisible} callAccept={callAccept} />
            }
              {isVisible==false&&Platform.OS=="ios"&&
                    <NotificationController
                        navigation={props.navigation}
                        callIsVisible={isVisible}
                        onMessageNotification={()=>{}}

                    />
                }
        </View>
    )
}

export default SerachPeople;