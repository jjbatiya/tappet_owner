import React, { useRef, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Dimensions, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../Resources/Colors';
import { styles } from './SerachStyle';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images';
import CommonButton from '../../../../../Components/CommonButton';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import RBSheet from "react-native-raw-bottom-sheet";
import BottomFilter from './BottomFilter';
import CommonStatusBar from '../../../../../Components/CommonStatusBar';
const { width, height } = Dimensions.get("window")
const data = [

    {
        image: Images.dogIcon,
        category: "DOG SItter",
        title: "Doggy Bed Day Care",
        doggyBedIcon: Images.doggyBedIcon,
        rate: 47,
        location: "42 Park Avenue, NY",
        id: 1
    },
    {
        image: Images.dogIcon2,
        category: "Groomer",
        title: "Doggy Bed Day Care",
        doggyBedIcon: Images.doggyBedIcon,
        rate: 47,
        location: "42 Park Avenue, NY",
        id: 2
    },

    {
        image: Images.dogIcon3,
        category: "DOG SItter",
        title: "Doggy Bed Day Care",
        doggyBedIcon: Images.doggyBedIcon,
        rate: 47,
        location: "42 Park Avenue, NY",
        id: 3
    },
    {
        image: Images.dogIcon2,
        category: "Groomer",
        title: "Doggy Bed Day Care",
        doggyBedIcon: Images.doggyBedIcon,
        rate: 47,
        location: "42 Park Avenue, NY",
        id: 4
    }

]
const filterOption = [
    {
        key: "Recommended (default)",
        title: "Recommended (default)"
    },
    {
        key: "Distance",
        title: "Distance"
    },
    {
        key: "Rating",
        title: "Rating"
    },
    {
        key: "Most Reviewed",
        title: "Most Reviewed"
    }
]
const BusinessesSerach = (props) => {
    let filterRef, filterRef1 = useRef();
    const [selectFilter, setFilter] = useState("Recommended (default)")
    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={styles.mainContent}>
                <Image style={styles.mainImgStyle} source={item.image}></Image>
                <View style={styles.topLabel}>
                    <View style={styles.topLabelInner} />
                    <Text style={styles.topLabelText}>{item.category}</Text>
                </View>
                <LinearGradient style={{
                    position: "absolute",
                    height: responsiveHeight(12),
                    width: width, bottom: 0
                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomView}>
                    <Image style={styles.bedIcon} source={item.doggyBedIcon}></Image>
                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={styles.txtTitle}>{item.title}</Text>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <ImageBackground style={styles.imgStar} source={Images.fillStar}></ImageBackground>
                            <Text style={styles.txtRate}>{item.rate}</Text>
                            <Image style={styles.imgPlace} source={Images.placeIcon}></Image>
                            <Text style={styles.txtPlace}>{item.location}</Text>
                        </View>

                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    const filterView = () => {
        return filterOption.map((item) => {
            return (
                <TouchableOpacity
                    onPress={() => {
                        setFilter(item.key)
                    }}
                    key={item.key} style={styles.filterContainer}>
                    <View
                        style={selectFilter === item.key ? styles.selectradioCircle : styles.radioCircle}
                    >
                        {selectFilter === item.key && <View style={styles.selectedRb} />}
                    </View>
                    <Text style={styles.radioText}>{item.title}</Text>
                </TouchableOpacity>
            )
        })
    }
    const onclickSave = () => {
        filterRef1.close();
    }
    return (
        <View style={{
            flex: 1,
            marginHorizontal: responsiveWidth(4),
            marginVertical: responsiveHeight(2)
        }}>
            <CommonStatusBar color={Colors.backgroundColor} />

            <View style={styles.categoryView}>
                <Text style={styles.titleText}>
                    {strings.topResult}
                </Text>
                <View style={styles.rightView}>
                    <TouchableOpacity onPress={() => filterRef.open()}>
                        <Image style={styles.rightArrow} source={Images.filterIcon}></Image>
                    </TouchableOpacity>
                    <View style={{ width: 1, height: 15, backgroundColor: "#EA8FFA" }}></View>
                    <TouchableOpacity onPress={() => filterRef1.open()}>
                        <Image style={styles.rightArrow} source={Images.menuSortIcon}></Image>
                    </TouchableOpacity>
                    <Text style={styles.rightText}>{strings.sortResult}</Text>
                </View>
            </View>
            <FlatList
                data={data}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}
            />
            <RBSheet
                ref={ref => {
                    filterRef = ref;
                }}
                height={212}
                openDuration={250}
                closeOnDragDown={true}
                customStyles={{
                    container: {

                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        backgroundColor: Colors.dialogBackground,
                        padding: 10,
                    }
                }}
            >
                {filterView()}
            </RBSheet>
            <RBSheet
                ref={ref => {
                    filterRef1 = ref;
                }}
                height={responsiveHeight(80)}
                openDuration={250}
                closeOnDragDown={true}
                customStyles={{
                    container: {

                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        backgroundColor: Colors.dialogBackground,
                        padding: 10,
                    }
                }}
            >
                <BottomFilter
                    onPress={onclickSave}
                />

            </RBSheet>
        </View>
    )
}
export default BusinessesSerach;