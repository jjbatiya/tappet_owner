import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    //Title view and right arrow
    titleText: { fontSize: 14, fontWeight: "700", lineHeight: 18.23, color: Colors.white, textTransform: "uppercase" },
    categoryView: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", height: 25, marginTop: responsiveHeight(2) },
    rightView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    rightText: {
        fontSize: 12, fontWeight: "500", lineHeight: 15.62, color: '#EA8FFA', fontFamily: Fonts.DMSansRegular
    },
    rightArrow: { width: 15, height: 15, tintColor: '#EA8FFA', marginHorizontal: 8 },

    //content view
    mainContent: {
        height: 180, width: responsiveWidth(100), marginRight: responsiveWidth(2.5),
        marginVertical: responsiveHeight(1)
    },
    mainImgStyle: { height: 180, width: responsiveWidth(100), borderRadius: 4, resizeMode: 'cover' },
    topLabel: { position: "absolute", alignItems: "center", justifyContent: "center" },
    topLabelInner: {
        borderRadius: 2, backgroundColor: Colors.white,
        opacity: 0.2, borderWidth: 1, margin: 8, height: 20, width: 72,
        borderColor: Colors.white
    },
    topLabelText: {
        position: "absolute", fontSize: 9, lineHeight: 11.72,
        fontWeight: "700", color: Colors.white, alignSelf: "center", opacity: 0.6,
        margin: 8, textTransform: "uppercase", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 10, left: 10, flexDirection: "row", alignItems: "center" },
    bedIcon: { height: 40, width: 40, borderRadius: 20 },
    txtTitle: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    imgStar: { height: 9, width: 9.6 },
    txtRate: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 3, marginRight: 10, fontFamily: Fonts.DMSansRegular
    },
    imgPlace: { height: 9, width: 9.6, tintColor: Colors.white, opacity: 0.3 },
    txtPlace: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        marginLeft: 3, color: Colors.white, opacity: 0.6, fontFamily: Fonts.DMSansRegular
    },

    //filter radio view
    filterContainer: {
        marginBottom: 20,
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        paddingHorizontal: 20
    },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        marginLeft: 12,
        fontFamily: Fonts.DMSansRegular

    },
    radioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectradioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: '#EA8FFA',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectedRb: {
        width: 10,
        height: 10,
        borderRadius: 50,
        backgroundColor: '#EA8FFA',
    },
})