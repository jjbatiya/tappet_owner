import React, { useRef, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Slider, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../../../Resources/Colors';
import { styles } from './BottomFilterStyle';
import { strings } from '../../../../../../Resources/Strings';
import { Images } from '../../../../../../Resources/Images';
import CommonButton from '../../../../../../Components/CommonButton';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import ModalDropdown from 'react-native-modal-dropdown';
import { ChevronDown } from '../../../../../../utils/svg/ChevronDown';
const filterDataDrop = [
    "More than 1000 reviews",
    "More than 10000 reviews",
    "Less than 100 reviews",
    "Less than 1000 review"
]
const BottomFilter = (props) => {
    const [catData, setCatData] = useState([
        {
            text: "Pet Boarding",
            image: Images.Boarding,
            id: 1
        },
        {
            text: "Day Care",
            image: Images.GroupIcon,
            id: 2
        },
        {
            text: "Pet Sitter",
            image: Images.PetSitter,
            id: 3
        },
        {
            text: "Pet Walker",
            image: Images.PetWalker,
            id: 4
        },
        {
            text: "Grooming",
            image: Images.Grooming,
            id: 5
        },
        {
            text: "Dog Training",
            image: Images.PetTrainer,
            id: 6
        },
        {
            text: "Dog Walker",
            image: Images.PetWalker,
            id: 7
        },
        {
            text: "Pet Stores",
            image: Images.PetStore,
            id: 8
        },

    ])
    const [selectedItemArray, setSelectedItem] = useState([])
    const [value, setValue] = useState(2)
    const [certificatesArray, setCertificatesArray] = useState([])

    const onCkickCategory = (index) => {
        let newArray = [...selectedItemArray]
        if (checkDataExist(catData[index])) {
            const removeIndex = selectedItemArray.indexOf(catData[index]);
            if (removeIndex > -1) {
                newArray.splice(removeIndex, 1);
                setSelectedItem(newArray)
            }

        }
        else {
            newArray.push(catData[index])
            setSelectedItem(newArray)

        }

    }
    const renderCategory = ({ item, index }) => {
        const isSelect = checkDataExist(item);
        return (
            <TouchableOpacity
                onPress={() => onCkickCategory(index)}
                style={{ marginRight: 15, height: 115, }}>
                <Image source={item.image} style={[styles.cameraStyle, { tintColor: isSelect ? Colors.primaryViolet : Colors.white }]}></Image>
                <Text style={[styles.labelText,{ color: isSelect ? Colors.primaryViolet : Colors.white }]}>{item.text}</Text>
            </TouchableOpacity>
        )
    }
    const checkDataExist = (item) => {
        if (selectedItemArray.indexOf(item) === -1) {
            return false
        }
        else {
            return true;
        }
    }
    const onSelect = (index) => {
        var newArray = [...certificatesArray];
        newArray.push({ name: "Certificate Name" + index })
        setCertificatesArray(newArray);
    }
    const onDelete = (index) => {
        var newArray = [...certificatesArray];
        newArray.pop(index)
        setCertificatesArray(newArray);
    }
    const renderCertificate = ({ item, index }) => {
        return (
            <View style={styles.mainVi} >
                <View style={styles.borderCertificate} />
                <View style={styles.contentCertificateView}>
                    <Text style={styles.txtCertificateName}>
                        {item.name}
                    </Text>
                    <TouchableOpacity onPress={() => onDelete(index)}>
                        <Image style={styles.closeImage} source={Images.closeIcon}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    return (
        <ScrollView>
            <View style={styles.contentView}>
                <Text style={styles.recommendedText}>{strings.categories}</Text>
                <View style={{ marginTop: 20, height: 80 }}>
                    <FlatList
                        data={catData}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        renderItem={renderCategory}
                    />
                </View>
                <Text style={[styles.recommendedText,]}>{strings.distance}</Text>
                <Slider
                    step={1}
                    minimumValue={1}
                    maximumValue={100}
                    value={14}
                    onValueChange={val => console.log(val)}
                    onSlidingComplete={val => console.log(val)}
                    thumbTintColor={Colors.primaryViolet}
                    minimumTrackTintColor={Colors.primaryViolet}
                />
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.textMile}>1 mile</Text>
                    <Text style={styles.textMile}>10 mile</Text>
                    <Text style={styles.textMile}>100 mile</Text>
                </View>
                <Text style={[styles.recommendedText, { marginTop: 40 }]}>{strings.review}</Text>
                <ModalDropdown
                    //defaultValue={"Enter City Name"}
                    options={filterDataDrop}
                    style={styles.dropView}
                    // onSelect={(index) => this.stateIndexToGetStateCode(index)}
                    dropdownStyle={{
                        width: 300,
                    }}
                    dropdownTextStyle={styles.dropdown}
                    textStyle={styles.txtStyle}
                    dropdownTextHighlightStyle={{
                        color: Colors.theme
                    }}
                    //defaultIndex={0}
                    renderRightComponent={() => {
                        return (
                            <View style={styles.arrow}>
                                <ChevronDown />
                            </View>
                        )
                    }}
                />
                <View style={styles.viewLine}></View>
                <Text style={[styles.recommendedText, { marginTop: 40 }]}>{strings.certificates}</Text>
                <ModalDropdown
                    defaultValue={"Type Certificate Name"}
                    options={filterDataDrop}
                    style={styles.dropView}
                    onSelect={(index) => onSelect(index)}
                    dropdownStyle={{
                        width: 300,
                    }}

                    dropdownTextStyle={styles.dropdown}
                    textStyle={styles.txtStyle}
                    dropdownTextHighlightStyle={{
                        color: Colors.theme
                    }}
                    //defaultIndex={0}
                    renderRightComponent={() => {
                        return (
                            <View style={styles.arrow}>
                                <ChevronDown />
                            </View>)
                    }}
                />
                <View style={styles.viewLine}></View>

                <FlatList
                    data={certificatesArray}
                    numColumns={2}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderCertificate}
                >
                </FlatList>

                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={{ marginTop: 40 }}
                    onPress={() => props.onPress()}
                    text={strings.save}
                />

            </View>
        </ScrollView>
    )
}
export default BottomFilter;