import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../../Resources/Colors';
import { Fonts } from '../../../../../../Resources/Fonts';

export const styles = StyleSheet.create({

    contentView: {
        //flex: 1 ,
        marginHorizontal: responsiveWidth(4),
        marginBottom: responsiveHeight(1),
        marginTop: responsiveHeight(1)
    },
    recommendedText: {
        fontWeight: "700",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        textTransform: "uppercase",
        fontFamily: Fonts.DMSansRegular

    },
    borderView: {
        height: 60, width: 60,
        borderRadius: 40, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white
    },
    catDataView: { alignItems: "center", justifyContent: "center", borderRadius: 40, borderWidth: 1.5, borderColor: Colors.transparent },
    selectCatDataView: {
        alignItems: "center", justifyContent: "center", borderRadius: 40, borderWidth: 1.5, borderColor: Colors.primaryViolet
    },

    cameraStyle: {
        height: 22, width: 22,
        tintColor: Colors.white, alignSelf: "center", 
    },
    labelText: {
        fontWeight: "400", fontSize: 10, lineHeight: 13.02,
        color: Colors.white, alignSelf: "center",
        marginVertical: responsiveHeight(1), fontFamily: Fonts.DMSansRegular,
        opacity:0.6
    },
    titleText: { fontSize: 14, fontWeight: "700", lineHeight: 18.23, color: Colors.white, textTransform: "uppercase" },

    //dropdown
    dropView: { marginTop: responsiveHeight(1.5), height: 30 },
    dropdown: {
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.black,
    },
    txtStyle: {
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400"
    },
    arrow: {
        position: 'absolute',
        right: responsiveHeight(0.5),
        height: responsiveHeight(4),
        width: responsiveWidth(3),
        resizeMode: "contain",
        flex: 0.1,
        tintColor: Colors.white
    },
    viewLine: { height: 1, backgroundColor: Colors.white, opacity: 0.2 },
    //common button style
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center"

    },
    //slider style
    textMile: {
        fontSize: 12, fontWeight: "400", lineHeight: 15.62,
         color: Colors.white, fontFamily: Fonts.DMSansRegular,
         opacity:0.6
    },

    //certificate /view
    borderCertificate: { backgroundColor: Colors.white, width: 132, height: 28, opacity: 0.1, borderRadius: 4 },
    mainCertificateView: { marginRight: 10, marginBottom: 5 },
    contentCertificateView: {
        position: "absolute", width: 132, height: 28, alignItems: "center", justifyContent: "space-between",
        flexDirection: "row", paddingHorizontal: 7
    },
    txtCertificateName: {
        color: Colors.white, lineHeight: 15.62,
        fontSize: 12, opacity: 0.6, textAlign: "center", fontFamily: Fonts.DMSansRegular
    },
    closeImage: { height: 14, width: 14, tintColor: Colors.white }



})