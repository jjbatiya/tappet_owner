import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },
    
})