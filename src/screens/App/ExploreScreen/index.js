import React, { useState } from 'react';
import { View, Text, useWindowDimensions, Platform } from 'react-native';
import AppHeader from '../../../Components/AppHeader/AppHeader';
import { Images } from '../../../Resources/Images';
import styles from './ExploreStyle';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../../Resources/Colors';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import People from './People/index';
import Businesses from './Businesses/index'
import { Fonts } from '../../../Resources/Fonts';
import { strings } from '../../../Resources/Strings';
import CometChatIncomingCall from '../../../Components/Calls/CometChatIncomingCall';
import { CometChat } from '@cometchat-pro/react-native-chat';
import NotificationController from '../../../Components/NotificationController';

const Explore = (props) => {
    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: strings.peopleKey, title: strings.peopleKey },
        { key: strings.businessesKey, title: strings.businessesKey },
    ]);
    const [callData, setCallData] = useState(null)
    const [isVisible, setVisible] = useState(false)
    var listnerID = "cllListner";
    CometChat.addCallListener(
        listnerID,
        new CometChat.CallListener({
            onIncomingCallReceived: (call) => {
                console.log("Incoming call coming:1234", call);
                // Handle incoming call

                setCallData(call)
                setTimeout(() => {
                    props.navigation.navigate(strings.incomingCallScreen, { callData: call, flag: "user", userData: null })

                    setVisible(true)
                }, 200);
            },
            onOutgoingCallAccepted: (call) => {

                // Outgoing Call Accepted
            },
            onOutgoingCallRejected: (call) => {
                // console.log("Outgoing call rejected:", call);
            },
            onIncomingCallCancelled: (call) => {
                setVisible(false)
            }
        })
    );
    const onSerach = () => {
        props.navigation.navigate(strings.serachPeopleScreen, { tabIndex: index })
    }
    // const renderScene = SceneMap({
    //     People: FirstRoute,
    //     Businesses: SecondRoute,
    // });
    const renderScene = ({ route, index }) => {
        switch (route.key) {
            case strings.peopleKey:
                return <People navigation={props.navigation} />;
            case strings.businessesKey:
                return <Businesses navigation={props.navigation} />
            default:
                return null;
        }
    };
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: '#7235FF', paddingVertical: -50 }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color, fontSize: 12, fontWeight: "500", fontFamily: Fonts.DMSansRegular, lineHeight: 15.62 }}>
                    {route.title}
                </Text>
            )}


        />
    );
    const callCancel = (data) => {
        var sessionID = data.sessionId;
        var status = CometChat.CALL_STATUS.REJECTED;

        CometChat.rejectCall(sessionID, status).then(
            call => {
                setVisible(false)

            },
            error => {
                setVisible(false)
            }
        );
    }
    const callAccept = (data) => {
        var sessionID = data.sessionId;

        CometChat.acceptCall(sessionID).then(
            call => {
                console.log("Call accepted successfully:", call);
                // start the call using the startCall() method
                setVisible(false)
                props.navigation.navigate(strings.callScreen, { callData: call })

            },
            error => {
                setVisible(false)

                // handle exception
            }
        );
    }
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.btnBackgroundColor}
                style={{ height: Platform.OS == "ios" ? responsiveHeight(12.4) : responsiveHeight(8.5) }}
            >
                <AppHeader
                    navigation={props.navigation}
                    serachIcon={Images.serachIcon}
                    rightSerachClick={() => onSerach()}
                    Tab={true}
                    title={'Explore'} />

            </LinearGradient>
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={renderTabBar}


            />
            {Platform.OS == "ios" &&
                <CometChatIncomingCall callData={callData} callCancel={callCancel}
                    isVisible={isVisible} callAccept={callAccept} />
            }
            {isVisible == false && Platform.OS == "ios" &&
                <NotificationController
                    navigation={props.navigation}
                    callIsVisible={isVisible}
                />
            }
        </View >
    )
}
const FirstRoute = () => (
    <People />
);

const SecondRoute = () => (
    <Businesses />
);
export default Explore; 5