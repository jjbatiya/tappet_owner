import { StyleSheet } from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { Colors } from "../../../../Resources/Colors";
import { Fonts } from "../../../../Resources/Fonts";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  linearGradient: {
    flex: 1,
  },
  btnlinearGradient: {
    height: 28,
    borderRadius: 5,
    // marginHorizontal: responsiveWidth(4),
    alignItems: "center",
    justifyContent: "center",
    width: 96,
  },
  recommendedText: {
    fontWeight: "700",
    fontSize: 14,
    lineHeight: 18.23,
    color: Colors.white,
    fontFamily: Fonts.DMSansRegular,
  },
  contentView: {
    flex: 1,
    marginHorizontal: responsiveWidth(4),
    marginVertical: responsiveHeight(2),
  },
  btnText: {
    fontWeight: "400",
    fontSize: 14,
    lineHeight: 18.23,
    color: Colors.white,
    fontFamily: Fonts.DMSansRegular,
  },
  renderStyle: { width: "48%", height: 220 },
  borderStyle: {
    borderColor: Colors.white,
    borderRadius: 4,
    borderWidth: 1,
    width: "100%",
    height: 220,
    opacity: 0.2,
  },
  renderContent: {
    position: "absolute",
    width: "100%",
    height: 220,
    alignItems: "center",
    justifyContent: "center",
  },
  imgStyle: { width: 64, height: 64,borderRadius:32 },
  txtName: {
    fontSize: 14,
    fontWeight: "400",
    lineHeight: 18.23,
    alignSelf: "center",
    color: Colors.white,
    marginTop: responsiveHeight(1.8),
    fontFamily: Fonts.DMSansRegular,
  },
  txtDetail: {
    fontSize: 12,
    fontWeight: "400",
    lineHeight: 15.62,
    alignSelf: "center",
    color: Colors.white,
    textAlign: "center",
    opacity: 0.6,
    fontFamily: Fonts.DMSansRegular,
  },
  btnView: {
    width: "100%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: responsiveHeight(2),
  },
  btnText1: {
    fontWeight: "400",
    fontSize: 12,
    lineHeight: 15.62,
    color: Colors.white,
    opacity: 0.6
},
});
