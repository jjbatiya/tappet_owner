import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { styles } from './PeopleStyle';
import { strings } from '../../../../Resources/Strings';
import CommonButton from '../../../../Components/CommonButton';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiCallGetWithToken, apiCallWithToken, getUserData } from '../../../../utils/helper';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import { toastConfig } from '../../../../utils/ToastConfig';


const People = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [token, setToken] = useState("")
    const [totalPage, setTotalPage] = useState(1)
    const [page, setPage] = useState(1)
    const [exploreData, setExploreData] = useState([])
    const [limit, setLimit] = useState(12)
    const onAddFriend = async (item) => {
      
        setShowing(true)
        var obj = {
            invited_user_id: item.u_id
        }
        await apiCallWithToken(obj, apiUrl.add_friend, token).then(res => {
            if (res.status == true) {
                setTimeout(() => {
                    getExplorePeople(token,1);

                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    useEffect(async () => {
        getUserData().then(res => {
            setShowing(true)
            setToken(res.token)
            getExplorePeople(res.token,1);
        })
    }, []);
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    function filterData(data) {
        return data.friend_request != 1;
    }
    const getExplorePeople = async (token,page) => {
        await apiCallGetWithToken(apiUrl.explore_people + "?page=" + page + "+&limit=" + limit, token).then(res => {
         //   setPage(page + 1)
            if (res.status == true) {

                setShowing(false)
                setTotalPage(res.pagination.lastPage)
                setPage(page)
                if (page == 1) {
                    setExploreData(res.result.filter((item) => filterData(item)))
                }
                else {
                    setExploreData(exploreData.concat(res.result.filter((item) => filterData(item))))
                }
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.UserProfileScreen, { u_id: item.u_id })}

                style={styles.renderStyle}>
                <View style={styles.borderStyle}></View>
                <View style={styles.renderContent}>
                    <Image source={{ uri: item.u_image }} style={styles.imgStyle}></Image>
                    <Text style={styles.txtName}>{item.u_first_name + " " + item.u_last_name}</Text>
                    <Text style={styles.txtDetail}>{item.total_mutual_friends + " Mutual Friends\n" + item.total_pets + " Pets"}</Text>

                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={item.friend_request == 0 || (item.friend_request_sent_by_me == false && item.friend_request == 2) ? Colors.btnBackgroundColor : [Colors.opacityColor2, Colors.opacityColor2]}
                        btnText={item.friend_request == 0 || (item.friend_request_sent_by_me == false && item.friend_request == 2) ? styles.btnText : [styles.btnText, { opacity: 0.6 }]}
                        viewStyle={styles.btnView}
                        onPress={() => (item.friend_request == 0 || (item.friend_request_sent_by_me == false && item.friend_request == 2)) && onAddFriend(item)}
                        text={item.friend_request == 0 || (item.friend_request_sent_by_me == false && item.friend_request == 2) ?
                            strings.addfriend : item.friend_request == 2 ?
                                strings.requestSent : strings.friends}
                    />

                </View>
            </TouchableOpacity>
        )
    }
    const handleMoreData = () => {
        if (totalPage > page) 
        {
            setShowing(true)
            getExplorePeople(token,(page+1))
        }
    }
    var onEndReached;

    return (
        <View style={styles.container}>
            <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >
                <View style={styles.contentView}>
                    <Text style={styles.recommendedText}>{strings.recommended}</Text>
                    <FlatList
                        columnWrapperStyle={{ justifyContent: 'space-between', marginTop: 16 }}
                        data={exploreData}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                        onEndReachedThreshold={0.1}
                        onMomentumScrollBegin={() => { onEndReached = false; }}
                        onEndReached={() => {
                            if (!onEndReached) {
                                handleMoreData(); // on End reached
                                onEndReached = true;
                            }
                        }}

                    />
                </View>
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
            </LinearGradient>
          
        </View>
    )
}
export default People