import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleSheet, TouchableOpacity, View, Platform } from 'react-native';
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { strings } from '../../../../Resources/Strings';
import { Images } from '../../../../Resources/Images'
import { Colors } from '../../../../Resources/Colors'
import { Fonts } from '../../../../Resources/Fonts';
import ProfileTab from './ProfileTab';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { EventIconUnselect } from '../../../../utils/svg/EventIconUnselect';
import { EventIcon } from '../../../../utils/svg/EventIcon';
import { UserIcon } from '../../../../utils/svg/UserIcon';
import Business from '../../../../utils/svg/Business';


const Tab = createMaterialBottomTabNavigator();


const ProfileCrop = (props) => {
    const test = () => {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.theme }}>
                {Platform.OS == "android" ?
                    <CommonStatusBar color={Colors.backgroundColor} /> : <></>}
            </View>
        )
    }
    const profile = () => {
        return (
            <ProfileTab navigation={props.navigation} />
        )
    }
    return (
        <Tab.Navigator
            shifting={true}
            barStyle={{ backgroundColor: Colors.theme }}
            tabBarOptions={{

                //  activeBackgroundColor: '#0C0024',
                // inactiveBackgroundColor: Colors.theme,

                style: {
                    backgroundColor: '#0C0024',
                    borderTopWidth: 1,
                    height: Platform.OS == "ios" ? responsiveHeight(8.3) : responsiveHeight(8.3),
                    borderTopColor: 'rgba(255,255,255,0.1)',
                    paddingBottom: Platform.OS == "ios" ? 1 : 0
                },
            }}
        >
            <Tab.Screen
                name="Dashboard"
                component={test}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.dashBoard}</Text>
                    // ),
                    tabBarLabel: strings.dashBoard,
                    tabBarIcon: ({ focused, color, size }) => (
                        <Image source={Images.dashBoard} style={[styles.icon, focused ? styles.tabFocus : styles.tabUnFocus]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Pet List"
                component={test}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.petList}</Text>
                    // ),
                    tabBarLabel: strings.petList,
                    tabBarIcon: ({ focused, color, size }) => (
                        focused ?
                            <Image source={Images.petProfileTab} style={[styles.icon, focused ? styles.tabFocus : styles.tabUnFocus]} resizeMode={'contain'} /> :
                            <Business />
                    ),
                }} />
            <Tab.Screen
                name="Events"
                component={test}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{"Events"}</Text>
                    // ),
                    tabBarLabel: "Events",
                    tabBarIcon: ({ focused, color, size }) => (
                        <View style={styles.viewTabIcon}>
                            {!focused ?
                                <EventIconUnselect width={24} height={24} /> :
                                <EventIcon fillColor={focused ? Colors.primaryViolet : null} storke={focused ? Colors.primaryViolet : Colors.opacityColor3} width={24} height={24} />
                            }
                        </View>
                        // <Image source={Images.calendarTab} style={[styles.icon, focused ? styles.tabFocus : styles.tabUnFocus]} resizeMode={'contain'} />
                    ),
                }} />
            <Tab.Screen
                name="Profile"
                component={profile}
                options={{
                    // tabBarLabel: ({ focused, color, size }) => (
                    //     <Text style={[styles.tabLable, focused ? styles.tabFocusLabel : styles.tabUnFocusLabel]}>{strings.profileTab}</Text>
                    // ),
                    tabBarLabel: strings.profileTab,
                    tabBarIcon: ({ focused, color, size }) => (
                        focused ?
                            <UserIcon fill={Colors.primaryViolet} stroke={Colors.primaryViolet} /> :
                            <UserIcon />

                    ),
                }} />
        </Tab.Navigator>
    )
}


const styles = StyleSheet.create({
    icon: {
        width: 19,
        height: 20
    },
    tabLable: {
        fontSize: 11,
        fontFamily: Fonts.DMSansRegular, letterSpacing: 0.2,
        fontWeight: "500", lineHeight: 13.02,
    },
    tabFocus: {
        tintColor: Colors.primaryViolet
    },
    tabUnFocus: {
        tintColor: Colors.white,
        opacity: 0.4
    },
    tabFocusLabel: {
        color: Colors.primaryViolet
    },
    tabUnFocusLabel: {
        color: Colors.white,
        opacity: 0.4
    },
})
export default ProfileCrop;