import React, { useEffect, useRef, useState, use } from 'react';
import { Image, Text, TouchableOpacity, Dimensions, View, FlatList, Animated, StatusBar } from 'react-native';
import { strings } from '../../../../../Resources/Strings';
import { Images } from '../../../../../Resources/Images'
import { Colors } from '../../../../../Resources/Colors'
import { Fonts } from '../../../../../Resources/Fonts';
import { styles } from './ProfileTabStyle'
import LinearGradient from 'react-native-linear-gradient';
import EventDetailHeader from '../../../../../Components/EventDetailHeader';
import { Rating } from 'react-native-ratings';
import PercentageBar from '../../../../../Components/PercentageBar';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { EventIcon } from '../../../../../utils/svg/EventIcon';
const { width: SCREEN_WIDTH } = Dimensions.get('screen')
const { width } = Dimensions.get("window")
const HEADER_EXPANDED_HEIGHT = 450
const HEADER_COLLAPSED_HEIGHT = 60
const categoryData = [
    {
        id: 1,
        title: "Overview"
    },
    {
        id: 2,
        title: "Ratings"
    },
    {
        id: 3,
        title: "Events"
    },
    {
        id: 4,
        title: "Followers"
    },
    {
        id: 5,
        title: "Photos"
    }
]
const ratingData = [
    {
        id: 1,
        percentage: "70%",
        value: 5
    },
    {
        id: 2,
        percentage: "15%",
        value: 4
    },
    {
        id: 3,
        percentage: "10%",
        value: 3
    },
    {
        id: 4,
        percentage: "13%",
        value: 2
    },
    {
        id: 5,
        percentage: "20%",
        value: 1
    }
]
const ratingReviewData = [
    {
        id: 1,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    },
    {
        id: 2,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    },
    {
        id: 3,
        name: "Amy Beck",
        time: "1w ago",
        image: Images.people,
        review: "Nice place to leave your pet for a day or two. They take good care. Love it.",
        ratingCount: 3
    }
]
const eventData = [
    {
        id: 1,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 2,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 3,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 4,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 5,
        image: Images.dogIcon5,
        eventName: "Shepherds of the world",
        evnetTime: "Thursday, May 6, 2021"
    },
    {
        id: 6,
        image: Images.dogIcon3,
        eventName: "Who is the strongest",
        evnetTime: "Thursday, May 6, 2021"
    }
]
const imgPersons = [
    { image: Images.people, id: 1 },
    { image: Images.people1, id: 2 },
    { image: Images.people, id: 3 },
    { image: Images.people1, id: 4 },
    { image: Images.people, id: 5 },
    { image: Images.people1, id: 6 },
    { image: Images.people, id: 7 },
    { image: Images.people1, id: 8 },
    { image: Images.people, id: 9 },
    { image: Images.people1, id: 10 },
    { image: Images.people, id: 11 },
    { image: Images.people1, id: 12 },
    { image: Images.people, id: 13 },
    { image: Images.people1, id: 14 },
    { image: Images.people, id: 15 },
    { image: Images.people1, id: 16 },
    { image: Images.people, id: 17 },
    { image: Images.people1, id: 18 },
]
const photoData = [
    {
        key: "dog1",
        image: Images.dogIcon
    },
    {
        key: "dog2",
        image: Images.dogIcon1
    },
    {
        key: "dog3",
        image: Images.dogIcon2
    },
    {
        key: "dog3",
        image: Images.dogIcon3
    },
    {
        key: "dog4",
        image: Images.dogIcon1
    },
    {
        key: "dog5",
        image: Images.dogIcon2
    },
    {
        key: "dog6",
        image: Images.dogIcon3
    }
]
const ProfileTab = (props) => {
    const [selectItem, setItem] = useState(categoryData[0])
    const backPress = () => {
        props.navigation.goBack(null);
    }
    const [flatlistRef, setFlatListRef] = useState(null)
    const [scrollY, setScrollY] = useState(new Animated.Value(0))
    const [flag, setFlag] = useState(true)
    const renderCategory = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    onCategoryClick(index, item)
                }
                style={{ marginRight: 20 }}>
                <Text style={selectItem?.id == item?.id ? styles.txtSelectCategory : styles.txtCategory}>{item.title}</Text>
                {selectItem?.id == item?.id &&
                    <View style={styles.viewLine} />
                }
            </TouchableOpacity>
        )
    }
    const overView = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={styles.viewOverView}>
                    <Image style={styles.imgIcon} source={Images.petIcon}></Image>
                    <Text style={styles.txtDetail}>{"Pet Boarding • Day Care • Pet Sitter"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={[styles.imgIcon]} source={Images.telephone}></Image>
                    <Text style={styles.txtDetail}>{"+164 6630 8679 \n+164 6540 1780"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.globe}></Image>
                    <Text style={styles.txtLink}>{"www.doggybed.com"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={[styles.imgIcon, { tintColor: Colors.opacityColor3 }]} source={Images.clock}></Image>
                    <Text style={styles.txtDetail}>{"Open 24 Hours"}</Text>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.placeIcon}></Image>
                    <View>
                        <Text style={styles.txtDetail}>{"234 Richmonf Tower, Opposite Star Bucks, Park Avenue, New York"}</Text>
                        <Text style={styles.txtLink}>{"View Direction"}</Text>
                    </View>
                </View>
                <View style={[styles.viewOverView, { marginTop: 20 }]}>
                    <Image style={styles.imgIcon} source={Images.document}></Image>
                    <Text style={styles.txtDetail}>{`All the fun stuff happening at B4 Apartment. Guys feel free to post anything and everything. Happy to share all the pet related and maintenance stuff. BTW monthlly outing to be held every last saturday. Cheers 🍺`}</Text>
                </View>
            </View>
        )
    }
    const ratingView = () => {
        return (
            <View style={{ margin: 20 }}>

                <View style={styles.ratingView}>
                    <Image style={styles.imgPeople} source={Images.people}></Image>
                    <Text style={styles.txtRate}>{strings.rateAndReview}</Text>
                    <Text style={styles.txtLabelRate}>{strings.shareYourExpMsg}</Text>
                    <Rating
                        type='custom'
                        ratingCount={5}
                        imageSize={25}
                        startingValue={0}
                        style={{ backgroundColor: Colors.opacityColor, marginTop: 5 }}
                        ratingBackgroundColor={Colors.opacityColor3}
                        ratingColor={"#FFB91B"}
                        tintColor={Colors.theme}
                    />
                </View>

            </View>
        )
    }
    const ratingReview = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.txtRateReviewLabel}>{strings.ratingAndReview}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{strings.viewAll}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flex: 0.2, marginTop: 5 }}>
                        <Text style={styles.txtRatingReview}>{"4.4"}</Text>
                        <Rating
                            type='custom'
                            ratingCount={5}
                            imageSize={8}
                            startingValue={4.5}
                            style={{ backgroundColor: Colors.opacityColor, alignSelf: "flex-start", marginVertical: 5 }}
                            ratingBackgroundColor={Colors.opacityColor3}
                            ratingColor={"#FFB91B"}
                            tintColor={Colors.theme}
                        />
                        <Text style={styles.txtTotalRating}>{"1,24,954"}</Text>
                    </View>
                    <View style={{ flex: 0.8, marginTop: 20, }}>
                        {ratingData.map((item, index) => {
                            return (
                                <View keyExtractor={index} style={{ flexDirection: "row", flex: 1, }}>
                                    <Text style={[styles.txtTotalRating, { flex: 0.05, marginTop: 0 }]}>{item.value}</Text>
                                    <PercentageBar
                                        height={6}
                                        backgroundColor={Colors.opacityColor2}
                                        completedColor={Colors.primaryViolet}
                                        percentage={item.percentage}
                                    />
                                </View>
                            )
                        })}

                    </View>
                </View>
                <View style={styles.viewLine1}></View>
                <FlatList
                    data={ratingReviewData}
                    renderItem={renderReview}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >

                </FlatList>

            </View>
        )
    }
    const onViewRef = React.useRef(({ viewableItems, changed }) => {

        setItem(categoryData[changed[0].index])
    })
    const viewConfigRef = React.useRef({ viewAreaCoveragePercentThreshold: 50 })
    const renderReview = ({ item, index }) => {
        return (
            <View style={styles.renderView}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", margin: 10 }}>
                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                        <Image style={{ height: 24, width: 24, borderRadius: 12, alignSelf: "center" }} source={item.image}></Image>
                        <View style={{ marginLeft: 10 }}>
                            <Text style={styles.txtName}>{item.name}</Text>
                            <Rating
                                type='custom'
                                ratingCount={5}
                                imageSize={8}
                                startingValue={3}
                                style={{ backgroundColor: Colors.opacityColor, alignSelf: "flex-start", marginTop: 0 }}
                                ratingBackgroundColor={Colors.opacityColor2}
                                ratingColor={Colors.white}
                                tintColor={Colors.theme}
                            />
                        </View>
                    </View>
                    <Text style={styles.txtTime}>{item.time}</Text>
                </View>
                <Text style={styles.txtItemRate}>{item.review}</Text>
            </View>
        )
    }
    const eventView = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Events (02)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{strings.viewAll}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                {
                    eventData.map((item, index) => {
                        return (
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <View>
                                    <Image style={{ height: 48, width: 48, borderRadius: 4 }} source={item.image}></Image>
                                    <View style={styles.roundView}>
                                        <EventIcon fillColor={Colors.primaryViolet} width={10} height={10} />

                                        {/* <Image style={styles.eventIcon} source={Images.placeIcon}></Image> */}
                                    </View>
                                </View>
                                <View style={{ marginLeft: 20, alignSelf: "center" }}>
                                    <Text style={styles.txtEventName}>{item.eventName}</Text>
                                    <Text style={styles.txtEventTime}>{item.evnetTime}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
    const followersView = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Followers (21)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{strings.viewAll}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                <FlatList
                    key={'_'}
                    keyExtractor={item => "_" + item.id}
                    data={imgPersons}
                    style={{ height: 150, marginTop: 20 }}
                    showsVerticalScrollIndicator={false}
                    numColumns={7}
                    initialNumToRender={5}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity key={index} style={{ margin: 6, flexDirection: "row" }}>
                                {index != imgPersons.length - 1 ? <Image style={styles.imgView} source={item.image}></Image>

                                    : <TouchableOpacity style={styles.roundViewFollowers}>
                                        <Text style={styles.txtMore}>{"+5"}</Text>
                                    </TouchableOpacity>
                                }
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            </View>
        )
    }
    const viewImage = ({ item, index }) => {
        return (
            <View key={index}>
                {index % 2 == 0 ?
                    <View>
                        <TouchableOpacity>
                            <Image style={{ width: 105, height: 77, marginBottom: 5 }} source={item.image}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image style={{ width: 105, height: 77 }} source={item.image}></Image>
                        </TouchableOpacity>
                    </View> :
                    <TouchableOpacity>
                        <Image style={{ width: 105, height: 158, marginHorizontal: 5 }} source={item.image}></Image>
                    </TouchableOpacity>}
            </View>
        )
    }
    const photoView = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={[styles.txtRateReviewLabel, { textTransform: "uppercase" }]}>{"Photos (24)"}</Text>
                    <TouchableOpacity style={{ flexDirection: "row" }}>
                        <Text style={styles.txtLink}>{strings.viewAll}</Text>
                        <Image style={styles.imgRight} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>

                <FlatList
                    data={photoData}
                    key={'#'}
                    keyExtractor={item => "#" + item.id}
                    horizontal
                    style={{ marginTop: 20 }}
                    showsHorizontalScrollIndicator={false}
                    renderItem={viewImage}
                >

                </FlatList>
            </View>
        )
    }
    const onCategoryClick = (index, item) => {
        setItem(item);
        flatlistRef.scrollToIndex({
            animated: true,
            index: index,
        })

    }
    const renderList = ({ item, index }) => {
        return (
            <View style={{ flex: 1 }}>
                {index == 0 && overView()}
                {index == 1 && ratingView()}
                {index == 3 && eventView()}
                {index == 4 && followersView()}
                {index == 5 && photoView()}
                {index == 2 && ratingReview()}
            </View>
        )
    }
    const headerView = () => {
        return (
            <View style={{
                // height: headerHeight, position: 'absolute',
                // width: SCREEN_WIDTH,
                // top: 0,
                // left: 0,
                // zIndex: 9999
            }}>
                <View>
                    <EventDetailHeader
                        onBackPress={backPress}
                        image={Images.dogIcon}
                        isMenu={false}
                        isShare={true}
                        onMoreClick={() => alert("hhhe")}
                        title={""}
                    >
                    </EventDetailHeader>
                    <Image style={styles.imgBedIcon} source={Images.doggyBedIcon} />
                </View>
                <Text style={styles.txtTitle}>{"Doggy Bed Day Care"}</Text>
                <View style={styles.viewRating}>
                    <Text style={styles.txtRating}>{4.4}</Text>
                    <Rating
                        type='custom'
                        ratingCount={5}
                        imageSize={20}
                        startingValue={4.5}
                        readonly={true}
                        style={{ backgroundColor: Colors.opacityColor, marginLeft: 10 }}
                        ratingBackgroundColor={Colors.opacityColor3}
                        ratingColor={"#FFB91B"}
                        tintColor={Colors.theme}
                    />
                </View>
                <LinearGradient colors={Colors.btnBackgroundColor} style={{ height: 40, margin: 20, borderRadius: 4 }} >
                    <TouchableOpacity style={styles.viewBtnFollow}>
                        <Image style={styles.imgPetIcon} source={Images.petProfileTab}></Image>
                        <Text style={styles.txtFollow}>{"Follow"}</Text>
                    </TouchableOpacity>
                </LinearGradient>
                {categoryView()}
            </View>
        )
    }
    const categoryView = () => {
        return (
            <View style={{ height: 38 }}>
                <FlatList
                    data={categoryData}
                    horizontal
                    style={{ marginHorizontal: 20, marginTop: 10 }}
                    renderItem={renderCategory}
                >
                </FlatList>
                <View style={styles.viewHorizontalLine}></View>
            </View>
        )
    }
    const scrollHeader = () => {
        return (
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 24 }}>
                <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10, marginLeft: 16 }}>
                    <TouchableOpacity onPress={() => setFlag(true)}>
                        <Image style={{ height: 20, width: 20, tintColor: Colors.white }} source={Images.backArrow}></Image>
                    </TouchableOpacity>
                    <Image style={{ height: 38, width: 38, borderRadius: 19, marginLeft: 22 }} source={Images.doggyBedIcon}></Image>
                    <View>
                        <Text style={styles.txtTitleScroll}>{"Doggy Bed Day Care"}</Text>
                        <View style={styles.viewRatingScroll}>
                            <Text style={styles.txtRatingScroll}>{4.4}</Text>
                            <Rating
                                type='custom'
                                ratingCount={5}
                                imageSize={10}
                                startingValue={4.5}
                                readonly={true}
                                style={{ backgroundColor: Colors.opacityColor, marginLeft: 10 }}
                                ratingBackgroundColor={Colors.opacityColor3}
                                ratingColor={"#FFB91B"}
                                tintColor={Colors.theme}
                            />
                        </View>
                    </View>
                    <Image style={{ height: 20, width: 20, tintColor: Colors.white, marginLeft: 80 }} source={Images.share}></Image>

                </View>
            </View>
        )
    }
    const onScroll = (event) => {
        setTimeout(() => {
            setFlag(false)
        }, 50);
    }
    return (
        <View style={styles.container}>
            <View style={{ backgroundColor: "#1D1233", marginVertical: 10, flex: 1 }}>
                {overView()}
                {ratingView()}
                {eventView()}
                {followersView()}
                {photoView()}
                {ratingReview()}
            </View>
            {/* <FlatList
                        data={["overView", "ratingView", "", "eventView", "followersView", "photoView"]}
                        renderItem={renderList}
                        ref={flatList1 => { setFlatListRef(flatList1) }}
                        onViewableItemsChanged={onViewRef.current}
                       // ListHeaderComponent={headerView}
                        showsVerticalScrollIndicator={false}
                        onScroll={onScroll}
                        viewabilityConfig={viewConfigRef.current}
                    >
                    </FlatList>
                </View> :
                <View style={{ marginVertical: 0, flex: 1 }}>
                                <StatusBar hidden />

                    {scrollHeader()}
                    {categoryView()}
                    <FlatList
                        data={["overView", "ratingView", "", "eventView", "followersView", "photoView"]}
                        renderItem={renderList}
                        style={{backgroundColor: "#1D1233",marginTop:16}}
                        ref={flatList1 => { setFlatListRef(flatList1) }}
                        onViewableItemsChanged={onViewRef.current}
                        viewabilityConfig={viewConfigRef.current}
                    >
                    </FlatList>
                </View>
             */}


        </View>
    )
}
export default ProfileTab;
