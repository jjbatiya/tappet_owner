import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../../Resources/Colors';
import { Fonts } from '../../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.black
    },
    linearGradient: {
        flex: 1
    },
    imgBedIcon: {
        height: 100, width: 100, alignSelf: "center",
        borderRadius: 50, position: "absolute", bottom: -40,
        borderWidth: 4, borderColor: Colors.theme
    },
    txtTitle: {
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular,
        fontSize: 28, textAlign: "center", alignSelf: "center",
        lineHeight: 35, letterSpacing: 0.2, color: Colors.white,
        marginTop: responsiveHeight(5)
    },
    txtRating: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "500", textAlign: "center", opacity: 0.6,
        fontSize: 14, lineHeight: 18.23, color: Colors.white
    },
    viewRating: { flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 20 },
    //follow btn
    txtFollow: {
        fontSize: 14, lineHeight: 18.23, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular, color: Colors.white,
        textAlign: "center", alignSelf: "center", marginLeft: responsiveWidth(2)
    },
    imgPetIcon: { height: 15, width: 15, tintColor: Colors.white, alignSelf: "center" },
    viewBtnFollow: { flexDirection: "row", alignItems: "center", justifyContent: "center", flex: 1 },
    txtCategory: {
        fontWeight: "500", opacity: 0.6,
        fontFamily: Fonts.DMSansRegular, fontSize: 12, lineHeight: 15.62, color: Colors.white
    },
    txtSelectCategory: {
        fontWeight: "500",
        fontFamily: Fonts.DMSansRegular, fontSize: 12, lineHeight: 15.62, color: Colors.white
    },
    viewLine: {
        flex: 1,
        height: 1,
        backgroundColor: Colors.white,
        marginTop: 10
    },
    viewHorizontalLine: { height: 1, backgroundColor: Colors.white, opacity: 0.15 },
    //overView
    viewOverView: {
        flexDirection: "row",
    },
    imgIcon: {
        height: 17, width: 17,
        tintColor: Colors.white
    },
    txtDetail: {
        fontSize: 14, fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, color: Colors.white, marginLeft: responsiveWidth(4)
    },
    txtLink: {
        fontSize: 14, fontFamily: Fonts.DMSansRegular,
        lineHeight: 18.23, color: Colors.primaryViolet, marginLeft: responsiveWidth(4)
    },
    //rating
    txtRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 16, lineHeight: 22, textAlign: "center", color: Colors.white,
        marginTop: 5
    },
    txtLabelRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "400",
        fontSize: 14, lineHeight: 22, textAlign: "center", color: Colors.white,
        opacity: 0.6
    },
    ratingView: { alignSelf: "center", alignItems: "center", justifyContent: "center", },
    imgPeople: { height: 48, width: 48, borderRadius: 24 },
    txtRateReviewLabel: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 14, lineHeight: 18.23, textAlign: "center", color: Colors.white,

    },
    imgRight: {
        height: 12, width: 12, marginLeft: 7,
        tintColor: Colors.primaryViolet, alignSelf: "center"
    },
    txtRatingReview: {
        fontFamily: Fonts.DMSansRegular, fontWeight: "400",
        fontSize: 26, lineHeight: 33.54, color: Colors.white,
        marginTop: 10
    },
    txtTotalRating: {
        fontFamily: Fonts.DMSansRegular, fontWeight: "400",
        fontSize: 10, lineHeight: 13.02, color: Colors.white,
        marginTop: 3, opacity: 0.7
    },
    viewLine1: { height: 1, flex: 1, backgroundColor: Colors.white, opacity: 0.1, marginVertical: 15 },
    renderView: {
        backgroundColor: Colors.theme, width: responsiveWidth(55),
        height: 112, borderRadius: 4, marginRight: 10
    },
    txtName:{
        fontFamily: Fonts.DMSansRegular, fontWeight: "700",
        fontSize: 10, lineHeight: 13.02, color: Colors.white,
        marginTop: 3, opacity: 0.7
    },
    txtTime:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:10,lineHeight:13.02,opacity:0.4
    },
    txtItemRate:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:12,lineHeight:15.62,
        margin:10
    },
    //event view
    roundView:{
        position: "absolute", bottom: -7,
        right: -7, height: 20, width: 20,
        borderWidth:1,borderColor:'#0C0024',
        backgroundColor:Colors.white,
        borderRadius:10,alignItems:"center",justifyContent:"center"
    },
    eventIcon:{height:10,width:10,tintColor:Colors.primaryViolet},
    txtEventName:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:14,lineHeight:18.23,letterSpacing:0.1
        
    },
    txtEventTime:{
        fontWeight:"400",fontFamily:Fonts.DMSansRegular,color:Colors.white,
        fontSize:12,lineHeight:15.62,letterSpacing:0.1,opacity:0.6
    },
    //followers view
    roundViewFollowers: {
        height: 32, width: 32, backgroundColor: Colors.theme, alignItems: "center",
        justifyContent: "center", borderRadius: 20,

    },
    imgView: { height: 32, width: 32, borderRadius: 20 },
    txtMore: {
        fontSize: 9,
        fontWeight: "700",
        lineHeight: 11.72,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular

    },

    //scrolble header
    txtTitleScroll: {
        fontWeight: "700",
        fontFamily: Fonts.DMSansRegular,
        fontSize: 16, textAlign: "center", alignSelf: "center",
        lineHeight: 22,  color: Colors.white,marginLeft:12
      
    },
    viewRatingScroll: { flexDirection: "row", alignItems: "center",marginLeft:12},
    txtRatingScroll: {
        fontFamily: Fonts.DMSansRegular,
        fontWeight: "400",  
        fontSize: 10, lineHeight: 13.02, color: Colors.white
    },
   
})