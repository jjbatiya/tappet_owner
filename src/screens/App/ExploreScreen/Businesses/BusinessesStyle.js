import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },

    contentView: {
        //flex: 1 ,
        marginLeft: responsiveWidth(4),
        marginVertical: responsiveHeight(2)
    },
    recommendedText: {
        fontWeight: "700",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    borderView: {
         height: 60, width: 60,
        // borderRadius: 40, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white
    },
    cameraStyle: {
        height: 28, width: 28, position: "absolute",
        tintColor: Colors.white, alignSelf: "center", resizeMode: "contain"
    },
    labelText: {
        fontWeight: "400", fontSize: 10, lineHeight: 13.02, color: Colors.white,
        alignSelf: "center", marginVertical: responsiveHeight(1), 
        fontFamily: Fonts.DMSansRegular,
        opacity:0.6
    },
    titleText: {
        fontSize: 14, fontWeight: "700", lineHeight: 18.23, color: Colors.white,
        textTransform: "uppercase", fontFamily: Fonts.DMSansRegular
    },

    //Title view and right arrow
    categoryView: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", height: 25,marginRight: responsiveWidth(4), },
    rightView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    rightText: {
        fontSize: 12, fontWeight: "500", lineHeight: 15.62, color: '#EA8FFA', fontFamily: Fonts.DMSansRegular
    },
    rightArrow: { width: 10, height: 10, tintColor: '#EA8FFA', marginLeft: 10 },

    //view dogImage and content view
    mainContent: {
        height: 160, width: responsiveWidth(70), marginRight: responsiveWidth(2.5),
        marginVertical: responsiveHeight(1)
    },
    mainImgStyle: { height: 160, width: responsiveWidth(70), borderRadius: 4, resizeMode: 'cover' },
    topLabel: { position: "absolute", alignItems: "center", justifyContent: "center" },
    topLabelInner: {
        borderRadius: 2, backgroundColor: Colors.white,
        opacity: 0.2, borderWidth: 1, margin: 8, height: 20, width: 72,
        borderColor: Colors.white
    },
    topLabelText: {
        position: "absolute", fontSize: 9, lineHeight: 11.72,
        fontWeight: "700", color: Colors.white, alignSelf: "center", opacity: 0.6,
        margin: 8, textTransform: "uppercase", fontFamily: Fonts.DMSansRegular
    },
    bottomView: { position: "absolute", bottom: 10, left: 10, flexDirection: "row", alignItems: "center" },
    bedIcon: { height: 40, width: 40, borderRadius: 20 },
    txtTitle: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    imgStar: { height: 9, width: 9.6 },
    txtRate: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62,
        color: Colors.white, opacity: 0.7, marginLeft: 3, marginRight: 10, fontFamily: Fonts.DMSansRegular
    },
    imgPlace: { height: 9, width: 9.6, tintColor: Colors.white, opacity: 0.3 },
    txtPlace: {
        fontWeight: "400", fontSize: 12, lineHeight: 15.62, marginLeft: 3,
        color: Colors.white, opacity: 0.6, fontFamily: Fonts.DMSansRegular
    }

})