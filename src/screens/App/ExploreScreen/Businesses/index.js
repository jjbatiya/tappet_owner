import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, Dimensions, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../../../Resources/Colors';
import { strings } from '../../../../Resources/Strings';
import { styles } from './BusinessesStyle';
import { Images } from '../../../../Resources/Images';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
const { width, height } = Dimensions.get("window")
import { PlaceIcon } from '../../../../utils/svg/PlaceIcon';
import CometChatIncomingCall from '../../../../Components/Calls/CometChatIncomingCall';


const catData = [
    {
        text: "Pet Boarding",
        image: Images.Boarding
    },
    {
        text: "Day Care",
        image: Images.GroupIcon
    },
    {
        text: "Pet Sitter",
        image: Images.PetSitter
    },
    {
        text: "Pet Walker",
        image: Images.PetWalker
    },
    {
        text: "Grooming",
        image: Images.Grooming
    },
    {
        text: "Dog Training",
        image: Images.PetTrainer
    },
    {
        text: "Dog Walker",
        image: Images.PetWalker
    },
    {
        text: "Pet Stores",
        image: Images.PetStore
    },
    
]
const data = [
    {
        title: "Most Popular",
        id: 1,
        data: [
            {
                image: Images.dogIcon,
                category: "DOG SItter",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 1
            },
            {
                image: Images.dogIcon1,
                category: "Groomer",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 2
            }
        ]
    },
    {
        title: "near You",
        id: 2,
        data: [
            {
                image: Images.dogIcon1,
                category: "DOG SItter",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 1
            },
            {
                image: Images.dogIcon2,
                category: "Groomer",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 2
            }
        ]
    },
    {
        title: "Top rated",
        id: 3,
        data: [
            {
                image: Images.dogIcon3,
                category: "DOG SItter",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 1
            },
            {
                image: Images.dogIcon2,
                category: "Groomer",
                title: "Doggy Bed Day Care",
                doggyBedIcon: Images.doggyBedIcon,
                rate: 47,
                location: "42 Park Avenue, NY",
                id: 2
            }
        ]
    }
]
const Businesses = (props) => {
  
    const renderCategory = ({ item, index }) => {
        return (
            <TouchableOpacity style={{ marginRight: 15, height: 115 }}>
                <View style={{ alignItems: "center", justifyContent: "center" }}>
                    <View style={styles.borderView}></View>
                    <Image source={item.image} style={styles.cameraStyle}></Image>
                </View>
                <Text style={styles.labelText}>{item.text}</Text>

            </TouchableOpacity>
        )
    }
    const renderItem = ({ item, index }) => {
        return (
            <View style={{ marginVertical: 10 }}>
                <View style={styles.categoryView}>
                    <Text style={styles.titleText}>
                        {item.title}
                    </Text>
                    <TouchableOpacity style={styles.rightView}>
                        <Text style={styles.rightText}>{strings.viewAll}</Text>
                        <Image style={styles.rightArrow} source={Images.rightArrow}></Image>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={item.data}
                    horizontal
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    const renderSubView = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate(strings.cutomHeaderScreen)}
                style={styles.mainContent}>
                <Image style={styles.mainImgStyle} source={item.image}></Image>
                <View style={styles.topLabel}>
                    <View style={styles.topLabelInner} />
                    <Text style={styles.topLabelText}>{item.category}</Text>
                </View>
                <LinearGradient style={{
                    position: "absolute",
                    height: responsiveHeight(12),
                    width: width, bottom: 0
                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={styles.bottomView}>
                    <Image style={styles.bedIcon} source={item.doggyBedIcon}></Image>
                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={styles.txtTitle}>{item.title}</Text>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image style={styles.imgStar} source={Images.fillStar}></Image>
                            <Text style={styles.txtRate}>{item.rate}</Text>
                            <PlaceIcon width={10} height={10} fill={"white"} />
                            {/* <Image style={styles.imgPlace} source={Images.placeIcon}></Image> */}
                            <Text style={styles.txtPlace}>{item.location}</Text>
                        </View>

                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    
    return (
        <View style={styles.container}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.linearGradient}
            >

                <View style={styles.contentView}>
                    <Text style={styles.recommendedText}>{strings.categories}</Text>
                    <FlatList
                        data={catData}
                        horizontal
                        style={{ marginTop: 10, height: 115, marginBottom:20}}
                        showsHorizontalScrollIndicator={false}
                        renderItem={renderCategory}
                    />
                    <FlatList
                        data={data}
                        renderItem={renderItem}
                        showsVerticalScrollIndicator={false}
                        style={{ marginBottom: 140 }}
                    />
                </View>
            </LinearGradient>
           
        </View>
    )
}
export default Businesses