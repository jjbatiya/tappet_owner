import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { strings } from '../../../Resources/Strings';
import { Colors } from '../../../Resources/Colors';
import { Fonts } from '../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    txtsignin: {
        // position: 'absolute',
        //top: 98,
        fontSize: 32,
        color: Colors.white,
        lineHeight: 40,
        alignItems: "center",
        textAlign: 'center',
        letterSpacing: 0.02,
        fontWeight: "400",
        fontFamily: Fonts.LobsterTwoRegular
    },
    txtlbl: {
        //position: 'absolute',
        // top: 146,
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'center',
        letterSpacing: 0.01,
        color: Colors.white,
        opacity: 0.6,
        marginVertical: responsiveHeight(1),
        width: 335,
        alignSelf: "center",
        fontFamily: Fonts.DMSansRegular
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.3,
        marginLeft:-8
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },

    emailIconStyle: { marginVertical: responsiveHeight(1) },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    txtlblRecoverStr: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    }

})