import React, { useState } from 'react';
import { View, Text, SafeAreaView, Modal, Share, KeyboardAvoidingView, ScrollView, Platform, NativeModules, Linking } from 'react-native';
import { styles } from './ResetPasswprdStyle';
import LinearGradient from 'react-native-linear-gradient';
var FloatingLabel = require('react-native-floating-labels');
import HeaderView from '../../../Components/HeaderView';
import CommonButton from '../../../Components/CommonButton';
import * as Constant from '../../../utils/Constant';
import { strings } from '../../../Resources/Strings';
import { Colors } from '../../../Resources/Colors';
import { Images } from '../../../Resources/Images'
import { responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../Components/CommonStatusBar';
import { EmailIcon } from '../../../utils/svg/EmailIcon';
import { openInbox } from "react-native-email-link";
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { apiCall, storeData } from '../../../utils/helper';
import Toast from 'react-native-toast-message';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Loader from '../../../Components/Loader';
import { validationMsg } from '../../../Resources/ValidationMsg';
import { toastConfig } from '../../../utils/ToastConfig';

const ResetPassword = (props) => {
    
    const [isEmailFocus, setEmailFocus] = useState(false)
    const [isVisible, setVisible] = useState(false)
    const [u_email, setEmail] = useState(props.route.params.u_email)
    const [isshowing, setShowing] = useState(false)

    const onBlur = (flag) => {
        if (flag == "email")
            setEmailFocus(!isEmailFocus)
    }
   const onSignClick = () => {
        if (u_email.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.email)
        else if (strings.emailRegrex.test(u_email) === false) {
            //  Toast.show(validationMsg.email, Toast.SHORT);
            showMessage(strings.error, strings.msgTag, validationMsg.emailRegrex)
        }

        else {
            apiCallReset();
        }
    }
 
    const apiCallReset = async () => {
        setShowing(true)
        var obj = {
            u_email: u_email,
            device_type: Platform.OS == "ios" ? "ios" : "android"
        }

        await apiCall(obj, apiUrl.password_email).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                storeData(res.result)
                setTimeout(() => {
                    setShowing(false)
                    setVisible(true)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }

        })
    }
    const backClick = () => {
        props.navigation.goBack(null);
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const openMail = () => {
        setVisible(false)
        openInbox();
        //linkCreate();
       // props.navigation.navigate("newPassword")
    }
    const linkCreate = async () => {
        const link = await dynamicLinks().buildLink({
            link: "https://testquestion.com/id?tid=10",
            // domainUriPrefix is created in your Firebase console
            domainUriPrefix: 'https://tappetowner.page.link',
            // optional setup which updates Firebase analytics campaign
            // "banner". This also needs setting up before hand
            // analytics: {
            //   campaign: 'banner',
            // },
            android: {
                packageName: 'com.tappetowner'
            },
            ios: {
                bundleId: 'com.tappetOwner'
            },
            social: {
                title: "This link shareable",
                imageUrl: "https://live.iiiem.in/storage/video/2_1610022256_63095291f56082415a19ff4005618035.jpg",
                descriptionText: "decsription data in react native"
            }
        });
        console.log(link)
        onShare(link)
    }
    const onShare = async (link) => {
        const link1 = await dynamicLinks().buildShortLink({
            link: link,
            domainUriPrefix: 'https://tappetowner.page.link',
        }, dynamicLinks.ShortLinkType.UNGUESSABLE);

        console.log("short limk", link1)
        try {
            const result = await Share.share({
                message: link1,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };
    const emailModal = () => {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={isVisible}
                onRequestClose={() => { console.log("Modal has been closed.") }}>
                {/*All views of Modal*/}
                <View style={styles.modal}>
                    <EmailIcon width={32} height={32} />
                    <Text style={styles.txtlblCheckEmail}>{strings.emailChecklbl}</Text>
                    <Text style={styles.txtlblRecoverStr}>{strings.emailDesc}</Text>
                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={Constant.ColorTheme.btnBackgroundColor}
                        btnText={styles.btnText}
                        viewStyle={{ marginVertical: 10, width: 219 }}
                        onPress={openMail}
                        text={strings.openEmailText}
                    />
                    <Text onPress={() => setVisible(false)} style={styles.txtCancel}>{strings.cancelText}</Text>

                </View>
            </Modal>
        )
    }
    return (
        <View style={{ flex: 1 }}>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}>
                    <ScrollView keyboardShouldPersistTaps='handled'>
                        <SafeAreaView>

                            <HeaderView onPress={backClick} />
                            <View style={{ marginTop: 10, marginHorizontal: responsiveWidth(3) }}>
                                <Text style={styles.txtsignin}>{strings.resetPasswordSmall}</Text>
                                <Text style={styles.txtlbl}>{strings.resetDescLabel}</Text>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    value={u_email}
                                    style={isEmailFocus ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("email")}
                                    onChangeText={(text) => setEmail(text)}
                                    onBlur={() => onBlur("email")}
                                >{strings.emailPlaceHolder}</FloatingLabel>
                                <CommonButton
                                    btnlinearGradient={styles.btnlinearGradient}
                                    colors={Colors.btnBackgroundColor}
                                    btnText={styles.btnText}
                                    viewStyle={{ marginTop: 40 }}
                                    onPress={onSignClick}
                                    text={strings.instructionsText}
                                />
                            </View>
                            {emailModal()}

                        </SafeAreaView>
                    </ScrollView>
                </KeyboardAvoidingView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default ResetPassword;