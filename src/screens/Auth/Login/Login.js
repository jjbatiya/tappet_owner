import React, { useState } from 'react';
import { SafeAreaView, Text, View, ActivityIndicator, Dimensions, TouchableOpacity, ScrollView, Platform } from 'react-native';
const { width, height } = Dimensions;
import LinearGradient from 'react-native-linear-gradient';
var FloatingLabel = require('react-native-floating-labels');
import { styles } from './LoginStyle';
import AppleBtn from '../../../Components/AppleBtn';
import GoogleBtn from '../../../Components/GoogleBtn';
import CommonButton from '../../../Components/CommonButton';
import { connect } from 'react-redux';
import RowLine from '../../../Components/RowLine';
import { strings } from '../../../Resources/Strings';
import { Colors } from '../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { loginUser } from '../../../Redux/actions/action'
import CommonStatusBar from '../../../Components/CommonStatusBar';
import { getUserData, storeData, getFirebaseToken, comeChatLogin, storeChatUserData } from '../../../utils/helper';

import { validationMsg } from '../../../Resources/ValidationMsg';
import Loader from '../../../Components/Loader';
import Toast from 'react-native-toast-message';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { comeChatUserTag, comeChat_apiKey } from '../../../utils/Constant';
import { toastConfig } from '../../../utils/ToastConfig';

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isEmailFocus: false,
            isPasswordFocus: false,
            email: "",
            password: "",
            isshowing: false,
            u_user_type: '1',
            u_social_id: "",
            device_token: "",
            device_type: Platform.OS == "ios" ? "ios" : "android"
        }
    }
    async componentDidMount() {

        
        await getFirebaseToken().then(res => {
            this.setState({ device_token: res })
        })

    }
    onBlur = (flag) => {
        if (flag == "email") {
            this.setState({
                isEmailFocus: !this.state.isEmailFocus
            })
        }
        else {
            this.setState({
                isPasswordFocus: !this.state.isPasswordFocus
            })
        }
    }
    errorMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    onSignClick = () => {
        const { email, password } = this.state
        if (email.trim() == "")
            this.errorMessage("error", "Message", validationMsg.email)
        else if (strings.emailRegrex.test(email.trim()) === false) {
            this.errorMessage("error", "Message", validationMsg.emailRegrex)
        }
        else if (password.trim() == "") {
            this.errorMessage("error", "Message", validationMsg.password)
        }
        else {
            this.setState({ isshowing: true })
            this.loginApi();
        }
    }

    loginApi = async () => {
        const obj = {
            u_email: this.state.email,
            u_password: this.state.password,
            u_user_type: this.state.u_user_type,
            u_social_id: this.state.u_social_id,
            device_token: this.state.device_token,
            device_type: this.state.device_type
        }
        const { dispatch } = this.props;
        await dispatch(loginUser(obj));
        const { data } = this.props;
        if (data.status) {

            await comeChatLogin(data.result.u_id).then(res => {
                if (res.status == true) {
                    this.errorMessage("success", "Message", data.message)
                    storeData(data.result)
                    storeChatUserData(res.response)
                    this.props.navigation.replace(strings.TabNavigation)
                }
                else {
                    this.createUserComeChat(data.result);
                }
            })
            await this.setState({ isshowing: false })

        }
        else {

            setTimeout( () => {
                 this.errorMessage("error", "Message", data.message)
                this.setState({ isshowing: false })
                if (data.code == 1000||data.code == 2000) {
                    var obj={
                        u_email:this.state.email
                    }
                    storeData(obj)
                    this.props.navigation.navigate(strings.verifyEmailScreen,{isCall:true})
                }
                

            }, 1000);
        }
    }
    createUserComeChat = (data) => {
        var uid = comeChatUserTag + data.u_id;
        var user = new CometChat.User(uid);
        user.setName(data.u_first_name);

        CometChat.createUser(user, comeChat_apiKey).then(
            user => {
                storeData(data)
                storeChatUserData(user)
                this.props.navigation.replace(strings.TabNavigation)
            }, error => {
                console.log("error", error);
            }
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.isshowing} style={{ alignItems: 'center' }} ></Loader>

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.linearGradient}
                >

                    <CommonStatusBar />
                    <ScrollView keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} style={{ flex: 1, marginHorizontal: responsiveWidth(3) }}>

                        <View style={{ marginTop: "30%" }} >
                            <Text style={styles.txtsignin}>{strings.signIn}</Text>
                            <Text style={styles.txtlbl}>{strings.loginLabel}</Text>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                autoCapitalize='none'

                                style={this.state.isEmailFocus ? styles.focusforminput : styles.formInput}
                                onFocus={() => this.onBlur("email")}
                                onChangeText={(value) => this.setState({ email: value })}
                                onBlur={() => this.onBlur("email")}
                            >{strings.emailPlace}</FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                onChangeText={(value) => this.setState({ password: value })}
                                style={this.state.isPasswordFocus ? styles.focusforminput : styles.formInput}
                                password
                                onFocus={this.onBlur}
                                onBlur={this.onBlur}
                            >{strings.password}</FloatingLabel>
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={Colors.btnBackgroundColor}
                                btnText={styles.btnText}
                                viewStyle={{ marginTop: 40 }}
                                onPress={this.onSignClick}
                                text={strings.signIn}
                            />
                            <Text style={styles.txtforget}>{strings.forgetText}
                                <Text onPress={() => this.props.navigation.navigate(strings.resetPasswordScreen,{u_email:this.state.email})} style={styles.txtReset}>{strings.resetPassword}</Text>
                            </Text>
                            <RowLine />
                            {Platform.OS == "ios" &&
                                <AppleBtn flag={"login"} navigation={this.props.navigation} />
                            }
                            <GoogleBtn flag={"login"} navigation={this.props.navigation} />
                            <View style={styles.bottomView}>
                                <Text style={[styles.txtforget, { top: 0 }]}>{strings.dontHaveText}
                                    <Text onPress={() => this.props.navigation.replace(strings.signUpScreen)} style={styles.txtReset}>{strings.signUp}</Text>
                                </Text>
                            </View>
                        </View>

                    </ScrollView>

                </LinearGradient>
                <Toast  config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
            </View>

        )
    }
}
function mapStateToProps(state) {
    return {
        data: state.reducer.data
    }
}
export default connect(mapStateToProps)(Login);
