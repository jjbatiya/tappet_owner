import React, { useState } from 'react';
import { View, SafeAreaView, Text, KeyboardAvoidingView, StatusBar, ScrollView, Modal, Image, TextInput, TouchableOpacity } from 'react-native';
import { styles } from './UploadPhotoStyle';
import LinearGradient from 'react-native-linear-gradient';
import * as Constant from '../../../../utils/Constant';
import HeaderView from '../../../../Components/HeaderView';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { Images } from '../../../../Resources/Images';

import CommonStatusBar from '../../../../Components/CommonStatusBar';
import { connect } from 'react-redux';
import { UploadImageIcon } from '../../../../utils/svg/UploadImageIcon';
import { apiCall, apiCallWithToken, getUserData, storeData, imageUploadApi } from '../../../../utils/helper';
import Toast from 'react-native-toast-message';
import Loader from '../../../../Components/Loader';
import { apiUrl } from '../../../../Redux/services/apiUrl';
import ImagePicker from 'react-native-image-crop-picker';
import { toastConfig } from '../../../../utils/ToastConfig';

class UploadPhoto extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            imagePath: "",
            isshowing: false,
            u_id: '',
            token: '',
            imageData: null
        }
    }
    componentDidMount() {
        getUserData().then(res => {
            this.setState({
                u_id: res.u_id,
                token: res.token
            })
        })
    }
    backClick = () => {
        this.props.navigation.goBack(null);
    }
    showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });

    }
    onSave = async (flag) => {
        if (flag == "skip") {
            this.props.navigation.replace(strings.TabNavigation)
            return
        }
        else {
            if (this.state.imagePath == "") {
                this.showMessage(strings.error, strings.msgTag, "Please Select User Image!")
                return
            }
            this.setState({ isshowing: true })

        }
        let data = new FormData();
        data.append("u_image", { type: this.state.imageData.type, uri: this.state.imageData.uri, name: this.state.imageData.fileName });
        data.append("u_id", this.state.u_id)
        await imageUploadApi(data, apiUrl.update_profile_picture, this.state.token).then(res => {
            if (res.status) {
                this.showMessage("success", "Message", res.message)
                var obj=res.result
                obj.token=this.state.token

                storeData(obj)
                setTimeout(() => {
                    this.setState({ isshowing: false })
                    this.props.navigation.replace(strings.TabNavigation)
                }, 1000);
            }
            else {
                this.showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    this.setState({ isshowing: false })
                }, 1000);
            }

        })

    }
    chooseFile = () => {
      
        ImagePicker.openPicker({
            compressImageQuality:0.7,
            cropping: true,
            width: 300,
            height: 400,
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                this.setState({
                    imagePath: image?.path,
                    imageData: obj

                })
            } catch (error) {

            }
        });
    };
    render() {
        return (
            <View style={{ flex: 1 }}>

                <LinearGradient
                    colors={Colors.screenBackgroundColor}
                    style={styles.container}
                >
                    <Loader loading={this.state.isshowing} style={{ alignItems: 'center' }} ></Loader>

                    <CommonStatusBar />

                    <SafeAreaView style={{ flex: 1 }}>

                        <HeaderView onSkip={() => this.onSave("skip")} isPhoto={true} onPress={this.backClick} text={strings.uploadPhoto} textSkip={strings.skip} />
                        <Text style={styles.txtlblStep}>{strings.stepStr2}</Text>
                        <View style={styles.mainView}>
                            <LinearGradient
                                colors={Constant.ColorTheme.btnBackgroundColor}
                                style={{ height: 4, flex: 0.5 }}
                            />
                            <View style={{ backgroundColor: "white", height: 4, flex: 0.5 }} />

                        </View>
                        <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                            ref={(ref) => Toast.setRef(ref)} />
                        <View style={styles.roundView}>
                            {this.state.imagePath == "" ?
                                <TouchableOpacity onPress={() => this.chooseFile()}>
                                    <View style={styles.imgUploadStyle}>
                                        <UploadImageIcon width={33.33} height={33.33} />
                                    </View>
                                    <Text style={styles.txtUploadText}>{strings.uploadPhoto}</Text>
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => this.chooseFile()} style={{ flex: 1 }}>
                                    <Image style={styles.profileStyle} source={{ uri: this.state.imagePath }}></Image>
                                    <View style={styles.editView} >
                                        <Image
                                            style={styles.imgEditStyle}
                                            source={Images.editIcon}
                                        />

                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={Colors.btnBackgroundColor}
                            btnText={styles.btnText}
                            viewStyle={styles.bottomView}
                            onPress={() => this.onSave("save")}
                            text={strings.saveProcessed}

                        />
                    </SafeAreaView>
                </LinearGradient>
            </View >

        )
    }
}
function mapStateToProps(state) {
    return {
        data: state.reducer.data
    }
}
export default connect(mapStateToProps)(UploadPhoto);