import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    txtlblStep: {
        alignSelf: "center", fontSize: 12,
        lineHeight: 15.62, fontWeight: "400", color: "white",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    mainView: {
        marginHorizontal: 20, marginTop: 10, marginBottom: 10, width: "100%", flexDirection: "row", height: 1
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    bottomView: {
        height: 50,
        width: "100%",
        alignSelf: "center",
        position: "absolute",
        bottom: 0,
        flex: 1,
        marginVertical: responsiveHeight(5)
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: "white",
        fontFamily:Fonts.DMSansRegular
    },
    roundView: {
        height: 195, width: 195, borderWidth: 1, borderColor: Colors.opacityColor2,
         alignSelf: "center",
        borderRadius: 114, marginTop: responsiveHeight(2),
        justifyContent: "center",
    },
    imgUploadStyle: {
        height: 33.33, width: 33.33,
        alignSelf: "center", borderRadius: 7
    },
    profileStyle: {
        height: 195, width: 195,
        alignSelf: "center", borderRadius: 7,
        borderRadius: 114
    },
    txtUploadText: {
        color: Colors.white, alignSelf: "center",
        lineHeight: 18.23, fontSize: 14, fontWeight: "400",
        marginTop: responsiveHeight(1), fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    editView: {
        width: 40, height: 40, backgroundColor: Colors.dialogBackground,
        position: 'relative', bottom: 50, left: 150,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: '#0C0024'
        ,
    },
    imgEditStyle: { height: 12, width: 12, tintColor: "white", alignSelf: "center" }

})