import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../Resources/Colors';
import { Fonts } from '../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    linearGradient: {
        flex: 1
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center"

    },
    txtsignin: {
        // position: 'absolute',
        //top: 98,
        fontSize: 32,
        color: Colors.white,
        lineHeight: 40,
        alignItems: "center",
        textAlign: 'center',
        letterSpacing: 0.02,
        fontWeight:"400",
        fontFamily:Fonts.LobsterTwoRegular,
        
    },
    txtlbl: {
        //position: 'absolute',
        // top: 146,
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'center',
        letterSpacing: 0.01,
        color:Colors.white,
        opacity: 0.6,
        marginVertical:responsiveHeight(1),
        fontFamily:Fonts.DMSansRegular
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft:-8,
        fontFamily:Fonts.DMSansRegular
    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular,
        opacity:0.3,
        marginLeft:-8

    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular

    },
    txtforget: {

        color: Colors.white,
        alignSelf: "center",
        marginTop:responsiveHeight(2),
        fontWeight:"400",
        lineHeight:15.62,
        fontFamily:Fonts.DMSansRegular,
        opacity:0.6,
        fontSize:12

    },
    txtReset: {
        color: Colors.white,
        lineHeight:15.62,
        fontFamily:Fonts.DMSansBold,
        fontSize:12

    },
    txtOR: {
        color: Colors.white,
        flex: 0.1,
        textAlign: "center",
        fontSize: 12,
        lineHeight: 15.62,
        fontWeight: "700",
    },
    bottomView: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
})