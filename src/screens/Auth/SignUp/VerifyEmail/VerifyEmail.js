import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, KeyboardAvoidingView, Platform, ScrollView, Animated, TouchableOpacity } from 'react-native';
import { styles } from './VerifyEmailStyle';
import LinearGradient from 'react-native-linear-gradient';
import HeaderView from '../../../../Components/HeaderView';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../Components/CommonButton';
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import Toast from 'react-native-toast-message';
import { validationMsg } from '../../../../Resources/ValidationMsg';
import { apiCall, getFirebaseToken, getUserData, storeData } from '../../../../utils/helper';
import { apiUrl } from "../../../../Redux/services/apiUrl";
import Loader from '../../../../Components/Loader';
import { toastConfig } from '../../../../utils/ToastConfig';

const VerifyEmail = (props) => {
    const [isCodeFocus, setFous] = useState(false)
    const [code, setCode] = useState("")
    const [device_token, setDeviceToken] = useState("");
    const [device_type] = useState(Platform.OS == "ios" ? "ios" : "android")
    const [u_email, setEmail] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [isCall]=useState(props.route.params?.isCall)
    useEffect(() => {
        getUserData().then(res => {
            setEmail(res.u_email)
            if(isCall==true)
                clickResend(res.u_email);
        })
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
    }, []);
    const onBlur = (flag) => {
        if (flag == "email")
            setFous(!isCodeFocus)
    }
    const backClick = () => {
        props.navigation.goBack(null);
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });

    }
    const onVerify = () => {
        if (code.trim().length == 0)
            showMessage(strings.error, strings.msgTag, validationMsg.code)
        else {
            verifyEmail()
        }
    }
    const verifyEmail = async () => {
        setShowing(true)
        var apiObj = {
            u_email: u_email,
            u_otp: code,
            device_token: device_token,
            device_type: device_type
        }
        await apiCall(apiObj, apiUrl.verifyEmail).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                storeData(res.result)
                closeShowing("1");
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                closeShowing("0");

            }
        })
    }
    const closeShowing = (flag) => {
        setTimeout(() => {
            setShowing(false)
            { flag == "1" && props.navigation.navigate(strings.createaccountScreen) }
        }, 1000);
    }
    const clickResend=async(email)=>{
        setShowing(true)
        var apiObj = {
            u_email: email,
        }
        await apiCall(apiObj, apiUrl.resendotp).then(res => {
            setShowing(false)

            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }
        })
    }
    return (

        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />

                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>
                <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                    ref={(ref) => Toast.setRef(ref)} />
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}>

                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' >
                        <SafeAreaView style={{ flex: 1, marginHorizontal: responsiveWidth(3) }}>
                            <HeaderView onPress={backClick} text={strings.verifyEmail} />
                            <Text style={styles.txtlblVerfiStr}>{strings.enterVerifyCode}</Text>
                            <Text style={styles.txtlblEmailStr}>{"Code is sent to " + u_email}</Text>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                style={isCodeFocus ? styles.focusforminput : styles.formInput}
                                onFocus={() => onBlur("email")}
                                keyboardType="numeric"
                                onChangeText={(value) => setCode(value)}
                                onBlur={() => onBlur("email")}
                            >{strings.confirmationcCode}</FloatingLabel>
                            <CommonButton
                                btnlinearGradient={styles.btnlinearGradient}
                                colors={Colors.btnBackgroundColor}
                                btnText={styles.btnText}
                                viewStyle={{ marginTop: responsiveHeight(10), marginHorizontal: responsiveWidth(1), marginBottom: responsiveHeight(3) }}
                                onPress={onVerify}
                                text={strings.verify}
                            />
                            <TouchableOpacity onPress={() => clickResend(u_email)} >
                                <Text style={styles.txtResendotp}>Resend OTP</Text>
                            </TouchableOpacity>

                        </SafeAreaView>
                    </ScrollView>
                </KeyboardAvoidingView>
            </LinearGradient>

        </View >
    )
}
export default VerifyEmail;