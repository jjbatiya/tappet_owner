import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily:Fonts.DMSansRegular


    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginHorizontal: responsiveWidth(1.5),
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular,
        opacity:0.3,
        marginLeft:-8

    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily:Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        marginHorizontal: responsiveWidth(1.5),
        alignItems: "center",
        justifyContent: "center"

    },
    txtlblVerfiStr: {
        alignSelf: "center", fontSize: 20, lineHeight: 22,
        fontWeight: '700', color: Colors.white, marginTop: 60, fontFamily: Fonts.DMSansRegular
    },
    txtlblEmailStr: {
        alignSelf: "center", fontSize: 14, lineHeight: 18.23,
        fontWeight: '400', color: Colors.white, marginTop: responsiveHeight(1),
        marginBottom: responsiveHeight(2),
        fontFamily: Fonts.DMSansRegular,
        opacity:0.6
    },
    txtResendotp: {
        textAlign: "center", alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        width: 295, opacity: 0.6
    }
})