import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  Platform,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
var FloatingLabel = require("react-native-floating-labels");
import { styles } from "./SignUpStyle";
import AppleBtn from "../../../Components/AppleBtn";
import GoogleBtn from "../../../Components/GoogleBtn";
import CommonButton from "../../../Components/CommonButton";
import RowLine from "../../../Components/RowLine";
import { strings } from "../../../Resources/Strings";
import { Colors } from "../../../Resources/Colors";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import CommonStatusBar from "../../../Components/CommonStatusBar";
import Toast from 'react-native-toast-message';
import { validationMsg } from "../../../Resources/ValidationMsg";
import { apiCall, getFirebaseToken, storeData } from '../../../utils/helper';
import { apiUrl } from "../../../Redux/services/apiUrl";
import Loader from "../../../Components/Loader";
import { toastConfig } from "../../../utils/ToastConfig";

const SignUp = (props) => {
  const [isEmailFocus, setEmailFocus] = useState(false);
  const [isPasswordFocus, setPasswordFocus] = useState(false);
  const [isConfiremPasswordFocus, setConfirmPasswordFocus] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [u_user_type] = useState("1");
  const [device_token, setDeviceToken] = useState("");
  const [device_type] = useState(Platform.OS == "ios" ? "ios" : "android")
  const [isshowing, setShowing] = useState(false);
  useEffect(() => {
    getFirebaseToken().then(res => {
      setDeviceToken(res)
    })
  }, []);
  const onBlur = (flag) => {
    if (flag == "email") setEmailFocus(!isEmailFocus);
    else if (flag == "password") setPasswordFocus(!isPasswordFocus);
    else setConfirmPasswordFocus(!isConfiremPasswordFocus);
  };
  const onSignClick = () => {
    if (email.trim() == "") {
      showMessage(strings.error, strings.msgTag, validationMsg.email)

    }
    else if (strings.emailRegrex.test(email.trim()) === false) {
      showMessage(strings.error, strings.msgTag, validationMsg.emailRegrex)
    } else if (password.trim() == "") {
      showMessage(strings.error, strings.msgTag, validationMsg.password)

    }
    else if (strings.passwordRegrex.test(password) === false) {
      showMessage(strings.error, strings.msgTag, validationMsg.passwordRegrex)
    } else if (password != confirmPassword) {
      showMessage(strings.error, strings.msgTag, validationMsg.confirmPassword)

    }
    else {
      checkSignUp()
    }


  };
  const showMessage = (type, title, message) => {
    Toast.show({
      type: type,
      text1: title,
      text2: message,
      autoHide: true,
      visibilityTime: 3000
    });
  }
  const checkSignUp = async () => {
    setShowing(true)
    var apiObj = {
      u_email: email.trim(),
      u_password: password,
      u_user_type: u_user_type,
      u_social_id: "",
      device_token: device_token,
      device_type: device_type

    }
    await apiCall(apiObj, apiUrl.register).then(res => {
      if (res.status) {
        if (res.code == 3000) {
          showMessage(strings.error, strings.msgTag, res.message)
          setShowing(false)

        }
        else {
          showMessage(strings.success, strings.msgTag, res.message)
          storeData(res.result)
          setTimeout(() => {
            setShowing(false)
            props.navigation.navigate(strings.verifyEmailScreen, { isCall: false });

          }, 1000);
        }
      }
      else {
        showMessage(strings.error, strings.msgTag, res.message)

        setTimeout(() => {
          setShowing(false)
          if (res.code == 1000||res.code == 2000) {
            props.navigation.navigate(strings.verifyEmailScreen, { isCall: true });

          }
          // else if (res.code == 2000) {
          //   storeData(res.result)
          //   setTimeout(() => {
          //     props.navigation.navigate("createaccount");
          //   }, 400);
          // }

        }, 1000);
      }


    })

  }
  return (
    <View style={styles.container}>

      <LinearGradient
        colors={Colors.screenBackgroundColor}
        style={styles.linearGradient}
      >

        <CommonStatusBar />
        <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

        <ScrollView keyboardShouldPersistTaps='handled' style={{ flex: 1, marginHorizontal: responsiveWidth(3) }}>

          <View style={{ marginTop: "30%" }}>
            <Text style={styles.txtsignin}>{strings.signUp}</Text>
            <Text style={styles.txtlbl}>{strings.signUpDesc}</Text>
            <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              autoCapitalize='none'
              style={isEmailFocus ? styles.focusforminput : styles.formInput}
              onFocus={() => onBlur("email")}
              onChangeText={(value) => setEmail(value)}
              onBlur={() => onBlur("email")}
              value={email}
            >
              {strings.emailAdress}
            </FloatingLabel>
            <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={isPasswordFocus ? styles.focusforminput : styles.formInput}
              password
              onChangeText={(value) => setPassword(value)}
              //value='john@email.com'
              onFocus={() => onBlur("password")}
              onBlur={() => onBlur("password")}
            >
              {strings.password}
            </FloatingLabel>
            <FloatingLabel
              labelStyle={styles.labelInput}
              inputStyle={styles.input}
              style={
                isConfiremPasswordFocus
                  ? styles.focusforminput
                  : styles.formInput
              }
              password
              onChangeText={(value) => setConfirmPassword(value)}
              //value='john@email.com'
              onFocus={onBlur}
              onBlur={onBlur}
            >
              Confirm Password
            </FloatingLabel>
            <CommonButton
              btnlinearGradient={styles.btnlinearGradient}
              colors={Colors.btnBackgroundColor}
              btnText={styles.btnText}
              viewStyle={{ marginTop: responsiveHeight(4) }}
              onPress={onSignClick}
              text={strings.signUp}
            />
            <Text style={styles.txtforget}>{strings.termsCondtion}</Text>
            <RowLine />
            {Platform.OS == "ios" &&
              <AppleBtn navigation={props.navigation} flag={"signUp"} />
            }
            <GoogleBtn navigation={props.navigation} flag={"signUp"} />
            <View style={styles.bottomView}>
              <Text style={[styles.txtforget, { top: 0 }]}>
                Already have an account?
                <Text
                  onPress={() => props.navigation.replace(strings.loginScreen)}
                  style={styles.txtReset}
                >
                  {" "}
                  Sign in
                </Text>
              </Text>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
      <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
        ref={(ref) => Toast.setRef(ref)} />
    </View>
  );
};
export default SignUp;
