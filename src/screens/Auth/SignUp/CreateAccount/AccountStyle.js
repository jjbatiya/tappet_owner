import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Colors } from '../../../../Resources/Colors';
import { Fonts } from '../../../../Resources/Fonts';
export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,
        marginTop: responsiveHeight(1),
    },
    focusforminput: {
        borderBottomWidth: 1,
        borderColor: Colors.themeColor,
        //   top: 200,
        marginTop: responsiveHeight(1),

    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        lineHeight: 22,
        color: "white",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6,
        marginLeft:-8
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center"

    },
    arrow: {
        position: 'absolute',
        right: responsiveHeight(0.5),
        height: responsiveHeight(4),
        width: responsiveWidth(3),
        flex: 0.1,
        tintColor: Colors.white
    },
    txtforget: {

        color: Colors.white,
        textAlign: "center",
        marginTop: 20,
        fontSize: 12,
        lineHeight: 15.62,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.8

    },
    txtReset: {
        fontWeight: "500",
        color: Colors.white,
        fontSize: 12,
        lineHeight: 15.62,
        textAlign: "center",
        fontFamily: Fonts.DMSansBold

    },
    txtlblStep: {
        alignSelf: "center", fontSize: 12,
        lineHeight: 15.62, fontWeight: "400", color: Colors.white, fontFamily: Fonts.DMSansRegular, opacity: 0.6
    },
    mainView: {
        marginHorizontal: responsiveWidth(5), marginVertical: responsiveHeight(1),
        flex: 1, width: "100%", flexDirection: "row"
    },
    modal: {
        //alignItems: 'center',   
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: '100%',
        width: '100%',


    },
    txtlblCheckEmail: {
        alignSelf: "center", fontSize: 16,
        lineHeight: 22, color: Colors.white, fontWeight: "700",
        marginTop: responsiveHeight(2.5), fontFamily: Fonts.DMSansRegular
    },
    txtlblRecoverStr: {
        textAlign: "center", alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        width: 295, marginTop: responsiveHeight(1.5), opacity: 0.6
    },
    txtCancel: { marginTop: responsiveHeight(1), alignSelf: "center", fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400" },
    verifyBtn: { marginBottom: responsiveHeight(1), marginTop: responsiveHeight(1.5), width: "90%", alignSelf: "center" },
    contentView: {
        marginHorizontal: responsiveWidth(5), marginTop: responsiveHeight(2.5),
        marginBottom: responsiveHeight(20)
    },
    basicStyle: {
        fontWeight: "700", fontSize: 14,
        lineHeight: 18.23, color: Colors.white, fontFamily: Fonts.DMSansRegular
    },
    dobText: { fontSize: 12, lineHeight: 15.62, fontWeight: "400", color: Colors.white, marginTop: responsiveHeight(1) },
    dateClickView: { flexDirection: "row", justifyContent: "space-between", paddingVertical: responsiveHeight(1) },
    calender: { resizeMode: "contain", height: 25, width: 25 },
    horizontalLine: { height: 1, backgroundColor: Colors.white },
    txtAddress: { fontWeight: "700", fontSize: 14, lineHeight: 18.23, color: Colors.white, marginTop: responsiveHeight(3) },
    txtCity: { fontSize: 12, lineHeight: 15.62, fontWeight: "400", color: "white", marginTop: responsiveHeight(2.5) },
    dropView: { height: 22, flex: 1, marginVertical: responsiveHeight(1) },
    dropdown: {
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.black,
    },
    txtStyle: {
        fontSize: 16,
        color: Colors.white,
        alignSelf: "center",
        lineHeight: 22,
        fontWeight: "400"
    },
    txtResendotp: {
        textAlign: "center", alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        width: 295, marginBottom: responsiveHeight(1.5), opacity: 0.6,marginTop:10
    },
    txtAuto: {
        fontSize: 16,
        backgroundColor: Colors.backgroundColor,
        color: "white",
        fontFamily: Fonts.DMSansRegular,
        flex: 1,
        marginLeft:-10,
        height:70,
        opacity:0.6
    
    },


})