import React, { useState, useRef, useEffect } from 'react';
import { View, SafeAreaView, Text, KeyboardAvoidingView, Platform, ScrollView, Modal, TouchableOpacity, TextInput, StatusBar, ActivityIndicator } from 'react-native';
import { styles } from './AccountStyle';
import LinearGradient from 'react-native-linear-gradient';
import HeaderView from '../../../../Components/HeaderView';
var FloatingLabel = require('react-native-floating-labels');
import CommonButton from '../../../../Components/CommonButton';
import RadioButton from '../../../../Components/RadioButton';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { strings } from '../../../../Resources/Strings';
import { Colors } from '../../../../Resources/Colors';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import ModalDropdown from 'react-native-modal-dropdown';
import RBSheet from "react-native-raw-bottom-sheet";
import CommonStatusBar from '../../../../Components/CommonStatusBar';
import Toast from 'react-native-toast-message';
import { validationMsg } from '../../../../Resources/ValidationMsg';
import { CalenderIcon } from '../../../../utils/svg/CalenderIcon';
import { ChevronDown } from '../../../../utils/svg/ChevronDown';
import {
    apiCall, apiCallWithToken, getApiCall, getFirebaseToken,
    getUserData, setFirebaseToken, storeData, addressGet, storeChatUserData
} from '../../../../utils/helper';
import { apiUrl } from "../../../../Redux/services/apiUrl";
import Loader from '../../../../Components/Loader';
import { comeChatUserTag, comeChat_apiKey } from '../../../../utils/Constant';
import { CometChat } from "@cometchat-pro/react-native-chat"
import AutoSerachComponent from '../../../../Components/AutoSerachComponent';
import { toastConfig } from '../../../../utils/ToastConfig';


const Account = (props) => {
    const [isFirstFocus, setFirstFocus] = useState(false)
    const [isLastFocus, setLastFocus] = useState(false)
    const [isPhoneFocus, setPhoneFous] = useState(false)
    const [isZipcodeFocus, setZipcodeFocus] = useState(false)
    const [isAddress, setIsAdress] = useState(false)
    const [date, setDate] = useState(new Date());
    const [showDate, setShowDate] = useState(false);
    const [isVisible, setVisible] = useState(false)
    const [isCodeFocus, setFocus] = useState(false)
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [phoneNo, setPhoneNumber] = useState("")
    const [zipCode, setZipCode] = useState("")
    const [address, setAddress] = useState("")
    const [city, setCityName] = useState("")
    const [dob, setDob] = useState("")
    const [gender, setGender] = useState()
    const [code, setCode] = useState("")
    const [u_id, setUiID] = useState("")
    const [device_token, setDeviceToken] = useState("")
    const [token, setToken] = useState("")
    const [isshowing, setShowing] = useState(false)
    const [refSheet, setRefernecce] = useState(null);
    const [countryData, setCountryData] = useState([])
    const [c_id, setCid] = useState("")
    const [cityData, setCityData] = useState([])
    const [country, setCountry] = useState('');
    const [userInfo] = useState(props.route.params?.userInfo != undefined ? props.route.params.userInfo : null)
    useEffect(() => {
        getUserData().then(res => {
            setUiID(res.u_id)
            setToken(res.token)
            setFirstName(res?.u_first_name)
            setLastName(res?.u_last_name)
            setPhoneNumber(res?.u_mobile_number)
            setZipCode(res?.u_zipcode)
            setCountry(res?.u_country)
            setCityName(res?.u_city)
            setAddress(res?.u_address)
            //getAllCountry();
        })
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
        if (userInfo != null) {
            setFirstName(userInfo.user.givenName)
            setLastName(userInfo.user.familyName)
        }
    }, []);

    const [genderArray, setGenderArray] = useState([
        {
            key: "Male",
            title: "Male",
            isChecked: false
        },
        {
            key: "Female",
            title: "Female",
            isChecked: false
        },
        {
            key: "Other",
            title: "Other",
            isChecked: false
        },
        {
            key: "Prefer not to say",
            title: "Prefer not to say",
            isChecked: false
        },
    ])

    const onBlur = (flag) => {
        if (flag == "first")
            setFirstFocus(!isFirstFocus)
        else if (flag == "last")
            setLastFocus(!isLastFocus)
        else if (flag == "phone")
            setPhoneFous(!isPhoneFocus)
        else if (flag == "zip")
            setZipcodeFocus(!isZipcodeFocus)
        else if (flag == "address")
            setIsAdress(!isAddress)
        else if (flag == "code")
            setFocus(!isCodeFocus)
    }
    const backClick = () => {
        props.navigation.replace(strings.loginScreen);
    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const onChangeDate = (selectedDate) => {
        const currentDate = selectedDate || date;
        setShowDate(false);
        setDate(currentDate);
        setDob(currentDate.getFullYear() + "/" + parseInt(currentDate.getMonth() + 1) + "/" + currentDate.getDate() + "/")

    };

    const hideShowDatePicker = () => {
        setShowDate(false);
    };
    const zipCodeToAddress = async () => {
        await addressGet(zipCode).then(res => {
            if (res.results.length != 0) {
                setCountry(res.results[0].address_components[res.results[0].address_components.length - 1].long_name)
                setCityName(res.results[0].address_components[1].long_name)
            }
            else {
                showMessage(strings.error, strings.msgTag, validationMsg.zipcodeInvalid)
            }
        }
        )
    }
    const onCreateAccount = () => {
        if (firstName?.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.firstname)
        else if (lastName?.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.lastname)
        else if (phoneNo?.trim() == "")
            showMessage(strings.error, strings.msgTag, validationMsg.phoneNo)
        else if (gender == undefined || gender == '')
            showMessage(strings.error, strings.msgTag, validationMsg.gender)
        // else if (dob.trim() == '')
        //     Toast.show(validationMsg.dob, Toast.SHORT);
        else if (zipCode?.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.zipCode)
        else if (c_id + "".trim() == '' && country == '')
            showMessage(strings.error, strings.msgTag, validationMsg.country)
        else if (city?.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.city)
        else if (address?.trim() == '')
            showMessage(strings.error, strings.msgTag, validationMsg.address)
        else {
            createAccountApi()
        }

    }
    const createAccountApi = async () => {
        setShowing(true)
        const obj = {
            u_first_name: firstName,
            u_last_name: lastName,
            u_mobile_number: phoneNo,
            u_id: u_id,
            u_gender: gender,
            u_dob: dob,
            u_zipcode: zipCode,
            u_city: city,
            u_address: address,
            //u_country: c_id,
            u_country: country
        }
        await apiCallWithToken(obj, apiUrl.updateuserdetail, token).then(res => {
            setShowing(false)
            if (res.status == true) {
                showMessage(strings.success, "Message", res.message)

                var obj = { ...res.result, token: token }
                storeData(obj);
                setCode(obj.u_otp + "")
                setTimeout(() => {
                    setShowing(false)
                    openBottomDialog(res)
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })
    }
    const openBottomDialog = () => {
        refSheet.open();
    }
    const verifyMobile = async () => {
        refSheet.close();
        setShowing(true)
        const obj = {
            u_mobile_number: phoneNo,
            u_otp: code,
            device_token: device_token,
            device_type: Platform.OS == "ios" ? "ios" : "android"
        }
        await apiCall(obj, apiUrl.verify_otp).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                    createUserComeChat();
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }
        })

    }
    const clickResend = async () => {
        const obj = {
            u_mobile_number: phoneNo,
            u_id: u_id,
            device_type: Platform.OS == "ios" ? "ios" : "android"
        }
        await apiCall(obj, apiUrl.resendotp).then(res => {
            if (res.status) {
                setCode(res.result.u_otp + "")

                showMessage(strings.success, strings.msgTag, res.message)
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
            }

        })
    }
    const verifyModal = () => {

        return (
            // <Modal
            //     animationType={"fade"}
            //     transparent={true}
            //     visible={isVisible}
            //     onRequestClose={() => { console.log("Modal has been closed.") }}>
            <View style={styles.modal}>
                {/* <TouchableOpacity style={{width:45,height:4,borderRadius:10,backgroundColor:"white",alignSelf:"center",marginTop:10}}></TouchableOpacity> */}
                <Text style={styles.txtlblCheckEmail}>{strings.verifyStr}</Text>
                <Text style={styles.txtlblRecoverStr}>{strings.verifyDescStr}{phoneNo}</Text>
                <TouchableOpacity onPress={() => clickResend()} >
                    <Text style={styles.txtResendotp}>Resend OTP</Text>
                </TouchableOpacity>

                <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={true ? [styles.focusforminput, { marginHorizontal: 20 }] : styles.formInput}
                    onFocus={() => onBlur("code")}
                    value={code}
                    keyboardType={"numeric"}
                    onChangeText={(value) => setCode(value)}
                    onBlur={() => onBlur("code")}
                >Confirmation code</FloatingLabel>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={styles.verifyBtn}
                    onPress={verifyMobile}
                    text={strings.confirm}
                />

            </View>
            // </Modal>
        )
    }
    const onClickGender = (item, index) => {
        setGender(item.title)
    }
    const getAllCountry = async () => {
        setShowing(true)
        await getApiCall(apiUrl.get_all_countries).then(res => {
            if (res.status) {
                setShowing(false)
                setCountryData(res.result)
            }
            else {
                setShowing(false)

            }
        })
    }
    const onSelectCountry = async (index) => {
        await setCid(countryData[index].c_id)
        getCity(countryData[index].c_id);
    }
    const getCity = async (cid) => {
        setShowing(true)

        await getApiCall(apiUrl.get_city_by_country_id + "/" + cid + "?type=2").then(res => {
            setTimeout(() => {
                setShowing(false)
            }, 1000);
            if (res.status == true) {
                setCityData(res.result)
            }
            else {
                //Toast.show(res.message, Toast.SHORT);
            }
        })
    }
    const onSelectCity = (index) => {
        setCityName(cityData[index].city_name)
    }
    const createUserComeChat = () => {
        var uid = comeChatUserTag + u_id;
        var user = new CometChat.User(uid);
        user.setName(firstName);

        CometChat.createUser(user, comeChat_apiKey).then(
            user => {
                storeChatUserData(user)
                props.navigation.navigate(strings.uploadPhotoScreen)

            }, error => {
                if (error?.code == "ERR_UID_ALREADY_EXISTS")
                    props.navigation.navigate(strings.uploadPhotoScreen)

                console.log("error", error);
            }
        )
    }
    const clickAddress = (data) => {
        setAddress(data.formatted_address)
    }
    return (
        <View style={{ flex: 1 }}>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}>
                    <SafeAreaView keyboardShouldPersistTaps='handled'>
                        <HeaderView onPress={backClick} text={strings.createAccount} />
                        <Text style={styles.txtlblStep}>{strings.stepStr1}</Text>
                        <View style={styles.mainView}>
                            <LinearGradient
                                colors={Colors.btnBackgroundColor}
                                style={{ height: 4, flex: 0.5 }}
                            />
                            <View style={{ backgroundColor: Colors.white, height: 4, flex: 0.5 }} />
                        </View>
                        <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                            ref={(ref) => Toast.setRef(ref)} />
                        <ScrollView style={{ marginBottom: 20 }} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' >

                            <View style={styles.contentView}>
                                <Text style={styles.basicStyle}>
                                    {strings.basicInformation}
                                </Text>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isFirstFocus ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("first")}
                                    onChangeText={(value) => setFirstName(value)}
                                    value={firstName}
                                    onBlur={() => onBlur("first")}
                                >{strings.firstName}</FloatingLabel>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isLastFocus ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("last")}
                                    value={lastName}
                                    onBlur={() => onBlur("last")}
                                    onChangeText={(value) => setLastName(value)}
                                >{strings.lastName}</FloatingLabel>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isPhoneFocus ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("phone")}
                                    keyboardType="numeric"
                                    onChangeText={(value) => setPhoneNumber(value)}
                                    value={phoneNo}
                                    onBlur={() => onBlur("phone")}
                                >{strings.phoneNumber}</FloatingLabel>
                                <RadioButton onClickGender={onClickGender} style={{ marginTop: responsiveHeight(3) }} PROP={genderArray} />
                                <Text style={styles.dobText}>{strings.dobStr}</Text>
                                <TouchableOpacity onPress={() => setShowDate(true)} style={styles.dateClickView}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="MM/DD/YYYY"
                                        editable={false}
                                        value={parseInt(date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear()}
                                        //  onChangeText={(searchString) => { this.setState({ searchString }) }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <CalenderIcon height={22} width={22} />
                                </TouchableOpacity>
                                <View style={styles.horizontalLine}></View>
                                <Text style={styles.txtAddress}>
                                    {strings.address}
                                </Text>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isZipcodeFocus ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("zip")}
                                    keyboardType="number-pad"
                                    onChangeText={(value) => setZipCode(value)}
                                    onBlur={() => onBlur("zip")}
                                    returnKeyType={'done'}
                                    value={zipCode}
                                    onEndEditing={() => zipCodeToAddress()}
                                >{strings.zipCode}</FloatingLabel>
                                {zipCode?.length != 0 &&
                                    <>
                                        <Text style={styles.txtCity}>Country</Text>
                                        <ModalDropdown
                                            defaultValue={country}
                                            disabled={true}
                                            //options={countryData.map(a => a.c_name)}
                                            style={styles.dropView}
                                            onSelect={(index) => onSelectCountry(index)}
                                            dropdownStyle={{
                                                width: 300
                                            }}
                                            editable={false}
                                            dropdownTextStyle={styles.dropdown}
                                            textStyle={styles.txtStyle}
                                            dropdownTextHighlightStyle={{
                                                color: Colors.theme
                                            }}
                                            renderRightComponent={() => {
                                                return (
                                                    <View style={styles.arrow}>
                                                        <ChevronDown width={responsiveWidth(3)} height={responsiveHeight(4)} />
                                                    </View>
                                                )
                                            }}
                                        />
                                        <View style={{  height: 1, backgroundColor: Colors.white }}></View>
                                    </>
                                }

                                {zipCode?.length != 0 &&
                                    <><Text style={styles.txtCity}>City</Text>
                                        <ModalDropdown
                                            defaultValue={city}
                                            disabled={true}

                                            //options={cityData.map(a => a.city_name)}
                                            style={styles.dropView}
                                            onSelect={(index) => onSelectCity(index)}
                                            dropdownStyle={{
                                                width: 300
                                            }}
                                            dropdownTextStyle={styles.dropdown}
                                            textStyle={styles.txtStyle}
                                            dropdownTextHighlightStyle={{
                                                color: Colors.theme
                                            }}
                                            //defaultIndex={this.stateNameTogetIndex(this.state.state)}
                                            renderRightComponent={() => {
                                                return (
                                                    <View style={styles.arrow}>
                                                        <ChevronDown width={responsiveWidth(3)} height={responsiveHeight(4)} />
                                                    </View>
                                                )
                                            }}
                                        />
                                        <View style={{ height: 1, backgroundColor: Colors.white }}></View>
                                    </>}

                                {/* <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isAddress ? styles.focusforminput : styles.formInput}
                                    onFocus={() => onBlur("address")}
                                    multiline={true}
                                    onChangeText={(value) => setAddress(value)}
                                    onBlur={() => onBlur("address")}
                                    value={address}
                                >{strings.streetAddress}</FloatingLabel> */}
                                <View style={{  flex: 1 }}>
                                    <Text style={[styles.txtCity, { marginLeft: 0 }]}>
                                        {strings.address}
                                    </Text>
                                    <AutoSerachComponent
                                        style={styles.txtAuto}
                                        onChangeText={(text) => setAddress(text)}
                                        event_location={address}
                                        onPress={(data) => clickAddress(data)}
                                        isRightIcon={false}
                                        placeholderTextColor={Colors.opacityColor3}
                                        placeholder={strings.streetAddress}
                                    />

                                </View>
                                <View style={{ height: 1, backgroundColor: Colors.white }}></View>
                                <CommonButton
                                    btnlinearGradient={styles.btnlinearGradient}
                                    colors={Colors.btnBackgroundColor}
                                    btnText={styles.btnText}
                                    viewStyle={{ marginTop: 40 }}
                                    onPress={onCreateAccount}
                                    text={strings.createAccount}
                                />
                                <Text style={[styles.txtforget]}>By pressing Create Account Button, I agree with
                                    <Text onPress={() => alert("terms")} style={styles.txtReset}>{'  \nTerms & Conditions  '}</Text>
                                    Tap Pet.</Text>
                            </View>
                            {showDate && (
                                <DateTimePickerModal
                                    isVisible={showDate}
                                    mode="date"
                                    date={date}
                                    onConfirm={onChangeDate}
                                    onCancel={hideShowDatePicker}
                                />
                            )}

                        </ScrollView>

                        <RBSheet
                            ref={(ref) => setRefernecce(ref)}
                            height={320}
                            openDuration={250}
                            closeOnDragDown={true}
                            customStyles={{
                                container: {
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                    backgroundColor: Colors.dialogBackground,
                                    padding: 10
                                }
                            }}
                        >
                            {verifyModal()}
                        </RBSheet>
                    </SafeAreaView>

                </KeyboardAvoidingView>
            </LinearGradient>
        </View>
    )
}
export default Account;