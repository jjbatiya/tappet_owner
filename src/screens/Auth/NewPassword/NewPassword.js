import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, Image, KeyboardAvoidingView, ScrollView, Platform } from 'react-native';
import { styles } from './NewPasswordStyle';
import LinearGradient from 'react-native-linear-gradient';
var FloatingLabel = require('react-native-floating-labels');
import HeaderView from '../../../Components/HeaderView';
import CommonButton from '../../../Components/CommonButton';
import * as Constant from '../../../utils/Constant';
import { strings } from '../../../Resources/Strings';
import { Colors } from '../../../Resources/Colors';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import CommonStatusBar from '../../../Components/CommonStatusBar';
import { apiCall, getFirebaseToken, getUserData, storeData } from '../../../utils/helper';
import { apiUrl } from '../../../Redux/services/apiUrl';
import Toast from 'react-native-toast-message';
import Loader from '../../../Components/Loader';
import { toastConfig } from '../../../utils/ToastConfig';

const NewPassword = (props) => {
    const [isPasswordFocus, setPasswordFocus] = useState(false)
    const [isConfiremPasswordFocus, setConfirmPasswordFocus] = useState(false)
    const [u_email, setEmail] = useState()
    const [password, setPassword] = useState("")
    const [confirm_password, setConfirmPassword] = useState("")
    const [device_token, setDeviceToken] = useState("");
    const [otp, setOtp] = useState("")
    const [u_id, setUid] = useState("")
    const [isshowing, setShowing] = useState(false)

    useEffect(() => {
        getFirebaseToken().then(res => {
            setDeviceToken(res)
        })
        getUserData().then(res => {
            setOtp(res.u_otp)
            setUid(res.u_id)
            setEmail(res.u_email)
        })
    }, []);
    const onBlur = (flag) => {
        if (flag == "password")
            setPasswordFocus(!isPasswordFocus)
        else
            setConfirmPasswordFocus(!isConfiremPasswordFocus)

    }
    const showMessage = (type, title, message) => {
        Toast.show({
            type: type,
            text1: title,
            text2: message,
            autoHide: true,
            visibilityTime: 3000
        });
    }
    const onSignClick = () => {
        setVisible(true)
    }
    const backClick = () => {
        props.navigation.goBack(null);
    }
    const onResetPassword = async () => {
        var obj = {
            u_id: u_id,
            u_email: u_email,
            u_otp: otp,
            device_token: device_token,
            password: password,
            password_confirmation: confirm_password,
            device_type: Platform.OS == "ios" ? "ios" : "android"
        }
        await apiCall(obj, apiUrl.create_password).then(res => {
            if (res.status) {
                showMessage(strings.success, strings.msgTag, res.message)
                storeData("")
                setTimeout(() => {
                    setShowing(false)
                    props.navigation.reset({
                        index: 0,
                        routes: [{ name: strings.loginScreen }],
                    });
                }, 1000);
            }
            else {
                showMessage(strings.error, strings.msgTag, res.message)
                setTimeout(() => {
                    setShowing(false)
                }, 1000);
            }

        })
    }

    return (
        <View style={{ flex: 1 }}>

            <LinearGradient
                colors={Colors.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                <Loader loading={isshowing} style={{ alignItems: 'center' }} ></Loader>

                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}>
                    <ScrollView>
                        <SafeAreaView>

                            <HeaderView isCloseIcon={true} onPress={backClick} />
                            <View style={{ marginTop: 10, marginHorizontal: responsiveWidth(3) }}>
                                <Text style={styles.txtsignin}>{strings.newPasswordText}</Text>
                                <Text style={styles.txtlbl}>{strings.newPasslbl}</Text>

                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isPasswordFocus ? styles.focusforminput : styles.formInput}
                                    password
                                    //value='john@email.com'
                                    onChangeText={(text) => setPassword(text)}
                                    onFocus={() => onBlur("password")}
                                    onBlur={() => onBlur("password")}
                                >{strings.password}</FloatingLabel>
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={isConfiremPasswordFocus ? styles.focusforminput : styles.formInput}
                                    password
                                    onChangeText={(text) => setConfirmPassword(text)}

                                    onFocus={onBlur}
                                    onBlur={onBlur}
                                >{strings.confirmPassword}</FloatingLabel>
                                <CommonButton
                                    btnlinearGradient={styles.btnlinearGradient}
                                    colors={Colors.btnBackgroundColor}
                                    btnText={styles.btnText}
                                    viewStyle={{ marginTop: 40 }}
                                    onPress={onResetPassword}
                                    text={strings.resetPassword}
                                />
                            </View>

                        </SafeAreaView>
                    </ScrollView>
                </KeyboardAvoidingView>
            </LinearGradient>
            <Toast config={toastConfig} style={{ alignItems: "center", alignSelf: "center" }}
                ref={(ref) => Toast.setRef(ref)} />
        </View>
    )
}
export default NewPassword;