import React, { useEffect, useState } from 'react';
import { View, Image, Text, Alert, DeviceEventEmitter, Platform, Linking } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { styles } from './SplashStyle';
import * as Constant from '../../utils/Constant';
import CommonStatusBar from '../../Components/CommonStatusBar';
import { getUserData, imageUploadApi } from '../../utils/helper';
import crashlytics from '@react-native-firebase/crashlytics';
import { Images } from '../../Resources/Images';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import Toast from 'react-native-simple-toast';
import { apiUrl } from '../../Redux/services/apiUrl';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { strings } from '../../Resources/Strings';
//import LocationEnabler from 'react-native-location-enabler';
// const {
//     PRIORITIES: { HIGH_ACCURACY },
//     useLocationSettings,
// } = LocationEnabler;
const SplashScreen = (props) => {
    const data = null;
    const [isVisible, setVisible] = useState(false)
    // const [enabled, requestResolution] = useLocationSettings({
    //     priority: HIGH_ACCURACY, // optional: default BALANCED_POWER_ACCURACY
    //     alwaysShow: true, // optional: default false
    //     needBle: true, // optional: default false
    // });
    useEffect(() => {
        const intervalId = setTimeout(() => {
            dynamicLinks()
                .getInitialLink()
                .then(link => {
                    if (link != null) {
                        var flag = getParameterByName("flag", link.url);
                        var share_id = getParameterByName("share_id", link.url);
                        linkToNavigate(flag, share_id, link);
                    }
                    else { checkLogin()
                    }
                });
                dynamicLinks().onLink(handleDynamicLink);
        }, 3000);
        //clear interval on re-render to avoid memory leaks
        return () => clearInterval(intervalId)

    }, []);
    const handleDynamicLink = link => {
        // Handle dynamic link inside your own application
        if (link != null) {
            var flag = getParameterByName("flag", link.url);
            var share_id = getParameterByName("share_id", link.url);
            linkToNavigate(flag, share_id, link);
        }
        else {
            checkLogin()
        }
    };

    function getParameterByName(name, url) {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
    const linkToNavigate = async (flag, share_id, link) => {
        await getUserData().then(res => {
            if (res != undefined) {
                if (res.token != '' && res.token != undefined) {
                    if (flag == "group") {
                        var obj = { group_id: share_id }
                        addGroupMember(share_id, res.u_id, res.token, obj)
                    }
                    else if (flag == "eventUpcoming") {

                        addMemberEvent(share_id, res.u_id, res.token)
                    }
                    else if (flag == "call") {
                        var sessionId = getParameterByName("sessionId", link.url);
                        CometChat.acceptCall(sessionId).then(
                            call => {
                                props.navigation.navigate(strings.callScreen, { callData: call })

                                // start the call using the startCall() method
                            },
                            error => {
                                console.log("Call acceptance failed with error", error);
                                Toast.show(error.message, Toast.SHORT);

                                // handle exception
                            }
                        );
                    }
                    else if (flag == "petProfile") {
                        var item = {
                            pet_id: share_id
                        }
                       // props.navigation.navigate(strings.petFullOtherView,{petData: item})
                        props.navigation.navigate(strings.PetProfileTabScreen, { screen: "MyProfile", petData: item })
                    }
                    else if (flag == "user") {
                        props.navigation.navigate(strings.UserProfileScreen, { u_id: share_id })
                    }
                }
                else
                    props.navigation.replace(strings.loginScreen)
            }
            else
                props.navigation.replace(strings.loginScreen)


        })
    }
    const checkLogin = async () => {
      // await requestResolution()

        await getUserData().then(res => {
            if (res != undefined) {
                if (res.token != '' && res.token != undefined) {
                    if (res.u_first_name == "" || res.u_first_name == null)
                        props.navigation.replace(strings.createaccountScreen)
                    else
                        props.navigation.replace(strings.TabNavigation)
                }
                else
                    props.navigation.replace(strings.loginScreen)
            }
            else
                props.navigation.replace(strings.loginScreen)


        })
    }

    const addGroupMember = async (group_id, id, token, obj) => {
        let data = new FormData();
        data.append("group_id", group_id)
        data.append("group_members[]", id)

        await imageUploadApi(data, apiUrl.add_group_member, token).then(res => {
            if (res.status == true) {
                props.navigation.replace(strings.chatMessageScreen, { groupData: obj })
            }
            else {
                Toast.show(res.message, Toast.SHORT);
            }
        })
    }
    const addMemberEvent = async (event_id, id, token) => {
        let data = new FormData();
        data.append("event_id", event_id)
        data.append("event_members[]", id);
        var obj = { event_id: event_id }
        await imageUploadApi(data, apiUrl.invite_friend_or_group, token).then(res => {
            if (res.status == true) {
                props.navigation.replace(strings.EventDetailUpcomingScreen, { item: obj })
            }
            else {
                Toast.show(res.message, Toast.SHORT)
            }
        })
    }
    return (
        <View style={{ flex: 1 }}>
            <LinearGradient
                colors={Constant.ColorTheme.screenBackgroundColor}
                style={styles.container}
            >
                <CommonStatusBar />
                {/* <LinearGradient
                    colors={Constant.ColorTheme.btnBackgroundColor}
                    style={styles.centerView}
                >
                    <LogoIcon />
                </LinearGradient>
                <Text style={styles.txtNameApp}>Tap Pet</Text> */}
                {/* <SplashLogo/> */}
                <Image style={{ height: 70, width: 190 }} source={Images.unionIcon} />
                <Text style={styles.bottomText}>Version 0.14</Text>

            </LinearGradient>
        </View>
    )
}
export default SplashScreen;