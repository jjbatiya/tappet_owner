import { StyleSheet } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import { Fonts } from '../../Resources/Fonts';
export const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    centerView:{ height: 140, width: 140, borderRadius: 100, alignItems: 'center', justifyContent: 'center' },
    imageView:{ height: 40, width: 40 },
    txtNameApp:{
        fontSize:32,
        color:"white",
        lineHeight:40,
        letterSpacing:2,
        marginTop:10,
        alignSelf:'center',
        fontFamily:Fonts.LobsterTwoRegular
    },
    bottomText:{
        height:50,
        justifyContent: 'center',
        alignItems: 'center',
        position:"absolute",
        bottom:0,
        color:"white",
        textAlign:'center',
        fontSize:14,
        lineHeight:18,
        letterSpacing:1
    }

})
