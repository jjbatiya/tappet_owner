import React from "react";
import {
    View,
    TouchableOpacity,
    Image,
    SafeAreaView,
    Text,
    Platform,
} from 'react-native';
import { CometChat } from '@cometchat-pro/react-native-chat';
import style from './styles';
import { Images } from "../../../Resources/Images";
import CallCancel from "../../../utils/svg/CallCancel";
import CallAccept from '../../../utils/svg/CallAccept'
import Modal from "react-native-modal";
import Sound from 'react-native-sound';
import { outgoingCallAlert } from '../../../assets/audio';
import RNSystemSounds from '@dashdoc/react-native-system-sounds';
import { strings } from "../../../Resources/Strings";

class CometChatIncomingCall extends React.Component {
    constructor(props) {
        super(props);

      

    //   this.playOutgoingAlert();

    }
    componentDidUpdate() {

        try {

            //this.props.isVisible == true&&  this.props.navigation.navigate(strings.incomingCallScreen, { callData: this.props?.callData, flag: "user", userData: this.props.userData })

        } catch (error) {
            console.log(error)
        }
    }
    playOutgoingAlert = () => {
        try {
            if (Platform.OS == "ios")
                this.props.isVisible == true && RNSystemSounds.beep(RNSystemSounds.iOSSoundIDs.SystemSoundPreview4)
            else
                this.props.isVisible == true && RNSystemSounds.beep(RNSystemSounds.AndroidSoundIDs.TONE_CDMA_CALL_SIGNAL_ISDN_INTERGROUP)
            // this.outgoingAlert.setCurrentTime(0);
            // this.outgoingAlert.setNumberOfLoops(-1);
            // this.outgoingAlert.play((success) => {
            //     if (success) {
            //         console.log('successfully finished playing');
            //     } else {
            //         console.log('playback failed due to audio decoding errors');
            //     }
            // })

        } catch (error) {
            console.log(error)

        }
    };
    callCancel() {
        this.props.callCancel(this.props?.callData)
    }
    callAccept() {
        this.props.callAccept(this.props?.callData)
    }
    render() {
        return (
            <Modal
                isVisible={false}
                style={{ flex: 1, position: "absolute", top: 0 }}
            //transparent animated animationType="fade"
            >
                <SafeAreaView>
                    <View style={[style.callContainerStyle]}>
                        <View style={style.senderDetailsContainer}>
                            <Image style={{ height: 48, width: 48, borderRadius: 24, alignSelf: "center" }} resizeMode={"contain"} source={{ uri: this.props?.callData?.callInitiator?.avatar }} />
                            <View style={{ alignSelf: "center", marginLeft: 16 }}>
                                <Text numberOfLines={1} style={style.nameStyle}>{this.props?.callData?.callInitiator?.name}</Text>
                                <Text numberOfLines={1} style={style.callTag}>Incoming Call</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: "row", alignSelf: "center" }}>
                            <TouchableOpacity
                                onPress={() => this.callCancel()}
                                style={style.callAcceptBtn}>
                                <CallCancel />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.callAccept()}
                                style={[style.callAcceptBtn, { backgroundColor: "green", marginLeft: 12, }]}>
                                <CallAccept />
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </Modal>
        )
    }
}
export default CometChatIncomingCall;