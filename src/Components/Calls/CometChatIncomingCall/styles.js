import { StyleSheet } from 'react-native';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../../../Resources/Colors';
import { Fonts } from '../../../Resources/Fonts';

export default StyleSheet.create({
  callContainerStyle: {
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 4,
    padding: 20,
    backgroundColor: Colors.dialogBackground,
    justifyContent: "space-between",
    flexDirection: "row",
    height:90,
    
  },
  senderDetailsContainer: {
    flexDirection: 'row',
  },
  nameStyle: {
    color: 'white',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 18.23,
    fontFamily: Fonts.DMSansRegular,
    width:responsiveWidth(25),
  },
  callTag: {
    color: 'white',
    fontSize: 12,
    fontWeight: '400',
    lineHeight: 15.62,
    fontFamily: Fonts.DMSansRegular,
    opacity: 0.6,
    width:responsiveWidth(25),
    
  },
  callAcceptBtn: {
    height: 36, width: 36, backgroundColor: "red",
    borderRadius: 18, alignItems: "center",
    justifyContent: "center"
  }

});
