import React from 'react';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import { Fonts } from '../Resources/Fonts';
import { Images } from '../Resources/Images';

const HeaderView = (props) => {
    return (
        <TouchableOpacity onPress={()=>props.onPress()} style={{ marginVertical: 10 }}>
            {props.isCloseIcon ? <Image resizeMode={"contain"} style={{ marginLeft: 10, height: 20, width: 30, tintColor: "white" }} source={require("../assets/Images/closeIcon.png")}></Image> :

                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", width: '100%', }}>
                    <Image resizeMode={"contain"} style={{ marginLeft: 10, height: 20, width: 30, tintColor: "white" }} source={props.isClose?Images.closeIcon:Images.backArrow}></Image>
                    <Text style={{ marginLeft:props.isPhoto?0:-30, color: "white",
                     alignSelf: "center", fontWeight: "700", fontSize: 16, lineHeight: 22 ,fontFamily:Fonts.DMSansRegular}}>{props.text}</Text>
                    {/* <Text style={{ color: "white", alignSelf: "center", fontWeight: "700", fontSize: 16, lineHeight: 22 }}></Text> */}
                    {props.isPhoto?
                     <TouchableOpacity onPress={()=>props.onSkip()} style={{ width: 60, marginRight:10,height: 28, backgroundColor: "white", borderRadius: 4, alignItems: "center", justifyContent: "center" }}>
                        <Text style={{ color: "#E25FF7", textAlign: "center",
                         fontSize: 12, lineHeight: 15.62, fontWeight: "500" ,fontFamily:Fonts.DMSansRegular}}>{props.textSkip}</Text>
                    </TouchableOpacity> : <View></View>}
                </View>}

        </TouchableOpacity>
    )
}
export default HeaderView;