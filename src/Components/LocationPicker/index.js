import React, { useRef, useState } from "react";
import { Modal, View, StyleSheet,Image,Dimensions,TouchableOpacity } from "react-native";
import { Colors } from "../../Resources/Colors";
import { Fonts } from "../../Resources/Fonts";
import AutoSerachComponent from "../AutoSerachComponent";
import MapView from 'react-native-maps';
import { Images } from "../../Resources/Images";
import { mapStyle } from "../../Resources/MapStyle";
import CommonButton from "../CommonButton";
import { responsiveWidth } from "react-native-responsive-dimensions";
import { strings } from "../../Resources/Strings";

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const LocationPicker = (props) => {
    const [post_latitude, setLatitude] = useState(-34.397)
    const [post_longitude, setLongitude] = useState(150.644)
    let mapRef = useRef();
    const [serachText,setSerachText]=useState("")
    const [locationData,setLocationData]=useState(null)
    const clickAddress = (data) => {
        var latitude = data?.geometry?.location?.lat
        var longitude = data?.geometry?.location?.lng
        setSerachText(data?.formatted_address)
        setLatitude(latitude)
        setLongitude(longitude)
        mapRef.current.animateToRegion({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        })
        setLocationData(data)

    }
    return (
        <Modal
            animationType="fade"
            visible={props.isVisible}
            transparent={true}
        >
            <View style={{width:width-50,flex:1,top:150,marginHorizontal:20}}>
                <MapView
                    style={{
                        height:400
                    }}
                 //   customMapStyle={mapStyle}

                    ref={mapRef}
                    provider={"google"}
                    mapType={strings.mapType}
                    initialRegion={{
                        latitude: post_latitude,
                        longitude: post_longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }}
                >
                    <MapView.Marker
                        coordinate={{
                            latitude: post_latitude,
                            longitude: post_longitude,
                        }}
                    >
                        <TouchableOpacity style={styles.roundView}

                        >
                            <View style={styles.markerView}>
                                <Image style={{ height: 15, width: 15, tintColor: Colors.theme }} source={Images.HomeTab}></Image>
                            </View>
                        </TouchableOpacity>
                    </MapView.Marker>
                </MapView>
                <View style={{ position: "absolute", top: 10, width: width-50,paddingHorizontal:10 }} >
                    <AutoSerachComponent
                        style={styles.txtAuto}
                        onChangeText={(text) =>setSerachText(text)}
                        event_location={serachText}
                        onPress={(data) =>clickAddress(data)}
                        isRightIcon={false}
                    />
                </View>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={{ marginVertical: 0, width: '100%' }}
                    onPress={() =>props.saveLocation(locationData)}
                    text={"Save Location"}
                />
                 <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={[Colors.black,Colors.black]}
                    btnText={styles.btnText}
                    viewStyle={{ marginVertical: 0, width: '100%' }}
                    onPress={() =>props.closeModal()}
                    text={"Cancel"}
                />
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    txtAuto: {
        height: 44,
        paddingVertical: 5,
        paddingHorizontal: 10,
        fontSize: 16,
        backgroundColor: Colors.opacityColor2,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        lineHeight: 22,
        flex: 1,
    },
    markerView: {
        height: 24, width: 24, alignItems: "center", justifyContent: "center",
        backgroundColor: Colors.primaryViolet, borderRadius: 12
    },
    roundView: {
        height: 44, width: 44, alignItems: "center", justifyContent: "center",
        backgroundColor: Colors.opacityColor2, borderRadius: 22, borderWidth: 1, borderColor: Colors.white
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular
    },
    btnlinearGradient: {
        height: 44,
        alignItems: "center",
        justifyContent: "center",

    },
})
export default LocationPicker;