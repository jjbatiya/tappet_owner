import React, { PureComponent } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
//import type Moment from 'moment';
import { Colors } from '../../Resources/Colors';
import { Fonts } from "../../Resources/Fonts";

export default class Date extends PureComponent {

//   props: {
//     // Date to render
//     date: Moment,
//     // Index for `onPress` and `onRender` callbacks
//     index: number,
//     // Whether it's the currently selected date or no
//     isActive: boolean,
//     // Called when user taps a date
//     onPress: (index: number) => void,
//     // Called after date is rendered to pass its width up to the parent component
//     onRender: (index: number, width: number) => void,
//   };

  // Style helper functions that merge active date styles with the default ones
  // when rendering a date that was selected by user or was set active by default

  getContainerStyle = () => ({
    ...styles.container,
    ...(this.props.isActive ? styles.containerActive : {})
  });

  getDayStyle = () => ({
    ...styles.text,
    ...styles.day,
    ...(this.props.isActive ? styles.textActive : {})
  });

  getDateStyle = () => ({
    ...styles.text,
    ...styles.date,
    ...(this.props.isActive ? styles.textActive : {})
  });

  // Call `onRender` and pass component's with when rendered
  //onLayout = (event: { nativeEvent: { layout: { x: number, y: number, width: number, height: number } } }) => {
  onLayout = (event) => {
    const {
      index,
      onRender,
    } = this.props;
    const { nativeEvent: { layout: { width } } } = event;
    onRender(index, width);
  };

  // Call `onPress` passed from the parent component when date is pressed
  onPress = () => {
    const { index, onPress } = this.props;
    onPress(index);
  };

  render() {
    const { date } = this.props;
    return (
      <TouchableOpacity
        style={this.getContainerStyle()}
        onLayout={this.onLayout}
        onPress={this.onPress}
      >
            <Text style={this.getDateStyle()}>{date.format('DD')}</Text>
            <Text style={this.getDayStyle()}>{date.format('ddd')}</Text>
    </TouchableOpacity>
    );
  }

}

const styles = {
  container: {
    borderWidth: 1,
    borderColor: Colors.opacityColor3,
    paddingHorizontal: 16,
    paddingVertical: 10,
    marginHorizontal : 5,
    borderRadius : 5
  },
  containerActive: {
    borderColor: Colors.primaryViolet,
    backgroundColor:'rgba(255, 255, 255, 0.2)'
  },
  day: {
    fontSize: 12,
    fontFamily : Fonts.DMSansBold,
  },
  date: {
    fontSize: 16,
    fontFamily : Fonts.DMSansBold,
  },
  text: {
    color: Colors.opacityColor3,
    textAlign: 'center',
  },
  textActive: {
    color: Colors.white,
  },
};