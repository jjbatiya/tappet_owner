import React, { useRef, useState } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Colors } from '../Resources/Colors';
import { Image, TouchableOpacity, Text, View } from 'react-native';
import { Fonts } from '../Resources/Fonts';
import { Images } from '../Resources/Images';
import Geolocation from '@react-native-community/geolocation';
import { addressGetFromLatLng } from '../utils/helper';


class AutoSerachComponent extends React.Component {
  constructor(props) {
    super(props)
    this.refValue = React.createRef()
    this.state = {
      count: 0,
      homePlace: null
    }
  }
  componentDidMount() {
    try {
      Geolocation.getCurrentPosition(info => {
        var latitude = info.coords.latitude
        var longitude = info.coords.longitude
        addressGetFromLatLng(latitude, longitude).then(res => {
          let workPlace = {
            description: "Current Location",
            geometry: { location: { lat: latitude, lng: longitude } },
            formatted_address: res.results[0].formatted_address,
            name: res.results[0].address_components[1]?.long_name
          };
          this.setState({
            homePlace: workPlace
          })
        })


      }
      );
    } catch (error) {

    }
  }
  //  renderDescription(rowData) {
  //   const title = rowData.structured_formatting.main_text;
  //   const address = rowData.structured_formatting.secondary_text;
  //   return (
  //     <View>
  //       <Text style={styles.rowTitle}>
  //         {title}
  //       </Text>
  //       <Text style={styles.rowAddress}>
  //         {address}
  //       </Text>
  //     </View>

  //   );
  // }
  render() {
    return (
      <GooglePlacesAutocomplete
        ref={this.refValue}
        placeholder={this.props.placeholder != undefined ? this.props.placeholder : 'Enter Location'}
        autoFocus={false}
        currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
        currentLocationLabel="Current Location"
        predefinedPlaces={[this.state.homePlace]}

        fetchDetails={true}
        textInputProps={{
          onChangeText: (text) => {

            (text != "" || this.props.event_location?.length == 1) && this.props.onChangeText(text)
          },
          value: this.props.event_location,
          placeholderTextColor: this.props.placeholderTextColor != undefined ? this.props.placeholderTextColor : Colors.black,
          multiline: true

        }}
        styles={{
          container: {
            flex: 1,

            //height:100
          },

          textInput: this.props.style,

        }}

        onPress={(data, details = null) => {
          this.props.onPress(details)
        }}
        query={{
          key: 'AIzaSyBhqD5BcsPIUmky6_n7ZjoeiwB3mEGLqiY',
          language: 'en',
          components: 'country:us',
        }}
        nearbyPlacesAPI='GooglePlacesSearch'
        debounce={200}
        listViewDisplayed={'auto'}

        renderRightButton={() => {
          return (
            (this.props.onBackPress == undefined && this.props.isRightIcon == undefined) &&
            <Image style={{ height: 20, width: 15, tintColor: Colors.white, alignSelf: "center" }} source={Images.placeIcon}></Image>
          )

        }}
        renderLeftButton={() => {
          return (
            this.props.onBackPress != undefined &&
            <TouchableOpacity onPress={() => this.props.onBackPress()} style={{ alignSelf: "center" }}>
              <Image style={{ height: 20, width: 20, tintColor: Colors.white }} source={Images.backArrow}></Image>
            </TouchableOpacity>
          )
        }}
      // renderRow={rowData => {
      //   return (
      //     <View>
      //       { this.renderDescription(rowData)}
      //     </View>
      //   )
      // }}
      />
    );
  }
};

export default AutoSerachComponent;