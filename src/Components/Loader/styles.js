import {StyleSheet,Dimensions} from 'react-native';
import { Colors } from '../../Resources/Colors';
const { height, width } = Dimensions.get("window")

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        // backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    //video view
    playIcon:{ tintColor: Colors.white, position: "absolute", height: 50, width: 50,alignSelf:"center",top:width/4,opacity:0.6 }
});

export default styles
