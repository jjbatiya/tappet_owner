import React from 'react';
import {
    View,
    Modal,
    ActivityIndicator
} from 'react-native';
import { Colors } from '../../Resources/Colors';

import styles from './styles';
const Loader = props => {
    const {
        loading,
        transparent
    } = props;
    return (
        <Modal
            transparent={transparent !== undefined ? transparent : true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() => { console.log('close modal') }}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                        size="large" color={Colors.primaryViolet}
                        animating={loading} />
                </View>
            </View>
        </Modal>
    )
}


export default Loader;
