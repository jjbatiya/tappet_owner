import { StyleSheet,Dimensions } from 'react-native';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
const { height, width } = Dimensions.get("window")

export default StyleSheet.create({
  container: { marginBottom: 16, marginRight: 8,flex:1 },
  messageWrapperStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3399FF',
    marginBottom: 4,
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
    paddingHorizontal: 12 ,
    paddingVertical: 8,
    maxWidth: '65%',
    borderRadius: 10,
  },
  messageInfoWrapperStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  messageDetailContainer: { flex: 1, marginRight: 4 },
  messageTextStyle: { color: 'white', fontSize: 15, textAlign: 'justify' },
  playIcon:{ tintColor: Colors.white, position: "absolute", height: 50, width: 50,alignSelf:"center",top:width/4,opacity:0.6},
  txtTime: {
    fontWeight: "400", fontSize: 8, alignSelf: "flex-end",
    lineHeight: 10.42, opacity: 0.6, color: Colors.white,
    fontFamily: Fonts.DMSansRegular,
    paddingVertical:2

},
tagView:{
    backgroundColor: '#3399FF',
    borderRadius: 10,
}

});
