import React, { useState, useEffect } from 'react';
import { View, Text, TouchableWithoutFeedback, Alert, Image, Linking, TouchableOpacity, Dimensions } from 'react-native';
import style from './styles';
import RNFetchBlob from 'rn-fetch-blob';
import { Colors } from '../../Resources/Colors';
import { Images } from '../../Resources/Images';
var Sound = require('react-native-sound');
import moment, { now } from 'moment';
import AudioChat from '../AudioChat';
import { createThumbnail } from "react-native-create-thumbnail";
import { strings } from '../../Resources/Strings';
const { height, width } = Dimensions.get("window")

const ChatFileBubbleSender = (props) => {
    const [fileType, setFileType] = useState("")
    const [thumbnail, setThumbnail] = useState("")
    const [newDate] = useState(moment(new Date(props.data.sentAt * 1000)).format('hh:MM a'));


    useEffect(async () => {
        var typeData = props.data.data.attachments[0].mimeType.split("/")
        var fileName = props.data.data.attachments[0].name
        const typeFile = getFileType(typeData, props.data.data.attachments[0].extension)
        if (fileName.includes("AUDIO-"))
            setFileType("audio")
        else
            setFileType(typeFile)
        if (typeFile == "video") {
            createThumbnail({
                url: props.data.data.attachments[0].url,
                timeStamp: 10000,
            })
                .then(response => setThumbnail(response.path))
                .catch(err => console.log("error", err));
        }
    }, [])

    const download = () => {
        try {
            let PictureDir = RNFetchBlob.fs.dirs.PictureDir;
            let date = new Date();
            let name = props.data.data.attachments[0].name;

            RNFetchBlob.config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache: true,
                appendExt: props.data.data.attachments[0].extension,
                addAndroidDownloads: {
                    useDownloadManager: true,
                    notification: true,
                    path: PictureDir + '/' + name,
                },
            })
                .fetch('GET', props.data.data.attachments[0].url, {
                    // some headers ..
                })
                .then((res) => {
                    Alert.alert('File Downloaded');
                    // RNFetchBlob.android.actionViewIntent(
                    //   res.path(),
                    //   props.message.data.attachments[0].mimeType,
                    // );
                });
        } catch (error) {
            console.log("", error)
        }
    };
    const getFileType = (data, extension) => {
        if (data[0] == "video" && data[1] == "3gpp")
            return "audio"
        else if (data[0] == "audio" || extension == "mp3")
            return "audio"
        else if (data[0] == "video" || extension == "mp4")
            return "video"
        else
            return "document"
    }
    const fileView = () => {
        var newDate1 = moment(new Date(props.data.sentAt * 1000)).format('MMMM d, YYYY');

        const typeFile = fileType
        if (typeFile == "audio")
            props.navigation.navigate(strings.playerScreen, { title: "Title", filepath: props.data.data.attachments[0].url, dirpath: null })
        else if (typeFile == "video")
            props.navigation.navigate(strings.webViewScreen, { url: props.data.data.attachments[0].url, title: props.data.sender.name, date: newDate1 })
        else
            Linking.openURL(props.data.data.attachments[0].url)
    }
    const onLongPress = () => {
        var obj = props.data
        obj.typeFile = fileType;
        props.onLongPress(obj)
    }
    return (
        <View style={style.container}>
            {fileType != "audio" && fileType != "video" ?
                <TouchableOpacity
                    onPress={() =>
                        //    Linking.openURL(props.data.data.attachments[0].url)
                        //  props.navigation.navigate("webViewScreen",{url:props.data.data.attachments[0].url})
                        fileView()
                    }
                    onLongPress={() => onLongPress()}
                >

                    <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                        && style.tagView}>
                        {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                            props.tagViewItSelf(props.data.data.metadata.tagData)}
                        <View style={style.messageWrapperStyle}>

                            <View style={style.messageDetailContainer}>
                                <Text style={style.messageTextStyle}>
                                    {props.data.data.attachments[0].name}
                                </Text>
                            </View>
                            <Image style={{ height: 24, width: 24, tintColor: Colors.white }} source={Images.downloadfile} />
                        </View>
                    </View>
                </TouchableOpacity> :
                fileType == "audio" ?
                    <TouchableOpacity onLongPress={() => onLongPress()}>
                        <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                            && style.tagView}>
                            {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                                props.tagViewItSelf(props.data.data.metadata.tagData)}
                            <AudioChat
                                data={props.data} isSender={true}
                            />
                        </View>
                    </TouchableOpacity> :
                    fileType == "video" ?
                        <TouchableOpacity
                            onLongPress={() => onLongPress()}
                            onPress={() => fileView()}
                            style={{ alignSelf: "flex-end", backgroundColor: Colors.opacityColor, borderRadius: 4 }}>
                            <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                                && {width: width / 1.6,backgroundColor:Colors.opacityColor,borderRadius: 4}}>
                                {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                                    props.tagViewItSelf(props.data.data.metadata.tagData)}
                                <Image style={{ width: width / 1.6, height: width / 1.6, borderTopLeftRadius: 4, borderTopRightRadius: 4 }} source={{ uri: thumbnail }} resizeMode={"cover"}>
                                </Image>
                                <Image source={Images.playerIcon} resizeMode={"contain"}
                                    style={style.playIcon} />
                                <Text style={style.txtTime}>{newDate}</Text>
                            </View>
                        </TouchableOpacity> : null
            }
        </View>
    );
};
export default ChatFileBubbleSender;
