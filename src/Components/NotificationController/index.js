import React, { useState, useEffect, useRef } from 'react';
import messaging from '@react-native-firebase/messaging';
import { StyleSheet, View, Text, Image, Modal, TouchableOpacity, SafeAreaView, Platform } from 'react-native';
import { Images } from '../../Resources/Images';
import { Fonts } from '../../Resources/Fonts';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../../Resources/Colors';
import { strings } from '../../Resources/Strings';

import RNSystemSounds from '@dashdoc/react-native-system-sounds';
import { comeGroupTag } from '../../utils/Constant';
import { storeChatNotificationFlag } from '../../utils/helper';

const NotificationController = (props) => {
    const [isshowing, setShowing] = useState(false)
    const [title, setTitle] = useState("")
    const [message, setMessage] = useState("")
    const [objData, setObjData] = useState(null)
    useEffect(() => {
        const unsubscribe = messaging().onMessage(async (remoteMessage) => {
            var notification_cat = JSON.parse(remoteMessage?.data?.message)?.category
            try {
                if (Platform.OS == "ios" && notification_cat != "call")
                    RNSystemSounds.beep(RNSystemSounds.iOSSoundIDs.SMSReceived_Alert1)
                else
                    notification_cat != "call" && RNSystemSounds.beep(RNSystemSounds.AndroidSoundIDs.TONE_SUP_PIP)

            } catch (error) {

            }
            setTimeout(() => {
                setShowing(false)
            }, 5000);
            if (props.callIsVisible == false)
                setShowing(true)
            else
                setShowing(false)

            setTitle(remoteMessage.notification.title)
            setMessage(remoteMessage.notification.body)
            var category = undefined;

            try {
                category = JSON.parse(remoteMessage?.data?.message)?.category
            } catch (err) {
                category = undefined;
            }
            if ((category != undefined && category != "")) {
                var objData = JSON.parse(remoteMessage?.data?.message)
                if (objData?.receiverType == "user" || objData?.receiverType == "group") {
                    if (objData.category == "message" || objData.category == "call") {
                        setObjData(objData)
                        props.onMessageNotification();
                        storeChatNotificationFlag({ chat_notification_flag: true })
                    }

                }

            }
            else {
                // props.navigation.navigate(strings.notificationScreen)
            }
        });

    }, []);

    const onNotificationClick = () => {
        setShowing(false)
        if (objData == null) {
            props.navigation.navigate(strings.notificationScreen)
        }
        else {
            if (objData?.receiverType == "user") {

                var id = objData.sender;
                id = id.replace("comechatuser_", "")
                setObjData(null)
                props.navigation.navigate(strings.typeMessageScreen, { userData: { u_id: id, u_first_name: "", u_last_name: "" } })
            }
            else {
                try {
                    var conversationId = objData?.conversationId
                    const group_id = conversationId.replace("group_" + comeGroupTag, "")
                    const obj = {
                        group_id: group_id
                    }
                    if (Number.isInteger(parseInt(group_id))) {
                        props.navigation.navigate(strings.chatMessageScreen, { groupData: obj })
                    }
                } catch (error) {

                }
            }
        }
    }
    return (
        <Modal
            visible={isshowing}
            style={{ flex: 1, position: "absolute", top: 0 }}
            transparent={true}

        >
            <SafeAreaView>
                <TouchableOpacity onPress={() => onNotificationClick()} style={styles.main}>
                    <Image style={{ height: 50, width: 30, alignSelf: "center" }} resizeMode={"contain"} source={Images.notification} />
                    <View style={{ alignSelf: "center", marginLeft: 16, flex: 0.8 }}>
                        <Text style={styles.nameStyle}>{title}</Text>
                        <Text numberOfLines={1} style={styles.callTag}>{message}</Text>
                    </View>
                </TouchableOpacity>
            </SafeAreaView>

        </Modal>

    )
}
const styles = StyleSheet.create({
    nameStyle: {
        color: 'white',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 18.23,
        fontFamily: Fonts.DMSansRegular,
    },
    main: {
        backgroundColor: Colors.dialogBackground,
        flexDirection: "row",
        padding: 10,
        borderRadius: 5,

    },
    callTag: {
        color: 'white',
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 15.62,
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6,
    },
})
export default NotificationController;