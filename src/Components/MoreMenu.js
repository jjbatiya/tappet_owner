import React from 'react';
import { FlatList, View, Image, Text, TouchableOpacity } from 'react-native';
import { Colors } from '../Resources/Colors';
import { Fonts } from '../Resources/Fonts';
import { Images } from '../Resources/Images';


const MoreMenu = (props) => {
    const renderView = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => props.clickItem(item)} style={{ flexDirection: "row", paddingVertical: 5 }}>
                <View style={{ alignItems: "center", padding: 7 }}>
                    <Image style={{ height: 15, width: 15, tintColor: item.key == "delete" ? "red" : Colors.white }} source={item.image}></Image>
                    {item.status == "pending" &&
                        <View style={{ height: 8, width: 8, backgroundColor: "red", position: "absolute", top: 3, right: 3, borderRadius: 4 }} /> 
                    }
                </View>
                <Text style={{
                    fontSize: 14, lineHeight: 18.23, fontFamily: Fonts.DMSansRegular,
                    color: item.key == "delete" ? "red" : Colors.white, fontWeight: "400",
                    alignSelf: "center", marginLeft: 5
                }}>{item.title}</Text>
                {item.status == "pending" &&
                    <View style={{ flex: 1, alignItems: "center",flexDirection:"row" ,justifyContent:"flex-end"}}>
                        <Text style={{
                            alignSelf: "center", fontFamily: Fonts.DMSansRegular,
                            color: Colors.white, opacity: 0.6, fontWeight: "400", 
                            fontSize: 12, lineHeight: 15.62,marginRight:10
                        }}>{"Pending.."}</Text>
                        <Image style={{height:10,width:10,tintColor:Colors.white,alignSelf:"center"}} source={Images.clock}></Image>
                    </View>
                }
            </TouchableOpacity>
        )
    }
    return (
        <View>
            <FlatList
                data={props.data}
                showsVerticalScrollIndicator={false}
                renderItem={renderView}
            >

            </FlatList>
        </View>

    )
}
export default MoreMenu;