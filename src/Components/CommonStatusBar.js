import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../Resources/Colors';
import { StatusBarHeight } from '../Resources/StatusbarHeight';

const CommonStatusBar = (props) => {
    const {
        color
    } = props
    return (
        Platform.OS == "android" ?
        <StatusBar
            animated={true}
            backgroundColor={color != undefined ? color : '#0C0024'}
        />:<></>
    )
}
export default CommonStatusBar;
