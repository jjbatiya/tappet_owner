import React from 'react';
import {
    View, StyleSheet, Image, Text,
    TouchableOpacity, ActivityIndicator, Dimensions, TextInput, StatusBar
} from 'react-native';
//import StatusBar from './AppHeader/StatusBar';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

import { Colors } from '../Resources/Colors';
import { Images } from '../Resources/Images';
import { Fonts } from '../Resources/Fonts';
import LinearGradient from 'react-native-linear-gradient';
const { height, width } = Dimensions.get("window")
const EventDetailHeader = (props) => {

    return (
        <View>
            <StatusBar hidden />

            <Image style={styles.imgBack} source={props.uri != undefined ? { uri: props.uri } : props.image}></Image>

            <View style={styles.mainView}>
                <View style={styles.topView}>
                    <TouchableOpacity
                        onPress={() => props.onBackPress()}
                        style={styles.touchbleView}>
                        <View style={styles.backGround} />
                        <Image style={styles.imgBackIcon} source={Images.backArrow}></Image>
                    </TouchableOpacity>
                    <View style={{ flexDirection: "row" }}>
                        {props.isShare ?
                            <TouchableOpacity
                                onPress={() =>props.onShareClick()}
                                style={[styles.touchbleView, { marginRight: 6 }]}>
                                <View style={styles.backGround} />
                                <Image style={styles.imgBackIcon} source={Images.share}></Image>
                            </TouchableOpacity> : null}
                        {props.isMenu ? <TouchableOpacity
                            onPress={() => props.onMoreClick()}
                            style={styles.touchbleView}>
                            <View style={styles.backGround} />
                            <Image style={styles.imgBackIcon} source={Images.menuDot}></Image>
                        </TouchableOpacity> : <></>}
                    </View>
                </View>

                <LinearGradient style={{
                    position: "absolute",
                    height: responsiveHeight(8),
                    width: width, bottom: 0,
                }} colors={Colors.viewCare}>
                </LinearGradient>
                <View style={{ padding: 20 }}>
                    <Text style={{
                        fontSize: 30, fontWeight: "400", lineHeight: 37.5,
                        color: Colors.white, fontFamily: Fonts.LobsterTwoRegular,
                        letterSpacing: 0.2
                    }}>{props.title}</Text>
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    imgBack: {
        height: responsiveHeight(30),
        width: "100%"
    },
    mainView: { position: "absolute", justifyContent: "space-between", width: "100%", height: responsiveHeight(30) },
    touchbleView: { justifyContent: "center", alignItems: "center" },
    backGround: { height: 32, width: 32, backgroundColor: Colors.white, opacity: 0.2, borderRadius: 20 },
    imgBackIcon: { height: 12, width: 12, alignSelf: "center", position: "absolute", tintColor: Colors.white },
    topView: { flexDirection: "row", marginHorizontal: 15, justifyContent: "space-between", flex: 1, marginTop: -25 }
})
export default EventDetailHeader;