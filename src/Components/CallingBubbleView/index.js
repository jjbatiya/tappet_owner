import React from 'react';
import {
    View, Image,Text,TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '../../Resources/Colors';
import { Images } from '../../Resources/Images';

class CallingBubbleView extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {

        return (
            <LinearGradient  colors={[Colors.opacityColor2,Colors.opacityColor2]} style={this.props.style}>
                <View style={{ flexDirection: "row",alignItems:"center" }}>
                    <Image style={{height:30,width:30}} source={Images.sendervideocall} resizeMode={"contain"} ></Image>
                    <Text style={{color:"white",marginLeft:0}}>{this.props.text}</Text>
                </View>
                <TouchableOpacity onPress={this.props.onJoin} style={{ backgroundColor: "white", alignItems: "center", justifyContent: "center",borderRadius:4,height:25 }}>
                    <Text>Join</Text>
                </TouchableOpacity>
            </LinearGradient>
        );
    }
}

export default CallingBubbleView;
