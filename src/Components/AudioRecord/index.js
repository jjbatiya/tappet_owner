import React from "react";
import { Modal, TouchableOpacity, View, Dimensions, StyleSheet, Text, Platform } from "react-native";
import { responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import { Colors } from "../../Resources/Colors";
import { Fonts } from "../../Resources/Fonts";
import CommonButton from "../CommonButton";
const { height, width } = Dimensions.get("window")
import SoundRecorder from 'react-native-sound-recorder';
import { getPermissions, requestExternalWritePermission, requestMicroPhonePermission } from "../../utils/helper";

class AudioRecord extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
            startDisable: false,
            filePath: "",
            filename: ""
        }
    }
    componentWillUnmount() {
        clearInterval(this.state.timer);
    }
    getFileName() {
        let d = new Date();
        let dformat = `${d.getDate()}${d.getMonth()}${d.getHours()}${d.getMinutes()}${d.getSeconds()}`;
        var name = "AUDIO-" + dformat + ".mp4";
        this.setState({
            filename: name
        })
        return name;
    }
    startRecording = async () => {
        await getPermissions();
        let isAudioRecord = await requestMicroPhonePermission();

        if (isAudioRecord || Platform.OS == "android") {
            this.startTimer();
            if (this.state.minutes_Counter == '00' && this.state.seconds_Counter == '00') {
                SoundRecorder.start(SoundRecorder.PATH_CACHE + "/" + this.getFileName())
                    .then(function () {
                        console.log('started recording');
                    });
            }
            else {
                SoundRecorder.resume();
            }
        }
        else {
            alert("Need Some Permission")
        }


    }

    startTimer = () => {
        let timer = setInterval(() => {

            var num = (Number(this.state.seconds_Counter) + 1).toString(),
                count = this.state.minutes_Counter;

            if (Number(this.state.seconds_Counter) == 59) {
                count = (Number(this.state.minutes_Counter) + 1).toString();
                num = '00';
            }

            this.setState({
                minutes_Counter: count.length == 1 ? '0' + count : count,
                seconds_Counter: num.length == 1 ? '0' + num : num
            });
        }, 1000);
        this.setState({ timer });

        this.setState({ startDisable: true })
    }
    pauseRecording = () => {
        clearInterval(this.state.timer);
        this.setState({ startDisable: false })
        SoundRecorder.pause();
    }
    stopRecording = () => {
        clearInterval(this.state.timer);
        this.setState({ startDisable: false })

        SoundRecorder.stop().then(result => {
            var obj = {
                filename: this.state.filename,
                ...result
            }
            this.props.stopRecord(obj)
        })
    }
    closeRecording = () => {
        const { seconds_Counter, minutes_Counter } = this.state
        try {
            if (seconds_Counter == '00' && minutes_Counter == '00') {
                clearInterval(this.state.timer);
                this.setState({
                    startDisable: false, minutes_Counter: '00',
                    seconds_Counter: '00',
                })
                this.props.closeRecord();

            }
            else {
                SoundRecorder.stop().then(result => {
                    clearInterval(this.state.timer);
                    this.setState({
                        startDisable: false, minutes_Counter: '00',
                        seconds_Counter: '00',
                    })
                    this.props.closeRecord();


                })
            }

        } catch (error) {
            this.props.closeRecord();

        }


    }
    render() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.isVisible}>
                <View style={{
                    backgroundColor: Colors.dialogBackground, height: 250, width: width - 100,
                    alignSelf: "center", alignItems: "center",
                    justifyContent: "center", position: "absolute", top: responsiveHeight(20),
                    borderRadius: 10
                }}>
                    <Text style={styles.counterText}>{this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>

                    <CommonButton
                        btnlinearGradient={styles.btnlinearGradient}
                        colors={this.state.startDisable == false ? Colors.btnBackgroundColor : [Colors.opacityColor3, Colors.opacityColor3]}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => this.startRecording()}
                        text={this.state.minutes_Counter != '00' || this.state.seconds_Counter != '00' ? "Resume Recording" : "Start Recording"}
                    />

                    <CommonButton
                        btnlinearGradient={[styles.btnlinearGradient]}
                        colors={this.state.startDisable != false ? Colors.btnBackgroundColor : [Colors.opacityColor3, Colors.opacityColor3]}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => this.pauseRecording()}
                        text={"Pause Recording"}
                    />
                    {(this.state.startDisable == false && (this.state.minutes_Counter != '00' || this.state.seconds_Counter != '00')) &&
                        <CommonButton
                            btnlinearGradient={[styles.btnlinearGradient]}
                            colors={Colors.btnBackgroundColor}
                            btnText={styles.btnText}
                            viewStyle={styles.btnView}
                            onPress={() => this.stopRecording()}
                            text={"Upload Audio"}
                        />}
                    <CommonButton
                        btnlinearGradient={[styles.btnlinearGradient]}
                        colors={[Colors.opacityColor2, Colors.opacityColor2]}
                        btnText={styles.btnText}
                        viewStyle={styles.btnView}
                        onPress={() => this.closeRecording()}
                        text={"Close Recording"}
                    />

                </View>

            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        paddingHorizontal: responsiveWidth(4)

    },
    btnView: { alignItems: "center", justifyContent: "center" },
    btnlinearGradient: {
        height: 40,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10
    },
    counterText: {
        fontSize: 28,
        color: '#fff'
    }

})
export default AudioRecord;

