/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableWithoutFeedback, Alert, Image, Linking, TouchableOpacity, Dimensions } from 'react-native';

import style from './styles';
import RNFetchBlob from 'rn-fetch-blob';
import { CometChat } from '@cometchat-pro/react-native-chat';
import { Images } from '../../Resources/Images';
import { Colors } from '../../Resources/Colors';
var Sound = require('react-native-sound');
import moment, { now } from 'moment';
import AudioChat from '../AudioChat';
import { createThumbnail } from "react-native-create-thumbnail";
import { strings } from '../../Resources/Strings';
const { height, width } = Dimensions.get("window")
const ChatFileBubbleReceiver = (props) => {
    const [fileType, setFileType] = useState("")
    const [thumbnail, setThumbnail] = useState("")

    useEffect(async () => {
        var typeData = props.data.data.attachments[0].mimeType.split("/")
        var fileName = props.data.data.attachments[0].name
        const typeFile = getFileType(typeData, props.data.data.attachments[0].extension)
        if (fileName.includes("AUDIO-"))
            setFileType("audio")
        else
            setFileType(typeFile)
        // var typeData = props.data.data.attachments[0].mimeType.split("/")
        // const typeFile = getFileType(typeData, props.data.data.attachments[0].extension)
        // setFileType(typeFile)
        if (typeFile == "video") {
            createThumbnail({
                url: props.data.data.attachments[0].url,
                timeStamp: 10000,
            })
                .then(response => setThumbnail(response.path))
                .catch(err => console.log({ err }));
        }
    }, [])
    const data = {
        ...props.data,
        messageFrom: "receiver",
    };
    let avatarImg = '';

    if (data.receiverType === CometChat.RECEIVER_TYPE.GROUP) {
        avatarImg = { uri: data.sender.avatar };
    }

    /**
     * Handler for downloading file attachment in local storage.
     * @param
     */

    const download = () => {
        let PictureDir = RNFetchBlob.fs.dirs.PictureDir;
        let date = new Date();
        let name = props.data.data.attachments[0].name;

        RNFetchBlob.config({
            // add option that makes response data to be stored as a file,
            // is much more performant.
            fileCache: true,
            appendExt: props.data.data.attachments[0].extension,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: PictureDir + '/' + name,
            },
        })
            .fetch('GET', props.data.data.attachments[0].url, {
                // some headers ..
            })
            .then((res) => {
                Alert.alert('File Downloaded');
            });
    };
    const getFileType = (data, extension) => {

        if (data[0] == "video" && data[1] == "3gpp")
            return "audio"
        else if (data[0] == "audio" || extension == "mp3")
            return "audio"
        else if (data[0] == "video")
            return "video"
        else
            return "document"
    }
    const fileView = () => {
        var newDate1 = moment(new Date(props.data.sentAt * 1000)).format('MMMM d, YYYY');
        const typeFile = fileType
        if (typeFile == "audio")
            props.navigation.navigate('player', { title: "Title", filepath: props.data.data.attachments[0].url, dirpath: null })
        else if (typeFile == "video")
            props.navigation.navigate(strings.webViewScreen, { url: props.data.data.attachments[0].url, title: props.data.sender.name, date: newDate1 })
        else
            Linking.openURL(props.data.data.attachments[0].url)
    }
    const onLongPress = () => {
        var obj = props.data
        obj.typeFile = fileType;
        props.onLongPress(obj)
    }
    return (
        <View style={style.mainContainerStyle}>
            <View style={style.messageContainer}>
                {props.data.receiverType === CometChat.RECEIVER_TYPE.GROUP ? (
                    <Image style={style.imgProfileEvent} source={{ uri: props.data.data.metadata?.senderImageUri }}></Image>
                ) : null}
                <View>
                    {props.data.receiverType === CometChat.RECEIVER_TYPE.GROUP ? (
                        <View style={style.senderNameContainer}>
                            <Text style={{ color: Colors.white }}>
                                {data.sender.name}
                            </Text>
                        </View>
                    ) : null}
                    {fileType != "audio" && fileType != "video" ?

                        <View style={style.messageContainerStyle}>
                            <TouchableOpacity
                                onPress={() => fileView()}
                                onLongPress={() => onLongPress()}>
                                <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                                    && style.tagView}>
                                    {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                                        props.tagViewItSelf(props.data.data.metadata.tagData)}
                                    <View
                                        style={[
                                            style.messageWrapperStyle,
                                            { backgroundColor: Colors.white },
                                        ]}>

                                        <View style={style.attachmentNameStyle}>
                                            <Text style={[style.attachmentName]}>
                                                {props.data.data.attachments[0].name}
                                            </Text>
                                        </View>
                                        <Image style={{ height: 24, width: 24, tintColor: '#3399FF' }} source={Images.downloadfile} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View> : fileType == "audio" ?
                            <TouchableOpacity onLongPress={() => onLongPress()}>
                                <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                                    && style.tagView}>
                                    {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                                        props.tagViewItSelf(props.data.data.metadata.tagData)}
                                    <AudioChat
                                        data={props.data} isSender={false}
                                    />
                                </View>
                            </TouchableOpacity> :
                            fileType == "video" ?
                                <TouchableOpacity
                                    onPress={() => fileView()}
                                    onLongPress={() => onLongPress()}
                                    style={{ alignSelf: "flex-start" }}>
                                    <View style={(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null)
                                        && { width: width / 1.6, backgroundColor: Colors.opacityColor, borderRadius: 4 }}>
                                        {(props.data.data.metadata?.tagData != undefined && props.data.data.metadata?.tagData != null) &&
                                            props.tagViewItSelf(props.data.data.metadata.tagData)}
                                        <Image style={{ width: width / 1.6, height: width / 1.6 }} source={{ uri: thumbnail }} resizeMode={"cover"}>
                                        </Image>
                                        <Image source={Images.playerIcon} resizeMode={"contain"}
                                            style={style.playIcon} />
                                    </View>
                                </TouchableOpacity> : null
                    }

                </View>
            </View>
        </View>
    );
};
export default ChatFileBubbleReceiver;
