import React from 'react'
import { View, Image, Text, Slider, TouchableOpacity, Platform, Alert, Dimensions, StyleSheet } from 'react-native';

import Sound from 'react-native-sound';
import { Images } from '../../Resources/Images';

const img_speaker = require('./resources/ui_speaker.png');
const img_pause = require('./resources/ui_pause.png');
const img_play = require('./resources/ui_play.png');
const img_playjumpleft = require('./resources/ui_playjumpleft.png');
const img_playjumpright = require('./resources/ui_playjumpright.png');
const { height, width } = Dimensions.get("window")
import moment, { now } from 'moment';

export default class AudioChat extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            playState: 'paused', //playing, paused
            playSeconds: 0,
            duration: 0
        }
        this.sliderEditing = false;
        Sound.setCategory('Playback');

    }

    componentDidMount() {
        //this.play();

        this.timeout = setInterval(() => {
            if (this.sound && this.sound.isLoaded() && this.state.playState == 'playing' && !this.sliderEditing) {
                this.sound.getCurrentTime((seconds, isPlaying) => {
                    this.setState({ playSeconds: seconds });
                })
            }
        }, 100);
    }
    componentWillUnmount() {
        if (this.sound) {
            this.sound.release();
            this.sound = null;
        }
        if (this.timeout) {
            clearInterval(this.timeout);
        }
    }

    onSliderEditStart = () => {
        this.sliderEditing = true;
    }
    onSliderEditEnd = () => {
        this.sliderEditing = false;
    }
    onSliderEditing = value => {
        if (this.sound) {
            this.sound.setCurrentTime(value);
            this.setState({ playSeconds: value });
        }
    }

    play = async () => {
        if (this.sound) {
            this.sound.play(this.playComplete);
            this.setState({ playState: 'playing' });
        } else {
            const filepath = this.props.data.data.attachments[0].url;

            this.sound = new Sound(filepath, '', (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                    Alert.alert('Notice', 'Audio File Not Support');
                    this.setState({ playState: 'paused' });
                } else {
                    this.setState({ playState: 'playing', duration: this.sound.getDuration() });
                    this.sound.play(this.playComplete);
                }
            });
        }
    }
    playComplete = (success) => {
        if (this.sound) {
            if (success) {
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
                Alert.alert('Notice', 'audio file error. (Error code : 2)');
            }
            this.setState({ playState: 'paused', playSeconds: 0 });
            this.sound.setCurrentTime(0);
        }
    }

    pause = () => {
        if (this.sound) {
            this.sound.pause();
        }

        this.setState({ playState: 'paused' });
    }

    jumpPrev15Seconds = () => { this.jumpSeconds(-15); }
    jumpNext15Seconds = () => { this.jumpSeconds(15); }
    jumpSeconds = (secsDelta) => {
        if (this.sound) {
            this.sound.getCurrentTime((secs, isPlaying) => {
                let nextSecs = secs + secsDelta;
                if (nextSecs < 0) nextSecs = 0;
                else if (nextSecs > this.state.duration) nextSecs = this.state.duration;
                this.sound.setCurrentTime(nextSecs);
                this.setState({ playSeconds: nextSecs });
            })
        }
    }

    getAudioTimeString(seconds) {
        const h = parseInt(seconds / (60 * 60));
        const m = parseInt(seconds % (60 * 60) / 60);
        const s = parseInt(seconds % 60);

        //return ((h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s));
        return (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s);
    }

    render() {

        const currentTimeString = this.getAudioTimeString(this.state.playSeconds);
        const durationString = this.getAudioTimeString(this.state.duration);
        var newDate = moment(new Date(this.props.data.sentAt * 1000)).format('hh:MM a');

        return (
            <View style={this.props.isSender ? styles.messageWrapperStyle : styles.messageWrapperStyleReciver}>
                {this.props.isSender == true && <Image style={{ height: 40, width: 40, borderRadius: 20 }} source={{ uri: this.props.data.metadata?.senderImageUri }} />}
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: "row", alignSelf: "center" }}>
                        {this.state.playState == 'playing' &&
                            <TouchableOpacity onPress={this.pause} style={{ marginLeft: this.props.isSender == true ? 20 : 0, alignSelf: "center" }}>
                                <Image source={img_pause} style={{ width: 20, height: 20, tintColor: this.props.isSender == false ? "black" : "white" }} />
                            </TouchableOpacity>}
                        {this.state.playState == 'paused' &&
                            <TouchableOpacity onPress={this.play} style={{ marginLeft: this.props.isSender == true ? 20 : 0, alignSelf: "center" }}>
                                <Image source={img_play} style={{ width: 20, height: 20, tintColor: this.props.isSender == false ? "black" : "white" }} />
                            </TouchableOpacity>
                        }

                        {/* <Text style={{ color: 'white', alignSelf: 'center' }}>{currentTimeString}</Text> */}
                        <Slider
                            onTouchStart={this.onSliderEditStart}
                            // onTouchMove={() => console.log('onTouchMove')}
                            onTouchEnd={this.onSliderEditEnd}
                            // onTouchEndCapture={() => console.log('onTouchEndCapture')}
                            // onTouchCancel={() => console.log('onTouchCancel')}

                            onValueChange={this.onSliderEditing}
                            value={this.state.playSeconds} maximumValue={this.state.duration} maximumTrackTintColor='gray'
                            minimumTrackTintColor={this.props.isSender == false ? "black" : "white"}
                            thumbTintColor={this.props.isSender == false ? "black" : "white"}
                            style={{ flex: 1, marginHorizontal: Platform.select({ ios: 5 }), alignSelf: "center" }} />
                    </View>
                    <View style={{ alignItems: "center", justifyContent: "space-between",flexDirection:"row" }}>
                        <Text style={{ color: this.props.isSender == false ? "black" : "white", marginLeft: this.props.isSender == false ? 0 : 54 }}>{this.state.playState == 'playing' ? currentTimeString : durationString}</Text>
                        <Text style={{ color: this.props.isSender == false ? "black" : "white", marginLeft: this.props.isSender == false ? 0 : 54 }}>{newDate}</Text>

                    </View>

                </View>

            </View>



        )
    }
}
const styles = StyleSheet.create({
    messageWrapperStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#3399FF',
        marginBottom: 4,
        alignSelf: 'flex-end',
        paddingHorizontal: 12,
        paddingVertical: 8,
        maxWidth: '100%',
        borderRadius: 10,
    },
    messageWrapperStyleReciver: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        alignSelf: 'flex-start',
        paddingHorizontal: 12,
        paddingVertical: 8,
        borderRadius: 10,
        width: width / 1.4,
        backgroundColor: "white"
    },

})