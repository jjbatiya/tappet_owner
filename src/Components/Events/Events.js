import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Dimensions,
  FlatList
} from 'react-native';
import Event from './Event';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import DashedLine from 'react-native-dashed-line';

const { width } = Dimensions.get('window')
export default class Events extends Component {

  constructor(props) {
    super(props)

    this.state = {
      timeList: [],
      scheduleData: this.props.data
    }
  }
  componentDidMount() {
    this.setListOfTimes()
    this.setState({
      scheduleData: this.props.data
    })

  }

  setListOfTimes = () => {
    var storeTime = [];
    for (var time = 0; time < 24; time++) {

      if (time < 10) {
        time = '0' + time
      }

      storeTime.push(time)
    }
    this.setState({ timeList: storeTime })
  }
  renderView = ({ item, index }) => {
    const { events, navigate } = this.props;
    return (
      <View style={{ marginVertical: 5 }}>
        {<Event event={item} key={index} navigate={navigate} />}
      </View>
    )
  }
  render() {
    const { events, navigate,pet_id } = this.props;
    const { timeList } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView>
          {/* {events && events.map((event, index) =>
            <Event event={event} key={index} />)} */}
          {timeList && timeList.map((time, index) => (
            <View style={styles.timeContainer}>
              <Text style={styles.time}>{time + ' : 00'}</Text>
              <View style={styles.timeView}>
                <DashedLine dashLength={5} dashColor='#FFFFFF90' />
                {/* <FlatList
                  data={this.state.scheduleData}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  scrollEnabled={false}
                  renderItem={this.renderView}
                >

                </FlatList> */}
                {events && events.map((event, index) => {
                       return (
                        <View style={{marginVertical : 5}}>
                          {<Event event={event} key={index} navigate={navigate} pet_id={pet_id} /> }
                        </View>
                       )
                     })
                    }



              </View>
            </View>
          )
          )}
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#243177',
  },
  timeContainer: {
    flexDirection: 'row',
    minHeight: 70,
    paddingHorizontal: 20
  },
  timeView: {
    borderTopWidth: 1,
    //borderTopColor :Colors.opacityColor3,
    //position : 'absolute',
    top: 6,
    left: 20,
    width: width * 0.73,
    borderStyle: 'dotted',
  },
  time: {
    color: Colors.opacityColor3,
    fontSize: 12,
    fontFamily: Fonts.DMSansRegular,
    textTransform: 'uppercase',
    minWidth: 50
  }
});