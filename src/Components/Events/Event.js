import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal
} from 'react-native';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import { strings } from '../../Resources/Strings';

export default class Event extends Component {

  render() {
    const { event,navigate,pet_id } = this.props;
    const {
      title,
      time,
    } = event;

    return (
      <TouchableOpacity style={styles.container} onPress={()=> navigate(strings.addScheduleScreen,{pet_id:pet_id})}>
        <View  style={styles.textContainer}>
          <Text style={[styles.text, styles.title]}>{title}</Text>
          <Text style={styles.hours}>{time} hr</Text>
        </View>
      </TouchableOpacity>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : Colors.eventBox,
    padding: 15,
    borderRadius : 5,
    marginVertical:3
  },
  textContainer: {
    flex: 1,
  },
  image: {
    width: 89,
    height: 89,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.75)',
  },
  title: {
    color: Colors.white,
    opacity : 0.6,
    fontSize: 14,
    fontFamily : Fonts.DMSansRegular,
    //marginBottom: 10,
  },
  hours : {
    color: Colors.white,
    opacity : 0.4,
    fontSize: 14,
    fontFamily : Fonts.DMSansRegular,
  }
});