import React from 'react';
import { TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';

const { width,height } = Dimensions.get('window');
const ButtonComponent = (props) => {

    const { title, onPress } = props;
    return (
        <TouchableOpacity 
            style={styles.container}
            onPress={()=> onPress()}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container : {
        width : width - 20,
        height: 40,
        justifyContent:'center',
        alignItems:'center',
        borderWidth : 1,
        borderRadius : 5,
    },
    text : {
        textAlign:'center'
    }
})
export default ButtonComponent;