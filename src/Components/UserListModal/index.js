import React, { useState, useEffect } from 'react';
import { FlatList, Modal, StyleSheet, TouchableOpacity, View, Image, Text, TouchableWithoutFeedback } from 'react-native';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import { Images } from '../../Resources/Images';

const tagData = [
    {
        id: 1,
        title: "members",

    }
]
const UserListModal = (props) => {
    const { isVisible, clickItem, data, onRequestClose, user_id } = props                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

    const renderSubView = ({ item, index }) => {
        return (
            item.gm_user_id != user_id &&
            <TouchableOpacity
                onPress={() => clickItem(item)}
                style={styles.mainGroupView}>
                <View>
                    <Image style={styles.imgGroup} source={{ uri: item.member.u_image }}></Image>
                    {/* <View style={{ flexDirection: "row", position: "absolute", bottom: 0, right: 0 }}>
                        <Image style={styles.peopleImg} source={Images.people1}></Image>
                    </View> */}
                </View>
                <View style={styles.radioViewGroup}>
                    <View style={{ marginLeft: 20, flexDirection: "row" }}>
                        <Text style={styles.radioText}>{item.member.u_first_name}</Text>

                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    const renderItem = ({ item, index }) => {
        return (

            <View style={{ marginVertical: 10, marginHorizontal: 10 }}>
                <Text style={styles.titleText}>
                    {item.title}
                </Text>
                <FlatList
                    data={data}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderSubView}
                />
            </View>
        )
    }
    return (
        <Modal
            animationType={"fade"}
            transparent={true}
            visible={isVisible}
            onRequestClose={() => onRequestClose()}>
            <View style={styles.tagView}>
                <FlatList
                    data={tagData}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}

                />
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    tagView: {
        position: "absolute", bottom: 60, width: 242, marginLeft: 30,
        backgroundColor: Colors.theme, borderWidth: 1, borderColor: Colors.opacityColor
    },
    titleText: {
        fontSize: 12, fontWeight: "700", lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular, textTransform: "uppercase"
    },
    mainGroupView: { flexDirection: "row", marginHorizontal: 0, marginVertical: 10, alignItems: "center" },

    imgGroup: {
        height: 32,
        width: 32,
        borderRadius: 16,
    },
    peopleImg: { height: 16, width: 16, borderRadius: 8, borderWidth: 1, borderColor: Colors.theme },
    radioViewGroup: { justifyContent: "space-between", flexDirection: "row", width: '90%' },
    radioText: {
        fontSize: 12,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        fontFamily: Fonts.DMSansRegular,

    },
    txtInvite: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        opacity: 0.6,
        fontFamily: Fonts.DMSansRegular,
        alignSelf: "center"
    },

})
export default UserListModal;