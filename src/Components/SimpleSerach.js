import React from 'react';
import {
    View, StyleSheet, Image, Text,
    TouchableOpacity, ActivityIndicator, SafeAreaView, TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveWidth } from 'react-native-responsive-dimensions';

import { Colors } from '../Resources/Colors';
import { Fonts } from '../Resources/Fonts';
import { Images } from '../Resources/Images';
//import { RFPercentage, RFValue } from '../../Library/DaynamicSize';

const SimpleSerach = (props) => {
    return (
        <View style={styles.header}>

            <View style={{ marginHorizontal: props.isScreen ? 0 : responsiveWidth(4) }}>
                <View style={props.isScreen ? styles.borderStyleScreen : styles.borderStyle}></View>

                <View style={{ flexDirection: 'row', alignItems: "center", justifyContent: 'space-between', position: "absolute", height: 36 }}>
                    <TouchableOpacity onPress={() => props.onPlaceClick()}>
                        <Image style={{ width: 14, height: 14, tintColor: Colors.white, opacity: 0.4, marginLeft: 10 }} source={Images.serachIcon}></Image>
                    </TouchableOpacity>
                    <TextInput
                        style={styles.textInputStyle}
                        placeholder={props.placeHolder}
                        placeholderTextColor={Colors.grey}
                        onChangeText={props.onChangeText}
                        editable={props.editable}
                        onSubmitEditing={props.onSubmitEditing}
                    >
                    </TextInput>



                </View>
            </View>
        </View>
    )
}
export default SimpleSerach;
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 70,
        width: '100%',
        // backgroundColor: white,
        //shadowColor: black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.10,
        shadowRadius: 1,
        elevation: 3,

    },
    backImgStyle: { marginLeft: 20, height: 20, width: 20, tintColor: "white" },
    borderStyle: {
        height: 36, width: responsiveWidth(88), borderRadius: 4, borderWidth: 1, borderColor: Colors.white,
        opacity: 0.2, backgroundColor: Colors.white, marginBottom: 10
    },
    textInputStyle: {
        color: Colors.white, fontSize: 14,
        lineHeight: 18.23, fontWeight: "400",
        paddingHorizontal: responsiveWidth(2),
        alignSelf: "center",
        width: responsiveWidth(78),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.4

    },
    borderStyleScreen: {
        height: 36, width: responsiveWidth(92), borderRadius: 4, borderWidth: 1, borderColor: Colors.white,
        opacity: 0.2, backgroundColor: Colors.white, marginBottom: 10
    }
});