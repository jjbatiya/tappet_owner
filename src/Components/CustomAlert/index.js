import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from "react-native-modal";
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';

const CustomAlert = (props) => {
    
    return (
        <Modal
            isVisible={props.isConfirmation}
            onRequestClose={() => props.cancelClick()}
            
        >
            <View style={styles.modal}>
                <Text style={styles.txtTitle}>{props.title}</Text>
                <Text style={styles.txtMessage}>{props.message}</Text>
                <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
                    <TouchableOpacity
                        onPress={() => props.cancelClick()}
                        style={styles.btnCancel}>
                        <Text style={styles.txtButton}>
                            CANCEL
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => props.okClick()}
                        style={[styles.btnCancel, { marginLeft: 10 }]}>
                        <Text style={styles.txtButton}>
                            OK
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modal: {
        backgroundColor: Colors.dialogBackground,
        borderRadius: 4,
        paddingHorizontal: 20,
        paddingTop: 20,
        paddingBottom: 10,
    },
    txtMessage: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansBold,
        paddingTop: 5,
        opacity: 0.6
    },
    btnCancel: {
        padding: 10,
    },
    txtTitle: {
        fontSize: 16,
        color: Colors.white,
        fontFamily: Fonts.DMSansBold,
        fontWeight: "700",
        lineHeight: 22
    },
    txtButton: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansBold,
        opacity: 0.3
    },
})
export default CustomAlert;