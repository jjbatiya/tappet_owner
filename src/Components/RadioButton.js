import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, FlatList } from 'react-native';
import { Fonts } from '../Resources/Fonts';
export default class RadioButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: this.props.selectRadio != undefined ? this.props.selectRadio : null,
        };
    }
    componentDidMount() {
        this.setState({
            value: this.props.selectRadio
        })
    }
    render() {
        const { PROP } = this.props;
        const { value } = this.state;
        const { style } = this.props

        return (
            <View style={style}>
                <Text style={{ fontSize: 12, lineHeight: 15.62, fontWeight: "400", color: "white", fontFamily: Fonts.DMSansRegular, opacity: 0.6 }}>Gender*</Text>
                <FlatList
                    data={PROP}
                    numColumns={2}
                    style={{ marginTop: 10 }}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        value: item.key,
                                    });
                                    this.props.onClickGender(item, index)
                                }}
                                key={item.key} style={styles.container}>
                                <View
                                    style={value === item.key ? styles.selectradioCircle : styles.radioCircle}
                                >

                                    {value === item.key && <View style={styles.selectedRb} />}
                                </View>
                                <Text style={styles.radioText}>{item.title}</Text>

                            </TouchableOpacity>
                        )
                    }}

                >
                </FlatList>

                {/* <Text> Selected: {this.state.value} </Text> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginBottom: 15,
        alignItems: 'center',
        flexDirection: 'row',
        width: "50%"
    },
    radioText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '400',
        lineHeight: 22,
        marginLeft: 12,
        fontFamily: Fonts.DMSansRegular

    },
    radioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectradioCircle: {
        height: 20,
        width: 20,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: '#EA8FFA',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectedRb: {
        width: 10,
        height: 10,
        borderRadius: 50,
        backgroundColor: '#EA8FFA',
    },
    result: {
        marginTop: 20,
        color: 'white',
        fontWeight: '600',
        backgroundColor: '#F3FBFE',
    },
});