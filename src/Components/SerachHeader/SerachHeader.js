import React from 'react';
import {
    View, StyleSheet, Image, Text,
    TouchableOpacity, ActivityIndicator, SafeAreaView, TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveWidth } from 'react-native-responsive-dimensions';

import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import { Images } from '../../Resources/Images';
import { strings } from '../../Resources/Strings';
import * as Constant from '../../utils/Constant';
//import { RFPercentage, RFValue } from '../../Library/DaynamicSize';
import StatusBar from '../AppHeader/StatusBar'
import { PlaceIconUnselect } from '../../utils/svg/PlaceIconUnselect';
const SerachHeader = (props) => {
    return (
        <View>
            <StatusBar />
            <View style={styles.header}>
                <TouchableOpacity onPress={() => props.onBackPress()}>
                    <Image resizeMode={"contain"} style={styles.backImgStyle} source={Images.backArrow}></Image>
                </TouchableOpacity>
                <View style={{ marginHorizontal: responsiveWidth(4) }}>
                    <View style={styles.borderStyle}></View>
                    <View style={{ flexDirection: 'row', alignItems: "center", justifyContent: 'space-between', position: "absolute", height: 36 }}>
                        <TextInput
                            style={styles.textInputStyle}
                            placeholder={props.placeHolder}
                            placeholderTextColor={Colors.grey}
                            onChangeText={props.onChangeText}
                            onSubmitEditing={props.onSubmitText}
                        >
                        </TextInput>
                        {props.isPlace ?
                            <TouchableOpacity onPress={() => props.onPlaceClick()}>
                                <PlaceIconUnselect opacity={1} width={15} height={15}/>
                            </TouchableOpacity>
                            : null}
                    </View>
                </View>
            </View>
        </View>
    )
}
export default SerachHeader;
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 70,
        width: '100%',
        // backgroundColor: white,
        //shadowColor: black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.10,
        shadowRadius: 1,
        elevation: 3,

    },
    backImgStyle: { marginLeft: 10, height: 20, width: 20, tintColor: "white" },
    borderStyle: { height: 36, width: responsiveWidth(84), borderRadius: 4, borderWidth: 1, borderColor: Colors.white, opacity: 0.2, backgroundColor: Colors.white },
    textInputStyle: {
        color: Colors.white, fontSize: 14,
        lineHeight: 18.23, fontWeight: "400",
        paddingHorizontal: responsiveWidth(3),
        alignSelf: "center",
        width: responsiveWidth(78),
        fontFamily: Fonts.DMSansRegular

    }
});