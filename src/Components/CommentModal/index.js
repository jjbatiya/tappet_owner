import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions, KeyboardAvoidingView, Platform, TextInput } from 'react-native';
import Modal from "react-native-modal";
import { responsiveHeight } from 'react-native-responsive-dimensions';
import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import CommonButton from '../CommonButton';
const { width, height } = Dimensions.get('window');
var FloatingLabel = require('react-native-floating-labels');


const CommentModal = (props) => {
    const { isModal } = props
    const [commentText, setCommentText] = useState("")

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={isModal}
            
        >
            {/*All views of Modal*/}
            {/* <KeyboardAvoidingView   behavior={Platform.OS === "android" ? "height" : "position"}> */}
            <KeyboardAvoidingView style={{}}>

                <View style={styles.modal}>
                    <Text style={styles.txtRate}>{"Add Comment"}</Text>

                    <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        multiline={true}
                        value={commentText}
                        onChangeText={(text) => setCommentText(text)}

                    >{"Comment..."}</FloatingLabel>
                    <View style={{ flexDirection: "row", alignSelf: "flex-end", marginTop: 20 }}>
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={[Colors.opacityColor2, Colors.opacityColor2]}
                            btnText={styles.btnText}
                            viewStyle={{ marginTop: 10, flex: 0.3, marginRight: 10 }}
                            onPress={() => props.onClose()}
                            text={"Cancel"}
                        />
                        <CommonButton
                            btnlinearGradient={styles.btnlinearGradient}
                            colors={Colors.btnBackgroundColor}
                            btnText={styles.btnText}
                            viewStyle={{ marginVertical: 10, flex: 0.4 }}
                            onPress={() => props.onAddComment(commentText)}
                            text={"Add Comment"}
                        />
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Modal >

    )
}
const styles = StyleSheet.create({
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 210,
        padding: 10,
        borderRadius: 4,
        marginTop: responsiveHeight(10),
        width: '98%'
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    formInput: {
        borderBottomWidth: 1,
        borderColor: Colors.white,

    },
    input: {
        borderWidth: 0,
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        width: width - 50,
        height: 60
    },
    labelInput: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.white,
        opacity: 0.6,
        marginLeft: -8,
        fontFamily: Fonts.DMSansRegular,
    },
    txtRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "700",
        fontSize: 16, lineHeight: 22, textAlign: "center", color: Colors.white,
        marginTop: 5
    },
    txtLabelRate: {
        fontFamily: Fonts.DMSansRegular, alignSelf: "center", fontWeight: "400",
        fontSize: 14, lineHeight: 22, textAlign: "center", color: Colors.white,
        opacity: 0.6
    },
})
export default CommentModal;