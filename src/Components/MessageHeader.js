import React from 'react';
import {
    View, StyleSheet, Image, Text,
    TouchableOpacity, ActivityIndicator, SafeAreaView, Platform,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveWidth } from 'react-native-responsive-dimensions';

import { Colors } from '../Resources/Colors';
import StatusBar from '../Components/AppHeader/StatusBar'
import { Images } from '../Resources/Images';
import { Fonts } from '../Resources/Fonts';
import { CallIcon } from '../utils/svg/CallIcon';
const MessageHeader = props => {

    const { navigation, backButton, title, menuClick, callClick, uri } = props;
    const buttonPressed = () => {

        setTimeout(() => {
            navigation.goBack()
        }, 200)
    };

    return (
        <View>
            {Platform.OS == "ios" ?
                <StatusBar />
                : <></>}
            <View style={[styles.header]}>

                <View style={styles.leftView}>
                    <TouchableOpacity
                        onPress={buttonPressed}
                        style={styles.leftIconStyle}>
                        <Image
                            source={backButton}
                            style={{ height: 20, width: 20, tintColor: "white" }} />
                    </TouchableOpacity>
                    {uri != undefined ? <Image style={styles.peopleView}
                        source={{ uri: uri }}
                    >
                    </Image> : <View style={styles.peopleView} />}
                    <Text style={styles.titleText}>{title}</Text>
                </View>
                <View style={styles.rightView}>
                    <TouchableOpacity
                        onPress={() => callClick()}>
                        <CallIcon />
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={{padding:5}}
                        onPress={() => menuClick()}>
                        <Image style={styles.menuIcon} source={Images.menuDot}></Image>
                    </TouchableOpacity>

                </View>


            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: Colors.btnBackgroundColor,
    },
    titleText: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '500',
        color: Colors.white,
        lineHeight: 22,
        marginHorizontal: 5,
        fontFamily: Fonts.DMSansRegular,
        width: 200

    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 70,
        width: '100%',
        // backgroundColor: white,
        //shadowColor: black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.10,
        shadowRadius: 1,
        elevation: 3,

    },
    leftView: { flexDirection: "row", alignItems: "center", justifyContent: "center" },
    peopleView: { height: 40, width: 40, borderRadius: 20 },
    rightView: { flexDirection: "row", alignItems: "center", justifyContent: "center", marginHorizontal: responsiveWidth(3) },
    leftIconStyle: {
        paddingHorizontal: 10,
    },
    callIcon: { height: 14, width: 14, tintColor: Colors.white },
    menuIcon: { height: 14, width: 14, tintColor: Colors.white, marginLeft: responsiveWidth(3) }
});
export default MessageHeader;