import React, { useEffect, useRef } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import RBSheet from "react-native-raw-bottom-sheet";
import { Colors } from "../../Resources/Colors";
import { Fonts } from "../../Resources/Fonts";
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import CallFriends from "../../utils/svg/CallFriends";
import ChatMessage from "../../utils/svg/ChatMessage";
const BottomCallMessageView = (props) => {
    const { isMore } = props
    let refSheet = useRef();

    useEffect(() => {
        if (isMore == true) {
            refSheet.open();
        }
        else {
            refSheet.close();
        }
    }, [isMore])
    const callClick = () => {
        props.onCall();
    }
    const onMessage = () => {
        props.onMessage();
    }
    const bottomView = () => {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={styles.txtHeading}>Choose Connection Type</Text>
                <Text style={styles.txtLabel}>How would you like to connect?</Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: responsiveWidth(10) }}>
                    <View>
                        <TouchableOpacity onPress={() => callClick()} style={styles.viewRound}>
                            <CallFriends />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Call</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => onMessage()} style={styles.viewRound}>
                            <ChatMessage />
                        </TouchableOpacity>
                        <Text style={styles.txtCall}>Message</Text>
                    </View>
                </View>
            </View>
        )
    }
    return (
        <RBSheet
            ref={(ref) => refSheet = ref}
            height={290}
            openDuration={250}
            closeOnDragDown={true}
            customStyles={{
                container: {
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                    backgroundColor: Colors.dialogBackground,
                    paddingHorizontal: 25,
                    paddingVertical: 10
                }
            }}
        >
            {bottomView()}
        </RBSheet>
    )
}
const styles = StyleSheet.create({
    //bottom view
    txtHeading: {
        fontWeight: "700", fontSize: 16, lineHeight: 22,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        alignSelf: "center"
    },
    txtLabel: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        letterSpacing: 0.1, opacity: 0.6,
        alignSelf: "center", marginTop: 10

    },
    viewRound: {
        width: 100, height: 100,
        backgroundColor: Colors.opacityColor2,
        borderRadius: 50, alignItems: "center",
        justifyContent: "center",
        marginTop: 20
    },
    txtCall: {
        fontWeight: "400", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontFamily: Fonts.DMSansRegular,
        letterSpacing: 0.1,
        alignSelf: "center",
        marginVertical: 10

    },
    btnlinearGradient: {
        height: 28,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10
    },
    btnText: {
        fontWeight: "400",
        fontSize: 12,
        lineHeight: 15.62,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,
        paddingHorizontal: responsiveWidth(5)
    },
    btnView: { alignItems: "center", justifyContent: "center" },
})
export default BottomCallMessageView;