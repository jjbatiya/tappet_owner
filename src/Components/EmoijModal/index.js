import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import Modal from "react-native-modal";
import { Colors } from '../../Resources/Colors';
import { emoij_reaction } from '../../utils/Constant';

const EmoijModal = (props) => {
    const { isModalVisible, setModalVisible,onSelectEmoij } = props
    const renderView = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={()=>onSelectEmoij(item)} style={{ alignSelf: "center" }}>
                <Text style={styles.txtStyle}>{item}</Text>
            </TouchableOpacity>
        )
    }
    return (
        <Modal
            isVisible={isModalVisible}
            onBackdropPress={() => setModalVisible()}
            backdropOpacity={0.1}
        >
            <View style={styles.viewMain}>
                <FlatList
                    data={emoij_reaction}
                    contentContainerStyle={styles.container}
                    horizontal
                    renderItem={renderView}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    viewMain: {
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.5)',
        borderRadius: 30, alignItems: "center",
        justifyContent: "center", alignSelf: "center",paddingHorizontal:5
    },
    container: {
        alignItems: "center",
        justifyContent: "center", alignSelf: "center"
    },
    txtStyle:{ marginHorizontal: 5, fontSize: 24 }
})
export default EmoijModal;