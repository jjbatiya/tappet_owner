import React, { useRef } from "react";
import { View, Dimensions, StyleSheet,Image } from 'react-native'
import MapView from 'react-native-maps';
import { Colors } from "../Resources/Colors";
import { Images } from "../Resources/Images";
import { mapStyle } from "../Resources/MapStyle";
import { strings } from "../Resources/Strings";
const { height, width } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0009;
 const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const EventMap = (props) => {
    let mapRef = useRef();

    return (
        <MapView
            customMapStyle={mapStyle}
            style={props.style}
            ref={mapRef}
            
            provider={"google"}
            mapType={strings.mapType}
            //     loadingEnabled={this.state.loadingEnabled}
            //   zoomEnabled={this.state.zoomEnabled}
            //   zoomTapEnabled={this.state.zoomTapEnabled}
            //   rotateEnabled={this.state.rotateEnabled}
            scrollEnabled={false}
            zoomEnabled={props?.zoomEnabled}
            initialRegion={{
                latitude: parseFloat(props.lat),
                longitude: parseFloat(props.long),
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }}
        >
            <MapView.Marker
                coordinate={{
                    latitude: parseFloat(props.lat),
                    longitude: parseFloat(props.long),
                }}
            >
                <View style={{alignItems:"center",justifyContent:"center"}}>
                    <View style={styles.roundView}>
                        <Image style={styles.imgPlace} resizeMode={"contain"} source={Images.marker}></Image>
                    </View>
                    <View style={styles.dotView} />
                </View>
            </MapView.Marker>
        </MapView>
    )
}
const styles = StyleSheet.create({
    roundView: {
        height: 24, width: 24,
        borderRadius: 12,
        backgroundColor: Colors.primaryViolet,
        alignItems: "center",
        justifyContent: "center"
    },
    imgPlace: { height: 10, width: 10,tintColor:Colors.theme },
    dotView: {
        height: 4, width: 4,
        borderRadius: 2,
        backgroundColor: Colors.primaryViolet,
        marginTop:3
    }
})
export default EventMap;
