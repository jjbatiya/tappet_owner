import React from 'react';
import {
    View, StyleSheet, Image, Text,
    TouchableOpacity, ActivityIndicator, SafeAreaView, Platform,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { responsiveWidth } from 'react-native-responsive-dimensions';

import { Colors } from '../../Resources/Colors';
import { Fonts } from '../../Resources/Fonts';
import * as Constant from '../../utils/Constant';
//import { RFPercentage, RFValue } from '../../Library/DaynamicSize';
import StatusBar from './StatusBar'
import CommonStatusBar from '../CommonStatusBar';

const AppHeader = props => {

    const {
        leftIconPress,
        navigation, title, backButton, rightTitleText,
        titleText, rightTitlePressed, headerBarViewStyle,
        spinner, serachIcon,
        previousScreen, rightIcon, rightSerachClick, Tab, rightIconRight, rightIconLeft,
        rightTitleRightPressed, rightTitleLeftPressed, isProfileImage, profileImage, leftIcon, isNotification,isMessageNotification
    } = props;



    // console.log("==>", backButton)
    const buttonPressed = () => {

        setTimeout(() => {
            navigation.goBack(null)
        }, 200)
    };

    return (
        <LinearGradient
            colors={!Tab ? Colors.btnBackgroundColor : ['#ffffff00', '#ffffff00']}

        >
            {Platform.OS == "ios" ?
                <StatusBar /> : <CommonStatusBar color={'#DD46F6'} />
            }
            <View style={[styles.header, headerBarViewStyle]}>
                {
                    !spinner && backButton &&
                    <TouchableOpacity
                        onPress={leftIconPress || buttonPressed}
                        style={styles.leftIconStyle}>
                        <Image
                            source={backButton}
                            style={{ height: 20, width: 20, tintColor: "white" }} />
                    </TouchableOpacity>
                }
                {
                    isProfileImage &&
                    <TouchableOpacity
                        onPress={leftIconPress || buttonPressed}
                        style={styles.leftIconStyle}>
                        <Image
                            source={profileImage}
                            style={{ height: 30, width: 30, borderRadius: 15 }} />
                    </TouchableOpacity>
                }
                <View style={styles.logoHolder}>
                    <Text numberOfLines={1} style={[styles.titleText, titleText]}>{title}</Text>
                </View>
                {
                    !spinner && rightIconLeft &&
                    <TouchableOpacity
                        onPress={rightTitleLeftPressed}
                        style={styles.rightIconLeftStyle}>
                        <Image
                            source={rightIconLeft}
                            style={{ height: 20, width: 20, tintColor: Colors.white }} />
                        {isNotification == true &&
                            <View style={{
                                height: 7, width: 7, borderRadius: 7, backgroundColor: Colors.redColor,
                                position: "absolute", top: 5, right: 7
                            }}>
                            </View>}
                    </TouchableOpacity>
                }
                {
                    leftIcon &&
                    <TouchableOpacity
                        onPress={rightTitleLeftPressed}
                        style={styles.LeftIconStyle}
                    >
                        <Image
                            source={leftIcon}
                            style={{ height: 18, width: 18, tintColor: Colors.white }} />
                    </TouchableOpacity>
                }
                {
                    !spinner && rightIconRight &&
                    <TouchableOpacity
                        onPress={rightTitleRightPressed}
                        style={styles.rightIconRightStyle}>
                        <Image
                            source={rightIconRight}
                            style={{ height: 20, width: 20, tintColor: Colors.white }} />
                        {isMessageNotification == true &&
                            <View style={{
                                height: 7, width: 7, borderRadius: 7, backgroundColor: Colors.redColor,
                                position: "absolute", top: 5, right: 5
                            }}>
                            </View>}
                    </TouchableOpacity>
                }
                {props.isSkip &&
                    <TouchableOpacity onPress={() => props.onSkip()}
                        style={{
                            width: 60, marginRight: 10, height: 28, backgroundColor: "white", marginTop: 20,
                            borderRadius: 4, alignItems: "center", justifyContent: "center"
                        }}>
                        <Text style={{
                            color: "#E25FF7", textAlign: "center",
                            fontSize: 12, lineHeight: 15.62, fontWeight: "500", fontFamily: Fonts.DMSansRegular
                        }}>{props.textSkip}</Text>
                    </TouchableOpacity>
                }
                {
                    rightIcon &&
                    // <TouchableOpacity
                    //     onPress={rightTitlePressed}
                    //     style={{ paddingHorizontal: responsiveWidth(10) }}>
                    //     <Image
                    //         source={rightIcon}
                    //         style={{ height: 30, width: 30 }} />
                    // </TouchableOpacity>
                    <TouchableOpacity
                        onPress={rightTitlePressed}
                        style={{
                            position: 'absolute',
                            top: 20,
                            right: 0,
                            padding: 5,
                            marginTop: 10,
                            borderRadius: 20,
                            marginRight: 10
                        }}
                    >
                        <Image
                            source={rightIcon}
                            style={{ height: 20, width: 20, tintColor: Colors.white }} />
                    </TouchableOpacity>
                }
                {
                    serachIcon &&
                    <TouchableOpacity
                        onPress={rightSerachClick}

                        style={{ alignItems: "center", justifyContent: "center" }}>
                        <View
                            style={{
                                width: 32, height: 32, opacity: 0.1,
                                backgroundColor: Colors.black,
                                borderRadius: 20, top: 11, right: 11,

                            }} />
                        <Image
                            source={serachIcon}
                            style={{ height: 13.33, width: 13.33, tintColor: Colors.white, position: "absolute", alignSelf: "center", top: 20, right: 20, }} />
                    </TouchableOpacity>
                }
            </View>
        </LinearGradient>
    );
};
const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: Colors.btnBackgroundColor,
    },
    titleText: {
        // fontFamily: MEDIUM,
        textAlign: 'center',
        // fontFamily: MEDIUM,
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: '700',
        color: Colors.white,
        paddingTop: 20,
        fontFamily: Fonts.DMSansBold
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 70,
        width: '100%',
        // backgroundColor: white,
        //shadowColor: black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.10,
        shadowRadius: 1,
        elevation: 3,

    },
    logoHolder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
        // alignItems: 'flex-start',
    },
    IconStyle: {
        height: 18, width: 18,
    },
    goBackIconStyle: {
        height: 14,
        width: 14,
        tintColor: Colors.white
    },
    disableBack: {
        height: 14,
        width: 14,
        //tintColor: darkGrey,
    },
    rightIconRightStyle: {
        position: 'absolute',
        top: 20,
        right: 10,
        padding: 5,
        marginTop: 10,
        backgroundColor: '#00000019',
        borderRadius: 20,
        marginRight: 10,

    },
    rightIconLeftStyle: {
        position: 'absolute',
        right: 50,
        top: 20,
        padding: 5,
        marginTop: 10,
        backgroundColor: '#00000019',
        //  opacity: 0.1,
        borderRadius: 20,
        marginRight: 10
    },
    leftIconStyle: {
        marginTop: 20,
        paddingHorizontal: 10,
        justifyContent: 'flex-start',
    },
    LeftIconStyle: {
        position: 'absolute',
        right: 50,
        top: 20,
        padding: 5,
        marginTop: 10,
        //  opacity: 0.1,
        borderRadius: 20,
        // marginRight: 10
    },
});
export default AppHeader;