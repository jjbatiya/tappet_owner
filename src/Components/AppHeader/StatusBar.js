import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { StatusBarHeight } from '../../Resources/StatusbarHeight';

const MyStatusBar = () => (
  <View
    style={styles.statusBar} >
    <StatusBar
      translucent
      barStyle="light-content"
    />
  </View>
)

function MyStatus() {
  return (
    <MyStatusBar />
  );
}

const styles = StyleSheet.create({
  statusBar: {
    //backgroundColor: white,
    height: StatusBarHeight,
  }
});
export default MyStatus