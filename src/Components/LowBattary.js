import React from 'react';
import { View, Text, StyleSheet, Modal, Image } from 'react-native';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors } from '../Resources/Colors';
import { Fonts } from '../Resources/Fonts';
import { Images } from '../Resources/Images';
import { strings } from '../Resources/Strings';
import CommonButton from './CommonButton';

const LowBattary = (props) => {
    const {
        isVisible, textHeader, iconTop,
        txtDetail, btnText, clickBtn, isPeople, cancelBtn
    } = props
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={isVisible}
        >
            {/*All views of Modal*/}
            <View style={styles.modal}>
                {isPeople ?
                    <Image style={styles.peopleIcon} source={iconTop}></Image> :
                    <Image style={styles.modalIconStyle} source={iconTop}></Image>
                }
                <Text style={styles.modalTitle}>{textHeader}</Text>
                <Text style={styles.modalDescription}>{txtDetail}</Text>
                <CommonButton
                    btnlinearGradient={styles.btnlinearGradient}
                    colors={Colors.btnBackgroundColor}
                    btnText={styles.btnText}
                    viewStyle={{ marginVertical: 10, width: 219 }}
                    onPress={clickBtn}
                    text={btnText}
                />
                {isPeople &&
                    <Text onPress={cancelBtn} style={styles.txtCancel}>{strings.cancelText}</Text>
                }

            </View>
        </Modal >

    )
}
const styles = StyleSheet.create({
    //modal view
    modal: {
        alignItems: 'center',
        backgroundColor: Colors.dialogBackground,
        alignSelf: "center",
        height: 260,
        width: '93%',
        borderRadius: 4,
        marginTop: responsiveHeight(30)
    },
    modalIconStyle: {
        tintColor: Colors.white,
        width: 25,
        height: 25,
        marginVertical: responsiveHeight(2)
    },
    modalTitle: {
        alignSelf: "center", fontSize: 16, lineHeight: 22, color: Colors.white, fontWeight: "700",
        fontFamily: Fonts.DMSansBold
    },
    modalDescription: {
        textAlign: "center", fontSize: 14, lineHeight: 18.23,
        color: Colors.white, fontWeight: "400", width: 295,
        marginVertical: responsiveHeight(1.2),
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    txtCancel: {
        marginTop: responsiveHeight(1), alignSelf: "center",
        fontSize: 14, lineHeight: 18.23, color: Colors.white, fontWeight: "400",
        fontFamily: Fonts.DMSansRegular,
        opacity: 0.6
    },
    btnlinearGradient: {
        flexDirection: 'row',
        height: 44,
        borderRadius: 5,
        // marginHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 18.23,
        color: Colors.white,
        fontFamily: Fonts.DMSansRegular,

    },
    peopleIcon:{
        width:40,
        height:40,
        borderRadius:20,
        borderWidth:1,borderColor:Colors.opacityColor3,
        marginVertical: responsiveHeight(2)

        
    }
})
export default LowBattary;