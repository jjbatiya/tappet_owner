import React, { useState } from 'react';

import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const PercentageBar = ({
    navigation,
    percentage,
    height,
    backgroundColor,
    completedColor,
}) => {
    const [getPercentage, setPercentage] = useState(percentage);
    const [getheight, setHeight] = useState(height);
    const [getBackgroundColor, setBackgroundColor] = useState(backgroundColor);
    const [getCompletedColor, setCompletedColor] = useState(completedColor);
    return (
        
            <View style={{flex: 0.95,height:getheight ,marginTop:3}}>
                <View
                    style={{
                        width: '100%',
                        height: getheight,
                        borderRadius: 10,
                        borderColor: getBackgroundColor,
                        borderWidth: 1,
                        bottom:0
                    }}
                />
                <View
                    style={{
                        width: getPercentage ? getPercentage : 0,
                        height: getheight,
                        borderRadius: 10,
                        backgroundColor: getCompletedColor,
                        position: 'absolute',
                        bottom: 0
                    }}
                />
                
            </View>
        
    );
};
export default PercentageBar;