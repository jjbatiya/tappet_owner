import React, { useEffect, useRef } from 'react';
import RBSheet from "react-native-raw-bottom-sheet";
import { Images } from '../../Resources/Images';
import MoreMenu from '../MoreMenu';
import ImagePicker from 'react-native-image-crop-picker';
import { Dimensions, PermissionsAndroid, Platform } from 'react-native';
import { Colors } from '../../Resources/Colors';
const { width ,height} = Dimensions.get('window');

const moreData = [

    {
        key: "Camera",
        title: "Take a photo",
        image: Images.cameraIcon
    },
    {
        key: "Gallery",
        title: "Upload from gallery",
        image: Images.addPicture
    },
]
const ImagePickerOption = (props) => {
    let refSheet = useRef();

    useEffect(() => {
        if (props.isRBShowing == true || props.isRBShowing == false)
            refSheet?.open()

    }, [props.isRBShowing])
    const moreItemClick = (item) => {
        refSheet.close();
        if (item.key == "Camera") {
            setTimeout(() => {
                cameraChoose();
            }, 500);
        }
        else {
            setTimeout(() => {
                galleryChoose();
            }, 500);
        }
    }
    const cameraChoose = async () => {
        if (Platform.OS == "android") {
            let isCameraPermitted = await requestCameraPermission();
            let isStoragePermitted = await requestExternalWritePermission();
            if (isCameraPermitted && isStoragePermitted) {
            }
            else {
                return 0;
            }
        }
        ImagePicker.openCamera({
            compressImageQuality: 0.6,
            cropping: true,
            compressImageMaxWidth:width,
            compressImageMaxHeight:height
            
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                props.clickItemMenu(obj)
                //  setImageData(obj)
                //setImagePath(image?.path)
            } catch (error) {

            }
        });
    }
    const galleryChoose = () => {
        ImagePicker.openPicker({
            compressImageQuality: 0.6,
            cropping: true,
            compressImageMaxWidth:width,
            compressImageMaxHeight:height
        
        }).then(image => {
            try {
                var uri = image?.path;
                var fileName = uri.substr(uri.lastIndexOf("/") + 1)

                var obj = {
                    type: image?.mime,
                    uri: image?.path,
                    fileName: fileName
                }
                props.clickItemMenu(obj)
                //  setImageData(obj)
                //setImagePath(image?.path)
            } catch (error) {

            }
        });
    }
    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };
    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };
    return (
        <RBSheet
            ref={(ref) => refSheet = ref}
            height={140}
            openDuration={250}
            closeOnDragDown={true}
            customStyles={{
                container: {
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                    backgroundColor: Colors.dialogBackground,
                    padding: 10,
                }
            }}
        >
            <MoreMenu
                data={moreData}
                clickItem={moreItemClick}
            >

            </MoreMenu>
        </RBSheet>
    )
}
export default ImagePickerOption;