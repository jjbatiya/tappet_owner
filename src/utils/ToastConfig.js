import React, { useState } from 'react';
import Toast, { BaseToast, ErrorToast } from 'react-native-toast-message';
import { Colors } from '../Resources/Colors';
export const toastConfig = {
    /*
      Overwrite 'success' type,
      by modifying the existing `BaseToast` component
    */
    success: (props) => (
      <BaseToast
        {...props}
        style={{ borderLeftColor: Colors.primaryViolet ,backgroundColor:Colors.theme}}
        contentContainerStyle={{ paddingHorizontal: 15 }}
        text1Style={{
            fontSize: 14,
            color:Colors.white
          }}
          text2Style={{
            fontSize: 12,
            color:Colors.opacityColor3
          }}
      />
    ),
    /*
      Overwrite 'error' type,
      by modifying the existing `ErrorToast` component
    */
    error: (props) => (
      <ErrorToast
        {...props}
        style={{ borderLeftColor: Colors.primaryViolet,backgroundColor:Colors.theme }}
        text1Style={{
          fontSize: 14,
          color:Colors.white
        }}
        text2Style={{
          fontSize: 12,
          color:Colors.opacityColor3
        }}
      />
    ),
    /*
      Or create a completely new type - `tomatoToast`,
      building the layout from scratch.
  
      I can consume any custom `props` I want.
      They will be passed when calling the `show` method (see below)
    */
   
  };