import React from 'react';
import Svg, { Path } from "react-native-svg"
export const CallDropIcon = (props) => (
    <Svg
        width={28}
        height={28}
        viewBox="0 0 28 28"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M9.266 23.8h-2.95a1.963 1.963 0 01-1.872-1.34 1.975 1.975 0 01-.095-.809 19.527 19.527 0 013.02-8.505 19.197 19.197 0 015.9-5.912 19.439 19.439 0 018.528-3.026 1.963 1.963 0 011.976 1.167c.11.25.167.52.168.794v2.957a1.973 1.973 0 01-1.692 1.97c-.944.126-1.871.357-2.764.69a1.964 1.964 0 01-2.075-.443l-1.25-1.252a15.752 15.752 0 00-5.9 5.913l1.249 1.252a1.973 1.973 0 01.442 2.08 12.675 12.675 0 00-.688 2.769A1.972 1.972 0 019.266 23.8z"
            fill="#fff"
            stroke="#fff"
            strokeWidth={1.75}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)