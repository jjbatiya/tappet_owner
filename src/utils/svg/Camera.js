import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Camera(props) {
    return (
        <Svg
            width={20}
            height={20}
            viewBox="0 0 20 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                clipRule="evenodd"
                d="M14.571 5.847v0a.778.778 0 01-.705-.467c-.233-.515-.529-1.173-.704-1.532-.259-.533-.678-.843-1.258-.847H8.096c-.58.004-.999.314-1.258.847-.174.359-.47 1.017-.703 1.532a.78.78 0 01-.706.467v0c-1.618 0-2.929 1.37-2.929 3.061v5.03C2.5 15.629 3.811 17 5.43 17h9.141c1.618 0 2.929-1.371 2.929-3.061v-5.03c0-1.691-1.311-3.062-2.929-3.062z"
                stroke="#fff"
                strokeWidth={1.25}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                clipRule="evenodd"
                d="M7.352 11.112a2.658 2.658 0 002.65 2.655 2.659 2.659 0 002.647-2.65 2.655 2.655 0 00-2.646-2.657c-1.468-.002-2.663 1.21-2.651 2.652z"
                stroke="#fff"
                strokeWidth={1.25}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Path
                d="M14.086 9.163a.84.84 0 01-.517-.25.85.85 0 01-.235-.58.85.85 0 01.265-.613.955.955 0 01.254-.162.852.852 0 01.901.18.8.8 0 01.158.217l.02.05c.045.103.069.215.069.329 0 .218-.085.42-.244.587a.83.83 0 01-.507.242l-.083.004-.08-.004z"
                fill="#fff"
            />
        </Svg>
    )
}

export default Camera
