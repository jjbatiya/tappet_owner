import React from 'react';
import Svg, { Path } from "react-native-svg"
export const AddUser = (props) => (
    <Svg
        width={20}
        height={20}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            clipRule="evenodd"
            d="M8.231 12.67c-3.203 0-5.939.485-5.939 2.425s2.719 2.442 5.94 2.442c3.203 0 5.939-.485 5.939-2.424 0-1.94-2.719-2.442-5.94-2.442zM8.231 9.904a3.793 3.793 0 003.807-3.806A3.793 3.793 0 008.231 2.29a3.793 3.793 0 00-3.806 3.807A3.794 3.794 0 008.23 9.904z"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <Path
            d="M16.004 7.224v3.341M17.709 8.894H14.3"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)