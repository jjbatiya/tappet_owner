import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const BackgroundChat = (props) => (
    <Svg
        width={295}
        height={62}
        viewBox="0 0 295 62"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M15.442 0c-4.486 0-8.123 3.208-8.123 7.164v46.36a13.495 13.495 0 01-3.143 4.316c-1.18 1.074-2.56 1.89-3.883 2.504C-.254 60.598.023 62 .64 62h286.236c4.486 0 8.123-3.208 8.123-7.164V7.164C295 3.208 291.363 0 286.877 0H15.442z"
            fill="#fff"
            fillOpacity={0.2}
        />
    </Svg>
)