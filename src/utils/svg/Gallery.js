import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Gallery(props) {
  return (
    <Svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        clipRule="evenodd"
        d="M17.5 6.686v6.627c0 2.455-1.536 4.187-3.99 4.187H6.482c-2.454 0-3.982-1.732-3.982-4.187V6.686C2.5 4.231 4.036 2.5 6.482 2.5h7.028c2.454 0 3.99 1.731 3.99 4.186z"
        stroke="#fff"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M5 14l1.141-1.169a1.07 1.07 0 011.517-.006l.66.655c.446.441 1.187.41 1.591-.068l1.664-1.965a1.284 1.284 0 011.877-.075L15 12.923"
        stroke="#fff"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        clipRule="evenodd"
        d="M9 7.5a1.501 1.501 0 11-3.002-.002A1.501 1.501 0 019 7.5z"
        stroke="#fff"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default Gallery
