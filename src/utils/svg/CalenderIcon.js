import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const CalenderIcon = (props) => (
    <Svg
        width={18}
        height={18}
        viewBox="0 0 18 18"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M1.577 6.924H16.43M12.701 10.048h.008M9.004 10.048h.008M5.298 10.048h.008M12.701 13.157h.008M9.004 13.157h.008M5.298 13.157h.008M12.37 1v2.633M5.638 1v2.633"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <Path
            clipRule="evenodd"
            d="M12.532 2.263H5.476C3.029 2.263 1.5 3.572 1.5 5.978v7.24C1.5 15.66 3.029 17 5.476 17h7.048c2.455 0 3.976-1.316 3.976-3.722v-7.3c.008-2.406-1.513-3.715-3.968-3.715z"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)
