import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
import { Colors } from '../../Resources/Colors';
export const PlaceIconUnselect = (props) => (
    <Svg
    width={18}
    height={22}
    viewBox="0 0 18 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <Path
      clipRule="evenodd"
      d="M9 20.5c-1.279 0-8-5.385-8-11.017C1 5.075 4.58 1.5 9 1.5c4.418 0 8 3.575 8 7.983C17 15.115 10.278 20.5 9 20.5z"
      stroke={props.opacity==undefined?Colors.opacityColor3:"white"}
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      clipRule="evenodd"
      d="M11.5 9a2.5 2.5 0 10-5 0 2.5 2.5 0 005 0z"
      stroke={props.opacity==undefined?Colors.opacityColor3:"white"}
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
  
)