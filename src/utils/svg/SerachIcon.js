import React from 'react';
import Svg, { Circle, Path, G } from 'react-native-svg';
export const SerachIcon = (props) => (

    <Svg
      width={22}
      height={22}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M9.944 18.39a8.445 8.445 0 100-16.89 8.445 8.445 0 000 16.89z"
        fill={props.fillColor}
        stroke={props.storke}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M20.5 20.5l-4.592-4.592"
        stroke={props.storke}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
)