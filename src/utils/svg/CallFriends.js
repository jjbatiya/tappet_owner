import * as React from "react"
import Svg, { Path } from "react-native-svg"

function CallFriends(props) {
  return (
    <Svg
      width={32}
      height={32}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M27.2 21.57v3.373a2.243 2.243 0 01-.73 1.665 2.255 2.255 0 01-1.725.583 22.318 22.318 0 01-9.72-3.451 21.941 21.941 0 01-6.757-6.744A22.216 22.216 0 014.81 7.25a2.244 2.244 0 011.333-2.258c.286-.127.595-.192.908-.192h3.379a2.255 2.255 0 012.252 1.933c.143 1.08.408 2.139.789 3.159a2.244 2.244 0 01-.507 2.372l-1.43 1.427a18.002 18.002 0 006.757 6.744l1.43-1.427a2.253 2.253 0 012.377-.506c1.022.38 2.084.644 3.165.787A2.253 2.253 0 0127.2 21.57z"
        fill="#fff"
        stroke="#fff"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}
export default CallFriends;