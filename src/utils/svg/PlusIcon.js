import * as React from "react"
import Svg, { Path } from "react-native-svg"

export const PlusIcon = (props) => (
    <Svg
        width={20}
        height={20}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M10 4v12M4 10h12"
            stroke={props.color == undefined ? "#fff" : props.color}
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)
