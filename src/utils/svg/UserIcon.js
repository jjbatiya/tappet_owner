import React from 'react';
import Svg, { Path } from 'react-native-svg';
export const UserIcon = (props) => (
    <Svg
        width={20}
        height={20}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            clipRule="evenodd"
            d="M10 12.88c-3.236 0-6 .484-6 2.424s2.746 2.442 6 2.442c3.236 0 6-.485 6-2.424 0-1.94-2.746-2.442-6-2.442zM10 10.113c2.135 0 3.846-1.694 3.846-3.806 0-2.114-1.71-3.807-3.846-3.807-2.134 0-3.846 1.693-3.846 3.807 0 2.112 1.712 3.806 3.846 3.806z"
            stroke={props.stroke==undefined?"#fff":props.stroke}
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)