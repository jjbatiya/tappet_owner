import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const CallIcon = (props) => (
    <Svg
        width={20}
        height={20}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M17 13.482v2.107a1.403 1.403 0 01-.957 1.337 1.41 1.41 0 01-.578.068 13.95 13.95 0 01-6.075-2.156 13.714 13.714 0 01-4.223-4.216 13.885 13.885 0 01-2.161-6.09 1.402 1.402 0 01.833-1.412A1.41 1.41 0 014.407 3h2.111a1.41 1.41 0 011.408 1.208c.09.675.255 1.337.493 1.974a1.402 1.402 0 01-.317 1.483l-.894.892a11.25 11.25 0 004.224 4.215l.894-.892a1.409 1.409 0 011.485-.316 9.059 9.059 0 001.978.492 1.408 1.408 0 011.21 1.426z"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)
