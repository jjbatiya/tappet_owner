import React from 'react';
import Svg, { Path,Rect } from 'react-native-svg';
export const EmailIcon = (props) => (
    <Svg
    width={33}
    height={32}
    viewBox="0 0 33 32"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <Path
      d="M29.3 9.6l-9.652 7.029c-1.826 1.295-4.387 1.295-6.213 0L3.7 9.6"
      stroke="#fff"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Rect
      x={3.7}
      y={4.8}
      width={25.6}
      height={22.4}
      rx={5}
      stroke="#fff"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
)