import React from 'react';
import Svg, {  Path } from 'react-native-svg';
export const UploadImageIcon = (props) => (
    <Svg
        width={34}
        height={34}
        viewBox="0 0 34 34"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7.334.334a7 7 0 00-7 7v19.333a7 7 0 007 7h19.333a7 7 0 007-7V7.334a7 7 0 00-7-7H7.334zm-.239 11.888a4.173 4.173 0 118.347 0 4.173 4.173 0 01-8.347 0zm18.302 3.99c-1.665-1.837-4.48-1.699-5.981.267l-3.605 4.715c-.408.534-1.096.556-1.53.08l-1.428-1.566a3.413 3.413 0 00-5.15.026l-1.641 1.86a4.583 4.583 0 00-1.145 3.03v1.542a2.917 2.917 0 002.917 2.916h20c.69 0 1.25-.56 1.25-1.25v-5.86c0-1.16-.44-2.277-1.23-3.125l-2.457-2.635z"
            fill="#fff"
        />
    </Svg>
)
