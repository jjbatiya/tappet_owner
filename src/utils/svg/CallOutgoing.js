import React from 'react';
import Svg, { G, Path } from "react-native-svg"
export const CallOutgoing = (props) => (
    <Svg
        width={16}
        height={16}
        viewBox="0 0 16 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <G
            opacity={0.6}
            stroke="#fff"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <Path d="M13.6 10.786v1.686a1.124 1.124 0 01-1.228 1.124 11.157 11.157 0 01-4.86-1.726 10.97 10.97 0 01-3.379-3.372 11.108 11.108 0 01-1.728-4.872A1.122 1.122 0 013.525 2.4h1.69a1.128 1.128 0 011.126.967c.071.54.203 1.07.394 1.58a1.122 1.122 0 01-.253 1.185l-.716.714a9.001 9.001 0 003.38 3.372l.714-.714a1.127 1.127 0 011.189-.253c.51.19 1.041.323 1.582.394a1.127 1.127 0 01.969 1.14zM13.6 2.4l-3.2 3.2M11.2 2.4h2.4v2.4" />
        </G>
    </Svg>

)