import React from 'react';
import Svg, { G, Path, Circle } from "react-native-svg"
export const CommentIcon = (props) => (
    <Svg
        width={20}
        height={20}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <G opacity={0.6}>
            <Path
                d="M18 10a8 8 0 01-8 8H2.5c1-2 1-2 1.393-2.832A8 8 0 1118 10z"
                stroke="#fff"
                strokeWidth={1.25}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <Circle cx={7} cy={10} r={1} fill="#fff" />
            <Circle cx={10} cy={10} r={1} fill="#fff" />
            <Circle cx={13} cy={10} r={1} fill="#fff" />
        </G>
    </Svg>
)