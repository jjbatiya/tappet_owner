import * as React from "react"
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

function CallCancel(props) {
  return (
    <Svg
      width={30}
      height={30}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <G clipPath="url(#prefix__clip0_2878:2691)">
        <Path
          d="M7.59 17.488l-1.49-1.49a1.402 1.402 0 01-.27-1.622c.088-.176.21-.332.36-.457a13.948 13.948 0 015.821-2.77 13.713 13.713 0 015.967-.007 13.887 13.887 0 015.835 2.78 1.402 1.402 0 01.41 1.586 1.41 1.41 0 01-.317.487l-1.493 1.493a1.41 1.41 0 01-1.85.14 9.005 9.005 0 00-1.745-1.047 1.402 1.402 0 01-.824-1.272l.001-1.263a11.251 11.251 0 00-5.967.006v1.263a1.41 1.41 0 01-.828 1.274 9.05 9.05 0 00-1.746 1.05 1.408 1.408 0 01-1.864-.151z"
          fill="#fff"
        />
      </G>
      <Defs>
        <ClipPath id="prefix__clip0_2878:2691">
          <Path
            fill="#fff"
            transform="rotate(135 11.465 13.536)"
            d="M0 0h20v20H0z"
          />
        </ClipPath>
      </Defs>
    </Svg>
  )
}

export default CallCancel
