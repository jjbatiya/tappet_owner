import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const EditIcon = (props) => (
  <Svg
      width={20}
      height={19}
      viewBox="0 0 20 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M11.63 17.88h6.77"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        clipRule="evenodd"
        d="M10.734 2.334c.783-.977 2.047-.926 3.045-.16l1.475 1.135c.998.766 1.351 1.956.57 2.935l-8.8 11.002a1.583 1.583 0 01-1.22.59l-3.394.043-.769-3.24c-.108-.455 0-.934.294-1.303l8.8-11.002z"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M9.086 4.394l5.09 3.91"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
)