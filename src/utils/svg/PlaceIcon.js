import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const PlaceIcon = (props) => (
    <Svg
      width={18}
      height={22}
      viewBox="0 0 18 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.25 9.483C.25 4.66 4.168.75 9 .75c4.83 0 8.75 3.91 8.75 8.733 0 3.093-1.829 6.023-3.713 8.122a18.881 18.881 0 01-2.805 2.57 9.264 9.264 0 01-1.177.749c-.315.162-.7.326-1.056.326-.355 0-.74-.164-1.055-.326a9.269 9.269 0 01-1.177-.748 18.876 18.876 0 01-2.805-2.571C2.078 15.505.25 12.576.25 9.483zM9 5.75a3.25 3.25 0 100 6.5 3.25 3.25 0 000-6.5z"
        fill={props.fill==undefined?"#EA8FFA":props.fill}
      />
    </Svg>
)