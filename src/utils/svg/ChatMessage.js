import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ChatMessage(props) {
  return (
    <Svg
      width={32}
      height={32}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M27.2 19.733a2.489 2.489 0 01-2.488 2.489H9.779L4.8 27.2V7.289a2.489 2.489 0 012.489-2.49h17.422A2.489 2.489 0 0127.2 7.29v12.444z"
        fill="#fff"
        stroke="#fff"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M10.4 11.1h11.2M10.4 15.3h11.2"
        stroke="#564C6C"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ChatMessage
