import * as React from "react"
import Svg, { G, Path } from "react-native-svg"

function Friends(props) {
  return (
    <Svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <G
        opacity={props.opacity==undefined?0.6:1}
        stroke="#fff"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <Path d="M13.35 9.11c1.517 0 2.747-1.177 2.747-2.629 0-1.45-1.23-2.628-2.747-2.628M14.613 11.967c.454.03.904.091 1.348.186.616.117 1.357.358 1.62.887.169.339.169.733 0 1.072-.262.529-1.004.77-1.62.892" />
        <Path
          clipRule="evenodd"
          d="M7.994 12.532c3.075 0 5.701.446 5.701 2.226 0 1.781-2.61 2.242-5.701 2.242-3.075 0-5.701-.445-5.701-2.226 0-1.78 2.61-2.242 5.7-2.242zM7.993 9.991c-2.028 0-3.653-1.555-3.653-3.496C4.34 4.555 5.965 3 7.993 3s3.654 1.556 3.654 3.495c0 1.94-1.626 3.496-3.654 3.496z"
        />
      </G>
    </Svg>
  )
}

export default Friends
