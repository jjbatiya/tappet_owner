import React from 'react';
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg"

export const GoogleIcon = (props) => (
    <Svg
      width={19}
      height={18}
      viewBox="0 0 19 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <G clipPath="url(#prefix__clip0)" fillRule="evenodd" clipRule="evenodd">
        <Path
          d="M18.14 9.205c0-.639-.057-1.252-.164-1.841H9.5v3.481h4.844a4.14 4.14 0 01-1.796 2.716v2.259h2.908c1.702-1.567 2.684-3.874 2.684-6.615z"
          fill="#4285F4"
        />
        <Path
          d="M9.5 18c2.43 0 4.467-.806 5.956-2.18l-2.909-2.259c-.805.54-1.836.86-3.047.86-2.344 0-4.328-1.584-5.036-3.711H1.457v2.332A8.997 8.997 0 009.5 18z"
          fill="#34A853"
        />
        <Path
          d="M4.464 10.71A5.41 5.41 0 014.182 9c0-.593.102-1.17.282-1.71V4.958H1.457A8.996 8.996 0 00.5 9c0 1.452.348 2.827.957 4.042l3.007-2.332z"
          fill="#FBBC05"
        />
        <Path
          d="M9.5 3.58c1.321 0 2.508.454 3.44 1.345l2.582-2.58C13.962.891 11.926 0 9.5 0a8.997 8.997 0 00-8.043 4.958L4.464 7.29C5.172 5.163 7.156 3.58 9.5 3.58z"
          fill="#EA4335"
        />
      </G>
      <Defs>
        <ClipPath id="prefix__clip0">
          <Path fill="#fff" transform="translate(.5)" d="M0 0h18v18H0z" />
        </ClipPath>
      </Defs>
    </Svg>
)