import React from 'react';
import Svg, { Circle, Path,G } from 'react-native-svg';
export const EventIconUnselect = (props) => (
    <Svg
    width={24}
    height={24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <G
      opacity={props.opacity==undefined?0.4:props.opacity}
      clipRule="evenodd"
      stroke="#fff"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <Path d="M18.332 21A3.665 3.665 0 0022 17.336V14.42a2.416 2.416 0 01-2.422-2.418A2.415 2.415 0 0122 9.584l-.002-2.92A3.665 3.665 0 0018.33 3H5.669a3.666 3.666 0 00-3.668 3.664L2 9.678c1.34 0 2.42.984 2.42 2.324A2.416 2.416 0 012 14.42v2.916A3.665 3.665 0 005.667 21h12.665z" />
      <Path d="M12 14.078l1.628.897c.152.083.331-.052.302-.23l-.312-1.9 1.319-1.345c.123-.127.055-.345-.115-.372L13 10.851l-.815-1.73a.204.204 0 00-.373 0l-.815 1.73-1.82.277c-.17.027-.238.245-.115.372l1.318 1.344-.312 1.901c-.029.178.15.313.302.23L12 14.078z" />
    </G>
  </Svg>
)
