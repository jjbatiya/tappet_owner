import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
export const ChevronDown = (props) => (
    <Svg
        width={12}
        height={8}
        viewBox="0 0 12 8"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <Path
            d="M11 1.5l-5 5-5-5"
            stroke="#fff"
            strokeWidth={1.25}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </Svg>
)