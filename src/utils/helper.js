
import AsyncStorage from '@react-native-async-storage/async-storage';
import { comeChatUserTag, comeChat_apiKey, comeChat_appID, comeChat_region, restApiUrl } from './Constant';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { CometChat } from "@cometchat-pro/react-native-chat"
import { PermissionsAndroid, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { apiUrl } from '../Redux/services/apiUrl';

export const storeData = async (data) => {
    try {
        await AsyncStorage.setItem("userData", JSON.stringify(data))
    } catch (error) {
    }
}
export const getUserData = async () => {
    try {
        const value = await AsyncStorage.getItem("userData");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
    }
}
export const storeChatUserData = async (data) => {
    try {
        await AsyncStorage.setItem("chatUserData", JSON.stringify(data))
    } catch (error) {
    }
}
export const getChatUserData = async () => {
    try {
        const value = await AsyncStorage.getItem("chatUserData");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
    }
}
export const apiCall = async (data, url) => {
    let init = {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en"
        },
        body: JSON.stringify(data)
    };
    let APIURL = restApiUrl + "/" + url;
    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const getFirebaseToken = async () => {
    try {
        const value = await AsyncStorage.getItem("token");
        if (value !== null) {
            // We have data!!
            return value
        }
        else
            return ""
    } catch (error) {
    }
}
export const setFirebaseToken = async (token) => {
    try {
        await AsyncStorage.setItem("token", token)
    } catch (error) {
    }
}
export const getApiCall = async (url) => {
    let init = {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en"
        },
    };
    let APIURL = restApiUrl + "/" + url;
    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const apiCallWithToken = async (data, url, token) => {
    let init = {
        method: 'post',
        headers: {
            //'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en",
            "Authorization": "Bearer " + token
        },
        body: JSON.stringify(data)
    };
    let APIURL = restApiUrl + "/" + url;
    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const apiCallGetWithToken = async (url, token) => {
    let init = {
        method: "get",
        headers: {
            'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en",
            "Authorization": "Bearer " + token
        },
    };
    let APIURL = restApiUrl + "/" + url;
    //   console.log("data", { apiurl: APIURL ,token:token})

    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const imageUploadApi = async (data, url, token) => {
    let init = {
        method: 'post',
        headers: {
            // 'Accept': 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en",
            "Authorization": "Bearer " + token
        },
        body: data
    };
    let APIURL = restApiUrl + "/" + url;
    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const addressGet = (zipCode) => {
    return fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + zipCode + "&sensor=true&key=AIzaSyBhqD5BcsPIUmky6_n7ZjoeiwB3mEGLqiY").then(response => response.json()).then(res => {
        return res
    }).catch(err => {
        return { status: false, message: err.toString() }
    })

}
export const addressGetFromLatLng = (latitude, longitude) => {
    return fetch("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true&key=AIzaSyBhqD5BcsPIUmky6_n7ZjoeiwB3mEGLqiY").then(response => response.json()).then(res => {
        return res
    }).catch(err => {
        return { status: false, message: err.toString() }
    })

}
export const apiCallWithTokenAddPet = async (data, url, token) => {
    let init = {
        method: 'post',
        headers: {
            //'Accept': 'application/json',
            //"Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en",
            "Authorization": "Bearer " + token
        },
        body: data
    };
    let APIURL = restApiUrl + "/" + url;
    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const apiDeleteCall = async (data, url, token) => {
    let init = {
        method: 'DELETE',
        headers: {
            //'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken": "A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language": "en",
            "Authorization": "Bearer " + token
        },
        body: JSON.stringify(data)
    };
    let APIURL = restApiUrl + "/" + url;
    console.log("data", { apiurl: APIURL, data: init })

    return fetch(APIURL, init).then(response => response.json()).then(res => {
        return res

    }).catch(err => {
        return { status: false, message: err.toString() }
    })
}
export const createDynamicLinks = async (shareData) => {
    const link = await dynamicLinks().buildShortLink({
        link: "https://testquestion.com/id?share_id=" + shareData.id + "&flag=" + shareData.flag,
        // domainUriPrefix is created in your Firebase console
        domainUriPrefix: 'https://tappetlink.page.link',
        // optional setup which updates Firebase analytics campaign
        // "banner". This also needs setting up before hand
        // analytics: {
        //   campaign: 'banner',
        // },
        android: {
            packageName: 'com.tappetowner',
        },
        ios: {
            bundleId: 'com.tappetOwner'
        },
        social: {
            title: shareData.title,
            imageUrl: shareData.imageUrl,
            descriptionText: shareData.descriptionText
        },
        navigation: {
            forcedRedirectEnabled: true,
        }
    });
    return link;
    //apiKeyGenerate(link)
}

export const storeGroupId = async (data) => {
    try {
        await AsyncStorage.setItem("groupId", data + "")
    } catch (error) {
    }
}
export const getGroupId = async () => {
    try {
        const value = await AsyncStorage.getItem("groupId");
        if (value !== null) {
            // We have data!!
            return value + ""
        }
    } catch (error) {
        // Error retrieving data
    }
}
export const comeChatInit = () => {
    var appID = comeChat_appID;
    var region = comeChat_region;
    var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region).build();
    CometChat.init(appID, appSetting).then(
        () => {
            console.log("Initialization completed successfully");
            // You can now call login function.
        },
        error => {
            console.log("Initialization failed with error:", error);
            // Check the reason for error and take appropriate action.
        }
    );
}

export const comeChatLogin = async (u_id) => {
    var UID = comeChatUserTag + u_id;
    var response = null
    await CometChat.login(UID, comeChat_apiKey).then(
        user => {
            response = { status: true, response: user }
        },
        error => {
            console.log(error)
            response = { status: false, response: error.message }
        }
    );
    return response;
}
export const sendNormalMessage = async (message, id, flag, uri, tagData) => {

    var receiverID = id;
    var messageText = message;
    var receiverType = flag == "user" ? CometChat.RECEIVER_TYPE.USER : CometChat.RECEIVER_TYPE.GROUP;
    var textMessage = new CometChat.TextMessage(
        receiverID,
        messageText,
        receiverType
    );

    var textMessage = new CometChat.TextMessage(
        receiverID,
        messageText,
        receiverType
    );
    var response = null;
    var metadata = {
        senderImageUri: uri,
        tagData: tagData
    };
    textMessage.setMetadata(metadata)
    await CometChat.sendMessage(textMessage).then(
        message => {
            response = "Message sent successfully";
        },
        error => {
            console.log("Message sending failed with error:", error);
            response = "Message sending failed with error";
        }
    );
    return response;
}
export const mediaMessage = async (id, flag, file, caption, uri, tagData) => {
    var response = null;

    var receiverID = id;
    var messageType = CometChat.MESSAGE_TYPE.FILE;
    var receiverType = flag == "user" ? CometChat.RECEIVER_TYPE.USER : CometChat.RECEIVER_TYPE.GROUP;
    var mediaMessage = new CometChat.MediaMessage(
        receiverID,
        file,
        messageType,
        receiverType
    );
    caption != "" && mediaMessage.setCaption(caption)
    var metadata = {
        senderImageUri: uri,
        tagData: tagData
    };
    mediaMessage.setMetadata(metadata)
    await CometChat.sendMediaMessage(mediaMessage).then(
        message => {
            // Message sent successfully.
            response = message;
        },
        error => {
            response = error
            // Handle exception.
        }
    );
    return response;
}
export const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: 'Camera Permission',
                    message: 'App needs camera permission',
                },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            console.warn(err);
            return false;
        }
    } else return true;
};
export const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'External Storage Write Permission',
                    message: 'App needs write permission',
                },
            );
            return granted === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            alert('Write permission err', err);
        }
        return false;
    } else return true;
};
export const requestMicroPhonePermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                {
                    title: 'External Storage Write Permission',
                    message: 'App needs write permission',
                },
            );
            return granted === PermissionsAndroid.RESULTS.GRANTED;
        } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
        }
        return false;
    } else return true;
};
export const getGroupChatImages = async (group_uuid, type) => {
    var GUID = group_uuid;
    var limit = 30;
    var response = null;
    let messagesRequest = new CometChat.MessagesRequestBuilder()
        .setCategories(["message"])
        .setLimit(limit)
        .setType(type)
        .setGUID(GUID)
        .build();

    await messagesRequest.fetchPrevious().then(
        messages => {
            response = { status: true, result: messages }
        },
        error => {
            response = { status: false }
        }
    );
    return response;

}
export const storeLocationData = async (data) => {
    try {
        await AsyncStorage.setItem("locationData", JSON.stringify(data))
    } catch (error) {
    }
}
export const getLocationData = async () => {
    try {
        const value = await AsyncStorage.getItem("locationData");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
    }
}
export const getPermissions = async () => {
    if (Platform.OS === 'android') {
        let granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ]);
        if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
            granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            ]);
        }
    }
};
export const sendEventMessage = async (id, uri, eventData, type, tagData) => {
    let receiverID = id;
    let customData = {}
    if (type == "Event")
        customData = { eventData: eventData }
    else if (type == "Location")
        customData = { locationData: eventData }

    let customType = type;
    let receiverType = CometChat.RECEIVER_TYPE.GROUP;
    let customMessage = new CometChat.CustomMessage(receiverID, receiverType, customType, customData);
    var metadata = {
        pushNotification: type == "Location" ? "Share Location in group" : "Share Play Date in group",
        senderImageUri: uri,
        tagData: tagData,

    };
    customMessage.setMetadata(metadata)
    CometChat.sendCustomMessage(customMessage).then(
        message => {
            console.log("custom message sent successfully", message);
        }, error => {
            console.log("custom message sending failed with error", error);
        }
    );
}
export const sendUserEventMessage = async (id, uri, eventData, type) => {
    let receiverID = id;
    let customData = {}
    if (type == "Event")
        customData = { eventData: eventData }
    else if (type == "Location")
        customData = { locationData: eventData }

    let customType = type;
    let receiverType = CometChat.RECEIVER_TYPE.USER;
    let customMessage = new CometChat.CustomMessage(receiverID, receiverType, customType, customData);
    var metadata = {
        senderImageUri: uri,
    };
    customMessage.setMetadata(metadata)
    CometChat.sendCustomMessage(customMessage).then(
        message => {
            console.log("custom message sent successfully", message);
        }, error => {
            console.log("custom message sending failed with error", error);
        }
    );
}
export const getDeviceModal = () => {
    let model = DeviceInfo.getModel();
    return model;
}
export const getDeviceUid = () => {
    let deviceId = DeviceInfo.getUniqueId();
    return deviceId;
}

export const createCallLink = async (shareData) => {
    const link = await dynamicLinks().buildLink({
        link: "https://testquestion.com/id?share_id=" + shareData.id + "&flag=" + shareData.flag + "&sessionId=" + shareData.sessionId,
        // domainUriPrefix is created in your Firebase console
        domainUriPrefix: 'https://tappetlink.page.link',
        // optional setup which updates Firebase analytics campaign
        // "banner". This also needs setting up before hand
        // analytics: {
        //   campaign: 'banner',
        // },
        android: {
            packageName: 'com.tappetowner',
        },
        ios: {
            bundleId: 'com.tappetOwner'
        },
        social: {
            title: shareData.title,
            descriptionText: shareData.descriptionText
        },
        navigation: {
            forcedRedirectEnabled: true,
        }
    });
    return link;
    //apiKeyGenerate(link)
}
export const storeCallNotification = async (data) => {
    try {
        await AsyncStorage.setItem("notificationData", JSON.stringify(data))
    } catch (error) {
    }
}

export const getCallNotificationData = async () => {
    try {
        const value = await AsyncStorage.getItem("notificationData");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
    }
}
export const storeserCallNotification = async (data) => {
    try {
        await AsyncStorage.setItem("usernotificationData", JSON.stringify(data))
    } catch (error) {
    }
}
export const getCallUserNotificationData = async () => {
    try {
        const value = await AsyncStorage.getItem("usernotificationData");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
        return "";
    }
}
export const storeMessageNotification = async (data) => {
    try {
        await AsyncStorage.setItem("messageNotification", JSON.stringify(data))
    } catch (error) {
    }
}
export const getMessageNotification = async () => {
    try {
        const value = await AsyncStorage.getItem("messageNotification");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
        return ""
    }
}
export const storeIsNotification = async (data) => {
    try {
        await AsyncStorage.setItem("isNotify", JSON.stringify(data))
    } catch (error) {
    }
}
export const getIsNotification = async () => {
    try {
        const value = await AsyncStorage.getItem("isNotify");
        if (value !== null) {
            // We have data!!
            return JSON.parse(value)
        }
    } catch (error) {
        // Error retrieving data
        return ""
    }
}
export const storeNotificatioCount = async (data) => {
    try {
        await AsyncStorage.setItem("total_notification", JSON.stringify(data))
    } catch (error) {
    }
}
export const getNotificationCount = async () => {
    try {
        const value = await AsyncStorage.getItem("total_notification");
        if (value !== null && value != undefined) {
            // We have data!!
            return JSON.parse(value)
        }
        else
            return { notification_count: 0 }
    } catch (error) {
        // Error retrieving data
        return { notification_count: 0 }
    }
}
export const storeChatNotificationFlag = async (data) => {
    try {
        await AsyncStorage.setItem("chat_notify_flag", JSON.stringify(data))
    } catch (error) {
    }
}
export const getChatNotificationFlag = async () => {
    try {
        const value = await AsyncStorage.getItem("chat_notify_flag");
        if (value !== null && value != undefined) {
            // We have data!!
            return JSON.parse(value)
        }
        else
            return { chat_notification_flag: false }
    } catch (error) {
        // Error retrieving data
        return { chat_notification_flag: false }
    }
}
export const getCountUnreadMessage = async () => {
    var response = null;

    await CometChat.getUnreadMessageCount().then(
        array => {
            response = { status: true, result: array }
        }, error => {
            response = { status: false }
        }
    );
    return response;
}
