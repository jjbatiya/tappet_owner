import { restApiUrl } from "./Constant";


class RestClient {

    constructor(baseUrl) {
        this._baseUrl = baseUrl;
    }
    
    get(url) {
        return this._callWithoutBody('GET', url)
    }
    postWithBody(url,data) {
        return this._callWithBody('POST', url,data) 
    }
    
    _callWithoutBody(method, url) {
        let init = {
          method: method,
        };
        return this.callRequest(init, url)
    }
    _callWithBody(method, url,data) {
        let init = {
       
        method:method ,
        headers: {
            'Accept': 'application/json',
            "Content-Type": 'application/json',
            "appaccesstoken":"A7UVIN#3943=T@b^Nbdhb7s3Sf_v@p_B",
            "Accept-Language":"en"

        },
            body: JSON.stringify(data)
        };
        return this.callRequest(init, url)
    }
    
    callRequest(init, url){
        let APIURL = `${API_URL}/${url}`;
        return fetch(APIURL,init).then(response => response.json()).then(res => {
            return res
        
        }).catch(err => {
          console.log("error ==>" , err)
        })
          
    }
}

const API_URL = restApiUrl;

export const defaultRestClient = new RestClient(API_URL);