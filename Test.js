import React from 'react';
import { Image, Platform, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Colors } from './src/Resources/Colors';
import { Fonts } from './src/Resources/Fonts';
import { Images } from './src/Resources/Images';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };

const Test = () => {
    return (
        <SafeAreaView style={{ flex: 1, marginTop: 100 }}>
            <GooglePlacesAutocomplete
                ref={this.refValue}
                placeholder={"Enter Location"}
                autoFocus={false}

                fetchDetails={true}
                textInputProps={{

                    placeholderTextColor: Colors.black,
                    multiline: true

                }}
                styles={{
                    container: {
                        flex: 1,

                        //height:100
                    },

                    textInput: {
                        height: 44,
                        paddingVertical: Platform.OS == "ios" ? 0 : 5,
                        paddingHorizontal: 10,
                        fontSize: 16,
                        backgroundColor: Colors.opacityColor3,
                        color: Colors.black,
                        fontFamily: Fonts.DMSansRegular,
                        lineHeight: 22,
                        flex: 1,
                        paddingTop: Platform.OS == "ios" ? 10 : 0
                    }


                }}

                onPress={(data, details = null) => {
                }}
                query={{
                    key: 'AIzaSyBhqD5BcsPIUmky6_n7ZjoeiwB3mEGLqiY',
                    language: 'en',
                    //   components: 'country:us',
                }}
                nearbyPlacesAPI={"GooglePlacesSearch"}
                debounce={200}
                listViewDisplayed={'auto'}
                GooglePlacesSearchQuery={{
                    rankby: 'distance',
                    type: 'address'
                }}

                renderLeftButton={() => {
                    return (

                        <TouchableOpacity style={{ alignSelf: "center" }}>
                            <Image style={{ height: 20, width: 20, tintColor: Colors.white }} source={Images.backArrow}></Image>
                        </TouchableOpacity>
                    )
                }}
            // renderRow={rowData => {
            //   return (
            //     <View>
            //       { this.renderDescription(rowData)}
            //     </View>
            //   )
            // }}
            />
        </SafeAreaView>
    );
}
export default Test